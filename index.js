module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./service-account.json":
/*!******************************!*\
  !*** ./service-account.json ***!
  \******************************/
/*! exports provided: type, project_id, private_key_id, private_key, client_email, client_id, auth_uri, token_uri, auth_provider_x509_cert_url, client_x509_cert_url, default */
/***/ (function(module) {

module.exports = JSON.parse("{\"type\":\"service_account\",\"project_id\":\"lideres-sociales-bd152\",\"private_key_id\":\"e84553395fe898154129cb27a51594596aff86f6\",\"private_key\":\"-----BEGIN PRIVATE KEY-----\\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQClKAhJq0qeJ7G1\\nzVet6y4BCRy4e4yvfSYt1RqMxmmiOHF2vii15A4n5xGEFCJJlorjYbPQs+GxjghM\\n0TKvfTtI5c+q2nKyJM0CYBtcFczGisoUzuj3KHROIyMDMX0u9nD1vfCvqI+oMiZV\\n9X2QH9tebYkAp4xvQaQRgGhzULQCGSQqttvX1O8dsos8qsmhilM8T75yYIXAaNN/\\nSAscVLvtpvqsjwF3Fy4JS3KUKB/Iu327l6vzZUF10YAwN8QR29P0Lq7PTsMmJklw\\nQySyYpbpSKI6b4FwuNy0a2Qccf3KCaFlvImbVz0UR+9mZv03UnM8qfDkvoiqk4Ma\\nlQpnUGWVAgMBAAECggEAFqcYA+q/sFOnd1Uz6oHwbGnwi4k+sa9LtkudeaOZR4vQ\\nXEYW1WPzYRu7dk9AqLFCWY/EFyMlGLJvhwHGnvVhVU1YH4oY40BS8jl2ZzoCPVU6\\nBIKgx42/7hu5qK14CyH4OmyT/7PyCFLgmTezwjnsX9ZMTU+t3nC1bUUcjnyKRCMw\\nPRGnmOnDdXWvPqtH3SZqRQWSg79WdaE/nKKvwtVLA18nTtWA4ckyFsxTqkkgsHde\\nvstuC6BN6cSMmkW27L40gfgMtIZOcApP9MN4u1jCzdWgwcyyI2/FfDiI2I0kt4d3\\ncc/BD4eRdSBJup7cvRBY5By9K6dR8P2vanTP3DGhgQKBgQDRksnvx+vHxtA4Ly14\\nqsiuvhZOjjKJ9ANXytjCmNASaq1UtT73t2RKWXMiCs/xNqNboUBKAolWbfhnlnsK\\nhHZp5g44s2VPmP5TS49dWYSeIBTxejkNKaf+lQivZCLRXXr13dBZHJVDuSXuQHGv\\nwr8Mw0ld4irWEHAmtqqxDNHnYQKBgQDJvkpCFPD5kR68wfKUIlmnIRK/ObT/cdc4\\n8+gNbtc6VpI/z7QQt87NeScpE0Z/PEja7bWYtHHRSIorZuSIf36cfKrZ8OEi8frS\\nbaAdKQlQHWcbbAPgd20dO5uiUeXvX7U7UJTuwcerln2no/2UbYKvKyfABkszdjyG\\nZKY/ApCOtQKBgDHdxaMCZP3yNpVH1IAD0yCRJmmmuCvPHgSDxbP3/HqFW5zuS39R\\n6dEvG5AcFoKEx7YD1nJFzDv+ir7LNZe5ERv9m6CuPwsDknBraD+ED2Q4qgMTXAPf\\nx5NyDtD8+LPslFdwYiTyAipNIdNhDiwOILt+tDRcnmi1HJp5mH/sVv3BAoGBALhr\\nKCyyQfqRi1yXmu2JXRUnaaC+2tMsOMcKR9WHvl3I55ZHg15I+FXia0bkJOyIqjzf\\nUcLPKMBcpFm6jCyvB4l7eR8TUGsGcvfLWLNvYOEIj8mEeyLFaUxwwfunDB8/CUe4\\niPycza7wOdt2UIdPeEgylfq2hiX77qYX+dTdvlGFAoGAD0Fo/jiIC5vCqsIMWLel\\n3PiKW7Rhl0GA23RZJuKbAy18vGagG9lZPAyxWuEWM/YveNVOvoGrfQV4aan0cVGo\\nRZRt/7YI48X4PfMyJK85z2Ap/2hal2YBpG58DEb621Fvwh5DlGPYd0k81HW8p9xL\\n95Ei0wmUSvkyq0wXP8CHuDs=\\n-----END PRIVATE KEY-----\\n\",\"client_email\":\"firebase-adminsdk-qeckf@lideres-sociales-bd152.iam.gserviceaccount.com\",\"client_id\":\"117492886052555363846\",\"auth_uri\":\"https://accounts.google.com/o/oauth2/auth\",\"token_uri\":\"https://oauth2.googleapis.com/token\",\"auth_provider_x509_cert_url\":\"https://www.googleapis.com/oauth2/v1/certs\",\"client_x509_cert_url\":\"https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-qeckf%40lideres-sociales-bd152.iam.gserviceaccount.com\"}");

/***/ }),

/***/ "./src/apis/events.js":
/*!****************************!*\
  !*** ./src/apis/events.js ***!
  \****************************/
/*! exports provided: onNewUser, updateConfiguration */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "onNewUser", function() { return onNewUser; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "updateConfiguration", function() { return updateConfiguration; });
/* harmony import */ var firebase_functions__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! firebase-functions */ "firebase-functions");
/* harmony import */ var firebase_functions__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(firebase_functions__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _sendgrid_mail__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @sendgrid/mail */ "@sendgrid/mail");
/* harmony import */ var _sendgrid_mail__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_sendgrid_mail__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _store_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../store/store */ "./src/store/store.js");
/* harmony import */ var _store_actions_config_actions__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../store/actions/config-actions */ "./src/store/actions/config-actions.js");
/* harmony import */ var _utils_intl_utils__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../utils/intl-utils */ "./src/utils/intl-utils.js");
/* harmony import */ var _utils_mail_utils__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../utils/mail-utils */ "./src/utils/mail-utils.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }







const {
  app
} = firebase_functions__WEBPACK_IMPORTED_MODULE_0__["config"](); // events

const onNewUser = firebase_functions__WEBPACK_IMPORTED_MODULE_0__["auth"].user().onCreate(async user => {
  try {
    const {
      config
    } = _store_store__WEBPACK_IMPORTED_MODULE_2__["default"].getState();
    const {
      sendgrid: sendgridConfig
    } = firebase_functions__WEBPACK_IMPORTED_MODULE_0__["config"]();
    const currentLanguage = user.preferences && user.preferences.language || 'en';
    const currentTranslations = config.intlData.locales[currentLanguage];
    _sendgrid_mail__WEBPACK_IMPORTED_MODULE_1__["setApiKey"](sendgridConfig.key);

    const configEmail = _objectSpread({}, firebase_functions__WEBPACK_IMPORTED_MODULE_0__["config"](), {
      translations: currentTranslations
    });

    const emailData = {
      to: user.email,
      subject: Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_4__["translate"])(currentTranslations, 'sampleSubject'),
      message: Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_4__["translate"])(currentTranslations, 'sampleMessage', {
        username: user.email
      })
    };
    await Object(_utils_mail_utils__WEBPACK_IMPORTED_MODULE_5__["sendNotification"])(configEmail, _sendgrid_mail__WEBPACK_IMPORTED_MODULE_1__, emailData);
  } catch (e) {
    console.error('Error: running event onNewUser', e);
  }
});
const updateConfiguration = firebase_functions__WEBPACK_IMPORTED_MODULE_0__["firestore"].document(`configuration/${app.projectid}`).onUpdate(change => {
  const newConfig = change.after.data();
  _store_store__WEBPACK_IMPORTED_MODULE_2__["default"].dispatch(Object(_store_actions_config_actions__WEBPACK_IMPORTED_MODULE_3__["setConfiguration"])(newConfig));
});

/***/ }),

/***/ "./src/apis/firebase.js":
/*!******************************!*\
  !*** ./src/apis/firebase.js ***!
  \******************************/
/*! exports provided: firebaseClient, firebaseAdmin */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "firebaseClient", function() { return firebaseClient; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "firebaseAdmin", function() { return firebaseAdmin; });
/* harmony import */ var firebase_auth__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! firebase/auth */ "firebase/auth");
/* harmony import */ var firebase_auth__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(firebase_auth__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var firebase_database__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! firebase/database */ "firebase/database");
/* harmony import */ var firebase_database__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(firebase_database__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var firebase_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! firebase/firestore */ "firebase/firestore");
/* harmony import */ var firebase_firestore__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(firebase_firestore__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var firebase_functions__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! firebase-functions */ "firebase-functions");
/* harmony import */ var firebase_functions__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(firebase_functions__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var firebase_admin__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! firebase-admin */ "firebase-admin");
/* harmony import */ var firebase_admin__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(firebase_admin__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! firebase/app */ "firebase/app");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(firebase_app__WEBPACK_IMPORTED_MODULE_5__);







const serviceAccount = __webpack_require__(/*! ../../service-account.json */ "./service-account.json");

const {
  app
} = firebase_functions__WEBPACK_IMPORTED_MODULE_3__["config"]();
const firebaseClient = firebase_app__WEBPACK_IMPORTED_MODULE_5___default.a.initializeApp({
  apiKey: app.apikey,
  authDomain: app.authdomain,
  databaseURL: app.databaseurl,
  projectId: app.projectid,
  storageBucket: app.storagebucket,
  messagingSenderId: app.messagingsenderid
});
const firebaseAdmin = firebase_admin__WEBPACK_IMPORTED_MODULE_4__["initializeApp"]({
  credential: firebase_admin__WEBPACK_IMPORTED_MODULE_4__["credential"].cert(serviceAccount),
  databaseURL: app.databaseurl
});

/***/ }),

/***/ "./src/apis/middlewares.js":
/*!*********************************!*\
  !*** ./src/apis/middlewares.js ***!
  \*********************************/
/*! exports provided: authorization, files */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "authorization", function() { return authorization; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "files", function() { return files; });
/* harmony import */ var fs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! fs */ "fs");
/* harmony import */ var fs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(fs__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var os__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! os */ "os");
/* harmony import */ var os__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(os__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var path__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! path */ "path");
/* harmony import */ var path__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(path__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var busboy__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! busboy */ "busboy");
/* harmony import */ var busboy__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(busboy__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _utils_intl_utils__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../utils/intl-utils */ "./src/utils/intl-utils.js");
/* harmony import */ var _utils_response_utils__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../utils/response-utils */ "./src/utils/response-utils.js");
/* harmony import */ var _firebase__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./firebase */ "./src/apis/firebase.js");







function authorization(requiredClaims) {
  return async (req, res, next) => {
    try {
      let token = req.headers['authorization'] || ''; // checking empty token

      if (!token || !token.startsWith('Bearer ')) {
        res.status(403).json({
          message: Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_4__["translate"])(req.translations, 'authNotLogin')
        });
        return;
      } // chacking user get info from token


      token = token.split('Bearer ')[1];
      req.user = await _firebase__WEBPACK_IMPORTED_MODULE_6__["firebaseAdmin"].auth().verifyIdToken(token);
      req.user.claims = Object.keys(req.user); // checking claims

      if (requiredClaims.length > 0 && !req.user.claims.filter(item => requiredClaims.includes(item)).length) {
        res.status(403).json({
          message: Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_4__["translate"])(req.translations, 'authNotPriviliges')
        });
        return;
      }

      next();
    } catch (e) {
      res.status(403).json({
        message: Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_4__["translate"])(req.translations, 'authNotLogin')
      });
    }
  };
}
function files(options) {
  return async (req, res, next) => {
    try {
      const contentType = req.headers['content-type'];

      if (!contentType || contentType.indexOf('application/json') > -1) {
        return next();
      }

      const {
        maxFiles,
        maxFileSize
      } = options;
      const busboy = new busboy__WEBPACK_IMPORTED_MODULE_3___default.a({
        headers: req.headers,
        limits: {
          files: maxFiles || 5,
          fileSize: (maxFileSize || 5) * 1024 * 1024
        }
      });
      busboy.on('field', (fieldname, val) => req.body[fieldname] = val);
      busboy.on('file', (fieldname, file, filename) => {
        const {
          allowedExtentions
        } = options;

        if (!allowedExtentions.find(ext => filename.endsWith('.' + ext))) {
          res.status(400).json({
            message: Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_4__["translate"])(req.translations, 'uploadErrorExtentions', {
              extentions: allowedExtentions.join(', ')
            })
          });
          return;
        }

        file.on('limit', () => {
          res.status(400).json({
            message: Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_4__["translate"])(req.translations, 'uploadErrorSize', {
              maxFiles,
              maxFileSize
            })
          });
        });
        let tmpFilePath = path__WEBPACK_IMPORTED_MODULE_2___default.a.join(os__WEBPACK_IMPORTED_MODULE_1___default.a.tmpdir(), filename);
        req.body[fieldname] = (req.body[fieldname] || []).concat(tmpFilePath);
        file.pipe(fs__WEBPACK_IMPORTED_MODULE_0___default.a.createWriteStream(tmpFilePath));
      });
      busboy.on('finish', () => next());
      busboy.end(req.rawBody);
    } catch (e) {
      const error = Object(_utils_response_utils__WEBPACK_IMPORTED_MODULE_5__["getBackendError"])(e);
      res.status(500).json(error);
    }
  };
}

/***/ }),

/***/ "./src/apis/routes/catalog.js":
/*!************************************!*\
  !*** ./src/apis/routes/catalog.js ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! express */ "express");
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _firebase__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../firebase */ "./src/apis/firebase.js");
/* harmony import */ var _middlewares__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../middlewares */ "./src/apis/middlewares.js");
/* harmony import */ var _utils_database_utils__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../utils/database-utils */ "./src/utils/database-utils.js");
/* harmony import */ var _utils_model_utils__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../utils/model-utils */ "./src/utils/model-utils.js");
/* harmony import */ var _utils_response_utils__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../utils/response-utils */ "./src/utils/response-utils.js");
/* harmony import */ var _utils_validation_utils__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../utils/validation-utils */ "./src/utils/validation-utils.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }







 // helpers

const router = express__WEBPACK_IMPORTED_MODULE_0___default.a.Router();

const getModelValidation = async (req, model, associations) => {
  // validating structure
  const validationResponse = Object(_utils_validation_utils__WEBPACK_IMPORTED_MODULE_6__["validateModel"])(req, model, Object(_utils_model_utils__WEBPACK_IMPORTED_MODULE_4__["getCatalogModel"])(associations, req.config.appLanguages));
  const errors = validationResponse.errors;
  let data = validationResponse.data;
  return {
    errors,
    data
  };
}; // routes


router.get('/', async (req, res) => {
  try {
    const db = _firebase__WEBPACK_IMPORTED_MODULE_1__["firebaseAdmin"].firestore();
    const response = await Object(_utils_database_utils__WEBPACK_IMPORTED_MODULE_3__["getDbQuery"])(db, 'catalogs', req.query);
    res.json(response);
  } catch (e) {
    const error = Object(_utils_response_utils__WEBPACK_IMPORTED_MODULE_5__["getBackendError"])(e);
    res.status(500).json(error);
  }
});
router.get('/:id', async (req, res) => {
  try {
    const db = _firebase__WEBPACK_IMPORTED_MODULE_1__["firebaseAdmin"].firestore();
    const response = await Object(_utils_database_utils__WEBPACK_IMPORTED_MODULE_3__["getDbDocument"])(db, 'catalogs', req.params.id);
    res.json(response);
  } catch (e) {
    const error = Object(_utils_response_utils__WEBPACK_IMPORTED_MODULE_5__["getBackendError"])(e);
    res.status(500).json(error);
  }
});
router.post('/', Object(_middlewares__WEBPACK_IMPORTED_MODULE_2__["authorization"])(['isAdmin']), async (req, res) => {
  try {
    const model = _objectSpread({}, req.body, {
      createdAt: Date.now()
    });

    const db = _firebase__WEBPACK_IMPORTED_MODULE_1__["firebaseAdmin"].firestore();
    const parents = await Object(_utils_database_utils__WEBPACK_IMPORTED_MODULE_3__["getDbQueryByIds"])(db, 'catalogs', [model.parent]);
    const {
      errors,
      data
    } = await getModelValidation(req, model, {
      parents
    });

    if (Object.keys(errors).length > 0) {
      return res.status(400).json({
        errors
      });
    }

    let response = await db.collection('catalogs').add(data);
    response = _objectSpread({}, data, {
      id: response.id
    });
    res.json(response);
  } catch (e) {
    const error = Object(_utils_response_utils__WEBPACK_IMPORTED_MODULE_5__["getBackendError"])(e);
    res.status(500).json(error);
  }
});
router.patch('/:id', Object(_middlewares__WEBPACK_IMPORTED_MODULE_2__["authorization"])(['isAdmin']), async (req, res) => {
  try {
    const db = _firebase__WEBPACK_IMPORTED_MODULE_1__["firebaseAdmin"].firestore();
    const dbData = await Object(_utils_database_utils__WEBPACK_IMPORTED_MODULE_3__["getDbDocument"])(db, 'catalogs', req.params.id);
    const model = Object.assign(dbData, _objectSpread({}, req.body, {
      updatedAt: Date.now()
    }));
    const parents = await Object(_utils_database_utils__WEBPACK_IMPORTED_MODULE_3__["getDbQueryByIds"])(db, 'catalogs', [model.parent]);
    const {
      errors,
      data
    } = await getModelValidation(req, model, {
      parents
    });

    if (Object.keys(errors).length > 0) {
      return res.status(400).json({
        errors
      });
    }

    await db.collection('catalogs').doc(req.params.id).update(data);

    const response = _objectSpread({}, data, {
      id: req.params.id
    });

    res.json(response);
  } catch (e) {
    const error = Object(_utils_response_utils__WEBPACK_IMPORTED_MODULE_5__["getBackendError"])(e);
    res.status(500).json(error);
  }
});
/* harmony default export */ __webpack_exports__["default"] = (router);

/***/ }),

/***/ "./src/apis/routes/config.js":
/*!***********************************!*\
  !*** ./src/apis/routes/config.js ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var firebase_functions__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! firebase-functions */ "firebase-functions");
/* harmony import */ var firebase_functions__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(firebase_functions__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "axios");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! express */ "express");
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var memory_cache__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! memory-cache */ "memory-cache");
/* harmony import */ var memory_cache__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(memory_cache__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _middlewares__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../middlewares */ "./src/apis/middlewares.js");
/* harmony import */ var _firebase__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../firebase */ "./src/apis/firebase.js");
/* harmony import */ var _utils_database_utils__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../utils/database-utils */ "./src/utils/database-utils.js");
/* harmony import */ var _utils_intl_utils__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../utils/intl-utils */ "./src/utils/intl-utils.js");
/* harmony import */ var _utils_model_utils__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../utils/model-utils */ "./src/utils/model-utils.js");
/* harmony import */ var _utils_response_utils__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../utils/response-utils */ "./src/utils/response-utils.js");
/* harmony import */ var _utils_validation_utils__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../utils/validation-utils */ "./src/utils/validation-utils.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }












const router = express__WEBPACK_IMPORTED_MODULE_2___default.a.Router(); // services

router.post('/sync', Object(_middlewares__WEBPACK_IMPORTED_MODULE_4__["authorization"])(['isAdmin']), async (req, res) => {
  try {
    const {
      app,
      intl,
      rate
    } = firebase_functions__WEBPACK_IMPORTED_MODULE_0__["config"]();
    const db = _firebase__WEBPACK_IMPORTED_MODULE_5__["firebaseAdmin"].firestore();
    const dbData = await Object(_utils_database_utils__WEBPACK_IMPORTED_MODULE_6__["getDbDocument"])(db, 'configuration', app.projectid);
    const model = Object.assign(dbData, _objectSpread({}, req.body, {
      appLastSync: Date.now()
    }));
    const {
      errors,
      data: configurationData
    } = Object(_utils_validation_utils__WEBPACK_IMPORTED_MODULE_10__["validateModel"])(req, model, Object(_utils_model_utils__WEBPACK_IMPORTED_MODULE_8__["getConfigurationModel"])());

    if (Object.keys(errors).length > 0) {
      res.status(400).json({
        errors
      });
      return;
    }

    await db.collection('configuration').doc(app.projectid).set(configurationData);
    const intlData = await Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_7__["getIntlData"])(req, axios__WEBPACK_IMPORTED_MODULE_1___default.a, memory_cache__WEBPACK_IMPORTED_MODULE_3___default.a, db, intl, rate);

    const response = _objectSpread({}, configurationData, {
      intlData
    });

    res.json(response);
  } catch (e) {
    const error = Object(_utils_response_utils__WEBPACK_IMPORTED_MODULE_9__["getBackendError"])(e);
    res.status(500).json(error);
  }
});
router.get('/test', async (req, res) => {
  try {
    const db = _firebase__WEBPACK_IMPORTED_MODULE_5__["firebaseAdmin"].firestore();
    const response = await Object(_utils_database_utils__WEBPACK_IMPORTED_MODULE_6__["getDbQuery"])(db, 'locales', {});
    res.json(response);
  } catch (e) {
    const error = Object(_utils_response_utils__WEBPACK_IMPORTED_MODULE_9__["getBackendError"])(e);
    res.status(500).json(error);
  }
});
/* harmony default export */ __webpack_exports__["default"] = (router);

/***/ }),

/***/ "./src/apis/routes/locale.js":
/*!***********************************!*\
  !*** ./src/apis/routes/locale.js ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! express */ "express");
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _firebase__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../firebase */ "./src/apis/firebase.js");
/* harmony import */ var _utils_database_utils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../utils/database-utils */ "./src/utils/database-utils.js");
/* harmony import */ var _utils_model_utils__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../utils/model-utils */ "./src/utils/model-utils.js");
/* harmony import */ var _utils_response_utils__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../utils/response-utils */ "./src/utils/response-utils.js");
/* harmony import */ var _utils_validation_utils__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../utils/validation-utils */ "./src/utils/validation-utils.js");
/* harmony import */ var _middlewares__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../middlewares */ "./src/apis/middlewares.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }







 // helpers

const router = express__WEBPACK_IMPORTED_MODULE_0___default.a.Router();

const getModelValidation = async (req, model) => {
  // validating structure
  const validationResponse = Object(_utils_validation_utils__WEBPACK_IMPORTED_MODULE_5__["validateModel"])(req, model, Object(_utils_model_utils__WEBPACK_IMPORTED_MODULE_3__["getLocaleModel"])(req.config.appLanguages));
  const errors = validationResponse.errors;
  let data = validationResponse.data;
  return {
    errors,
    data
  };
}; // routes


router.get('/', async (req, res) => {
  try {
    const db = _firebase__WEBPACK_IMPORTED_MODULE_1__["firebaseAdmin"].firestore();
    const response = await Object(_utils_database_utils__WEBPACK_IMPORTED_MODULE_2__["getDbQuery"])(db, 'locales', req.query);
    res.json(response);
  } catch (e) {
    const error = Object(_utils_response_utils__WEBPACK_IMPORTED_MODULE_4__["getBackendError"])(e);
    res.status(500).json(error);
  }
});
router.get('/:id', async (req, res) => {
  try {
    const db = _firebase__WEBPACK_IMPORTED_MODULE_1__["firebaseAdmin"].firestore();
    const response = await Object(_utils_database_utils__WEBPACK_IMPORTED_MODULE_2__["getDbDocument"])(db, 'locales', req.params.id);
    res.json(response);
  } catch (e) {
    const error = Object(_utils_response_utils__WEBPACK_IMPORTED_MODULE_4__["getBackendError"])(e);
    res.status(500).json(error);
  }
});
router.post('/', Object(_middlewares__WEBPACK_IMPORTED_MODULE_6__["authorization"])(['isAdmin']), async (req, res) => {
  try {
    const model = _objectSpread({}, req.body, {
      createdAt: Date.now()
    });

    const db = _firebase__WEBPACK_IMPORTED_MODULE_1__["firebaseAdmin"].firestore();
    const {
      errors,
      data
    } = await getModelValidation(req, model);

    if (Object.keys(errors).length > 0) {
      return res.status(400).json({
        errors
      });
    }

    let response = await db.collection('locales').add(data);
    response = _objectSpread({}, data, {
      id: response.id
    });
    res.json(response);
  } catch (e) {
    const error = Object(_utils_response_utils__WEBPACK_IMPORTED_MODULE_4__["getBackendError"])(e);
    res.status(500).json(error);
  }
});
router.patch('/:id', Object(_middlewares__WEBPACK_IMPORTED_MODULE_6__["authorization"])(['isAdmin']), async (req, res) => {
  try {
    const db = _firebase__WEBPACK_IMPORTED_MODULE_1__["firebaseAdmin"].firestore();
    const dbData = await Object(_utils_database_utils__WEBPACK_IMPORTED_MODULE_2__["getDbDocument"])(db, 'locales', req.params.id);
    const model = Object.assign(dbData, _objectSpread({}, req.body, {
      updatedAt: Date.now()
    }));
    const {
      errors,
      data
    } = await getModelValidation(req, model);

    if (Object.keys(errors).length > 0) {
      return res.status(400).json({
        errors
      });
    }

    await db.collection('locales').doc(req.params.id).update(data);

    const response = _objectSpread({}, data, {
      id: req.params.id
    });

    res.json(response);
  } catch (e) {
    const error = Object(_utils_response_utils__WEBPACK_IMPORTED_MODULE_4__["getBackendError"])(e);
    res.status(500).json(error);
  }
});
router.delete('/:id', Object(_middlewares__WEBPACK_IMPORTED_MODULE_6__["authorization"])(['isAdmin']), async (req, res) => {
  try {
    const {
      id
    } = req.params;
    const db = _firebase__WEBPACK_IMPORTED_MODULE_1__["firebaseAdmin"].firestore();
    const response = await Object(_utils_database_utils__WEBPACK_IMPORTED_MODULE_2__["getDbDocument"])(db, 'locales', req.params.id);
    await db.collection('locales').doc(id).delete();
    res.json(response);
  } catch (e) {
    const error = Object(_utils_response_utils__WEBPACK_IMPORTED_MODULE_4__["getBackendError"])(e);
    res.status(500).json(error);
  }
});
/* harmony default export */ __webpack_exports__["default"] = (router);

/***/ }),

/***/ "./src/apis/routes/socialLeader.js":
/*!*****************************************!*\
  !*** ./src/apis/routes/socialLeader.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! express */ "express");
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _firebase__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../firebase */ "./src/apis/firebase.js");
/* harmony import */ var _utils_model_utils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../utils/model-utils */ "./src/utils/model-utils.js");
/* harmony import */ var _utils_validation_utils__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../utils/validation-utils */ "./src/utils/validation-utils.js");
/* harmony import */ var _utils_response_utils__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../utils/response-utils */ "./src/utils/response-utils.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


 // import { getDbQuery, getDbDocument } from '../../utils/database-utils';
// import { translate } from '../../utils/intl-utils';



 // import { authorization } from '../middlewares';
// helpers

const router = express__WEBPACK_IMPORTED_MODULE_0___default.a.Router();

const getModelValidation = async (req, model) => {
  // validating structure
  const passwordIsRequired = model.id ? false : true;
  const validationResponse = Object(_utils_validation_utils__WEBPACK_IMPORTED_MODULE_3__["validateModel"])(req, model, Object(_utils_model_utils__WEBPACK_IMPORTED_MODULE_2__["getReportModel"])(model, passwordIsRequired));
  const errors = validationResponse.errors;
  let data = validationResponse.data;
  return {
    errors,
    data
  };
}; // services


router.post('/report', async (req, res) => {
  try {
    const db = _firebase__WEBPACK_IMPORTED_MODULE_1__["firebaseAdmin"].firestore();
    const reportData = Object.keys(req.body).reduce((accum, key) => {
      return !['id'].includes(key) ? Object.assign(accum, {
        [key]: req.body[key]
      }) : accum;
    }, {});

    const model = _objectSpread({}, reportData, {
      createdAt: Date.now()
    });

    const {
      errors,
      data
    } = await getModelValidation(req, model);

    if (Object.keys(errors).length > 0) {
      return res.status(400).json({
        errors
      });
    }

    console.warn('data =>', data);
    await db.collection('reports').doc().set(reportData);
    res.json({
      created: true
    });
  } catch (e) {
    const error = Object(_utils_response_utils__WEBPACK_IMPORTED_MODULE_4__["getBackendError"])(e);
    res.status(500).json(error);
  }
});
/* harmony default export */ __webpack_exports__["default"] = (router);

/***/ }),

/***/ "./src/apis/routes/user.js":
/*!*********************************!*\
  !*** ./src/apis/routes/user.js ***!
  \*********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! express */ "express");
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _firebase__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../firebase */ "./src/apis/firebase.js");
/* harmony import */ var _utils_database_utils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../utils/database-utils */ "./src/utils/database-utils.js");
/* harmony import */ var _utils_intl_utils__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../utils/intl-utils */ "./src/utils/intl-utils.js");
/* harmony import */ var _utils_model_utils__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../utils/model-utils */ "./src/utils/model-utils.js");
/* harmony import */ var _utils_validation_utils__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../utils/validation-utils */ "./src/utils/validation-utils.js");
/* harmony import */ var _utils_response_utils__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../utils/response-utils */ "./src/utils/response-utils.js");
/* harmony import */ var _middlewares__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../middlewares */ "./src/apis/middlewares.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }








 // helpers

const router = express__WEBPACK_IMPORTED_MODULE_0___default.a.Router();

const getModelValidation = async (req, model) => {
  // validating structure
  const passwordIsRequired = model.id ? false : true;
  const validationResponse = Object(_utils_validation_utils__WEBPACK_IMPORTED_MODULE_5__["validateModel"])(req, model, Object(_utils_model_utils__WEBPACK_IMPORTED_MODULE_4__["getUserModel"])(model, passwordIsRequired));
  const errors = validationResponse.errors;
  let data = validationResponse.data;
  return {
    errors,
    data
  };
}; // services


router.get('/', Object(_middlewares__WEBPACK_IMPORTED_MODULE_7__["authorization"])(['isAdmin']), async (req, res) => {
  try {
    const db = _firebase__WEBPACK_IMPORTED_MODULE_1__["firebaseAdmin"].firestore();
    const response = await Object(_utils_database_utils__WEBPACK_IMPORTED_MODULE_2__["getDbQuery"])(db, 'users', req.query);
    res.json(response);
  } catch (e) {
    const error = Object(_utils_response_utils__WEBPACK_IMPORTED_MODULE_6__["getBackendError"])(e);
    res.status(500).json(error);
  }
});
router.get('/me', Object(_middlewares__WEBPACK_IMPORTED_MODULE_7__["authorization"])(['isRegistered']), async (req, res) => {
  try {
    let user = req.user;

    if (user) {
      const db = _firebase__WEBPACK_IMPORTED_MODULE_1__["firebaseAdmin"].firestore();
      const profile = await Object(_utils_database_utils__WEBPACK_IMPORTED_MODULE_2__["getDbDocument"])(db, 'users', user.uid);
      user = Object.assign(user, {
        profile
      });
    }

    res.json(user);
  } catch (e) {
    const error = Object(_utils_response_utils__WEBPACK_IMPORTED_MODULE_6__["getBackendError"])(e);
    res.status(500).json(error);
  }
});
router.get('/:id', Object(_middlewares__WEBPACK_IMPORTED_MODULE_7__["authorization"])(['isRegistered']), async (req, res) => {
  try {
    const db = _firebase__WEBPACK_IMPORTED_MODULE_1__["firebaseAdmin"].firestore();
    const response = await Object(_utils_database_utils__WEBPACK_IMPORTED_MODULE_2__["getDbDocument"])(db, 'users', req.params.id);
    res.json(response);
  } catch (e) {
    const error = Object(_utils_response_utils__WEBPACK_IMPORTED_MODULE_6__["getBackendError"])(e);
    res.status(500).json(error);
  }
});
router.get('/:id/claims', Object(_middlewares__WEBPACK_IMPORTED_MODULE_7__["authorization"])(['isAdmin']), async (req, res) => {
  try {
    const {
      id: userId
    } = req.params;
    const user = await _firebase__WEBPACK_IMPORTED_MODULE_1__["firebaseAdmin"].auth().getUser(userId);
    let response = [];

    if (user.customClaims) {
      response = Object.keys(user.customClaims);
    }

    res.send(response);
  } catch (e) {
    const error = Object(_utils_response_utils__WEBPACK_IMPORTED_MODULE_6__["getBackendError"])(e);
    res.status(500).json(error);
  }
});
router.post('/:id/claims', Object(_middlewares__WEBPACK_IMPORTED_MODULE_7__["authorization"])(['isAdmin']), async (req, res) => {
  try {
    const {
      id: userId
    } = req.params;
    const {
      claim
    } = req.body;
    const user = await _firebase__WEBPACK_IMPORTED_MODULE_1__["firebaseAdmin"].auth().getUser(userId);

    const claims = _objectSpread({}, user.customClaims, {
      [claim]: true
    });

    await _firebase__WEBPACK_IMPORTED_MODULE_1__["firebaseAdmin"].auth().setCustomUserClaims(userId, claims);
    res.json(claim);
  } catch (e) {
    const error = Object(_utils_response_utils__WEBPACK_IMPORTED_MODULE_6__["getBackendError"])(e);
    res.status(500).json(error);
  }
});
router.patch('/:id/claims', Object(_middlewares__WEBPACK_IMPORTED_MODULE_7__["authorization"])(['isAdmin']), async (req, res) => {
  try {
    const {
      id: userId
    } = req.params;
    const {
      claim
    } = req.body;
    const user = await _firebase__WEBPACK_IMPORTED_MODULE_1__["firebaseAdmin"].auth().getUser(userId);
    delete user.customClaims[claim];
    await _firebase__WEBPACK_IMPORTED_MODULE_1__["firebaseAdmin"].auth().setCustomUserClaims(userId, user.customClaims);
    res.json(claim);
  } catch (e) {
    const error = Object(_utils_response_utils__WEBPACK_IMPORTED_MODULE_6__["getBackendError"])(e);
    res.status(500).json(error);
  }
});
router.patch('/:id', Object(_middlewares__WEBPACK_IMPORTED_MODULE_7__["authorization"])(['isRegistered']), async (req, res) => {
  try {
    const db = _firebase__WEBPACK_IMPORTED_MODULE_1__["firebaseAdmin"].firestore();
    const dbData = await Object(_utils_database_utils__WEBPACK_IMPORTED_MODULE_2__["getDbDocument"])(db, 'users', req.params.id);
    const userData = Object.keys(req.body).reduce((accum, key) => {
      return !['email'].includes(key) ? Object.assign(accum, {
        [key]: req.body[key]
      }) : accum;
    }, {});
    const model = Object.assign(dbData, _objectSpread({}, userData, {
      updatedAt: Date.now()
    }));
    const {
      errors,
      data
    } = await getModelValidation(req, model);

    if (Object.keys(errors).length > 0) {
      res.status(400).json({
        errors
      });
      return;
    }

    const dataToSave = Object.keys(data).reduce((accum, key) => {
      return !['password', 'passwordConfirmation'].includes(key) ? Object.assign(accum, {
        [key]: data[key]
      }) : accum;
    }, {});
    await db.collection('users').doc(req.params.id).update(dataToSave);

    const response = _objectSpread({}, data, {
      id: req.params.id
    });

    res.json(response);
  } catch (e) {
    const error = Object(_utils_response_utils__WEBPACK_IMPORTED_MODULE_6__["getBackendError"])(e);
    res.status(500).json(error);
  }
});
router.post('/change-password', Object(_middlewares__WEBPACK_IMPORTED_MODULE_7__["authorization"])(['isRegistered']), async (req, res) => {
  try {
    const {
      body
    } = req;
    const {
      data,
      errors
    } = Object(_utils_validation_utils__WEBPACK_IMPORTED_MODULE_5__["validateModel"])(req, body, [{
      property: 'password',
      type: 'string',
      rules: [{
        required: true
      }, {
        minLength: 6
      }]
    }, {
      property: 'passwordConfirmation',
      type: 'string',
      rules: [{
        required: true
      }, {
        minLength: 6
      }]
    }]);

    if (Object.keys(errors).length > 0) {
      res.status(400).json({
        errors
      });
      return;
    }

    await _firebase__WEBPACK_IMPORTED_MODULE_1__["firebaseAdmin"].auth().updateUser(req.user.uid, {
      password: data.password
    });
    res.json({
      message: Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_3__["translate"])(req.translations, 'sucessfulOperation')
    });
  } catch (e) {
    const error = Object(_utils_response_utils__WEBPACK_IMPORTED_MODULE_6__["getBackendError"])(e);
    res.status(500).json(error);
  }
});
router.post('/recover-password', async (req, res) => {
  try {
    const {
      body
    } = req;
    const {
      data,
      errors
    } = Object(_utils_validation_utils__WEBPACK_IMPORTED_MODULE_5__["validateModel"])(req, body, [{
      property: 'email',
      type: 'string',
      rules: [{
        required: true
      }, {
        format: 'email'
      }]
    }]);

    if (Object.keys(errors).length > 0) {
      res.status(400).json({
        errors
      });
      return;
    }

    await _firebase__WEBPACK_IMPORTED_MODULE_1__["firebaseClient"].auth().sendPasswordResetEmail(data.email);
    res.json({
      message: Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_3__["translate"])(req.translations, 'recoverPasswordSuccess')
    });
  } catch (e) {
    const error = Object(_utils_response_utils__WEBPACK_IMPORTED_MODULE_6__["getBackendError"])(e);
    res.status(500).json(error);
  }
});
router.post('/login', async (req, res) => {
  try {
    const model = req.body;
    const {
      errors,
      data
    } = Object(_utils_validation_utils__WEBPACK_IMPORTED_MODULE_5__["validateModel"])(req, model, [{
      property: 'email',
      type: 'string',
      rules: [{
        required: true
      }, {
        format: 'email'
      }]
    }, {
      property: 'password',
      type: 'string',
      rules: [{
        required: true
      }]
    }]);

    if (Object.keys(errors).length > 0) {
      return res.status(400).json({
        errors
      });
    }

    const response = await _firebase__WEBPACK_IMPORTED_MODULE_1__["firebaseClient"].auth().signInWithEmailAndPassword(data.email, data.password);
    const token = await response.user.getIdToken();
    res.json({
      token
    });
  } catch (e) {
    const error = Object(_utils_response_utils__WEBPACK_IMPORTED_MODULE_6__["getBackendError"])(e);
    res.status(500).json(error);
  }
});
router.post('/register', async (req, res) => {
  try {
    const db = _firebase__WEBPACK_IMPORTED_MODULE_1__["firebaseAdmin"].firestore();
    const userData = Object.keys(req.body).reduce((accum, key) => {
      return !['id'].includes(key) ? Object.assign(accum, {
        [key]: req.body[key]
      }) : accum;
    }, {});

    const model = _objectSpread({}, userData, {
      createdAt: Date.now()
    });

    const {
      errors,
      data
    } = await getModelValidation(req, model);

    if (Object.keys(errors).length > 0) {
      return res.status(400).json({
        errors
      });
    } // registerig user


    const userRegistered = await _firebase__WEBPACK_IMPORTED_MODULE_1__["firebaseClient"].auth().createUserWithEmailAndPassword(data.email, data.password);
    const {
      uid
    } = userRegistered.user;
    const dataToSave = Object.keys(data).reduce((accum, key) => {
      return !['password', 'passwordConfirmation'].includes(key) ? Object.assign(accum, {
        [key]: data[key]
      }) : accum;
    }, {});
    await db.collection('users').doc(uid).set(dataToSave);
    await _firebase__WEBPACK_IMPORTED_MODULE_1__["firebaseAdmin"].auth().setCustomUserClaims(uid, {
      isRegistered: true
    }); // loging user to refresh custom claims

    const response = await _firebase__WEBPACK_IMPORTED_MODULE_1__["firebaseClient"].auth().signInWithEmailAndPassword(data.email, data.password);
    const token = await response.user.getIdToken();
    res.json({
      token
    });
  } catch (e) {
    const error = Object(_utils_response_utils__WEBPACK_IMPORTED_MODULE_6__["getBackendError"])(e);
    res.status(500).json(error);
  }
});
/* harmony default export */ __webpack_exports__["default"] = (router);

/***/ }),

/***/ "./src/context.js":
/*!************************!*\
  !*** ./src/context.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _store_store__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./store/store */ "./src/store/store.js");
/* harmony import */ var _store_actions_config_actions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./store/actions/config-actions */ "./src/store/actions/config-actions.js");
/* harmony import */ var _store_actions_user_actions__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./store/actions/user-actions */ "./src/store/actions/user-actions.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }






const changeCurrency = async currency => {
  try {
    const {
      user
    } = _store_store__WEBPACK_IMPORTED_MODULE_1__["default"].getState();
    const {
      auth
    } = user;
    await _store_store__WEBPACK_IMPORTED_MODULE_1__["default"].dispatch(Object(_store_actions_config_actions__WEBPACK_IMPORTED_MODULE_2__["getCurrencyConversions"])({
      currency,
      date: 'latest'
    }));
    _store_store__WEBPACK_IMPORTED_MODULE_1__["default"].dispatch(Object(_store_actions_config_actions__WEBPACK_IMPORTED_MODULE_2__["setCurrencyConversions"])(_store_store__WEBPACK_IMPORTED_MODULE_1__["default"].getState().config.temp));
    localStorage.currency = currency;
    _store_store__WEBPACK_IMPORTED_MODULE_1__["default"].dispatch(Object(_store_actions_config_actions__WEBPACK_IMPORTED_MODULE_2__["setPreferences"])({
      currency: currency
    }));

    if (auth) {
      const newPreferences = _objectSpread({}, auth.profile.preferences, {
        currency
      });

      _store_store__WEBPACK_IMPORTED_MODULE_1__["default"].dispatch(Object(_store_actions_user_actions__WEBPACK_IMPORTED_MODULE_3__["updateUser"])({
        id: auth.uid,
        preferences: newPreferences
      }));
    }
  } catch (e) {
    console.error(e);
  }
};

const changeDateFormat = async dateFormat => {
  try {
    const {
      user
    } = _store_store__WEBPACK_IMPORTED_MODULE_1__["default"].getState();
    const {
      auth
    } = user;
    localStorage.dateFormat = dateFormat;
    _store_store__WEBPACK_IMPORTED_MODULE_1__["default"].dispatch(Object(_store_actions_config_actions__WEBPACK_IMPORTED_MODULE_2__["setPreferences"])({
      dateFormat
    }));

    if (auth) {
      const newPreferences = _objectSpread({}, auth.profile.preferences, {
        dateFormat
      });

      _store_store__WEBPACK_IMPORTED_MODULE_1__["default"].dispatch(Object(_store_actions_user_actions__WEBPACK_IMPORTED_MODULE_3__["updateUser"])({
        id: auth.uid,
        preferences: newPreferences
      }));
    }
  } catch (e) {
    console.error(e);
  }
};

const changeLanguage = async language => {
  try {
    const {
      user
    } = _store_store__WEBPACK_IMPORTED_MODULE_1__["default"].getState();
    const {
      auth
    } = user;
    localStorage.language = language;
    _store_store__WEBPACK_IMPORTED_MODULE_1__["default"].dispatch(Object(_store_actions_config_actions__WEBPACK_IMPORTED_MODULE_2__["setPreferences"])({
      language: language
    }));

    if (auth) {
      const newPreferences = _objectSpread({}, auth.profile.preferences, {
        language
      });

      _store_store__WEBPACK_IMPORTED_MODULE_1__["default"].dispatch(Object(_store_actions_user_actions__WEBPACK_IMPORTED_MODULE_3__["updateUser"])({
        id: auth.uid,
        preferences: newPreferences
      }));
    }
  } catch (e) {
    console.error(e);
  }
};

const loadPreferences = () => {
  const {
    config,
    user
  } = _store_store__WEBPACK_IMPORTED_MODULE_1__["default"].getState();
  const defaultPreferences = config.preferences;
  const userPreferences = user.auth && user.auth.profile && user.auth.profile.preferences;
  const localPreferences = {
    currency: localStorage.currency || defaultPreferences.currency,
    dateFormat: localStorage.dateFormat || defaultPreferences.dateFormat,
    language: localStorage.language || defaultPreferences.language
  };
  const preferences = Object.assign(defaultPreferences, localPreferences, userPreferences);
  const {
    currency
  } = preferences;
  _store_store__WEBPACK_IMPORTED_MODULE_1__["default"].dispatch(Object(_store_actions_config_actions__WEBPACK_IMPORTED_MODULE_2__["getCurrencyConversions"])({
    currency,
    date: 'latest'
  })).then(() => _store_store__WEBPACK_IMPORTED_MODULE_1__["default"].dispatch(Object(_store_actions_config_actions__WEBPACK_IMPORTED_MODULE_2__["setCurrencyConversions"])(_store_store__WEBPACK_IMPORTED_MODULE_1__["default"].getState().config.temp)));
  _store_store__WEBPACK_IMPORTED_MODULE_1__["default"].dispatch(Object(_store_actions_config_actions__WEBPACK_IMPORTED_MODULE_2__["setPreferences"])(preferences));
};

const logout = async () => {
  try {
    localStorage.removeItem('token');
    await _store_store__WEBPACK_IMPORTED_MODULE_1__["default"].dispatch(Object(_store_actions_user_actions__WEBPACK_IMPORTED_MODULE_3__["logout"])());
    window.location.href = `${window.location.origin}/login`;
  } catch (e) {
    console.error(e);
  }
};

/* harmony default export */ __webpack_exports__["default"] = (react__WEBPACK_IMPORTED_MODULE_0___default.a.createContext({
  changeCurrency,
  changeDateFormat,
  changeLanguage,
  loadPreferences,
  logout
}));

/***/ }),

/***/ "./src/pages/_components/collapsible.js":
/*!**********************************************!*\
  !*** ./src/pages/_components/collapsible.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Collapsible; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);

class Collapsible extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  constructor(...args) {
    var _temp;

    return _temp = super(...args), this.componentDidUpdate = prevProps => {
      if (prevProps.isOpen !== this.props.isOpen) {
        this.collapsible.classList.toggle('collapsing');
        setTimeout(() => {
          this.collapsible.classList.toggle('collapsing');

          if (this.props.isOpen) {
            this.props.onEntering();
          }
        }, 0);
      }
    }, _temp;
  }

  render() {
    const {
      isOpen,
      className,
      children
    } = this.props;
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      ref: elem => this.collapsible = elem,
      "aria-expanded": isOpen,
      className: `${className} collapse ${isOpen && 'show'}`
    }, children);
  }

}

/***/ }),

/***/ "./src/pages/_components/footer.js":
/*!*****************************************!*\
  !*** ./src/pages/_components/footer.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Footer; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "react-router-dom");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_router_dom__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _utils_intl_utils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../utils/intl-utils */ "./src/utils/intl-utils.js");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! styled-components */ "styled-components");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_3__);




const FooterWrapper = styled_components__WEBPACK_IMPORTED_MODULE_3___default.a.footer`
  &, * {
    box-sizing: border-box;
  }

  .container {
    .slogan span {
      font-size: 1.25rem;
    }

    .wrapper-buttons {
      button {
        display: block;
        margin: 0 auto;
        margin-bottom: 10px;
      }
    }
  }
`;
function Footer(props) {
  const {
    config
  } = props;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(FooterWrapper, {
    className: "mt-4 mb-4 pt-4 border-top"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "container text-body"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "col-md-7 row"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "col-12 slogan"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: config.appLogo,
    alt: config.appName,
    width: 35
  }), ' ', /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, config.appName)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "col-12"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_2__["translate"])(config.translations, 'homeDescription'))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "col-12"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
    className: "list-inline"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "list-inline-item"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    href: "https://www.instagram.com/lideressociale3/",
    title: "Instagram",
    target: "_blank",
    rel: "noopener noreferrer"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    className: "fab fa-instagram fa-2x text-dark"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "list-inline-item"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    href: "https://www.facebook.com/Lideres-Sociales-106902417680240/",
    title: "Facebook",
    target: "_blank",
    rel: "noopener noreferrer"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    className: "fab fa-facebook fa-2x text-dark"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "list-inline-item"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    href: "https://twitter.com/LideresSociale3",
    title: "Twitter",
    target: "_blank",
    rel: "noopener noreferrer"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    className: "fab fa-twitter fa-2x text-dark"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "list-inline-item"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    href: "mailto:lideressocialescolombia@gmail.com",
    title: "Gmail",
    target: "_blank",
    rel: "noopener noreferrer"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    className: "fas fa-envelope fa-2x text-dark"
  })))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "col-md-5"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
    className: "list-group list-group-flush"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "list-group-item"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
    to: "/contact"
  }, Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_2__["translate"])(config.translations, 'mailContactSubject'))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "list-group-item"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
    to: "/app/dashboard"
  }, Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_2__["translate"])(config.translations, 'complaint'))))))));
}

/***/ }),

/***/ "./src/pages/_components/header.js":
/*!*****************************************!*\
  !*** ./src/pages/_components/header.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Header; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! styled-components */ "styled-components");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _navbar__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./navbar */ "./src/pages/_components/navbar.js");
/* harmony import */ var _navigation_bar__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./navigation-bar */ "./src/pages/_components/navigation-bar.js");




const HeaderWrapper = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.header`
  &, * {
    box-sizing: border-box;
  }
`;
function Header(props) {
  const {
    config,
    user,
    title,
    description,
    history,
    showHeading,
    btnLeft,
    btnRight
  } = props;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(HeaderWrapper, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_navbar__WEBPACK_IMPORTED_MODULE_2__["default"], {
    config: config,
    user: user,
    history: history
  }), showHeading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_navigation_bar__WEBPACK_IMPORTED_MODULE_3__["default"], {
    config: config,
    title: title,
    description: description,
    btnLeft: btnLeft,
    btnRight: btnRight
  }));
}

/***/ }),

/***/ "./src/pages/_components/menu-main.js":
/*!********************************************!*\
  !*** ./src/pages/_components/menu-main.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return MenuMain; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _utils_intl_utils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../utils/intl-utils */ "./src/utils/intl-utils.js");


function MenuMain(props) {
  const {
    auth,
    authLoaded,
    config,
    currentPath,
    onChangePath
  } = props;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
    className: "ml-auto navbar-nav"
  }, authLoaded && auth && auth.claims.find(item => item === 'isRegistered') && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    role: "menuitem",
    className: `nav-link ${currentPath === '/app/dashboard' && 'active'}`,
    href: "/app/dashboard",
    title: Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_1__["translate"])(config.translations, 'complaint'),
    onClick: e => {
      e.preventDefault();
      onChangePath(e.currentTarget.pathname);
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    className: "fas fa-assistive-listening-systems mr-1"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "d-md-none d-lg-inline-block"
  }, Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_1__["translate"])(config.translations, 'complaint')))), !auth && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    role: "menuitem",
    className: `nav-link ${currentPath === '/' && 'active'}`,
    href: "/",
    onClick: e => {
      e.preventDefault();
      onChangePath(e.currentTarget.pathname);
    }
  }, Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_1__["translate"])(config.translations, 'home')), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    role: "menuitem",
    className: `nav-link btn btn-info text-white ml-md-2 mb-2 mb-md-0 ${currentPath === '/login' && 'active'}`,
    href: "/login",
    onClick: e => {
      e.preventDefault();
      onChangePath(e.currentTarget.pathname);
    }
  }, Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_1__["translate"])(config.translations, 'login')), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    role: "menuitem",
    className: `nav-link btn btn-primary text-white ml-md-2 ${currentPath === '/register' && 'active'}`,
    href: "/register",
    onClick: e => {
      e.preventDefault();
      onChangePath(e.currentTarget.pathname);
    }
  }, Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_1__["translate"])(config.translations, 'register'))));
}

/***/ }),

/***/ "./src/pages/_components/menu-popovers.js":
/*!************************************************!*\
  !*** ./src/pages/_components/menu-popovers.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return MenuPopovers; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_select__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-select */ "react-select");
/* harmony import */ var react_select__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_select__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_popover__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../_components/popover */ "./src/pages/_components/popover.js");
/* harmony import */ var _utils_intl_utils__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../utils/intl-utils */ "./src/utils/intl-utils.js");




function MenuPopovers(props) {
  const [elemPopoverAdmin, setElemPopoverAdmin] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(null);
  const [elemPopoverPreferences, setElemPopoverPreferences] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(null);
  const [elemPopoverSession, setElemPopoverSession] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(null);
  const [showPopoverAdmin, setPopoverAdmin] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false);
  const [showPopoverPreferences, setPopoverPreferences] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false);
  const [showPopoverSession, setPopoverSession] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false);
  const {
    auth,
    authLoaded,
    config,
    onChangePath,
    onChangeDateFormat,
    onChangeLanguage,
    onLogout
  } = props; // const { 
  //   auth, authLoaded, config, 
  //   onChangePath, onChangeCurrency, onChangeDateFormat, onChangeLanguage, onLogout 
  // } = props;
  // const currencies = config.intlData.currencies || []; 

  const dateFormats = ['DD/MM/YYYY', 'MM/DD/YYYY', 'YYYY/MM/DD'].map(item => ({
    label: item,
    value: item
  }));
  const languages = config.intlData.languages || [];

  const clickAdminMenu = path => {
    setPopoverAdmin(false);
    onChangePath(path);
  };

  const clickSessionMenu = path => {
    setPopoverSession(false);
    onChangePath(path);
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    ref: elem => setElemPopoverPreferences(elem),
    className: "btn btn-light ml-1",
    title: Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_3__["translate"])(config.translations, 'preferences'),
    onClick: () => setPopoverPreferences(!showPopoverPreferences)
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    className: "fas fa-cog"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_popover__WEBPACK_IMPORTED_MODULE_2__["default"], {
    isOpen: showPopoverPreferences,
    target: elemPopoverPreferences,
    style: {
      width: 200
    },
    onToggle: () => setPopoverPreferences(!showPopoverPreferences)
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", {
    className: "popover-header"
  }, Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_3__["translate"])(config.translations, 'preferences')), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "popover-body"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "form-group"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", null, Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_3__["translate"])(config.translations, 'dateFormat')), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_select__WEBPACK_IMPORTED_MODULE_1___default.a, {
    placeholder: Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_3__["translate"])(config.translations, 'select'),
    options: dateFormats,
    value: dateFormats.find(item => item.value === config.preferences.dateFormat),
    onChange: option => onChangeDateFormat(option.value)
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "form-group"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", null, Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_3__["translate"])(config.translations, 'language')), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_select__WEBPACK_IMPORTED_MODULE_1___default.a, {
    placeholder: Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_3__["translate"])(config.translations, 'select'),
    options: languages.filter(item => config.appLanguages.includes(item.value)),
    value: languages.find(item => item.value === config.preferences.language),
    onChange: option => onChangeLanguage(option.value)
  })))), authLoaded && auth && auth.claims.find(item => item === 'isAdmin') && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    ref: elem => setElemPopoverAdmin(elem),
    className: "btn btn-light ml-1",
    title: Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_3__["translate"])(config.translations, 'admin'),
    onClick: () => setPopoverAdmin(!showPopoverAdmin)
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    className: "fas fa-lock"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_popover__WEBPACK_IMPORTED_MODULE_2__["default"], {
    isOpen: showPopoverAdmin,
    target: elemPopoverAdmin,
    style: {
      width: 200
    },
    onToggle: () => setPopoverAdmin(!showPopoverAdmin)
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", {
    className: "popover-header"
  }, Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_3__["translate"])(config.translations, 'admin')), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "popover-body"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
    className: "list-group list-group-flush"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "list-group-item"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    className: "btn p-0",
    onClick: () => clickAdminMenu('/admin/catalogs')
  }, Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_3__["translate"])(config.translations, 'catalogs'))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "list-group-item"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    className: "btn p-0",
    onClick: () => clickAdminMenu('/admin/configuration')
  }, Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_3__["translate"])(config.translations, 'configuration'))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "list-group-item"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    className: "btn p-0",
    onClick: () => clickAdminMenu('/admin/locales')
  }, Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_3__["translate"])(config.translations, 'locales'))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "list-group-item"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    className: "btn p-0",
    onClick: () => clickAdminMenu('/admin/users')
  }, Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_3__["translate"])(config.translations, 'users'))))))), auth && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    ref: elem => setElemPopoverSession(elem),
    className: "btn btn-light ml-1",
    title: Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_3__["translate"])(config.translations, 'yourAccount'),
    onClick: () => setPopoverSession(!showPopoverSession)
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    className: "fas fa-user"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_popover__WEBPACK_IMPORTED_MODULE_2__["default"], {
    isOpen: showPopoverSession,
    target: elemPopoverSession,
    style: {
      width: 200
    },
    onToggle: () => setPopoverSession(!showPopoverSession)
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", {
    className: "popover-header"
  }, Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_3__["translate"])(config.translations, 'yourAccount')), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "popover-body"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
    className: "list-group list-group-flush"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "list-group-item"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    className: "btn p-0",
    onClick: () => clickSessionMenu('/app/user/profile')
  }, Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_3__["translate"])(config.translations, 'profile'))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    className: "list-group-item"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    className: "btn p-0",
    onClick: () => {
      setPopoverSession(false);
      onLogout();
    }
  }, Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_3__["translate"])(config.translations, 'logout'))))))));
}

/***/ }),

/***/ "./src/pages/_components/navbar.js":
/*!*****************************************!*\
  !*** ./src/pages/_components/navbar.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Navbar; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! styled-components */ "styled-components");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _context__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../context */ "./src/context.js");
/* harmony import */ var _collapsible__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./collapsible */ "./src/pages/_components/collapsible.js");
/* harmony import */ var _menu_main__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./menu-main */ "./src/pages/_components/menu-main.js");
/* harmony import */ var _menu_popovers__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./menu-popovers */ "./src/pages/_components/menu-popovers.js");






const NavbarWrapper = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.nav`
  &, * {
    box-sizing: border-box;
  }

  .navbar-collapse {
    order: 0;
  }
  .navbar-options {
    order: 1;
    margin-left: auto;

    .btn {
      color: #666;
    }
  }    
  .navbar-toggler {
    order: 2;
  }

  @media screen and (max-width: 768px) {
    .navbar.navbar-light {
      z-index: 9;
      .navbar-toggler {
        color: #666;
        border: none;
        padding: inherit;
        font-size: inherit;
        line-height: normal;
      }
    }
    .navbar-options {
      order: 0;
    }
    .navbar-toggler {
      order: 1;
    }
    .navbar-collapse {
      order: 2;
      ul > a:not(.btn), 
      ul > li {
        border-top: 1px solid #EEE;
        padding-left: 16px;
        padding-right: 16px;

        &.active,
        &:hover {
          background-color: rgba(161, 194, 250, 0.16);
        }
      }
    }
  }
`;
function Navbar(props) {
  const [showMenu, setMenu] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false);
  const context = Object(react__WEBPACK_IMPORTED_MODULE_0__["useContext"])(_context__WEBPACK_IMPORTED_MODULE_2__["default"]);
  const {
    config,
    user,
    history
  } = props;
  const {
    auth,
    authLoaded
  } = user;
  const currentPath = history.location.pathname;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(NavbarWrapper, {
    className: "border-bottom navbar navbar-expand-md navbar-light bg-light"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    className: "btn navbar-brand",
    onClick: () => history.push('/')
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: config.appLogo,
    alt: config.appName,
    width: 35
  }), ' ', /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, config.appName)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_collapsible__WEBPACK_IMPORTED_MODULE_3__["default"], {
    className: "navbar-collapse",
    isOpen: showMenu
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_menu_main__WEBPACK_IMPORTED_MODULE_4__["default"], {
    auth: auth,
    authLoaded: authLoaded,
    config: config,
    currentPath: currentPath,
    onChangePath: path => {
      setMenu(false);
      history.push(path);
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "navbar-options"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_menu_popovers__WEBPACK_IMPORTED_MODULE_5__["default"], {
    auth: auth,
    authLoaded: authLoaded,
    config: config,
    onChangePath: path => history.push(path),
    onChangeCurrency: context.changeCurrency,
    onChangeDateFormat: context.changeDateFormat,
    onChangeLanguage: context.changeLanguage,
    onLogout: context.logout
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    "aria-label": "Toggle navigation",
    className: "navbar-toggler border-0 btn btn-light ml-1",
    onClick: () => setMenu(!showMenu)
  }, !showMenu && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    className: "fas fa-bars"
  }), showMenu && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    className: "fas fa-times"
  })));
}

/***/ }),

/***/ "./src/pages/_components/navigation-bar.js":
/*!*************************************************!*\
  !*** ./src/pages/_components/navigation-bar.js ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return NavigationBar; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-helmet */ "react-helmet");
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_helmet__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! styled-components */ "styled-components");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_2__);



const NavigationBarWrapper = styled_components__WEBPACK_IMPORTED_MODULE_2___default.a.section`
  &, * {
    box-sizing: border-box;
  }

  display: flex;
  align-items: center;
  justify-content: center;

  .left-box {
    flex-grow: 0;
    text-align: left;
  }

  .center-box {
    flex-grow: 1;
    max-width: 100%;
    text-align: center;

    h1 {
      font-size: 2rem;
    }
  }

  .right-box {
    flex-grow: 0;
    text-align: right;
  }

  @media screen and (max-width: 768px) {
    .center-box h1 {
      font-size: 1.5rem;
    }
  }
`;
function NavigationBar(props) {
  const {
    config,
    title,
    description,
    btnLeft,
    btnRight
  } = props;
  const keywords = (props.keywords || []).concat('sample');
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_helmet__WEBPACK_IMPORTED_MODULE_1__["Helmet"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("html", {
    lang: config.preferences.language
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("title", null, props.title), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("meta", {
    charset: "utf-8"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("meta", {
    name: "description",
    content: props.description
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("meta", {
    name: "keywords",
    content: keywords.join(', ')
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("meta", {
    name: "robots",
    content: "index, follow"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("meta", {
    property: "og:title",
    content: props.title
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("meta", {
    property: "og:type",
    content: "business.business"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("meta", {
    property: "og:site_name",
    content: config.appName
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("meta", {
    property: "og:description",
    content: props.title
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("meta", {
    name: "twitter:card",
    content: props.description
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("meta", {
    name: "twitter:creator",
    content: "@jhonfredynova"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("meta", {
    name: "twitter:site",
    content: "@jhonfredynova"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(NavigationBarWrapper, {
    className: "container-fluid mb-4 border-bottom"
  }, btnLeft && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "left-box"
  }, btnLeft), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "center-box",
    role: "heading"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
    className: "mt-2"
  }, title), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "mt-2"
  }, description)), btnRight && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "right-box"
  }, btnRight)));
}

/***/ }),

/***/ "./src/pages/_components/news.js":
/*!***************************************!*\
  !*** ./src/pages/_components/news.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return News; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
 // import styled from 'styled-components';

function News(props) {
  const {
    news
  } = props;
  news.map(obj => {
    obj.dateForSorted = new Date(obj.publicationDate);
  });
  news.sort((a, b) => b.dateForSorted - a.dateForSorted);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, news[0] && news.map((itemNew, index) => /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    key: index,
    className: "card"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "card-body"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h5", {
    className: "card-title"
  }, itemNew.title), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "card-text"
  }, itemNew.description, "."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    href: itemNew.linkToPage,
    className: "btn btn-primary"
  }, "Conocer M\xE1s"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "card-text"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("small", {
    className: "text-muted"
  }, itemNew.publicationDate))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: itemNew.image,
    className: "card-img-bottom",
    alt: itemNew.title
  }))));
}

/***/ }),

/***/ "./src/pages/_components/popover.js":
/*!******************************************!*\
  !*** ./src/pages/_components/popover.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Popover; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! styled-components */ "styled-components");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_1__);
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }



const PopoverWrapper = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.div`
  &, * {
    box-sizing: border-box;
  }

  display: none;
  background: white;
  border: 1px solid #ccc;
  position: absolute;
  top: 0px;
  left: 0px;
  width: 300px;
  z-index: 1000;

  &.show {
    display: block;
  }
`;
class Popover extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  constructor(...args) {
    var _temp;

    return _temp = super(...args), this.state = {
      screenWidth: 0
    }, this.componentDidMount = () => {
      document.addEventListener('click', this.onDocumentClick);
      window.addEventListener('resize', this.onResizeWindow);
      window.dispatchEvent(new CustomEvent('resize'));
    }, this.componentWillUnMount = () => {
      document.removeEventListener('click', this.onDocumentClick);
      window.removeEventListener('resize', this.onResizeWindow);
    }, this.onDocumentClick = event => {
      const {
        isOpen,
        target
      } = this.props;

      if (isOpen && !target.contains(event.target) && !this.popover.contains(event.target)) {
        this.props.onToggle();
      }
    }, this.onResizeWindow = () => {
      this.setState({
        screenWidth: window.innerWidth
      });
    }, this.getPopoverPosition = targetElem => {
      const {
        style
      } = this.props;
      const popoverWidth = style && style.width || 300;
      const targetPosition = targetElem ? targetElem.getBoundingClientRect() : null;
      const targetWidth = targetElem ? targetElem.clientWidth : 0;
      const targetHeight = targetElem ? targetElem.clientHeight : 0;
      const targetPositionX = targetPosition ? targetPosition.x : 0;
      const targetPositionY = targetPosition ? targetPosition.y : 0;
      const screenWidth = this.state.screenWidth - 10;
      let popoverPositionX = targetPositionX + targetWidth / 2 - popoverWidth / 2;
      let popoverPositionY = targetPositionY + (targetHeight + 10);
      return {
        x: popoverPositionX + popoverWidth > screenWidth ? screenWidth - popoverWidth - 5 : popoverPositionX,
        y: popoverPositionY
      };
    }, _temp;
  }

  render() {
    const {
      isOpen,
      className,
      children,
      target,
      style
    } = this.props;
    const targetPosition = this.getPopoverPosition(target);
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(PopoverWrapper, {
      ref: elem => this.popover = elem,
      "aria-expanded": isOpen,
      className: `${className} ${isOpen && 'show'}`,
      style: _objectSpread({}, style, {
        transform: `translate3d(${targetPosition.x}px, ${targetPosition.y}px, 0px)`
      })
    }, children);
  }

}

/***/ }),

/***/ "./src/pages/any.json":
/*!****************************!*\
  !*** ./src/pages/any.json ***!
  \****************************/
/*! exports provided: news, default */
/***/ (function(module) {

module.exports = JSON.parse("{\"news\":[{\"linkToPage\":\"https://www.semana.com/nacion/articulo/patrocinio-bonilla-lider-afrocolombiano-asesinado-en-alto-baudo--colombia-hoy/693598\",\"image\":\"https://static.iris.net.co/semana/upload/images/2020/8/12/693597_1.jpg\",\"publicationDate\":\" | 2020/08/12 06:47\",\"title\":\"Asesinan a reconocido líder afrocolombiano en Alto Baudó, Chocó\",\"description\":\"Este hecho despertó el rechazo e indignación de varias organizaciones, quienes una vez más le exigen al Gobierno nacional protección para los líderes sociales en todo el país.\"},{\"linkToPage\":\"https://www.semana.com/nacion/articulo/luis-carlos-gomez-lider-social-asesinado-en-cimitarra-santander--colombia-hoy/690498\",\"image\":\"https://static.iris.net.co/semana/upload/images/2020/4/9/662789_1.jpg\",\"publicationDate\":\" | 2020/07/31 06:14\",\"title\":\"Denuncian asesinato de líder comunal en Cimitarra, Santander\",\"description\":\"Se trata de Luis Carlos Gómez, quien se desempeñaba como presidente de la Junta de Acción Comunal de la vereda Aterrado. De acuerdo con cifras actualizadas de Indepaz, 173 líderes sociales y defensores de los derechos humanos han sido asesinados durante el año.\"},{\"linkToPage\":\"https://www.semana.com/nacion/articulo/francia-marquez-y-su-lucha-bajo-fuego-por-la-defensa-del-agua-en-colombia/689935\",\"image\":\"https://static.iris.net.co/semana/upload/images/2020/7/29/689941_1.jpg\",\"publicationDate\":\" | 2020/07/29 08:40\",\"title\":\"Francia Márquez y su lucha bajo fuego por la defensa del agua en Colombia\",\"description\":\"Todavía bajo protección estatal, Francia está lejos de su territorio, pero no ha permitido que el miedo le reduzca la sonrisa ni su compromiso ambiental en un país donde la violencia brota cada tanto.\"},{\"linkToPage\":\"https://www.semana.com/nacion/articulo/respaldan-al-arzobispo-de-cali-tras-desautorizacion-de-nunciatura-i-colombia-hoy/685156\",\"image\":\"https://static.iris.net.co/semana/upload/images/2019/3/12/605180_1.jpg\",\"publicationDate\":\" | 2020/07/09 07:02\",\"title\":\"Respaldan al arzobispo de Cali tras desautorización de Nunciatura \",\"description\":\"Luego de que la representación diplomática del papa en el país se pronunciara y desautorizara las declaraciones del religioso, una avalancha de voces ha salido en respaldo del máximo líder de la Iglesia católica en el Valle. \"},{\"linkToPage\":\"https://www.semana.com/enfoque/articulo/la-marcha-por-la-dignidad-y-otras-notas-de-la-semana/682518\",\"image\":\"https://static.iris.net.co/semana/upload/images/2020/6/26/682538_1.jpg\",\"publicationDate\":\" | 2020/06/27 02:00\",\"title\":\"La marcha por la dignidad y otras notas de la semana\",\"description\":\"La marcha de varias organizaciones sociales desde Popayán, el método Bunbury, el ensayo del físico italiano Paolo Giordano, la reflexión del Consejero de Clima y Bosque de la Real Embajada de Noruega en Bogotá y más.\"},{\"linkToPage\":\"https://www.semana.com/nacion/articulo/recuperan-cuerpo-del-lider-edier-lopera-nueve-dias-despues-de-su-asesinato/681968\",\"image\":\"https://static.iris.net.co/semana/upload/images/2020/6/22/681406_1.jpg\",\"publicationDate\":\" | 2020/06/25 06:05\",\"title\":\"Recuperan cuerpo del líder Edier Lopera nueve días después de su asesinato\",\"description\":\"La falta de condiciones de seguridad impidieron que la Fuerza Pública rescatara al integrante de la JAC de la vereda Urales, asesinado presuntamente por Los Caparros el pasado 15 de junio. \"},{\"linkToPage\":\"https://www.semana.com/opinion/articulo/columna-los-falsos-lideres-sociales/681123\",\"image\":\"https://static.iris.net.co/semana/upload/images/2019/11/9/639720_1.jpg\",\"publicationDate\":\" | 2020/06/20 04:00\",\"title\":\"Los falsos líderes sociales\",\"description\":\"Si en la otra Colombia a un personaje le asignan carro blindado y dos guardaespaldas, dan por hecho que es un duro con amigos poderosos. \"},{\"linkToPage\":\"https://www.semana.com/nacion/articulo/recompensa-de-30-millones-para-dar-con-el-paradero-de-asesinos-de-lider-social/680110\",\"image\":\"https://static.iris.net.co/semana/upload/images/2020/6/17/680108_1.jpg\",\"publicationDate\":\" | 2020/06/17 08:49\",\"title\":\"Recompensa de 30 millones para dar con el paradero de asesinos de líder social\",\"description\":\"Se trata de José Manuel Ortiz, docente y líder social asesinado en las últimas horas en el municipio de Barranco de Loba, Bolívar.\"},{\"linkToPage\":\"https://www.semana.comhttp://justiciarural.com/somos-negros-e-indigenas-pero-tambien-somos-victimas-yolanda-perea-mosquera/\",\"image\":\"https://static.iris.net.co/semana/upload/images/2020/6/12/679321_1.jpg\",\"publicationDate\":\" | 2020/06/12 11:00\",\"title\":\"“Somos negros e indígenas, pero también somos víctimas”: Yolanda Perea Mosquera\",\"description\":\"Es una de las nuevas integrantes del Consejo Nacional de Paz, Reconciliación y Convivencia. Sobre el nombramiento de Paloma Valencia afirma: “¿Ella tiene una posición muy dura frente al proceso? Eso también es bueno porque nos permite un debate”.\"},{\"linkToPage\":\"https://www.semana.com/nacion/articulo/capturados-seis-presuntos-responsables-de-amenazas-a-lideres-sociales/678388\",\"image\":\"https://static.iris.net.co/semana/upload/images/2018/2/13/557012_1.jpg\",\"publicationDate\":\" | 2020/06/09 14:52\",\"title\":\"Capturados seis presuntos responsables de amenazas a líderes sociales\",\"description\":\"De acuerdo con el Gobierno nacional, serían responsables del desplazamiento de líderes del programa de sustitución de cultivos ilícitos.\"},{\"linkToPage\":\"https://www.dejusticia.org/estado-debe-acoger-recomendaciones-lideres-sociales-cidh-covid-19/\",\"image\":\"data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20325%20225'%3E%3C/svg%3E\",\"publicationDate\":\"julio 8, 2020\",\"title\":\"El Estado colombiano debe acoger las recomendaciones de la CIDH en materia de líderes sociales incluso durante la pandemia COVID-19\",\"description\":\"Los líderes y lideresas sociales cumplen un rol fundamental en el mantenimiento del tejido social en sus comunidades, a menudo bajo condiciones precarias de seguridad. En el actual escenario de la pandemia por COVID-19, las recomendaciones de la CIDH cobran especial relevancia.\"},{\"linkToPage\":\"https://www.dejusticia.org/lideres-sociales-en-la-pandemia/\",\"image\":\"data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20325%20166'%3E%3C/svg%3E\",\"publicationDate\":\"junio 4, 2020\",\"title\":\"Un camino hacia la pospandemia: el rol de los defensores y líderes sociales\",\"description\":\"Aunque la labor de personas defensoras de derechos humanos y líderes sociales en tiempos de normalidad es fundamental para la democracia, en estos momentos contar con estos liderazgos resulta indispensable para superar la pandemia en los países del Sur Global.\"},{\"linkToPage\":\"https://www.dejusticia.org/tribunal-superior-de-bogota-confirma-fallo-el-derecho-a-defender-derechos/\",\"image\":\"data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20325%20217'%3E%3C/svg%3E\",\"publicationDate\":\"may 20, 2020\",\"title\":\"Tribunal Superior de Bogotá reitera que el derecho a defender derechos humanos de líderes sociales debe ser protegido\",\"description\":\"Este importante fallo ratifica la urgente necesidad de garantizar  este derecho   a las lideresas y líderes sociales y personas defensoras de derechos humanos, en la actual de situación de riesgo en la que se encuentran\"},{\"linkToPage\":\"https://www.dejusticia.org/le-damos-la-bienvenida-a-la-nueva-relatora-especial-sobre-la-situacion-de-los-defensores-de-ddhh-mary-lawlor/\",\"image\":\"data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20325%20217'%3E%3C/svg%3E\",\"publicationDate\":\"may 1, 2020\",\"title\":\"Le damos la bienvenida a la nueva Relatora Especial sobre la situación de los defensores de DD.HH., Mary Lawlor\",\"description\":\"En continuidad con la labor de Michel Forst, esperamos que la Relatora Especial visite Colombia y le de seguimiento al informe realizado por su antecesor.\"},{\"linkToPage\":\"https://www.dejusticia.org/juzgado-falla-a-favor-de-lideres-sociales-y-ordena-que-se-garantice-el-derecho-a-defender-derechos-humanos/\",\"image\":\"data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20325%20217'%3E%3C/svg%3E\",\"publicationDate\":\"april 3, 2020\",\"title\":\"Juzgado falla a favor de líderes sociales y ordena que se garantice el derecho a defender derechos humanos\",\"description\":\"La jueza de tutela enfatizó que los derechos fundamentales de los defensores de derechos humanos no se suspenden en ningún momento y eso incluye los estados de excepción. Por lo tanto, el COVID-19 no puede ser excusa para incumplir con las órdenes de esta tutela. \"},{\"linkToPage\":\"https://www.dejusticia.org/mas-que-autores-de-obituarios-dilemas-eticos-de-cubrir-lideres-sociales/\",\"image\":\"data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20325%20216'%3E%3C/svg%3E\",\"publicationDate\":\"march 30, 2020\",\"title\":\"Más que autores de obituarios: dilemas éticos de cubrir líderes sociales\",\"description\":\"El periodismo nacional se enfrenta a un panorama extraño para el que no estaba preparado. Los desplazamientos, las amenazas y los asesinatos siguen siendo noticia en paralelo con las historias de búsqueda de la verdad y los avances atropellados de la justicia transicional.\"},{\"linkToPage\":\"https://www.dejusticia.org/lideres-en-peligro-el-tribunal-lo-reconoce-la-presidencia-lo-niega/\",\"image\":\"data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20325%20167'%3E%3C/svg%3E\",\"publicationDate\":\"january 22, 2020\",\"title\":\"Líderes en peligro: el Tribunal lo reconoce, la Presidencia lo niega\",\"description\":\"Consideramos que la respuesta del Tribunal Superior de Bogotá es un paso importante en la protección del derecho a defender derechos humanos en Colombia.\"},{\"linkToPage\":\"https://www.dejusticia.org/intervenciones-a-favor-de-la-tutela-lideres-sociales/\",\"image\":\"data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20325%20217'%3E%3C/svg%3E\",\"publicationDate\":\"january 20, 2020\",\"title\":\"#ElDerechoADefenderDerechos: Intervenciones a favor de la tutela que busca proteger líderes sociales\",\"description\":\"Académicas y académicos de universidades de México, Estados Unidos y Colombia, hicieron dos intervenciones ciudadanas que apoyan la tutela interpuesta por diez (10) líderes y lideresas sociales el 10 de diciembre de 2019. \"},{\"linkToPage\":\"https://www.dejusticia.org/barbosa-no-debe-ser-fiscal/\",\"image\":\"data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20325%20183'%3E%3C/svg%3E\",\"publicationDate\":\"january 19, 2020\",\"title\":\"Barbosa no debe ser fiscal\",\"description\":\"Hay dos razones posibles de ese grosero error de Barbosa en el tema más grave del país: la matazón de líderes sociales. O no comprende la información que debe orientar la política criminal, o actúa de mala fe. Cualquiera de las dos razones lo hace no apto para ser fiscal general, de quien se espera que actúe de buena fe y comprenda la política criminal.\"},{\"linkToPage\":\"https://www.dejusticia.org/los-asesinatos-no-dan-tregua-navidena/\",\"image\":\"data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20325%20120'%3E%3C/svg%3E\",\"publicationDate\":\"december 29, 2019\",\"title\":\"Los asesinatos no dan tregua navideña\",\"description\":\"El gobierno Duque tiene que reconocer la ineficacia de su política y reorientarla, poniendo en marcha los mecanismos previstos en el Acuerdo de Paz, que podrían tener mejores resultados.\"}]}");

/***/ }),

/***/ "./src/pages/home.js":
/*!***************************!*\
  !*** ./src/pages/home.js ***!
  \***************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-redux */ "react-redux");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! styled-components */ "styled-components");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _components_header__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./_components/header */ "./src/pages/_components/header.js");
/* harmony import */ var _components_news__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./_components/news */ "./src/pages/_components/news.js");
/* harmony import */ var _components_footer__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./_components/footer */ "./src/pages/_components/footer.js");
/* harmony import */ var _utils_intl_utils__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../utils/intl-utils */ "./src/utils/intl-utils.js");
/* harmony import */ var _any_json__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./any.json */ "./src/pages/any.json");
var _any_json__WEBPACK_IMPORTED_MODULE_7___namespace = /*#__PURE__*/__webpack_require__.t(/*! ./any.json */ "./src/pages/any.json", 1);







const HomeWrapper = styled_components__WEBPACK_IMPORTED_MODULE_2___default.a.main`
  &, * {
    box-sizing: border-box;
  }
`;


class Home extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  constructor(...args) {
    var _temp;

    return _temp = super(...args), this.state = {
      error: null,
      isLoading: false,
      news: []
    }, this.fetchNews = async () => {
      this.setState({
        isLoading: true,
        error: null
      }); // try {
      //   const response = await fetch('https://evident-factor-259411.appspot.com/api/scraping/analyzeWebs');
      //   const data = await response.json();
      //   this.setState({
      //     isLoading: false,
      //     news: data
      //   });
      // } catch (error) {
      //   this.setState({
      //     isLoading: false,
      //     error: error
      //   });
      // }

      this.setState({
        isLoading: false,
        news: _any_json__WEBPACK_IMPORTED_MODULE_7__.news
      });
    }, _temp;
  }

  async componentDidMount() {
    await this.fetchNews();
  }

  render() {
    const {
      config,
      user
    } = this.props;
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_header__WEBPACK_IMPORTED_MODULE_3__["default"], {
      config: config,
      user: user,
      title: Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_6__["translate"])(config.translations, 'homeTitle'),
      description: Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_6__["translate"])(config.translations, 'homeDescription'),
      history: this.props.history,
      showHeading: true
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(HomeWrapper, {
      className: "container-fluid"
    }, this.state.isLoading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "text-center"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "spinner-border text-info",
      role: "status"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "sr-only"
    }, "Loading..."))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_news__WEBPACK_IMPORTED_MODULE_4__["default"], {
      news: this.state.news
    }), this.state.error && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "text-center"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      className: "text-danger"
    }, "Noticias No Disponibles"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_footer__WEBPACK_IMPORTED_MODULE_5__["default"], {
      config: config,
      routerHistory: this.props.history
    }));
  }

}

const mapStateToProps = state => ({
  config: state.config,
  catalog: state.catalog,
  plan: state.plan,
  user: state.user
});

/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_1__["connect"])(mapStateToProps)(Home));

/***/ }),

/***/ "./src/pages/login.js":
/*!****************************!*\
  !*** ./src/pages/login.js ***!
  \****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-redux */ "react-redux");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "react-router-dom");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_router_dom__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! styled-components */ "styled-components");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _components_header__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./_components/header */ "./src/pages/_components/header.js");
/* harmony import */ var _components_footer__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./_components/footer */ "./src/pages/_components/footer.js");
/* harmony import */ var _store_store__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../store/store */ "./src/store/store.js");
/* harmony import */ var _store_actions_user_actions__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../store/actions/user-actions */ "./src/store/actions/user-actions.js");
/* harmony import */ var _utils_intl_utils__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../utils/intl-utils */ "./src/utils/intl-utils.js");
/* harmony import */ var _utils_request_utils__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../utils/request-utils */ "./src/utils/request-utils.js");
/* harmony import */ var _utils_response_utils__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../utils/response-utils */ "./src/utils/response-utils.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }












const LoginWrapper = styled_components__WEBPACK_IMPORTED_MODULE_3___default.a.main`
  &, * {
    box-sizing: border-box;
  }
`;

class Login extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  constructor(...args) {
    var _temp;

    return _temp = super(...args), this.state = {
      isLoading: false,
      errors: {},
      model: {
        email: '',
        password: ''
      }
    }, this.componentDidUpdate = async () => {
      if (this.props.auth) {
        this.props.history.push('/app/dashboard');
      }
    }, this.login = async e => {
      try {
        if (e) {
          e.preventDefault();
        }

        this.setState({
          isLoading: true,
          errors: {}
        });
        await _store_store__WEBPACK_IMPORTED_MODULE_6__["default"].dispatch(Object(_store_actions_user_actions__WEBPACK_IMPORTED_MODULE_7__["login"])(this.state.model));
        await _store_store__WEBPACK_IMPORTED_MODULE_6__["default"].dispatch(Object(_store_actions_user_actions__WEBPACK_IMPORTED_MODULE_7__["setToken"])(this.props.user.token));
        await _store_store__WEBPACK_IMPORTED_MODULE_6__["default"].dispatch(Object(_store_actions_user_actions__WEBPACK_IMPORTED_MODULE_7__["me"])());
        this.setState({
          isLoading: false
        });
      } catch (e) {
        const {
          errors,
          message
        } = Object(_utils_response_utils__WEBPACK_IMPORTED_MODULE_10__["getFrontendError"])(e);
        this.setState({
          isLoading: false,
          errors: _objectSpread({}, errors, {
            general: message
          })
        });
      }
    }, _temp;
  }

  render() {
    const {
      isLoading
    } = this.state;
    const {
      config,
      user
    } = this.props;
    const queryParams = Object(_utils_request_utils__WEBPACK_IMPORTED_MODULE_9__["getQueryParamsJson"])(this.props.location.search);
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, queryParams.message && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "alert alert-info m-0"
    }, queryParams.message, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
      className: "close",
      "aria-label": "Close",
      onClick: () => this.props.history.push(this.props.location.pathname)
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      "aria-hidden": "true"
    }, "\xD7"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_header__WEBPACK_IMPORTED_MODULE_4__["default"], {
      config: config,
      user: user,
      title: Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_8__["translate"])(config.translations, 'loginTitle'),
      description: Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_8__["translate"])(config.translations, 'loginDescription'),
      history: this.props.history,
      showHeading: true
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(LoginWrapper, {
      className: "container"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "row"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "col-md-12"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "alert alert-info",
      role: "alert"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      className: "mb-0"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
      to: "/register"
    }, Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_8__["translate"])(config.translations, 'haveNotAccount'))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      className: "mb-0"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
      to: "/recover-password"
    }, Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_8__["translate"])(config.translations, 'forgotPassword')))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "alert alert-warning",
      role: "alert"
    }, Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_8__["translate"])(config.translations, 'requiredFields')), this.state.errors.general && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "alert alert-danger",
      role: "alert"
    }, this.state.errors.general), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("form", {
      onSubmit: this.login
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "form-group"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", null, Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_8__["translate"])(config.translations, 'email'), " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "*")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
      type: "text",
      className: "form-control",
      value: this.state.model.email,
      onChange: e => this.setState({
        model: Object.assign(this.state.model, {
          email: (e.target.value || '').replace(/\s/g, '')
        })
      })
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "text-danger"
    }, this.state.errors.email)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "form-group"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", null, Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_8__["translate"])(config.translations, 'password'), " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "*")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
      type: "password",
      className: "form-control",
      value: this.state.model.password,
      onChange: e => this.setState({
        model: Object.assign(this.state.model, {
          password: e.target.value
        })
      })
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "text-danger"
    }, this.state.errors.password)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "form-group text-right"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
      type: "submit",
      className: "btn btn-primary",
      disabled: isLoading
    }, Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_8__["translate"])(config.translations, 'login'))))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_footer__WEBPACK_IMPORTED_MODULE_5__["default"], {
      config: config,
      routerHistory: this.props.history
    }));
  }

}

const mapStateToProps = state => ({
  auth: state.user.auth,
  config: state.config,
  user: state.user
});

/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_1__["connect"])(mapStateToProps)(Login));

/***/ }),

/***/ "./src/pages/main.js":
/*!***************************!*\
  !*** ./src/pages/main.js ***!
  \***************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "react-router-dom");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_router_dom__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _context__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../context */ "./src/context.js");
/* harmony import */ var _store_actions_user_actions__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../store/actions/user-actions */ "./src/store/actions/user-actions.js");
/* harmony import */ var _store_store__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../store/store */ "./src/store/store.js");






class Main extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  constructor(...args) {
    var _temp;

    return _temp = super(...args), this.componentDidMount = () => {
      _store_store__WEBPACK_IMPORTED_MODULE_4__["default"].dispatch(Object(_store_actions_user_actions__WEBPACK_IMPORTED_MODULE_3__["setToken"])(localStorage.token));
      _store_store__WEBPACK_IMPORTED_MODULE_4__["default"].dispatch(Object(_store_actions_user_actions__WEBPACK_IMPORTED_MODULE_3__["me"])()).finally(() => this.context.loadPreferences());
    }, _temp;
  }

  render() {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, this.props.children);
  }

}

Main.contextType = _context__WEBPACK_IMPORTED_MODULE_2__["default"];
/* harmony default export */ __webpack_exports__["default"] = (Object(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["withRouter"])(Main));

/***/ }),

/***/ "./src/pages/not-found.js":
/*!********************************!*\
  !*** ./src/pages/not-found.js ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-redux */ "react-redux");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "react-router-dom");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_router_dom__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! styled-components */ "styled-components");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _components_header__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./_components/header */ "./src/pages/_components/header.js");
/* harmony import */ var _components_footer__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./_components/footer */ "./src/pages/_components/footer.js");
/* harmony import */ var _utils_intl_utils__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../utils/intl-utils */ "./src/utils/intl-utils.js");







const NotFoundWrapper = styled_components__WEBPACK_IMPORTED_MODULE_3___default.a.main`
  &, * {
    box-sizing: border-box;
  }
`;

class NotFound extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  render() {
    const {
      config,
      user
    } = this.props;
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_header__WEBPACK_IMPORTED_MODULE_4__["default"], {
      config: config,
      user: user,
      title: Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_6__["translate"])(config.translations, 'notFoundTitle'),
      description: Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_6__["translate"])(config.translations, 'notFoundDescription'),
      history: this.props.history,
      showHeading: true
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(NotFoundWrapper, {
      className: "container text-center"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
      className: "fas fa-unlink fa-2x"
    })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_6__["translate"])(config.translations, 'notFoundDescription')), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
      to: "/"
    }, Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_6__["translate"])(config.translations, 'goHomePage')))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_footer__WEBPACK_IMPORTED_MODULE_5__["default"], {
      config: config,
      routerHistory: this.props.history
    }));
  }

}

const mapStateToProps = state => ({
  config: state.config,
  user: state.user
});

/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_1__["connect"])(mapStateToProps)(NotFound));

/***/ }),

/***/ "./src/pages/recover-password.js":
/*!***************************************!*\
  !*** ./src/pages/recover-password.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-redux */ "react-redux");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! styled-components */ "styled-components");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _utils_intl_utils__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../utils/intl-utils */ "./src/utils/intl-utils.js");
/* harmony import */ var _utils_response_utils__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../utils/response-utils */ "./src/utils/response-utils.js");
/* harmony import */ var _store_actions_user_actions__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../store/actions/user-actions */ "./src/store/actions/user-actions.js");
/* harmony import */ var _components_header__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./_components/header */ "./src/pages/_components/header.js");
/* harmony import */ var _components_footer__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./_components/footer */ "./src/pages/_components/footer.js");
/* harmony import */ var _store_store__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../store/store */ "./src/store/store.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }










const RecoverPasswordWrapper = styled_components__WEBPACK_IMPORTED_MODULE_2___default.a.main`
  &, * {
    box-sizing: border-box;
  }
`;

class RecoverPassword extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  constructor(...args) {
    var _temp;

    return _temp = super(...args), this.state = {
      errors: {},
      model: {
        email: ''
      }
    }, this.forgotPassword = async e => {
      try {
        e.preventDefault();
        this.setState({
          isLoading: true,
          errors: {}
        });
        await this.setState({
          model: _objectSpread({}, this.state.model, {
            token: this.props.match.params.token
          })
        });
        await _store_store__WEBPACK_IMPORTED_MODULE_8__["default"].dispatch(Object(_store_actions_user_actions__WEBPACK_IMPORTED_MODULE_5__["recoverPassword"])(this.state.model));
        const successMessage = this.props.user.temp.message;
        this.setState({
          isLoading: false
        });
        this.props.history.push(`/login?message=${successMessage}`);
      } catch (e) {
        const {
          errors,
          message
        } = Object(_utils_response_utils__WEBPACK_IMPORTED_MODULE_4__["getFrontendError"])(e);
        this.setState({
          isLoading: false,
          errors: _objectSpread({}, errors, {
            general: message
          })
        });
      }
    }, _temp;
  }

  render() {
    const {
      isLoading
    } = this.state;
    const {
      config,
      user
    } = this.props;
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_header__WEBPACK_IMPORTED_MODULE_6__["default"], {
      config: config,
      user: user,
      title: Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_3__["translate"])(config.translations, 'recoverPasswordTitle'),
      description: Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_3__["translate"])(config.translations, 'recoverPasswordDescription'),
      history: this.props.history,
      showHeading: true
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(RecoverPasswordWrapper, {
      className: "container"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "row"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "col-md-12"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "alert alert-warning",
      role: "alert"
    }, Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_3__["translate"])(config.translations, 'requiredFields')), this.state.errors.general && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "alert alert-danger",
      role: "alert"
    }, this.state.errors.general), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("form", {
      onSubmit: this.forgotPassword
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "form-group"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", null, Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_3__["translate"])(config.translations, 'email'), " *"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
      type: "text",
      className: "form-control",
      onChange: e => this.setState({
        model: Object.assign(this.state.model, {
          email: e.target.value
        })
      })
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "text-danger"
    }, this.state.errors.email)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "form-group text-right"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
      type: "submit",
      className: "btn btn-primary",
      disabled: isLoading
    }, Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_3__["translate"])(config.translations, 'recoverPassword'))))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_footer__WEBPACK_IMPORTED_MODULE_7__["default"], {
      config: config,
      routerHistory: this.props.history
    }));
  }

}

const mapStateToProps = state => ({
  config: state.config,
  user: state.user
});

/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_1__["connect"])(mapStateToProps)(RecoverPassword));

/***/ }),

/***/ "./src/pages/register.js":
/*!*******************************!*\
  !*** ./src/pages/register.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-redux */ "react-redux");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "react-router-dom");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_router_dom__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! styled-components */ "styled-components");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _utils_intl_utils__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../utils/intl-utils */ "./src/utils/intl-utils.js");
/* harmony import */ var _utils_response_utils__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../utils/response-utils */ "./src/utils/response-utils.js");
/* harmony import */ var _components_header__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./_components/header */ "./src/pages/_components/header.js");
/* harmony import */ var _components_footer__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./_components/footer */ "./src/pages/_components/footer.js");
/* harmony import */ var _store_store__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../store/store */ "./src/store/store.js");
/* harmony import */ var _store_actions_user_actions__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../store/actions/user-actions */ "./src/store/actions/user-actions.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }











const RegisterWrapper = styled_components__WEBPACK_IMPORTED_MODULE_3___default.a.main`
  &, * {
    box-sizing: border-box;
  }
`;

class Register extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  constructor(...args) {
    var _temp;

    return _temp = super(...args), this.state = {
      isLoading: false,
      errors: {},
      model: {
        active: true,
        email: '',
        password: '',
        passwordConfirmation: '',
        plan: this.props.config.plans.free
      }
    }, this.componentDidMount = () => {
      if (this.props.user.auth) {
        this.props.history.push('/app/dashboard');
      }
    }, this.registerWithEmail = async e => {
      try {
        if (e) {
          e.preventDefault();
        }

        this.setState({
          isLoading: true,
          errors: {}
        });
        await _store_store__WEBPACK_IMPORTED_MODULE_8__["default"].dispatch(Object(_store_actions_user_actions__WEBPACK_IMPORTED_MODULE_9__["register"])(this.state.model));
        await _store_store__WEBPACK_IMPORTED_MODULE_8__["default"].dispatch(Object(_store_actions_user_actions__WEBPACK_IMPORTED_MODULE_9__["setToken"])(this.props.user.token));
        await _store_store__WEBPACK_IMPORTED_MODULE_8__["default"].dispatch(Object(_store_actions_user_actions__WEBPACK_IMPORTED_MODULE_9__["me"])());
        this.setState({
          isLoading: false
        });
        this.props.history.push('/app/dashboard');
      } catch (e) {
        const {
          errors,
          message
        } = Object(_utils_response_utils__WEBPACK_IMPORTED_MODULE_5__["getFrontendError"])(e);
        this.setState({
          isLoading: false,
          errors: _objectSpread({}, errors, {
            general: message
          })
        });
      }
    }, _temp;
  }

  render() {
    const {
      isLoading
    } = this.state;
    const {
      config,
      user
    } = this.props;
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_header__WEBPACK_IMPORTED_MODULE_6__["default"], {
      config: config,
      user: user,
      title: Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_4__["translate"])(config.translations, 'registerTitle'),
      description: Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_4__["translate"])(config.translations, 'registerDescription'),
      history: this.props.history,
      showHeading: true
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(RegisterWrapper, {
      className: "container"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "row"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "col-md-12"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "alert alert-info"
    }, Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_4__["translate"])(config.translations, 'userAlreadyHasAccount'), ' ', /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
      to: "/login"
    }, Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_4__["translate"])(config.translations, 'here'))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "alert alert-warning",
      role: "alert"
    }, Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_4__["translate"])(config.translations, 'requiredFields')), this.state.errors.general && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "alert alert-danger",
      role: "alert"
    }, this.state.errors.general), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("form", {
      onSubmit: this.registerWithEmail
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "form-group"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", null, Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_4__["translate"])(config.translations, 'email'), " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "*")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
      type: "text",
      className: "form-control",
      value: this.state.model.email,
      onChange: e => this.setState({
        model: Object.assign(this.state.model, {
          email: (e.target.value || '').replace(/\s/g, '')
        })
      })
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "text-danger"
    }, this.state.errors.email)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "form-group"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", null, Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_4__["translate"])(config.translations, 'password'), " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "*")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
      type: "password",
      className: "form-control",
      value: this.state.model.password,
      onChange: e => this.setState({
        model: Object.assign(this.state.model, {
          password: e.target.value
        })
      })
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "text-danger"
    }, this.state.errors.password)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "form-group"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", null, Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_4__["translate"])(config.translations, 'passwordConfirm'), " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "*")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
      type: "password",
      className: "form-control",
      onChange: e => this.setState({
        model: Object.assign(this.state.model, {
          passwordConfirmation: e.target.value
        })
      })
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "text-danger"
    }, this.state.errors.passwordConfirmation)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "form-group text-right"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
      type: "submit",
      disabled: isLoading,
      className: "btn btn-primary"
    }, Object(_utils_intl_utils__WEBPACK_IMPORTED_MODULE_4__["translate"])(config.translations, 'register'))))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_footer__WEBPACK_IMPORTED_MODULE_7__["default"], {
      config: config,
      routerHistory: this.props.history
    }));
  }

}

const mapStateToProps = state => ({
  config: state.config,
  user: state.user
});

/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_1__["connect"])(mapStateToProps)(Register));

/***/ }),

/***/ "./src/routes-ssr.js":
/*!***************************!*\
  !*** ./src/routes-ssr.js ***!
  \***************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _pages_home__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./pages/home */ "./src/pages/home.js");
/* harmony import */ var _pages_login__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./pages/login */ "./src/pages/login.js");
/* harmony import */ var _pages_recover_password__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./pages/recover-password */ "./src/pages/recover-password.js");
/* harmony import */ var _pages_register__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./pages/register */ "./src/pages/register.js");
/* harmony import */ var _pages_not_found__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./pages/not-found */ "./src/pages/not-found.js");





/* harmony default export */ __webpack_exports__["default"] = ([{
  exact: true,
  path: '/',
  component: _pages_home__WEBPACK_IMPORTED_MODULE_0__["default"]
}, {
  path: '/login',
  component: _pages_login__WEBPACK_IMPORTED_MODULE_1__["default"]
}, {
  path: '/login/:provider?/:token',
  component: _pages_login__WEBPACK_IMPORTED_MODULE_1__["default"]
}, {
  path: '/register',
  component: _pages_register__WEBPACK_IMPORTED_MODULE_3__["default"]
}, {
  path: '/recover-password',
  component: _pages_recover_password__WEBPACK_IMPORTED_MODULE_2__["default"]
}, {
  component: _pages_not_found__WEBPACK_IMPORTED_MODULE_4__["default"]
}]);

/***/ }),

/***/ "./src/server.js":
/*!***********************!*\
  !*** ./src/server.js ***!
  \***********************/
/*! exports provided: onNewUser, updateConfiguration, app */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "app", function() { return app; });
/* harmony import */ var firebase_functions__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! firebase-functions */ "firebase-functions");
/* harmony import */ var firebase_functions__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(firebase_functions__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! express */ "express");
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var fs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! fs */ "fs");
/* harmony import */ var fs__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(fs__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-helmet */ "react-helmet");
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_helmet__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var serialize_javascript__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! serialize-javascript */ "serialize-javascript");
/* harmony import */ var serialize_javascript__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(serialize_javascript__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-redux */ "react-redux");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react-router-dom */ "react-router-dom");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react_router_dom__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var react_dom_server__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react-dom/server */ "react-dom/server");
/* harmony import */ var react_dom_server__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(react_dom_server__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! styled-components */ "styled-components");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _static_configuration_json__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./static/configuration.json */ "./src/static/configuration.json");
var _static_configuration_json__WEBPACK_IMPORTED_MODULE_10___namespace = /*#__PURE__*/__webpack_require__.t(/*! ./static/configuration.json */ "./src/static/configuration.json", 1);
/* harmony import */ var _pages_main__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./pages/main */ "./src/pages/main.js");
/* harmony import */ var _store_store__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./store/store */ "./src/store/store.js");
/* harmony import */ var _store_actions_config_actions__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./store/actions/config-actions */ "./src/store/actions/config-actions.js");
/* harmony import */ var _utils_request_utils__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./utils/request-utils */ "./src/utils/request-utils.js");
/* harmony import */ var _utils_response_utils__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./utils/response-utils */ "./src/utils/response-utils.js");
/* harmony import */ var _routes_ssr__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./routes-ssr */ "./src/routes-ssr.js");
/* harmony import */ var _apis_routes_catalog__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./apis/routes/catalog */ "./src/apis/routes/catalog.js");
/* harmony import */ var _apis_routes_config__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./apis/routes/config */ "./src/apis/routes/config.js");
/* harmony import */ var _apis_routes_locale__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./apis/routes/locale */ "./src/apis/routes/locale.js");
/* harmony import */ var _apis_routes_user__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./apis/routes/user */ "./src/apis/routes/user.js");
/* harmony import */ var _apis_routes_socialLeader__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./apis/routes/socialLeader */ "./src/apis/routes/socialLeader.js");
/* harmony import */ var _apis_events__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./apis/events */ "./src/apis/events.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "onNewUser", function() { return _apis_events__WEBPACK_IMPORTED_MODULE_22__["onNewUser"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "updateConfiguration", function() { return _apis_events__WEBPACK_IMPORTED_MODULE_22__["updateConfiguration"]; });

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

// libraries









 // utilities







 // routes





 // helpers

_store_store__WEBPACK_IMPORTED_MODULE_12__["default"].dispatch(Object(_store_actions_config_actions__WEBPACK_IMPORTED_MODULE_13__["setConfiguration"])(_static_configuration_json__WEBPACK_IMPORTED_MODULE_10__));
const indexHtml = fs__WEBPACK_IMPORTED_MODULE_2___default.a.readFileSync('./src/index.html', 'utf8');
const maintenanceHtml = fs__WEBPACK_IMPORTED_MODULE_2___default.a.readFileSync('./src/static/maintenance.html', 'utf8');
const appServer = express__WEBPACK_IMPORTED_MODULE_1___default()();

const cors = __webpack_require__(/*! cors */ "cors")({
  origin: true
}); // server configuration


appServer.use(cors);
appServer.use((req, res, next) => {
  try {
    const acceptLanguage = (req.headers['accept-language'] || '').toLowerCase();
    req.config = _store_store__WEBPACK_IMPORTED_MODULE_12__["default"].getState().config;
    req.currency = req.headers['accept-currency'] || 'usd';
    req.dateFormat = req.headers['accept-date-format'] || 'dd/mm/yyyy';
    req.language = req.config.appLanguages.includes(acceptLanguage) ? acceptLanguage : 'en';
    req.translations = req.config.intlData && req.config.intlData.locales[req.language] || {};
    req.query = Object(_utils_request_utils__WEBPACK_IMPORTED_MODULE_14__["getQueryParamsJson"])(req.query);

    if (req.config.appDisabled) {
      res.send(maintenanceHtml);
      return;
    }

    next();
  } catch (e) {
    const error = Object(_utils_response_utils__WEBPACK_IMPORTED_MODULE_15__["getBackendError"])(e);
    res.status(500).json(error);
  }
}); // server routes

appServer.use('/api/catalogs', _apis_routes_catalog__WEBPACK_IMPORTED_MODULE_17__["default"]);
appServer.use('/api/config', _apis_routes_config__WEBPACK_IMPORTED_MODULE_18__["default"]);
appServer.use('/api/locales', _apis_routes_locale__WEBPACK_IMPORTED_MODULE_19__["default"]);
appServer.use('/api/users', _apis_routes_user__WEBPACK_IMPORTED_MODULE_20__["default"]);
appServer.use('/api/socialLeader', _apis_routes_socialLeader__WEBPACK_IMPORTED_MODULE_21__["default"]);
appServer.get('/*', async (req, res) => {
  try {
    const currentRoute = _routes_ssr__WEBPACK_IMPORTED_MODULE_16__["default"].find(route => Object(react_router_dom__WEBPACK_IMPORTED_MODULE_7__["matchPath"])(req.url, route));
    await _store_store__WEBPACK_IMPORTED_MODULE_12__["default"].dispatch(Object(_store_actions_config_actions__WEBPACK_IMPORTED_MODULE_13__["setPreferences"])({
      currency: req.currency,
      dateFormat: req.dateFormat,
      language: req.language
    }));

    if (currentRoute && currentRoute.component.getInitialState) {
      await currentRoute.component.getInitialState();
    }

    const initialState = _store_store__WEBPACK_IMPORTED_MODULE_12__["default"].getState();
    const styles = new styled_components__WEBPACK_IMPORTED_MODULE_9__["ServerStyleSheet"]();
    const reactDom = Object(react_dom_server__WEBPACK_IMPORTED_MODULE_8__["renderToString"])( /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(react_redux__WEBPACK_IMPORTED_MODULE_6__["Provider"], {
      store: _store_store__WEBPACK_IMPORTED_MODULE_12__["default"]
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_7__["StaticRouter"], {
      location: req.url
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(styled_components__WEBPACK_IMPORTED_MODULE_9__["StyleSheetManager"], {
      sheet: styles.instance
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(_pages_main__WEBPACK_IMPORTED_MODULE_11__["default"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_7__["Switch"], null, _routes_ssr__WEBPACK_IMPORTED_MODULE_16__["default"].map((route, index) => /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_7__["Route"], _extends({
      key: index
    }, route)))))))));
    const helmet = react_helmet__WEBPACK_IMPORTED_MODULE_3__["Helmet"].renderStatic();
    const styleTags = styles.getStyleTags();
    let response = indexHtml;
    response = response.replace(/{htmlAttributes}/g, helmet.htmlAttributes.toString());
    response = response.replace(/{bodyAttributes}/g, helmet.bodyAttributes.toString());
    response = response.replace(/<!--title-->/g, helmet.title.toString());
    response = response.replace(/<!--metaTags-->/g, helmet.meta.toString());
    response = response.replace(/<!--links-->/g, helmet.link.toString());
    response = response.replace(/<!--styles-->/g, styleTags);
    response = response.replace(/<!--content-->/g, reactDom);
    response = response.replace(/<!--scripts-->/g, `<script>window.initialState=${serialize_javascript__WEBPACK_IMPORTED_MODULE_4___default()(initialState)}</script>`);
    res.send(response);
  } catch (e) {
    const error = Object(_utils_response_utils__WEBPACK_IMPORTED_MODULE_15__["getBackendError"])(e);
    res.status(500).json(error);
  }
}); // exporting services
// export * from './apis/cronos';


const app = firebase_functions__WEBPACK_IMPORTED_MODULE_0__["https"].onRequest(appServer);

/***/ }),

/***/ "./src/static/configuration.json":
/*!***************************************!*\
  !*** ./src/static/configuration.json ***!
  \***************************************/
/*! exports provided: appDisabled, appDisabledMessage, appLastSync, intlData, default */
/***/ (function(module) {

module.exports = JSON.parse("{\"appDisabled\":false,\"appDisabledMessage\":\"wwwwww\",\"appLastSync\":1585697898625,\"intlData\":{\"callingCodes\":[{\"label\":\"+1\",\"value\":\"+1\"},{\"label\":\"+1 340\",\"value\":\"+1 340\"},{\"label\":\"+1242\",\"value\":\"+1242\"},{\"label\":\"+1246\",\"value\":\"+1246\"},{\"label\":\"+1264\",\"value\":\"+1264\"},{\"label\":\"+1268\",\"value\":\"+1268\"},{\"label\":\"+1284\",\"value\":\"+1284\"},{\"label\":\"+1345\",\"value\":\"+1345\"},{\"label\":\"+1441\",\"value\":\"+1441\"},{\"label\":\"+1473\",\"value\":\"+1473\"},{\"label\":\"+1649\",\"value\":\"+1649\"},{\"label\":\"+1664\",\"value\":\"+1664\"},{\"label\":\"+1670\",\"value\":\"+1670\"},{\"label\":\"+1671\",\"value\":\"+1671\"},{\"label\":\"+1684\",\"value\":\"+1684\"},{\"label\":\"+1721\",\"value\":\"+1721\"},{\"label\":\"+1758\",\"value\":\"+1758\"},{\"label\":\"+1767\",\"value\":\"+1767\"},{\"label\":\"+1784\",\"value\":\"+1784\"},{\"label\":\"+1787\",\"value\":\"+1787\"},{\"label\":\"+1809\",\"value\":\"+1809\"},{\"label\":\"+1829\",\"value\":\"+1829\"},{\"label\":\"+1849\",\"value\":\"+1849\"},{\"label\":\"+1868\",\"value\":\"+1868\"},{\"label\":\"+1869\",\"value\":\"+1869\"},{\"label\":\"+1876\",\"value\":\"+1876\"},{\"label\":\"+1939\",\"value\":\"+1939\"},{\"label\":\"+20\",\"value\":\"+20\"},{\"label\":\"+211\",\"value\":\"+211\"},{\"label\":\"+212\",\"value\":\"+212\"},{\"label\":\"+213\",\"value\":\"+213\"},{\"label\":\"+216\",\"value\":\"+216\"},{\"label\":\"+218\",\"value\":\"+218\"},{\"label\":\"+220\",\"value\":\"+220\"},{\"label\":\"+221\",\"value\":\"+221\"},{\"label\":\"+222\",\"value\":\"+222\"},{\"label\":\"+223\",\"value\":\"+223\"},{\"label\":\"+224\",\"value\":\"+224\"},{\"label\":\"+225\",\"value\":\"+225\"},{\"label\":\"+226\",\"value\":\"+226\"},{\"label\":\"+227\",\"value\":\"+227\"},{\"label\":\"+228\",\"value\":\"+228\"},{\"label\":\"+229\",\"value\":\"+229\"},{\"label\":\"+230\",\"value\":\"+230\"},{\"label\":\"+231\",\"value\":\"+231\"},{\"label\":\"+232\",\"value\":\"+232\"},{\"label\":\"+233\",\"value\":\"+233\"},{\"label\":\"+234\",\"value\":\"+234\"},{\"label\":\"+235\",\"value\":\"+235\"},{\"label\":\"+236\",\"value\":\"+236\"},{\"label\":\"+237\",\"value\":\"+237\"},{\"label\":\"+238\",\"value\":\"+238\"},{\"label\":\"+239\",\"value\":\"+239\"},{\"label\":\"+240\",\"value\":\"+240\"},{\"label\":\"+241\",\"value\":\"+241\"},{\"label\":\"+242\",\"value\":\"+242\"},{\"label\":\"+243\",\"value\":\"+243\"},{\"label\":\"+244\",\"value\":\"+244\"},{\"label\":\"+245\",\"value\":\"+245\"},{\"label\":\"+246\",\"value\":\"+246\"},{\"label\":\"+248\",\"value\":\"+248\"},{\"label\":\"+249\",\"value\":\"+249\"},{\"label\":\"+250\",\"value\":\"+250\"},{\"label\":\"+251\",\"value\":\"+251\"},{\"label\":\"+252\",\"value\":\"+252\"},{\"label\":\"+253\",\"value\":\"+253\"},{\"label\":\"+254\",\"value\":\"+254\"},{\"label\":\"+255\",\"value\":\"+255\"},{\"label\":\"+256\",\"value\":\"+256\"},{\"label\":\"+257\",\"value\":\"+257\"},{\"label\":\"+258\",\"value\":\"+258\"},{\"label\":\"+260\",\"value\":\"+260\"},{\"label\":\"+261\",\"value\":\"+261\"},{\"label\":\"+262\",\"value\":\"+262\"},{\"label\":\"+263\",\"value\":\"+263\"},{\"label\":\"+264\",\"value\":\"+264\"},{\"label\":\"+265\",\"value\":\"+265\"},{\"label\":\"+266\",\"value\":\"+266\"},{\"label\":\"+267\",\"value\":\"+267\"},{\"label\":\"+268\",\"value\":\"+268\"},{\"label\":\"+269\",\"value\":\"+269\"},{\"label\":\"+27\",\"value\":\"+27\"},{\"label\":\"+290\",\"value\":\"+290\"},{\"label\":\"+291\",\"value\":\"+291\"},{\"label\":\"+297\",\"value\":\"+297\"},{\"label\":\"+298\",\"value\":\"+298\"},{\"label\":\"+299\",\"value\":\"+299\"},{\"label\":\"+30\",\"value\":\"+30\"},{\"label\":\"+31\",\"value\":\"+31\"},{\"label\":\"+32\",\"value\":\"+32\"},{\"label\":\"+33\",\"value\":\"+33\"},{\"label\":\"+34\",\"value\":\"+34\"},{\"label\":\"+350\",\"value\":\"+350\"},{\"label\":\"+351\",\"value\":\"+351\"},{\"label\":\"+352\",\"value\":\"+352\"},{\"label\":\"+353\",\"value\":\"+353\"},{\"label\":\"+354\",\"value\":\"+354\"},{\"label\":\"+355\",\"value\":\"+355\"},{\"label\":\"+356\",\"value\":\"+356\"},{\"label\":\"+357\",\"value\":\"+357\"},{\"label\":\"+358\",\"value\":\"+358\"},{\"label\":\"+359\",\"value\":\"+359\"},{\"label\":\"+36\",\"value\":\"+36\"},{\"label\":\"+370\",\"value\":\"+370\"},{\"label\":\"+371\",\"value\":\"+371\"},{\"label\":\"+372\",\"value\":\"+372\"},{\"label\":\"+373\",\"value\":\"+373\"},{\"label\":\"+374\",\"value\":\"+374\"},{\"label\":\"+375\",\"value\":\"+375\"},{\"label\":\"+376\",\"value\":\"+376\"},{\"label\":\"+377\",\"value\":\"+377\"},{\"label\":\"+378\",\"value\":\"+378\"},{\"label\":\"+379\",\"value\":\"+379\"},{\"label\":\"+380\",\"value\":\"+380\"},{\"label\":\"+381\",\"value\":\"+381\"},{\"label\":\"+382\",\"value\":\"+382\"},{\"label\":\"+383\",\"value\":\"+383\"},{\"label\":\"+385\",\"value\":\"+385\"},{\"label\":\"+386\",\"value\":\"+386\"},{\"label\":\"+387\",\"value\":\"+387\"},{\"label\":\"+389\",\"value\":\"+389\"},{\"label\":\"+39\",\"value\":\"+39\"},{\"label\":\"+40\",\"value\":\"+40\"},{\"label\":\"+41\",\"value\":\"+41\"},{\"label\":\"+420\",\"value\":\"+420\"},{\"label\":\"+421\",\"value\":\"+421\"},{\"label\":\"+423\",\"value\":\"+423\"},{\"label\":\"+43\",\"value\":\"+43\"},{\"label\":\"+44\",\"value\":\"+44\"},{\"label\":\"+45\",\"value\":\"+45\"},{\"label\":\"+46\",\"value\":\"+46\"},{\"label\":\"+47\",\"value\":\"+47\"},{\"label\":\"+4779\",\"value\":\"+4779\"},{\"label\":\"+48\",\"value\":\"+48\"},{\"label\":\"+49\",\"value\":\"+49\"},{\"label\":\"+500\",\"value\":\"+500\"},{\"label\":\"+501\",\"value\":\"+501\"},{\"label\":\"+502\",\"value\":\"+502\"},{\"label\":\"+503\",\"value\":\"+503\"},{\"label\":\"+504\",\"value\":\"+504\"},{\"label\":\"+505\",\"value\":\"+505\"},{\"label\":\"+506\",\"value\":\"+506\"},{\"label\":\"+507\",\"value\":\"+507\"},{\"label\":\"+508\",\"value\":\"+508\"},{\"label\":\"+509\",\"value\":\"+509\"},{\"label\":\"+51\",\"value\":\"+51\"},{\"label\":\"+52\",\"value\":\"+52\"},{\"label\":\"+53\",\"value\":\"+53\"},{\"label\":\"+54\",\"value\":\"+54\"},{\"label\":\"+55\",\"value\":\"+55\"},{\"label\":\"+56\",\"value\":\"+56\"},{\"label\":\"+57\",\"value\":\"+57\"},{\"label\":\"+58\",\"value\":\"+58\"},{\"label\":\"+590\",\"value\":\"+590\"},{\"label\":\"+591\",\"value\":\"+591\"},{\"label\":\"+592\",\"value\":\"+592\"},{\"label\":\"+593\",\"value\":\"+593\"},{\"label\":\"+594\",\"value\":\"+594\"},{\"label\":\"+595\",\"value\":\"+595\"},{\"label\":\"+596\",\"value\":\"+596\"},{\"label\":\"+597\",\"value\":\"+597\"},{\"label\":\"+598\",\"value\":\"+598\"},{\"label\":\"+599\",\"value\":\"+599\"},{\"label\":\"+5997\",\"value\":\"+5997\"},{\"label\":\"+60\",\"value\":\"+60\"},{\"label\":\"+61\",\"value\":\"+61\"},{\"label\":\"+62\",\"value\":\"+62\"},{\"label\":\"+63\",\"value\":\"+63\"},{\"label\":\"+64\",\"value\":\"+64\"},{\"label\":\"+65\",\"value\":\"+65\"},{\"label\":\"+66\",\"value\":\"+66\"},{\"label\":\"+670\",\"value\":\"+670\"},{\"label\":\"+672\",\"value\":\"+672\"},{\"label\":\"+673\",\"value\":\"+673\"},{\"label\":\"+674\",\"value\":\"+674\"},{\"label\":\"+675\",\"value\":\"+675\"},{\"label\":\"+676\",\"value\":\"+676\"},{\"label\":\"+677\",\"value\":\"+677\"},{\"label\":\"+678\",\"value\":\"+678\"},{\"label\":\"+679\",\"value\":\"+679\"},{\"label\":\"+680\",\"value\":\"+680\"},{\"label\":\"+681\",\"value\":\"+681\"},{\"label\":\"+682\",\"value\":\"+682\"},{\"label\":\"+683\",\"value\":\"+683\"},{\"label\":\"+685\",\"value\":\"+685\"},{\"label\":\"+686\",\"value\":\"+686\"},{\"label\":\"+687\",\"value\":\"+687\"},{\"label\":\"+688\",\"value\":\"+688\"},{\"label\":\"+689\",\"value\":\"+689\"},{\"label\":\"+690\",\"value\":\"+690\"},{\"label\":\"+691\",\"value\":\"+691\"},{\"label\":\"+692\",\"value\":\"+692\"},{\"label\":\"+7\",\"value\":\"+7\"},{\"label\":\"+76\",\"value\":\"+76\"},{\"label\":\"+77\",\"value\":\"+77\"},{\"label\":\"+81\",\"value\":\"+81\"},{\"label\":\"+82\",\"value\":\"+82\"},{\"label\":\"+84\",\"value\":\"+84\"},{\"label\":\"+850\",\"value\":\"+850\"},{\"label\":\"+852\",\"value\":\"+852\"},{\"label\":\"+853\",\"value\":\"+853\"},{\"label\":\"+855\",\"value\":\"+855\"},{\"label\":\"+856\",\"value\":\"+856\"},{\"label\":\"+86\",\"value\":\"+86\"},{\"label\":\"+880\",\"value\":\"+880\"},{\"label\":\"+886\",\"value\":\"+886\"},{\"label\":\"+90\",\"value\":\"+90\"},{\"label\":\"+91\",\"value\":\"+91\"},{\"label\":\"+92\",\"value\":\"+92\"},{\"label\":\"+93\",\"value\":\"+93\"},{\"label\":\"+94\",\"value\":\"+94\"},{\"label\":\"+95\",\"value\":\"+95\"},{\"label\":\"+960\",\"value\":\"+960\"},{\"label\":\"+961\",\"value\":\"+961\"},{\"label\":\"+962\",\"value\":\"+962\"},{\"label\":\"+963\",\"value\":\"+963\"},{\"label\":\"+964\",\"value\":\"+964\"},{\"label\":\"+965\",\"value\":\"+965\"},{\"label\":\"+966\",\"value\":\"+966\"},{\"label\":\"+967\",\"value\":\"+967\"},{\"label\":\"+968\",\"value\":\"+968\"},{\"label\":\"+970\",\"value\":\"+970\"},{\"label\":\"+971\",\"value\":\"+971\"},{\"label\":\"+972\",\"value\":\"+972\"},{\"label\":\"+973\",\"value\":\"+973\"},{\"label\":\"+974\",\"value\":\"+974\"},{\"label\":\"+975\",\"value\":\"+975\"},{\"label\":\"+976\",\"value\":\"+976\"},{\"label\":\"+977\",\"value\":\"+977\"},{\"label\":\"+98\",\"value\":\"+98\"},{\"label\":\"+992\",\"value\":\"+992\"},{\"label\":\"+993\",\"value\":\"+993\"},{\"label\":\"+994\",\"value\":\"+994\"},{\"label\":\"+995\",\"value\":\"+995\"},{\"label\":\"+996\",\"value\":\"+996\"},{\"label\":\"+998\",\"value\":\"+998\"}],\"countries\":[{\"label\":\"Afghanistan\",\"value\":\"afg\"},{\"label\":\"Åland Islands\",\"value\":\"ala\"},{\"label\":\"Albania\",\"value\":\"alb\"},{\"label\":\"Algeria\",\"value\":\"dza\"},{\"label\":\"American Samoa\",\"value\":\"asm\"},{\"label\":\"Andorra\",\"value\":\"and\"},{\"label\":\"Angola\",\"value\":\"ago\"},{\"label\":\"Anguilla\",\"value\":\"aia\"},{\"label\":\"Antarctica\",\"value\":\"ata\"},{\"label\":\"Antigua and Barbuda\",\"value\":\"atg\"},{\"label\":\"Argentina\",\"value\":\"arg\"},{\"label\":\"Armenia\",\"value\":\"arm\"},{\"label\":\"Aruba\",\"value\":\"abw\"},{\"label\":\"Australia\",\"value\":\"aus\"},{\"label\":\"Austria\",\"value\":\"aut\"},{\"label\":\"Azerbaijan\",\"value\":\"aze\"},{\"label\":\"Bahamas\",\"value\":\"bhs\"},{\"label\":\"Bahrain\",\"value\":\"bhr\"},{\"label\":\"Bangladesh\",\"value\":\"bgd\"},{\"label\":\"Barbados\",\"value\":\"brb\"},{\"label\":\"Belarus\",\"value\":\"blr\"},{\"label\":\"Belgium\",\"value\":\"bel\"},{\"label\":\"Belize\",\"value\":\"blz\"},{\"label\":\"Benin\",\"value\":\"ben\"},{\"label\":\"Bermuda\",\"value\":\"bmu\"},{\"label\":\"Bhutan\",\"value\":\"btn\"},{\"label\":\"Bolivia (Plurinational State of)\",\"value\":\"bol\"},{\"label\":\"Bonaire, Sint Eustatius and Saba\",\"value\":\"bes\"},{\"label\":\"Bosnia and Herzegovina\",\"value\":\"bih\"},{\"label\":\"Botswana\",\"value\":\"bwa\"},{\"label\":\"Bouvet Island\",\"value\":\"bvt\"},{\"label\":\"Brazil\",\"value\":\"bra\"},{\"label\":\"British Indian Ocean Territory\",\"value\":\"iot\"},{\"label\":\"Brunei Darussalam\",\"value\":\"brn\"},{\"label\":\"Bulgaria\",\"value\":\"bgr\"},{\"label\":\"Burkina Faso\",\"value\":\"bfa\"},{\"label\":\"Burundi\",\"value\":\"bdi\"},{\"label\":\"Cabo Verde\",\"value\":\"cpv\"},{\"label\":\"Cambodia\",\"value\":\"khm\"},{\"label\":\"Cameroon\",\"value\":\"cmr\"},{\"label\":\"Canada\",\"value\":\"can\"},{\"label\":\"Cayman Islands\",\"value\":\"cym\"},{\"label\":\"Central African Republic\",\"value\":\"caf\"},{\"label\":\"Chad\",\"value\":\"tcd\"},{\"label\":\"Chile\",\"value\":\"chl\"},{\"label\":\"China\",\"value\":\"chn\"},{\"label\":\"Christmas Island\",\"value\":\"cxr\"},{\"label\":\"Cocos (Keeling) Islands\",\"value\":\"cck\"},{\"label\":\"Colombia\",\"value\":\"col\"},{\"label\":\"Comoros\",\"value\":\"com\"},{\"label\":\"Congo\",\"value\":\"cog\"},{\"label\":\"Congo (Democratic Republic of the)\",\"value\":\"cod\"},{\"label\":\"Cook Islands\",\"value\":\"cok\"},{\"label\":\"Costa Rica\",\"value\":\"cri\"},{\"label\":\"Côte d'Ivoire\",\"value\":\"civ\"},{\"label\":\"Croatia\",\"value\":\"hrv\"},{\"label\":\"Cuba\",\"value\":\"cub\"},{\"label\":\"Curaçao\",\"value\":\"cuw\"},{\"label\":\"Cyprus\",\"value\":\"cyp\"},{\"label\":\"Czech Republic\",\"value\":\"cze\"},{\"label\":\"Denmark\",\"value\":\"dnk\"},{\"label\":\"Djibouti\",\"value\":\"dji\"},{\"label\":\"Dominica\",\"value\":\"dma\"},{\"label\":\"Dominican Republic\",\"value\":\"dom\"},{\"label\":\"Ecuador\",\"value\":\"ecu\"},{\"label\":\"Egypt\",\"value\":\"egy\"},{\"label\":\"El Salvador\",\"value\":\"slv\"},{\"label\":\"Equatorial Guinea\",\"value\":\"gnq\"},{\"label\":\"Eritrea\",\"value\":\"eri\"},{\"label\":\"Estonia\",\"value\":\"est\"},{\"label\":\"Ethiopia\",\"value\":\"eth\"},{\"label\":\"Falkland Islands (Malvinas)\",\"value\":\"flk\"},{\"label\":\"Faroe Islands\",\"value\":\"fro\"},{\"label\":\"Fiji\",\"value\":\"fji\"},{\"label\":\"Finland\",\"value\":\"fin\"},{\"label\":\"France\",\"value\":\"fra\"},{\"label\":\"French Guiana\",\"value\":\"guf\"},{\"label\":\"French Polynesia\",\"value\":\"pyf\"},{\"label\":\"French Southern Territories\",\"value\":\"atf\"},{\"label\":\"Gabon\",\"value\":\"gab\"},{\"label\":\"Gambia\",\"value\":\"gmb\"},{\"label\":\"Georgia\",\"value\":\"geo\"},{\"label\":\"Germany\",\"value\":\"deu\"},{\"label\":\"Ghana\",\"value\":\"gha\"},{\"label\":\"Gibraltar\",\"value\":\"gib\"},{\"label\":\"Greece\",\"value\":\"grc\"},{\"label\":\"Greenland\",\"value\":\"grl\"},{\"label\":\"Grenada\",\"value\":\"grd\"},{\"label\":\"Guadeloupe\",\"value\":\"glp\"},{\"label\":\"Guam\",\"value\":\"gum\"},{\"label\":\"Guatemala\",\"value\":\"gtm\"},{\"label\":\"Guernsey\",\"value\":\"ggy\"},{\"label\":\"Guinea\",\"value\":\"gin\"},{\"label\":\"Guinea-Bissau\",\"value\":\"gnb\"},{\"label\":\"Guyana\",\"value\":\"guy\"},{\"label\":\"Haiti\",\"value\":\"hti\"},{\"label\":\"Heard Island and McDonald Islands\",\"value\":\"hmd\"},{\"label\":\"Holy See\",\"value\":\"vat\"},{\"label\":\"Honduras\",\"value\":\"hnd\"},{\"label\":\"Hong Kong\",\"value\":\"hkg\"},{\"label\":\"Hungary\",\"value\":\"hun\"},{\"label\":\"Iceland\",\"value\":\"isl\"},{\"label\":\"India\",\"value\":\"ind\"},{\"label\":\"Indonesia\",\"value\":\"idn\"},{\"label\":\"Iran (Islamic Republic of)\",\"value\":\"irn\"},{\"label\":\"Iraq\",\"value\":\"irq\"},{\"label\":\"Ireland\",\"value\":\"irl\"},{\"label\":\"Isle of Man\",\"value\":\"imn\"},{\"label\":\"Israel\",\"value\":\"isr\"},{\"label\":\"Italy\",\"value\":\"ita\"},{\"label\":\"Jamaica\",\"value\":\"jam\"},{\"label\":\"Japan\",\"value\":\"jpn\"},{\"label\":\"Jersey\",\"value\":\"jey\"},{\"label\":\"Jordan\",\"value\":\"jor\"},{\"label\":\"Kazakhstan\",\"value\":\"kaz\"},{\"label\":\"Kenya\",\"value\":\"ken\"},{\"label\":\"Kiribati\",\"value\":\"kir\"},{\"label\":\"Korea (Democratic People's Republic of)\",\"value\":\"prk\"},{\"label\":\"Korea (Republic of)\",\"value\":\"kor\"},{\"label\":\"Kuwait\",\"value\":\"kwt\"},{\"label\":\"Kyrgyzstan\",\"value\":\"kgz\"},{\"label\":\"Lao People's Democratic Republic\",\"value\":\"lao\"},{\"label\":\"Latvia\",\"value\":\"lva\"},{\"label\":\"Lebanon\",\"value\":\"lbn\"},{\"label\":\"Lesotho\",\"value\":\"lso\"},{\"label\":\"Liberia\",\"value\":\"lbr\"},{\"label\":\"Libya\",\"value\":\"lby\"},{\"label\":\"Liechtenstein\",\"value\":\"lie\"},{\"label\":\"Lithuania\",\"value\":\"ltu\"},{\"label\":\"Luxembourg\",\"value\":\"lux\"},{\"label\":\"Macao\",\"value\":\"mac\"},{\"label\":\"Macedonia (the former Yugoslav Republic of)\",\"value\":\"mkd\"},{\"label\":\"Madagascar\",\"value\":\"mdg\"},{\"label\":\"Malawi\",\"value\":\"mwi\"},{\"label\":\"Malaysia\",\"value\":\"mys\"},{\"label\":\"Maldives\",\"value\":\"mdv\"},{\"label\":\"Mali\",\"value\":\"mli\"},{\"label\":\"Malta\",\"value\":\"mlt\"},{\"label\":\"Marshall Islands\",\"value\":\"mhl\"},{\"label\":\"Martinique\",\"value\":\"mtq\"},{\"label\":\"Mauritania\",\"value\":\"mrt\"},{\"label\":\"Mauritius\",\"value\":\"mus\"},{\"label\":\"Mayotte\",\"value\":\"myt\"},{\"label\":\"Mexico\",\"value\":\"mex\"},{\"label\":\"Micronesia (Federated States of)\",\"value\":\"fsm\"},{\"label\":\"Moldova (Republic of)\",\"value\":\"mda\"},{\"label\":\"Monaco\",\"value\":\"mco\"},{\"label\":\"Mongolia\",\"value\":\"mng\"},{\"label\":\"Montenegro\",\"value\":\"mne\"},{\"label\":\"Montserrat\",\"value\":\"msr\"},{\"label\":\"Morocco\",\"value\":\"mar\"},{\"label\":\"Mozambique\",\"value\":\"moz\"},{\"label\":\"Myanmar\",\"value\":\"mmr\"},{\"label\":\"Namibia\",\"value\":\"nam\"},{\"label\":\"Nauru\",\"value\":\"nru\"},{\"label\":\"Nepal\",\"value\":\"npl\"},{\"label\":\"Netherlands\",\"value\":\"nld\"},{\"label\":\"New Caledonia\",\"value\":\"ncl\"},{\"label\":\"New Zealand\",\"value\":\"nzl\"},{\"label\":\"Nicaragua\",\"value\":\"nic\"},{\"label\":\"Niger\",\"value\":\"ner\"},{\"label\":\"Nigeria\",\"value\":\"nga\"},{\"label\":\"Niue\",\"value\":\"niu\"},{\"label\":\"Norfolk Island\",\"value\":\"nfk\"},{\"label\":\"Northern Mariana Islands\",\"value\":\"mnp\"},{\"label\":\"Norway\",\"value\":\"nor\"},{\"label\":\"Oman\",\"value\":\"omn\"},{\"label\":\"Pakistan\",\"value\":\"pak\"},{\"label\":\"Palau\",\"value\":\"plw\"},{\"label\":\"Palestine, State of\",\"value\":\"pse\"},{\"label\":\"Panama\",\"value\":\"pan\"},{\"label\":\"Papua New Guinea\",\"value\":\"png\"},{\"label\":\"Paraguay\",\"value\":\"pry\"},{\"label\":\"Peru\",\"value\":\"per\"},{\"label\":\"Philippines\",\"value\":\"phl\"},{\"label\":\"Pitcairn\",\"value\":\"pcn\"},{\"label\":\"Poland\",\"value\":\"pol\"},{\"label\":\"Portugal\",\"value\":\"prt\"},{\"label\":\"Puerto Rico\",\"value\":\"pri\"},{\"label\":\"Qatar\",\"value\":\"qat\"},{\"label\":\"Republic of Kosovo\",\"value\":\"kos\"},{\"label\":\"Réunion\",\"value\":\"reu\"},{\"label\":\"Romania\",\"value\":\"rou\"},{\"label\":\"Russian Federation\",\"value\":\"rus\"},{\"label\":\"Rwanda\",\"value\":\"rwa\"},{\"label\":\"Saint Barthélemy\",\"value\":\"blm\"},{\"label\":\"Saint Helena, Ascension and Tristan da Cunha\",\"value\":\"shn\"},{\"label\":\"Saint Kitts and Nevis\",\"value\":\"kna\"},{\"label\":\"Saint Lucia\",\"value\":\"lca\"},{\"label\":\"Saint Martin (French part)\",\"value\":\"maf\"},{\"label\":\"Saint Pierre and Miquelon\",\"value\":\"spm\"},{\"label\":\"Saint Vincent and the Grenadines\",\"value\":\"vct\"},{\"label\":\"Samoa\",\"value\":\"wsm\"},{\"label\":\"San Marino\",\"value\":\"smr\"},{\"label\":\"Sao Tome and Principe\",\"value\":\"stp\"},{\"label\":\"Saudi Arabia\",\"value\":\"sau\"},{\"label\":\"Senegal\",\"value\":\"sen\"},{\"label\":\"Serbia\",\"value\":\"srb\"},{\"label\":\"Seychelles\",\"value\":\"syc\"},{\"label\":\"Sierra Leone\",\"value\":\"sle\"},{\"label\":\"Singapore\",\"value\":\"sgp\"},{\"label\":\"Sint Maarten (Dutch part)\",\"value\":\"sxm\"},{\"label\":\"Slovakia\",\"value\":\"svk\"},{\"label\":\"Slovenia\",\"value\":\"svn\"},{\"label\":\"Solomon Islands\",\"value\":\"slb\"},{\"label\":\"Somalia\",\"value\":\"som\"},{\"label\":\"South Africa\",\"value\":\"zaf\"},{\"label\":\"South Georgia and the South Sandwich Islands\",\"value\":\"sgs\"},{\"label\":\"South Sudan\",\"value\":\"ssd\"},{\"label\":\"Spain\",\"value\":\"esp\"},{\"label\":\"Sri Lanka\",\"value\":\"lka\"},{\"label\":\"Sudan\",\"value\":\"sdn\"},{\"label\":\"Suriname\",\"value\":\"sur\"},{\"label\":\"Svalbard and Jan Mayen\",\"value\":\"sjm\"},{\"label\":\"Swaziland\",\"value\":\"swz\"},{\"label\":\"Sweden\",\"value\":\"swe\"},{\"label\":\"Switzerland\",\"value\":\"che\"},{\"label\":\"Syrian Arab Republic\",\"value\":\"syr\"},{\"label\":\"Taiwan\",\"value\":\"twn\"},{\"label\":\"Tajikistan\",\"value\":\"tjk\"},{\"label\":\"Tanzania, United Republic of\",\"value\":\"tza\"},{\"label\":\"Thailand\",\"value\":\"tha\"},{\"label\":\"Timor-Leste\",\"value\":\"tls\"},{\"label\":\"Togo\",\"value\":\"tgo\"},{\"label\":\"Tokelau\",\"value\":\"tkl\"},{\"label\":\"Tonga\",\"value\":\"ton\"},{\"label\":\"Trinidad and Tobago\",\"value\":\"tto\"},{\"label\":\"Tunisia\",\"value\":\"tun\"},{\"label\":\"Turkey\",\"value\":\"tur\"},{\"label\":\"Turkmenistan\",\"value\":\"tkm\"},{\"label\":\"Turks and Caicos Islands\",\"value\":\"tca\"},{\"label\":\"Tuvalu\",\"value\":\"tuv\"},{\"label\":\"Uganda\",\"value\":\"uga\"},{\"label\":\"Ukraine\",\"value\":\"ukr\"},{\"label\":\"United Arab Emirates\",\"value\":\"are\"},{\"label\":\"United Kingdom of Great Britain and Northern Ireland\",\"value\":\"gbr\"},{\"label\":\"United States Minor Outlying Islands\",\"value\":\"umi\"},{\"label\":\"United States of America\",\"value\":\"usa\"},{\"label\":\"Uruguay\",\"value\":\"ury\"},{\"label\":\"Uzbekistan\",\"value\":\"uzb\"},{\"label\":\"Vanuatu\",\"value\":\"vut\"},{\"label\":\"Venezuela (Bolivarian Republic of)\",\"value\":\"ven\"},{\"label\":\"Viet Nam\",\"value\":\"vnm\"},{\"label\":\"Virgin Islands (British)\",\"value\":\"vgb\"},{\"label\":\"Virgin Islands (U.S.)\",\"value\":\"vir\"},{\"label\":\"Wallis and Futuna\",\"value\":\"wlf\"},{\"label\":\"Western Sahara\",\"value\":\"esh\"},{\"label\":\"Yemen\",\"value\":\"yem\"},{\"label\":\"Zambia\",\"value\":\"zmb\"},{\"label\":\"Zimbabwe\",\"value\":\"zwe\"}],\"currencies\":[{\"label\":\"AED\",\"value\":\"aed\"},{\"label\":\"AFN\",\"value\":\"afn\"},{\"label\":\"ALL\",\"value\":\"all\"},{\"label\":\"AMD\",\"value\":\"amd\"},{\"label\":\"ANG\",\"value\":\"ang\"},{\"label\":\"AOA\",\"value\":\"aoa\"},{\"label\":\"ARS\",\"value\":\"ars\"},{\"label\":\"AUD\",\"value\":\"aud\"},{\"label\":\"AWG\",\"value\":\"awg\"},{\"label\":\"AZN\",\"value\":\"azn\"},{\"label\":\"BAM\",\"value\":\"bam\"},{\"label\":\"BBD\",\"value\":\"bbd\"},{\"label\":\"BDT\",\"value\":\"bdt\"},{\"label\":\"BGN\",\"value\":\"bgn\"},{\"label\":\"BHD\",\"value\":\"bhd\"},{\"label\":\"BIF\",\"value\":\"bif\"},{\"label\":\"BMD\",\"value\":\"bmd\"},{\"label\":\"BND\",\"value\":\"bnd\"},{\"label\":\"BOB\",\"value\":\"bob\"},{\"label\":\"BRL\",\"value\":\"brl\"},{\"label\":\"BSD\",\"value\":\"bsd\"},{\"label\":\"BTN\",\"value\":\"btn\"},{\"label\":\"BWP\",\"value\":\"bwp\"},{\"label\":\"BYN\",\"value\":\"byn\"},{\"label\":\"BYR\",\"value\":\"byr\"},{\"label\":\"BZD\",\"value\":\"bzd\"},{\"label\":\"CAD\",\"value\":\"cad\"},{\"label\":\"CDF\",\"value\":\"cdf\"},{\"label\":\"CHF\",\"value\":\"chf\"},{\"label\":\"CKD\",\"value\":\"ckd\"},{\"label\":\"CLP\",\"value\":\"clp\"},{\"label\":\"CNY\",\"value\":\"cny\"},{\"label\":\"COP\",\"value\":\"cop\"},{\"label\":\"CRC\",\"value\":\"crc\"},{\"label\":\"CUC\",\"value\":\"cuc\"},{\"label\":\"CUP\",\"value\":\"cup\"},{\"label\":\"CVE\",\"value\":\"cve\"},{\"label\":\"CZK\",\"value\":\"czk\"},{\"label\":\"DJF\",\"value\":\"djf\"},{\"label\":\"DKK\",\"value\":\"dkk\"},{\"label\":\"DOP\",\"value\":\"dop\"},{\"label\":\"DZD\",\"value\":\"dzd\"},{\"label\":\"EGP\",\"value\":\"egp\"},{\"label\":\"ERN\",\"value\":\"ern\"},{\"label\":\"ETB\",\"value\":\"etb\"},{\"label\":\"EUR\",\"value\":\"eur\"},{\"label\":\"FJD\",\"value\":\"fjd\"},{\"label\":\"FKP\",\"value\":\"fkp\"},{\"label\":\"GBP\",\"value\":\"gbp\"},{\"label\":\"GEL\",\"value\":\"gel\"},{\"label\":\"GHS\",\"value\":\"ghs\"},{\"label\":\"GIP\",\"value\":\"gip\"},{\"label\":\"GMD\",\"value\":\"gmd\"},{\"label\":\"GNF\",\"value\":\"gnf\"},{\"label\":\"GTQ\",\"value\":\"gtq\"},{\"label\":\"GYD\",\"value\":\"gyd\"},{\"label\":\"HKD\",\"value\":\"hkd\"},{\"label\":\"HNL\",\"value\":\"hnl\"},{\"label\":\"HRK\",\"value\":\"hrk\"},{\"label\":\"HTG\",\"value\":\"htg\"},{\"label\":\"HUF\",\"value\":\"huf\"},{\"label\":\"IDR\",\"value\":\"idr\"},{\"label\":\"ILS\",\"value\":\"ils\"},{\"label\":\"IMP[G]\",\"value\":\"imp[g]\"},{\"label\":\"INR\",\"value\":\"inr\"},{\"label\":\"IQD\",\"value\":\"iqd\"},{\"label\":\"IRR\",\"value\":\"irr\"},{\"label\":\"ISK\",\"value\":\"isk\"},{\"label\":\"JEP[G]\",\"value\":\"jep[g]\"},{\"label\":\"JMD\",\"value\":\"jmd\"},{\"label\":\"JOD\",\"value\":\"jod\"},{\"label\":\"JPY\",\"value\":\"jpy\"},{\"label\":\"KES\",\"value\":\"kes\"},{\"label\":\"KGS\",\"value\":\"kgs\"},{\"label\":\"KHR\",\"value\":\"khr\"},{\"label\":\"KMF\",\"value\":\"kmf\"},{\"label\":\"KPW\",\"value\":\"kpw\"},{\"label\":\"KRW\",\"value\":\"krw\"},{\"label\":\"KWD\",\"value\":\"kwd\"},{\"label\":\"KYD\",\"value\":\"kyd\"},{\"label\":\"KZT\",\"value\":\"kzt\"},{\"label\":\"LAK\",\"value\":\"lak\"},{\"label\":\"LBP\",\"value\":\"lbp\"},{\"label\":\"LKR\",\"value\":\"lkr\"},{\"label\":\"LRD\",\"value\":\"lrd\"},{\"label\":\"LSL\",\"value\":\"lsl\"},{\"label\":\"LYD\",\"value\":\"lyd\"},{\"label\":\"MAD\",\"value\":\"mad\"},{\"label\":\"MDL\",\"value\":\"mdl\"},{\"label\":\"MGA\",\"value\":\"mga\"},{\"label\":\"MKD\",\"value\":\"mkd\"},{\"label\":\"MMK\",\"value\":\"mmk\"},{\"label\":\"MNT\",\"value\":\"mnt\"},{\"label\":\"MOP\",\"value\":\"mop\"},{\"label\":\"MRO\",\"value\":\"mro\"},{\"label\":\"MUR\",\"value\":\"mur\"},{\"label\":\"MVR\",\"value\":\"mvr\"},{\"label\":\"MWK\",\"value\":\"mwk\"},{\"label\":\"MXN\",\"value\":\"mxn\"},{\"label\":\"MYR\",\"value\":\"myr\"},{\"label\":\"MZN\",\"value\":\"mzn\"},{\"label\":\"NAD\",\"value\":\"nad\"},{\"label\":\"NGN\",\"value\":\"ngn\"},{\"label\":\"NIO\",\"value\":\"nio\"},{\"label\":\"NOK\",\"value\":\"nok\"},{\"label\":\"NPR\",\"value\":\"npr\"},{\"label\":\"NZD\",\"value\":\"nzd\"},{\"label\":\"OMR\",\"value\":\"omr\"},{\"label\":\"PAB\",\"value\":\"pab\"},{\"label\":\"PEN\",\"value\":\"pen\"},{\"label\":\"PGK\",\"value\":\"pgk\"},{\"label\":\"PHP\",\"value\":\"php\"},{\"label\":\"PKR\",\"value\":\"pkr\"},{\"label\":\"PLN\",\"value\":\"pln\"},{\"label\":\"PYG\",\"value\":\"pyg\"},{\"label\":\"QAR\",\"value\":\"qar\"},{\"label\":\"RON\",\"value\":\"ron\"},{\"label\":\"RSD\",\"value\":\"rsd\"},{\"label\":\"RUB\",\"value\":\"rub\"},{\"label\":\"RWF\",\"value\":\"rwf\"},{\"label\":\"SAR\",\"value\":\"sar\"},{\"label\":\"SBD\",\"value\":\"sbd\"},{\"label\":\"SCR\",\"value\":\"scr\"},{\"label\":\"SDG\",\"value\":\"sdg\"},{\"label\":\"SEK\",\"value\":\"sek\"},{\"label\":\"SGD\",\"value\":\"sgd\"},{\"label\":\"SHP\",\"value\":\"shp\"},{\"label\":\"SLL\",\"value\":\"sll\"},{\"label\":\"SOS\",\"value\":\"sos\"},{\"label\":\"SRD\",\"value\":\"srd\"},{\"label\":\"SSP\",\"value\":\"ssp\"},{\"label\":\"STD\",\"value\":\"std\"},{\"label\":\"SYP\",\"value\":\"syp\"},{\"label\":\"SZL\",\"value\":\"szl\"},{\"label\":\"THB\",\"value\":\"thb\"},{\"label\":\"TJS\",\"value\":\"tjs\"},{\"label\":\"TMT\",\"value\":\"tmt\"},{\"label\":\"TND\",\"value\":\"tnd\"},{\"label\":\"TOP\",\"value\":\"top\"},{\"label\":\"TRY\",\"value\":\"try\"},{\"label\":\"TTD\",\"value\":\"ttd\"},{\"label\":\"TVD[G]\",\"value\":\"tvd[g]\"},{\"label\":\"TWD\",\"value\":\"twd\"},{\"label\":\"TZS\",\"value\":\"tzs\"},{\"label\":\"UAH\",\"value\":\"uah\"},{\"label\":\"UGX\",\"value\":\"ugx\"},{\"label\":\"USD\",\"value\":\"usd\"},{\"label\":\"UYU\",\"value\":\"uyu\"},{\"label\":\"UZS\",\"value\":\"uzs\"},{\"label\":\"VEF\",\"value\":\"vef\"},{\"label\":\"VND\",\"value\":\"vnd\"},{\"label\":\"VUV\",\"value\":\"vuv\"},{\"label\":\"WST\",\"value\":\"wst\"},{\"label\":\"XAF\",\"value\":\"xaf\"},{\"label\":\"XCD\",\"value\":\"xcd\"},{\"label\":\"XOF\",\"value\":\"xof\"},{\"label\":\"XPF\",\"value\":\"xpf\"},{\"label\":\"YER\",\"value\":\"yer\"},{\"label\":\"ZAR\",\"value\":\"zar\"},{\"label\":\"ZMW\",\"value\":\"zmw\"}],\"languages\":[{\"label\":\"(Eastern) Punjabi\",\"value\":\"pa\"},{\"label\":\"Afrikaans\",\"value\":\"af\"},{\"label\":\"Albanian\",\"value\":\"sq\"},{\"label\":\"Amharic\",\"value\":\"am\"},{\"label\":\"Arabic\",\"value\":\"ar\"},{\"label\":\"Armenian\",\"value\":\"hy\"},{\"label\":\"Aymara\",\"value\":\"ay\"},{\"label\":\"Azerbaijani\",\"value\":\"az\"},{\"label\":\"Belarusian\",\"value\":\"be\"},{\"label\":\"Bengali\",\"value\":\"bn\"},{\"label\":\"Bislama\",\"value\":\"bi\"},{\"label\":\"Bosnian\",\"value\":\"bs\"},{\"label\":\"Bulgarian\",\"value\":\"bg\"},{\"label\":\"Burmese\",\"value\":\"my\"},{\"label\":\"Catalan\",\"value\":\"ca\"},{\"label\":\"Chamorro\",\"value\":\"ch\"},{\"label\":\"Chichewa\",\"value\":\"ny\"},{\"label\":\"Chinese\",\"value\":\"zh\"},{\"label\":\"Croatian\",\"value\":\"hr\"},{\"label\":\"Czech\",\"value\":\"cs\"},{\"label\":\"Danish\",\"value\":\"da\"},{\"label\":\"Divehi\",\"value\":\"dv\"},{\"label\":\"Dutch\",\"value\":\"nl\"},{\"label\":\"Dzongkha\",\"value\":\"dz\"},{\"label\":\"English\",\"value\":\"en\"},{\"label\":\"Estonian\",\"value\":\"et\"},{\"label\":\"Faroese\",\"value\":\"fo\"},{\"label\":\"Fijian\",\"value\":\"fj\"},{\"label\":\"Finnish\",\"value\":\"fi\"},{\"label\":\"French\",\"value\":\"fr\"},{\"label\":\"Fula\",\"value\":\"ff\"},{\"label\":\"Georgian\",\"value\":\"ka\"},{\"label\":\"German\",\"value\":\"de\"},{\"label\":\"Greek (modern)\",\"value\":\"el\"},{\"label\":\"Guaraní\",\"value\":\"gn\"},{\"label\":\"Haitian\",\"value\":\"ht\"},{\"label\":\"Hebrew (modern)\",\"value\":\"he\"},{\"label\":\"Hindi\",\"value\":\"hi\"},{\"label\":\"Hungarian\",\"value\":\"hu\"},{\"label\":\"Icelandic\",\"value\":\"is\"},{\"label\":\"Indonesian\",\"value\":\"id\"},{\"label\":\"Irish\",\"value\":\"ga\"},{\"label\":\"Italian\",\"value\":\"it\"},{\"label\":\"Japanese\",\"value\":\"ja\"},{\"label\":\"Kalaallisut\",\"value\":\"kl\"},{\"label\":\"Kazakh\",\"value\":\"kk\"},{\"label\":\"Khmer\",\"value\":\"km\"},{\"label\":\"Kinyarwanda\",\"value\":\"rw\"},{\"label\":\"Kirundi\",\"value\":\"rn\"},{\"label\":\"Kongo\",\"value\":\"kg\"},{\"label\":\"Korean\",\"value\":\"ko\"},{\"label\":\"Kurdish\",\"value\":\"ku\"},{\"label\":\"Kyrgyz\",\"value\":\"ky\"},{\"label\":\"Lao\",\"value\":\"lo\"},{\"label\":\"Latin\",\"value\":\"la\"},{\"label\":\"Latvian\",\"value\":\"lv\"},{\"label\":\"Lingala\",\"value\":\"ln\"},{\"label\":\"Lithuanian\",\"value\":\"lt\"},{\"label\":\"Luba-Katanga\",\"value\":\"lu\"},{\"label\":\"Luxembourgish\",\"value\":\"lb\"},{\"label\":\"Macedonian\",\"value\":\"mk\"},{\"label\":\"Malagasy\",\"value\":\"mg\"},{\"label\":\"Malay\",\"value\":\"ms\"},{\"label\":\"Maltese\",\"value\":\"mt\"},{\"label\":\"Manx\",\"value\":\"gv\"},{\"label\":\"Māori\",\"value\":\"mi\"},{\"label\":\"Marshallese\",\"value\":\"mh\"},{\"label\":\"Mongolian\",\"value\":\"mn\"},{\"label\":\"Nauruan\",\"value\":\"na\"},{\"label\":\"Nepali\",\"value\":\"ne\"},{\"label\":\"Northern Ndebele\",\"value\":\"nd\"},{\"label\":\"Norwegian\",\"value\":\"no\"},{\"label\":\"Norwegian Bokmål\",\"value\":\"nb\"},{\"label\":\"Norwegian Nynorsk\",\"value\":\"nn\"},{\"label\":\"Pashto\",\"value\":\"ps\"},{\"label\":\"Persian (Farsi)\",\"value\":\"fa\"},{\"label\":\"Polish\",\"value\":\"pl\"},{\"label\":\"Portuguese\",\"value\":\"pt\"},{\"label\":\"Quechua\",\"value\":\"qu\"},{\"label\":\"Romanian\",\"value\":\"ro\"},{\"label\":\"Russian\",\"value\":\"ru\"},{\"label\":\"Samoan\",\"value\":\"sm\"},{\"label\":\"Sango\",\"value\":\"sg\"},{\"label\":\"Serbian\",\"value\":\"sr\"},{\"label\":\"Shona\",\"value\":\"sn\"},{\"label\":\"Sinhalese\",\"value\":\"si\"},{\"label\":\"Slovak\",\"value\":\"sk\"},{\"label\":\"Slovene\",\"value\":\"sl\"},{\"label\":\"Somali\",\"value\":\"so\"},{\"label\":\"Southern Ndebele\",\"value\":\"nr\"},{\"label\":\"Southern Sotho\",\"value\":\"st\"},{\"label\":\"Spanish\",\"value\":\"es\"},{\"label\":\"Swahili\",\"value\":\"sw\"},{\"label\":\"Swati\",\"value\":\"ss\"},{\"label\":\"Swedish\",\"value\":\"sv\"},{\"label\":\"Tajik\",\"value\":\"tg\"},{\"label\":\"Tamil\",\"value\":\"ta\"},{\"label\":\"Thai\",\"value\":\"th\"},{\"label\":\"Tigrinya\",\"value\":\"ti\"},{\"label\":\"Tonga (Tonga Islands)\",\"value\":\"to\"},{\"label\":\"Tsonga\",\"value\":\"ts\"},{\"label\":\"Tswana\",\"value\":\"tn\"},{\"label\":\"Turkish\",\"value\":\"tr\"},{\"label\":\"Turkmen\",\"value\":\"tk\"},{\"label\":\"Ukrainian\",\"value\":\"uk\"},{\"label\":\"Urdu\",\"value\":\"ur\"},{\"label\":\"Uzbek\",\"value\":\"uz\"},{\"label\":\"Venda\",\"value\":\"ve\"},{\"label\":\"Vietnamese\",\"value\":\"vi\"},{\"label\":\"Xhosa\",\"value\":\"xh\"},{\"label\":\"Zulu\",\"value\":\"zu\"}],\"locales\":{\"en\":{\"configuration\":\"Configuration\",\"contactEditionDescription\":\"Complete the contact details\",\"createCreditCard\":\"Create credit card\",\"idFromProviderSheet\":\"ID from providers sheet\",\"billingEditionDescription\":\"Complete billing details\",\"transactionErrorDescription\":\"The transaction was unsuccessful, try again when you want.\",\"company\":\"Company\",\"companyEditionDescription\":\"Complete your company information\",\"seeMore\":\"See more\",\"subscriptionClientError\":\"There is no client associated with the subscription.\",\"rolNameAlreadyExist\":\"There is already a rol with that name\",\"welcome\":\"Welcome\",\"mailForgotAccountSubject\":\"Recover Password\",\"notAvailable\":\"Not available\",\"slogan\":\"Archive and manage your income and expenses invoices, get real time reports of your personal or business finances.\",\"billingTitle\":\"Billing\",\"welcomeToVenpadSubject\":\"Welcome to Venpad!\",\"subsciptionPlanError\":\"Sorry, the selected plan is not valid to create a subscription.\",\"disable\":\"Disable\",\"messages\":\"Messages\",\"taxesTitle\":\"Taxes\",\"noPaymentMethods\":\"You don't have payment methods registered at the moment.\",\"userEditionDescription\":\"Complete the user details\",\"contactName\":\"Contact name\",\"continue\":\"Continue\",\"subscription\":\"Subscription\",\"clientId\":\"Client ID\",\"saveFeature\":\"Save Feature\",\"worksheetNotFound\":\"Was not found a worksheet called {worksheetName}\",\"provider\":\"Provider\",\"ok\":\"Ok\",\"maxLength\":\"Maximum length allowed is {maxLength} characteres\",\"userPlanAlreadyExist\":\"The plan is already associated with the user\",\"paymentMethods\":\"Payment methods\",\"mailPostSucessSubject\":\"Congrats! Your post was published\",\"subscriptionChangePlan\":\"The change of plan will be made on {date}.\",\"subscriptionNoCharges\":\"No recurring charges will be made.\",\"language\":\"Language\",\"configurationEditionDescription\":\"Manage the main application settings\",\"dueDate\":\"Due Date\",\"minLength\":\"Minimum length allowed is {minLength} characteres\",\"creditCardInfo\":\"Credit card info\",\"totalResults\":\"About {total} results\",\"row\":\"Row\",\"goToDashboard\":\"Go to dashboard\",\"username\":\"Username\",\"resetAccountDescription\":\"Set a new password to recovery your account\",\"saleInvoices\":\"Sale invoices\",\"contactPhone\":\"Contact phone\",\"subscriptionCreatedErrorPlan\":\"No plan associated with the subscription was found\",\"change\":\"Change\",\"mailSubscriptionChangedMessage\":\"<p>Yu have changed to {planName}, the subscription payment has been successful.</p><p>Thanks for being part of {appName}, Remember that this subscription makes automatic charges to your credit card. You can suspend the subscription whenever you want and view the history of your payments at any time from our web application.</p>\",\"total\":\"Total\",\"yourAccount\":\"Your Account\",\"filters\":\"Filters\",\"propertyUbication\":\"Property Ubication\",\"forgotAccountDescription\":\"We will send you an email with instructions to recover your account.\",\"withoutBilling\":\"WIthout billing\",\"providerId\":\"Provider ID\",\"uploadErrorSize\":\"Something is wrong, the maximum file size allowed is {maxFileSize} MB\",\"switchToPlan\":\"Switch to this plan\",\"transactionsDescription\":\"List of transactions made by the users\",\"expeditionDate\":\"Expedition date\",\"displayName\":\"Fullname\",\"date\":\"Date\",\"mail\":\"<h1><img src=\\\"{appLogo}\\\" width=\\\"35\\\" style=\\\"vertical-align:middle;\\\"> {appName}</h1><hr><h1>{title}</h1><p>{message}</p><hr><p>Sincerily,</p><p><a href=\\\"{appUrl}\\\" target=\\\"_blank\\\">{appUrl}</a></p>\",\"home\":\"Home\",\"forgotPassword\":\"Have you forgotten your password?\",\"purchaseCompleted\":\"Purchase was processed successfully\",\"homeDescription\":\"Informative News Regarding Social Leaders\",\"transactionDeclinedTitle\":\"Delined transaction\",\"saleTags\":\"Sale Tags\",\"planEditionDescription\":\"Complete the plan details\",\"manageCompany\":\"Manage your company\",\"taxEditionDescription\":\"Complete the details of the tax\",\"id\":\"ID\",\"menu\":\"Menu\",\"day\":\"Day\",\"document\":\"Document\",\"mailSubscriptionCreatedMessage\":\"<p>Your monthly subscription payment has been successful and will be active from day {startDate}.</p><p>Thanks for being part of {appName}, Remember that this subscription makes automatic charges to your credit card. You can suspend the subscription whenever you want and view the history of your payments at any time from our web application.</p>\",\"options\":\"Options\",\"forgotAccountTitle\":\"Forgot your account?\",\"rooms\":\"Rooms\",\"billing/invoiceNumber\":\"Billing ID / Invoice #\",\"expensesTitle\":\"Expenses\",\"description\":\"Description\",\"authNotPriviliges\":\"You do not have enough privileges to access this page\",\"recoverPasswordDescription\":\"Enter your registered email, a link will be sent so you can change your password.\",\"order\":\"Order\",\"localesDescription\":\"List of locales\",\"mailSubscriptionCanceledSubject\":\"Your subscription has been canceled \",\"comingSoonDescription\":\"We are working hard to offer you a better service, we will return shortly.\",\"creditCardCvc\":\"last 3 numbers\",\"faq\":\"FAQ\",\"siteName\":\"Venpad\",\"catalogs\":\"Catalogs\",\"subscriptionChangePlanImmediately\":\"The change of plan will be made immediately.\",\"contactTitle\":\"Contact Us\",\"transactions\":\"Transactions\",\"creditCardInvalidNumber\":\"Credit card number invalid\",\"mailSubscriptionCanceledMessage\":\"Your subscription has been canceled, we had problems debiting the payment on your credit card, verify the funds of your credit card or contact your bank for more information.\",\"year\":\"Year\",\"newInvoice\":\"New Invoice\",\"confirmation\":\"Confimation\",\"acceptTerms\":\"Accept terms and conditions\",\"planEditionTitle\":\"Plan Edition\",\"uploadInProcess\":\"We are processing the excel file...\",\"creditCardInvalidCVC\":\"Invalid credit card cvc\",\"transactionExpiredDescription\":\"We are sorry, the transaction has expired, you can try again when you want.\",\"multiCurrency\":\"Multi-currency\",\"customText\":\"Custom text\",\"notPlanAssociatedWithSubscription\":\"Was not plan found associated with the subscription\",\"mailPostSucessMessage\":\"<p>Congratulations! your post was published.</p><ul><li>Now you can access and highlight your post through this <a href=\\\"{postUrl}\\\" target=\\\"_blank\\\">link</a></li><li>Manage your other posts in this <a href=\\\"{posManagementUrl}\\\" target=\\\"_blank\\\">link</a></li></ul>\",\"subscriptionCurrent\":\"You are subscribed to the ({planName}).\",\"companyTaxesTitle\":\"Impuestos\",\"serverError\":\"Sorry, we are having some temporary server issues\",\"lastname\":\"Lastname\",\"documentType\":\"Document type\",\"welcomeUser\":\"Welcome {username}!\",\"quantity\":\"Quantity\",\"recoverPasswordSuccess\":\"An email has been sent with details to reset your password\",\"privacyTitle\":\"Privacy Policy\",\"loginWithFacebook\":\"Login with Facebook\",\"reportDescription\":\"Analyze your information\",\"templateForBulkUpload\":\"Template for bulk upload\",\"limitedReports\":\"Limited reports\",\"updateCreditCard\":\"Update credit card\",\"spendingInvoices\":\"Spending invoices\",\"transactionApprovedTitle\":\"Approved transaction\",\"loginWithEmail\":\"Login with your email\",\"mailSubscriptionCreatedSubject\":\"Your subscription to {appName} has started\",\"subTotal\":\"Subtotal\",\"login\":\"Login\",\"emailOrUsername\":\"Email or username\",\"syncDescription\":\"Sync external data\",\"uploadWarnings\":\"The accepted formats are .jpg, .gif and .png. The maximum size is 10 MB.\",\"transactionExpiredTitle\":\"Expired transaction\",\"passwordConfirm\":\"Confirm Password\",\"subscriptionSchedule\":\"Your subscription will be renewed at {planName} on {date}.\\n\",\"expenseUploadDescription\":\"Bulk upload of expenses\",\"subscriptionNextPayment\":\"The next payment will be made on {date}.\",\"onlyWithoutTaxes\":\"Only without taxes\",\"seeAll\":\"See all\",\"loginError\":\"There was problem authenticating your acount via {provider}.\",\"mailContactSubject\":\"Contact\",\"contactEmail\":\"Contact email\",\"billingDescription\":\"Billing history\",\"billingIsNotActive\":\"Billing selected is not active\",\"profileDescription\":\"Manage your data\",\"buyDescription\":\"Confirmation and payment method\",\"createSubscriptionError\":\"We are sorry, there was an error charging the subscription to your credit card, contact your bank for more information.\",\"emailSuccess\":\"The email was sent succesfully\",\"userAlreadySignout\":\"User already signout\",\"docsDescription\":\"Everything you need to know to get started with Venpad\",\"subscriptionAlreadyExist\":\"Sorry, you alredy have a subscription.\",\"signUp\":\"Sign Up\",\"unitCost\":\"Unit Cost\",\"invoiceSaveDescription\":\"Complete the details of your invoice\",\"idFromBillingSheet\":\"ID from billing sheet\",\"range\":\"Range\",\"companyPerformance\":\"Company Performance\",\"firstname\":\"Firstname\",\"billingHasExpired\":\"Billing selected has expired\",\"userResetError\":\"No requests for account recovery were found\",\"mailSubscriptionSuspendedMessage\":\"<p>Your suscription has been canceled. You will have access at the resources allowed by your plan until {expirationDate}. We will not charge you more, until you decided return.</p><p>You can reactivate your subscription whenever you want. When you decide to return, we will be waiting for you happy.</p>\",\"reportTitle\":\"Reports\",\"customMessage\":\"Custom message\",\"creditCardHolder\":\"Credit card holder\",\"currencyCodeIncorrect\":\"Currency code incorrect\",\"mailContactMessage\":\"<p><strong>Name:</strong> {name}</p><p><strong>Email:</strong> {email}</p><p><strong>Phone:</strong> {phone}</p><p><strong>Message:</strong> {message}</p>\",\"mainError\":\"We are sorry, something went wrong ({error})\",\"userAlreadyHasSubscription\":\"The user is already subscribed to the plan selected\",\"transactionPendingDescription\":\"The transaction is beign processed at this time, we will update the status of your payment when we have a response from it.\",\"currency\":\"Currency\",\"country\":\"Country\",\"terms\":\"Terms\",\"sales\":\"Sales\",\"usernameNotFound\":\"Username was not found\",\"planNotFound\":\"We are sorry the plan was not found or is not available to buy.\",\"addLocaleCodeForMultilanguage\":\"You can add a locale code for multilanguage support\",\"userInfo\":\"User Info\",\"fullname\":\"Fullname\",\"resetAccountTitle\":\"New Password\",\"paymentDate\":\"Payment Date\",\"rentPrice\":\"Rent price\",\"contactUs\":\"Contact Us\",\"planFeatures\":\"Plan features\",\"faqDescription\":\"Resolve any concerns you have about Venpad\",\"email\":\"Email\",\"profile\":\"Profile\",\"planFeatureAlreadyExist\":\"The feature already exist in the plan\",\"income\":\"Income\",\"billing\":\"Billing\",\"securePaymentInfo\":\"We protect your payment information with SSL (Secure Sockets Layer) when it is sent over the internet. SSL encrypts the payment information so it can not be intercepted by a third party. Venpad does not save your credit card information.\",\"summary\":\"Summary\",\"companyTaxEditionTitle\":\"Tax Edition\",\"taxEditionTitle\":\"Tax Edition\",\"termsDescription\":\"Terms and conditions of use of Venpad service\",\"receiptsDescription\":\"Here you receipts of your payments\",\"InvoicesDescription\":\"Record your income\",\"features\":\"Features\",\"localeEditionDescription\":\"Complete the locale details\",\"transactionErrorTitle\":\"Failed Transaction\",\"catalogsTitle\":\"Catalogs\",\"denounce\":\"Denounce\",\"phone\":\"Phone\",\"department\":\"Department\",\"updateSubscription\":\"Update subscription\",\"usersTitle\":\"Users\",\"userNotHaveLocalPassport\":\"User does not have local authentication strategy, please contact us for more information\",\"userAlreadyHasAccount\":\"Do you already have an account? you can login \",\"loginWithGoogle\":\"Login with Google\",\"termsTitle\":\"Terms and Conditions\",\"feature\":\"Feature\",\"website\":\"Website\",\"withoutTaxes\":\"Without Taxes\",\"uploadPhoto\":\"Upload photo\",\"backupByYear\":\"Backup excel by year\",\"catalogNameAlreadyExist\":\"There is already a catalog with that name\",\"successfulOperation\":\"Succesful Operation\",\"providers\":\"Providers\",\"companyTaxEditionDescription\":\"Complete tax details\",\"contactsTitle\":\"Your Contacts\",\"receipts\":\"Receipts\",\"withTaxes\":\"With taxes\",\"onlyWithTaxes\":\"Only with taxes\",\"perMonth\":\"per month\",\"usernameAlreadyExist\":\"There is already an account with that username\",\"emailConfirmed\":\"Congratulations you have already confirmed your email\",\"register\":\"Register\",\"registerTitle\":\"Create an account\",\"wrongAssociatedID\":\"Was not found record associated with that code/id\",\"welcomeToVenpadMessage\":\"We are pleased to have you at Venpad {username}, this is the first step to improve your financial life, we are working to improve our system day by day and provide you with the best strategies to help you in your finances.\\n\",\"postTitle\":\"Post\",\"mostPopular\":\"Most Popular!\",\"plan\":\"Plan\",\"configurationEditionTitle\":\"Configuration edition\",\"invoicesDescription\":\"Register your sales invoices\",\"securePayment\":\"Secure payment through PayU\",\"select\":\"Select\",\"invoicesTitle\":\"Income\",\"subscriptionNoPaymentMethods\":\"To change your subscription, you need to create a credit card.\",\"deleteExpenseConfirmation\":\"Are you sure to delete this expense?\",\"ubication\":\"Ubication\",\"uploadExcel\":\"Upload Excel\",\"recoverPasswordTitle\":\"Recover Password\",\"userValidateEmailSuccess\":\"Thanks your email was succesfully confirmed\",\"userEditionTitle\":\"User Edition\",\"logout\":\"logout\",\"post\":\"Post\",\"noSummaryYet\":\"There is not summary yet\",\"authStrategyFailed\":\"We are sorry, no users were found with that authentication strategy.\",\"invoices\":\"Invoices\",\"auth/user-not-found\":\"User was not found\",\"excelReport\":\"Excel Report\",\"invoicesUploadDescription\":\"Bulk upload of invoices\",\"notFoundDescription\":\"The link you followed was may broken, or the page may have been removed\",\"invoicesUploadTitle\":\"Upload Invoices\",\"incorrectProperties\":\"The following properties are required {properties}\",\"unauthorized\":\"Unauthorized\",\"haveNotAccount\":\"You have not created your account yet?\",\"admin\":\"Admin\",\"expenseUploadTitle\":\"Upload Expenses\",\"expensesDescription\":\"Register your expenses invoices\",\"creditCardExpired\":\"Credit card is expired\",\"unlimitedReports\":\"Unlimited reports\",\"syncTitle\":\"Synchronization\",\"synchronization\":\"Synchronization\",\"profit\":\"Profit\",\"creditCardUpdatedSuccessfully\":\"The credit card was updated successfully.\",\"userNotHaveNotifications\":\"You don`t have new notifications\",\"buyTitle\":\"Buy {planName} \",\"here\":\"Here\",\"localesTitle\":\"Locales\",\"contactDescription\":\"You can contact us at anytime, through any of our chanels:\",\"docsTitle\":\"Documentation\",\"disableApp\":\"Disable App\",\"month\":\"Month\",\"notFoundTitle\":\"This page is not available\",\"emailError\":\"There was an error sending email\",\"creditCardNumber\":\"Credit card number\",\"unlimitedExpenses\":\"Unlimited expenses\",\"allIncludedTaxes\":\"All included taxes\",\"emailUnconfirmed\":\"You haven`t confirmed your email, please review your email and follor the instructions\",\"incorrectFormat\":\"Format incorrect\",\"invoiceNumber\":\"Invoice #\",\"billingIdHelp\":\"Billing ID = Copy the billing Id from billing sheet\",\"passwordIncorrect\":\"Password is incorrect\",\"loginTitle\":\"Login\",\"companyEditionTitle\":\"Your Company\",\"uploadFileToProcess\":\"Upload a file to process\",\"publish\":\"Publish\",\"contacts\":\"Contacts\",\"neighborhood\":\"Neighborhood\",\"requiredFields\":\"Fields marked with (*) are required\\n\",\"catalogEditionTitle\":\"Catalog Edition\",\"newExpense\":\"New Expense\",\"permission\":\"Permission\",\"complaint\":\"Complaint\",\"notEmpty\":\"Must not be empty\",\"faqTitle\":\"Frequently Asked Questions\",\"uploadErrorExtentions\":\"Something is wrong, only the following formats are supported: {extentions}\",\"expenses\":\"Expenses\",\"companyTaxesDescription\":\"List of taxes\",\"name\":\"Name\",\"contact\":\"Contact\",\"search\":\"Search\",\"administration\":\"Administration\",\"users\":\"Users\",\"localesRefreshed\":\"Translations was updated\",\"mailWelcomeUserMessage\":\"<p>We're happy to welcome you at Venpad. To get you started, here's your account information.</p><p><strong>Login url: </strong><a href=\\\"{urlLogin}\\\" target=\\\"_blank\\\">{urlLogin}</a></p><p><strong>Login username:</strong> {username}</p><p><strong>Login password: </strong>{password}</p><p>To confirm your email address, please click on the following <a href=\\\"{urlConfirmEmail}\\\" target=\\\"_blank\\\">{urlConfirmEmail}</a>.</p>\",\"catalogEditionDescription\":\"Complete the catalog details\",\"getStarted\":\"Get Started\",\"deleteItem\":\"Are you sure to delete this item?\",\"mailSubscriptionSuspendedSubject\":\"<p>Your subscription to {appName} has been suspended</p>\",\"selectBilling\":\"Select billing\",\"loginDescription\":\"Login quickly with your email or social networks.\",\"formErrors\":\"There are errors in the form, please review the comments marked with red color.\",\"status\":\"Status\",\"getStartedNow\":\"Get started\",\"profileTitle\":\"Profile\",\"userNotHaveMessages\":\"You don't have new messages\",\"usersDescription\":\"List of registered users\",\"invoiceEditionDescription\":\"Complete the details of your invoice\",\"sync\":\"Sync\",\"loginWithSocialNetworks\":\"Login with your social networks\",\"transactionNotFound\":\"It was not possible update the status of the transaction, the transaction was not found.\",\"expenseTags\":\"Expense Tags\",\"propertyType\":\"Property type\",\"privacyDescription\":\"Below we explaine how Venpad manages your registered data\",\"savePassword\":\"Save Password\",\"referenceId\":\"Reference Number\",\"send\":\"Send\",\"errors\":\"Errors\",\"localeEditionTitle\":\"Locale Edition\",\"priceTitle\":\"Prices\",\"value\":\"Value\",\"apiIntegration\":\"API integration\",\"dashboardTitle\":\"Complaint\",\"offerType\":\"Offer type\",\"userResetPasswordEmpty\":\"The new password can not be empty\",\"planNameAlreadyExist\":\"There is already a plan with that name\",\"thereIsNotDataToChart\":\"There is not data to chart\",\"developersDescription\":\"API documentation for developers\",\"mailWelcomeUserSubject\":\"Welcome\",\"amount\":\"Amount\",\"cancel\":\"Cancel\",\"expirationDate\":\"Expiration date\",\"subscriptionDescription\":\"Cancel, suspend or update your subscription plan\",\"taxesDescription\":\"Configure your taxes\",\"dragProfilePicture\":\"Here, drag your profile picture or upload a photo\",\"photo\":\"Photo\",\"months\":\"January, February, March, May, June, July, August, September, October, November, December\",\"address\":\"Address\",\"mustInclude\":\"Must include some of the following values {values}\",\"taxes\":\"Taxes\",\"documentNumber\":\"Document number\",\"buy\":\"Buy\",\"invalidDataType\":\"Invalid data type, it must be {dataType}\",\"excelBackup\":\"Excel backup\",\"tags\":\"Tags\",\"backup\":\"Backup\",\"priceDescription\":\"Choose a plan and start billing\",\"privacy\":\"Privacy\",\"try\":\"Try\",\"noApiIntegration\":\"No API integration\",\"addItem\":\"Add item\",\"auth/wrong-password\":\"The password is wrong\",\"sendEmail\":\"Send email\",\"subscriptionNextPlan\":\"Next to change to {planName}\",\"contactEditionTitle\":\"Contact Edition\",\"unlimitedInvoices\":\"Unlimited invoices\",\"plansDescription\":\"List of plans\",\"contactsDescription\":\"Record your clients and providers\",\"mustAcceptTerms\":\"You must agree to terms and conditions\",\"idFromClientSheet\":\"ID from client sheet\",\"comingSoonTitle\":\"Coming Soon!\",\"expirationDateInvalid\":\"Expiration date invalid\",\"bulkUpload\":\"Bulk upload\",\"locales\":\"Locales\",\"reports\":\"Reports\",\"preferences\":\"Preferences\",\"city\":\"City\",\"auth/email-already-in-use\":\"Email already in use\",\"dateFormat\":\"Date format\",\"billingEditionTitle\":\"Billing Edition\",\"subscriptionCreatedErrorCard\":\"Your credit card has been rejected, please contact your bank for more information.\",\"client\":\"Client\",\"support\":\"Support\",\"mailForgotAccountMessage\":\"<p>You have requested a re-establishment of your password, you can do this by following the link <a href=\\\"{url}\\\" target=\\\"_blank\\\">{url}</a>, remember that this link is valid for 1 hour.</p><p>If you have not requested it, please ignore this email.</p>\",\"all\":\"All\",\"cancelInvoiceConfirmation\":\"Are you sure to cancel this invoice?\",\"syncIntlData\":\"Sync international data\",\"propertyInfo\":\"Property Information\",\"plansTitle\":\"Plans\",\"dashboardDescription\":\"Welcome to Your Complaint Form\",\"password\":\"Password\",\"fieldIsRequired\":\"This field is required\",\"salePrice\":\"Sale price\",\"auth/invalid-email\":\"Email already in use\",\"notMatch\":\"Not match\",\"userEdition\":\"User edition\",\"photos\":\"Photos\",\"dragPhotos\":\"Here, drag your or upload a photos\",\"localeNameAlreadyExist\":\"There is already an locale with that name\",\"securityCode\":\"Security code\",\"transactionPendingTitle\":\"Pending transaction\",\"transactionApprovedDescription\":\"The transaction has been approved successfully.\",\"transactionsTitle\":\"Transactions\",\"save\":\"Save\",\"creditCardInvalidExpirationDate\":\"La fecha de expiració de la tarjet de credito es invalida\",\"results\":\"Results\",\"userInactive\":\"Yout account has been inactivated, please contact us for more information\",\"notifications\":\"Notifications\",\"currencyAtRecurrence\":\"{currency} per {recurrence}\",\"postDescription\":\"Easy and fast\",\"price\":\"Price\",\"subscriptionDeletedSuccess\":\"The subscription was canceled successfully\",\"receiptsTitle\":\"Receipts\",\"auth/argument-error\":\"Invalid token\",\"mailTransactionSuccessSubject\":\"Successful transaction\",\"invoiceEditionTitle\":\"Invoice Edition\",\"registerWithEmail\":\"Register with email\",\"clients\":\"Clients\",\"thereIsNotData\":\"There is no data to show at this moment\",\"saveUserClaim\":\"Save Permission\",\"userClaims\":\"User permissions\",\"billingExceedRangeOfInvoices\":\"Billing selected exceed the maximum range allowed\",\"allBilling\":\"All billing\",\"invalidDateFormat\":\"Date has invalid format\",\"subscriptionCanceled\":\"Your subscription has been canceled, we will stop charging your credit card.\",\"paymentMethod\":\"Payment method\",\"userValidateEmailError\":\"The email could not be confirmed, the user was not found\",\"mailTransactionSuccessMessage\":\"Congratulations!, the payment for {itemDescription} was processed correctly.\\n\",\"registerDescription\":\"Create an account easy and fast, with your email or social networks.\",\"transactionDeclinedDescription\":\"We are sorry, the transaction has been rejected, verify your data and try again.\",\"loadMore\":\"Load more\",\"expenseEditionDescription\":\"Complete expense details\",\"subscriptionWarnings\":\"<p>I understand that my subscription will make automatic charges every month.</p><p>I understant that i can suspend my subscription whenever i want.</p>\",\"subscriptionCreatedSuccess\":\"The purchase was made successfully\",\"includeTaxes\":\"Include taxes\",\"help\":\"Help\",\"subscriptionCreatedError\":\"We are sorry we could not execute the transaction at this time, please try later\",\"expenseEditionTitle\":\"Expense Edition\",\"allMonths\":\"All months\",\"registerWithSocialNetworks\":\"Register with social networks\",\"plans\":\"Plans\",\"updateExpense\":\"Update Expense\",\"selectYear\":\"Select year\",\"deleteConfirmation\":\"Are you sure you delete this?\",\"authStrategyNotProvided\":\"Authentication strategy not provided.\",\"updateCreditCardSuccessful\":\"The credit card was updated successfuly.\",\"userEmailAlreadyExist\":\"There is already an account with that email\",\"authNotLogin\":\"You have to login to access this page\",\"errorFileUpload\":\"There were errors uploading the file, please check the following errors and try again\",\"authStrategyNotAvailable\":\"Authentication startegy not available.\",\"mailSubscriptionChangedSubject\":\"Your subscription has changed\",\"recoverPassword\":\"Recovery Password\",\"subscriptionPlanFree\":\"With this option you pause or suspend the payments of your subscription. You will have limited access to the services.\",\"updateInvoice\":\"Update Invoice\",\"docs\":\"Documents\",\"billingId\":\"Billing ID\",\"subscriptionTitle\":\"Manage suscription\",\"userResetSuccess\":\"The password was updated succesfully, now you can access your account with your new password\",\"goHomePage\":\"Go to homepage\",\"homeTitle\":\"News\",\"developersTitle\":\"Developers\",\"sessionFinished\":\"Your session was sucessfully finished, now you can leave the device safely\",\"developers\":\"Developers\",\"expenseInvoices\":\"Expense invoices\",\"processedDataComplete\":\"{numData} file(s) were processed succesfully\"},\"es\":{\"configuration\":\"Configuración\",\"contactEditionDescription\":\"Completa los detalles de contacto\",\"createCreditCard\":\"Crear tarjeta de crédito\",\"idFromProviderSheet\":\"ID de hoja de proveedores\",\"billingEditionDescription\":\"Complete los detalles de facturación\",\"transactionErrorDescription\":\"La transacción fue fallida, inténtalo de nuevo cuando quieras.\",\"company\":\"Empresa\",\"companyEditionDescription\":\"Completa la información de tu compañia\",\"seeMore\":\"Ver más\",\"subscriptionClientError\":\"No hay una cliente asociado a la suscripción.\",\"rolNameAlreadyExist\":\"Ya hay un rol con ese nombre\",\"welcome\":\"Bienvenido\",\"mailForgotAccountSubject\":\"Recuperar Contraseña\",\"notAvailable\":\"No disponible\",\"slogan\":\"Archiva y administra tus facturas de ingreso y gastos, obten reportes en tiempo real de tus finanzas personales o de negocio.\",\"billingTitle\":\"Facturación\",\"welcomeToVenpadSubject\":\"Bienvenido a Venpad!\",\"subsciptionPlanError\":\"Lo sentimos, el plan seleccionado no es válido para crear una suscripción.\",\"disable\":\"Deshabilitar\",\"messages\":\"Mensajes\",\"taxesTitle\":\"Impuestos\",\"noPaymentMethods\":\"No tienes métodos de pago registrados en el momento.\",\"userEditionDescription\":\"Complete los detalles de usuario\",\"contactName\":\"Nombre de contacto\",\"continue\":\"Continuar\",\"subscription\":\"Suscripción\",\"clientId\":\"Cliente ID\",\"saveFeature\":\"Guardar Catacterística\",\"worksheetNotFound\":\"No se encontró una hoja de calculo llamana {worksheetName}\",\"provider\":\"Proveedor\",\"ok\":\"Aceptar\",\"maxLength\":\"Maximá longitud permitida  {maxLength} caracteres\",\"userPlanAlreadyExist\":\"El plan ya esta asociado con el usuario\",\"paymentMethods\":\"Medios de pago\",\"mailPostSucessSubject\":\"Felicitaciones! Tu aviso fue publicado\",\"subscriptionChangePlan\":\"El cambio de plan se realizará el {date}.\",\"subscriptionNoCharges\":\"No se realizarán cargos recurrentes.\",\"language\":\"Idioma\",\"configurationEditionDescription\":\"Administra la configuración principal de la aplicación\",\"dueDate\":\"Fecha de Vecimiento\",\"minLength\":\"Minima longitud permitida es {minLength} caracteres\",\"creditCardInfo\":\"Datos de la tarjeta de crédito\",\"totalResults\":\"Aproximadamente {total} resulados\",\"row\":\"Fila\",\"goToDashboard\":\"Ir al dashboard\",\"username\":\"Nombre de usuario\",\"resetAccountDescription\":\"Asigna una nueva contraseña para recuperar tu cuenta\",\"saleInvoices\":\"Facturas de venta\",\"contactPhone\":\"Teléfono de contacto\",\"subscriptionCreatedErrorPlan\":\"No se encontró plan asociado a la suscripción\",\"change\":\"Cambiar\",\"mailSubscriptionChangedMessage\":\"<p>Has cambiado tu suscripción a {planName}, el pago de suscripción ha sido exitoso.</p><p>Gracias por ser parte de {appName}, Recuerda que esta suscripción hace cobros automáticos a tu tarjeta. Podrás suspender la suscripción y ver el historial de tus pagos en cualquier momento desde nuestra aplicación web.</p>\",\"total\":\"Total\",\"yourAccount\":\"Tu Cuenta\",\"filters\":\"Filtros\",\"propertyUbication\":\"Ubicación del Inmueble\",\"forgotAccountDescription\":\"Te enviaremos un correo electrónico con instrucciones para recuperar tu cuenta.\",\"withoutBilling\":\"Sin faturación\",\"providerId\":\"Provedor ID\",\"uploadErrorSize\":\"Algo anda mal, el máximo peso permitido por archivo es {maxFileSize} MB\",\"switchToPlan\":\"Cambiar a este plan\",\"transactionsDescription\":\"Lista de transacciones hechas por los usuarios\",\"expeditionDate\":\"Fecha de expedición\",\"displayName\":\"Nombre\",\"date\":\"Fecha\",\"mail\":\"<h1><img src=\\\"{appLogo}\\\" width=\\\"35\\\" style=\\\"vertical-align:middle;\\\"> {appName}</h1><hr><h1>{title}</h1><p>{message}</p><p><br></p><p><br></p><hr><p>Cordialmente,</p><p><a href=\\\"{appUrl}\\\" target=\\\"_blank\\\">{appUrl}</a></p>\",\"home\":\"Inicio\",\"forgotPassword\":\"¿Has olvidadó tu contraseña?\",\"purchaseCompleted\":\"Compra procesada correctamente\",\"homeDescription\":\"Actualidad Informativa Referente A Líderes Sociales.\",\"transactionDeclinedTitle\":\"Transacción rechazada\",\"saleTags\":\"Etiquetas de Venta\",\"planEditionDescription\":\"Complete los detalles del plan\",\"manageCompany\":\"Gestiona tu empresa\",\"taxEditionDescription\":\"Completa los detalles del impuesto\",\"id\":\"ID\",\"menu\":\"Menu\",\"day\":\"Día\",\"document\":\"Documento\",\"mailSubscriptionCreatedMessage\":\"<p>Tu pago de suscripción mensual ha sido exitoso y estará activo desde el día {startDate}.</p><p>Gracias por ser parte de {appName}, Recuerda que esta suscripción hace cobros automáticos a tu tarjeta. Podrás suspender la suscripción y ver el historial de tus pagos en cualquier momento desde nuestra aplicación web.</p>\",\"options\":\"Opciones\",\"forgotAccountTitle\":\"¿Olvidaste tu cuenta?\",\"rooms\":\"Habitaciones\",\"billing/invoiceNumber\":\"ID Facturación / Factura #\",\"expensesTitle\":\"Gastos\",\"description\":\"Descripción\",\"authNotPriviliges\":\"Tu no tienes los privilegios suficientes para acceder a está página\",\"recoverPasswordDescription\":\"Ingresa tu correo electrónico registrado, un enlace sera enviado para que puedas cambiar tu contraseña.\",\"order\":\"Orden\",\"localesDescription\":\"Lista de traducciones\",\"mailSubscriptionCanceledSubject\":\"Tu suscripción ha sido cancelada\",\"comingSoonDescription\":\"Estamos trabajando para ofrecerte un mejor servicio, regresaremos en breve.\",\"creditCardCvc\":\"últimos 3 números\",\"faq\":\"FAQ\",\"siteName\":\"Venpad\",\"catalogs\":\"Catalogos\",\"subscriptionChangePlanImmediately\":\"El cambio de plan se efectuara inmediatamente.\",\"contactTitle\":\"Contáctanos\",\"transactions\":\"Transacciones\",\"creditCardInvalidNumber\":\"Número de tarjeta de crédito invalido\",\"mailSubscriptionCanceledMessage\":\"Tu suscripción ha sido cancelada, tuvimos problemas debitando el pago en tu tarjeta de crédito, verifique los fondos de tu tarjeta de crédito o comunicate con tu banco para más información.\",\"year\":\"Año\",\"newInvoice\":\"Nueva Factura\",\"confirmation\":\"Confirmación\",\"acceptTerms\":\"Acepta terminos y condiciones\",\"planEditionTitle\":\"Edición de Plan\",\"uploadInProcess\":\"Estamos procesando el archivo de excel...\",\"creditCardInvalidCVC\":\"Invalido cvc de tarjeta de crédito\",\"transactionExpiredDescription\":\"Lo sentimos, la transacción ha expirado, puedes volver a intentalo cuando desees.\",\"multiCurrency\":\"Multi-moneda\",\"customText\":\"Texto personalizado\",\"notPlanAssociatedWithSubscription\":\"No se encontró plan asociado a la suscripción\",\"mailPostSucessMessage\":\"<p>¡Felicidades! Tu post fue publicado.</p><ul><li>Ahora puedes acceder y resaltar tu publicación a través de este <a href=\\\"{postUrl}\\\" target=\\\"_blank\\\">enlace</a>.</li><li>Gestiona tus otras publicaciones en este <a href=\\\"{postManagementUrl}\\\" target=\\\"_blank\\\">enlace</a></li></ul>\",\"subscriptionCurrent\":\"Te encuentras suscrito al ({planName}).\",\"companyTaxesTitle\":\"Taxes\",\"serverError\":\"Lo sentimos, estamos teniendo problemas temporales con el servidor\",\"lastname\":\"Apellidos\",\"documentType\":\"Tipo documento\",\"welcomeUser\":\"Bienvenido {username}!\",\"quantity\":\"Cantidad\",\"recoverPasswordSuccess\":\"Un correo electrónico ha sido enviado con instrucciones para resetear tu contraseña.\",\"privacyTitle\":\"Politicas de Privacidad\",\"loginWithFacebook\":\"Ingresar con Facebook\",\"reportDescription\":\"Analiza tu información\",\"templateForBulkUpload\":\"Plantilla para carga masiva\",\"limitedReports\":\"Reportes limitados\",\"updateCreditCard\":\"Actualizar tarjeta de crédito\",\"spendingInvoices\":\"Facturas de gasto\",\"transactionApprovedTitle\":\"Transacción aprobada\",\"loginWithEmail\":\"Ingresar con tu correo electrónico\",\"mailSubscriptionCreatedSubject\":\"Tu suscripción a {appName} ha iniciado\",\"subTotal\":\"Subtotal\",\"login\":\"Ingresar\",\"emailOrUsername\":\"Correo electrónico o nombre de usuario\",\"syncDescription\":\"Syncronizar datos externos\",\"uploadWarnings\":\"Los formatos aceptados son .jpg, .gif y .png. El tamaño máximo es de 10 MB.\",\"transactionExpiredTitle\":\"Transacción ha expirado\",\"passwordConfirm\":\"Confirmar Contraseña\",\"subscriptionSchedule\":\"Su suscripción se renovará a {planName} el {date}.\",\"expenseUploadDescription\":\"Carga masiva de gastos\",\"subscriptionNextPayment\":\"El próximo pago se realizará el día {date}.\\n\",\"onlyWithoutTaxes\":\"Solo sin impuestos\",\"seeAll\":\"Ver todo\",\"loginError\":\"Hubó un error en la autenticación via {provider}.\",\"mailContactSubject\":\"Contacto\",\"contactEmail\":\"Email de contacto\",\"billingDescription\":\"Historial de facturación\",\"billingIsNotActive\":\"La facturación seleccionada no está activa\",\"profileDescription\":\"Administra tus datos\",\"buyDescription\":\"Confirmación y método de pago\",\"createSubscriptionError\":\"Lo sentimos, hubó un error haciendo el cargo de la suscrición a tu tarjeta de crédito comunicate con tu banco para mayor información.\",\"emailSuccess\":\"El correo electrónico fue enviado correctamente\",\"userAlreadySignout\":\"Usuario ya ha sido desconectado\",\"docsDescription\":\"Todo lo que necesitas saber para empezar con Venpad\",\"subscriptionAlreadyExist\":\"Lo sentimos, ya tienes una suscripción activa.\",\"signUp\":\"Comenzar\",\"unitCost\":\"Costo Unitario\",\"invoiceSaveDescription\":\"Completa los detalles de tu factura\",\"idFromBillingSheet\":\"ID de hoja de facturación\",\"range\":\"Rango\",\"companyPerformance\":\"Rendimiento de la Empresa\",\"firstname\":\"Nombres\",\"billingHasExpired\":\"La facturación seleccionada ha expiradó\",\"userResetError\":\"No se encontrarón solicitudes para recuperación de cuenta\",\"mailSubscriptionSuspendedMessage\":\"<p>Tu suscripción ha sido cancelada. Tendrás acceso a los recursos permitidos por tu plan hasta el {expirationDate}. No te cobraremos más, hasta que decidas regresar.</p><p>Puedes reactivar tu suscripción cuando quieras. Cuando decidas volver, te estaremos esperando felices.</p>\",\"reportTitle\":\"Reportes\",\"customMessage\":\"Mensage personalizado\",\"creditCardHolder\":\"Títular de la tarjeta de crédito\",\"currencyCodeIncorrect\":\"Código de moneda incorrecto\",\"mailContactMessage\":\"<p><strong>Name:</strong> {name}</p><p><strong>Email:</strong> {email}</p><p><strong>Phone:</strong> {phone}</p><p><strong>Message:</strong> {message}</p>\",\"mainError\":\"Lo sentimos, tuvimos un problema ({error})\",\"userAlreadyHasSubscription\":\"El usuario ya se encuentra suscritó al plan seleccionado\",\"transactionPendingDescription\":\"La transacción esta siendo procesada en este momento, actualizaremos el estado de tu pago cuando tengamos respuesta de esta.\",\"currency\":\"Moneda\",\"country\":\"País\",\"terms\":\"Terminos\",\"sales\":\"Ventas\",\"usernameNotFound\":\"Nombre de usuario no existe\",\"planNotFound\":\"Lo sentimos el plan no fue encontrado o no esta disponible para comprar.\",\"addLocaleCodeForMultilanguage\":\"Puedes agregar una llave de traduccion para soportar multi-idioma\",\"userInfo\":\"Datos del Usuario\",\"fullname\":\"Nombres y apellidos\",\"resetAccountTitle\":\"Nueva Contraseña\",\"paymentDate\":\"Fecha Pago\",\"rentPrice\":\"Precio de renta\",\"contactUs\":\"Contactanos\",\"planFeatures\":\"Características del plan\",\"faqDescription\":\"Resuelve cualquier inquietud que tu tengas acerca de Venpad\",\"email\":\"Correo electrónico\",\"profile\":\"Perfil\",\"planFeatureAlreadyExist\":\"La característica ya existe en el pan\",\"income\":\"Ingresos\",\"billing\":\"Facturación\",\"securePaymentInfo\":\"Protegemos su información de pago con SSL (Secure Sockets Layer) cuando se envía a través de internet. SSL encripta la información de pago para que no pueda ser interceptada por un tercero. Venpad no guarda la información de su tarjeta de crédito.\",\"summary\":\"Resumen\",\"companyTaxEditionTitle\":\"Edición de Impuesto\",\"taxEditionTitle\":\"Edición de Impuesto\",\"termsDescription\":\"Términos y condiciones de uso de servicio de Venpad\",\"receiptsDescription\":\"Aquí los recibos de tus pagos\",\"InvoicesDescription\":\"Registras tus ingresos\",\"features\":\"Característica\",\"localeEditionDescription\":\"Complete los detalles de traducción\",\"transactionErrorTitle\":\"Transacción fallida\",\"catalogsTitle\":\"Catalogos\",\"denounce\":\"Denunciar\",\"phone\":\"Teléfono\",\"department\":\"Departamento\",\"updateSubscription\":\"Actualizar suscripción\",\"usersTitle\":\"Usuarios\",\"userNotHaveLocalPassport\":\"El usuario no tiene una estrategia de autenticación local, por favor contactenos para más información\",\"userAlreadyHasAccount\":\"¿Ya tienes una cuenta? puedes ingresar\",\"loginWithGoogle\":\"Ingresar con Google\",\"termsTitle\":\"Términos y Condiciones\",\"feature\":\"Característica\",\"website\":\"Sitio web\",\"withoutTaxes\":\"Sin Impuestos\",\"uploadPhoto\":\"Subir foto\",\"backupByYear\":\"Backup excel por año\",\"catalogNameAlreadyExist\":\"Ya existe un catalogo con ese nombre\",\"successfulOperation\":\"Operación Exitosa\",\"providers\":\"Proveedores\",\"companyTaxEditionDescription\":\"Complete los detalles del impuesto\",\"contactsTitle\":\"Tus Contactos\",\"receipts\":\"Recibos\",\"withTaxes\":\"Con impuestos\",\"onlyWithTaxes\":\"Solo con impuestos\",\"perMonth\":\"por mes\",\"usernameAlreadyExist\":\"Ya hay una cuenta con ese nombre de usuario\",\"emailConfirmed\":\"Felicitaciones tu ya has confirmado tu correo electrónico\",\"register\":\"Registrar\",\"registerTitle\":\"Crear una cuenta\",\"wrongAssociatedID\":\"No se encontró registro asociado con ese código/id\",\"welcomeToVenpadMessage\":\"Nos complace tenerte en Venpad {username}, este es el primer paso para mejorar tu vida financiera, estamos trabajando para mejorar nuestro sistema día a día y brindarte las mejores estrategias para ayudarte en tus finanzas.\",\"postTitle\":\"Publicar\",\"mostPopular\":\"Más Popular!\",\"plan\":\"Plan\",\"configurationEditionTitle\":\"Edición de configuración\",\"invoicesDescription\":\"Registra tus facturas de venta\",\"securePayment\":\"Pago seguro a través de PayU\",\"select\":\"Seleccione\",\"invoicesTitle\":\"Ingresos\",\"subscriptionNoPaymentMethods\":\"Para cambiar su suscripción, necesita crear una tarjeta de crédito.\",\"deleteExpenseConfirmation\":\"¿Estás seguro de eliminar este gasto?\",\"ubication\":\"Ubicación\",\"uploadExcel\":\"Cargar Excel\",\"recoverPasswordTitle\":\"Recuperar Contraseña\",\"userValidateEmailSuccess\":\"Gracias su email fue confirmado exitosamente\",\"userEditionTitle\":\"Edición de usuario\",\"logout\":\"Cerrar Sesión\",\"post\":\"Publicar\",\"noSummaryYet\":\"No has agregado nada aún\",\"authStrategyFailed\":\"Lo sentimos, no se econtrarón usuarios con esa estrategia de authenticación.\",\"invoices\":\"Facturas\",\"auth/user-not-found\":\"Usuario no fue encontradó\",\"excelReport\":\"Reporte Excel\",\"invoicesUploadDescription\":\"Carga masiva de facturas\",\"notFoundDescription\":\"El enlace que seguiste puede estar roto, o la pagina ha sido removida\",\"invoicesUploadTitle\":\"Subir Facturas\",\"incorrectProperties\":\"Las siguientes propiedades son requeridas {properties}\",\"unauthorized\":\"No autorizado\",\"haveNotAccount\":\"¿Aún no has creado tu cuenta?\",\"admin\":\"Admon\",\"expenseUploadTitle\":\"Subir Gastos\",\"expensesDescription\":\"Registra tus facturas de gasto\",\"creditCardExpired\":\"Tarjeta de crédito expirada\",\"unlimitedReports\":\"Reportes ilimitados\",\"syncTitle\":\"Syncronización\",\"synchronization\":\"Syncronización\",\"profit\":\"Utilidad\",\"creditCardUpdatedSuccessfully\":\"La tarjeta de credito fue actualizada correctamente.\",\"userNotHaveNotifications\":\"No tienes nuevas notificaciones\",\"buyTitle\":\"Comprar {planName}\",\"here\":\"Aquí\",\"localesTitle\":\"Traducciones\",\"contactDescription\":\"Tu puedes contactarnos en cualquier momento, a través de cualquier de nuestros canales:\",\"docsTitle\":\"Documentación\",\"disableApp\":\"Deshabilotar Aplicación\",\"month\":\"Mes\",\"notFoundTitle\":\"Está página no esta disponible\",\"emailError\":\"Ocurrio un error enviando correo electrónico\",\"creditCardNumber\":\"Número de la tarjeta de crédito\",\"unlimitedExpenses\":\"Gastos ilimitados\",\"allIncludedTaxes\":\"Todo incluido impuestos\",\"emailUnconfirmed\":\"Tu no has confirmado tu correo electrónico, revisa tu correo y sigue las instrucciones\",\"incorrectFormat\":\"Formato incorrecto\",\"invoiceNumber\":\"Factura #\",\"billingIdHelp\":\"ID Facturación = Copia el id de la hoja de facturación\",\"passwordIncorrect\":\"La contraseña es incorrecta\",\"loginTitle\":\"Ingresar\",\"companyEditionTitle\":\"Tu Empresa\",\"uploadFileToProcess\":\"Sube un archivo para procesar\",\"publish\":\"Publicar\",\"contacts\":\"Contactos\",\"neighborhood\":\"Barrio\",\"requiredFields\":\"Los campos marcados con (*) son obligatorios\",\"catalogEditionTitle\":\"Edición de Catalogo\",\"newExpense\":\"Nuevo Gasto\",\"permission\":\"Permiso\",\"complaint\":\"Denuncia\",\"notEmpty\":\"No puede ser vacio\",\"faqTitle\":\"Preguntas Frecuentes\",\"uploadErrorExtentions\":\"Algo anda mal, solo los siguientes formatos son soportados: {extentions}\",\"expenses\":\"Gastos\",\"companyTaxesDescription\":\"Lista de impuestos\",\"name\":\"Nombre\",\"contact\":\"Contacto\",\"search\":\"Buscar\",\"administration\":\"Administración\",\"users\":\"Usuarios\",\"localesRefreshed\":\"Las traducciones fuerón actualizadas\",\"mailWelcomeUserMessage\":\"<p>Estamos felices de darte la bienvenida a Venpad, para empezar, a continuación la información de tu cuenta:</p><p><strong>Url de ingreso: </strong><a href=\\\"{urlLogin}\\\" target=\\\"_blank\\\" style=\\\"background-color: rgb(255, 255, 255);\\\">{urlLogin}</a></p><p><strong>Nombre de usuario:</strong> {username}</p><p><strong>Contraseña: </strong>{password}</p><p>Para confirmar tu correo electrónico da clic en el siguiente enlace <a href=\\\"{urlConfirmEmail}\\\" target=\\\"_blank\\\" style=\\\"background-color: rgb(255, 255, 255);\\\">{urlConfirmEmail}</a>.</p>\",\"catalogEditionDescription\":\"Complete los detalles del catalogo\",\"getStarted\":\"Empezar\",\"deleteItem\":\"¿Estás seguro de eliminar este item?\",\"mailSubscriptionSuspendedSubject\":\"<p>Tu suscripción a {appName} ha sido suspendida</p>\",\"selectBilling\":\"Seleccione facturación\",\"loginDescription\":\"Ingresa rápidamente con tu correo electrónico o redes sociales.\",\"formErrors\":\"Hay errores en el formulario, por favor revise los campos marcados con color rojo.\",\"status\":\"Estado\",\"getStartedNow\":\"Comenzar ahora\",\"profileTitle\":\"Perfil\",\"userNotHaveMessages\":\"No tienes mensajes nuevos\",\"usersDescription\":\"Lista de usuarios registrados\",\"invoiceEditionDescription\":\"Completa los detalles de tu factura\",\"sync\":\"Sync\",\"loginWithSocialNetworks\":\"Ingresar con tus redes sociales\",\"transactionNotFound\":\"No fue posible actualizar el estado de la transacción, la transacción no fue encontrada.\",\"expenseTags\":\"Etiquetas de Gasto\",\"propertyType\":\"Tipo de inmueble\",\"privacyDescription\":\"Abajo te explicamos como Venpad maneja tus datos registrados\",\"savePassword\":\"Guardar Contraseña\",\"referenceId\":\"Número de referencia\",\"send\":\"Enviar\",\"errors\":\"Errores\",\"localeEditionTitle\":\"Edición de traducción\",\"priceTitle\":\"Precios\",\"value\":\"Valor\",\"apiIntegration\":\"Integración con API\",\"dashboardTitle\":\"Denuncia\",\"offerType\":\"Tipo de oferta\",\"userResetPasswordEmpty\":\"La nueva contraseña no puede ser vacia\",\"planNameAlreadyExist\":\"Ya hay un plan con ese nombre\",\"thereIsNotDataToChart\":\"No hay datos para gráficar\",\"developersDescription\":\"Documentación de API para desarrolladores\",\"mailWelcomeUserSubject\":\"Bienvenido\",\"amount\":\"Importe\",\"cancel\":\"Cancelar\",\"expirationDate\":\"Fecha de expiración\",\"subscriptionDescription\":\"Cancelar, suspender o actualizar tu plan de suscripción\",\"taxesDescription\":\"Configura tus impuestos\",\"dragProfilePicture\":\"Arrastra aquí tu imagen de perfil o sube una foto\",\"photo\":\"Foto\",\"months\":\"Enero, Febrero, Marzo, Abril, Mayo, Junio, Julio, Agosto, Septiembre, Octubre, Noviembre, Diciembre\",\"address\":\"Dirección\",\"mustInclude\":\"Debe incluir alguno de los siguientes valores {values}\",\"taxes\":\"Impuestos\",\"documentNumber\":\"Número documento\",\"buy\":\"Comprar\",\"invalidDataType\":\"Invalido tipo de dato, este debe ser {dataType}\",\"excelBackup\":\"Backup en excel\",\"tags\":\"Etiquetas\",\"backup\":\"Copia de respaldo\",\"priceDescription\":\"Elije un plan y empieza a facturar\",\"privacy\":\"Privacidad\",\"try\":\"Probar\",\"noApiIntegration\":\"Sin integración con API\",\"addItem\":\"Agregar articulo\",\"auth/wrong-password\":\"La contraseña es incorrecta\",\"sendEmail\":\"Enviar correo electrónico\",\"subscriptionNextPlan\":\"Proximo a cambiar a {planName} \",\"contactEditionTitle\":\"Edición de Contacto\",\"unlimitedInvoices\":\"Facturas ilimitadas\",\"plansDescription\":\"Lista de planes\",\"contactsDescription\":\"Registra tus clientes y proveedores\",\"mustAcceptTerms\":\"Debes aceptar los términos y condiciones\",\"idFromClientSheet\":\"ID de hoja de clientes\",\"comingSoonTitle\":\"Proximamente!\",\"expirationDateInvalid\":\"Fecha de expiración invalida\",\"bulkUpload\":\"Carga masiva\",\"locales\":\"Locales\",\"reports\":\"Reportes\",\"preferences\":\"Preferencias\",\"city\":\"Ciudad\",\"auth/email-already-in-use\":\"Correo electrónico ya en uso\",\"dateFormat\":\"Formato de fecha\",\"billingEditionTitle\":\"Edición de facturación\",\"subscriptionCreatedErrorCard\":\"Tu tarjeta ha sido rechazada, comunícate con tu banco para obtener más información.\",\"client\":\"Cliente\",\"support\":\"Suporte\",\"mailForgotAccountMessage\":\"<p>Has solictado un re-establecimiento de tu contraseña, puedes hacer esto siguiendo el enlace <a href=\\\"{url}\\\" target=\\\"_blank\\\">{url}</a>, recuerda que este enlace es valido solo por 1 hora.</p><p>Si tu no lo has solicitado, por favor ignora este correo.</p>\",\"all\":\"Todos\",\"cancelInvoiceConfirmation\":\"¿Estas seguro de cancelar esta factura?\",\"syncIntlData\":\"Syncronizar datos internacionles\",\"propertyInfo\":\"Datos del Inmueble \",\"plansTitle\":\"Planes\",\"dashboardDescription\":\"Bienvenido a Tu Formulario de Denuncia\",\"password\":\"Contraseña\",\"fieldIsRequired\":\"Este campo es requerido\",\"salePrice\":\"Precio de venta\",\"auth/invalid-email\":\"Correo electrónico ya en uso\",\"notMatch\":\"No coincide\",\"userEdition\":\"Edición de usuario\",\"photos\":\"Fotos\",\"dragPhotos\":\"Aqui, arrastra o sube tus fotos.\",\"localeNameAlreadyExist\":\"Ya hay un literal con ese nombre\",\"securityCode\":\"Código de seguridad\",\"transactionPendingTitle\":\"Transacción pendiente\",\"transactionApprovedDescription\":\"La transacción ha sido aprobada correctamente, \",\"transactionsTitle\":\"Transacciones\",\"save\":\"Guardar\",\"creditCardInvalidExpirationDate\":\"´The credit card expiration date is invalid\",\"results\":\"Resultados\",\"userInactive\":\"Tu cuenta ha sido inactivada, por favor comunicate con nosotros para más información\",\"notifications\":\"Notificaciones\",\"currencyAtRecurrence\":\"{currency} por {recurrence}\",\"postDescription\":\"Fácil y rápido\",\"price\":\"Precio\",\"subscriptionDeletedSuccess\":\"Se eliminó la suscripción correctamente\",\"receiptsTitle\":\"Recibos\",\"auth/argument-error\":\"Token invalido\",\"mailTransactionSuccessSubject\":\"Transacción exitosa\",\"invoiceEditionTitle\":\"Edición de factura\",\"registerWithEmail\":\"Registrase con correo electrónico\",\"clients\":\"Clientes\",\"thereIsNotData\":\"No hay datos para mostrar en este momento\",\"saveUserClaim\":\"Guardar Permiso\",\"userClaims\":\"Permisos de usuario\",\"billingExceedRangeOfInvoices\":\"La faturación superá el rango maximo permitido\",\"allBilling\":\"Toda la facturación\",\"invalidDateFormat\":\"Fecha con formato invalido\",\"subscriptionCanceled\":\"Tu suscripción ha sido cancelada, dejaremos de hacer cargos a tu tarjeta de crédito\",\"paymentMethod\":\"Metodo de pago\",\"userValidateEmailError\":\"El correo electrónico no pudó ser confirmado, el usuario no fue encontrado\",\"mailTransactionSuccessMessage\":\"Felicitaciones!, el pago de {itemDescription} fue procesado correctamente.\",\"registerDescription\":\"Crear una cuenta es fácil y rapido, con tu correo electrónico o redes sociales.\",\"transactionDeclinedDescription\":\"Lo sentimos, la transacción ha sido rechazada, verifica tus datos he intenta nuevamente.\",\"loadMore\":\"Cargar más\",\"expenseEditionDescription\":\"Completa los detalles del gasto\",\"subscriptionWarnings\":\"<p>Entiendo que mi suscripción hará los cobros automáticos cada mes.</p><p>Entiendo que puedo suspender mi suscripción cuando quiera.</p>\",\"subscriptionCreatedSuccess\":\"Se realizó la compra correctamente\",\"includeTaxes\":\"Incluir impuestos\",\"help\":\"Ayuda\",\"subscriptionCreatedError\":\"Lo sentimos, no se pudo ejecutar la transacción en este momento, por favor intente más tarde\",\"expenseEditionTitle\":\"Edición de gasto\",\"allMonths\":\"Todos los meses\",\"registerWithSocialNetworks\":\"Registrarse con redes sociales\",\"plans\":\"Planes\",\"updateExpense\":\"Actualizar Gasto\",\"selectYear\":\"Seleccion año\",\"deleteConfirmation\":\"¿Estás seguro de borrar esto?\",\"authStrategyNotProvided\":\"No se especificó strategia de autenticación.\",\"updateCreditCardSuccessful\":\"La tarjeta de crédito fue actualizada correctamente.\",\"userEmailAlreadyExist\":\"Ya hay una cuenta con ese correo electrónico \",\"authNotLogin\":\"Tienes que iniciar sesión para acceder a está página\",\"errorFileUpload\":\"Hubo errores procesando el archivo, por favor corrija los siguientes errores e intente nuevamente\",\"authStrategyNotAvailable\":\"La estrategia de autenticación no esta disponible.\",\"mailSubscriptionChangedSubject\":\"Tu suscripción ha cambiado\",\"recoverPassword\":\"Recuperar Contraseña\",\"subscriptionPlanFree\":\"Con esta opción, pausará o suspenderá los pagos de su suscripción. Tendrá acceso limitado a los servicios.\\n\",\"updateInvoice\":\"Actualizar Factura\",\"docs\":\"Documentos\",\"billingId\":\"Facturación ID\",\"subscriptionTitle\":\"Administrar suscripción\",\"userResetSuccess\":\"La contraseña fue actualizada exitosamente, ahora ya puedes acceder a tu cuenta con tu nueva contraseña\",\"goHomePage\":\"Ir a página principal\",\"homeTitle\":\"Noticias\",\"developersTitle\":\"Desarrolladores\",\"sessionFinished\":\"Tu sesión ha sido finalizada, ahora puedes abandonar el dispositivo tranquilamente\",\"developers\":\"Desarrolladores\",\"expenseInvoices\":\"Facturas de gasto\",\"processedDataComplete\":\"{numData} archivo(s) fuerón procesados correctamente\"}}}}");

/***/ }),

/***/ "./src/store/actions/catalog-actions.js":
/*!**********************************************!*\
  !*** ./src/store/actions/catalog-actions.js ***!
  \**********************************************/
/*! exports provided: TYPES, getCatalogs, getCatalog, createCatalog, updateCatalog */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TYPES", function() { return TYPES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getCatalogs", function() { return getCatalogs; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getCatalog", function() { return getCatalog; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createCatalog", function() { return createCatalog; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "updateCatalog", function() { return updateCatalog; });
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "axios");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _utils_request_utils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../utils/request-utils */ "./src/utils/request-utils.js");


const {
  API_URL
} = {"APP_URL":"http://localhost:8080","API_URL":"http://localhost:8080/api","NODE_ENV":"development"};
const TYPES = {
  GET_CATALOGS: 'GET_CATALOGS',
  GET_CATALOG: 'GET_CATALOG',
  POST_CATALOG: 'POST_CATALOG',
  PATCH_CATALOG: 'PATCH_CATALOG'
};
const getCatalogs = query => async (dispatch, getState) => {
  const {
    catalog
  } = getState();
  const response = catalog.records.length ? catalog.records : (await axios__WEBPACK_IMPORTED_MODULE_0___default.a.get(`${API_URL}/catalogs/${Object(_utils_request_utils__WEBPACK_IMPORTED_MODULE_1__["getQueryParamsString"])(query)}`)).data;
  dispatch({
    type: TYPES.GET_CATALOGS,
    payload: response
  });
};
const getCatalog = id => async dispatch => {
  const response = await axios__WEBPACK_IMPORTED_MODULE_0___default.a.get(`${API_URL}/catalogs/${id}`);
  dispatch({
    type: TYPES.GET_CATALOG,
    payload: response.data
  });
};
const createCatalog = data => async dispatch => {
  const response = await axios__WEBPACK_IMPORTED_MODULE_0___default.a.post(`${API_URL}/catalogs`, data);
  dispatch({
    type: TYPES.POST_CATALOG,
    payload: response.data
  });
};
const updateCatalog = data => async dispatch => {
  const response = await axios__WEBPACK_IMPORTED_MODULE_0___default.a.patch(`${API_URL}/catalogs/${data.id}`, data);
  dispatch({
    type: TYPES.PATCH_CATALOG,
    payload: response.data
  });
};

/***/ }),

/***/ "./src/store/actions/config-actions.js":
/*!*********************************************!*\
  !*** ./src/store/actions/config-actions.js ***!
  \*********************************************/
/*! exports provided: TYPES, getCurrencyConversions, setConfiguration, setCurrencyConversions, setPreferences, syncConfiguration */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TYPES", function() { return TYPES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getCurrencyConversions", function() { return getCurrencyConversions; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setConfiguration", function() { return setConfiguration; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setCurrencyConversions", function() { return setCurrencyConversions; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setPreferences", function() { return setPreferences; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "syncConfiguration", function() { return syncConfiguration; });
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "axios");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);

const {
  API_URL
} = {"APP_URL":"http://localhost:8080","API_URL":"http://localhost:8080/api","NODE_ENV":"development"};
const TYPES = {
  GET_CURRENCY_CONVERSIONS: 'GET_CURRENCY_CONVERSIONS',
  SET_CONFIGURATION: 'SET_CONFIGURATION',
  SET_CURRENCY_CONVERSIONS: 'SET_CURRENCY_CONVERSIONS',
  SET_PREFERENCES: 'SET_PREFERENCES'
};
const getCurrencyConversions = data => async dispatch => {
  const {
    currency,
    date
  } = data;
  const response = await axios__WEBPACK_IMPORTED_MODULE_0___default.a.get(`${API_URL}/config/currency-conversions/${currency}/${date}`);
  dispatch({
    type: TYPES.GET_CURRENCY_CONVERSIONS,
    payload: response.data
  });
};
const setConfiguration = data => async dispatch => {
  dispatch({
    type: TYPES.SET_CONFIGURATION,
    payload: data
  });
};
const setCurrencyConversions = data => async dispatch => {
  dispatch({
    type: TYPES.SET_CURRENCY_CONVERSIONS,
    payload: data
  });
};
const setPreferences = data => async (dispatch, getState) => {
  const response = Object.assign(getState().config.preferences, data);
  dispatch({
    type: TYPES.SET_PREFERENCES,
    payload: response
  });
};
const syncConfiguration = data => async dispatch => {
  const response = await axios__WEBPACK_IMPORTED_MODULE_0___default.a.post(`${API_URL}/config/sync`, data);
  dispatch({
    type: TYPES.SET_CONFIGURATION,
    payload: response.data
  });
};

/***/ }),

/***/ "./src/store/actions/home-actions.js":
/*!*******************************************!*\
  !*** ./src/store/actions/home-actions.js ***!
  \*******************************************/
/*! exports provided: TYPES, getDevelopersInfo, getFaqInfo, getPlanFeatures, getPrivacyInfo, getTermsInfo */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TYPES", function() { return TYPES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getDevelopersInfo", function() { return getDevelopersInfo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getFaqInfo", function() { return getFaqInfo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getPlanFeatures", function() { return getPlanFeatures; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getPrivacyInfo", function() { return getPrivacyInfo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getTermsInfo", function() { return getTermsInfo; });
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "axios");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _utils_request_utils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../utils/request-utils */ "./src/utils/request-utils.js");


const {
  API_URL
} = {"APP_URL":"http://localhost:8080","API_URL":"http://localhost:8080/api","NODE_ENV":"development"};
const TYPES = {
  GET_DEVELOPERS_INFO: 'GET_DEVELOPERS_INFO',
  GET_FAQ_INFO: 'GET_FAQ_INFO',
  GET_PRIVACY_INFO: 'GET_PRIVACY_INFO',
  GET_PLAN_FEATURES: 'GET_PLAN_FEATURES',
  GET_TERMS_INFO: 'GET_TERMS_INFO'
};
const getDevelopersInfo = () => async (dispatch, getState) => {
  const {
    config,
    home
  } = getState();
  const query = {
    sort: {
      order: 'asc'
    },
    where: {
      parent: {
        '==': config.catalogs.developersDocs
      }
    }
  };
  const response = home.developersInfo.length ? home.developersInfo : (await axios__WEBPACK_IMPORTED_MODULE_0___default.a.get(`${API_URL}/catalogs/${Object(_utils_request_utils__WEBPACK_IMPORTED_MODULE_1__["getQueryParamsString"])(query)}`)).data;
  dispatch({
    type: TYPES.GET_DEVELOPERS_INFO,
    payload: response
  });
};
const getFaqInfo = () => async (dispatch, getState) => {
  const {
    config,
    home
  } = getState();
  const response = Object.keys(home.faqInfo).length ? home.faqInfo : (await axios__WEBPACK_IMPORTED_MODULE_0___default.a.get(`${API_URL}/catalogs/${config.catalogs.faqInfo}`)).data;
  dispatch({
    type: TYPES.GET_FAQ_INFO,
    payload: response
  });
};
const getPlanFeatures = () => async (dispatch, getState) => {
  const {
    config,
    home
  } = getState();
  const query = {
    sort: {
      order: 'asc'
    },
    where: {
      parent: {
        '==': config.catalogs.planFeatures
      }
    }
  };
  const response = home.planFeatures.length ? home.planFeatures : (await axios__WEBPACK_IMPORTED_MODULE_0___default.a.get(`${API_URL}/catalogs/${Object(_utils_request_utils__WEBPACK_IMPORTED_MODULE_1__["getQueryParamsString"])(query)}`)).data;
  dispatch({
    type: TYPES.GET_PLAN_FEATURES,
    payload: response
  });
};
const getPrivacyInfo = () => async (dispatch, getState) => {
  const {
    config,
    home
  } = getState();
  const response = Object.keys(home.privacyInfo).length ? home.privacyInfo : (await axios__WEBPACK_IMPORTED_MODULE_0___default.a.get(`${API_URL}/catalogs/${config.catalogs.privacyInfo}`)).data;
  dispatch({
    type: TYPES.GET_PRIVACY_INFO,
    payload: response
  });
};
const getTermsInfo = () => async (dispatch, getState) => {
  const {
    config,
    home
  } = getState();
  const response = Object.keys(home.termsInfo).length ? home.termsInfo : (await axios__WEBPACK_IMPORTED_MODULE_0___default.a.get(`${API_URL}/catalogs/${config.catalogs.termsInfo}`)).data;
  dispatch({
    type: TYPES.GET_TERMS_INFO,
    payload: response
  });
};

/***/ }),

/***/ "./src/store/actions/locale-actions.js":
/*!*********************************************!*\
  !*** ./src/store/actions/locale-actions.js ***!
  \*********************************************/
/*! exports provided: TYPES, getLocales, getLocale, createLocale, updateLocale, deleteLocale */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TYPES", function() { return TYPES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getLocales", function() { return getLocales; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getLocale", function() { return getLocale; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createLocale", function() { return createLocale; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "updateLocale", function() { return updateLocale; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "deleteLocale", function() { return deleteLocale; });
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "axios");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _utils_request_utils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../utils/request-utils */ "./src/utils/request-utils.js");


const {
  API_URL
} = {"APP_URL":"http://localhost:8080","API_URL":"http://localhost:8080/api","NODE_ENV":"development"};
const TYPES = {
  GET_LOCALES: 'GET_LOCALES',
  GET_LOCALE: 'GET_LOCALE',
  POST_LOCALE: 'POST_LOCALE',
  PATCH_LOCALE: 'PATCH_LOCALE',
  DELETE_LOCALE: 'DELETE_LOCALE'
};
const getLocales = query => async (dispatch, getState) => {
  const {
    locale
  } = getState();
  const response = locale.records.length ? locale.records : (await axios__WEBPACK_IMPORTED_MODULE_0___default.a.get(`${API_URL}/locales/${Object(_utils_request_utils__WEBPACK_IMPORTED_MODULE_1__["getQueryParamsString"])(query)}`)).data;
  dispatch({
    type: TYPES.GET_LOCALES,
    payload: response
  });
};
const getLocale = id => async dispatch => {
  const response = await axios__WEBPACK_IMPORTED_MODULE_0___default.a.get(`${API_URL}/locales/${id}`);
  dispatch({
    type: TYPES.GET_LOCALE,
    payload: response.data
  });
};
const createLocale = data => async dispatch => {
  const response = await axios__WEBPACK_IMPORTED_MODULE_0___default.a.post(`${API_URL}/locales`, data);
  dispatch({
    type: TYPES.POST_LOCALE,
    payload: response.data
  });
};
const updateLocale = data => async dispatch => {
  const response = await axios__WEBPACK_IMPORTED_MODULE_0___default.a.patch(`${API_URL}/locales/${data.id}`, data);
  dispatch({
    type: TYPES.PATCH_LOCALE,
    payload: response.data
  });
};
const deleteLocale = id => async dispatch => {
  const response = await axios__WEBPACK_IMPORTED_MODULE_0___default.a.delete(`${API_URL}/locales/${id}`);
  dispatch({
    type: TYPES.DELETE_LOCALE,
    payload: response.data
  });
};

/***/ }),

/***/ "./src/store/actions/user-actions.js":
/*!*******************************************!*\
  !*** ./src/store/actions/user-actions.js ***!
  \*******************************************/
/*! exports provided: TYPES, getBackupExcel, getUsers, getUser, getCustomerInfo, getCustomerReceipts, updateUser, changePassword, login, logout, register, me, recoverPassword, setToken, getUserClaims, createUserClaim, deleteUserClaim, updateCreditCard, createSubscription, updateSubscription, createReport */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TYPES", function() { return TYPES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getBackupExcel", function() { return getBackupExcel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getUsers", function() { return getUsers; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getUser", function() { return getUser; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getCustomerInfo", function() { return getCustomerInfo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getCustomerReceipts", function() { return getCustomerReceipts; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "updateUser", function() { return updateUser; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "changePassword", function() { return changePassword; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "login", function() { return login; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "logout", function() { return logout; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "register", function() { return register; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "me", function() { return me; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "recoverPassword", function() { return recoverPassword; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setToken", function() { return setToken; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getUserClaims", function() { return getUserClaims; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createUserClaim", function() { return createUserClaim; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "deleteUserClaim", function() { return deleteUserClaim; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "updateCreditCard", function() { return updateCreditCard; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createSubscription", function() { return createSubscription; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "updateSubscription", function() { return updateSubscription; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createReport", function() { return createReport; });
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "axios");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _utils_request_utils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../utils/request-utils */ "./src/utils/request-utils.js");


const {
  API_URL
} = {"APP_URL":"http://localhost:8080","API_URL":"http://localhost:8080/api","NODE_ENV":"development"};
const TYPES = {
  GET_BACKUP: 'GET_BACKUP',
  GET_USERS: 'GET_USERS',
  GET_USER: 'GET_USER',
  GET_CUSTOMER_INFO: 'GET_CUSTOMER_INFO',
  GET_CUSTOMER_RECEIPTS: 'GET_CUSTOMER_RECEIPTS',
  PATCH_USER: 'PATCH_USER',
  CHANGE_PASSWORD: 'CHANGE_PASSWORD',
  LOGIN: 'LOGIN',
  LOGOUT: 'LOGOUT',
  REGISTER: 'REGISTER',
  ME: 'ME',
  RECOVER_PASSWORD: 'RECOVER_PASSWORD',
  SET_TOKEN: 'SET_TOKEN',
  GET_USER_CLAIMS: 'GET_USER_CLAIMS',
  POST_USER_CLAIM: 'POST_USER_CLAIM',
  DELETE_USER_CLAIM: 'DELETE_USER_CLAIM',
  UPDATE_CREDIT_CARD: 'UPDATE_CREDIT_CARD',
  CREATE_SUBSCRIPTION: 'CREATE_SUBSCRIPTION',
  UPDATE_SUBSCRIPTION: 'UPDATE_SUBSCRIPTION',
  CREATE_REPORT: 'CREATE_REPORT'
};
const getBackupExcel = year => async (dispatch, getState) => {
  const {
    user
  } = getState();
  const response = user.backup ? user.backup : (await axios__WEBPACK_IMPORTED_MODULE_0___default.a.get(`${API_URL}/users/backup/${year}`, {
    responseType: 'arraybuffer'
  })).data;
  dispatch({
    type: TYPES.GET_BACKUP,
    payload: response
  });
};
const getUsers = query => async (dispatch, getState) => {
  const {
    user
  } = getState();
  const response = user.records.length ? user.records : (await axios__WEBPACK_IMPORTED_MODULE_0___default.a.get(`${API_URL}/users/${Object(_utils_request_utils__WEBPACK_IMPORTED_MODULE_1__["getQueryParamsString"])(query)}`)).data;
  dispatch({
    type: TYPES.GET_USERS,
    payload: response
  });
};
const getUser = id => async dispatch => {
  const response = await axios__WEBPACK_IMPORTED_MODULE_0___default.a.get(`${API_URL}/users/${id}`);
  dispatch({
    type: TYPES.GET_USER,
    payload: response.data
  });
};
const getCustomerInfo = userId => async dispatch => {
  const response = (await axios__WEBPACK_IMPORTED_MODULE_0___default.a.get(`${API_URL}/users/${userId}/info`)).data;
  dispatch({
    type: TYPES.GET_CUSTOMER_INFO,
    payload: response
  });
};
const getCustomerReceipts = (id, query) => async (dispatch, getState) => {
  const {
    user
  } = getState();
  const response = user.customerReceipts.length > 0 ? user.customerReceipts : (await axios__WEBPACK_IMPORTED_MODULE_0___default.a.get(`${API_URL}/users/${id}/receipts/${Object(_utils_request_utils__WEBPACK_IMPORTED_MODULE_1__["getQueryParamsString"])(query)}`)).data;
  dispatch({
    type: TYPES.GET_CUSTOMER_RECEIPTS,
    payload: response
  });
};
const updateUser = data => async dispatch => {
  const response = await axios__WEBPACK_IMPORTED_MODULE_0___default.a.patch(`${API_URL}/users/${data.id}`, data);
  dispatch({
    type: TYPES.PATCH_USER,
    payload: response.data
  });
};
const changePassword = data => async dispatch => {
  const response = await axios__WEBPACK_IMPORTED_MODULE_0___default.a.post(`${API_URL}/users/change-password`, data);
  dispatch({
    type: TYPES.CHANGE_PASSWORD,
    payload: response.data.messsage
  });
};
const login = data => async dispatch => {
  const response = await axios__WEBPACK_IMPORTED_MODULE_0___default.a.post(`${API_URL}/users/login`, data);
  dispatch({
    type: TYPES.LOGIN,
    payload: response.data
  });
};
const logout = () => async dispatch => {
  dispatch({
    type: TYPES.LOGOUT,
    payload: null
  });
};
const register = data => async dispatch => {
  const response = await axios__WEBPACK_IMPORTED_MODULE_0___default.a.post(`${API_URL}/users/register`, data);
  dispatch({
    type: TYPES.REGISTER,
    payload: response.data
  });
};
const me = () => async dispatch => {
  try {
    let response = await axios__WEBPACK_IMPORTED_MODULE_0___default.a.get(`${API_URL}/users/me`);
    dispatch({
      type: TYPES.ME,
      payload: response.data
    });
  } catch (e) {
    dispatch({
      type: TYPES.ME,
      payload: null
    });
  }
};
const recoverPassword = data => async dispatch => {
  const response = await axios__WEBPACK_IMPORTED_MODULE_0___default.a.post(`${API_URL}/users/recover-password`, data);
  dispatch({
    type: TYPES.RECOVER_PASSWORD,
    payload: response.data
  });
};
const setToken = token => async dispatch => {
  localStorage.token = token;
  dispatch({
    type: TYPES.SET_TOKEN,
    payload: token
  });
};
const getUserClaims = id => async dispatch => {
  const response = await axios__WEBPACK_IMPORTED_MODULE_0___default.a.get(`${API_URL}/users/${id}/claims`);
  dispatch({
    type: TYPES.GET_USER_CLAIMS,
    payload: response.data
  });
};
const createUserClaim = data => async dispatch => {
  const response = await axios__WEBPACK_IMPORTED_MODULE_0___default.a.post(`${API_URL}/users/${data.id}/claims`, data);
  dispatch({
    type: TYPES.POST_USER_CLAIM,
    payload: response.data
  });
};
const deleteUserClaim = data => async dispatch => {
  const response = await axios__WEBPACK_IMPORTED_MODULE_0___default.a.patch(`${API_URL}/users/${data.id}/claims`, data);
  dispatch({
    type: TYPES.DELETE_USER_CLAIM,
    payload: response.data
  });
};
const updateCreditCard = (customerCode, creditCard) => async dispatch => {
  const response = await axios__WEBPACK_IMPORTED_MODULE_0___default.a.post(`${API_URL}/users/${customerCode}/update-credit-card`, creditCard);
  dispatch({
    type: TYPES.UPDATE_CREDIT_CARD,
    payload: response.data
  });
};
const createSubscription = data => async dispatch => {
  let response = await axios__WEBPACK_IMPORTED_MODULE_0___default.a.post(`${API_URL}/users/create-subscription`, data);
  dispatch({
    type: TYPES.CREATE_SUBSCRIPTION,
    payload: response.data
  });
};
const updateSubscription = (customerCode, data) => async dispatch => {
  const response = await axios__WEBPACK_IMPORTED_MODULE_0___default.a.post(`${API_URL}/users/${customerCode}/update-subscription`, data);
  dispatch({
    type: TYPES.UPDATE_CREDIT_CARD,
    payload: response.data
  });
};
const createReport = data => async dispatch => {
  let response = await axios__WEBPACK_IMPORTED_MODULE_0___default.a.post(`${API_URL}/socialLeader/report`, data);
  dispatch({
    type: TYPES.CREATE_REPORT,
    payload: response.data
  });
};

/***/ }),

/***/ "./src/store/middlewares/request-middleware.js":
/*!*****************************************************!*\
  !*** ./src/store/middlewares/request-middleware.js ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "axios");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);


const request = store => next => action => {
  const {
    config,
    user
  } = store.getState();
  const {
    preferences
  } = config;
  delete axios__WEBPACK_IMPORTED_MODULE_0___default.a.defaults.headers.common.authorization;

  if (user.token) {
    axios__WEBPACK_IMPORTED_MODULE_0___default.a.defaults.headers.common['authorization'] = `Bearer ${user.token}`;
  }

  if (preferences.currency) {
    axios__WEBPACK_IMPORTED_MODULE_0___default.a.defaults.headers.common['accept-currency'] = preferences.currency;
  }

  if (preferences.language) {
    axios__WEBPACK_IMPORTED_MODULE_0___default.a.defaults.headers.common['accept-language'] = preferences.language;
  }

  return next(action);
};

/* harmony default export */ __webpack_exports__["default"] = (request);

/***/ }),

/***/ "./src/store/middlewares/thunk-middleware.js":
/*!***************************************************!*\
  !*** ./src/store/middlewares/thunk-middleware.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var redux_thunk__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! redux-thunk */ "redux-thunk");
/* harmony import */ var redux_thunk__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(redux_thunk__WEBPACK_IMPORTED_MODULE_0__);

/* harmony default export */ __webpack_exports__["default"] = (redux_thunk__WEBPACK_IMPORTED_MODULE_0___default.a);

/***/ }),

/***/ "./src/store/reducers/catalog-reducer.js":
/*!***********************************************!*\
  !*** ./src/store/reducers/catalog-reducer.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return reducer; });
/* harmony import */ var _actions_catalog_actions__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../actions/catalog-actions */ "./src/store/actions/catalog-actions.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const initialState = {
  records: [],
  temp: null
};
function reducer(state = initialState, action) {
  switch (action.type) {
    default:
      return state;

    case _actions_catalog_actions__WEBPACK_IMPORTED_MODULE_0__["TYPES"].GET_CATALOGS:
      return _objectSpread({}, state, {
        records: action.payload
      });

    case _actions_catalog_actions__WEBPACK_IMPORTED_MODULE_0__["TYPES"].GET_CATALOG:
      return _objectSpread({}, state, {
        temp: action.payload
      });

    case _actions_catalog_actions__WEBPACK_IMPORTED_MODULE_0__["TYPES"].POST_CATALOG:
      return _objectSpread({}, state, {
        records: state.records.concat(action.payload),
        temp: action.payload
      });

    case _actions_catalog_actions__WEBPACK_IMPORTED_MODULE_0__["TYPES"].PATCH_CATALOG:
      return _objectSpread({}, state, {
        records: state.records.map(item => item.id === action.payload.id ? action.payload : item),
        temp: action.payload
      });
  }
}

/***/ }),

/***/ "./src/store/reducers/config-reducer.js":
/*!**********************************************!*\
  !*** ./src/store/reducers/config-reducer.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return reducer; });
/* harmony import */ var _actions_config_actions__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../actions/config-actions */ "./src/store/actions/config-actions.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }



const getTranslations = state => {
  return state.intlData.locales[state.preferences.language] || {};
};

const storageBucket = 'https://firebasestorage.googleapis.com/v0/b/lideres-sociales-bd152.appspot.com/o';
const initialState = {
  appDisabled: false,
  appDisabledMessage: '',
  appLanguages: ['en', 'es'],
  appLastSync: null,
  appLogo: `${storageBucket}/palomita.png?alt=media&token=21ad61e8-8e69-4dc7-9303-b92b32a3a8d4`,
  appName: 'Lideres Sociales',
  appUrl: 'https://react.com',
  catalogs: {
    planFeatures: 'Olfm2WjrW40DZMAFdLrZ',
    developersDocs: 'vzVa29WTqL1cfhwWbo6C',
    faqInfo: 'Naq9GQBXYHFY7xXfH06l',
    privacyInfo: 'R5Uola6kIV99td57mIgF',
    termsInfo: 'Zn0E6zs2B9Tny7y4T1BO'
  },
  intlData: {
    callingCodes: [],
    countries: [],
    currencies: [],
    currencyConversions: {},
    languages: [],
    locales: {}
  },
  emails: {
    noreply: 'noreply@mail.com',
    support: 'sample@mail.com'
  },
  plans: {
    free: 'tCfrWhFSY8aPv4bix6pV',
    standard: 'nZJF2RyrN9GXsiWD7iOb',
    premium: 'vKTHJhFqXGp4KkBaBPWG'
  },
  preferences: {
    currency: 'usd',
    dateFormat: 'DD/MM/YYYY',
    language: 'en'
  },
  translations: {}
};
function reducer(state = initialState, action) {
  switch (action.type) {
    default:
      return state;

    case _actions_config_actions__WEBPACK_IMPORTED_MODULE_0__["TYPES"].GET_CURRENCY_CONVERSIONS:
      return _objectSpread({}, state, {
        temp: action.payload
      });

    case _actions_config_actions__WEBPACK_IMPORTED_MODULE_0__["TYPES"].SET_CONFIGURATION:
      {
        const newState = Object.assign(state, action.payload);
        return _objectSpread({}, newState, {
          translations: getTranslations(newState)
        });
      }

    case _actions_config_actions__WEBPACK_IMPORTED_MODULE_0__["TYPES"].SET_CURRENCY_CONVERSIONS:
      return _objectSpread({}, state, {
        intlData: _objectSpread({}, state.intlData, {
          currencyConversions: action.payload
        })
      });

    case _actions_config_actions__WEBPACK_IMPORTED_MODULE_0__["TYPES"].SET_PREFERENCES:
      return _objectSpread({}, state, {
        preferences: action.payload,
        translations: getTranslations(state)
      });
  }
}

/***/ }),

/***/ "./src/store/reducers/home-reducer.js":
/*!********************************************!*\
  !*** ./src/store/reducers/home-reducer.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return reducer; });
/* harmony import */ var _actions_home_actions__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../actions/home-actions */ "./src/store/actions/home-actions.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const initialState = {
  developersInfo: [],
  faqInfo: {},
  planFeatures: [],
  privacyInfo: {},
  termsInfo: {}
};
function reducer(state = initialState, action) {
  switch (action.type) {
    default:
      return state;

    case _actions_home_actions__WEBPACK_IMPORTED_MODULE_0__["TYPES"].GET_DEVELOPERS_INFO:
      return _objectSpread({}, state, {
        developersInfo: action.payload
      });

    case _actions_home_actions__WEBPACK_IMPORTED_MODULE_0__["TYPES"].GET_FAQ_INFO:
      return _objectSpread({}, state, {
        faqInfo: action.payload
      });

    case _actions_home_actions__WEBPACK_IMPORTED_MODULE_0__["TYPES"].GET_PLAN_FEATURES:
      return _objectSpread({}, state, {
        planFeatures: action.payload
      });

    case _actions_home_actions__WEBPACK_IMPORTED_MODULE_0__["TYPES"].GET_PRIVACY_INFO:
      return _objectSpread({}, state, {
        privacyInfo: action.payload
      });

    case _actions_home_actions__WEBPACK_IMPORTED_MODULE_0__["TYPES"].GET_TERMS_INFO:
      return _objectSpread({}, state, {
        termsInfo: action.payload
      });
  }
}

/***/ }),

/***/ "./src/store/reducers/locale-reducer.js":
/*!**********************************************!*\
  !*** ./src/store/reducers/locale-reducer.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return reducer; });
/* harmony import */ var _actions_locale_actions__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../actions/locale-actions */ "./src/store/actions/locale-actions.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const initialState = {
  records: [],
  temp: null
};
function reducer(state = initialState, action) {
  switch (action.type) {
    default:
      return state;

    case _actions_locale_actions__WEBPACK_IMPORTED_MODULE_0__["TYPES"].GET_LOCALES:
      return _objectSpread({}, state, {
        records: action.payload
      });

    case _actions_locale_actions__WEBPACK_IMPORTED_MODULE_0__["TYPES"].GET_LOCALE:
      return _objectSpread({}, state, {
        temp: action.payload
      });

    case _actions_locale_actions__WEBPACK_IMPORTED_MODULE_0__["TYPES"].POST_LOCALE:
      return _objectSpread({}, state, {
        records: state.records.concat(action.payload),
        temp: action.payload
      });

    case _actions_locale_actions__WEBPACK_IMPORTED_MODULE_0__["TYPES"].PATCH_LOCALE:
      return _objectSpread({}, state, {
        records: state.records.map(item => item.id === action.payload.id ? action.payload : item),
        temp: action.payload
      });

    case _actions_locale_actions__WEBPACK_IMPORTED_MODULE_0__["TYPES"].DELETE_LOCALE:
      return _objectSpread({}, state, {
        records: state.records.filter(item => item.id !== action.payload.id)
      });
  }
}

/***/ }),

/***/ "./src/store/reducers/user-reducer.js":
/*!********************************************!*\
  !*** ./src/store/reducers/user-reducer.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return reducer; });
/* harmony import */ var _actions_user_actions__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../actions/user-actions */ "./src/store/actions/user-actions.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const initialState = {
  auth: null,
  authLoaded: false,
  backup: null,
  claims: [],
  customerInfo: null,
  customerReceipts: [],
  records: [],
  temp: null,
  token: null
};
function reducer(state = initialState, action) {
  switch (action.type) {
    default:
      return state;

    case _actions_user_actions__WEBPACK_IMPORTED_MODULE_0__["TYPES"].GET_BACKUP:
      return _objectSpread({}, state, {
        backup: action.payload
      });

    case _actions_user_actions__WEBPACK_IMPORTED_MODULE_0__["TYPES"].GET_USERS:
      return _objectSpread({}, state, {
        records: action.payload
      });

    case _actions_user_actions__WEBPACK_IMPORTED_MODULE_0__["TYPES"].GET_USER:
      return _objectSpread({}, state, {
        temp: action.payload
      });

    case _actions_user_actions__WEBPACK_IMPORTED_MODULE_0__["TYPES"].GET_CUSTOMER_INFO:
      return _objectSpread({}, state, {
        customerInfo: action.payload
      });

    case _actions_user_actions__WEBPACK_IMPORTED_MODULE_0__["TYPES"].GET_CUSTOMER_RECEIPTS:
      return _objectSpread({}, state, {
        customerReceipts: action.payload
      });

    case _actions_user_actions__WEBPACK_IMPORTED_MODULE_0__["TYPES"].PATCH_USER:
      return _objectSpread({}, state, {
        records: state.records.map(item => item.id === action.payload.id ? action.payload : item),
        temp: action.payload
      });

    case _actions_user_actions__WEBPACK_IMPORTED_MODULE_0__["TYPES"].CHANGE_PASSWORD:
      return _objectSpread({}, state, {
        temp: action.payload
      });

    case _actions_user_actions__WEBPACK_IMPORTED_MODULE_0__["TYPES"].LOGIN:
      return _objectSpread({}, state, {
        temp: action.payload.messsage,
        token: action.payload.token
      });

    case _actions_user_actions__WEBPACK_IMPORTED_MODULE_0__["TYPES"].LOGOUT:
      return _objectSpread({}, state, {
        auth: null,
        token: null
      });

    case _actions_user_actions__WEBPACK_IMPORTED_MODULE_0__["TYPES"].REGISTER:
      return _objectSpread({}, state, {
        temp: action.payload.messsage,
        token: action.payload.token
      });

    case _actions_user_actions__WEBPACK_IMPORTED_MODULE_0__["TYPES"].ME:
      return _objectSpread({}, state, {
        auth: action.payload,
        authLoaded: true
      });

    case _actions_user_actions__WEBPACK_IMPORTED_MODULE_0__["TYPES"].RECOVER_PASSWORD:
      return _objectSpread({}, state, {
        temp: action.payload
      });

    case _actions_user_actions__WEBPACK_IMPORTED_MODULE_0__["TYPES"].SET_TOKEN:
      return _objectSpread({}, state, {
        token: action.payload
      });

    case _actions_user_actions__WEBPACK_IMPORTED_MODULE_0__["TYPES"].GET_USER_CLAIMS:
      return _objectSpread({}, state, {
        claims: action.payload
      });

    case _actions_user_actions__WEBPACK_IMPORTED_MODULE_0__["TYPES"].POST_USER_CLAIM:
      return _objectSpread({}, state, {
        claims: state.claims.concat(action.payload),
        temp: action.payload
      });

    case _actions_user_actions__WEBPACK_IMPORTED_MODULE_0__["TYPES"].DELETE_USER_CLAIM:
      return _objectSpread({}, state, {
        claims: state.claims.filter(item => item !== action.payload),
        temp: action.payload
      });

    case _actions_user_actions__WEBPACK_IMPORTED_MODULE_0__["TYPES"].UPDATE_CREDIT_CARD:
      return _objectSpread({}, state, {
        temp: action.payload
      });

    case _actions_user_actions__WEBPACK_IMPORTED_MODULE_0__["TYPES"].CREATE_SUBSCRIPTION:
      return _objectSpread({}, state, {
        token: action.payload.token,
        temp: action.payload.message
      });

    case _actions_user_actions__WEBPACK_IMPORTED_MODULE_0__["TYPES"].CREATE_REPORT:
      return _objectSpread({}, state, {
        // token: action.payload.token,
        temp: action.payload.message
      });
  }
}

/***/ }),

/***/ "./src/store/store.js":
/*!****************************!*\
  !*** ./src/store/store.js ***!
  \****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! redux */ "redux");
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(redux__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var redux_devtools_extension_developmentOnly__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! redux-devtools-extension/developmentOnly */ "redux-devtools-extension/developmentOnly");
/* harmony import */ var redux_devtools_extension_developmentOnly__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(redux_devtools_extension_developmentOnly__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _middlewares_request_middleware__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./middlewares/request-middleware */ "./src/store/middlewares/request-middleware.js");
/* harmony import */ var _middlewares_thunk_middleware__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./middlewares/thunk-middleware */ "./src/store/middlewares/thunk-middleware.js");
/* harmony import */ var _reducers_catalog_reducer__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./reducers/catalog-reducer */ "./src/store/reducers/catalog-reducer.js");
/* harmony import */ var _reducers_config_reducer__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./reducers/config-reducer */ "./src/store/reducers/config-reducer.js");
/* harmony import */ var _reducers_home_reducer__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./reducers/home-reducer */ "./src/store/reducers/home-reducer.js");
/* harmony import */ var _reducers_locale_reducer__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./reducers/locale-reducer */ "./src/store/reducers/locale-reducer.js");
/* harmony import */ var _reducers_user_reducer__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./reducers/user-reducer */ "./src/store/reducers/user-reducer.js");
 // middlewares



 // reducers






let initialState = {};

if (typeof window !== 'undefined') {
  initialState = window.initialState || initialState;
}

const reducers = Object(redux__WEBPACK_IMPORTED_MODULE_0__["combineReducers"])({
  catalog: _reducers_catalog_reducer__WEBPACK_IMPORTED_MODULE_4__["default"],
  config: _reducers_config_reducer__WEBPACK_IMPORTED_MODULE_5__["default"],
  home: _reducers_home_reducer__WEBPACK_IMPORTED_MODULE_6__["default"],
  locale: _reducers_locale_reducer__WEBPACK_IMPORTED_MODULE_7__["default"],
  user: _reducers_user_reducer__WEBPACK_IMPORTED_MODULE_8__["default"]
});
const middleware = Object(redux_devtools_extension_developmentOnly__WEBPACK_IMPORTED_MODULE_1__["composeWithDevTools"])(Object(redux__WEBPACK_IMPORTED_MODULE_0__["applyMiddleware"])(_middlewares_request_middleware__WEBPACK_IMPORTED_MODULE_2__["default"], _middlewares_thunk_middleware__WEBPACK_IMPORTED_MODULE_3__["default"]));
const store = Object(redux__WEBPACK_IMPORTED_MODULE_0__["createStore"])(reducers, initialState, middleware);
/* harmony default export */ __webpack_exports__["default"] = (store);

/***/ }),

/***/ "./src/utils/database-utils.js":
/*!*************************************!*\
  !*** ./src/utils/database-utils.js ***!
  \*************************************/
/*! exports provided: getDbDocument, getDbQuery, getDbQueryByIds, getDbParsedQuery */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getDbDocument", function() { return getDbDocument; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getDbQuery", function() { return getDbQuery; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getDbQueryByIds", function() { return getDbQueryByIds; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getDbParsedQuery", function() { return getDbParsedQuery; });
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const getDbDocument = async (db, collection, documentId) => {
  let response = null;

  if (!documentId) {
    return null;
  }

  response = db.collection(collection);
  response = await response.doc(documentId).get();
  response = response.data();

  if (response) {
    response = _objectSpread({}, response, {
      id: documentId
    });
  }

  return response;
};
const getDbQuery = async (db, collection, query) => {
  const parsedQuery = getDbParsedQuery(query);
  let dbQuery = db.collection(collection);
  let response = [];

  if (parsedQuery.where && parsedQuery.where.length) {
    parsedQuery.where.forEach(item => dbQuery = dbQuery.where(item[0], item[1], item[2]));
  }

  if (parsedQuery.sort && parsedQuery.sort.length) {
    parsedQuery.sort.forEach(item => dbQuery = dbQuery.orderBy(item[0], item[1] || 'asc'));
  }

  if (parsedQuery.limit) {
    dbQuery = dbQuery.limit(parsedQuery.limit);
  }

  response = await dbQuery.get();
  response = response.docs.map(doc => _objectSpread({}, doc.data(), {
    id: doc.id
  }));

  if (parsedQuery.select) {
    response = response.map(item => {
      return Object.keys(item).reduce((accum, key) => {
        return parsedQuery.select.includes(key) ? Object.assign(accum, {
          [key]: item[key]
        }) : accum;
      }, {});
    });
  }

  return response;
};
const getDbQueryByIds = async (db, collection, ids) => {
  let response = [];
  let documentReferences = ids.filter(id => id).map(id => db.collection(collection).doc(id).get());
  response = await Promise.all(documentReferences);
  response = response.filter(doc => !doc.isEmpty).map(doc => _objectSpread({}, doc.data(), {
    id: doc.id
  }));
  return response;
};
const getDbParsedQuery = data => {
  let response = {};
  let searchCriteria = {};

  for (const key in data) {
    searchCriteria = data[key];

    if (key === 'limit') {
      response.limit = searchCriteria;
    }

    if (key === 'select') {
      response.select = searchCriteria;
    }

    if (key === 'sort') {
      response.sort = [];

      for (const key in searchCriteria) {
        response.sort.push([key, searchCriteria[key]]);
      }
    }

    if (key === 'where') {
      response.where = [];

      for (const key in searchCriteria) {
        for (const criteria in searchCriteria[key]) {
          if (criteria === 'in') {
            response.where.push([key, 'array-contains', searchCriteria[key][criteria]]);
          } else if (criteria === 'like') {
            response.where.push([key, '>=', searchCriteria[key][criteria]]);
          } else if (criteria === 'range') {
            const range = searchCriteria[key][criteria];
            response.where.push([key, '>=', range.start]);
            response.where.push([key, '<=', range.end]);
          } else {
            response.where.push([key, criteria, searchCriteria[key][criteria]]);
          }
        }
      }
    }
  }

  return response;
};

/***/ }),

/***/ "./src/utils/intl-utils.js":
/*!*********************************!*\
  !*** ./src/utils/intl-utils.js ***!
  \*********************************/
/*! exports provided: formatDate, formatLocales, formatMoney, getIntlData, translate */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "formatDate", function() { return formatDate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "formatLocales", function() { return formatLocales; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "formatMoney", function() { return formatMoney; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getIntlData", function() { return getIntlData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "translate", function() { return translate; });
/* harmony import */ var _database_utils__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./database-utils */ "./src/utils/database-utils.js");
/* harmony import */ var _text_utils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./text-utils */ "./src/utils/text-utils.js");


const CACHE = {
  INTL_DATA: 'CACHE_INTL_DATA',
  CURRENCY_CONVERSIONS: 'CACHE_CURRENCY_CONVERSIONS'
};
const formatDate = (datetime, format) => {
  const date = new Date(datetime);
  const day = date.getDate();
  const month = date.getMonth() + 1;
  const year = date.getFullYear();
  const response = format.toLowerCase().replace(new RegExp('dd', 'g'), day.toString().padStart(2, '0')).replace(new RegExp('mm', 'g'), month.toString().padStart(2, '0')).replace(new RegExp('yyyy', 'g'), year);
  return response;
};
const formatLocales = (languages, locales) => {
  let response = {};
  response = languages.reduce((accum, lang) => {
    locales.forEach(locale => {
      accum[lang] = accum[lang] || {};
      accum[lang][locale.name] = locale.value[lang];
    });
    return accum;
  }, {});
  return response;
};
const formatMoney = (value, thousands = ',') => {
  const negativeSign = value < 0 ? '-' : '';
  let i = Math.round(value).toString();
  let j = i.length > 3 ? i.length % 3 : 0;
  return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, '$1' + thousands);
};
const getIntlData = async (config, axios, cache, db, intlAPI) => {
  let response = cache.get(CACHE.INTL_DATA);

  if (!response) {
    let sourceIntl = await axios.get(`${intlAPI.url}/all?fields=alpha3Code;callingCodes;currencies;flag;languages;name;translations`);
    sourceIntl = sourceIntl.data;
    response = {}; // calling codes

    response.callingCodes = sourceIntl.reduce((accum, item) => {
      accum = accum.concat(item.callingCodes.filter(callingCode => callingCode).map(callingCode => ({
        label: `+${callingCode}`,
        value: `+${callingCode}`
      })));
      return accum;
    }, []).filter((item, index, array) => array.findIndex(obj => obj.value === item.value) === index).sort((a, b) => a.label.localeCompare(b.label)); // countries 

    response.countries = sourceIntl.reduce((accum, item) => {
      accum = accum.concat({
        label: item.name,
        value: item.alpha3Code.toLowerCase()
      });
      return accum;
    }, []).sort((a, b) => a.label.localeCompare(b.label)); // currencies

    response.currencies = sourceIntl.reduce((accum, item) => {
      accum = accum.concat(item.currencies.filter(currencyInfo => currencyInfo.code && currencyInfo.code !== '(none)').map(currencyInfo => ({
        label: currencyInfo.code.toUpperCase(),
        value: currencyInfo.code.toLowerCase()
      })));
      return accum;
    }, []).filter((item, index, array) => array.findIndex(obj => obj.value === item.value) === index).sort((a, b) => a.label.localeCompare(b.label)); // languages

    response.languages = sourceIntl.reduce((accum, item) => {
      accum = accum.concat(item.languages.filter(languageInfo => languageInfo.iso639_1).map(languageInfo => ({
        label: languageInfo.name,
        value: languageInfo.iso639_1
      })));
      return accum;
    }, []).filter((item, index, array) => array.findIndex(obj => obj.value === item.value) === index).sort((a, b) => a.label.localeCompare(b.label)); // locales

    const locales = await Object(_database_utils__WEBPACK_IMPORTED_MODULE_0__["getDbQuery"])(db, 'locales', {});
    const formattedLocales = formatLocales(config.appLanguages, locales);
    response.locales = formattedLocales;
    cache.put(CACHE.INTL_DATA, response);
  }

  return response;
};
const translate = (translations = {}, locale = '', params = {}) => {
  const localeValue = translations[locale] || '';
  const response = Object(_text_utils__WEBPACK_IMPORTED_MODULE_1__["formatTemplate"])(localeValue, params) || locale;
  return response;
};

/***/ }),

/***/ "./src/utils/mail-utils.js":
/*!*********************************!*\
  !*** ./src/utils/mail-utils.js ***!
  \*********************************/
/*! exports provided: sendNotification */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "sendNotification", function() { return sendNotification; });
/* harmony import */ var _intl_utils__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./intl-utils */ "./src/utils/intl-utils.js");

const sendNotification = async (config, emailLibrary, emailData) => {
  try {
    const {
      sendgrid: sendgridConfig
    } = config;
    emailLibrary.setApiKey(sendgridConfig.key);
    await emailLibrary.send({
      from: 'noreply@mail.com',
      to: emailData.to,
      templateId: sendgridConfig.templategeneral,
      'dynamic_template_data': {
        slogan: Object(_intl_utils__WEBPACK_IMPORTED_MODULE_0__["translate"])(config.translations, 'slogan'),
        subject: emailData.subject,
        message: emailData.message
      }
    });
  } catch (error) {
    console.warn('Error sending subscription notification', emailData, error);
  }
};

/***/ }),

/***/ "./src/utils/model-utils.js":
/*!**********************************!*\
  !*** ./src/utils/model-utils.js ***!
  \**********************************/
/*! exports provided: getCatalogModel, getConfigurationModel, getLocaleModel, getUserModel, getReportModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getCatalogModel", function() { return getCatalogModel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getConfigurationModel", function() { return getConfigurationModel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getLocaleModel", function() { return getLocaleModel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getUserModel", function() { return getUserModel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getReportModel", function() { return getReportModel; });
const getCatalogModel = (associations, appLanguages) => {
  return [{
    property: 'active',
    type: 'boolean',
    rules: [{
      required: true
    }]
  }, {
    property: 'createdAt',
    type: 'number',
    rules: [{
      required: true
    }]
  }, {
    property: 'name',
    type: 'string',
    rules: [{
      required: true
    }]
  }, {
    property: 'order',
    type: 'number',
    rules: [{
      required: true
    }]
  }, {
    property: 'parent',
    type: 'string',
    rules: [{
      association: associations.parents
    }]
  }, {
    property: 'thumbnail',
    type: 'string',
    rules: [{
      format: 'url'
    }]
  }, {
    property: 'updatedAt',
    type: 'number'
  }, {
    property: 'value',
    type: 'object',
    rules: [{
      required: true
    }],
    validations: appLanguages.map(key => {
      return {
        property: key,
        type: 'string'
      };
    })
  }];
};
const getConfigurationModel = () => {
  return [{
    property: 'appDisabled',
    type: 'boolean',
    rules: [{
      required: true
    }]
  }, {
    property: 'appDisabledMessage',
    type: 'string',
    rules: [{
      required: true
    }]
  }, {
    property: 'appLastSync',
    type: 'number',
    rules: [{
      required: true
    }]
  }];
};
const getLocaleModel = appLanguages => {
  return [{
    property: 'active',
    type: 'boolean',
    rules: [{
      required: true
    }]
  }, {
    property: 'createdAt',
    type: 'number',
    rules: [{
      required: true
    }]
  }, {
    property: 'name',
    type: 'string',
    rules: [{
      required: true
    }]
  }, {
    property: 'updatedAt',
    type: 'number'
  }, {
    property: 'value',
    type: 'object',
    rules: [{
      required: true
    }],
    validations: appLanguages.map(key => {
      return {
        property: key,
        type: 'string'
      };
    })
  }];
};
const getUserModel = (userData, passwordIsRequired) => {
  return [{
    property: 'active',
    type: 'boolean',
    rules: [{
      required: true
    }]
  }, {
    property: 'createdAt',
    type: 'number',
    rules: [{
      required: true
    }]
  }, {
    property: 'customerCodePayment',
    type: 'string'
  }, {
    property: 'displayName',
    type: 'string'
  }, {
    property: 'email',
    type: 'string',
    rules: [{
      required: true
    }, {
      format: 'email'
    }]
  }, {
    property: 'password',
    type: 'string',
    rules: [{
      required: passwordIsRequired
    }, {
      minLength: 6
    }]
  }, {
    property: 'passwordConfirmation',
    type: 'string',
    rules: [{
      required: passwordIsRequired
    }, {
      equalTo: userData.password
    }]
  }, {
    property: 'preferences',
    type: 'object',
    validations: [{
      property: 'currency',
      type: 'string'
    }, {
      property: 'dateFormat',
      type: 'string'
    }, {
      property: 'language',
      type: 'string'
    }]
  }, {
    property: 'updatedAt',
    type: 'number'
  }];
};
const getReportModel = () => {
  return [{
    property: 'active',
    type: 'boolean',
    rules: [{
      required: false
    }, {
      default: false
    }]
  }, {
    property: 'createdAt',
    type: 'number',
    rules: [{
      required: true
    }]
  }, {
    property: 'names',
    type: 'string',
    rules: [{
      required: true
    }]
  }, {
    property: 'lastNames',
    type: 'string',
    rules: [{
      required: true
    }]
  }, {
    property: 'email',
    type: 'string',
    rules: [{
      required: true
    }, {
      format: 'email'
    }]
  }, {
    property: 'phone',
    type: 'string',
    rules: [{
      required: true
    }, {
      minLength: 10
    }]
  }, {
    property: 'department',
    type: 'string',
    rules: [{
      required: true
    }]
  }, {
    property: 'city',
    type: 'string',
    rules: [{
      required: true
    }]
  }, {
    property: 'address',
    type: 'string'
  }, {
    property: 'description',
    type: 'string',
    rules: [{
      required: true
    }]
  }, {
    property: 'checkSendEmail',
    type: 'string'
  }];
};

/***/ }),

/***/ "./src/utils/request-utils.js":
/*!************************************!*\
  !*** ./src/utils/request-utils.js ***!
  \************************************/
/*! exports provided: getDownload, getQueryParamsString, getQueryParamsJson */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getDownload", function() { return getDownload; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getQueryParamsString", function() { return getQueryParamsString; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getQueryParamsJson", function() { return getQueryParamsJson; });
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const getDownload = (data, filename) => {
  let a = document.createElement('a');
  document.body.appendChild(a);
  a.style = 'display: none';
  a.href = URL.createObjectURL(new Blob([data], {
    type: 'octet/stream'
  }));
  a.download = filename;
  a.click();
  a.remove();
  window.URL.revokeObjectURL(a.href);
};
const getQueryParamsString = query => {
  let queryString = [];
  let queryKeys = Object.keys(query || {}).filter(key => query[key]);

  for (const key of queryKeys) {
    queryString = queryString.concat(`${key}=${JSON.stringify(query[key])}`);
  }

  return `?${queryString.join('&')}`;
};
const getQueryParamsJson = query => {
  const urlParams = new URLSearchParams(query);
  let paramValue = null;
  return Array.from(urlParams).reduce((accum, item) => {
    try {
      paramValue = JSON.parse(item[1]);
    } catch (e) {
      paramValue = item[1];
    }

    accum = _objectSpread({}, accum, {
      [item[0]]: paramValue
    });
    return accum;
  }, {});
};

/***/ }),

/***/ "./src/utils/response-utils.js":
/*!*************************************!*\
  !*** ./src/utils/response-utils.js ***!
  \*************************************/
/*! exports provided: getBackendError, getFrontendError */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getBackendError", function() { return getBackendError; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getFrontendError", function() { return getFrontendError; });
const getBackendError = error => {
  let response = {
    code: error.code,
    message: error.message,
    stack: error.stack
  };
  return response;
};
const getFrontendError = error => {
  let response = {
    message: error.message
  };
  response.errors = error.response && error.response.data && error.response.data.errors || null;
  response.message = error.response && error.response.data && error.response.data.message || error.message;
  return response;
};

/***/ }),

/***/ "./src/utils/text-utils.js":
/*!*********************************!*\
  !*** ./src/utils/text-utils.js ***!
  \*********************************/
/*! exports provided: formatTemplate, like, toUrl */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "formatTemplate", function() { return formatTemplate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "like", function() { return like; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "toUrl", function() { return toUrl; });
const formatTemplate = (template, data) => {
  let response = template;

  for (const key in data) {
    response = response.replace(new RegExp(`{${key}}`, 'g'), data[key]);
  }

  return response;
};
const like = (value, criteria) => {
  return (value || '').toLowerCase().includes((criteria || '').toLowerCase());
};
const toUrl = value => {
  let encodedUrl = value.toString().toLowerCase();
  encodedUrl = encodedUrl.replace(/[^\w ]+/g, '');
  encodedUrl = encodedUrl.replace(/ +/g, '-');
  return encodedUrl;
};

/***/ }),

/***/ "./src/utils/validation-utils.js":
/*!***************************************!*\
  !*** ./src/utils/validation-utils.js ***!
  \***************************************/
/*! exports provided: isEmail, isHtml, isUrl, validateModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isEmail", function() { return isEmail; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isHtml", function() { return isHtml; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isUrl", function() { return isUrl; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "validateModel", function() { return validateModel; });
/* harmony import */ var _intl_utils__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./intl-utils */ "./src/utils/intl-utils.js");
 // helpers

const isPropertyEmpty = value => {
  let isEmpty = false;

  switch (typeof value) {
    case 'boolean':
      isEmpty = ![true, false].includes(value);
      break;

    case 'number':
      isEmpty = [null, undefined].includes(value);
      break;

    case 'object':
      if (value === null) {
        isEmpty = true;
      } else if (Array.isArray(value)) {
        isEmpty = value.length === 0;
      } else if (!Array.isArray(value)) {
        isEmpty = Object.keys(value).length === 0;
      }

      break;

    case 'string':
      isEmpty = value.trim().length === 0;
      break;

    default:
      isEmpty = true;
      break;
  }

  return isEmpty;
};

const getPropertyValidation = (config, propertyData) => {
  const {
    propertyRules,
    propertyType,
    propertyValue
  } = propertyData;
  let error = ''; // validating property type

  let propertyValueType = Array.isArray(propertyValue) ? 'array' : typeof propertyValue;

  if (!isPropertyEmpty(propertyValue) && propertyValueType !== propertyType) {
    error = Object(_intl_utils__WEBPACK_IMPORTED_MODULE_0__["translate"])(config.translations, 'invalidDataType', {
      dataType: propertyType
    });
  } // validating property rules


  const isPropertyRequired = propertyRules.find(rule => rule.name === 'required' && rule.value === true);

  if (isPropertyEmpty(error) && (isPropertyRequired || !isPropertyEmpty(propertyValue))) {
    for (const rule of propertyRules) {
      if (!isPropertyEmpty(error)) {
        break;
      }

      switch (rule.name) {
        case 'required':
          if (isPropertyEmpty(propertyValue)) {
            error = Object(_intl_utils__WEBPACK_IMPORTED_MODULE_0__["translate"])(config.translations, 'fieldIsRequired');
          }

          break;

        case 'association':
          if (!rule.value.find(item => item.id === propertyValue)) {
            error = Object(_intl_utils__WEBPACK_IMPORTED_MODULE_0__["translate"])(config.translations, 'wrongAssociatedID');
          }

          break;

        case 'currencies':
          if (!rule.value.includes(propertyValue.toLowerCase())) {
            error = Object(_intl_utils__WEBPACK_IMPORTED_MODULE_0__["translate"])(config.translations, 'currencyCodeIncorrect');
          }

          break;

        case 'equalTo':
          if (rule.value !== propertyValue) {
            error = Object(_intl_utils__WEBPACK_IMPORTED_MODULE_0__["translate"])(config.translations, 'notMatch');
          }

          break;

        case 'format':
          if (rule.value === 'email' && !isEmail(propertyValue)) {
            error = Object(_intl_utils__WEBPACK_IMPORTED_MODULE_0__["translate"])(config.translations, 'incorrectFormat');
          }

          if (rule.value === 'url' && !isUrl(propertyValue)) {
            error = Object(_intl_utils__WEBPACK_IMPORTED_MODULE_0__["translate"])(config.translations, 'incorrectFormat');
          }

          break;

        case 'include':
          if (!rule.value.includes(propertyValue)) {
            error = Object(_intl_utils__WEBPACK_IMPORTED_MODULE_0__["translate"])(config.translations, 'mustInclude', {
              values: rule.value.join(',')
            });
          }

          break;

        case 'maxLength':
          if (propertyValue.toString().length > rule.value) {
            error = Object(_intl_utils__WEBPACK_IMPORTED_MODULE_0__["translate"])(config.translations, 'maxLength', {
              maxLength: rule.value
            });
          }

          break;

        case 'minLength':
          if (propertyValue.toString().length < rule.value) {
            error = Object(_intl_utils__WEBPACK_IMPORTED_MODULE_0__["translate"])(config.translations, 'minLength', {
              minLength: rule.value
            });
          }

          break;
      }
    }
  }

  return error;
}; // methods


const isEmail = value => {
  const regExpEmail = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/;
  return regExpEmail.test(value);
};
const isHtml = value => {
  let div = document.createElement('div');
  div.innerHTML = value;

  for (let c = div.childNodes, i = c.length; i--;) {
    if (c[i].nodeType === 1) {
      return true;
    }
  }

  return false;
};
const isUrl = value => {
  const pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
  '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.?)+[a-z]{2,}|' + // domain name
  '((\\d{1,3}\\.){3}\\d{1,3}))' + // ip (v4) address
  '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port
  '(\\?[;&amp;a-z\\d%_.~+=-]*)?' + // query string
  '(\\#[-a-z\\d_]*)?$', 'i');
  return pattern.test(value);
};
const validateModel = (config, model, validations = []) => {
  let errors = {};
  let data = {};
  let property = null;
  let propertyType = null;
  let propertyRules = [];
  let propertyValue = null;

  for (let validation of validations) {
    property = validation.property;
    propertyType = validation.type;
    propertyRules = (validation.rules || []).flatMap(rule => Object.keys(rule).map(key => ({
      name: key,
      value: rule[key]
    })));
    propertyValue = model[property]; // assigning only primitive data (boolean, numbers, strings)

    if (!isPropertyEmpty(propertyValue) && typeof propertyValue !== 'object') {
      data[property] = propertyValue;
    } // validating inner properties for arrays


    if (!isPropertyEmpty(propertyValue) && Array.isArray(propertyValue) && propertyType === 'array') {
      let validationResponse = null;
      let itemValue = {};

      for (const item of propertyValue) {
        if (typeof item === 'object') {
          if (validation.validations) {
            itemValue = Object.keys(item).reduce((accum, key) => {
              return (validation.validations || []).map(item => item.property).includes(key) ? Object.assign(accum, {
                [key]: item[key]
              }) : accum;
            }, {});
            validationResponse = validateModel(config, itemValue, validation.validations);
            data[property] = (data[property] || []).concat(validationResponse.data);

            if (!isPropertyEmpty(validationResponse.errors)) {
              errors[property] = (errors[property] || []).concat(validationResponse.errors);
            }
          } else {
            data[property] = (data[property] || []).concat(item);
          }
        } else {
          data[property] = (data[property] || []).concat(item);
        }
      }
    } // validating inner properties for objects


    if (!isPropertyEmpty(propertyValue) && Object.prototype.toString.call(propertyValue) === '[object Object]' && propertyType === 'object') {
      if (validation.validations) {
        propertyValue = Object.keys(propertyValue).reduce((accum, key) => {
          return (validation.validations || []).map(item => item.property).includes(key) ? Object.assign(accum, {
            [key]: propertyValue[key]
          }) : accum;
        }, {});
        const validationResponse = validateModel(config, propertyValue, validation.validations);
        data[property] = validationResponse.data;

        if (!isPropertyEmpty(validationResponse.errors)) {
          errors[property] = validationResponse.errors;
        }
      } else {
        data[property] = propertyValue;
      }
    } // validating property


    const validationResponse = getPropertyValidation(config, {
      propertyRules,
      propertyType,
      propertyValue
    });

    if (!isPropertyEmpty(validationResponse)) {
      errors[property] = validationResponse;
    }
  }

  return {
    errors,
    data
  };
};

/***/ }),

/***/ 0:
/*!*********************************************!*\
  !*** multi @babel/polyfill ./src/server.js ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! @babel/polyfill */"@babel/polyfill");
module.exports = __webpack_require__(/*! ./src/server.js */"./src/server.js");


/***/ }),

/***/ "@babel/polyfill":
/*!**********************************!*\
  !*** external "@babel/polyfill" ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@babel/polyfill");

/***/ }),

/***/ "@sendgrid/mail":
/*!*********************************!*\
  !*** external "@sendgrid/mail" ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@sendgrid/mail");

/***/ }),

/***/ "axios":
/*!************************!*\
  !*** external "axios" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("axios");

/***/ }),

/***/ "busboy":
/*!*************************!*\
  !*** external "busboy" ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("busboy");

/***/ }),

/***/ "cors":
/*!***********************!*\
  !*** external "cors" ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("cors");

/***/ }),

/***/ "express":
/*!**************************!*\
  !*** external "express" ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("express");

/***/ }),

/***/ "firebase-admin":
/*!*********************************!*\
  !*** external "firebase-admin" ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("firebase-admin");

/***/ }),

/***/ "firebase-functions":
/*!*************************************!*\
  !*** external "firebase-functions" ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("firebase-functions");

/***/ }),

/***/ "firebase/app":
/*!*******************************!*\
  !*** external "firebase/app" ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("firebase/app");

/***/ }),

/***/ "firebase/auth":
/*!********************************!*\
  !*** external "firebase/auth" ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("firebase/auth");

/***/ }),

/***/ "firebase/database":
/*!************************************!*\
  !*** external "firebase/database" ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("firebase/database");

/***/ }),

/***/ "firebase/firestore":
/*!*************************************!*\
  !*** external "firebase/firestore" ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("firebase/firestore");

/***/ }),

/***/ "fs":
/*!*********************!*\
  !*** external "fs" ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("fs");

/***/ }),

/***/ "memory-cache":
/*!*******************************!*\
  !*** external "memory-cache" ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("memory-cache");

/***/ }),

/***/ "os":
/*!*********************!*\
  !*** external "os" ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("os");

/***/ }),

/***/ "path":
/*!***********************!*\
  !*** external "path" ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("path");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "react-dom/server":
/*!***********************************!*\
  !*** external "react-dom/server" ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-dom/server");

/***/ }),

/***/ "react-helmet":
/*!*******************************!*\
  !*** external "react-helmet" ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-helmet");

/***/ }),

/***/ "react-redux":
/*!******************************!*\
  !*** external "react-redux" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-redux");

/***/ }),

/***/ "react-router-dom":
/*!***********************************!*\
  !*** external "react-router-dom" ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-router-dom");

/***/ }),

/***/ "react-select":
/*!*******************************!*\
  !*** external "react-select" ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-select");

/***/ }),

/***/ "redux":
/*!************************!*\
  !*** external "redux" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("redux");

/***/ }),

/***/ "redux-devtools-extension/developmentOnly":
/*!***********************************************************!*\
  !*** external "redux-devtools-extension/developmentOnly" ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("redux-devtools-extension/developmentOnly");

/***/ }),

/***/ "redux-thunk":
/*!******************************!*\
  !*** external "redux-thunk" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("redux-thunk");

/***/ }),

/***/ "serialize-javascript":
/*!***************************************!*\
  !*** external "serialize-javascript" ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("serialize-javascript");

/***/ }),

/***/ "styled-components":
/*!************************************!*\
  !*** external "styled-components" ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("styled-components");

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL2FwaXMvZXZlbnRzLmpzIiwid2VicGFjazovLy8uL3NyYy9hcGlzL2ZpcmViYXNlLmpzIiwid2VicGFjazovLy8uL3NyYy9hcGlzL21pZGRsZXdhcmVzLmpzIiwid2VicGFjazovLy8uL3NyYy9hcGlzL3JvdXRlcy9jYXRhbG9nLmpzIiwid2VicGFjazovLy8uL3NyYy9hcGlzL3JvdXRlcy9jb25maWcuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2FwaXMvcm91dGVzL2xvY2FsZS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvYXBpcy9yb3V0ZXMvc29jaWFsTGVhZGVyLmpzIiwid2VicGFjazovLy8uL3NyYy9hcGlzL3JvdXRlcy91c2VyLmpzIiwid2VicGFjazovLy8uL3NyYy9jb250ZXh0LmpzIiwid2VicGFjazovLy8uL3NyYy9wYWdlcy9fY29tcG9uZW50cy9jb2xsYXBzaWJsZS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvcGFnZXMvX2NvbXBvbmVudHMvZm9vdGVyLmpzIiwid2VicGFjazovLy8uL3NyYy9wYWdlcy9fY29tcG9uZW50cy9oZWFkZXIuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3BhZ2VzL19jb21wb25lbnRzL21lbnUtbWFpbi5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvcGFnZXMvX2NvbXBvbmVudHMvbWVudS1wb3BvdmVycy5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvcGFnZXMvX2NvbXBvbmVudHMvbmF2YmFyLmpzIiwid2VicGFjazovLy8uL3NyYy9wYWdlcy9fY29tcG9uZW50cy9uYXZpZ2F0aW9uLWJhci5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvcGFnZXMvX2NvbXBvbmVudHMvbmV3cy5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvcGFnZXMvX2NvbXBvbmVudHMvcG9wb3Zlci5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvcGFnZXMvaG9tZS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvcGFnZXMvbG9naW4uanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3BhZ2VzL21haW4uanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3BhZ2VzL25vdC1mb3VuZC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvcGFnZXMvcmVjb3Zlci1wYXNzd29yZC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvcGFnZXMvcmVnaXN0ZXIuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3JvdXRlcy1zc3IuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3NlcnZlci5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvc3RvcmUvYWN0aW9ucy9jYXRhbG9nLWFjdGlvbnMuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3N0b3JlL2FjdGlvbnMvY29uZmlnLWFjdGlvbnMuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3N0b3JlL2FjdGlvbnMvaG9tZS1hY3Rpb25zLmpzIiwid2VicGFjazovLy8uL3NyYy9zdG9yZS9hY3Rpb25zL2xvY2FsZS1hY3Rpb25zLmpzIiwid2VicGFjazovLy8uL3NyYy9zdG9yZS9hY3Rpb25zL3VzZXItYWN0aW9ucy5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvc3RvcmUvbWlkZGxld2FyZXMvcmVxdWVzdC1taWRkbGV3YXJlLmpzIiwid2VicGFjazovLy8uL3NyYy9zdG9yZS9taWRkbGV3YXJlcy90aHVuay1taWRkbGV3YXJlLmpzIiwid2VicGFjazovLy8uL3NyYy9zdG9yZS9yZWR1Y2Vycy9jYXRhbG9nLXJlZHVjZXIuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3N0b3JlL3JlZHVjZXJzL2NvbmZpZy1yZWR1Y2VyLmpzIiwid2VicGFjazovLy8uL3NyYy9zdG9yZS9yZWR1Y2Vycy9ob21lLXJlZHVjZXIuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3N0b3JlL3JlZHVjZXJzL2xvY2FsZS1yZWR1Y2VyLmpzIiwid2VicGFjazovLy8uL3NyYy9zdG9yZS9yZWR1Y2Vycy91c2VyLXJlZHVjZXIuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3N0b3JlL3N0b3JlLmpzIiwid2VicGFjazovLy8uL3NyYy91dGlscy9kYXRhYmFzZS11dGlscy5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvdXRpbHMvaW50bC11dGlscy5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvdXRpbHMvbWFpbC11dGlscy5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvdXRpbHMvbW9kZWwtdXRpbHMuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3V0aWxzL3JlcXVlc3QtdXRpbHMuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3V0aWxzL3Jlc3BvbnNlLXV0aWxzLmpzIiwid2VicGFjazovLy8uL3NyYy91dGlscy90ZXh0LXV0aWxzLmpzIiwid2VicGFjazovLy8uL3NyYy91dGlscy92YWxpZGF0aW9uLXV0aWxzLmpzIiwid2VicGFjazovLy9leHRlcm5hbCBcIkBiYWJlbC9wb2x5ZmlsbFwiIiwid2VicGFjazovLy9leHRlcm5hbCBcIkBzZW5kZ3JpZC9tYWlsXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwiYXhpb3NcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJidXNib3lcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJjb3JzXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwiZXhwcmVzc1wiIiwid2VicGFjazovLy9leHRlcm5hbCBcImZpcmViYXNlLWFkbWluXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwiZmlyZWJhc2UtZnVuY3Rpb25zXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwiZmlyZWJhc2UvYXBwXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwiZmlyZWJhc2UvYXV0aFwiIiwid2VicGFjazovLy9leHRlcm5hbCBcImZpcmViYXNlL2RhdGFiYXNlXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwiZmlyZWJhc2UvZmlyZXN0b3JlXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwiZnNcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJtZW1vcnktY2FjaGVcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJvc1wiIiwid2VicGFjazovLy9leHRlcm5hbCBcInBhdGhcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJyZWFjdFwiIiwid2VicGFjazovLy9leHRlcm5hbCBcInJlYWN0LWRvbS9zZXJ2ZXJcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJyZWFjdC1oZWxtZXRcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJyZWFjdC1yZWR1eFwiIiwid2VicGFjazovLy9leHRlcm5hbCBcInJlYWN0LXJvdXRlci1kb21cIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJyZWFjdC1zZWxlY3RcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJyZWR1eFwiIiwid2VicGFjazovLy9leHRlcm5hbCBcInJlZHV4LWRldnRvb2xzLWV4dGVuc2lvbi9kZXZlbG9wbWVudE9ubHlcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJyZWR1eC10aHVua1wiIiwid2VicGFjazovLy9leHRlcm5hbCBcInNlcmlhbGl6ZS1qYXZhc2NyaXB0XCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwic3R5bGVkLWNvbXBvbmVudHNcIiJdLCJuYW1lcyI6WyJhcHAiLCJmdW5jdGlvbnMiLCJvbk5ld1VzZXIiLCJ1c2VyIiwib25DcmVhdGUiLCJjb25maWciLCJzdG9yZSIsImdldFN0YXRlIiwic2VuZGdyaWQiLCJzZW5kZ3JpZENvbmZpZyIsImN1cnJlbnRMYW5ndWFnZSIsInByZWZlcmVuY2VzIiwibGFuZ3VhZ2UiLCJjdXJyZW50VHJhbnNsYXRpb25zIiwiaW50bERhdGEiLCJsb2NhbGVzIiwia2V5IiwiY29uZmlnRW1haWwiLCJ0cmFuc2xhdGlvbnMiLCJlbWFpbERhdGEiLCJ0byIsImVtYWlsIiwic3ViamVjdCIsInRyYW5zbGF0ZSIsIm1lc3NhZ2UiLCJ1c2VybmFtZSIsInNlbmROb3RpZmljYXRpb24iLCJlIiwiY29uc29sZSIsImVycm9yIiwidXBkYXRlQ29uZmlndXJhdGlvbiIsImRvY3VtZW50IiwicHJvamVjdGlkIiwib25VcGRhdGUiLCJjaGFuZ2UiLCJuZXdDb25maWciLCJhZnRlciIsImRhdGEiLCJkaXNwYXRjaCIsInNldENvbmZpZ3VyYXRpb24iLCJzZXJ2aWNlQWNjb3VudCIsInJlcXVpcmUiLCJmaXJlYmFzZUNsaWVudCIsImZpcmViYXNlQ2xpZW50TGliIiwiaW5pdGlhbGl6ZUFwcCIsImFwaUtleSIsImFwaWtleSIsImF1dGhEb21haW4iLCJhdXRoZG9tYWluIiwiZGF0YWJhc2VVUkwiLCJkYXRhYmFzZXVybCIsInByb2plY3RJZCIsInN0b3JhZ2VCdWNrZXQiLCJzdG9yYWdlYnVja2V0IiwibWVzc2FnaW5nU2VuZGVySWQiLCJtZXNzYWdpbmdzZW5kZXJpZCIsImZpcmViYXNlQWRtaW4iLCJmaXJlYmFzZUFkbWluTGliIiwiY3JlZGVudGlhbCIsImNlcnQiLCJhdXRob3JpemF0aW9uIiwicmVxdWlyZWRDbGFpbXMiLCJyZXEiLCJyZXMiLCJuZXh0IiwidG9rZW4iLCJoZWFkZXJzIiwic3RhcnRzV2l0aCIsInN0YXR1cyIsImpzb24iLCJzcGxpdCIsImF1dGgiLCJ2ZXJpZnlJZFRva2VuIiwiY2xhaW1zIiwiT2JqZWN0Iiwia2V5cyIsImxlbmd0aCIsImZpbHRlciIsIml0ZW0iLCJpbmNsdWRlcyIsImZpbGVzIiwib3B0aW9ucyIsImNvbnRlbnRUeXBlIiwiaW5kZXhPZiIsIm1heEZpbGVzIiwibWF4RmlsZVNpemUiLCJidXNib3kiLCJCdXNib3kiLCJsaW1pdHMiLCJmaWxlU2l6ZSIsIm9uIiwiZmllbGRuYW1lIiwidmFsIiwiYm9keSIsImZpbGUiLCJmaWxlbmFtZSIsImFsbG93ZWRFeHRlbnRpb25zIiwiZmluZCIsImV4dCIsImVuZHNXaXRoIiwiZXh0ZW50aW9ucyIsImpvaW4iLCJ0bXBGaWxlUGF0aCIsInBhdGgiLCJvcyIsInRtcGRpciIsImNvbmNhdCIsInBpcGUiLCJmcyIsImNyZWF0ZVdyaXRlU3RyZWFtIiwiZW5kIiwicmF3Qm9keSIsImdldEJhY2tlbmRFcnJvciIsInJvdXRlciIsImV4cHJlc3MiLCJSb3V0ZXIiLCJnZXRNb2RlbFZhbGlkYXRpb24iLCJtb2RlbCIsImFzc29jaWF0aW9ucyIsInZhbGlkYXRpb25SZXNwb25zZSIsInZhbGlkYXRlTW9kZWwiLCJnZXRDYXRhbG9nTW9kZWwiLCJhcHBMYW5ndWFnZXMiLCJlcnJvcnMiLCJnZXQiLCJkYiIsImZpcmVzdG9yZSIsInJlc3BvbnNlIiwiZ2V0RGJRdWVyeSIsInF1ZXJ5IiwiZ2V0RGJEb2N1bWVudCIsInBhcmFtcyIsImlkIiwicG9zdCIsImNyZWF0ZWRBdCIsIkRhdGUiLCJub3ciLCJwYXJlbnRzIiwiZ2V0RGJRdWVyeUJ5SWRzIiwicGFyZW50IiwiY29sbGVjdGlvbiIsImFkZCIsInBhdGNoIiwiZGJEYXRhIiwiYXNzaWduIiwidXBkYXRlZEF0IiwiZG9jIiwidXBkYXRlIiwiaW50bCIsInJhdGUiLCJhcHBMYXN0U3luYyIsImNvbmZpZ3VyYXRpb25EYXRhIiwiZ2V0Q29uZmlndXJhdGlvbk1vZGVsIiwic2V0IiwiZ2V0SW50bERhdGEiLCJheGlvcyIsImNhY2hlIiwiZ2V0TG9jYWxlTW9kZWwiLCJkZWxldGUiLCJwYXNzd29yZElzUmVxdWlyZWQiLCJnZXRSZXBvcnRNb2RlbCIsInJlcG9ydERhdGEiLCJyZWR1Y2UiLCJhY2N1bSIsIndhcm4iLCJjcmVhdGVkIiwiZ2V0VXNlck1vZGVsIiwicHJvZmlsZSIsInVpZCIsInVzZXJJZCIsImdldFVzZXIiLCJjdXN0b21DbGFpbXMiLCJzZW5kIiwiY2xhaW0iLCJzZXRDdXN0b21Vc2VyQ2xhaW1zIiwidXNlckRhdGEiLCJkYXRhVG9TYXZlIiwicHJvcGVydHkiLCJ0eXBlIiwicnVsZXMiLCJyZXF1aXJlZCIsIm1pbkxlbmd0aCIsInVwZGF0ZVVzZXIiLCJwYXNzd29yZCIsImZvcm1hdCIsInNlbmRQYXNzd29yZFJlc2V0RW1haWwiLCJzaWduSW5XaXRoRW1haWxBbmRQYXNzd29yZCIsImdldElkVG9rZW4iLCJ1c2VyUmVnaXN0ZXJlZCIsImNyZWF0ZVVzZXJXaXRoRW1haWxBbmRQYXNzd29yZCIsImlzUmVnaXN0ZXJlZCIsImNoYW5nZUN1cnJlbmN5IiwiY3VycmVuY3kiLCJnZXRDdXJyZW5jeUNvbnZlcnNpb25zIiwiZGF0ZSIsInNldEN1cnJlbmN5Q29udmVyc2lvbnMiLCJ0ZW1wIiwibG9jYWxTdG9yYWdlIiwic2V0UHJlZmVyZW5jZXMiLCJuZXdQcmVmZXJlbmNlcyIsImNoYW5nZURhdGVGb3JtYXQiLCJkYXRlRm9ybWF0IiwiY2hhbmdlTGFuZ3VhZ2UiLCJsb2FkUHJlZmVyZW5jZXMiLCJkZWZhdWx0UHJlZmVyZW5jZXMiLCJ1c2VyUHJlZmVyZW5jZXMiLCJsb2NhbFByZWZlcmVuY2VzIiwidGhlbiIsImxvZ291dCIsInJlbW92ZUl0ZW0iLCJsb2dvdXRBY3Rpb24iLCJ3aW5kb3ciLCJsb2NhdGlvbiIsImhyZWYiLCJvcmlnaW4iLCJSZWFjdCIsImNyZWF0ZUNvbnRleHQiLCJDb2xsYXBzaWJsZSIsIkNvbXBvbmVudCIsImNvbXBvbmVudERpZFVwZGF0ZSIsInByZXZQcm9wcyIsImlzT3BlbiIsInByb3BzIiwiY29sbGFwc2libGUiLCJjbGFzc0xpc3QiLCJ0b2dnbGUiLCJzZXRUaW1lb3V0Iiwib25FbnRlcmluZyIsInJlbmRlciIsImNsYXNzTmFtZSIsImNoaWxkcmVuIiwiZWxlbSIsIkZvb3RlcldyYXBwZXIiLCJzdHlsZWQiLCJmb290ZXIiLCJGb290ZXIiLCJhcHBMb2dvIiwiYXBwTmFtZSIsIkhlYWRlcldyYXBwZXIiLCJoZWFkZXIiLCJIZWFkZXIiLCJ0aXRsZSIsImRlc2NyaXB0aW9uIiwiaGlzdG9yeSIsInNob3dIZWFkaW5nIiwiYnRuTGVmdCIsImJ0blJpZ2h0IiwiTWVudU1haW4iLCJhdXRoTG9hZGVkIiwiY3VycmVudFBhdGgiLCJvbkNoYW5nZVBhdGgiLCJwcmV2ZW50RGVmYXVsdCIsImN1cnJlbnRUYXJnZXQiLCJwYXRobmFtZSIsIk1lbnVQb3BvdmVycyIsImVsZW1Qb3BvdmVyQWRtaW4iLCJzZXRFbGVtUG9wb3ZlckFkbWluIiwidXNlU3RhdGUiLCJlbGVtUG9wb3ZlclByZWZlcmVuY2VzIiwic2V0RWxlbVBvcG92ZXJQcmVmZXJlbmNlcyIsImVsZW1Qb3BvdmVyU2Vzc2lvbiIsInNldEVsZW1Qb3BvdmVyU2Vzc2lvbiIsInNob3dQb3BvdmVyQWRtaW4iLCJzZXRQb3BvdmVyQWRtaW4iLCJzaG93UG9wb3ZlclByZWZlcmVuY2VzIiwic2V0UG9wb3ZlclByZWZlcmVuY2VzIiwic2hvd1BvcG92ZXJTZXNzaW9uIiwic2V0UG9wb3ZlclNlc3Npb24iLCJvbkNoYW5nZURhdGVGb3JtYXQiLCJvbkNoYW5nZUxhbmd1YWdlIiwib25Mb2dvdXQiLCJkYXRlRm9ybWF0cyIsIm1hcCIsImxhYmVsIiwidmFsdWUiLCJsYW5ndWFnZXMiLCJjbGlja0FkbWluTWVudSIsImNsaWNrU2Vzc2lvbk1lbnUiLCJ3aWR0aCIsIm9wdGlvbiIsIk5hdmJhcldyYXBwZXIiLCJuYXYiLCJOYXZiYXIiLCJzaG93TWVudSIsInNldE1lbnUiLCJjb250ZXh0IiwidXNlQ29udGV4dCIsIkNvbnRleHQiLCJwdXNoIiwiTmF2aWdhdGlvbkJhcldyYXBwZXIiLCJzZWN0aW9uIiwiTmF2aWdhdGlvbkJhciIsImtleXdvcmRzIiwiTmV3cyIsIm5ld3MiLCJvYmoiLCJkYXRlRm9yU29ydGVkIiwicHVibGljYXRpb25EYXRlIiwic29ydCIsImEiLCJiIiwiaXRlbU5ldyIsImluZGV4IiwibGlua1RvUGFnZSIsImltYWdlIiwiUG9wb3ZlcldyYXBwZXIiLCJkaXYiLCJQb3BvdmVyIiwic3RhdGUiLCJzY3JlZW5XaWR0aCIsImNvbXBvbmVudERpZE1vdW50IiwiYWRkRXZlbnRMaXN0ZW5lciIsIm9uRG9jdW1lbnRDbGljayIsIm9uUmVzaXplV2luZG93IiwiZGlzcGF0Y2hFdmVudCIsIkN1c3RvbUV2ZW50IiwiY29tcG9uZW50V2lsbFVuTW91bnQiLCJyZW1vdmVFdmVudExpc3RlbmVyIiwiZXZlbnQiLCJ0YXJnZXQiLCJjb250YWlucyIsInBvcG92ZXIiLCJvblRvZ2dsZSIsInNldFN0YXRlIiwiaW5uZXJXaWR0aCIsImdldFBvcG92ZXJQb3NpdGlvbiIsInRhcmdldEVsZW0iLCJzdHlsZSIsInBvcG92ZXJXaWR0aCIsInRhcmdldFBvc2l0aW9uIiwiZ2V0Qm91bmRpbmdDbGllbnRSZWN0IiwidGFyZ2V0V2lkdGgiLCJjbGllbnRXaWR0aCIsInRhcmdldEhlaWdodCIsImNsaWVudEhlaWdodCIsInRhcmdldFBvc2l0aW9uWCIsIngiLCJ0YXJnZXRQb3NpdGlvblkiLCJ5IiwicG9wb3ZlclBvc2l0aW9uWCIsInBvcG92ZXJQb3NpdGlvblkiLCJ0cmFuc2Zvcm0iLCJIb21lV3JhcHBlciIsIm1haW4iLCJIb21lIiwiaXNMb2FkaW5nIiwiZmV0Y2hOZXdzIiwibWFwU3RhdGVUb1Byb3BzIiwiY2F0YWxvZyIsInBsYW4iLCJjb25uZWN0IiwiTG9naW5XcmFwcGVyIiwiTG9naW4iLCJsb2dpbiIsInNldFRva2VuIiwibWUiLCJnZXRGcm9udGVuZEVycm9yIiwiZ2VuZXJhbCIsInF1ZXJ5UGFyYW1zIiwiZ2V0UXVlcnlQYXJhbXNKc29uIiwic2VhcmNoIiwicmVwbGFjZSIsIk1haW4iLCJmaW5hbGx5IiwiY29udGV4dFR5cGUiLCJ3aXRoUm91dGVyIiwiTm90Rm91bmRXcmFwcGVyIiwiTm90Rm91bmQiLCJSZWNvdmVyUGFzc3dvcmRXcmFwcGVyIiwiUmVjb3ZlclBhc3N3b3JkIiwiZm9yZ290UGFzc3dvcmQiLCJtYXRjaCIsInJlY292ZXJQYXNzd29yZCIsInN1Y2Nlc3NNZXNzYWdlIiwiUmVnaXN0ZXJXcmFwcGVyIiwiUmVnaXN0ZXIiLCJhY3RpdmUiLCJwYXNzd29yZENvbmZpcm1hdGlvbiIsInBsYW5zIiwiZnJlZSIsInJlZ2lzdGVyV2l0aEVtYWlsIiwicmVnaXN0ZXIiLCJleGFjdCIsImNvbXBvbmVudCIsImNvbmZpZ3VyYXRpb24iLCJpbmRleEh0bWwiLCJyZWFkRmlsZVN5bmMiLCJtYWludGVuYW5jZUh0bWwiLCJhcHBTZXJ2ZXIiLCJjb3JzIiwidXNlIiwiYWNjZXB0TGFuZ3VhZ2UiLCJ0b0xvd2VyQ2FzZSIsImFwcERpc2FibGVkIiwicm91dGVzQ2F0YWxvZyIsInJvdXRlc0NvbmZpZyIsInJvdXRlc0xvY2FsZSIsInJvdXRlc1VzZXIiLCJyb3V0ZXNTb2NpYWxMZWFkZXIiLCJjdXJyZW50Um91dGUiLCJyb3V0ZXNTU1IiLCJyb3V0ZSIsIm1hdGNoUGF0aCIsInVybCIsImdldEluaXRpYWxTdGF0ZSIsImluaXRpYWxTdGF0ZSIsInN0eWxlcyIsIlNlcnZlclN0eWxlU2hlZXQiLCJyZWFjdERvbSIsInJlbmRlclRvU3RyaW5nIiwiaW5zdGFuY2UiLCJoZWxtZXQiLCJIZWxtZXQiLCJyZW5kZXJTdGF0aWMiLCJzdHlsZVRhZ3MiLCJnZXRTdHlsZVRhZ3MiLCJodG1sQXR0cmlidXRlcyIsInRvU3RyaW5nIiwiYm9keUF0dHJpYnV0ZXMiLCJtZXRhIiwibGluayIsInNlcmlhbGl6ZSIsIm9uUmVxdWVzdCIsIkFQSV9VUkwiLCJwcm9jZXNzIiwiVFlQRVMiLCJHRVRfQ0FUQUxPR1MiLCJHRVRfQ0FUQUxPRyIsIlBPU1RfQ0FUQUxPRyIsIlBBVENIX0NBVEFMT0ciLCJnZXRDYXRhbG9ncyIsInJlY29yZHMiLCJnZXRRdWVyeVBhcmFtc1N0cmluZyIsInBheWxvYWQiLCJnZXRDYXRhbG9nIiwiY3JlYXRlQ2F0YWxvZyIsInVwZGF0ZUNhdGFsb2ciLCJHRVRfQ1VSUkVOQ1lfQ09OVkVSU0lPTlMiLCJTRVRfQ09ORklHVVJBVElPTiIsIlNFVF9DVVJSRU5DWV9DT05WRVJTSU9OUyIsIlNFVF9QUkVGRVJFTkNFUyIsInN5bmNDb25maWd1cmF0aW9uIiwiR0VUX0RFVkVMT1BFUlNfSU5GTyIsIkdFVF9GQVFfSU5GTyIsIkdFVF9QUklWQUNZX0lORk8iLCJHRVRfUExBTl9GRUFUVVJFUyIsIkdFVF9URVJNU19JTkZPIiwiZ2V0RGV2ZWxvcGVyc0luZm8iLCJob21lIiwib3JkZXIiLCJ3aGVyZSIsImNhdGFsb2dzIiwiZGV2ZWxvcGVyc0RvY3MiLCJkZXZlbG9wZXJzSW5mbyIsImdldEZhcUluZm8iLCJmYXFJbmZvIiwiZ2V0UGxhbkZlYXR1cmVzIiwicGxhbkZlYXR1cmVzIiwiZ2V0UHJpdmFjeUluZm8iLCJwcml2YWN5SW5mbyIsImdldFRlcm1zSW5mbyIsInRlcm1zSW5mbyIsIkdFVF9MT0NBTEVTIiwiR0VUX0xPQ0FMRSIsIlBPU1RfTE9DQUxFIiwiUEFUQ0hfTE9DQUxFIiwiREVMRVRFX0xPQ0FMRSIsImdldExvY2FsZXMiLCJsb2NhbGUiLCJnZXRMb2NhbGUiLCJjcmVhdGVMb2NhbGUiLCJ1cGRhdGVMb2NhbGUiLCJkZWxldGVMb2NhbGUiLCJHRVRfQkFDS1VQIiwiR0VUX1VTRVJTIiwiR0VUX1VTRVIiLCJHRVRfQ1VTVE9NRVJfSU5GTyIsIkdFVF9DVVNUT01FUl9SRUNFSVBUUyIsIlBBVENIX1VTRVIiLCJDSEFOR0VfUEFTU1dPUkQiLCJMT0dJTiIsIkxPR09VVCIsIlJFR0lTVEVSIiwiTUUiLCJSRUNPVkVSX1BBU1NXT1JEIiwiU0VUX1RPS0VOIiwiR0VUX1VTRVJfQ0xBSU1TIiwiUE9TVF9VU0VSX0NMQUlNIiwiREVMRVRFX1VTRVJfQ0xBSU0iLCJVUERBVEVfQ1JFRElUX0NBUkQiLCJDUkVBVEVfU1VCU0NSSVBUSU9OIiwiVVBEQVRFX1NVQlNDUklQVElPTiIsIkNSRUFURV9SRVBPUlQiLCJnZXRCYWNrdXBFeGNlbCIsInllYXIiLCJiYWNrdXAiLCJyZXNwb25zZVR5cGUiLCJnZXRVc2VycyIsImdldEN1c3RvbWVySW5mbyIsImdldEN1c3RvbWVyUmVjZWlwdHMiLCJjdXN0b21lclJlY2VpcHRzIiwiY2hhbmdlUGFzc3dvcmQiLCJtZXNzc2FnZSIsImdldFVzZXJDbGFpbXMiLCJjcmVhdGVVc2VyQ2xhaW0iLCJkZWxldGVVc2VyQ2xhaW0iLCJ1cGRhdGVDcmVkaXRDYXJkIiwiY3VzdG9tZXJDb2RlIiwiY3JlZGl0Q2FyZCIsImNyZWF0ZVN1YnNjcmlwdGlvbiIsInVwZGF0ZVN1YnNjcmlwdGlvbiIsImNyZWF0ZVJlcG9ydCIsInJlcXVlc3QiLCJhY3Rpb24iLCJkZWZhdWx0cyIsImNvbW1vbiIsInRodW5rTWlkZGxld2FyZSIsInJlZHVjZXIiLCJnZXRUcmFuc2xhdGlvbnMiLCJhcHBEaXNhYmxlZE1lc3NhZ2UiLCJhcHBVcmwiLCJjYWxsaW5nQ29kZXMiLCJjb3VudHJpZXMiLCJjdXJyZW5jaWVzIiwiY3VycmVuY3lDb252ZXJzaW9ucyIsImVtYWlscyIsIm5vcmVwbHkiLCJzdXBwb3J0Iiwic3RhbmRhcmQiLCJwcmVtaXVtIiwibmV3U3RhdGUiLCJjdXN0b21lckluZm8iLCJyZWR1Y2VycyIsImNvbWJpbmVSZWR1Y2VycyIsImNhdGFsb2dSZWR1Y2VyIiwiY29uZmlnUmVkdWNlciIsImhvbWVSZWR1Y2VyIiwibG9jYWxlUmVkdWNlciIsInVzZXJSZWR1Y2VyIiwibWlkZGxld2FyZSIsImNvbXBvc2VXaXRoRGV2VG9vbHMiLCJhcHBseU1pZGRsZXdhcmUiLCJyZXF1ZXN0TWlkZGxld2FyZSIsImNyZWF0ZVN0b3JlIiwiZG9jdW1lbnRJZCIsInBhcnNlZFF1ZXJ5IiwiZ2V0RGJQYXJzZWRRdWVyeSIsImRiUXVlcnkiLCJmb3JFYWNoIiwib3JkZXJCeSIsImxpbWl0IiwiZG9jcyIsInNlbGVjdCIsImlkcyIsImRvY3VtZW50UmVmZXJlbmNlcyIsIlByb21pc2UiLCJhbGwiLCJpc0VtcHR5Iiwic2VhcmNoQ3JpdGVyaWEiLCJjcml0ZXJpYSIsInJhbmdlIiwic3RhcnQiLCJDQUNIRSIsIklOVExfREFUQSIsIkNVUlJFTkNZX0NPTlZFUlNJT05TIiwiZm9ybWF0RGF0ZSIsImRhdGV0aW1lIiwiZGF5IiwiZ2V0RGF0ZSIsIm1vbnRoIiwiZ2V0TW9udGgiLCJnZXRGdWxsWWVhciIsIlJlZ0V4cCIsInBhZFN0YXJ0IiwiZm9ybWF0TG9jYWxlcyIsImxhbmciLCJuYW1lIiwiZm9ybWF0TW9uZXkiLCJ0aG91c2FuZHMiLCJuZWdhdGl2ZVNpZ24iLCJpIiwiTWF0aCIsInJvdW5kIiwiaiIsInN1YnN0ciIsImludGxBUEkiLCJzb3VyY2VJbnRsIiwiY2FsbGluZ0NvZGUiLCJhcnJheSIsImZpbmRJbmRleCIsImxvY2FsZUNvbXBhcmUiLCJhbHBoYTNDb2RlIiwiY3VycmVuY3lJbmZvIiwiY29kZSIsInRvVXBwZXJDYXNlIiwibGFuZ3VhZ2VJbmZvIiwiaXNvNjM5XzEiLCJmb3JtYXR0ZWRMb2NhbGVzIiwicHV0IiwibG9jYWxlVmFsdWUiLCJmb3JtYXRUZW1wbGF0ZSIsImVtYWlsTGlicmFyeSIsInNldEFwaUtleSIsImZyb20iLCJ0ZW1wbGF0ZUlkIiwidGVtcGxhdGVnZW5lcmFsIiwic2xvZ2FuIiwiYXNzb2NpYXRpb24iLCJ2YWxpZGF0aW9ucyIsImVxdWFsVG8iLCJkZWZhdWx0IiwiZ2V0RG93bmxvYWQiLCJjcmVhdGVFbGVtZW50IiwiYXBwZW5kQ2hpbGQiLCJVUkwiLCJjcmVhdGVPYmplY3RVUkwiLCJCbG9iIiwiZG93bmxvYWQiLCJjbGljayIsInJlbW92ZSIsInJldm9rZU9iamVjdFVSTCIsInF1ZXJ5U3RyaW5nIiwicXVlcnlLZXlzIiwiSlNPTiIsInN0cmluZ2lmeSIsInVybFBhcmFtcyIsIlVSTFNlYXJjaFBhcmFtcyIsInBhcmFtVmFsdWUiLCJBcnJheSIsInBhcnNlIiwic3RhY2siLCJ0ZW1wbGF0ZSIsImxpa2UiLCJ0b1VybCIsImVuY29kZWRVcmwiLCJpc1Byb3BlcnR5RW1wdHkiLCJ1bmRlZmluZWQiLCJpc0FycmF5IiwidHJpbSIsImdldFByb3BlcnR5VmFsaWRhdGlvbiIsInByb3BlcnR5RGF0YSIsInByb3BlcnR5UnVsZXMiLCJwcm9wZXJ0eVR5cGUiLCJwcm9wZXJ0eVZhbHVlIiwicHJvcGVydHlWYWx1ZVR5cGUiLCJkYXRhVHlwZSIsImlzUHJvcGVydHlSZXF1aXJlZCIsInJ1bGUiLCJpc0VtYWlsIiwiaXNVcmwiLCJ2YWx1ZXMiLCJtYXhMZW5ndGgiLCJyZWdFeHBFbWFpbCIsInRlc3QiLCJpc0h0bWwiLCJpbm5lckhUTUwiLCJjIiwiY2hpbGROb2RlcyIsIm5vZGVUeXBlIiwicGF0dGVybiIsInZhbGlkYXRpb24iLCJmbGF0TWFwIiwiaXRlbVZhbHVlIiwicHJvdG90eXBlIiwiY2FsbCJdLCJtYXBwaW5ncyI6Ijs7UUFBQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTs7O1FBR0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDBDQUEwQyxnQ0FBZ0M7UUFDMUU7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSx3REFBd0Qsa0JBQWtCO1FBQzFFO1FBQ0EsaURBQWlELGNBQWM7UUFDL0Q7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBLHlDQUF5QyxpQ0FBaUM7UUFDMUUsZ0hBQWdILG1CQUFtQixFQUFFO1FBQ3JJO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMkJBQTJCLDBCQUEwQixFQUFFO1FBQ3ZELGlDQUFpQyxlQUFlO1FBQ2hEO1FBQ0E7UUFDQTs7UUFFQTtRQUNBLHNEQUFzRCwrREFBK0Q7O1FBRXJIO1FBQ0E7OztRQUdBO1FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDbEZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE1BQU07QUFBRUE7QUFBRixJQUFVQyx5REFBQSxFQUFoQixDLENBRUE7O0FBQ08sTUFBTUMsU0FBUyxHQUFHRCx1REFBQSxDQUN0QkUsSUFEc0IsR0FFdEJDLFFBRnNCLENBRWIsTUFBTUQsSUFBTixJQUFjO0FBQ3RCLE1BQUk7QUFDRixVQUFNO0FBQUVFO0FBQUYsUUFBYUMsb0RBQUssQ0FBQ0MsUUFBTixFQUFuQjtBQUNBLFVBQU07QUFBRUMsY0FBUSxFQUFFQztBQUFaLFFBQStCUix5REFBQSxFQUFyQztBQUNBLFVBQU1TLGVBQWUsR0FBSVAsSUFBSSxDQUFDUSxXQUFMLElBQW9CUixJQUFJLENBQUNRLFdBQUwsQ0FBaUJDLFFBQXRDLElBQW1ELElBQTNFO0FBQ0EsVUFBTUMsbUJBQW1CLEdBQUdSLE1BQU0sQ0FBQ1MsUUFBUCxDQUFnQkMsT0FBaEIsQ0FBd0JMLGVBQXhCLENBQTVCO0FBQ0FGLDREQUFBLENBQW1CQyxjQUFjLENBQUNPLEdBQWxDOztBQUVBLFVBQU1DLFdBQVcscUJBQ1poQix5REFBQSxFQURZO0FBRWZpQixrQkFBWSxFQUFFTDtBQUZDLE1BQWpCOztBQUlBLFVBQU1NLFNBQVMsR0FBRztBQUNoQkMsUUFBRSxFQUFFakIsSUFBSSxDQUFDa0IsS0FETztBQUVoQkMsYUFBTyxFQUFFQyxtRUFBUyxDQUFDVixtQkFBRCxFQUFzQixlQUF0QixDQUZGO0FBR2hCVyxhQUFPLEVBQUVELG1FQUFTLENBQUNWLG1CQUFELEVBQXNCLGVBQXRCLEVBQXVDO0FBQUVZLGdCQUFRLEVBQUV0QixJQUFJLENBQUNrQjtBQUFqQixPQUF2QztBQUhGLEtBQWxCO0FBS0EsVUFBTUssMEVBQWdCLENBQUNULFdBQUQsRUFBY1QsMkNBQWQsRUFBd0JXLFNBQXhCLENBQXRCO0FBQ0QsR0FqQkQsQ0FpQkUsT0FBT1EsQ0FBUCxFQUFVO0FBQ1ZDLFdBQU8sQ0FBQ0MsS0FBUixDQUFjLGdDQUFkLEVBQWdERixDQUFoRDtBQUNEO0FBQ0YsQ0F2QnNCLENBQWxCO0FBeUJFLE1BQU1HLG1CQUFtQixHQUFHN0IsNERBQUEsQ0FDaEM4QixRQURnQyxDQUN0QixpQkFBZ0IvQixHQUFHLENBQUNnQyxTQUFVLEVBRFIsRUFFaENDLFFBRmdDLENBRXZCQyxNQUFNLElBQUk7QUFDbEIsUUFBTUMsU0FBUyxHQUFHRCxNQUFNLENBQUNFLEtBQVAsQ0FBYUMsSUFBYixFQUFsQjtBQUNBL0Isc0RBQUssQ0FBQ2dDLFFBQU4sQ0FBZUMsc0ZBQWdCLENBQUNKLFNBQUQsQ0FBL0I7QUFDRCxDQUxnQyxDQUE1QixDOzs7Ozs7Ozs7Ozs7QUNsQ1Q7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLE1BQU1LLGNBQWMsR0FBR0MsbUJBQU8sQ0FBQywwREFBRCxDQUE5Qjs7QUFDQSxNQUFNO0FBQUV6QztBQUFGLElBQVVDLHlEQUFBLEVBQWhCO0FBRU8sTUFBTXlDLGNBQWMsR0FBR0MsbURBQWlCLENBQUNDLGFBQWxCLENBQWdDO0FBQzVEQyxRQUFNLEVBQUU3QyxHQUFHLENBQUM4QyxNQURnRDtBQUU1REMsWUFBVSxFQUFFL0MsR0FBRyxDQUFDZ0QsVUFGNEM7QUFHNURDLGFBQVcsRUFBRWpELEdBQUcsQ0FBQ2tELFdBSDJDO0FBSTVEQyxXQUFTLEVBQUVuRCxHQUFHLENBQUNnQyxTQUo2QztBQUs1RG9CLGVBQWEsRUFBRXBELEdBQUcsQ0FBQ3FELGFBTHlDO0FBTTVEQyxtQkFBaUIsRUFBRXRELEdBQUcsQ0FBQ3VEO0FBTnFDLENBQWhDLENBQXZCO0FBU0EsTUFBTUMsYUFBYSxHQUFHQyw0REFBQSxDQUErQjtBQUMxREMsWUFBVSxFQUFFRCx5REFBQSxDQUE0QkUsSUFBNUIsQ0FBaUNuQixjQUFqQyxDQUQ4QztBQUUxRFMsYUFBVyxFQUFFakQsR0FBRyxDQUFDa0Q7QUFGeUMsQ0FBL0IsQ0FBdEIsQzs7Ozs7Ozs7Ozs7O0FDbkJQO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVPLFNBQVNVLGFBQVQsQ0FBdUJDLGNBQXZCLEVBQXVDO0FBQzVDLFNBQU8sT0FBT0MsR0FBUCxFQUFZQyxHQUFaLEVBQWlCQyxJQUFqQixLQUEwQjtBQUMvQixRQUFJO0FBQ0YsVUFBSUMsS0FBSyxHQUFHSCxHQUFHLENBQUNJLE9BQUosQ0FBWSxlQUFaLEtBQWdDLEVBQTVDLENBREUsQ0FHRjs7QUFDQSxVQUFJLENBQUNELEtBQUQsSUFBVSxDQUFDQSxLQUFLLENBQUNFLFVBQU4sQ0FBaUIsU0FBakIsQ0FBZixFQUE0QztBQUMxQ0osV0FBRyxDQUFDSyxNQUFKLENBQVcsR0FBWCxFQUFnQkMsSUFBaEIsQ0FBcUI7QUFBRTdDLGlCQUFPLEVBQUVELG1FQUFTLENBQUN1QyxHQUFHLENBQUM1QyxZQUFMLEVBQW1CLGNBQW5CO0FBQXBCLFNBQXJCO0FBQ0E7QUFDRCxPQVBDLENBU0Y7OztBQUNBK0MsV0FBSyxHQUFHQSxLQUFLLENBQUNLLEtBQU4sQ0FBWSxTQUFaLEVBQXVCLENBQXZCLENBQVI7QUFDQVIsU0FBRyxDQUFDM0QsSUFBSixHQUFXLE1BQU1xRCx1REFBYSxDQUFDZSxJQUFkLEdBQXFCQyxhQUFyQixDQUFtQ1AsS0FBbkMsQ0FBakI7QUFDQUgsU0FBRyxDQUFDM0QsSUFBSixDQUFTc0UsTUFBVCxHQUFrQkMsTUFBTSxDQUFDQyxJQUFQLENBQVliLEdBQUcsQ0FBQzNELElBQWhCLENBQWxCLENBWkUsQ0FjRjs7QUFDQSxVQUNFMEQsY0FBYyxDQUFDZSxNQUFmLEdBQXdCLENBQXhCLElBQ0EsQ0FBQ2QsR0FBRyxDQUFDM0QsSUFBSixDQUFTc0UsTUFBVCxDQUFnQkksTUFBaEIsQ0FBdUJDLElBQUksSUFBSWpCLGNBQWMsQ0FBQ2tCLFFBQWYsQ0FBd0JELElBQXhCLENBQS9CLEVBQThERixNQUZqRSxFQUdFO0FBQ0FiLFdBQUcsQ0FBQ0ssTUFBSixDQUFXLEdBQVgsRUFBZ0JDLElBQWhCLENBQXFCO0FBQUU3QyxpQkFBTyxFQUFFRCxtRUFBUyxDQUFDdUMsR0FBRyxDQUFDNUMsWUFBTCxFQUFtQixtQkFBbkI7QUFBcEIsU0FBckI7QUFDQTtBQUNEOztBQUVEOEMsVUFBSTtBQUNMLEtBeEJELENBd0JFLE9BQU9yQyxDQUFQLEVBQVU7QUFDVm9DLFNBQUcsQ0FBQ0ssTUFBSixDQUFXLEdBQVgsRUFBZ0JDLElBQWhCLENBQXFCO0FBQUU3QyxlQUFPLEVBQUVELG1FQUFTLENBQUN1QyxHQUFHLENBQUM1QyxZQUFMLEVBQW1CLGNBQW5CO0FBQXBCLE9BQXJCO0FBQ0Q7QUFDRixHQTVCRDtBQTZCRDtBQUVNLFNBQVM4RCxLQUFULENBQWVDLE9BQWYsRUFBd0I7QUFDN0IsU0FBTyxPQUFPbkIsR0FBUCxFQUFZQyxHQUFaLEVBQWlCQyxJQUFqQixLQUEwQjtBQUMvQixRQUFJO0FBQ0YsWUFBTWtCLFdBQVcsR0FBR3BCLEdBQUcsQ0FBQ0ksT0FBSixDQUFZLGNBQVosQ0FBcEI7O0FBRUEsVUFBSSxDQUFDZ0IsV0FBRCxJQUFnQkEsV0FBVyxDQUFDQyxPQUFaLENBQW9CLGtCQUFwQixJQUEwQyxDQUFDLENBQS9ELEVBQWtFO0FBQ2hFLGVBQU9uQixJQUFJLEVBQVg7QUFDRDs7QUFFRCxZQUFNO0FBQUVvQixnQkFBRjtBQUFZQztBQUFaLFVBQTRCSixPQUFsQztBQUNBLFlBQU1LLE1BQU0sR0FBRyxJQUFJQyw2Q0FBSixDQUFXO0FBQ3hCckIsZUFBTyxFQUFFSixHQUFHLENBQUNJLE9BRFc7QUFFeEJzQixjQUFNLEVBQUU7QUFDTlIsZUFBSyxFQUFFSSxRQUFRLElBQUksQ0FEYjtBQUVOSyxrQkFBUSxFQUFFLENBQUNKLFdBQVcsSUFBSSxDQUFoQixJQUFxQixJQUFyQixHQUE0QjtBQUZoQztBQUZnQixPQUFYLENBQWY7QUFRQUMsWUFBTSxDQUFDSSxFQUFQLENBQVUsT0FBVixFQUFtQixDQUFDQyxTQUFELEVBQVlDLEdBQVosS0FBcUI5QixHQUFHLENBQUMrQixJQUFKLENBQVNGLFNBQVQsSUFBc0JDLEdBQTlEO0FBQ0FOLFlBQU0sQ0FBQ0ksRUFBUCxDQUFVLE1BQVYsRUFBa0IsQ0FBQ0MsU0FBRCxFQUFZRyxJQUFaLEVBQWtCQyxRQUFsQixLQUErQjtBQUMvQyxjQUFNO0FBQUVDO0FBQUYsWUFBd0JmLE9BQTlCOztBQUVBLFlBQUksQ0FBQ2UsaUJBQWlCLENBQUNDLElBQWxCLENBQXVCQyxHQUFHLElBQUlILFFBQVEsQ0FBQ0ksUUFBVCxDQUFrQixNQUFNRCxHQUF4QixDQUE5QixDQUFMLEVBQWtFO0FBQ2hFbkMsYUFBRyxDQUFDSyxNQUFKLENBQVcsR0FBWCxFQUFnQkMsSUFBaEIsQ0FBcUI7QUFDbkI3QyxtQkFBTyxFQUFFRCxtRUFBUyxDQUFDdUMsR0FBRyxDQUFDNUMsWUFBTCxFQUFtQix1QkFBbkIsRUFBNEM7QUFDNURrRix3QkFBVSxFQUFFSixpQkFBaUIsQ0FBQ0ssSUFBbEIsQ0FBdUIsSUFBdkI7QUFEZ0QsYUFBNUM7QUFEQyxXQUFyQjtBQUtBO0FBQ0Q7O0FBRURQLFlBQUksQ0FBQ0osRUFBTCxDQUFRLE9BQVIsRUFBaUIsTUFBTTtBQUNyQjNCLGFBQUcsQ0FBQ0ssTUFBSixDQUFXLEdBQVgsRUFBZ0JDLElBQWhCLENBQXFCO0FBQ25CN0MsbUJBQU8sRUFBRUQsbUVBQVMsQ0FBQ3VDLEdBQUcsQ0FBQzVDLFlBQUwsRUFBbUIsaUJBQW5CLEVBQXNDO0FBQUVrRSxzQkFBRjtBQUFZQztBQUFaLGFBQXRDO0FBREMsV0FBckI7QUFHRCxTQUpEO0FBTUEsWUFBSWlCLFdBQVcsR0FBR0MsMkNBQUksQ0FBQ0YsSUFBTCxDQUFVRyx5Q0FBRSxDQUFDQyxNQUFILEVBQVYsRUFBdUJWLFFBQXZCLENBQWxCO0FBQ0FqQyxXQUFHLENBQUMrQixJQUFKLENBQVNGLFNBQVQsSUFBc0IsQ0FBQzdCLEdBQUcsQ0FBQytCLElBQUosQ0FBU0YsU0FBVCxLQUF1QixFQUF4QixFQUE0QmUsTUFBNUIsQ0FBbUNKLFdBQW5DLENBQXRCO0FBQ0FSLFlBQUksQ0FBQ2EsSUFBTCxDQUFVQyx5Q0FBRSxDQUFDQyxpQkFBSCxDQUFxQlAsV0FBckIsQ0FBVjtBQUNELE9BckJEO0FBc0JBaEIsWUFBTSxDQUFDSSxFQUFQLENBQVUsUUFBVixFQUFvQixNQUFNMUIsSUFBSSxFQUE5QjtBQUNBc0IsWUFBTSxDQUFDd0IsR0FBUCxDQUFXaEQsR0FBRyxDQUFDaUQsT0FBZjtBQUNELEtBekNELENBeUNFLE9BQU9wRixDQUFQLEVBQVU7QUFDVixZQUFNRSxLQUFLLEdBQUdtRiw2RUFBZSxDQUFDckYsQ0FBRCxDQUE3QjtBQUNBb0MsU0FBRyxDQUFDSyxNQUFKLENBQVcsR0FBWCxFQUFnQkMsSUFBaEIsQ0FBcUJ4QyxLQUFyQjtBQUNEO0FBQ0YsR0E5Q0Q7QUErQ0QsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDeEZEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtDQUdBOztBQUNBLE1BQU1vRixNQUFNLEdBQUdDLDhDQUFPLENBQUNDLE1BQVIsRUFBZjs7QUFDQSxNQUFNQyxrQkFBa0IsR0FBRyxPQUFPdEQsR0FBUCxFQUFZdUQsS0FBWixFQUFtQkMsWUFBbkIsS0FBb0M7QUFDN0Q7QUFDQSxRQUFNQyxrQkFBa0IsR0FBR0MsNkVBQWEsQ0FBQzFELEdBQUQsRUFBTXVELEtBQU4sRUFBYUksMEVBQWUsQ0FBQ0gsWUFBRCxFQUFleEQsR0FBRyxDQUFDekQsTUFBSixDQUFXcUgsWUFBMUIsQ0FBNUIsQ0FBeEM7QUFDQSxRQUFNQyxNQUFNLEdBQUdKLGtCQUFrQixDQUFDSSxNQUFsQztBQUNBLE1BQUl0RixJQUFJLEdBQUdrRixrQkFBa0IsQ0FBQ2xGLElBQTlCO0FBRUEsU0FBTztBQUFFc0YsVUFBRjtBQUFVdEY7QUFBVixHQUFQO0FBQ0QsQ0FQRCxDLENBU0E7OztBQUNBNEUsTUFBTSxDQUFDVyxHQUFQLENBQVcsR0FBWCxFQUFnQixPQUFPOUQsR0FBUCxFQUFZQyxHQUFaLEtBQW9CO0FBQ2xDLE1BQUk7QUFDRixVQUFNOEQsRUFBRSxHQUFHckUsdURBQWEsQ0FBQ3NFLFNBQWQsRUFBWDtBQUNBLFVBQU1DLFFBQVEsR0FBRyxNQUFNQyx3RUFBVSxDQUFDSCxFQUFELEVBQUssVUFBTCxFQUFpQi9ELEdBQUcsQ0FBQ21FLEtBQXJCLENBQWpDO0FBRUFsRSxPQUFHLENBQUNNLElBQUosQ0FBUzBELFFBQVQ7QUFDRCxHQUxELENBS0UsT0FBT3BHLENBQVAsRUFBVTtBQUNWLFVBQU1FLEtBQUssR0FBR21GLDZFQUFlLENBQUNyRixDQUFELENBQTdCO0FBQ0FvQyxPQUFHLENBQUNLLE1BQUosQ0FBVyxHQUFYLEVBQWdCQyxJQUFoQixDQUFxQnhDLEtBQXJCO0FBQ0Q7QUFDRixDQVZEO0FBWUFvRixNQUFNLENBQUNXLEdBQVAsQ0FBVyxNQUFYLEVBQW1CLE9BQU85RCxHQUFQLEVBQVlDLEdBQVosS0FBb0I7QUFDckMsTUFBSTtBQUNGLFVBQU04RCxFQUFFLEdBQUdyRSx1REFBYSxDQUFDc0UsU0FBZCxFQUFYO0FBQ0EsVUFBTUMsUUFBUSxHQUFHLE1BQU1HLDJFQUFhLENBQUNMLEVBQUQsRUFBSyxVQUFMLEVBQWlCL0QsR0FBRyxDQUFDcUUsTUFBSixDQUFXQyxFQUE1QixDQUFwQztBQUVBckUsT0FBRyxDQUFDTSxJQUFKLENBQVMwRCxRQUFUO0FBQ0QsR0FMRCxDQUtFLE9BQU9wRyxDQUFQLEVBQVU7QUFDVixVQUFNRSxLQUFLLEdBQUdtRiw2RUFBZSxDQUFDckYsQ0FBRCxDQUE3QjtBQUNBb0MsT0FBRyxDQUFDSyxNQUFKLENBQVcsR0FBWCxFQUFnQkMsSUFBaEIsQ0FBcUJ4QyxLQUFyQjtBQUNEO0FBQ0YsQ0FWRDtBQVlBb0YsTUFBTSxDQUFDb0IsSUFBUCxDQUFZLEdBQVosRUFBaUJ6RSxrRUFBYSxDQUFDLENBQUMsU0FBRCxDQUFELENBQTlCLEVBQTZDLE9BQU9FLEdBQVAsRUFBWUMsR0FBWixLQUFvQjtBQUMvRCxNQUFJO0FBQ0YsVUFBTXNELEtBQUsscUJBQVF2RCxHQUFHLENBQUMrQixJQUFaO0FBQWtCeUMsZUFBUyxFQUFFQyxJQUFJLENBQUNDLEdBQUw7QUFBN0IsTUFBWDs7QUFDQSxVQUFNWCxFQUFFLEdBQUdyRSx1REFBYSxDQUFDc0UsU0FBZCxFQUFYO0FBQ0EsVUFBTVcsT0FBTyxHQUFHLE1BQU1DLDZFQUFlLENBQUNiLEVBQUQsRUFBSyxVQUFMLEVBQWlCLENBQUNSLEtBQUssQ0FBQ3NCLE1BQVAsQ0FBakIsQ0FBckM7QUFDQSxVQUFNO0FBQUVoQixZQUFGO0FBQVV0RjtBQUFWLFFBQW1CLE1BQU0rRSxrQkFBa0IsQ0FBQ3RELEdBQUQsRUFBTXVELEtBQU4sRUFBYTtBQUFFb0I7QUFBRixLQUFiLENBQWpEOztBQUVBLFFBQUkvRCxNQUFNLENBQUNDLElBQVAsQ0FBWWdELE1BQVosRUFBb0IvQyxNQUFwQixHQUE2QixDQUFqQyxFQUFvQztBQUNsQyxhQUFPYixHQUFHLENBQUNLLE1BQUosQ0FBVyxHQUFYLEVBQWdCQyxJQUFoQixDQUFxQjtBQUFFc0Q7QUFBRixPQUFyQixDQUFQO0FBQ0Q7O0FBRUQsUUFBSUksUUFBUSxHQUFHLE1BQU1GLEVBQUUsQ0FBQ2UsVUFBSCxDQUFjLFVBQWQsRUFBMEJDLEdBQTFCLENBQThCeEcsSUFBOUIsQ0FBckI7QUFFQTBGLFlBQVEscUJBQVExRixJQUFSO0FBQWMrRixRQUFFLEVBQUVMLFFBQVEsQ0FBQ0s7QUFBM0IsTUFBUjtBQUNBckUsT0FBRyxDQUFDTSxJQUFKLENBQVMwRCxRQUFUO0FBQ0QsR0FkRCxDQWNFLE9BQU9wRyxDQUFQLEVBQVU7QUFDVixVQUFNRSxLQUFLLEdBQUdtRiw2RUFBZSxDQUFDckYsQ0FBRCxDQUE3QjtBQUNBb0MsT0FBRyxDQUFDSyxNQUFKLENBQVcsR0FBWCxFQUFnQkMsSUFBaEIsQ0FBcUJ4QyxLQUFyQjtBQUNEO0FBQ0YsQ0FuQkQ7QUFxQkFvRixNQUFNLENBQUM2QixLQUFQLENBQWEsTUFBYixFQUFxQmxGLGtFQUFhLENBQUMsQ0FBQyxTQUFELENBQUQsQ0FBbEMsRUFBaUQsT0FBT0UsR0FBUCxFQUFZQyxHQUFaLEtBQW9CO0FBQ25FLE1BQUk7QUFDRixVQUFNOEQsRUFBRSxHQUFHckUsdURBQWEsQ0FBQ3NFLFNBQWQsRUFBWDtBQUNBLFVBQU1pQixNQUFNLEdBQUcsTUFBTWIsMkVBQWEsQ0FBQ0wsRUFBRCxFQUFLLFVBQUwsRUFBaUIvRCxHQUFHLENBQUNxRSxNQUFKLENBQVdDLEVBQTVCLENBQWxDO0FBQ0EsVUFBTWYsS0FBSyxHQUFHM0MsTUFBTSxDQUFDc0UsTUFBUCxDQUFjRCxNQUFkLG9CQUEyQmpGLEdBQUcsQ0FBQytCLElBQS9CO0FBQXFDb0QsZUFBUyxFQUFFVixJQUFJLENBQUNDLEdBQUw7QUFBaEQsT0FBZDtBQUNBLFVBQU1DLE9BQU8sR0FBRyxNQUFNQyw2RUFBZSxDQUFDYixFQUFELEVBQUssVUFBTCxFQUFpQixDQUFDUixLQUFLLENBQUNzQixNQUFQLENBQWpCLENBQXJDO0FBQ0EsVUFBTTtBQUFFaEIsWUFBRjtBQUFVdEY7QUFBVixRQUFtQixNQUFNK0Usa0JBQWtCLENBQUN0RCxHQUFELEVBQU11RCxLQUFOLEVBQWE7QUFBRW9CO0FBQUYsS0FBYixDQUFqRDs7QUFFQSxRQUFJL0QsTUFBTSxDQUFDQyxJQUFQLENBQVlnRCxNQUFaLEVBQW9CL0MsTUFBcEIsR0FBNkIsQ0FBakMsRUFBb0M7QUFDbEMsYUFBT2IsR0FBRyxDQUFDSyxNQUFKLENBQVcsR0FBWCxFQUFnQkMsSUFBaEIsQ0FBcUI7QUFBRXNEO0FBQUYsT0FBckIsQ0FBUDtBQUNEOztBQUVELFVBQU1FLEVBQUUsQ0FDTGUsVUFERyxDQUNRLFVBRFIsRUFFSE0sR0FGRyxDQUVDcEYsR0FBRyxDQUFDcUUsTUFBSixDQUFXQyxFQUZaLEVBR0hlLE1BSEcsQ0FHSTlHLElBSEosQ0FBTjs7QUFLQSxVQUFNMEYsUUFBUSxxQkFBUTFGLElBQVI7QUFBYytGLFFBQUUsRUFBRXRFLEdBQUcsQ0FBQ3FFLE1BQUosQ0FBV0M7QUFBN0IsTUFBZDs7QUFDQXJFLE9BQUcsQ0FBQ00sSUFBSixDQUFTMEQsUUFBVDtBQUNELEdBbEJELENBa0JFLE9BQU9wRyxDQUFQLEVBQVU7QUFDVixVQUFNRSxLQUFLLEdBQUdtRiw2RUFBZSxDQUFDckYsQ0FBRCxDQUE3QjtBQUNBb0MsT0FBRyxDQUFDSyxNQUFKLENBQVcsR0FBWCxFQUFnQkMsSUFBaEIsQ0FBcUJ4QyxLQUFyQjtBQUNEO0FBQ0YsQ0F2QkQ7QUF5QmVvRixxRUFBZixFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDMUZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQSxNQUFNQSxNQUFNLEdBQUdDLDhDQUFPLENBQUNDLE1BQVIsRUFBZixDLENBRUE7O0FBQ0FGLE1BQU0sQ0FBQ29CLElBQVAsQ0FBWSxPQUFaLEVBQXFCekUsa0VBQWEsQ0FBQyxDQUFDLFNBQUQsQ0FBRCxDQUFsQyxFQUFpRCxPQUFPRSxHQUFQLEVBQVlDLEdBQVosS0FBb0I7QUFDbkUsTUFBSTtBQUNGLFVBQU07QUFBRS9ELFNBQUY7QUFBT29KLFVBQVA7QUFBYUM7QUFBYixRQUFzQnBKLHlEQUFBLEVBQTVCO0FBQ0EsVUFBTTRILEVBQUUsR0FBR3JFLHVEQUFhLENBQUNzRSxTQUFkLEVBQVg7QUFDQSxVQUFNaUIsTUFBTSxHQUFHLE1BQU1iLDJFQUFhLENBQUNMLEVBQUQsRUFBSyxlQUFMLEVBQXNCN0gsR0FBRyxDQUFDZ0MsU0FBMUIsQ0FBbEM7QUFDQSxVQUFNcUYsS0FBSyxHQUFHM0MsTUFBTSxDQUFDc0UsTUFBUCxDQUFjRCxNQUFkLG9CQUEyQmpGLEdBQUcsQ0FBQytCLElBQS9CO0FBQXFDeUQsaUJBQVcsRUFBRWYsSUFBSSxDQUFDQyxHQUFMO0FBQWxELE9BQWQ7QUFDQSxVQUFNO0FBQUViLFlBQUY7QUFBVXRGLFVBQUksRUFBRWtIO0FBQWhCLFFBQXNDL0IsOEVBQWEsQ0FBQzFELEdBQUQsRUFBTXVELEtBQU4sRUFBYW1DLGdGQUFxQixFQUFsQyxDQUF6RDs7QUFFQSxRQUFJOUUsTUFBTSxDQUFDQyxJQUFQLENBQVlnRCxNQUFaLEVBQW9CL0MsTUFBcEIsR0FBNkIsQ0FBakMsRUFBb0M7QUFDbENiLFNBQUcsQ0FBQ0ssTUFBSixDQUFXLEdBQVgsRUFBZ0JDLElBQWhCLENBQXFCO0FBQUVzRDtBQUFGLE9BQXJCO0FBQ0E7QUFDRDs7QUFFRCxVQUFNRSxFQUFFLENBQ0xlLFVBREcsQ0FDUSxlQURSLEVBRUhNLEdBRkcsQ0FFQ2xKLEdBQUcsQ0FBQ2dDLFNBRkwsRUFHSHlILEdBSEcsQ0FHQ0YsaUJBSEQsQ0FBTjtBQUtBLFVBQU16SSxRQUFRLEdBQUcsTUFBTTRJLHFFQUFXLENBQUM1RixHQUFELEVBQU02Riw0Q0FBTixFQUFhQyxtREFBYixFQUFvQi9CLEVBQXBCLEVBQXdCdUIsSUFBeEIsRUFBOEJDLElBQTlCLENBQWxDOztBQUNBLFVBQU10QixRQUFRLHFCQUFRd0IsaUJBQVI7QUFBMkJ6STtBQUEzQixNQUFkOztBQUVBaUQsT0FBRyxDQUFDTSxJQUFKLENBQVMwRCxRQUFUO0FBQ0QsR0FyQkQsQ0FxQkUsT0FBT3BHLENBQVAsRUFBVTtBQUNWLFVBQU1FLEtBQUssR0FBR21GLDZFQUFlLENBQUNyRixDQUFELENBQTdCO0FBQ0FvQyxPQUFHLENBQUNLLE1BQUosQ0FBVyxHQUFYLEVBQWdCQyxJQUFoQixDQUFxQnhDLEtBQXJCO0FBQ0Q7QUFDRixDQTFCRDtBQTRCQW9GLE1BQU0sQ0FBQ1csR0FBUCxDQUFXLE9BQVgsRUFBb0IsT0FBTzlELEdBQVAsRUFBWUMsR0FBWixLQUFvQjtBQUN0QyxNQUFJO0FBQ0YsVUFBTThELEVBQUUsR0FBR3JFLHVEQUFhLENBQUNzRSxTQUFkLEVBQVg7QUFDQSxVQUFNQyxRQUFRLEdBQUcsTUFBTUMsd0VBQVUsQ0FBQ0gsRUFBRCxFQUFLLFNBQUwsRUFBZ0IsRUFBaEIsQ0FBakM7QUFFQTlELE9BQUcsQ0FBQ00sSUFBSixDQUFTMEQsUUFBVDtBQUNELEdBTEQsQ0FLRSxPQUFPcEcsQ0FBUCxFQUFVO0FBQ1YsVUFBTUUsS0FBSyxHQUFHbUYsNkVBQWUsQ0FBQ3JGLENBQUQsQ0FBN0I7QUFDQW9DLE9BQUcsQ0FBQ0ssTUFBSixDQUFXLEdBQVgsRUFBZ0JDLElBQWhCLENBQXFCeEMsS0FBckI7QUFDRDtBQUNGLENBVkQ7QUFZZW9GLHFFQUFmLEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3ZEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Q0FHQTs7QUFDQSxNQUFNQSxNQUFNLEdBQUdDLDhDQUFPLENBQUNDLE1BQVIsRUFBZjs7QUFDQSxNQUFNQyxrQkFBa0IsR0FBRyxPQUFPdEQsR0FBUCxFQUFZdUQsS0FBWixLQUFzQjtBQUMvQztBQUNBLFFBQU1FLGtCQUFrQixHQUFHQyw2RUFBYSxDQUFDMUQsR0FBRCxFQUFNdUQsS0FBTixFQUFhd0MseUVBQWMsQ0FBQy9GLEdBQUcsQ0FBQ3pELE1BQUosQ0FBV3FILFlBQVosQ0FBM0IsQ0FBeEM7QUFDQSxRQUFNQyxNQUFNLEdBQUdKLGtCQUFrQixDQUFDSSxNQUFsQztBQUNBLE1BQUl0RixJQUFJLEdBQUdrRixrQkFBa0IsQ0FBQ2xGLElBQTlCO0FBRUEsU0FBTztBQUFFc0YsVUFBRjtBQUFVdEY7QUFBVixHQUFQO0FBQ0QsQ0FQRCxDLENBU0E7OztBQUNBNEUsTUFBTSxDQUFDVyxHQUFQLENBQVcsR0FBWCxFQUFnQixPQUFPOUQsR0FBUCxFQUFZQyxHQUFaLEtBQW9CO0FBQ2xDLE1BQUk7QUFDRixVQUFNOEQsRUFBRSxHQUFHckUsdURBQWEsQ0FBQ3NFLFNBQWQsRUFBWDtBQUNBLFVBQU1DLFFBQVEsR0FBRyxNQUFNQyx3RUFBVSxDQUFDSCxFQUFELEVBQUssU0FBTCxFQUFnQi9ELEdBQUcsQ0FBQ21FLEtBQXBCLENBQWpDO0FBRUFsRSxPQUFHLENBQUNNLElBQUosQ0FBUzBELFFBQVQ7QUFDRCxHQUxELENBS0UsT0FBT3BHLENBQVAsRUFBVTtBQUNWLFVBQU1FLEtBQUssR0FBR21GLDZFQUFlLENBQUNyRixDQUFELENBQTdCO0FBQ0FvQyxPQUFHLENBQUNLLE1BQUosQ0FBVyxHQUFYLEVBQWdCQyxJQUFoQixDQUFxQnhDLEtBQXJCO0FBQ0Q7QUFDRixDQVZEO0FBWUFvRixNQUFNLENBQUNXLEdBQVAsQ0FBVyxNQUFYLEVBQW1CLE9BQU85RCxHQUFQLEVBQVlDLEdBQVosS0FBb0I7QUFDckMsTUFBSTtBQUNGLFVBQU04RCxFQUFFLEdBQUdyRSx1REFBYSxDQUFDc0UsU0FBZCxFQUFYO0FBQ0EsVUFBTUMsUUFBUSxHQUFHLE1BQU1HLDJFQUFhLENBQUNMLEVBQUQsRUFBSyxTQUFMLEVBQWdCL0QsR0FBRyxDQUFDcUUsTUFBSixDQUFXQyxFQUEzQixDQUFwQztBQUNBckUsT0FBRyxDQUFDTSxJQUFKLENBQVMwRCxRQUFUO0FBQ0QsR0FKRCxDQUlFLE9BQU9wRyxDQUFQLEVBQVU7QUFDVixVQUFNRSxLQUFLLEdBQUdtRiw2RUFBZSxDQUFDckYsQ0FBRCxDQUE3QjtBQUNBb0MsT0FBRyxDQUFDSyxNQUFKLENBQVcsR0FBWCxFQUFnQkMsSUFBaEIsQ0FBcUJ4QyxLQUFyQjtBQUNEO0FBQ0YsQ0FURDtBQVdBb0YsTUFBTSxDQUFDb0IsSUFBUCxDQUFZLEdBQVosRUFBaUJ6RSxrRUFBYSxDQUFDLENBQUMsU0FBRCxDQUFELENBQTlCLEVBQTZDLE9BQU9FLEdBQVAsRUFBWUMsR0FBWixLQUFvQjtBQUMvRCxNQUFJO0FBQ0YsVUFBTXNELEtBQUsscUJBQVF2RCxHQUFHLENBQUMrQixJQUFaO0FBQWtCeUMsZUFBUyxFQUFFQyxJQUFJLENBQUNDLEdBQUw7QUFBN0IsTUFBWDs7QUFDQSxVQUFNWCxFQUFFLEdBQUdyRSx1REFBYSxDQUFDc0UsU0FBZCxFQUFYO0FBQ0EsVUFBTTtBQUFFSCxZQUFGO0FBQVV0RjtBQUFWLFFBQW1CLE1BQU0rRSxrQkFBa0IsQ0FBQ3RELEdBQUQsRUFBTXVELEtBQU4sQ0FBakQ7O0FBRUEsUUFBSTNDLE1BQU0sQ0FBQ0MsSUFBUCxDQUFZZ0QsTUFBWixFQUFvQi9DLE1BQXBCLEdBQTZCLENBQWpDLEVBQW9DO0FBQ2xDLGFBQU9iLEdBQUcsQ0FBQ0ssTUFBSixDQUFXLEdBQVgsRUFBZ0JDLElBQWhCLENBQXFCO0FBQUVzRDtBQUFGLE9BQXJCLENBQVA7QUFDRDs7QUFFRCxRQUFJSSxRQUFRLEdBQUcsTUFBTUYsRUFBRSxDQUFDZSxVQUFILENBQWMsU0FBZCxFQUF5QkMsR0FBekIsQ0FBNkJ4RyxJQUE3QixDQUFyQjtBQUVBMEYsWUFBUSxxQkFBUTFGLElBQVI7QUFBYytGLFFBQUUsRUFBRUwsUUFBUSxDQUFDSztBQUEzQixNQUFSO0FBQ0FyRSxPQUFHLENBQUNNLElBQUosQ0FBUzBELFFBQVQ7QUFDRCxHQWJELENBYUUsT0FBT3BHLENBQVAsRUFBVTtBQUNWLFVBQU1FLEtBQUssR0FBR21GLDZFQUFlLENBQUNyRixDQUFELENBQTdCO0FBQ0FvQyxPQUFHLENBQUNLLE1BQUosQ0FBVyxHQUFYLEVBQWdCQyxJQUFoQixDQUFxQnhDLEtBQXJCO0FBQ0Q7QUFDRixDQWxCRDtBQW9CQW9GLE1BQU0sQ0FBQzZCLEtBQVAsQ0FBYSxNQUFiLEVBQXFCbEYsa0VBQWEsQ0FBQyxDQUFDLFNBQUQsQ0FBRCxDQUFsQyxFQUFpRCxPQUFPRSxHQUFQLEVBQVlDLEdBQVosS0FBb0I7QUFDbkUsTUFBSTtBQUNGLFVBQU04RCxFQUFFLEdBQUdyRSx1REFBYSxDQUFDc0UsU0FBZCxFQUFYO0FBQ0EsVUFBTWlCLE1BQU0sR0FBRyxNQUFNYiwyRUFBYSxDQUFDTCxFQUFELEVBQUssU0FBTCxFQUFnQi9ELEdBQUcsQ0FBQ3FFLE1BQUosQ0FBV0MsRUFBM0IsQ0FBbEM7QUFDQSxVQUFNZixLQUFLLEdBQUczQyxNQUFNLENBQUNzRSxNQUFQLENBQWNELE1BQWQsb0JBQTJCakYsR0FBRyxDQUFDK0IsSUFBL0I7QUFBcUNvRCxlQUFTLEVBQUVWLElBQUksQ0FBQ0MsR0FBTDtBQUFoRCxPQUFkO0FBQ0EsVUFBTTtBQUFFYixZQUFGO0FBQVV0RjtBQUFWLFFBQW1CLE1BQU0rRSxrQkFBa0IsQ0FBQ3RELEdBQUQsRUFBTXVELEtBQU4sQ0FBakQ7O0FBRUEsUUFBSTNDLE1BQU0sQ0FBQ0MsSUFBUCxDQUFZZ0QsTUFBWixFQUFvQi9DLE1BQXBCLEdBQTZCLENBQWpDLEVBQW9DO0FBQ2xDLGFBQU9iLEdBQUcsQ0FBQ0ssTUFBSixDQUFXLEdBQVgsRUFBZ0JDLElBQWhCLENBQXFCO0FBQUVzRDtBQUFGLE9BQXJCLENBQVA7QUFDRDs7QUFFRCxVQUFNRSxFQUFFLENBQ0xlLFVBREcsQ0FDUSxTQURSLEVBRUhNLEdBRkcsQ0FFQ3BGLEdBQUcsQ0FBQ3FFLE1BQUosQ0FBV0MsRUFGWixFQUdIZSxNQUhHLENBR0k5RyxJQUhKLENBQU47O0FBS0EsVUFBTTBGLFFBQVEscUJBQVExRixJQUFSO0FBQWMrRixRQUFFLEVBQUV0RSxHQUFHLENBQUNxRSxNQUFKLENBQVdDO0FBQTdCLE1BQWQ7O0FBQ0FyRSxPQUFHLENBQUNNLElBQUosQ0FBUzBELFFBQVQ7QUFDRCxHQWpCRCxDQWlCRSxPQUFPcEcsQ0FBUCxFQUFVO0FBQ1YsVUFBTUUsS0FBSyxHQUFHbUYsNkVBQWUsQ0FBQ3JGLENBQUQsQ0FBN0I7QUFDQW9DLE9BQUcsQ0FBQ0ssTUFBSixDQUFXLEdBQVgsRUFBZ0JDLElBQWhCLENBQXFCeEMsS0FBckI7QUFDRDtBQUNGLENBdEJEO0FBd0JBb0YsTUFBTSxDQUFDNkMsTUFBUCxDQUFjLE1BQWQsRUFBc0JsRyxrRUFBYSxDQUFDLENBQUMsU0FBRCxDQUFELENBQW5DLEVBQWtELE9BQU9FLEdBQVAsRUFBWUMsR0FBWixLQUFvQjtBQUNwRSxNQUFJO0FBQ0YsVUFBTTtBQUFFcUU7QUFBRixRQUFTdEUsR0FBRyxDQUFDcUUsTUFBbkI7QUFDQSxVQUFNTixFQUFFLEdBQUdyRSx1REFBYSxDQUFDc0UsU0FBZCxFQUFYO0FBQ0EsVUFBTUMsUUFBUSxHQUFHLE1BQU1HLDJFQUFhLENBQUNMLEVBQUQsRUFBSyxTQUFMLEVBQWdCL0QsR0FBRyxDQUFDcUUsTUFBSixDQUFXQyxFQUEzQixDQUFwQztBQUVBLFVBQU1QLEVBQUUsQ0FDTGUsVUFERyxDQUNRLFNBRFIsRUFFSE0sR0FGRyxDQUVDZCxFQUZELEVBR0gwQixNQUhHLEVBQU47QUFLQS9GLE9BQUcsQ0FBQ00sSUFBSixDQUFTMEQsUUFBVDtBQUNELEdBWEQsQ0FXRSxPQUFPcEcsQ0FBUCxFQUFVO0FBQ1YsVUFBTUUsS0FBSyxHQUFHbUYsNkVBQWUsQ0FBQ3JGLENBQUQsQ0FBN0I7QUFDQW9DLE9BQUcsQ0FBQ0ssTUFBSixDQUFXLEdBQVgsRUFBZ0JDLElBQWhCLENBQXFCeEMsS0FBckI7QUFDRDtBQUNGLENBaEJEO0FBa0Jlb0YscUVBQWYsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3pHQTtDQUVBO0FBQ0E7O0FBQ0E7QUFDQTtDQUVBO0FBRUE7O0FBQ0EsTUFBTUEsTUFBTSxHQUFHQyw4Q0FBTyxDQUFDQyxNQUFSLEVBQWY7O0FBQ0EsTUFBTUMsa0JBQWtCLEdBQUcsT0FBT3RELEdBQVAsRUFBWXVELEtBQVosS0FBc0I7QUFDL0M7QUFDQSxRQUFNMEMsa0JBQWtCLEdBQUcxQyxLQUFLLENBQUNlLEVBQU4sR0FBVyxLQUFYLEdBQW1CLElBQTlDO0FBQ0EsUUFBTWIsa0JBQWtCLEdBQUdDLDZFQUFhLENBQUMxRCxHQUFELEVBQU11RCxLQUFOLEVBQWEyQyx5RUFBYyxDQUFDM0MsS0FBRCxFQUFRMEMsa0JBQVIsQ0FBM0IsQ0FBeEM7QUFDQSxRQUFNcEMsTUFBTSxHQUFHSixrQkFBa0IsQ0FBQ0ksTUFBbEM7QUFDQSxNQUFJdEYsSUFBSSxHQUFHa0Ysa0JBQWtCLENBQUNsRixJQUE5QjtBQUVBLFNBQU87QUFBRXNGLFVBQUY7QUFBVXRGO0FBQVYsR0FBUDtBQUNELENBUkQsQyxDQVVBOzs7QUFDQTRFLE1BQU0sQ0FBQ29CLElBQVAsQ0FBWSxTQUFaLEVBQXVCLE9BQU92RSxHQUFQLEVBQVlDLEdBQVosS0FBb0I7QUFDekMsTUFBSTtBQUNGLFVBQU04RCxFQUFFLEdBQUdyRSx1REFBYSxDQUFDc0UsU0FBZCxFQUFYO0FBQ0EsVUFBTW1DLFVBQVUsR0FBR3ZGLE1BQU0sQ0FBQ0MsSUFBUCxDQUFZYixHQUFHLENBQUMrQixJQUFoQixFQUFzQnFFLE1BQXRCLENBQTZCLENBQUNDLEtBQUQsRUFBUW5KLEdBQVIsS0FBZ0I7QUFDNUQsYUFBTyxDQUFDLENBQUMsSUFBRCxFQUFPK0QsUUFBUCxDQUFnQi9ELEdBQWhCLENBQUQsR0FDSDBELE1BQU0sQ0FBQ3NFLE1BQVAsQ0FBY21CLEtBQWQsRUFBcUI7QUFBRSxTQUFDbkosR0FBRCxHQUFPOEMsR0FBRyxDQUFDK0IsSUFBSixDQUFTN0UsR0FBVDtBQUFULE9BQXJCLENBREcsR0FFSG1KLEtBRko7QUFHSCxLQUprQixFQUloQixFQUpnQixDQUFuQjs7QUFLQSxVQUFNOUMsS0FBSyxxQkFBUTRDLFVBQVI7QUFBb0IzQixlQUFTLEVBQUVDLElBQUksQ0FBQ0MsR0FBTDtBQUEvQixNQUFYOztBQUNBLFVBQU07QUFBRWIsWUFBRjtBQUFVdEY7QUFBVixRQUFtQixNQUFNK0Usa0JBQWtCLENBQUN0RCxHQUFELEVBQU11RCxLQUFOLENBQWpEOztBQUNBLFFBQUkzQyxNQUFNLENBQUNDLElBQVAsQ0FBWWdELE1BQVosRUFBb0IvQyxNQUFwQixHQUE2QixDQUFqQyxFQUFvQztBQUNsQyxhQUFPYixHQUFHLENBQUNLLE1BQUosQ0FBVyxHQUFYLEVBQWdCQyxJQUFoQixDQUFxQjtBQUFFc0Q7QUFBRixPQUFyQixDQUFQO0FBQ0Q7O0FBQ0QvRixXQUFPLENBQUN3SSxJQUFSLENBQWEsU0FBYixFQUF1Qi9ILElBQXZCO0FBQ0EsVUFBTXdGLEVBQUUsQ0FBQ2UsVUFBSCxDQUFjLFNBQWQsRUFBeUJNLEdBQXpCLEdBQStCTyxHQUEvQixDQUFtQ1EsVUFBbkMsQ0FBTjtBQUNBbEcsT0FBRyxDQUFDTSxJQUFKLENBQVM7QUFBQ2dHLGFBQU8sRUFBRTtBQUFWLEtBQVQ7QUFDRCxHQWZELENBZUUsT0FBTzFJLENBQVAsRUFBVTtBQUNWLFVBQU1FLEtBQUssR0FBR21GLDZFQUFlLENBQUNyRixDQUFELENBQTdCO0FBQ0FvQyxPQUFHLENBQUNLLE1BQUosQ0FBVyxHQUFYLEVBQWdCQyxJQUFoQixDQUFxQnhDLEtBQXJCO0FBQ0Q7QUFDRixDQXBCRDtBQXNCZW9GLHFFQUFmLEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM1Q0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Q0FHQTs7QUFDQSxNQUFNQSxNQUFNLEdBQUdDLDhDQUFPLENBQUNDLE1BQVIsRUFBZjs7QUFDQSxNQUFNQyxrQkFBa0IsR0FBRyxPQUFPdEQsR0FBUCxFQUFZdUQsS0FBWixLQUFzQjtBQUMvQztBQUNBLFFBQU0wQyxrQkFBa0IsR0FBRzFDLEtBQUssQ0FBQ2UsRUFBTixHQUFXLEtBQVgsR0FBbUIsSUFBOUM7QUFDQSxRQUFNYixrQkFBa0IsR0FBR0MsNkVBQWEsQ0FBQzFELEdBQUQsRUFBTXVELEtBQU4sRUFBYWlELHVFQUFZLENBQUNqRCxLQUFELEVBQVEwQyxrQkFBUixDQUF6QixDQUF4QztBQUNBLFFBQU1wQyxNQUFNLEdBQUdKLGtCQUFrQixDQUFDSSxNQUFsQztBQUNBLE1BQUl0RixJQUFJLEdBQUdrRixrQkFBa0IsQ0FBQ2xGLElBQTlCO0FBRUEsU0FBTztBQUFFc0YsVUFBRjtBQUFVdEY7QUFBVixHQUFQO0FBQ0QsQ0FSRCxDLENBVUE7OztBQUNBNEUsTUFBTSxDQUFDVyxHQUFQLENBQVcsR0FBWCxFQUFnQmhFLGtFQUFhLENBQUMsQ0FBQyxTQUFELENBQUQsQ0FBN0IsRUFBNEMsT0FBT0UsR0FBUCxFQUFZQyxHQUFaLEtBQW9CO0FBQzlELE1BQUk7QUFDRixVQUFNOEQsRUFBRSxHQUFHckUsdURBQWEsQ0FBQ3NFLFNBQWQsRUFBWDtBQUNBLFVBQU1DLFFBQVEsR0FBRyxNQUFNQyx3RUFBVSxDQUFDSCxFQUFELEVBQUssT0FBTCxFQUFjL0QsR0FBRyxDQUFDbUUsS0FBbEIsQ0FBakM7QUFFQWxFLE9BQUcsQ0FBQ00sSUFBSixDQUFTMEQsUUFBVDtBQUNELEdBTEQsQ0FLRSxPQUFPcEcsQ0FBUCxFQUFVO0FBQ1YsVUFBTUUsS0FBSyxHQUFHbUYsNkVBQWUsQ0FBQ3JGLENBQUQsQ0FBN0I7QUFDQW9DLE9BQUcsQ0FBQ0ssTUFBSixDQUFXLEdBQVgsRUFBZ0JDLElBQWhCLENBQXFCeEMsS0FBckI7QUFDRDtBQUNGLENBVkQ7QUFZQW9GLE1BQU0sQ0FBQ1csR0FBUCxDQUFXLEtBQVgsRUFBa0JoRSxrRUFBYSxDQUFDLENBQUMsY0FBRCxDQUFELENBQS9CLEVBQW1ELE9BQU9FLEdBQVAsRUFBWUMsR0FBWixLQUFvQjtBQUNyRSxNQUFJO0FBQ0YsUUFBSTVELElBQUksR0FBRzJELEdBQUcsQ0FBQzNELElBQWY7O0FBRUEsUUFBSUEsSUFBSixFQUFVO0FBQ1IsWUFBTTBILEVBQUUsR0FBR3JFLHVEQUFhLENBQUNzRSxTQUFkLEVBQVg7QUFDQSxZQUFNeUMsT0FBTyxHQUFHLE1BQU1yQywyRUFBYSxDQUFDTCxFQUFELEVBQUssT0FBTCxFQUFjMUgsSUFBSSxDQUFDcUssR0FBbkIsQ0FBbkM7QUFDQXJLLFVBQUksR0FBR3VFLE1BQU0sQ0FBQ3NFLE1BQVAsQ0FBYzdJLElBQWQsRUFBb0I7QUFBRW9LO0FBQUYsT0FBcEIsQ0FBUDtBQUNEOztBQUVEeEcsT0FBRyxDQUFDTSxJQUFKLENBQVNsRSxJQUFUO0FBQ0QsR0FWRCxDQVVFLE9BQU93QixDQUFQLEVBQVU7QUFDVixVQUFNRSxLQUFLLEdBQUdtRiw2RUFBZSxDQUFDckYsQ0FBRCxDQUE3QjtBQUNBb0MsT0FBRyxDQUFDSyxNQUFKLENBQVcsR0FBWCxFQUFnQkMsSUFBaEIsQ0FBcUJ4QyxLQUFyQjtBQUNEO0FBQ0YsQ0FmRDtBQWlCQW9GLE1BQU0sQ0FBQ1csR0FBUCxDQUFXLE1BQVgsRUFBbUJoRSxrRUFBYSxDQUFDLENBQUMsY0FBRCxDQUFELENBQWhDLEVBQW9ELE9BQU9FLEdBQVAsRUFBWUMsR0FBWixLQUFvQjtBQUN0RSxNQUFJO0FBQ0YsVUFBTThELEVBQUUsR0FBR3JFLHVEQUFhLENBQUNzRSxTQUFkLEVBQVg7QUFDQSxVQUFNQyxRQUFRLEdBQUcsTUFBTUcsMkVBQWEsQ0FBQ0wsRUFBRCxFQUFLLE9BQUwsRUFBYy9ELEdBQUcsQ0FBQ3FFLE1BQUosQ0FBV0MsRUFBekIsQ0FBcEM7QUFFQXJFLE9BQUcsQ0FBQ00sSUFBSixDQUFTMEQsUUFBVDtBQUNELEdBTEQsQ0FLRSxPQUFPcEcsQ0FBUCxFQUFVO0FBQ1YsVUFBTUUsS0FBSyxHQUFHbUYsNkVBQWUsQ0FBQ3JGLENBQUQsQ0FBN0I7QUFDQW9DLE9BQUcsQ0FBQ0ssTUFBSixDQUFXLEdBQVgsRUFBZ0JDLElBQWhCLENBQXFCeEMsS0FBckI7QUFDRDtBQUNGLENBVkQ7QUFZQW9GLE1BQU0sQ0FBQ1csR0FBUCxDQUFXLGFBQVgsRUFBMEJoRSxrRUFBYSxDQUFDLENBQUMsU0FBRCxDQUFELENBQXZDLEVBQXNELE9BQU9FLEdBQVAsRUFBWUMsR0FBWixLQUFvQjtBQUN4RSxNQUFJO0FBQ0YsVUFBTTtBQUFFcUUsUUFBRSxFQUFFcUM7QUFBTixRQUFpQjNHLEdBQUcsQ0FBQ3FFLE1BQTNCO0FBQ0EsVUFBTWhJLElBQUksR0FBRyxNQUFNcUQsdURBQWEsQ0FBQ2UsSUFBZCxHQUFxQm1HLE9BQXJCLENBQTZCRCxNQUE3QixDQUFuQjtBQUNBLFFBQUkxQyxRQUFRLEdBQUcsRUFBZjs7QUFFQSxRQUFJNUgsSUFBSSxDQUFDd0ssWUFBVCxFQUF1QjtBQUNyQjVDLGNBQVEsR0FBR3JELE1BQU0sQ0FBQ0MsSUFBUCxDQUFZeEUsSUFBSSxDQUFDd0ssWUFBakIsQ0FBWDtBQUNEOztBQUVENUcsT0FBRyxDQUFDNkcsSUFBSixDQUFTN0MsUUFBVDtBQUNELEdBVkQsQ0FVRSxPQUFPcEcsQ0FBUCxFQUFVO0FBQ1YsVUFBTUUsS0FBSyxHQUFHbUYsNkVBQWUsQ0FBQ3JGLENBQUQsQ0FBN0I7QUFDQW9DLE9BQUcsQ0FBQ0ssTUFBSixDQUFXLEdBQVgsRUFBZ0JDLElBQWhCLENBQXFCeEMsS0FBckI7QUFDRDtBQUNGLENBZkQ7QUFpQkFvRixNQUFNLENBQUNvQixJQUFQLENBQVksYUFBWixFQUEyQnpFLGtFQUFhLENBQUMsQ0FBQyxTQUFELENBQUQsQ0FBeEMsRUFBdUQsT0FBT0UsR0FBUCxFQUFZQyxHQUFaLEtBQW9CO0FBQ3pFLE1BQUk7QUFDRixVQUFNO0FBQUVxRSxRQUFFLEVBQUVxQztBQUFOLFFBQWlCM0csR0FBRyxDQUFDcUUsTUFBM0I7QUFDQSxVQUFNO0FBQUUwQztBQUFGLFFBQVkvRyxHQUFHLENBQUMrQixJQUF0QjtBQUVBLFVBQU0xRixJQUFJLEdBQUcsTUFBTXFELHVEQUFhLENBQUNlLElBQWQsR0FBcUJtRyxPQUFyQixDQUE2QkQsTUFBN0IsQ0FBbkI7O0FBQ0EsVUFBTWhHLE1BQU0scUJBQVF0RSxJQUFJLENBQUN3SyxZQUFiO0FBQTJCLE9BQUNFLEtBQUQsR0FBUztBQUFwQyxNQUFaOztBQUNBLFVBQU1ySCx1REFBYSxDQUFDZSxJQUFkLEdBQXFCdUcsbUJBQXJCLENBQXlDTCxNQUF6QyxFQUFpRGhHLE1BQWpELENBQU47QUFFQVYsT0FBRyxDQUFDTSxJQUFKLENBQVN3RyxLQUFUO0FBQ0QsR0FURCxDQVNFLE9BQU9sSixDQUFQLEVBQVU7QUFDVixVQUFNRSxLQUFLLEdBQUdtRiw2RUFBZSxDQUFDckYsQ0FBRCxDQUE3QjtBQUNBb0MsT0FBRyxDQUFDSyxNQUFKLENBQVcsR0FBWCxFQUFnQkMsSUFBaEIsQ0FBcUJ4QyxLQUFyQjtBQUNEO0FBQ0YsQ0FkRDtBQWdCQW9GLE1BQU0sQ0FBQzZCLEtBQVAsQ0FBYSxhQUFiLEVBQTRCbEYsa0VBQWEsQ0FBQyxDQUFDLFNBQUQsQ0FBRCxDQUF6QyxFQUF3RCxPQUFPRSxHQUFQLEVBQVlDLEdBQVosS0FBb0I7QUFDMUUsTUFBSTtBQUNGLFVBQU07QUFBRXFFLFFBQUUsRUFBRXFDO0FBQU4sUUFBaUIzRyxHQUFHLENBQUNxRSxNQUEzQjtBQUNBLFVBQU07QUFBRTBDO0FBQUYsUUFBWS9HLEdBQUcsQ0FBQytCLElBQXRCO0FBRUEsVUFBTTFGLElBQUksR0FBRyxNQUFNcUQsdURBQWEsQ0FBQ2UsSUFBZCxHQUFxQm1HLE9BQXJCLENBQTZCRCxNQUE3QixDQUFuQjtBQUNBLFdBQU90SyxJQUFJLENBQUN3SyxZQUFMLENBQWtCRSxLQUFsQixDQUFQO0FBQ0EsVUFBTXJILHVEQUFhLENBQUNlLElBQWQsR0FBcUJ1RyxtQkFBckIsQ0FBeUNMLE1BQXpDLEVBQWlEdEssSUFBSSxDQUFDd0ssWUFBdEQsQ0FBTjtBQUVBNUcsT0FBRyxDQUFDTSxJQUFKLENBQVN3RyxLQUFUO0FBQ0QsR0FURCxDQVNFLE9BQU9sSixDQUFQLEVBQVU7QUFDVixVQUFNRSxLQUFLLEdBQUdtRiw2RUFBZSxDQUFDckYsQ0FBRCxDQUE3QjtBQUNBb0MsT0FBRyxDQUFDSyxNQUFKLENBQVcsR0FBWCxFQUFnQkMsSUFBaEIsQ0FBcUJ4QyxLQUFyQjtBQUNEO0FBQ0YsQ0FkRDtBQWdCQW9GLE1BQU0sQ0FBQzZCLEtBQVAsQ0FBYSxNQUFiLEVBQXFCbEYsa0VBQWEsQ0FBQyxDQUFDLGNBQUQsQ0FBRCxDQUFsQyxFQUFzRCxPQUFPRSxHQUFQLEVBQVlDLEdBQVosS0FBb0I7QUFDeEUsTUFBSTtBQUNGLFVBQU04RCxFQUFFLEdBQUdyRSx1REFBYSxDQUFDc0UsU0FBZCxFQUFYO0FBQ0EsVUFBTWlCLE1BQU0sR0FBRyxNQUFNYiwyRUFBYSxDQUFDTCxFQUFELEVBQUssT0FBTCxFQUFjL0QsR0FBRyxDQUFDcUUsTUFBSixDQUFXQyxFQUF6QixDQUFsQztBQUNBLFVBQU0yQyxRQUFRLEdBQUdyRyxNQUFNLENBQUNDLElBQVAsQ0FBWWIsR0FBRyxDQUFDK0IsSUFBaEIsRUFBc0JxRSxNQUF0QixDQUE2QixDQUFDQyxLQUFELEVBQVFuSixHQUFSLEtBQWdCO0FBQzVELGFBQU8sQ0FBQyxDQUFDLE9BQUQsRUFBVStELFFBQVYsQ0FBbUIvRCxHQUFuQixDQUFELEdBQ0gwRCxNQUFNLENBQUNzRSxNQUFQLENBQWNtQixLQUFkLEVBQXFCO0FBQUUsU0FBQ25KLEdBQUQsR0FBTzhDLEdBQUcsQ0FBQytCLElBQUosQ0FBUzdFLEdBQVQ7QUFBVCxPQUFyQixDQURHLEdBRUhtSixLQUZKO0FBR0QsS0FKZ0IsRUFJZCxFQUpjLENBQWpCO0FBS0EsVUFBTTlDLEtBQUssR0FBRzNDLE1BQU0sQ0FBQ3NFLE1BQVAsQ0FBY0QsTUFBZCxvQkFBMkJnQyxRQUEzQjtBQUFxQzlCLGVBQVMsRUFBRVYsSUFBSSxDQUFDQyxHQUFMO0FBQWhELE9BQWQ7QUFDQSxVQUFNO0FBQUViLFlBQUY7QUFBVXRGO0FBQVYsUUFBbUIsTUFBTStFLGtCQUFrQixDQUFDdEQsR0FBRCxFQUFNdUQsS0FBTixDQUFqRDs7QUFFQSxRQUFJM0MsTUFBTSxDQUFDQyxJQUFQLENBQVlnRCxNQUFaLEVBQW9CL0MsTUFBcEIsR0FBNkIsQ0FBakMsRUFBb0M7QUFDbENiLFNBQUcsQ0FBQ0ssTUFBSixDQUFXLEdBQVgsRUFBZ0JDLElBQWhCLENBQXFCO0FBQUVzRDtBQUFGLE9BQXJCO0FBQ0E7QUFDRDs7QUFFRCxVQUFNcUQsVUFBVSxHQUFHdEcsTUFBTSxDQUFDQyxJQUFQLENBQVl0QyxJQUFaLEVBQWtCNkgsTUFBbEIsQ0FBeUIsQ0FBQ0MsS0FBRCxFQUFRbkosR0FBUixLQUFnQjtBQUMxRCxhQUFPLENBQUMsQ0FBQyxVQUFELEVBQWEsc0JBQWIsRUFBcUMrRCxRQUFyQyxDQUE4Qy9ELEdBQTlDLENBQUQsR0FDSDBELE1BQU0sQ0FBQ3NFLE1BQVAsQ0FBY21CLEtBQWQsRUFBcUI7QUFBRSxTQUFDbkosR0FBRCxHQUFPcUIsSUFBSSxDQUFDckIsR0FBRDtBQUFiLE9BQXJCLENBREcsR0FFSG1KLEtBRko7QUFHRCxLQUprQixFQUloQixFQUpnQixDQUFuQjtBQUtBLFVBQU10QyxFQUFFLENBQ0xlLFVBREcsQ0FDUSxPQURSLEVBRUhNLEdBRkcsQ0FFQ3BGLEdBQUcsQ0FBQ3FFLE1BQUosQ0FBV0MsRUFGWixFQUdIZSxNQUhHLENBR0k2QixVQUhKLENBQU47O0FBS0EsVUFBTWpELFFBQVEscUJBQVExRixJQUFSO0FBQWMrRixRQUFFLEVBQUV0RSxHQUFHLENBQUNxRSxNQUFKLENBQVdDO0FBQTdCLE1BQWQ7O0FBQ0FyRSxPQUFHLENBQUNNLElBQUosQ0FBUzBELFFBQVQ7QUFDRCxHQTVCRCxDQTRCRSxPQUFPcEcsQ0FBUCxFQUFVO0FBQ1YsVUFBTUUsS0FBSyxHQUFHbUYsNkVBQWUsQ0FBQ3JGLENBQUQsQ0FBN0I7QUFDQW9DLE9BQUcsQ0FBQ0ssTUFBSixDQUFXLEdBQVgsRUFBZ0JDLElBQWhCLENBQXFCeEMsS0FBckI7QUFDRDtBQUNGLENBakNEO0FBbUNBb0YsTUFBTSxDQUFDb0IsSUFBUCxDQUFZLGtCQUFaLEVBQWdDekUsa0VBQWEsQ0FBQyxDQUFDLGNBQUQsQ0FBRCxDQUE3QyxFQUFpRSxPQUFPRSxHQUFQLEVBQVlDLEdBQVosS0FBb0I7QUFDbkYsTUFBSTtBQUNGLFVBQU07QUFBRThCO0FBQUYsUUFBVy9CLEdBQWpCO0FBQ0EsVUFBTTtBQUFFekIsVUFBRjtBQUFRc0Y7QUFBUixRQUFtQkgsNkVBQWEsQ0FBQzFELEdBQUQsRUFBTStCLElBQU4sRUFBWSxDQUNoRDtBQUNFb0YsY0FBUSxFQUFFLFVBRFo7QUFFRUMsVUFBSSxFQUFFLFFBRlI7QUFHRUMsV0FBSyxFQUFFLENBQUM7QUFBRUMsZ0JBQVEsRUFBRTtBQUFaLE9BQUQsRUFBcUI7QUFBRUMsaUJBQVMsRUFBRTtBQUFiLE9BQXJCO0FBSFQsS0FEZ0QsRUFNaEQ7QUFDRUosY0FBUSxFQUFFLHNCQURaO0FBRUVDLFVBQUksRUFBRSxRQUZSO0FBR0VDLFdBQUssRUFBRSxDQUFDO0FBQUVDLGdCQUFRLEVBQUU7QUFBWixPQUFELEVBQXFCO0FBQUVDLGlCQUFTLEVBQUU7QUFBYixPQUFyQjtBQUhULEtBTmdELENBQVosQ0FBdEM7O0FBYUEsUUFBSTNHLE1BQU0sQ0FBQ0MsSUFBUCxDQUFZZ0QsTUFBWixFQUFvQi9DLE1BQXBCLEdBQTZCLENBQWpDLEVBQW9DO0FBQ2xDYixTQUFHLENBQUNLLE1BQUosQ0FBVyxHQUFYLEVBQWdCQyxJQUFoQixDQUFxQjtBQUFFc0Q7QUFBRixPQUFyQjtBQUNBO0FBQ0Q7O0FBRUQsVUFBTW5FLHVEQUFhLENBQUNlLElBQWQsR0FBcUIrRyxVQUFyQixDQUFnQ3hILEdBQUcsQ0FBQzNELElBQUosQ0FBU3FLLEdBQXpDLEVBQThDO0FBQ2xEZSxjQUFRLEVBQUVsSixJQUFJLENBQUNrSjtBQURtQyxLQUE5QyxDQUFOO0FBR0F4SCxPQUFHLENBQUNNLElBQUosQ0FBUztBQUFFN0MsYUFBTyxFQUFFRCxtRUFBUyxDQUFDdUMsR0FBRyxDQUFDNUMsWUFBTCxFQUFtQixvQkFBbkI7QUFBcEIsS0FBVDtBQUNELEdBeEJELENBd0JFLE9BQU9TLENBQVAsRUFBVTtBQUNWLFVBQU1FLEtBQUssR0FBR21GLDZFQUFlLENBQUNyRixDQUFELENBQTdCO0FBQ0FvQyxPQUFHLENBQUNLLE1BQUosQ0FBVyxHQUFYLEVBQWdCQyxJQUFoQixDQUFxQnhDLEtBQXJCO0FBQ0Q7QUFDRixDQTdCRDtBQStCQW9GLE1BQU0sQ0FBQ29CLElBQVAsQ0FBWSxtQkFBWixFQUFpQyxPQUFPdkUsR0FBUCxFQUFZQyxHQUFaLEtBQW9CO0FBQ25ELE1BQUk7QUFDRixVQUFNO0FBQUU4QjtBQUFGLFFBQVcvQixHQUFqQjtBQUNBLFVBQU07QUFBRXpCLFVBQUY7QUFBUXNGO0FBQVIsUUFBbUJILDZFQUFhLENBQUMxRCxHQUFELEVBQU0rQixJQUFOLEVBQVksQ0FDaEQ7QUFDRW9GLGNBQVEsRUFBRSxPQURaO0FBRUVDLFVBQUksRUFBRSxRQUZSO0FBR0VDLFdBQUssRUFBRSxDQUFDO0FBQUVDLGdCQUFRLEVBQUU7QUFBWixPQUFELEVBQXFCO0FBQUVJLGNBQU0sRUFBRTtBQUFWLE9BQXJCO0FBSFQsS0FEZ0QsQ0FBWixDQUF0Qzs7QUFRQSxRQUFJOUcsTUFBTSxDQUFDQyxJQUFQLENBQVlnRCxNQUFaLEVBQW9CL0MsTUFBcEIsR0FBNkIsQ0FBakMsRUFBb0M7QUFDbENiLFNBQUcsQ0FBQ0ssTUFBSixDQUFXLEdBQVgsRUFBZ0JDLElBQWhCLENBQXFCO0FBQUVzRDtBQUFGLE9BQXJCO0FBQ0E7QUFDRDs7QUFFRCxVQUFNakYsd0RBQWMsQ0FBQzZCLElBQWYsR0FBc0JrSCxzQkFBdEIsQ0FBNkNwSixJQUFJLENBQUNoQixLQUFsRCxDQUFOO0FBQ0EwQyxPQUFHLENBQUNNLElBQUosQ0FBUztBQUFFN0MsYUFBTyxFQUFFRCxtRUFBUyxDQUFDdUMsR0FBRyxDQUFDNUMsWUFBTCxFQUFtQix3QkFBbkI7QUFBcEIsS0FBVDtBQUNELEdBakJELENBaUJFLE9BQU9TLENBQVAsRUFBVTtBQUNWLFVBQU1FLEtBQUssR0FBR21GLDZFQUFlLENBQUNyRixDQUFELENBQTdCO0FBQ0FvQyxPQUFHLENBQUNLLE1BQUosQ0FBVyxHQUFYLEVBQWdCQyxJQUFoQixDQUFxQnhDLEtBQXJCO0FBQ0Q7QUFDRixDQXRCRDtBQXdCQW9GLE1BQU0sQ0FBQ29CLElBQVAsQ0FBWSxRQUFaLEVBQXNCLE9BQU92RSxHQUFQLEVBQVlDLEdBQVosS0FBb0I7QUFDeEMsTUFBSTtBQUNGLFVBQU1zRCxLQUFLLEdBQUd2RCxHQUFHLENBQUMrQixJQUFsQjtBQUNBLFVBQU07QUFBRThCLFlBQUY7QUFBVXRGO0FBQVYsUUFBbUJtRiw2RUFBYSxDQUFDMUQsR0FBRCxFQUFNdUQsS0FBTixFQUFhLENBQ2pEO0FBQ0U0RCxjQUFRLEVBQUUsT0FEWjtBQUVFQyxVQUFJLEVBQUUsUUFGUjtBQUdFQyxXQUFLLEVBQUUsQ0FBQztBQUFFQyxnQkFBUSxFQUFFO0FBQVosT0FBRCxFQUFxQjtBQUFFSSxjQUFNLEVBQUU7QUFBVixPQUFyQjtBQUhULEtBRGlELEVBTWpEO0FBQ0VQLGNBQVEsRUFBRSxVQURaO0FBRUVDLFVBQUksRUFBRSxRQUZSO0FBR0VDLFdBQUssRUFBRSxDQUFDO0FBQUVDLGdCQUFRLEVBQUU7QUFBWixPQUFEO0FBSFQsS0FOaUQsQ0FBYixDQUF0Qzs7QUFhQSxRQUFJMUcsTUFBTSxDQUFDQyxJQUFQLENBQVlnRCxNQUFaLEVBQW9CL0MsTUFBcEIsR0FBNkIsQ0FBakMsRUFBb0M7QUFDbEMsYUFBT2IsR0FBRyxDQUFDSyxNQUFKLENBQVcsR0FBWCxFQUFnQkMsSUFBaEIsQ0FBcUI7QUFBRXNEO0FBQUYsT0FBckIsQ0FBUDtBQUNEOztBQUVELFVBQU1JLFFBQVEsR0FBRyxNQUFNckYsd0RBQWMsQ0FDbEM2QixJQURvQixHQUVwQm1ILDBCQUZvQixDQUVPckosSUFBSSxDQUFDaEIsS0FGWixFQUVtQmdCLElBQUksQ0FBQ2tKLFFBRnhCLENBQXZCO0FBR0EsVUFBTXRILEtBQUssR0FBRyxNQUFNOEQsUUFBUSxDQUFDNUgsSUFBVCxDQUFjd0wsVUFBZCxFQUFwQjtBQUVBNUgsT0FBRyxDQUFDTSxJQUFKLENBQVM7QUFBRUo7QUFBRixLQUFUO0FBQ0QsR0F6QkQsQ0F5QkUsT0FBT3RDLENBQVAsRUFBVTtBQUNWLFVBQU1FLEtBQUssR0FBR21GLDZFQUFlLENBQUNyRixDQUFELENBQTdCO0FBQ0FvQyxPQUFHLENBQUNLLE1BQUosQ0FBVyxHQUFYLEVBQWdCQyxJQUFoQixDQUFxQnhDLEtBQXJCO0FBQ0Q7QUFDRixDQTlCRDtBQWdDQW9GLE1BQU0sQ0FBQ29CLElBQVAsQ0FBWSxXQUFaLEVBQXlCLE9BQU92RSxHQUFQLEVBQVlDLEdBQVosS0FBb0I7QUFDM0MsTUFBSTtBQUNGLFVBQU04RCxFQUFFLEdBQUdyRSx1REFBYSxDQUFDc0UsU0FBZCxFQUFYO0FBQ0EsVUFBTWlELFFBQVEsR0FBR3JHLE1BQU0sQ0FBQ0MsSUFBUCxDQUFZYixHQUFHLENBQUMrQixJQUFoQixFQUFzQnFFLE1BQXRCLENBQTZCLENBQUNDLEtBQUQsRUFBUW5KLEdBQVIsS0FBZ0I7QUFDNUQsYUFBTyxDQUFDLENBQUMsSUFBRCxFQUFPK0QsUUFBUCxDQUFnQi9ELEdBQWhCLENBQUQsR0FDSDBELE1BQU0sQ0FBQ3NFLE1BQVAsQ0FBY21CLEtBQWQsRUFBcUI7QUFBRSxTQUFDbkosR0FBRCxHQUFPOEMsR0FBRyxDQUFDK0IsSUFBSixDQUFTN0UsR0FBVDtBQUFULE9BQXJCLENBREcsR0FFSG1KLEtBRko7QUFHRCxLQUpnQixFQUlkLEVBSmMsQ0FBakI7O0FBS0EsVUFBTTlDLEtBQUsscUJBQVEwRCxRQUFSO0FBQWtCekMsZUFBUyxFQUFFQyxJQUFJLENBQUNDLEdBQUw7QUFBN0IsTUFBWDs7QUFDQSxVQUFNO0FBQUViLFlBQUY7QUFBVXRGO0FBQVYsUUFBbUIsTUFBTStFLGtCQUFrQixDQUFDdEQsR0FBRCxFQUFNdUQsS0FBTixDQUFqRDs7QUFFQSxRQUFJM0MsTUFBTSxDQUFDQyxJQUFQLENBQVlnRCxNQUFaLEVBQW9CL0MsTUFBcEIsR0FBNkIsQ0FBakMsRUFBb0M7QUFDbEMsYUFBT2IsR0FBRyxDQUFDSyxNQUFKLENBQVcsR0FBWCxFQUFnQkMsSUFBaEIsQ0FBcUI7QUFBRXNEO0FBQUYsT0FBckIsQ0FBUDtBQUNELEtBWkMsQ0FjRjs7O0FBQ0EsVUFBTWlFLGNBQWMsR0FBRyxNQUFNbEosd0RBQWMsQ0FDeEM2QixJQUQwQixHQUUxQnNILDhCQUYwQixDQUVLeEosSUFBSSxDQUFDaEIsS0FGVixFQUVpQmdCLElBQUksQ0FBQ2tKLFFBRnRCLENBQTdCO0FBR0EsVUFBTTtBQUFFZjtBQUFGLFFBQVVvQixjQUFjLENBQUN6TCxJQUEvQjtBQUVBLFVBQU02SyxVQUFVLEdBQUd0RyxNQUFNLENBQUNDLElBQVAsQ0FBWXRDLElBQVosRUFBa0I2SCxNQUFsQixDQUF5QixDQUFDQyxLQUFELEVBQVFuSixHQUFSLEtBQWdCO0FBQzFELGFBQU8sQ0FBQyxDQUFDLFVBQUQsRUFBYSxzQkFBYixFQUFxQytELFFBQXJDLENBQThDL0QsR0FBOUMsQ0FBRCxHQUNIMEQsTUFBTSxDQUFDc0UsTUFBUCxDQUFjbUIsS0FBZCxFQUFxQjtBQUFFLFNBQUNuSixHQUFELEdBQU9xQixJQUFJLENBQUNyQixHQUFEO0FBQWIsT0FBckIsQ0FERyxHQUVIbUosS0FGSjtBQUdELEtBSmtCLEVBSWhCLEVBSmdCLENBQW5CO0FBS0EsVUFBTXRDLEVBQUUsQ0FDTGUsVUFERyxDQUNRLE9BRFIsRUFFSE0sR0FGRyxDQUVDc0IsR0FGRCxFQUdIZixHQUhHLENBR0N1QixVQUhELENBQU47QUFJQSxVQUFNeEgsdURBQWEsQ0FBQ2UsSUFBZCxHQUFxQnVHLG1CQUFyQixDQUF5Q04sR0FBekMsRUFBOEM7QUFBRXNCLGtCQUFZLEVBQUU7QUFBaEIsS0FBOUMsQ0FBTixDQTdCRSxDQStCRjs7QUFDQSxVQUFNL0QsUUFBUSxHQUFHLE1BQU1yRix3REFBYyxDQUNsQzZCLElBRG9CLEdBRXBCbUgsMEJBRm9CLENBRU9ySixJQUFJLENBQUNoQixLQUZaLEVBRW1CZ0IsSUFBSSxDQUFDa0osUUFGeEIsQ0FBdkI7QUFHQSxVQUFNdEgsS0FBSyxHQUFHLE1BQU04RCxRQUFRLENBQUM1SCxJQUFULENBQWN3TCxVQUFkLEVBQXBCO0FBRUE1SCxPQUFHLENBQUNNLElBQUosQ0FBUztBQUFFSjtBQUFGLEtBQVQ7QUFDRCxHQXRDRCxDQXNDRSxPQUFPdEMsQ0FBUCxFQUFVO0FBQ1YsVUFBTUUsS0FBSyxHQUFHbUYsNkVBQWUsQ0FBQ3JGLENBQUQsQ0FBN0I7QUFDQW9DLE9BQUcsQ0FBQ0ssTUFBSixDQUFXLEdBQVgsRUFBZ0JDLElBQWhCLENBQXFCeEMsS0FBckI7QUFDRDtBQUNGLENBM0NEO0FBNkNlb0YscUVBQWYsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDdlJBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLE1BQU04RSxjQUFjLEdBQUcsTUFBTUMsUUFBTixJQUFrQjtBQUN2QyxNQUFJO0FBQ0YsVUFBTTtBQUFFN0w7QUFBRixRQUFXRyxvREFBSyxDQUFDQyxRQUFOLEVBQWpCO0FBQ0EsVUFBTTtBQUFFZ0U7QUFBRixRQUFXcEUsSUFBakI7QUFFQSxVQUFNRyxvREFBSyxDQUFDZ0MsUUFBTixDQUFlMkosNEZBQXNCLENBQUM7QUFBRUQsY0FBRjtBQUFZRSxVQUFJLEVBQUU7QUFBbEIsS0FBRCxDQUFyQyxDQUFOO0FBQ0E1TCx3REFBSyxDQUFDZ0MsUUFBTixDQUFlNkosNEZBQXNCLENBQUM3TCxvREFBSyxDQUFDQyxRQUFOLEdBQWlCRixNQUFqQixDQUF3QitMLElBQXpCLENBQXJDO0FBQ0FDLGdCQUFZLENBQUNMLFFBQWIsR0FBd0JBLFFBQXhCO0FBQ0ExTCx3REFBSyxDQUFDZ0MsUUFBTixDQUFlZ0ssb0ZBQWMsQ0FBQztBQUFFTixjQUFRLEVBQUVBO0FBQVosS0FBRCxDQUE3Qjs7QUFFQSxRQUFJekgsSUFBSixFQUFVO0FBQ1IsWUFBTWdJLGNBQWMscUJBQVFoSSxJQUFJLENBQUNnRyxPQUFMLENBQWE1SixXQUFyQjtBQUFrQ3FMO0FBQWxDLFFBQXBCOztBQUNBMUwsMERBQUssQ0FBQ2dDLFFBQU4sQ0FBZWdKLDhFQUFVLENBQUM7QUFBRWxELFVBQUUsRUFBRTdELElBQUksQ0FBQ2lHLEdBQVg7QUFBZ0I3SixtQkFBVyxFQUFFNEw7QUFBN0IsT0FBRCxDQUF6QjtBQUNEO0FBQ0YsR0FiRCxDQWFFLE9BQU81SyxDQUFQLEVBQVU7QUFDVkMsV0FBTyxDQUFDQyxLQUFSLENBQWNGLENBQWQ7QUFDRDtBQUNGLENBakJEOztBQW1CQSxNQUFNNkssZ0JBQWdCLEdBQUcsTUFBTUMsVUFBTixJQUFvQjtBQUMzQyxNQUFJO0FBQ0YsVUFBTTtBQUFFdE07QUFBRixRQUFXRyxvREFBSyxDQUFDQyxRQUFOLEVBQWpCO0FBQ0EsVUFBTTtBQUFFZ0U7QUFBRixRQUFXcEUsSUFBakI7QUFFQWtNLGdCQUFZLENBQUNJLFVBQWIsR0FBMEJBLFVBQTFCO0FBQ0FuTSx3REFBSyxDQUFDZ0MsUUFBTixDQUFlZ0ssb0ZBQWMsQ0FBQztBQUFFRztBQUFGLEtBQUQsQ0FBN0I7O0FBRUEsUUFBSWxJLElBQUosRUFBVTtBQUNSLFlBQU1nSSxjQUFjLHFCQUFRaEksSUFBSSxDQUFDZ0csT0FBTCxDQUFhNUosV0FBckI7QUFBa0M4TDtBQUFsQyxRQUFwQjs7QUFDQW5NLDBEQUFLLENBQUNnQyxRQUFOLENBQWVnSiw4RUFBVSxDQUFDO0FBQUVsRCxVQUFFLEVBQUU3RCxJQUFJLENBQUNpRyxHQUFYO0FBQWdCN0osbUJBQVcsRUFBRTRMO0FBQTdCLE9BQUQsQ0FBekI7QUFDRDtBQUNGLEdBWEQsQ0FXRSxPQUFPNUssQ0FBUCxFQUFVO0FBQ1ZDLFdBQU8sQ0FBQ0MsS0FBUixDQUFjRixDQUFkO0FBQ0Q7QUFDRixDQWZEOztBQWlCQSxNQUFNK0ssY0FBYyxHQUFHLE1BQU05TCxRQUFOLElBQWtCO0FBQ3ZDLE1BQUk7QUFDRixVQUFNO0FBQUVUO0FBQUYsUUFBV0csb0RBQUssQ0FBQ0MsUUFBTixFQUFqQjtBQUNBLFVBQU07QUFBRWdFO0FBQUYsUUFBV3BFLElBQWpCO0FBQ0FrTSxnQkFBWSxDQUFDekwsUUFBYixHQUF3QkEsUUFBeEI7QUFDQU4sd0RBQUssQ0FBQ2dDLFFBQU4sQ0FBZWdLLG9GQUFjLENBQUM7QUFBRTFMLGNBQVEsRUFBRUE7QUFBWixLQUFELENBQTdCOztBQUVBLFFBQUkyRCxJQUFKLEVBQVU7QUFDUixZQUFNZ0ksY0FBYyxxQkFBUWhJLElBQUksQ0FBQ2dHLE9BQUwsQ0FBYTVKLFdBQXJCO0FBQWtDQztBQUFsQyxRQUFwQjs7QUFDQU4sMERBQUssQ0FBQ2dDLFFBQU4sQ0FBZWdKLDhFQUFVLENBQUM7QUFBRWxELFVBQUUsRUFBRTdELElBQUksQ0FBQ2lHLEdBQVg7QUFBZ0I3SixtQkFBVyxFQUFFNEw7QUFBN0IsT0FBRCxDQUF6QjtBQUNEO0FBQ0YsR0FWRCxDQVVFLE9BQU81SyxDQUFQLEVBQVU7QUFDVkMsV0FBTyxDQUFDQyxLQUFSLENBQWNGLENBQWQ7QUFDRDtBQUNGLENBZEQ7O0FBZ0JBLE1BQU1nTCxlQUFlLEdBQUcsTUFBTTtBQUM1QixRQUFNO0FBQUV0TSxVQUFGO0FBQVVGO0FBQVYsTUFBbUJHLG9EQUFLLENBQUNDLFFBQU4sRUFBekI7QUFDQSxRQUFNcU0sa0JBQWtCLEdBQUd2TSxNQUFNLENBQUNNLFdBQWxDO0FBQ0EsUUFBTWtNLGVBQWUsR0FBSTFNLElBQUksQ0FBQ29FLElBQUwsSUFBYXBFLElBQUksQ0FBQ29FLElBQUwsQ0FBVWdHLE9BQXZCLElBQWtDcEssSUFBSSxDQUFDb0UsSUFBTCxDQUFVZ0csT0FBVixDQUFrQjVKLFdBQTdFO0FBQ0EsUUFBTW1NLGdCQUFnQixHQUFHO0FBQ3ZCZCxZQUFRLEVBQUVLLFlBQVksQ0FBQ0wsUUFBYixJQUF5Qlksa0JBQWtCLENBQUNaLFFBRC9CO0FBRXZCUyxjQUFVLEVBQUVKLFlBQVksQ0FBQ0ksVUFBYixJQUEyQkcsa0JBQWtCLENBQUNILFVBRm5DO0FBR3ZCN0wsWUFBUSxFQUFFeUwsWUFBWSxDQUFDekwsUUFBYixJQUF5QmdNLGtCQUFrQixDQUFDaE07QUFIL0IsR0FBekI7QUFLQSxRQUFNRCxXQUFXLEdBQUcrRCxNQUFNLENBQUNzRSxNQUFQLENBQ2xCNEQsa0JBRGtCLEVBRWxCRSxnQkFGa0IsRUFHbEJELGVBSGtCLENBQXBCO0FBS0EsUUFBTTtBQUFFYjtBQUFGLE1BQWVyTCxXQUFyQjtBQUVBTCxzREFBSyxDQUFDZ0MsUUFBTixDQUFlMkosNEZBQXNCLENBQUM7QUFBRUQsWUFBRjtBQUFZRSxRQUFJLEVBQUU7QUFBbEIsR0FBRCxDQUFyQyxFQUNHYSxJQURILENBQ1EsTUFBTXpNLG9EQUFLLENBQUNnQyxRQUFOLENBQWU2Siw0RkFBc0IsQ0FBQzdMLG9EQUFLLENBQUNDLFFBQU4sR0FBaUJGLE1BQWpCLENBQXdCK0wsSUFBekIsQ0FBckMsQ0FEZDtBQUdBOUwsc0RBQUssQ0FBQ2dDLFFBQU4sQ0FBZWdLLG9GQUFjLENBQUMzTCxXQUFELENBQTdCO0FBQ0QsQ0FwQkQ7O0FBc0JBLE1BQU1xTSxNQUFNLEdBQUcsWUFBWTtBQUN6QixNQUFJO0FBQ0ZYLGdCQUFZLENBQUNZLFVBQWIsQ0FBd0IsT0FBeEI7QUFDQSxVQUFNM00sb0RBQUssQ0FBQ2dDLFFBQU4sQ0FBZTRLLDBFQUFZLEVBQTNCLENBQU47QUFDQUMsVUFBTSxDQUFDQyxRQUFQLENBQWdCQyxJQUFoQixHQUF3QixHQUFFRixNQUFNLENBQUNDLFFBQVAsQ0FBZ0JFLE1BQU8sUUFBakQ7QUFDRCxHQUpELENBSUUsT0FBTzNMLENBQVAsRUFBVTtBQUNWQyxXQUFPLENBQUNDLEtBQVIsQ0FBY0YsQ0FBZDtBQUNEO0FBQ0YsQ0FSRDs7QUFVZTRMLDJHQUFLLENBQUNDLGFBQU4sQ0FBb0I7QUFDakN6QixnQkFEaUM7QUFFakNTLGtCQUZpQztBQUdqQ0UsZ0JBSGlDO0FBSWpDQyxpQkFKaUM7QUFLakNLO0FBTGlDLENBQXBCLENBQWYsRTs7Ozs7Ozs7Ozs7O0FDekZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFZSxNQUFNUyxXQUFOLFNBQTBCRiw0Q0FBSyxDQUFDRyxTQUFoQyxDQUEwQztBQUFBO0FBQUE7O0FBQUEsd0NBRXZEQyxrQkFGdUQsR0FFbENDLFNBQVMsSUFBSTtBQUNoQyxVQUFJQSxTQUFTLENBQUNDLE1BQVYsS0FBcUIsS0FBS0MsS0FBTCxDQUFXRCxNQUFwQyxFQUE0QztBQUMxQyxhQUFLRSxXQUFMLENBQWlCQyxTQUFqQixDQUEyQkMsTUFBM0IsQ0FBa0MsWUFBbEM7QUFFQUMsa0JBQVUsQ0FBQyxNQUFNO0FBQ2YsZUFBS0gsV0FBTCxDQUFpQkMsU0FBakIsQ0FBMkJDLE1BQTNCLENBQWtDLFlBQWxDOztBQUVBLGNBQUksS0FBS0gsS0FBTCxDQUFXRCxNQUFmLEVBQXVCO0FBQ3JCLGlCQUFLQyxLQUFMLENBQVdLLFVBQVg7QUFDRDtBQUNGLFNBTlMsRUFNUCxDQU5PLENBQVY7QUFPRDtBQUNGLEtBZHNEO0FBQUE7O0FBZ0J2REMsUUFBTSxHQUFJO0FBQ1IsVUFBTTtBQUFFUCxZQUFGO0FBQVVRLGVBQVY7QUFBcUJDO0FBQXJCLFFBQWtDLEtBQUtSLEtBQTdDO0FBRUEsd0JBQ0U7QUFDRSxTQUFHLEVBQUVTLElBQUksSUFBSSxLQUFLUixXQUFMLEdBQW1CUSxJQURsQztBQUVFLHVCQUFlVixNQUZqQjtBQUdFLGVBQVMsRUFBRyxHQUFFUSxTQUFVLGFBQVlSLE1BQU0sSUFBSSxNQUFPO0FBSHZELE9BSUdTLFFBSkgsQ0FERjtBQVFEOztBQTNCc0QsQzs7Ozs7Ozs7Ozs7O0FDRnpEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBRUEsTUFBTUUsYUFBYSxHQUFHQyx3REFBTSxDQUFDQyxNQUFPOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Q0FBcEM7QUFvQmUsU0FBU0MsTUFBVCxDQUFnQmIsS0FBaEIsRUFBdUI7QUFDcEMsUUFBTTtBQUFFek47QUFBRixNQUFheU4sS0FBbkI7QUFFQSxzQkFDRSwyREFBQyxhQUFEO0FBQWUsYUFBUyxFQUFDO0FBQXpCLGtCQUNFO0FBQUssYUFBUyxFQUFDO0FBQWYsa0JBQ0U7QUFBSyxhQUFTLEVBQUM7QUFBZixrQkFDRTtBQUFLLGFBQVMsRUFBQztBQUFmLGtCQUNFO0FBQUssYUFBUyxFQUFDO0FBQWYsa0JBQ0U7QUFBSyxPQUFHLEVBQUV6TixNQUFNLENBQUN1TyxPQUFqQjtBQUEwQixPQUFHLEVBQUV2TyxNQUFNLENBQUN3TyxPQUF0QztBQUErQyxTQUFLLEVBQUU7QUFBdEQsSUFERixFQUMrRCxHQUQvRCxlQUVFLHlFQUFPeE8sTUFBTSxDQUFDd08sT0FBZCxDQUZGLENBREYsZUFLRTtBQUFLLGFBQVMsRUFBQztBQUFmLGtCQUNFLHNFQUFJdE4sbUVBQVMsQ0FBQ2xCLE1BQU0sQ0FBQ2EsWUFBUixFQUFzQixpQkFBdEIsQ0FBYixDQURGLENBTEYsZUFRRTtBQUFLLGFBQVMsRUFBQztBQUFmLGtCQUNFO0FBQUksYUFBUyxFQUFDO0FBQWQsa0JBQ0U7QUFBSSxhQUFTLEVBQUM7QUFBZCxrQkFDRTtBQUNFLFFBQUksRUFBQyw0Q0FEUDtBQUVFLFNBQUssRUFBQyxXQUZSO0FBR0UsVUFBTSxFQUFDLFFBSFQ7QUFJRSxPQUFHLEVBQUM7QUFKTixrQkFLRTtBQUFHLGFBQVMsRUFBQztBQUFiLElBTEYsQ0FERixDQURGLGVBVUU7QUFBSSxhQUFTLEVBQUM7QUFBZCxrQkFDRTtBQUNFLFFBQUksRUFBQyw0REFEUDtBQUVFLFNBQUssRUFBQyxVQUZSO0FBR0UsVUFBTSxFQUFDLFFBSFQ7QUFJRSxPQUFHLEVBQUM7QUFKTixrQkFLRTtBQUFHLGFBQVMsRUFBQztBQUFiLElBTEYsQ0FERixDQVZGLGVBbUJFO0FBQUksYUFBUyxFQUFDO0FBQWQsa0JBQ0U7QUFDRSxRQUFJLEVBQUMscUNBRFA7QUFFRSxTQUFLLEVBQUMsU0FGUjtBQUdFLFVBQU0sRUFBQyxRQUhUO0FBSUUsT0FBRyxFQUFDO0FBSk4sa0JBS0U7QUFBRyxhQUFTLEVBQUM7QUFBYixJQUxGLENBREYsQ0FuQkYsZUE0QkU7QUFBSSxhQUFTLEVBQUM7QUFBZCxrQkFDRTtBQUNFLFFBQUksRUFBQywwQ0FEUDtBQUVFLFNBQUssRUFBQyxPQUZSO0FBR0UsVUFBTSxFQUFDLFFBSFQ7QUFJRSxPQUFHLEVBQUM7QUFKTixrQkFLRTtBQUFHLGFBQVMsRUFBQztBQUFiLElBTEYsQ0FERixDQTVCRixDQURGLENBUkYsQ0FERixlQWtERTtBQUFLLGFBQVMsRUFBQztBQUFmLGtCQUNFO0FBQUksYUFBUyxFQUFDO0FBQWQsa0JBQ0U7QUFBSSxhQUFTLEVBQUM7QUFBZCxrQkFDRSwyREFBQyxxREFBRDtBQUFNLE1BQUUsRUFBQztBQUFULEtBQXFCSyxtRUFBUyxDQUFDbEIsTUFBTSxDQUFDYSxZQUFSLEVBQXNCLG9CQUF0QixDQUE5QixDQURGLENBREYsZUFJRTtBQUFJLGFBQVMsRUFBQztBQUFkLGtCQUNFLDJEQUFDLHFEQUFEO0FBQU0sTUFBRSxFQUFDO0FBQVQsS0FBMkJLLG1FQUFTLENBQUNsQixNQUFNLENBQUNhLFlBQVIsRUFBc0IsV0FBdEIsQ0FBcEMsQ0FERixDQUpGLENBREYsQ0FsREYsQ0FERixDQURGLENBREY7QUFtRUQsQzs7Ozs7Ozs7Ozs7O0FDL0ZEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUVBLE1BQU00TixhQUFhLEdBQUdMLHdEQUFNLENBQUNNLE1BQU87Ozs7Q0FBcEM7QUFNZSxTQUFTQyxNQUFULENBQWdCbEIsS0FBaEIsRUFBdUI7QUFDcEMsUUFBTTtBQUFFek4sVUFBRjtBQUFVRixRQUFWO0FBQWdCOE8sU0FBaEI7QUFBdUJDLGVBQXZCO0FBQW9DQyxXQUFwQztBQUE2Q0MsZUFBN0M7QUFBMERDLFdBQTFEO0FBQW1FQztBQUFuRSxNQUFnRnhCLEtBQXRGO0FBRUEsc0JBQ0UsMkRBQUMsYUFBRCxxQkFDRSwyREFBQywrQ0FBRDtBQUNFLFVBQU0sRUFBRXpOLE1BRFY7QUFFRSxRQUFJLEVBQUVGLElBRlI7QUFHRSxXQUFPLEVBQUVnUDtBQUhYLElBREYsRUFPSUMsV0FBVyxpQkFDWCwyREFBQyx1REFBRDtBQUNFLFVBQU0sRUFBRS9PLE1BRFY7QUFFRSxTQUFLLEVBQUU0TyxLQUZUO0FBR0UsZUFBVyxFQUFFQyxXQUhmO0FBSUUsV0FBTyxFQUFFRyxPQUpYO0FBS0UsWUFBUSxFQUFFQztBQUxaLElBUkosQ0FERjtBQW1CRCxDOzs7Ozs7Ozs7Ozs7QUNqQ0Q7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFFZSxTQUFTQyxRQUFULENBQWtCekIsS0FBbEIsRUFBeUI7QUFDdEMsUUFBTTtBQUFFdkosUUFBRjtBQUFRaUwsY0FBUjtBQUFvQm5QLFVBQXBCO0FBQTRCb1AsZUFBNUI7QUFBeUNDO0FBQXpDLE1BQTBENUIsS0FBaEU7QUFFQSxzQkFDRTtBQUFJLGFBQVMsRUFBQztBQUFkLEtBRUcwQixVQUFVLElBQUlqTCxJQUFkLElBQXNCQSxJQUFJLENBQUNFLE1BQUwsQ0FBWXdCLElBQVosQ0FBaUJuQixJQUFJLElBQUlBLElBQUksS0FBSyxjQUFsQyxDQUF0QixpQkFDQywyREFBQyw0Q0FBRCxDQUFPLFFBQVAscUJBQ0U7QUFDRSxRQUFJLEVBQUMsVUFEUDtBQUVFLGFBQVMsRUFBRyxZQUFXMkssV0FBVyxLQUFLLGdCQUFoQixJQUFvQyxRQUFTLEVBRnRFO0FBR0UsUUFBSSxFQUFDLGdCQUhQO0FBSUUsU0FBSyxFQUFFbE8sbUVBQVMsQ0FBQ2xCLE1BQU0sQ0FBQ2EsWUFBUixFQUFzQixXQUF0QixDQUpsQjtBQUtFLFdBQU8sRUFBRVMsQ0FBQyxJQUFJO0FBQ1pBLE9BQUMsQ0FBQ2dPLGNBQUY7QUFDQUQsa0JBQVksQ0FBQy9OLENBQUMsQ0FBQ2lPLGFBQUYsQ0FBZ0JDLFFBQWpCLENBQVo7QUFDRDtBQVJILGtCQVNFO0FBQUcsYUFBUyxFQUFDO0FBQWIsSUFURixlQVVFO0FBQU0sYUFBUyxFQUFDO0FBQWhCLEtBQ0d0TyxtRUFBUyxDQUFDbEIsTUFBTSxDQUFDYSxZQUFSLEVBQXNCLFdBQXRCLENBRFosQ0FWRixDQURGLENBSEosRUFxQkcsQ0FBQ3FELElBQUQsaUJBQ0MsMkRBQUMsNENBQUQsQ0FBTyxRQUFQLHFCQUNFO0FBQ0UsUUFBSSxFQUFDLFVBRFA7QUFFRSxhQUFTLEVBQUcsWUFBV2tMLFdBQVcsS0FBSyxHQUFoQixJQUF1QixRQUFTLEVBRnpEO0FBR0UsUUFBSSxFQUFDLEdBSFA7QUFJRSxXQUFPLEVBQUU5TixDQUFDLElBQUk7QUFDWkEsT0FBQyxDQUFDZ08sY0FBRjtBQUNBRCxrQkFBWSxDQUFDL04sQ0FBQyxDQUFDaU8sYUFBRixDQUFnQkMsUUFBakIsQ0FBWjtBQUNEO0FBUEgsS0FRR3RPLG1FQUFTLENBQUNsQixNQUFNLENBQUNhLFlBQVIsRUFBc0IsTUFBdEIsQ0FSWixDQURGLGVBV0U7QUFDRSxRQUFJLEVBQUMsVUFEUDtBQUVFLGFBQVMsRUFBRyx5REFBd0R1TyxXQUFXLEtBQUssUUFBaEIsSUFBNEIsUUFBUyxFQUYzRztBQUdFLFFBQUksRUFBQyxRQUhQO0FBSUUsV0FBTyxFQUFFOU4sQ0FBQyxJQUFJO0FBQ1pBLE9BQUMsQ0FBQ2dPLGNBQUY7QUFDQUQsa0JBQVksQ0FBQy9OLENBQUMsQ0FBQ2lPLGFBQUYsQ0FBZ0JDLFFBQWpCLENBQVo7QUFDRDtBQVBILEtBUUd0TyxtRUFBUyxDQUFDbEIsTUFBTSxDQUFDYSxZQUFSLEVBQXNCLE9BQXRCLENBUlosQ0FYRixlQXFCRTtBQUNFLFFBQUksRUFBQyxVQURQO0FBRUUsYUFBUyxFQUFHLCtDQUE4Q3VPLFdBQVcsS0FBSyxXQUFoQixJQUErQixRQUFTLEVBRnBHO0FBR0UsUUFBSSxFQUFDLFdBSFA7QUFJRSxXQUFPLEVBQUU5TixDQUFDLElBQUk7QUFDWkEsT0FBQyxDQUFDZ08sY0FBRjtBQUNBRCxrQkFBWSxDQUFDL04sQ0FBQyxDQUFDaU8sYUFBRixDQUFnQkMsUUFBakIsQ0FBWjtBQUNEO0FBUEgsS0FRR3RPLG1FQUFTLENBQUNsQixNQUFNLENBQUNhLFlBQVIsRUFBc0IsVUFBdEIsQ0FSWixDQXJCRixDQXRCSixDQURGO0FBMERELEM7Ozs7Ozs7Ozs7OztBQ2hFRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFFZSxTQUFTNE8sWUFBVCxDQUFzQmhDLEtBQXRCLEVBQTZCO0FBQzFDLFFBQU0sQ0FBQ2lDLGdCQUFELEVBQW1CQyxtQkFBbkIsSUFBMENDLHNEQUFRLENBQUMsSUFBRCxDQUF4RDtBQUNBLFFBQU0sQ0FBQ0Msc0JBQUQsRUFBeUJDLHlCQUF6QixJQUFzREYsc0RBQVEsQ0FBQyxJQUFELENBQXBFO0FBQ0EsUUFBTSxDQUFDRyxrQkFBRCxFQUFxQkMscUJBQXJCLElBQThDSixzREFBUSxDQUFDLElBQUQsQ0FBNUQ7QUFDQSxRQUFNLENBQUNLLGdCQUFELEVBQW1CQyxlQUFuQixJQUFzQ04sc0RBQVEsQ0FBQyxLQUFELENBQXBEO0FBQ0EsUUFBTSxDQUFDTyxzQkFBRCxFQUF5QkMscUJBQXpCLElBQWtEUixzREFBUSxDQUFDLEtBQUQsQ0FBaEU7QUFDQSxRQUFNLENBQUNTLGtCQUFELEVBQXFCQyxpQkFBckIsSUFBMENWLHNEQUFRLENBQUMsS0FBRCxDQUF4RDtBQUVBLFFBQU07QUFDSjFMLFFBREk7QUFDRWlMLGNBREY7QUFDY25QLFVBRGQ7QUFFSnFQLGdCQUZJO0FBRVVrQixzQkFGVjtBQUU4QkMsb0JBRjlCO0FBRWdEQztBQUZoRCxNQUdGaEQsS0FISixDQVIwQyxDQVkxQztBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNBLFFBQU1pRCxXQUFXLEdBQUcsQ0FBQyxZQUFELEVBQWUsWUFBZixFQUE2QixZQUE3QixFQUEyQ0MsR0FBM0MsQ0FBK0NsTSxJQUFJLEtBQUs7QUFBRW1NLFNBQUssRUFBRW5NLElBQVQ7QUFBZW9NLFNBQUssRUFBRXBNO0FBQXRCLEdBQUwsQ0FBbkQsQ0FBcEI7QUFDQSxRQUFNcU0sU0FBUyxHQUFHOVEsTUFBTSxDQUFDUyxRQUFQLENBQWdCcVEsU0FBaEIsSUFBNkIsRUFBL0M7O0FBRUEsUUFBTUMsY0FBYyxHQUFHN0ssSUFBSSxJQUFJO0FBQzdCZ0ssbUJBQWUsQ0FBQyxLQUFELENBQWY7QUFDQWIsZ0JBQVksQ0FBQ25KLElBQUQsQ0FBWjtBQUNELEdBSEQ7O0FBS0EsUUFBTThLLGdCQUFnQixHQUFHOUssSUFBSSxJQUFJO0FBQy9Cb0sscUJBQWlCLENBQUMsS0FBRCxDQUFqQjtBQUNBakIsZ0JBQVksQ0FBQ25KLElBQUQsQ0FBWjtBQUNELEdBSEQ7O0FBS0Esc0JBQ0UsMkRBQUMsNENBQUQsQ0FBTyxRQUFQLHFCQUVJO0FBQ0EsT0FBRyxFQUFFZ0ksSUFBSSxJQUFJNEIseUJBQXlCLENBQUM1QixJQUFELENBRHRDO0FBRUEsYUFBUyxFQUFDLG9CQUZWO0FBR0EsU0FBSyxFQUFFaE4sbUVBQVMsQ0FBQ2xCLE1BQU0sQ0FBQ2EsWUFBUixFQUFzQixhQUF0QixDQUhoQjtBQUlBLFdBQU8sRUFBRSxNQUFNdVAscUJBQXFCLENBQUMsQ0FBQ0Qsc0JBQUY7QUFKcEMsa0JBS0E7QUFBRyxhQUFTLEVBQUM7QUFBYixJQUxBLENBRkosZUFTRSwyREFBQywyREFBRDtBQUNFLFVBQU0sRUFBRUEsc0JBRFY7QUFFRSxVQUFNLEVBQUVOLHNCQUZWO0FBR0UsU0FBSyxFQUFFO0FBQUVvQixXQUFLLEVBQUU7QUFBVCxLQUhUO0FBSUUsWUFBUSxFQUFFLE1BQU1iLHFCQUFxQixDQUFDLENBQUNELHNCQUFGO0FBSnZDLGtCQUtFO0FBQUksYUFBUyxFQUFDO0FBQWQsS0FBZ0NqUCxtRUFBUyxDQUFDbEIsTUFBTSxDQUFDYSxZQUFSLEVBQXNCLGFBQXRCLENBQXpDLENBTEYsZUFNRTtBQUFLLGFBQVMsRUFBQztBQUFmLGtCQUNFO0FBQUssYUFBUyxFQUFDO0FBQWYsa0JBQ0UsMEVBQVFLLG1FQUFTLENBQUNsQixNQUFNLENBQUNhLFlBQVIsRUFBc0IsWUFBdEIsQ0FBakIsQ0FERixlQUVFLDJEQUFDLG1EQUFEO0FBQ0UsZUFBVyxFQUFFSyxtRUFBUyxDQUFDbEIsTUFBTSxDQUFDYSxZQUFSLEVBQXNCLFFBQXRCLENBRHhCO0FBRUUsV0FBTyxFQUFFNlAsV0FGWDtBQUdFLFNBQUssRUFBRUEsV0FBVyxDQUFDOUssSUFBWixDQUFpQm5CLElBQUksSUFBSUEsSUFBSSxDQUFDb00sS0FBTCxLQUFlN1EsTUFBTSxDQUFDTSxXQUFQLENBQW1COEwsVUFBM0QsQ0FIVDtBQUlFLFlBQVEsRUFBRThFLE1BQU0sSUFBSVgsa0JBQWtCLENBQUNXLE1BQU0sQ0FBQ0wsS0FBUjtBQUp4QyxJQUZGLENBREYsZUFTRTtBQUFLLGFBQVMsRUFBQztBQUFmLGtCQUNFLDBFQUFRM1AsbUVBQVMsQ0FBQ2xCLE1BQU0sQ0FBQ2EsWUFBUixFQUFzQixVQUF0QixDQUFqQixDQURGLGVBRUUsMkRBQUMsbURBQUQ7QUFDRSxlQUFXLEVBQUVLLG1FQUFTLENBQUNsQixNQUFNLENBQUNhLFlBQVIsRUFBc0IsUUFBdEIsQ0FEeEI7QUFFRSxXQUFPLEVBQUVpUSxTQUFTLENBQUN0TSxNQUFWLENBQWlCQyxJQUFJLElBQUl6RSxNQUFNLENBQUNxSCxZQUFQLENBQW9CM0MsUUFBcEIsQ0FBNkJELElBQUksQ0FBQ29NLEtBQWxDLENBQXpCLENBRlg7QUFHRSxTQUFLLEVBQUVDLFNBQVMsQ0FBQ2xMLElBQVYsQ0FBZW5CLElBQUksSUFBSUEsSUFBSSxDQUFDb00sS0FBTCxLQUFlN1EsTUFBTSxDQUFDTSxXQUFQLENBQW1CQyxRQUF6RCxDQUhUO0FBSUUsWUFBUSxFQUFFMlEsTUFBTSxJQUFJVixnQkFBZ0IsQ0FBQ1UsTUFBTSxDQUFDTCxLQUFSO0FBSnRDLElBRkYsQ0FURixDQU5GLENBVEYsRUE0Q0kxQixVQUFVLElBQUlqTCxJQUFkLElBQXNCQSxJQUFJLENBQUNFLE1BQUwsQ0FBWXdCLElBQVosQ0FBaUJuQixJQUFJLElBQUlBLElBQUksS0FBSyxTQUFsQyxDQUF0QixpQkFDQSwyREFBQyw0Q0FBRCxDQUFPLFFBQVAscUJBQ0U7QUFDRSxPQUFHLEVBQUV5SixJQUFJLElBQUl5QixtQkFBbUIsQ0FBQ3pCLElBQUQsQ0FEbEM7QUFFRSxhQUFTLEVBQUMsb0JBRlo7QUFHRSxTQUFLLEVBQUVoTixtRUFBUyxDQUFDbEIsTUFBTSxDQUFDYSxZQUFSLEVBQXNCLE9BQXRCLENBSGxCO0FBSUUsV0FBTyxFQUFFLE1BQU1xUCxlQUFlLENBQUMsQ0FBQ0QsZ0JBQUY7QUFKaEMsa0JBS0U7QUFBRyxhQUFTLEVBQUM7QUFBYixJQUxGLENBREYsZUFRRSwyREFBQywyREFBRDtBQUNFLFVBQU0sRUFBRUEsZ0JBRFY7QUFFRSxVQUFNLEVBQUVQLGdCQUZWO0FBR0UsU0FBSyxFQUFFO0FBQUV1QixXQUFLLEVBQUU7QUFBVCxLQUhUO0FBSUUsWUFBUSxFQUFFLE1BQU1mLGVBQWUsQ0FBQyxDQUFDRCxnQkFBRjtBQUpqQyxrQkFLRTtBQUFJLGFBQVMsRUFBQztBQUFkLEtBQWdDL08sbUVBQVMsQ0FBQ2xCLE1BQU0sQ0FBQ2EsWUFBUixFQUFzQixPQUF0QixDQUF6QyxDQUxGLGVBTUU7QUFBSyxhQUFTLEVBQUM7QUFBZixrQkFDRTtBQUFJLGFBQVMsRUFBQztBQUFkLGtCQUNFO0FBQUksYUFBUyxFQUFDO0FBQWQsa0JBQ0U7QUFDRSxhQUFTLEVBQUMsU0FEWjtBQUVFLFdBQU8sRUFBRSxNQUFNa1EsY0FBYyxDQUFDLGlCQUFEO0FBRi9CLEtBR0c3UCxtRUFBUyxDQUFDbEIsTUFBTSxDQUFDYSxZQUFSLEVBQXNCLFVBQXRCLENBSFosQ0FERixDQURGLGVBUUU7QUFBSSxhQUFTLEVBQUM7QUFBZCxrQkFDRTtBQUNFLGFBQVMsRUFBQyxTQURaO0FBRUUsV0FBTyxFQUFFLE1BQU1rUSxjQUFjLENBQUMsc0JBQUQ7QUFGL0IsS0FHRzdQLG1FQUFTLENBQUNsQixNQUFNLENBQUNhLFlBQVIsRUFBc0IsZUFBdEIsQ0FIWixDQURGLENBUkYsZUFlRTtBQUFJLGFBQVMsRUFBQztBQUFkLGtCQUNFO0FBQ0UsYUFBUyxFQUFDLFNBRFo7QUFFRSxXQUFPLEVBQUUsTUFBTWtRLGNBQWMsQ0FBQyxnQkFBRDtBQUYvQixLQUdHN1AsbUVBQVMsQ0FBQ2xCLE1BQU0sQ0FBQ2EsWUFBUixFQUFzQixTQUF0QixDQUhaLENBREYsQ0FmRixlQXNCRTtBQUFJLGFBQVMsRUFBQztBQUFkLGtCQUNFO0FBQ0UsYUFBUyxFQUFDLFNBRFo7QUFFRSxXQUFPLEVBQUUsTUFBTWtRLGNBQWMsQ0FBQyxjQUFEO0FBRi9CLEtBR0c3UCxtRUFBUyxDQUFDbEIsTUFBTSxDQUFDYSxZQUFSLEVBQXNCLE9BQXRCLENBSFosQ0FERixDQXRCRixDQURGLENBTkYsQ0FSRixDQTdDSixFQWdHSXFELElBQUksaUJBQ0osMkRBQUMsNENBQUQsQ0FBTyxRQUFQLHFCQUNFO0FBQ0UsT0FBRyxFQUFFZ0ssSUFBSSxJQUFJOEIscUJBQXFCLENBQUM5QixJQUFELENBRHBDO0FBRUUsYUFBUyxFQUFDLG9CQUZaO0FBR0UsU0FBSyxFQUFFaE4sbUVBQVMsQ0FBQ2xCLE1BQU0sQ0FBQ2EsWUFBUixFQUFzQixhQUF0QixDQUhsQjtBQUlFLFdBQU8sRUFBRSxNQUFNeVAsaUJBQWlCLENBQUMsQ0FBQ0Qsa0JBQUY7QUFKbEMsa0JBS0U7QUFBRyxhQUFTLEVBQUM7QUFBYixJQUxGLENBREYsZUFRRSwyREFBQywyREFBRDtBQUNFLFVBQU0sRUFBRUEsa0JBRFY7QUFFRSxVQUFNLEVBQUVOLGtCQUZWO0FBR0UsU0FBSyxFQUFFO0FBQUVrQixXQUFLLEVBQUU7QUFBVCxLQUhUO0FBSUUsWUFBUSxFQUFFLE1BQU1YLGlCQUFpQixDQUFDLENBQUNELGtCQUFGO0FBSm5DLGtCQUtFO0FBQUksYUFBUyxFQUFDO0FBQWQsS0FBZ0NuUCxtRUFBUyxDQUFDbEIsTUFBTSxDQUFDYSxZQUFSLEVBQXNCLGFBQXRCLENBQXpDLENBTEYsZUFNRTtBQUFLLGFBQVMsRUFBQztBQUFmLGtCQUNFO0FBQUksYUFBUyxFQUFDO0FBQWQsa0JBQ0U7QUFBSSxhQUFTLEVBQUM7QUFBZCxrQkFDRTtBQUNFLGFBQVMsRUFBQyxTQURaO0FBRUUsV0FBTyxFQUFFLE1BQU1tUSxnQkFBZ0IsQ0FBQyxtQkFBRDtBQUZqQyxLQUdHOVAsbUVBQVMsQ0FBQ2xCLE1BQU0sQ0FBQ2EsWUFBUixFQUFzQixTQUF0QixDQUhaLENBREYsQ0FERixlQVFFO0FBQUksYUFBUyxFQUFDO0FBQWQsa0JBQ0U7QUFDRSxhQUFTLEVBQUMsU0FEWjtBQUVFLFdBQU8sRUFBRSxNQUFNO0FBQ2J5UCx1QkFBaUIsQ0FBQyxLQUFELENBQWpCO0FBQ0FHLGNBQVE7QUFDVDtBQUxILEtBTUd2UCxtRUFBUyxDQUFDbEIsTUFBTSxDQUFDYSxZQUFSLEVBQXNCLFFBQXRCLENBTlosQ0FERixDQVJGLENBREYsQ0FORixDQVJGLENBakdKLENBREY7QUEwSUQsQzs7Ozs7Ozs7Ozs7O0FDN0tEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUEsTUFBTXNRLGFBQWEsR0FBRy9DLHdEQUFNLENBQUNnRCxHQUFJOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0NBQWpDO0FBc0RlLFNBQVNDLE1BQVQsQ0FBZ0I1RCxLQUFoQixFQUF1QjtBQUNwQyxRQUFNLENBQUM2RCxRQUFELEVBQVdDLE9BQVgsSUFBc0IzQixzREFBUSxDQUFDLEtBQUQsQ0FBcEM7QUFDQSxRQUFNNEIsT0FBTyxHQUFHQyx3REFBVSxDQUFDQyxnREFBRCxDQUExQjtBQUNBLFFBQU07QUFBRTFSLFVBQUY7QUFBVUYsUUFBVjtBQUFnQmdQO0FBQWhCLE1BQTRCckIsS0FBbEM7QUFDQSxRQUFNO0FBQUV2SixRQUFGO0FBQVFpTDtBQUFSLE1BQXVCclAsSUFBN0I7QUFDQSxRQUFNc1AsV0FBVyxHQUFHTixPQUFPLENBQUMvQixRQUFSLENBQWlCeUMsUUFBckM7QUFFQSxzQkFDRSwyREFBQyxhQUFEO0FBQWUsYUFBUyxFQUFDO0FBQXpCLGtCQUNFO0FBQVEsYUFBUyxFQUFDLGtCQUFsQjtBQUFxQyxXQUFPLEVBQUUsTUFBTVYsT0FBTyxDQUFDNkMsSUFBUixDQUFhLEdBQWI7QUFBcEQsa0JBQ0U7QUFBSyxPQUFHLEVBQUUzUixNQUFNLENBQUN1TyxPQUFqQjtBQUEwQixPQUFHLEVBQUV2TyxNQUFNLENBQUN3TyxPQUF0QztBQUErQyxTQUFLLEVBQUU7QUFBdEQsSUFERixFQUMrRCxHQUQvRCxlQUVFLHlFQUFPeE8sTUFBTSxDQUFDd08sT0FBZCxDQUZGLENBREYsZUFLRSwyREFBQyxvREFBRDtBQUFhLGFBQVMsRUFBQyxpQkFBdkI7QUFBeUMsVUFBTSxFQUFFOEM7QUFBakQsa0JBQ0UsMkRBQUMsa0RBQUQ7QUFDRSxRQUFJLEVBQUVwTixJQURSO0FBRUUsY0FBVSxFQUFFaUwsVUFGZDtBQUdFLFVBQU0sRUFBRW5QLE1BSFY7QUFJRSxlQUFXLEVBQUVvUCxXQUpmO0FBS0UsZ0JBQVksRUFBRWxKLElBQUksSUFBSTtBQUNwQnFMLGFBQU8sQ0FBQyxLQUFELENBQVA7QUFDQXpDLGFBQU8sQ0FBQzZDLElBQVIsQ0FBYXpMLElBQWI7QUFDRDtBQVJILElBREYsQ0FMRixlQWlCRTtBQUFLLGFBQVMsRUFBQztBQUFmLGtCQUNFLDJEQUFDLHNEQUFEO0FBQ0UsUUFBSSxFQUFFaEMsSUFEUjtBQUVFLGNBQVUsRUFBRWlMLFVBRmQ7QUFHRSxVQUFNLEVBQUVuUCxNQUhWO0FBSUUsZ0JBQVksRUFBRWtHLElBQUksSUFBSTRJLE9BQU8sQ0FBQzZDLElBQVIsQ0FBYXpMLElBQWIsQ0FKeEI7QUFLRSxvQkFBZ0IsRUFBRXNMLE9BQU8sQ0FBQzlGLGNBTDVCO0FBTUUsc0JBQWtCLEVBQUU4RixPQUFPLENBQUNyRixnQkFOOUI7QUFPRSxvQkFBZ0IsRUFBRXFGLE9BQU8sQ0FBQ25GLGNBUDVCO0FBUUUsWUFBUSxFQUFFbUYsT0FBTyxDQUFDN0U7QUFScEIsSUFERixDQWpCRixlQTZCRTtBQUNFLGtCQUFXLG1CQURiO0FBRUUsYUFBUyxFQUFDLDRDQUZaO0FBR0UsV0FBTyxFQUFFLE1BQU00RSxPQUFPLENBQUMsQ0FBQ0QsUUFBRjtBQUh4QixLQUlHLENBQUNBLFFBQUQsaUJBQWE7QUFBRyxhQUFTLEVBQUM7QUFBYixJQUpoQixFQUtHQSxRQUFRLGlCQUFJO0FBQUcsYUFBUyxFQUFDO0FBQWIsSUFMZixDQTdCRixDQURGO0FBdUNELEM7Ozs7Ozs7Ozs7OztBQzNHRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBRUEsTUFBTU0sb0JBQW9CLEdBQUd4RCx3REFBTSxDQUFDeUQsT0FBUTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztDQUE1QztBQW9DZSxTQUFTQyxhQUFULENBQXVCckUsS0FBdkIsRUFBOEI7QUFDM0MsUUFBTTtBQUFFek4sVUFBRjtBQUFVNE8sU0FBVjtBQUFpQkMsZUFBakI7QUFBOEJHLFdBQTlCO0FBQXVDQztBQUF2QyxNQUFvRHhCLEtBQTFEO0FBQ0EsUUFBTXNFLFFBQVEsR0FBRyxDQUFDdEUsS0FBSyxDQUFDc0UsUUFBTixJQUFrQixFQUFuQixFQUF1QjFMLE1BQXZCLENBQThCLFFBQTlCLENBQWpCO0FBRUEsc0JBQ0UsMkRBQUMsNENBQUQsQ0FBTyxRQUFQLHFCQUNFLDJEQUFDLG1EQUFELHFCQUNFO0FBQU0sUUFBSSxFQUFFckcsTUFBTSxDQUFDTSxXQUFQLENBQW1CQztBQUEvQixJQURGLGVBRUUsMEVBQVFrTixLQUFLLENBQUNtQixLQUFkLENBRkYsZUFJRTtBQUFNLFdBQU8sRUFBQztBQUFkLElBSkYsZUFLRTtBQUFNLFFBQUksRUFBQyxhQUFYO0FBQXlCLFdBQU8sRUFBRW5CLEtBQUssQ0FBQ29CO0FBQXhDLElBTEYsZUFNRTtBQUFNLFFBQUksRUFBQyxVQUFYO0FBQXNCLFdBQU8sRUFBRWtELFFBQVEsQ0FBQy9MLElBQVQsQ0FBYyxJQUFkO0FBQS9CLElBTkYsZUFPRTtBQUFNLFFBQUksRUFBQyxRQUFYO0FBQW9CLFdBQU8sRUFBQztBQUE1QixJQVBGLGVBU0U7QUFBTSxZQUFRLEVBQUMsVUFBZjtBQUEwQixXQUFPLEVBQUV5SCxLQUFLLENBQUNtQjtBQUF6QyxJQVRGLGVBVUU7QUFBTSxZQUFRLEVBQUMsU0FBZjtBQUF5QixXQUFPLEVBQUM7QUFBakMsSUFWRixlQVdFO0FBQU0sWUFBUSxFQUFDLGNBQWY7QUFBOEIsV0FBTyxFQUFFNU8sTUFBTSxDQUFDd087QUFBOUMsSUFYRixlQVlFO0FBQU0sWUFBUSxFQUFDLGdCQUFmO0FBQWdDLFdBQU8sRUFBRWYsS0FBSyxDQUFDbUI7QUFBL0MsSUFaRixlQWNFO0FBQU0sUUFBSSxFQUFDLGNBQVg7QUFBMEIsV0FBTyxFQUFFbkIsS0FBSyxDQUFDb0I7QUFBekMsSUFkRixlQWVFO0FBQU0sUUFBSSxFQUFDLGlCQUFYO0FBQTZCLFdBQU8sRUFBQztBQUFyQyxJQWZGLGVBZ0JFO0FBQU0sUUFBSSxFQUFDLGNBQVg7QUFBMEIsV0FBTyxFQUFDO0FBQWxDLElBaEJGLENBREYsZUFtQkUsMkRBQUMsb0JBQUQ7QUFBc0IsYUFBUyxFQUFDO0FBQWhDLEtBQ0dHLE9BQU8saUJBQUk7QUFBSyxhQUFTLEVBQUM7QUFBZixLQUEyQkEsT0FBM0IsQ0FEZCxlQUVFO0FBQUssYUFBUyxFQUFDLFlBQWY7QUFBNEIsUUFBSSxFQUFDO0FBQWpDLGtCQUNFO0FBQUksYUFBUyxFQUFDO0FBQWQsS0FBc0JKLEtBQXRCLENBREYsZUFFRTtBQUFHLGFBQVMsRUFBQztBQUFiLEtBQXFCQyxXQUFyQixDQUZGLENBRkYsRUFNR0ksUUFBUSxpQkFBSTtBQUFLLGFBQVMsRUFBQztBQUFmLEtBQTRCQSxRQUE1QixDQU5mLENBbkJGLENBREY7QUE4QkQsQzs7Ozs7Ozs7Ozs7O0FDMUVEO0FBQUE7QUFBQTtBQUFBO0NBQ0E7O0FBRWUsU0FBUytDLElBQVQsQ0FBY3ZFLEtBQWQsRUFBcUI7QUFDaEMsUUFBTTtBQUFFd0U7QUFBRixNQUFXeEUsS0FBakI7QUFDQXdFLE1BQUksQ0FBQ3RCLEdBQUwsQ0FBVXVCLEdBQUQsSUFBUztBQUNkQSxPQUFHLENBQUNDLGFBQUosR0FBb0IsSUFBSWpLLElBQUosQ0FBU2dLLEdBQUcsQ0FBQ0UsZUFBYixDQUFwQjtBQUNILEdBRkQ7QUFHQUgsTUFBSSxDQUFDSSxJQUFMLENBQVUsQ0FBQ0MsQ0FBRCxFQUFJQyxDQUFKLEtBQVVBLENBQUMsQ0FBQ0osYUFBRixHQUFrQkcsQ0FBQyxDQUFDSCxhQUF4QztBQUNBLHNCQUNJLDJEQUFDLDRDQUFELENBQU8sUUFBUCxRQUNDRixJQUFJLENBQUMsQ0FBRCxDQUFKLElBQVdBLElBQUksQ0FBQ3RCLEdBQUwsQ0FBUyxDQUFDNkIsT0FBRCxFQUFVQyxLQUFWLGtCQUNyQjtBQUFLLE9BQUcsRUFBRUEsS0FBVjtBQUFpQixhQUFTLEVBQUM7QUFBM0Isa0JBQ0k7QUFBSyxhQUFTLEVBQUM7QUFBZixrQkFDSTtBQUFJLGFBQVMsRUFBQztBQUFkLEtBQTRCRCxPQUFPLENBQUM1RCxLQUFwQyxDQURKLGVBRUk7QUFBRyxhQUFTLEVBQUM7QUFBYixLQUEwQjRELE9BQU8sQ0FBQzNELFdBQWxDLE1BRkosZUFHSTtBQUFHLFFBQUksRUFBRTJELE9BQU8sQ0FBQ0UsVUFBakI7QUFBNkIsYUFBUyxFQUFDO0FBQXZDLHNCQUhKLGVBSUk7QUFBRyxhQUFTLEVBQUM7QUFBYixrQkFBeUI7QUFBTyxhQUFTLEVBQUM7QUFBakIsS0FBK0JGLE9BQU8sQ0FBQ0osZUFBdkMsQ0FBekIsQ0FKSixDQURKLGVBT0k7QUFBSyxPQUFHLEVBQUVJLE9BQU8sQ0FBQ0csS0FBbEI7QUFBeUIsYUFBUyxFQUFDLGlCQUFuQztBQUFxRCxPQUFHLEVBQUVILE9BQU8sQ0FBQzVEO0FBQWxFLElBUEosQ0FEWSxDQURaLENBREo7QUFpQkgsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDMUJEO0FBQ0E7QUFFQSxNQUFNZ0UsY0FBYyxHQUFHeEUsd0RBQU0sQ0FBQ3lFLEdBQUk7Ozs7Ozs7Ozs7Ozs7Ozs7O0NBQWxDO0FBbUJlLE1BQU1DLE9BQU4sU0FBc0I1Riw0Q0FBSyxDQUFDRyxTQUE1QixDQUFzQztBQUFBO0FBQUE7O0FBQUEsd0NBRW5EMEYsS0FGbUQsR0FFM0M7QUFDTkMsaUJBQVcsRUFBRTtBQURQLEtBRjJDLE9BTW5EQyxpQkFObUQsR0FNL0IsTUFBTTtBQUN4QnZSLGNBQVEsQ0FBQ3dSLGdCQUFULENBQTBCLE9BQTFCLEVBQW1DLEtBQUtDLGVBQXhDO0FBQ0FyRyxZQUFNLENBQUNvRyxnQkFBUCxDQUF3QixRQUF4QixFQUFrQyxLQUFLRSxjQUF2QztBQUNBdEcsWUFBTSxDQUFDdUcsYUFBUCxDQUFxQixJQUFJQyxXQUFKLENBQWdCLFFBQWhCLENBQXJCO0FBQ0QsS0FWa0QsT0FZbkRDLG9CQVptRCxHQVk1QixNQUFNO0FBQzNCN1IsY0FBUSxDQUFDOFIsbUJBQVQsQ0FBNkIsT0FBN0IsRUFBc0MsS0FBS0wsZUFBM0M7QUFDQXJHLFlBQU0sQ0FBQzBHLG1CQUFQLENBQTJCLFFBQTNCLEVBQXFDLEtBQUtKLGNBQTFDO0FBQ0QsS0Fma0QsT0FpQm5ERCxlQWpCbUQsR0FpQmpDTSxLQUFLLElBQUk7QUFDekIsWUFBTTtBQUFFakcsY0FBRjtBQUFVa0c7QUFBVixVQUFxQixLQUFLakcsS0FBaEM7O0FBRUEsVUFBSUQsTUFBTSxJQUFJLENBQUNrRyxNQUFNLENBQUNDLFFBQVAsQ0FBZ0JGLEtBQUssQ0FBQ0MsTUFBdEIsQ0FBWCxJQUE0QyxDQUFDLEtBQUtFLE9BQUwsQ0FBYUQsUUFBYixDQUFzQkYsS0FBSyxDQUFDQyxNQUE1QixDQUFqRCxFQUFzRjtBQUNwRixhQUFLakcsS0FBTCxDQUFXb0csUUFBWDtBQUNEO0FBQ0YsS0F2QmtELE9BeUJuRFQsY0F6Qm1ELEdBeUJsQyxNQUFNO0FBQ3JCLFdBQUtVLFFBQUwsQ0FBYztBQUFFZCxtQkFBVyxFQUFFbEcsTUFBTSxDQUFDaUg7QUFBdEIsT0FBZDtBQUNELEtBM0JrRCxPQTZCbkRDLGtCQTdCbUQsR0E2QjlCQyxVQUFVLElBQUk7QUFDakMsWUFBTTtBQUFFQztBQUFGLFVBQVksS0FBS3pHLEtBQXZCO0FBQ0EsWUFBTTBHLFlBQVksR0FBSUQsS0FBSyxJQUFJQSxLQUFLLENBQUNqRCxLQUFoQixJQUEwQixHQUEvQztBQUNBLFlBQU1tRCxjQUFjLEdBQUdILFVBQVUsR0FBR0EsVUFBVSxDQUFDSSxxQkFBWCxFQUFILEdBQXdDLElBQXpFO0FBQ0EsWUFBTUMsV0FBVyxHQUFHTCxVQUFVLEdBQUdBLFVBQVUsQ0FBQ00sV0FBZCxHQUE0QixDQUExRDtBQUNBLFlBQU1DLFlBQVksR0FBR1AsVUFBVSxHQUFHQSxVQUFVLENBQUNRLFlBQWQsR0FBNkIsQ0FBNUQ7QUFDQSxZQUFNQyxlQUFlLEdBQUdOLGNBQWMsR0FBR0EsY0FBYyxDQUFDTyxDQUFsQixHQUFzQixDQUE1RDtBQUNBLFlBQU1DLGVBQWUsR0FBR1IsY0FBYyxHQUFHQSxjQUFjLENBQUNTLENBQWxCLEdBQXNCLENBQTVEO0FBQ0EsWUFBTTdCLFdBQVcsR0FBSSxLQUFLRCxLQUFMLENBQVdDLFdBQVgsR0FBeUIsRUFBOUM7QUFFQSxVQUFJOEIsZ0JBQWdCLEdBQUdKLGVBQWUsR0FBSUosV0FBVyxHQUFHLENBQWpDLEdBQXVDSCxZQUFZLEdBQUcsQ0FBN0U7QUFDQSxVQUFJWSxnQkFBZ0IsR0FBR0gsZUFBZSxJQUFJSixZQUFZLEdBQUcsRUFBbkIsQ0FBdEM7QUFFQSxhQUFPO0FBQ0xHLFNBQUMsRUFBR0csZ0JBQWdCLEdBQUdYLFlBQXBCLEdBQW9DbkIsV0FBcEMsR0FBbURBLFdBQVcsR0FBR21CLFlBQWQsR0FBNkIsQ0FBaEYsR0FBcUZXLGdCQURuRjtBQUVMRCxTQUFDLEVBQUVFO0FBRkUsT0FBUDtBQUlELEtBOUNrRDtBQUFBOztBQWdEbkRoSCxRQUFNLEdBQUc7QUFDUCxVQUFNO0FBQUVQLFlBQUY7QUFBVVEsZUFBVjtBQUFxQkMsY0FBckI7QUFBK0J5RixZQUEvQjtBQUF1Q1E7QUFBdkMsUUFBaUQsS0FBS3pHLEtBQTVEO0FBQ0EsVUFBTTJHLGNBQWMsR0FBRyxLQUFLSixrQkFBTCxDQUF3Qk4sTUFBeEIsQ0FBdkI7QUFFQSx3QkFDRSwyREFBQyxjQUFEO0FBQ0UsU0FBRyxFQUFFeEYsSUFBSSxJQUFJLEtBQUswRixPQUFMLEdBQWUxRixJQUQ5QjtBQUVFLHVCQUFlVixNQUZqQjtBQUdFLGVBQVMsRUFBRyxHQUFFUSxTQUFVLElBQUdSLE1BQU0sSUFBSSxNQUFPLEVBSDlDO0FBSUUsV0FBSyxvQkFBTzBHLEtBQVA7QUFBY2MsaUJBQVMsRUFBRyxlQUFjWixjQUFjLENBQUNPLENBQUUsT0FBTVAsY0FBYyxDQUFDUyxDQUFFO0FBQWhGO0FBSlAsT0FLRzVHLFFBTEgsQ0FERjtBQVNEOztBQTdEa0QsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN0QnJEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxNQUFNZ0gsV0FBVyxHQUFHN0csd0RBQU0sQ0FBQzhHLElBQUs7Ozs7Q0FBaEM7QUFNQTs7QUFDQSxNQUFNQyxJQUFOLFNBQW1CakksNENBQUssQ0FBQ0csU0FBekIsQ0FBbUM7QUFBQTtBQUFBOztBQUFBLHdDQUNqQzBGLEtBRGlDLEdBQ3pCO0FBQ052UixXQUFLLEVBQUUsSUFERDtBQUVONFQsZUFBUyxFQUFFLEtBRkw7QUFHTm5ELFVBQUksRUFBRTtBQUhBLEtBRHlCLE9BVWpDb0QsU0FWaUMsR0FVckIsWUFBWTtBQUN0QixXQUFLdkIsUUFBTCxDQUFjO0FBQUNzQixpQkFBUyxFQUFFLElBQVo7QUFBa0I1VCxhQUFLLEVBQUU7QUFBekIsT0FBZCxFQURzQixDQUV0QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQSxXQUFLc1MsUUFBTCxDQUFjO0FBQ1pzQixpQkFBUyxFQUFFLEtBREM7QUFFWm5ELFlBQUksRUFBRWpRLHNDQUFJLENBQUNpUTtBQUZDLE9BQWQ7QUFJRCxLQTdCZ0M7QUFBQTs7QUFNakMsUUFBTWdCLGlCQUFOLEdBQTBCO0FBQ3hCLFVBQU0sS0FBS29DLFNBQUwsRUFBTjtBQUNEOztBQXVCRHRILFFBQU0sR0FBRztBQUNQLFVBQU07QUFBRS9OLFlBQUY7QUFBVUY7QUFBVixRQUFtQixLQUFLMk4sS0FBOUI7QUFDQSx3QkFDRSwyREFBQyw0Q0FBRCxDQUFPLFFBQVAscUJBQ0UsMkRBQUMsMERBQUQ7QUFDRSxZQUFNLEVBQUV6TixNQURWO0FBRUUsVUFBSSxFQUFFRixJQUZSO0FBR0UsV0FBSyxFQUFFb0IsbUVBQVMsQ0FBQ2xCLE1BQU0sQ0FBQ2EsWUFBUixFQUFzQixXQUF0QixDQUhsQjtBQUlFLGlCQUFXLEVBQUVLLG1FQUFTLENBQUNsQixNQUFNLENBQUNhLFlBQVIsRUFBc0IsaUJBQXRCLENBSnhCO0FBS0UsYUFBTyxFQUFFLEtBQUs0TSxLQUFMLENBQVdxQixPQUx0QjtBQU1FLGlCQUFXLEVBQUU7QUFOZixNQURGLGVBUUUsMkRBQUMsV0FBRDtBQUFhLGVBQVMsRUFBQztBQUF2QixPQUNHLEtBQUtpRSxLQUFMLENBQVdxQyxTQUFYLGlCQUNEO0FBQUssZUFBUyxFQUFDO0FBQWYsb0JBQ0U7QUFBSyxlQUFTLEVBQUMsMEJBQWY7QUFBMEMsVUFBSSxFQUFDO0FBQS9DLG9CQUNFO0FBQU0sZUFBUyxFQUFDO0FBQWhCLG9CQURGLENBREYsQ0FGRixlQVFFLDJEQUFDLHdEQUFEO0FBQU0sVUFBSSxFQUFFLEtBQUtyQyxLQUFMLENBQVdkO0FBQXZCLE1BUkYsRUFTRyxLQUFLYyxLQUFMLENBQVd2UixLQUFYLGlCQUNEO0FBQUssZUFBUyxFQUFDO0FBQWYsb0JBQ0U7QUFBRyxlQUFTLEVBQUM7QUFBYixpQ0FERixDQVZGLENBUkYsZUF1QkUsMkRBQUMsMERBQUQ7QUFDRSxZQUFNLEVBQUV4QixNQURWO0FBRUUsbUJBQWEsRUFBRSxLQUFLeU4sS0FBTCxDQUFXcUI7QUFGNUIsTUF2QkYsQ0FERjtBQTZCRDs7QUE5RGdDOztBQWdFbkMsTUFBTXdHLGVBQWUsR0FBR3ZDLEtBQUssS0FBSztBQUNoQy9TLFFBQU0sRUFBRStTLEtBQUssQ0FBQy9TLE1BRGtCO0FBRWhDdVYsU0FBTyxFQUFFeEMsS0FBSyxDQUFDd0MsT0FGaUI7QUFHaENDLE1BQUksRUFBRXpDLEtBQUssQ0FBQ3lDLElBSG9CO0FBSWhDMVYsTUFBSSxFQUFFaVQsS0FBSyxDQUFDalQ7QUFKb0IsQ0FBTCxDQUE3Qjs7QUFNZTJWLDBIQUFPLENBQUNILGVBQUQsQ0FBUCxDQUF5QkgsSUFBekIsQ0FBZixFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDcEZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQSxNQUFNTyxZQUFZLEdBQUd0SCx3REFBTSxDQUFDOEcsSUFBSzs7OztDQUFqQzs7QUFNQSxNQUFNUyxLQUFOLFNBQW9CekksNENBQUssQ0FBQ0csU0FBMUIsQ0FBb0M7QUFBQTtBQUFBOztBQUFBLHdDQUNsQzBGLEtBRGtDLEdBQzFCO0FBQ05xQyxlQUFTLEVBQUUsS0FETDtBQUVOOU4sWUFBTSxFQUFFLEVBRkY7QUFHTk4sV0FBSyxFQUFFO0FBQ0xoRyxhQUFLLEVBQUUsRUFERjtBQUVMa0ssZ0JBQVEsRUFBRTtBQUZMO0FBSEQsS0FEMEIsT0FVbENvQyxrQkFWa0MsR0FVYixZQUFZO0FBQy9CLFVBQUksS0FBS0csS0FBTCxDQUFXdkosSUFBZixFQUFxQjtBQUNuQixhQUFLdUosS0FBTCxDQUFXcUIsT0FBWCxDQUFtQjZDLElBQW5CLENBQXdCLGdCQUF4QjtBQUNEO0FBQ0YsS0FkaUMsT0FnQmxDaUUsS0FoQmtDLEdBZ0IxQixNQUFNdFUsQ0FBTixJQUFXO0FBQ2pCLFVBQUk7QUFDRixZQUFJQSxDQUFKLEVBQU87QUFDTEEsV0FBQyxDQUFDZ08sY0FBRjtBQUNEOztBQUVELGFBQUt3RSxRQUFMLENBQWM7QUFBRXNCLG1CQUFTLEVBQUUsSUFBYjtBQUFtQjlOLGdCQUFNLEVBQUU7QUFBM0IsU0FBZDtBQUNBLGNBQU1ySCxvREFBSyxDQUFDZ0MsUUFBTixDQUFlMlQseUVBQUssQ0FBQyxLQUFLN0MsS0FBTCxDQUFXL0wsS0FBWixDQUFwQixDQUFOO0FBQ0EsY0FBTS9HLG9EQUFLLENBQUNnQyxRQUFOLENBQWU0VCw0RUFBUSxDQUFDLEtBQUtwSSxLQUFMLENBQVczTixJQUFYLENBQWdCOEQsS0FBakIsQ0FBdkIsQ0FBTjtBQUNBLGNBQU0zRCxvREFBSyxDQUFDZ0MsUUFBTixDQUFlNlQsc0VBQUUsRUFBakIsQ0FBTjtBQUNBLGFBQUtoQyxRQUFMLENBQWM7QUFBRXNCLG1CQUFTLEVBQUU7QUFBYixTQUFkO0FBQ0QsT0FWRCxDQVVFLE9BQU85VCxDQUFQLEVBQVU7QUFDVixjQUFNO0FBQUVnRyxnQkFBRjtBQUFVbkc7QUFBVixZQUFzQjRVLCtFQUFnQixDQUFDelUsQ0FBRCxDQUE1QztBQUNBLGFBQUt3UyxRQUFMLENBQWM7QUFBRXNCLG1CQUFTLEVBQUUsS0FBYjtBQUFvQjlOLGdCQUFNLG9CQUFNQSxNQUFOO0FBQWMwTyxtQkFBTyxFQUFFN1U7QUFBdkI7QUFBMUIsU0FBZDtBQUNEO0FBQ0YsS0EvQmlDO0FBQUE7O0FBaUNsQzRNLFFBQU0sR0FBRztBQUNQLFVBQU07QUFBRXFIO0FBQUYsUUFBZ0IsS0FBS3JDLEtBQTNCO0FBQ0EsVUFBTTtBQUFFL1MsWUFBRjtBQUFVRjtBQUFWLFFBQW1CLEtBQUsyTixLQUE5QjtBQUNBLFVBQU13SSxXQUFXLEdBQUdDLCtFQUFrQixDQUFDLEtBQUt6SSxLQUFMLENBQVdWLFFBQVgsQ0FBb0JvSixNQUFyQixDQUF0QztBQUVBLHdCQUNFLDJEQUFDLDRDQUFELENBQU8sUUFBUCxRQUVJRixXQUFXLENBQUM5VSxPQUFaLGlCQUNBO0FBQUssZUFBUyxFQUFDO0FBQWYsT0FDRzhVLFdBQVcsQ0FBQzlVLE9BRGYsZUFFRTtBQUFRLGVBQVMsRUFBQyxPQUFsQjtBQUEwQixvQkFBVyxPQUFyQztBQUE2QyxhQUFPLEVBQUUsTUFBTSxLQUFLc00sS0FBTCxDQUFXcUIsT0FBWCxDQUFtQjZDLElBQW5CLENBQXdCLEtBQUtsRSxLQUFMLENBQVdWLFFBQVgsQ0FBb0J5QyxRQUE1QztBQUE1RCxvQkFDRTtBQUFNLHFCQUFZO0FBQWxCLGNBREYsQ0FGRixDQUhKLGVBVUUsMkRBQUMsMERBQUQ7QUFDRSxZQUFNLEVBQUV4UCxNQURWO0FBRUUsVUFBSSxFQUFFRixJQUZSO0FBR0UsV0FBSyxFQUFFb0IsbUVBQVMsQ0FBQ2xCLE1BQU0sQ0FBQ2EsWUFBUixFQUFzQixZQUF0QixDQUhsQjtBQUlFLGlCQUFXLEVBQUVLLG1FQUFTLENBQUNsQixNQUFNLENBQUNhLFlBQVIsRUFBc0Isa0JBQXRCLENBSnhCO0FBS0UsYUFBTyxFQUFFLEtBQUs0TSxLQUFMLENBQVdxQixPQUx0QjtBQU1FLGlCQUFXLEVBQUU7QUFOZixNQVZGLGVBaUJFLDJEQUFDLFlBQUQ7QUFBYyxlQUFTLEVBQUM7QUFBeEIsb0JBQ0U7QUFBSyxlQUFTLEVBQUM7QUFBZixvQkFDRTtBQUFLLGVBQVMsRUFBQztBQUFmLG9CQUNFO0FBQUssZUFBUyxFQUFDLGtCQUFmO0FBQWtDLFVBQUksRUFBQztBQUF2QyxvQkFDRTtBQUFHLGVBQVMsRUFBQztBQUFiLG9CQUNFLDJEQUFDLHFEQUFEO0FBQU0sUUFBRSxFQUFDO0FBQVQsT0FBc0I1TixtRUFBUyxDQUFDbEIsTUFBTSxDQUFDYSxZQUFSLEVBQXNCLGdCQUF0QixDQUEvQixDQURGLENBREYsZUFJRTtBQUFHLGVBQVMsRUFBQztBQUFiLG9CQUNFLDJEQUFDLHFEQUFEO0FBQU0sUUFBRSxFQUFDO0FBQVQsT0FBOEJLLG1FQUFTLENBQUNsQixNQUFNLENBQUNhLFlBQVIsRUFBc0IsZ0JBQXRCLENBQXZDLENBREYsQ0FKRixDQURGLGVBU0U7QUFBSyxlQUFTLEVBQUMscUJBQWY7QUFBcUMsVUFBSSxFQUFDO0FBQTFDLE9BQ0dLLG1FQUFTLENBQUNsQixNQUFNLENBQUNhLFlBQVIsRUFBc0IsZ0JBQXRCLENBRFosQ0FURixFQWFJLEtBQUtrUyxLQUFMLENBQVd6TCxNQUFYLENBQWtCME8sT0FBbEIsaUJBQ0E7QUFBSyxlQUFTLEVBQUMsb0JBQWY7QUFBb0MsVUFBSSxFQUFDO0FBQXpDLE9BQ0csS0FBS2pELEtBQUwsQ0FBV3pMLE1BQVgsQ0FBa0IwTyxPQURyQixDQWRKLGVBa0JFO0FBQU0sY0FBUSxFQUFFLEtBQUtKO0FBQXJCLG9CQUNFO0FBQUssZUFBUyxFQUFDO0FBQWYsb0JBQ0UsMEVBQ0cxVSxtRUFBUyxDQUFDbEIsTUFBTSxDQUFDYSxZQUFSLEVBQXNCLE9BQXRCLENBRFosb0JBQzRDLDZFQUQ1QyxDQURGLGVBSUU7QUFDRSxVQUFJLEVBQUMsTUFEUDtBQUVFLGVBQVMsRUFBQyxjQUZaO0FBR0UsV0FBSyxFQUFFLEtBQUtrUyxLQUFMLENBQVcvTCxLQUFYLENBQWlCaEcsS0FIMUI7QUFJRSxjQUFRLEVBQUVNLENBQUMsSUFDVCxLQUFLd1MsUUFBTCxDQUFjO0FBQUU5TSxhQUFLLEVBQUUzQyxNQUFNLENBQUNzRSxNQUFQLENBQWMsS0FBS29LLEtBQUwsQ0FBVy9MLEtBQXpCLEVBQWdDO0FBQUVoRyxlQUFLLEVBQUUsQ0FBQ00sQ0FBQyxDQUFDb1MsTUFBRixDQUFTN0MsS0FBVCxJQUFrQixFQUFuQixFQUMzRHVGLE9BRDJELENBQ25ELEtBRG1ELEVBQzVDLEVBRDRDO0FBQVQsU0FBaEM7QUFBVCxPQUFkO0FBTEosTUFKRixlQWNFO0FBQU0sZUFBUyxFQUFDO0FBQWhCLE9BQStCLEtBQUtyRCxLQUFMLENBQVd6TCxNQUFYLENBQWtCdEcsS0FBakQsQ0FkRixDQURGLGVBaUJFO0FBQUssZUFBUyxFQUFDO0FBQWYsb0JBQ0UsMEVBQ0dFLG1FQUFTLENBQUNsQixNQUFNLENBQUNhLFlBQVIsRUFBc0IsVUFBdEIsQ0FEWixvQkFDK0MsNkVBRC9DLENBREYsZUFJRTtBQUNFLFVBQUksRUFBQyxVQURQO0FBRUUsZUFBUyxFQUFDLGNBRlo7QUFHRSxXQUFLLEVBQUUsS0FBS2tTLEtBQUwsQ0FBVy9MLEtBQVgsQ0FBaUJrRSxRQUgxQjtBQUlFLGNBQVEsRUFBRTVKLENBQUMsSUFBSSxLQUFLd1MsUUFBTCxDQUFjO0FBQUU5TSxhQUFLLEVBQUUzQyxNQUFNLENBQUNzRSxNQUFQLENBQWMsS0FBS29LLEtBQUwsQ0FBVy9MLEtBQXpCLEVBQWdDO0FBQUVrRSxrQkFBUSxFQUFFNUosQ0FBQyxDQUFDb1MsTUFBRixDQUFTN0M7QUFBckIsU0FBaEM7QUFBVCxPQUFkO0FBSmpCLE1BSkYsZUFTRTtBQUFNLGVBQVMsRUFBQztBQUFoQixPQUNHLEtBQUtrQyxLQUFMLENBQVd6TCxNQUFYLENBQWtCNEQsUUFEckIsQ0FURixDQWpCRixlQThCRTtBQUFLLGVBQVMsRUFBQztBQUFmLG9CQUNFO0FBQ0UsVUFBSSxFQUFDLFFBRFA7QUFFRSxlQUFTLEVBQUMsaUJBRlo7QUFHRSxjQUFRLEVBQUVrSztBQUhaLE9BSUdsVSxtRUFBUyxDQUFDbEIsTUFBTSxDQUFDYSxZQUFSLEVBQXNCLE9BQXRCLENBSlosQ0FERixDQTlCRixDQWxCRixDQURGLENBREYsQ0FqQkYsZUErRUUsMkRBQUMsMERBQUQ7QUFDRSxZQUFNLEVBQUViLE1BRFY7QUFFRSxtQkFBYSxFQUFFLEtBQUt5TixLQUFMLENBQVdxQjtBQUY1QixNQS9FRixDQURGO0FBcUZEOztBQTNIaUM7O0FBOEhwQyxNQUFNd0csZUFBZSxHQUFHdkMsS0FBSyxLQUFLO0FBQ2hDN08sTUFBSSxFQUFFNk8sS0FBSyxDQUFDalQsSUFBTixDQUFXb0UsSUFEZTtBQUVoQ2xFLFFBQU0sRUFBRStTLEtBQUssQ0FBQy9TLE1BRmtCO0FBR2hDRixNQUFJLEVBQUVpVCxLQUFLLENBQUNqVDtBQUhvQixDQUFMLENBQTdCOztBQU1lMlYsMEhBQU8sQ0FBQ0gsZUFBRCxDQUFQLENBQXlCSyxLQUF6QixDQUFmLEU7Ozs7Ozs7Ozs7OztBQ3RKQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxNQUFNVSxJQUFOLFNBQW1CbkosNENBQUssQ0FBQ0csU0FBekIsQ0FBbUM7QUFBQTtBQUFBOztBQUFBLHdDQUdqQzRGLGlCQUhpQyxHQUdiLE1BQU07QUFDeEJoVCwwREFBSyxDQUFDZ0MsUUFBTixDQUFlNFQsNEVBQVEsQ0FBQzdKLFlBQVksQ0FBQ3BJLEtBQWQsQ0FBdkI7QUFDQTNELDBEQUFLLENBQUNnQyxRQUFOLENBQWU2VCxzRUFBRSxFQUFqQixFQUNHUSxPQURILENBQ1csTUFBTSxLQUFLOUUsT0FBTCxDQUFhbEYsZUFBYixFQURqQjtBQUVELEtBUGdDO0FBQUE7O0FBU2pDeUIsUUFBTSxHQUFHO0FBQ1Asd0JBQ0UsMkRBQUMsNENBQUQsQ0FBTyxRQUFQLFFBQ0csS0FBS04sS0FBTCxDQUFXUSxRQURkLENBREY7QUFLRDs7QUFmZ0M7O0FBQTdCb0ksSSxDQUNHRSxXLEdBQWM3RSxnRDtBQWlCUjhFLGtJQUFVLENBQUNILElBQUQsQ0FBekIsRTs7Ozs7Ozs7Ozs7O0FDeEJBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUEsTUFBTUksZUFBZSxHQUFHckksd0RBQU0sQ0FBQzhHLElBQUs7Ozs7Q0FBcEM7O0FBS0EsTUFBTXdCLFFBQU4sU0FBdUJ4Siw0Q0FBSyxDQUFDRyxTQUE3QixDQUF1QztBQUNyQ1UsUUFBTSxHQUFHO0FBQ1AsVUFBTTtBQUFFL04sWUFBRjtBQUFVRjtBQUFWLFFBQW1CLEtBQUsyTixLQUE5QjtBQUVBLHdCQUNFLDJEQUFDLDRDQUFELENBQU8sUUFBUCxxQkFDRSwyREFBQywwREFBRDtBQUNFLFlBQU0sRUFBRXpOLE1BRFY7QUFFRSxVQUFJLEVBQUVGLElBRlI7QUFHRSxXQUFLLEVBQUVvQixtRUFBUyxDQUFDbEIsTUFBTSxDQUFDYSxZQUFSLEVBQXNCLGVBQXRCLENBSGxCO0FBSUUsaUJBQVcsRUFBRUssbUVBQVMsQ0FBQ2xCLE1BQU0sQ0FBQ2EsWUFBUixFQUFzQixxQkFBdEIsQ0FKeEI7QUFLRSxhQUFPLEVBQUUsS0FBSzRNLEtBQUwsQ0FBV3FCLE9BTHRCO0FBTUUsaUJBQVcsRUFBRTtBQU5mLE1BREYsZUFRRSwyREFBQyxlQUFEO0FBQWlCLGVBQVMsRUFBQztBQUEzQixvQkFDRSxvRkFBSTtBQUFHLGVBQVMsRUFBQztBQUFiLE1BQUosQ0FERixlQUVFLHNFQUFJNU4sbUVBQVMsQ0FBQ2xCLE1BQU0sQ0FBQ2EsWUFBUixFQUFzQixxQkFBdEIsQ0FBYixDQUZGLGVBR0UsbUZBQUcsMkRBQUMscURBQUQ7QUFBTSxRQUFFLEVBQUM7QUFBVCxPQUFjSyxtRUFBUyxDQUFDbEIsTUFBTSxDQUFDYSxZQUFSLEVBQXNCLFlBQXRCLENBQXZCLENBQUgsQ0FIRixDQVJGLGVBYUUsMkRBQUMsMERBQUQ7QUFDRSxZQUFNLEVBQUViLE1BRFY7QUFFRSxtQkFBYSxFQUFFLEtBQUt5TixLQUFMLENBQVdxQjtBQUY1QixNQWJGLENBREY7QUFtQkQ7O0FBdkJvQzs7QUEwQnZDLE1BQU13RyxlQUFlLEdBQUd2QyxLQUFLLEtBQUs7QUFDaEMvUyxRQUFNLEVBQUUrUyxLQUFLLENBQUMvUyxNQURrQjtBQUVoQ0YsTUFBSSxFQUFFaVQsS0FBSyxDQUFDalQ7QUFGb0IsQ0FBTCxDQUE3Qjs7QUFLZTJWLDBIQUFPLENBQUNILGVBQUQsQ0FBUCxDQUF5Qm9CLFFBQXpCLENBQWYsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzVDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQSxNQUFNQyxzQkFBc0IsR0FBR3ZJLHdEQUFNLENBQUM4RyxJQUFLOzs7O0NBQTNDOztBQU1BLE1BQU0wQixlQUFOLFNBQThCMUosNENBQUssQ0FBQ0csU0FBcEMsQ0FBOEM7QUFBQTtBQUFBOztBQUFBLHdDQUM1QzBGLEtBRDRDLEdBQ3BDO0FBQ056TCxZQUFNLEVBQUUsRUFERjtBQUVOTixXQUFLLEVBQUU7QUFDTGhHLGFBQUssRUFBRTtBQURGO0FBRkQsS0FEb0MsT0FRNUM2VixjQVI0QyxHQVEzQixNQUFNdlYsQ0FBTixJQUFXO0FBQzFCLFVBQUk7QUFDRkEsU0FBQyxDQUFDZ08sY0FBRjtBQUVBLGFBQUt3RSxRQUFMLENBQWM7QUFBRXNCLG1CQUFTLEVBQUUsSUFBYjtBQUFtQjlOLGdCQUFNLEVBQUU7QUFBM0IsU0FBZDtBQUNBLGNBQU0sS0FBS3dNLFFBQUwsQ0FBYztBQUNsQjlNLGVBQUssb0JBQ0EsS0FBSytMLEtBQUwsQ0FBVy9MLEtBRFg7QUFFSHBELGlCQUFLLEVBQUUsS0FBSzZKLEtBQUwsQ0FBV3FKLEtBQVgsQ0FBaUJoUCxNQUFqQixDQUF3QmxFO0FBRjVCO0FBRGEsU0FBZCxDQUFOO0FBTUEsY0FBTTNELG9EQUFLLENBQUNnQyxRQUFOLENBQWU4VSxtRkFBZSxDQUFDLEtBQUtoRSxLQUFMLENBQVcvTCxLQUFaLENBQTlCLENBQU47QUFDQSxjQUFNZ1EsY0FBYyxHQUFHLEtBQUt2SixLQUFMLENBQVczTixJQUFYLENBQWdCaU0sSUFBaEIsQ0FBcUI1SyxPQUE1QztBQUNBLGFBQUsyUyxRQUFMLENBQWM7QUFBRXNCLG1CQUFTLEVBQUU7QUFBYixTQUFkO0FBQ0EsYUFBSzNILEtBQUwsQ0FBV3FCLE9BQVgsQ0FBbUI2QyxJQUFuQixDQUF5QixrQkFBaUJxRixjQUFlLEVBQXpEO0FBQ0QsT0FkRCxDQWNFLE9BQU8xVixDQUFQLEVBQVU7QUFDVixjQUFNO0FBQUVnRyxnQkFBRjtBQUFVbkc7QUFBVixZQUFzQjRVLDhFQUFnQixDQUFDelUsQ0FBRCxDQUE1QztBQUNBLGFBQUt3UyxRQUFMLENBQWM7QUFBRXNCLG1CQUFTLEVBQUUsS0FBYjtBQUFvQjlOLGdCQUFNLG9CQUFPQSxNQUFQO0FBQWUwTyxtQkFBTyxFQUFFN1U7QUFBeEI7QUFBMUIsU0FBZDtBQUNEO0FBQ0YsS0EzQjJDO0FBQUE7O0FBNkI1QzRNLFFBQU0sR0FBRztBQUNQLFVBQU07QUFBRXFIO0FBQUYsUUFBZ0IsS0FBS3JDLEtBQTNCO0FBQ0EsVUFBTTtBQUFFL1MsWUFBRjtBQUFVRjtBQUFWLFFBQW1CLEtBQUsyTixLQUE5QjtBQUVBLHdCQUNFLDJEQUFDLDRDQUFELENBQU8sUUFBUCxxQkFDRSwyREFBQywwREFBRDtBQUNFLFlBQU0sRUFBRXpOLE1BRFY7QUFFRSxVQUFJLEVBQUVGLElBRlI7QUFHRSxXQUFLLEVBQUVvQixtRUFBUyxDQUFDbEIsTUFBTSxDQUFDYSxZQUFSLEVBQXNCLHNCQUF0QixDQUhsQjtBQUlFLGlCQUFXLEVBQUVLLG1FQUFTLENBQUNsQixNQUFNLENBQUNhLFlBQVIsRUFBc0IsNEJBQXRCLENBSnhCO0FBS0UsYUFBTyxFQUFFLEtBQUs0TSxLQUFMLENBQVdxQixPQUx0QjtBQU1FLGlCQUFXLEVBQUU7QUFOZixNQURGLGVBUUUsMkRBQUMsc0JBQUQ7QUFBd0IsZUFBUyxFQUFDO0FBQWxDLG9CQUNFO0FBQUssZUFBUyxFQUFDO0FBQWYsb0JBQ0U7QUFBSyxlQUFTLEVBQUM7QUFBZixvQkFDRTtBQUFLLGVBQVMsRUFBQyxxQkFBZjtBQUFxQyxVQUFJLEVBQUM7QUFBMUMsT0FDRzVOLG1FQUFTLENBQUNsQixNQUFNLENBQUNhLFlBQVIsRUFBc0IsZ0JBQXRCLENBRFosQ0FERixFQUtJLEtBQUtrUyxLQUFMLENBQVd6TCxNQUFYLENBQWtCME8sT0FBbEIsaUJBQ0E7QUFBSyxlQUFTLEVBQUMsb0JBQWY7QUFBb0MsVUFBSSxFQUFDO0FBQXpDLE9BQ0csS0FBS2pELEtBQUwsQ0FBV3pMLE1BQVgsQ0FBa0IwTyxPQURyQixDQU5KLGVBVUU7QUFBTSxjQUFRLEVBQUUsS0FBS2E7QUFBckIsb0JBQ0U7QUFBSyxlQUFTLEVBQUM7QUFBZixvQkFDRSwwRUFBUTNWLG1FQUFTLENBQUNsQixNQUFNLENBQUNhLFlBQVIsRUFBc0IsT0FBdEIsQ0FBakIsT0FERixlQUVFO0FBQ0UsVUFBSSxFQUFDLE1BRFA7QUFFRSxlQUFTLEVBQUMsY0FGWjtBQUdFLGNBQVEsRUFBRVMsQ0FBQyxJQUNULEtBQUt3UyxRQUFMLENBQWM7QUFBRTlNLGFBQUssRUFBRTNDLE1BQU0sQ0FBQ3NFLE1BQVAsQ0FBYyxLQUFLb0ssS0FBTCxDQUFXL0wsS0FBekIsRUFBZ0M7QUFBRWhHLGVBQUssRUFBRU0sQ0FBQyxDQUFDb1MsTUFBRixDQUFTN0M7QUFBbEIsU0FBaEM7QUFBVCxPQUFkO0FBSkosTUFGRixlQVNFO0FBQU0sZUFBUyxFQUFDO0FBQWhCLE9BQStCLEtBQUtrQyxLQUFMLENBQVd6TCxNQUFYLENBQWtCdEcsS0FBakQsQ0FURixDQURGLGVBWUU7QUFBSyxlQUFTLEVBQUM7QUFBZixvQkFDRTtBQUFRLFVBQUksRUFBQyxRQUFiO0FBQXNCLGVBQVMsRUFBQyxpQkFBaEM7QUFBa0QsY0FBUSxFQUFFb1U7QUFBNUQsT0FDR2xVLG1FQUFTLENBQUNsQixNQUFNLENBQUNhLFlBQVIsRUFBc0IsaUJBQXRCLENBRFosQ0FERixDQVpGLENBVkYsQ0FERixDQURGLENBUkYsZUF5Q0UsMkRBQUMsMERBQUQ7QUFDRSxZQUFNLEVBQUViLE1BRFY7QUFFRSxtQkFBYSxFQUFFLEtBQUt5TixLQUFMLENBQVdxQjtBQUY1QixNQXpDRixDQURGO0FBK0NEOztBQWhGMkM7O0FBbUY5QyxNQUFNd0csZUFBZSxHQUFHdkMsS0FBSyxLQUFLO0FBQ2hDL1MsUUFBTSxFQUFFK1MsS0FBSyxDQUFDL1MsTUFEa0I7QUFFaENGLE1BQUksRUFBRWlULEtBQUssQ0FBQ2pUO0FBRm9CLENBQUwsQ0FBN0I7O0FBS2UyViwwSEFBTyxDQUFDSCxlQUFELENBQVAsQ0FBeUJzQixlQUF6QixDQUFmLEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3hHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBLE1BQU1LLGVBQWUsR0FBRzdJLHdEQUFNLENBQUM4RyxJQUFLOzs7O0NBQXBDOztBQU1BLE1BQU1nQyxRQUFOLFNBQXVCaEssNENBQUssQ0FBQ0csU0FBN0IsQ0FBdUM7QUFBQTtBQUFBOztBQUFBLHdDQUNyQzBGLEtBRHFDLEdBQzdCO0FBQ05xQyxlQUFTLEVBQUUsS0FETDtBQUVOOU4sWUFBTSxFQUFFLEVBRkY7QUFHTk4sV0FBSyxFQUFFO0FBQ0xtUSxjQUFNLEVBQUUsSUFESDtBQUVMblcsYUFBSyxFQUFFLEVBRkY7QUFHTGtLLGdCQUFRLEVBQUUsRUFITDtBQUlMa00sNEJBQW9CLEVBQUUsRUFKakI7QUFLTDVCLFlBQUksRUFBRSxLQUFLL0gsS0FBTCxDQUFXek4sTUFBWCxDQUFrQnFYLEtBQWxCLENBQXdCQztBQUx6QjtBQUhELEtBRDZCLE9BYXJDckUsaUJBYnFDLEdBYWpCLE1BQU07QUFDeEIsVUFBSSxLQUFLeEYsS0FBTCxDQUFXM04sSUFBWCxDQUFnQm9FLElBQXBCLEVBQTBCO0FBQ3hCLGFBQUt1SixLQUFMLENBQVdxQixPQUFYLENBQW1CNkMsSUFBbkIsQ0FBd0IsZ0JBQXhCO0FBQ0Q7QUFDRixLQWpCb0MsT0FtQnJDNEYsaUJBbkJxQyxHQW1CakIsTUFBTWpXLENBQU4sSUFBVztBQUM3QixVQUFJO0FBQ0YsWUFBSUEsQ0FBSixFQUFPO0FBQ0xBLFdBQUMsQ0FBQ2dPLGNBQUY7QUFDRDs7QUFFRCxhQUFLd0UsUUFBTCxDQUFjO0FBQUVzQixtQkFBUyxFQUFFLElBQWI7QUFBbUI5TixnQkFBTSxFQUFFO0FBQTNCLFNBQWQ7QUFDQSxjQUFNckgsb0RBQUssQ0FBQ2dDLFFBQU4sQ0FBZXVWLDRFQUFRLENBQUMsS0FBS3pFLEtBQUwsQ0FBVy9MLEtBQVosQ0FBdkIsQ0FBTjtBQUNBLGNBQU0vRyxvREFBSyxDQUFDZ0MsUUFBTixDQUFlNFQsNEVBQVEsQ0FBQyxLQUFLcEksS0FBTCxDQUFXM04sSUFBWCxDQUFnQjhELEtBQWpCLENBQXZCLENBQU47QUFDQSxjQUFNM0Qsb0RBQUssQ0FBQ2dDLFFBQU4sQ0FBZTZULHNFQUFFLEVBQWpCLENBQU47QUFDQSxhQUFLaEMsUUFBTCxDQUFjO0FBQUVzQixtQkFBUyxFQUFFO0FBQWIsU0FBZDtBQUNBLGFBQUszSCxLQUFMLENBQVdxQixPQUFYLENBQW1CNkMsSUFBbkIsQ0FBd0IsZ0JBQXhCO0FBQ0QsT0FYRCxDQVdFLE9BQU9yUSxDQUFQLEVBQVU7QUFDVixjQUFNO0FBQUVnRyxnQkFBRjtBQUFVbkc7QUFBVixZQUFzQjRVLDhFQUFnQixDQUFDelUsQ0FBRCxDQUE1QztBQUNBLGFBQUt3UyxRQUFMLENBQWM7QUFBRXNCLG1CQUFTLEVBQUUsS0FBYjtBQUFvQjlOLGdCQUFNLG9CQUFNQSxNQUFOO0FBQWMwTyxtQkFBTyxFQUFFN1U7QUFBdkI7QUFBMUIsU0FBZDtBQUNEO0FBQ0YsS0FuQ29DO0FBQUE7O0FBcUNyQzRNLFFBQU0sR0FBRztBQUNQLFVBQU07QUFBRXFIO0FBQUYsUUFBZ0IsS0FBS3JDLEtBQTNCO0FBQ0EsVUFBTTtBQUFFL1MsWUFBRjtBQUFVRjtBQUFWLFFBQW1CLEtBQUsyTixLQUE5QjtBQUVBLHdCQUNFLDJEQUFDLDRDQUFELENBQU8sUUFBUCxxQkFDRSwyREFBQywwREFBRDtBQUNFLFlBQU0sRUFBRXpOLE1BRFY7QUFFRSxVQUFJLEVBQUVGLElBRlI7QUFHRSxXQUFLLEVBQUVvQixtRUFBUyxDQUFDbEIsTUFBTSxDQUFDYSxZQUFSLEVBQXNCLGVBQXRCLENBSGxCO0FBSUUsaUJBQVcsRUFBRUssbUVBQVMsQ0FBQ2xCLE1BQU0sQ0FBQ2EsWUFBUixFQUFzQixxQkFBdEIsQ0FKeEI7QUFLRSxhQUFPLEVBQUUsS0FBSzRNLEtBQUwsQ0FBV3FCLE9BTHRCO0FBTUUsaUJBQVcsRUFBRTtBQU5mLE1BREYsZUFRRSwyREFBQyxlQUFEO0FBQWlCLGVBQVMsRUFBQztBQUEzQixvQkFDRTtBQUFLLGVBQVMsRUFBQztBQUFmLG9CQUNFO0FBQUssZUFBUyxFQUFDO0FBQWYsb0JBQ0U7QUFBSyxlQUFTLEVBQUM7QUFBZixPQUNHNU4sbUVBQVMsQ0FBQ2xCLE1BQU0sQ0FBQ2EsWUFBUixFQUFzQix1QkFBdEIsQ0FEWixFQUM0RCxHQUQ1RCxlQUVFLDJEQUFDLHFEQUFEO0FBQU0sUUFBRSxFQUFDO0FBQVQsT0FBbUJLLG1FQUFTLENBQUNsQixNQUFNLENBQUNhLFlBQVIsRUFBc0IsTUFBdEIsQ0FBNUIsQ0FGRixDQURGLGVBS0U7QUFBSyxlQUFTLEVBQUMscUJBQWY7QUFBcUMsVUFBSSxFQUFDO0FBQTFDLE9BQ0dLLG1FQUFTLENBQUNsQixNQUFNLENBQUNhLFlBQVIsRUFBc0IsZ0JBQXRCLENBRFosQ0FMRixFQVNJLEtBQUtrUyxLQUFMLENBQVd6TCxNQUFYLENBQWtCME8sT0FBbEIsaUJBQ0E7QUFBSyxlQUFTLEVBQUMsb0JBQWY7QUFBb0MsVUFBSSxFQUFDO0FBQXpDLE9BQ0csS0FBS2pELEtBQUwsQ0FBV3pMLE1BQVgsQ0FBa0IwTyxPQURyQixDQVZKLGVBY0U7QUFBTSxjQUFRLEVBQUUsS0FBS3VCO0FBQXJCLG9CQUNFO0FBQUssZUFBUyxFQUFDO0FBQWYsb0JBQ0UsMEVBQ0dyVyxtRUFBUyxDQUFDbEIsTUFBTSxDQUFDYSxZQUFSLEVBQXNCLE9BQXRCLENBRFosb0JBQzRDLDZFQUQ1QyxDQURGLGVBSUU7QUFDRSxVQUFJLEVBQUMsTUFEUDtBQUVFLGVBQVMsRUFBQyxjQUZaO0FBR0UsV0FBSyxFQUFFLEtBQUtrUyxLQUFMLENBQVcvTCxLQUFYLENBQWlCaEcsS0FIMUI7QUFJRSxjQUFRLEVBQUVNLENBQUMsSUFDVCxLQUFLd1MsUUFBTCxDQUFjO0FBQUU5TSxhQUFLLEVBQUUzQyxNQUFNLENBQUNzRSxNQUFQLENBQWMsS0FBS29LLEtBQUwsQ0FBVy9MLEtBQXpCLEVBQWdDO0FBQUVoRyxlQUFLLEVBQUUsQ0FBQ00sQ0FBQyxDQUFDb1MsTUFBRixDQUFTN0MsS0FBVCxJQUFrQixFQUFuQixFQUM3RHVGLE9BRDZELENBQ3JELEtBRHFELEVBQzlDLEVBRDhDO0FBQVQsU0FBaEM7QUFBVCxPQUFkO0FBTEosTUFKRixlQWVFO0FBQU0sZUFBUyxFQUFDO0FBQWhCLE9BQStCLEtBQUtyRCxLQUFMLENBQVd6TCxNQUFYLENBQWtCdEcsS0FBakQsQ0FmRixDQURGLGVBa0JFO0FBQUssZUFBUyxFQUFDO0FBQWYsb0JBQ0UsMEVBQ0dFLG1FQUFTLENBQUNsQixNQUFNLENBQUNhLFlBQVIsRUFBc0IsVUFBdEIsQ0FEWixvQkFDK0MsNkVBRC9DLENBREYsZUFJRTtBQUNFLFVBQUksRUFBQyxVQURQO0FBRUUsZUFBUyxFQUFDLGNBRlo7QUFHRSxXQUFLLEVBQUUsS0FBS2tTLEtBQUwsQ0FBVy9MLEtBQVgsQ0FBaUJrRSxRQUgxQjtBQUlFLGNBQVEsRUFBRTVKLENBQUMsSUFDVCxLQUFLd1MsUUFBTCxDQUFjO0FBQUU5TSxhQUFLLEVBQUUzQyxNQUFNLENBQUNzRSxNQUFQLENBQWMsS0FBS29LLEtBQUwsQ0FBVy9MLEtBQXpCLEVBQWdDO0FBQUVrRSxrQkFBUSxFQUFFNUosQ0FBQyxDQUFDb1MsTUFBRixDQUFTN0M7QUFBckIsU0FBaEM7QUFBVCxPQUFkO0FBTEosTUFKRixlQVlFO0FBQU0sZUFBUyxFQUFDO0FBQWhCLE9BQ0csS0FBS2tDLEtBQUwsQ0FBV3pMLE1BQVgsQ0FBa0I0RCxRQURyQixDQVpGLENBbEJGLGVBa0NFO0FBQUssZUFBUyxFQUFDO0FBQWYsb0JBQ0UsMEVBQ0doSyxtRUFBUyxDQUFDbEIsTUFBTSxDQUFDYSxZQUFSLEVBQXNCLGlCQUF0QixDQURaLG9CQUNzRCw2RUFEdEQsQ0FERixlQUlFO0FBQ0UsVUFBSSxFQUFDLFVBRFA7QUFFRSxlQUFTLEVBQUMsY0FGWjtBQUdFLGNBQVEsRUFBRVMsQ0FBQyxJQUNULEtBQUt3UyxRQUFMLENBQWM7QUFBRTlNLGFBQUssRUFBRTNDLE1BQU0sQ0FBQ3NFLE1BQVAsQ0FBYyxLQUFLb0ssS0FBTCxDQUFXL0wsS0FBekIsRUFBZ0M7QUFBRW9RLDhCQUFvQixFQUFFOVYsQ0FBQyxDQUFDb1MsTUFBRixDQUFTN0M7QUFBakMsU0FBaEM7QUFBVCxPQUFkO0FBSkosTUFKRixlQVdFO0FBQU0sZUFBUyxFQUFDO0FBQWhCLE9BQ0csS0FBS2tDLEtBQUwsQ0FBV3pMLE1BQVgsQ0FBa0I4UCxvQkFEckIsQ0FYRixDQWxDRixlQWlERTtBQUFLLGVBQVMsRUFBQztBQUFmLG9CQUNFO0FBQVEsVUFBSSxFQUFDLFFBQWI7QUFBc0IsY0FBUSxFQUFFaEMsU0FBaEM7QUFBMkMsZUFBUyxFQUFDO0FBQXJELE9BQ0dsVSxtRUFBUyxDQUFDbEIsTUFBTSxDQUFDYSxZQUFSLEVBQXNCLFVBQXRCLENBRFosQ0FERixDQWpERixDQWRGLENBREYsQ0FERixDQVJGLGVBa0ZFLDJEQUFDLDBEQUFEO0FBQ0UsWUFBTSxFQUFFYixNQURWO0FBRUUsbUJBQWEsRUFBRSxLQUFLeU4sS0FBTCxDQUFXcUI7QUFGNUIsTUFsRkYsQ0FERjtBQXdGRDs7QUFqSW9DOztBQW9JdkMsTUFBTXdHLGVBQWUsR0FBR3ZDLEtBQUssS0FBSztBQUNoQy9TLFFBQU0sRUFBRStTLEtBQUssQ0FBQy9TLE1BRGtCO0FBRWhDRixNQUFJLEVBQUVpVCxLQUFLLENBQUNqVDtBQUZvQixDQUFMLENBQTdCOztBQUtlMlYsMEhBQU8sQ0FBQ0gsZUFBRCxDQUFQLENBQXlCNEIsUUFBekIsQ0FBZixFOzs7Ozs7Ozs7Ozs7QUMxSkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVlLGdFQUNiO0FBQ0VPLE9BQUssRUFBRSxJQURUO0FBRUV2UixNQUFJLEVBQUUsR0FGUjtBQUdFd1IsV0FBUyxFQUFFdkMsbURBQUlBO0FBSGpCLENBRGEsRUFNYjtBQUNFalAsTUFBSSxFQUFFLFFBRFI7QUFFRXdSLFdBQVMsRUFBRS9CLG9EQUFLQTtBQUZsQixDQU5hLEVBVWI7QUFDRXpQLE1BQUksRUFBRSwwQkFEUjtBQUVFd1IsV0FBUyxFQUFFL0Isb0RBQUtBO0FBRmxCLENBVmEsRUFjYjtBQUNFelAsTUFBSSxFQUFFLFdBRFI7QUFFRXdSLFdBQVMsRUFBRVIsdURBQVFBO0FBRnJCLENBZGEsRUFrQmI7QUFDRWhSLE1BQUksRUFBRSxtQkFEUjtBQUVFd1IsV0FBUyxFQUFFZCwrREFBZUE7QUFGNUIsQ0FsQmEsRUFzQmI7QUFDRWMsV0FBUyxFQUFFaEIsd0RBQVFBO0FBRHJCLENBdEJhLENBQWYsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDTkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Q0FFQTs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Q0FFQTs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtDQUdBOztBQUNBelcscURBQUssQ0FBQ2dDLFFBQU4sQ0FBZUMsdUZBQWdCLENBQUN5Vix3REFBRCxDQUEvQjtBQUNBLE1BQU1DLFNBQVMsR0FBR3JSLHlDQUFFLENBQUNzUixZQUFILENBQWdCLGtCQUFoQixFQUFvQyxNQUFwQyxDQUFsQjtBQUNBLE1BQU1DLGVBQWUsR0FBR3ZSLHlDQUFFLENBQUNzUixZQUFILENBQWdCLCtCQUFoQixFQUFpRCxNQUFqRCxDQUF4QjtBQUNBLE1BQU1FLFNBQVMsR0FBR2xSLDhDQUFPLEVBQXpCOztBQUNBLE1BQU1tUixJQUFJLEdBQUc1VixtQkFBTyxDQUFDLGtCQUFELENBQVAsQ0FBZ0I7QUFBRTZLLFFBQU0sRUFBRTtBQUFWLENBQWhCLENBQWIsQyxDQUVBOzs7QUFDQThLLFNBQVMsQ0FBQ0UsR0FBVixDQUFjRCxJQUFkO0FBQ0FELFNBQVMsQ0FBQ0UsR0FBVixDQUFjLENBQUN4VSxHQUFELEVBQU1DLEdBQU4sRUFBV0MsSUFBWCxLQUFvQjtBQUNoQyxNQUFJO0FBQ0YsVUFBTXVVLGNBQWMsR0FBRyxDQUFDelUsR0FBRyxDQUFDSSxPQUFKLENBQVksaUJBQVosS0FBa0MsRUFBbkMsRUFBdUNzVSxXQUF2QyxFQUF2QjtBQUVBMVUsT0FBRyxDQUFDekQsTUFBSixHQUFhQyxxREFBSyxDQUFDQyxRQUFOLEdBQWlCRixNQUE5QjtBQUNBeUQsT0FBRyxDQUFDa0ksUUFBSixHQUFlbEksR0FBRyxDQUFDSSxPQUFKLENBQVksaUJBQVosS0FBa0MsS0FBakQ7QUFDQUosT0FBRyxDQUFDMkksVUFBSixHQUFpQjNJLEdBQUcsQ0FBQ0ksT0FBSixDQUFZLG9CQUFaLEtBQXFDLFlBQXREO0FBQ0FKLE9BQUcsQ0FBQ2xELFFBQUosR0FBZWtELEdBQUcsQ0FBQ3pELE1BQUosQ0FBV3FILFlBQVgsQ0FBd0IzQyxRQUF4QixDQUFpQ3dULGNBQWpDLElBQW1EQSxjQUFuRCxHQUFvRSxJQUFuRjtBQUNBelUsT0FBRyxDQUFDNUMsWUFBSixHQUFvQjRDLEdBQUcsQ0FBQ3pELE1BQUosQ0FBV1MsUUFBWCxJQUF1QmdELEdBQUcsQ0FBQ3pELE1BQUosQ0FBV1MsUUFBWCxDQUFvQkMsT0FBcEIsQ0FBNEIrQyxHQUFHLENBQUNsRCxRQUFoQyxDQUF4QixJQUFzRSxFQUF6RjtBQUNBa0QsT0FBRyxDQUFDbUUsS0FBSixHQUFZc08sZ0ZBQWtCLENBQUN6UyxHQUFHLENBQUNtRSxLQUFMLENBQTlCOztBQUVBLFFBQUluRSxHQUFHLENBQUN6RCxNQUFKLENBQVdvWSxXQUFmLEVBQTRCO0FBQzFCMVUsU0FBRyxDQUFDNkcsSUFBSixDQUFTdU4sZUFBVDtBQUNBO0FBQ0Q7O0FBRURuVSxRQUFJO0FBQ0wsR0FoQkQsQ0FnQkUsT0FBT3JDLENBQVAsRUFBVTtBQUNWLFVBQU1FLEtBQUssR0FBR21GLDhFQUFlLENBQUNyRixDQUFELENBQTdCO0FBQ0FvQyxPQUFHLENBQUNLLE1BQUosQ0FBVyxHQUFYLEVBQWdCQyxJQUFoQixDQUFxQnhDLEtBQXJCO0FBQ0Q7QUFDRixDQXJCRCxFLENBdUJBOztBQUNBdVcsU0FBUyxDQUFDRSxHQUFWLENBQWMsZUFBZCxFQUErQkksNkRBQS9CO0FBQ0FOLFNBQVMsQ0FBQ0UsR0FBVixDQUFjLGFBQWQsRUFBNkJLLDREQUE3QjtBQUNBUCxTQUFTLENBQUNFLEdBQVYsQ0FBYyxjQUFkLEVBQThCTSw0REFBOUI7QUFDQVIsU0FBUyxDQUFDRSxHQUFWLENBQWMsWUFBZCxFQUE0Qk8sMERBQTVCO0FBQ0FULFNBQVMsQ0FBQ0UsR0FBVixDQUFjLG1CQUFkLEVBQW1DUSxrRUFBbkM7QUFDQVYsU0FBUyxDQUFDeFEsR0FBVixDQUFjLElBQWQsRUFBb0IsT0FBTzlELEdBQVAsRUFBWUMsR0FBWixLQUFvQjtBQUN0QyxNQUFJO0FBQ0YsVUFBTWdWLFlBQVksR0FBR0Msb0RBQVMsQ0FBQy9TLElBQVYsQ0FBZWdULEtBQUssSUFBSUMsa0VBQVMsQ0FBQ3BWLEdBQUcsQ0FBQ3FWLEdBQUwsRUFBVUYsS0FBVixDQUFqQyxDQUFyQjtBQUVBLFVBQU0zWSxxREFBSyxDQUFDZ0MsUUFBTixDQUFlZ0sscUZBQWMsQ0FBQztBQUNsQ04sY0FBUSxFQUFFbEksR0FBRyxDQUFDa0ksUUFEb0I7QUFFbENTLGdCQUFVLEVBQUUzSSxHQUFHLENBQUMySSxVQUZrQjtBQUdsQzdMLGNBQVEsRUFBRWtELEdBQUcsQ0FBQ2xEO0FBSG9CLEtBQUQsQ0FBN0IsQ0FBTjs7QUFNQSxRQUFJbVksWUFBWSxJQUFJQSxZQUFZLENBQUNoQixTQUFiLENBQXVCcUIsZUFBM0MsRUFBNEQ7QUFDMUQsWUFBTUwsWUFBWSxDQUFDaEIsU0FBYixDQUF1QnFCLGVBQXZCLEVBQU47QUFDRDs7QUFFRCxVQUFNQyxZQUFZLEdBQUcvWSxxREFBSyxDQUFDQyxRQUFOLEVBQXJCO0FBQ0EsVUFBTStZLE1BQU0sR0FBRyxJQUFJQyxrRUFBSixFQUFmO0FBQ0EsVUFBTUMsUUFBUSxHQUFHQyx1RUFBYyxlQUM3QiwyREFBQyxvREFBRDtBQUFVLFdBQUssRUFBRW5aLHFEQUFLQTtBQUF0QixvQkFDRSwyREFBQyw2REFBRDtBQUFjLGNBQVEsRUFBRXdELEdBQUcsQ0FBQ3FWO0FBQTVCLG9CQUNFLDJEQUFDLG1FQUFEO0FBQW1CLFdBQUssRUFBRUcsTUFBTSxDQUFDSTtBQUFqQyxvQkFDRSwyREFBQyxvREFBRCxxQkFDRSwyREFBQyx1REFBRCxRQUNHVixvREFBUyxDQUFDaEksR0FBVixDQUFjLENBQUNpSSxLQUFELEVBQVFuRyxLQUFSLGtCQUFrQiwyREFBQyxzREFBRDtBQUFPLFNBQUcsRUFBRUE7QUFBWixPQUF1Qm1HLEtBQXZCLEVBQWhDLENBREgsQ0FERixDQURGLENBREYsQ0FERixDQUQ2QixDQUEvQjtBQWNBLFVBQU1VLE1BQU0sR0FBR0MsbURBQU0sQ0FBQ0MsWUFBUCxFQUFmO0FBQ0EsVUFBTUMsU0FBUyxHQUFHUixNQUFNLENBQUNTLFlBQVAsRUFBbEI7QUFDQSxRQUFJaFMsUUFBUSxHQUFHa1EsU0FBZjtBQUVBbFEsWUFBUSxHQUFHQSxRQUFRLENBQUMwTyxPQUFULENBQWlCLG1CQUFqQixFQUFzQ2tELE1BQU0sQ0FBQ0ssY0FBUCxDQUFzQkMsUUFBdEIsRUFBdEMsQ0FBWDtBQUNBbFMsWUFBUSxHQUFHQSxRQUFRLENBQUMwTyxPQUFULENBQWlCLG1CQUFqQixFQUFzQ2tELE1BQU0sQ0FBQ08sY0FBUCxDQUFzQkQsUUFBdEIsRUFBdEMsQ0FBWDtBQUNBbFMsWUFBUSxHQUFHQSxRQUFRLENBQUMwTyxPQUFULENBQWlCLGVBQWpCLEVBQWtDa0QsTUFBTSxDQUFDMUssS0FBUCxDQUFhZ0wsUUFBYixFQUFsQyxDQUFYO0FBQ0FsUyxZQUFRLEdBQUdBLFFBQVEsQ0FBQzBPLE9BQVQsQ0FBaUIsa0JBQWpCLEVBQXFDa0QsTUFBTSxDQUFDUSxJQUFQLENBQVlGLFFBQVosRUFBckMsQ0FBWDtBQUNBbFMsWUFBUSxHQUFHQSxRQUFRLENBQUMwTyxPQUFULENBQWlCLGVBQWpCLEVBQWtDa0QsTUFBTSxDQUFDUyxJQUFQLENBQVlILFFBQVosRUFBbEMsQ0FBWDtBQUNBbFMsWUFBUSxHQUFHQSxRQUFRLENBQUMwTyxPQUFULENBQWlCLGdCQUFqQixFQUFtQ3FELFNBQW5DLENBQVg7QUFDQS9SLFlBQVEsR0FBR0EsUUFBUSxDQUFDME8sT0FBVCxDQUFpQixpQkFBakIsRUFBb0MrQyxRQUFwQyxDQUFYO0FBQ0F6UixZQUFRLEdBQUdBLFFBQVEsQ0FBQzBPLE9BQVQsQ0FDVCxpQkFEUyxFQUVSLCtCQUE4QjRELDJEQUFTLENBQUNoQixZQUFELENBQWUsV0FGOUMsQ0FBWDtBQUtBdFYsT0FBRyxDQUFDNkcsSUFBSixDQUFTN0MsUUFBVDtBQUNELEdBOUNELENBOENFLE9BQU9wRyxDQUFQLEVBQVU7QUFDVixVQUFNRSxLQUFLLEdBQUdtRiw4RUFBZSxDQUFDckYsQ0FBRCxDQUE3QjtBQUNBb0MsT0FBRyxDQUFDSyxNQUFKLENBQVcsR0FBWCxFQUFnQkMsSUFBaEIsQ0FBcUJ4QyxLQUFyQjtBQUNEO0FBQ0YsQ0FuREQsRSxDQXFEQTtBQUNBOztBQUNBO0FBQ08sTUFBTTdCLEdBQUcsR0FBR0Msd0RBQUEsQ0FBZ0JxYSxTQUFoQixDQUEwQmxDLFNBQTFCLENBQVosQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN4SFA7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBLE1BQU07QUFBRW1DO0FBQUYsSUFBY0Msa0dBQXBCO0FBRU8sTUFBTUMsS0FBSyxHQUFHO0FBQ25CQyxjQUFZLEVBQUUsY0FESztBQUVuQkMsYUFBVyxFQUFFLGFBRk07QUFHbkJDLGNBQVksRUFBRSxjQUhLO0FBSW5CQyxlQUFhLEVBQUU7QUFKSSxDQUFkO0FBT0EsTUFBTUMsV0FBVyxHQUFHN1MsS0FBSyxJQUFJLE9BQU8zRixRQUFQLEVBQWlCL0IsUUFBakIsS0FBOEI7QUFDaEUsUUFBTTtBQUFFcVY7QUFBRixNQUFjclYsUUFBUSxFQUE1QjtBQUNBLFFBQU13SCxRQUFRLEdBQUc2TixPQUFPLENBQUNtRixPQUFSLENBQWdCblcsTUFBaEIsR0FDYmdSLE9BQU8sQ0FBQ21GLE9BREssR0FFYixDQUFDLE1BQU1wUiw0Q0FBSyxDQUFDL0IsR0FBTixDQUFXLEdBQUUyUyxPQUFRLGFBQVlTLGlGQUFvQixDQUFDL1MsS0FBRCxDQUFRLEVBQTdELENBQVAsRUFBd0U1RixJQUY1RTtBQUlBQyxVQUFRLENBQUM7QUFBRTRJLFFBQUksRUFBRXVQLEtBQUssQ0FBQ0MsWUFBZDtBQUE0Qk8sV0FBTyxFQUFFbFQ7QUFBckMsR0FBRCxDQUFSO0FBQ0QsQ0FQTTtBQVNBLE1BQU1tVCxVQUFVLEdBQUc5UyxFQUFFLElBQUksTUFBTTlGLFFBQU4sSUFBa0I7QUFDaEQsUUFBTXlGLFFBQVEsR0FBRyxNQUFNNEIsNENBQUssQ0FBQy9CLEdBQU4sQ0FBVyxHQUFFMlMsT0FBUSxhQUFZblMsRUFBRyxFQUFwQyxDQUF2QjtBQUNBOUYsVUFBUSxDQUFDO0FBQUU0SSxRQUFJLEVBQUV1UCxLQUFLLENBQUNFLFdBQWQ7QUFBMkJNLFdBQU8sRUFBRWxULFFBQVEsQ0FBQzFGO0FBQTdDLEdBQUQsQ0FBUjtBQUNELENBSE07QUFLQSxNQUFNOFksYUFBYSxHQUFHOVksSUFBSSxJQUFJLE1BQU1DLFFBQU4sSUFBa0I7QUFDckQsUUFBTXlGLFFBQVEsR0FBRyxNQUFNNEIsNENBQUssQ0FBQ3RCLElBQU4sQ0FBWSxHQUFFa1MsT0FBUSxXQUF0QixFQUFrQ2xZLElBQWxDLENBQXZCO0FBQ0FDLFVBQVEsQ0FBQztBQUFFNEksUUFBSSxFQUFFdVAsS0FBSyxDQUFDRyxZQUFkO0FBQTRCSyxXQUFPLEVBQUVsVCxRQUFRLENBQUMxRjtBQUE5QyxHQUFELENBQVI7QUFDRCxDQUhNO0FBS0EsTUFBTStZLGFBQWEsR0FBRy9ZLElBQUksSUFBSSxNQUFNQyxRQUFOLElBQWtCO0FBQ3JELFFBQU15RixRQUFRLEdBQUcsTUFBTTRCLDRDQUFLLENBQUNiLEtBQU4sQ0FBYSxHQUFFeVIsT0FBUSxhQUFZbFksSUFBSSxDQUFDK0YsRUFBRyxFQUEzQyxFQUE4Qy9GLElBQTlDLENBQXZCO0FBQ0FDLFVBQVEsQ0FBQztBQUFFNEksUUFBSSxFQUFFdVAsS0FBSyxDQUFDSSxhQUFkO0FBQTZCSSxXQUFPLEVBQUVsVCxRQUFRLENBQUMxRjtBQUEvQyxHQUFELENBQVI7QUFDRCxDQUhNLEM7Ozs7Ozs7Ozs7OztBQzlCUDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBLE1BQU07QUFBRWtZO0FBQUYsSUFBY0Msa0dBQXBCO0FBRU8sTUFBTUMsS0FBSyxHQUFHO0FBQ25CWSwwQkFBd0IsRUFBRSwwQkFEUDtBQUVuQkMsbUJBQWlCLEVBQUUsbUJBRkE7QUFHbkJDLDBCQUF3QixFQUFFLDBCQUhQO0FBSW5CQyxpQkFBZSxFQUFFO0FBSkUsQ0FBZDtBQU9BLE1BQU12UCxzQkFBc0IsR0FBRzVKLElBQUksSUFBSSxNQUFNQyxRQUFOLElBQWtCO0FBQzlELFFBQU07QUFBRTBKLFlBQUY7QUFBWUU7QUFBWixNQUFxQjdKLElBQTNCO0FBQ0EsUUFBTTBGLFFBQVEsR0FBRyxNQUFNNEIsNENBQUssQ0FBQy9CLEdBQU4sQ0FBVyxHQUFFMlMsT0FBUSxnQ0FBK0J2TyxRQUFTLElBQUdFLElBQUssRUFBckUsQ0FBdkI7QUFDQTVKLFVBQVEsQ0FBQztBQUFFNEksUUFBSSxFQUFFdVAsS0FBSyxDQUFDWSx3QkFBZDtBQUF3Q0osV0FBTyxFQUFFbFQsUUFBUSxDQUFDMUY7QUFBMUQsR0FBRCxDQUFSO0FBQ0QsQ0FKTTtBQU1BLE1BQU1FLGdCQUFnQixHQUFHRixJQUFJLElBQUksTUFBTUMsUUFBTixJQUFrQjtBQUN4REEsVUFBUSxDQUFDO0FBQUU0SSxRQUFJLEVBQUV1UCxLQUFLLENBQUNhLGlCQUFkO0FBQWlDTCxXQUFPLEVBQUU1WTtBQUExQyxHQUFELENBQVI7QUFDRCxDQUZNO0FBSUEsTUFBTThKLHNCQUFzQixHQUFHOUosSUFBSSxJQUFJLE1BQU1DLFFBQU4sSUFBa0I7QUFDOURBLFVBQVEsQ0FBQztBQUFFNEksUUFBSSxFQUFFdVAsS0FBSyxDQUFDYyx3QkFBZDtBQUF3Q04sV0FBTyxFQUFFNVk7QUFBakQsR0FBRCxDQUFSO0FBQ0QsQ0FGTTtBQUlBLE1BQU1pSyxjQUFjLEdBQUdqSyxJQUFJLElBQUksT0FBT0MsUUFBUCxFQUFpQi9CLFFBQWpCLEtBQThCO0FBQ2xFLFFBQU13SCxRQUFRLEdBQUdyRCxNQUFNLENBQUNzRSxNQUFQLENBQWN6SSxRQUFRLEdBQUdGLE1BQVgsQ0FBa0JNLFdBQWhDLEVBQTZDMEIsSUFBN0MsQ0FBakI7QUFDQUMsVUFBUSxDQUFDO0FBQUU0SSxRQUFJLEVBQUV1UCxLQUFLLENBQUNlLGVBQWQ7QUFBK0JQLFdBQU8sRUFBRWxUO0FBQXhDLEdBQUQsQ0FBUjtBQUNELENBSE07QUFLQSxNQUFNMFQsaUJBQWlCLEdBQUdwWixJQUFJLElBQUksTUFBTUMsUUFBTixJQUFrQjtBQUN6RCxRQUFNeUYsUUFBUSxHQUFHLE1BQU00Qiw0Q0FBSyxDQUFDdEIsSUFBTixDQUFZLEdBQUVrUyxPQUFRLGNBQXRCLEVBQXFDbFksSUFBckMsQ0FBdkI7QUFDQUMsVUFBUSxDQUFDO0FBQUU0SSxRQUFJLEVBQUV1UCxLQUFLLENBQUNhLGlCQUFkO0FBQWlDTCxXQUFPLEVBQUVsVCxRQUFRLENBQUMxRjtBQUFuRCxHQUFELENBQVI7QUFDRCxDQUhNLEM7Ozs7Ozs7Ozs7OztBQzdCUDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQSxNQUFNO0FBQUVrWTtBQUFGLElBQWNDLGtHQUFwQjtBQUVPLE1BQU1DLEtBQUssR0FBRztBQUNuQmlCLHFCQUFtQixFQUFFLHFCQURGO0FBRW5CQyxjQUFZLEVBQUUsY0FGSztBQUduQkMsa0JBQWdCLEVBQUUsa0JBSEM7QUFJbkJDLG1CQUFpQixFQUFFLG1CQUpBO0FBS25CQyxnQkFBYyxFQUFFO0FBTEcsQ0FBZDtBQVFBLE1BQU1DLGlCQUFpQixHQUFHLE1BQU0sT0FBT3paLFFBQVAsRUFBaUIvQixRQUFqQixLQUE4QjtBQUNuRSxRQUFNO0FBQUVGLFVBQUY7QUFBVTJiO0FBQVYsTUFBbUJ6YixRQUFRLEVBQWpDO0FBQ0EsUUFBTTBILEtBQUssR0FBRztBQUNaeUssUUFBSSxFQUFFO0FBQUV1SixXQUFLLEVBQUU7QUFBVCxLQURNO0FBRVpDLFNBQUssRUFBRTtBQUFFdlQsWUFBTSxFQUFFO0FBQUUsY0FBTXRJLE1BQU0sQ0FBQzhiLFFBQVAsQ0FBZ0JDO0FBQXhCO0FBQVY7QUFGSyxHQUFkO0FBSUEsUUFBTXJVLFFBQVEsR0FBR2lVLElBQUksQ0FBQ0ssY0FBTCxDQUFvQnpYLE1BQXBCLEdBQ2JvWCxJQUFJLENBQUNLLGNBRFEsR0FFYixDQUFDLE1BQU0xUyw0Q0FBSyxDQUFDL0IsR0FBTixDQUFXLEdBQUUyUyxPQUFRLGFBQVlTLGlGQUFvQixDQUFDL1MsS0FBRCxDQUFRLEVBQTdELENBQVAsRUFBd0U1RixJQUY1RTtBQUlBQyxVQUFRLENBQUM7QUFBRTRJLFFBQUksRUFBRXVQLEtBQUssQ0FBQ2lCLG1CQUFkO0FBQW1DVCxXQUFPLEVBQUVsVDtBQUE1QyxHQUFELENBQVI7QUFDRCxDQVhNO0FBYUEsTUFBTXVVLFVBQVUsR0FBRyxNQUFNLE9BQU9oYSxRQUFQLEVBQWlCL0IsUUFBakIsS0FBOEI7QUFDNUQsUUFBTTtBQUFFRixVQUFGO0FBQVUyYjtBQUFWLE1BQW1CemIsUUFBUSxFQUFqQztBQUNBLFFBQU13SCxRQUFRLEdBQUdyRCxNQUFNLENBQUNDLElBQVAsQ0FBWXFYLElBQUksQ0FBQ08sT0FBakIsRUFBMEIzWCxNQUExQixHQUNib1gsSUFBSSxDQUFDTyxPQURRLEdBRWIsQ0FBQyxNQUFNNVMsNENBQUssQ0FBQy9CLEdBQU4sQ0FBVyxHQUFFMlMsT0FBUSxhQUFZbGEsTUFBTSxDQUFDOGIsUUFBUCxDQUFnQkksT0FBUSxFQUF6RCxDQUFQLEVBQW9FbGEsSUFGeEU7QUFJQUMsVUFBUSxDQUFDO0FBQUU0SSxRQUFJLEVBQUV1UCxLQUFLLENBQUNrQixZQUFkO0FBQTRCVixXQUFPLEVBQUVsVDtBQUFyQyxHQUFELENBQVI7QUFDRCxDQVBNO0FBU0EsTUFBTXlVLGVBQWUsR0FBRyxNQUFNLE9BQU9sYSxRQUFQLEVBQWlCL0IsUUFBakIsS0FBOEI7QUFDakUsUUFBTTtBQUFFRixVQUFGO0FBQVUyYjtBQUFWLE1BQW1CemIsUUFBUSxFQUFqQztBQUNBLFFBQU0wSCxLQUFLLEdBQUc7QUFDWnlLLFFBQUksRUFBRTtBQUFFdUosV0FBSyxFQUFFO0FBQVQsS0FETTtBQUVaQyxTQUFLLEVBQUU7QUFBRXZULFlBQU0sRUFBRTtBQUFFLGNBQU10SSxNQUFNLENBQUM4YixRQUFQLENBQWdCTTtBQUF4QjtBQUFWO0FBRkssR0FBZDtBQUlBLFFBQU0xVSxRQUFRLEdBQUdpVSxJQUFJLENBQUNTLFlBQUwsQ0FBa0I3WCxNQUFsQixHQUNib1gsSUFBSSxDQUFDUyxZQURRLEdBRWIsQ0FBQyxNQUFNOVMsNENBQUssQ0FBQy9CLEdBQU4sQ0FBVyxHQUFFMlMsT0FBUSxhQUFZUyxpRkFBb0IsQ0FBQy9TLEtBQUQsQ0FBUSxFQUE3RCxDQUFQLEVBQXdFNUYsSUFGNUU7QUFJQUMsVUFBUSxDQUFDO0FBQUU0SSxRQUFJLEVBQUV1UCxLQUFLLENBQUNvQixpQkFBZDtBQUFpQ1osV0FBTyxFQUFFbFQ7QUFBMUMsR0FBRCxDQUFSO0FBQ0QsQ0FYTTtBQWFBLE1BQU0yVSxjQUFjLEdBQUcsTUFBTSxPQUFPcGEsUUFBUCxFQUFpQi9CLFFBQWpCLEtBQThCO0FBQ2hFLFFBQU07QUFBRUYsVUFBRjtBQUFVMmI7QUFBVixNQUFtQnpiLFFBQVEsRUFBakM7QUFDQSxRQUFNd0gsUUFBUSxHQUFHckQsTUFBTSxDQUFDQyxJQUFQLENBQVlxWCxJQUFJLENBQUNXLFdBQWpCLEVBQThCL1gsTUFBOUIsR0FDYm9YLElBQUksQ0FBQ1csV0FEUSxHQUViLENBQUMsTUFBTWhULDRDQUFLLENBQUMvQixHQUFOLENBQVcsR0FBRTJTLE9BQVEsYUFBWWxhLE1BQU0sQ0FBQzhiLFFBQVAsQ0FBZ0JRLFdBQVksRUFBN0QsQ0FBUCxFQUF3RXRhLElBRjVFO0FBSUFDLFVBQVEsQ0FBQztBQUFFNEksUUFBSSxFQUFFdVAsS0FBSyxDQUFDbUIsZ0JBQWQ7QUFBZ0NYLFdBQU8sRUFBRWxUO0FBQXpDLEdBQUQsQ0FBUjtBQUNELENBUE07QUFTQSxNQUFNNlUsWUFBWSxHQUFHLE1BQU0sT0FBT3RhLFFBQVAsRUFBaUIvQixRQUFqQixLQUE4QjtBQUM5RCxRQUFNO0FBQUVGLFVBQUY7QUFBVTJiO0FBQVYsTUFBbUJ6YixRQUFRLEVBQWpDO0FBQ0EsUUFBTXdILFFBQVEsR0FBR3JELE1BQU0sQ0FBQ0MsSUFBUCxDQUFZcVgsSUFBSSxDQUFDYSxTQUFqQixFQUE0QmpZLE1BQTVCLEdBQ2JvWCxJQUFJLENBQUNhLFNBRFEsR0FFYixDQUFDLE1BQU1sVCw0Q0FBSyxDQUFDL0IsR0FBTixDQUFXLEdBQUUyUyxPQUFRLGFBQVlsYSxNQUFNLENBQUM4YixRQUFQLENBQWdCVSxTQUFVLEVBQTNELENBQVAsRUFBc0V4YSxJQUYxRTtBQUlBQyxVQUFRLENBQUM7QUFBRTRJLFFBQUksRUFBRXVQLEtBQUssQ0FBQ3FCLGNBQWQ7QUFBOEJiLFdBQU8sRUFBRWxUO0FBQXZDLEdBQUQsQ0FBUjtBQUNELENBUE0sQzs7Ozs7Ozs7Ozs7O0FDeERQO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBLE1BQU07QUFBRXdTO0FBQUYsSUFBY0Msa0dBQXBCO0FBRU8sTUFBTUMsS0FBSyxHQUFHO0FBQ25CcUMsYUFBVyxFQUFFLGFBRE07QUFFbkJDLFlBQVUsRUFBRSxZQUZPO0FBR25CQyxhQUFXLEVBQUUsYUFITTtBQUluQkMsY0FBWSxFQUFFLGNBSks7QUFLbkJDLGVBQWEsRUFBRTtBQUxJLENBQWQ7QUFRQSxNQUFNQyxVQUFVLEdBQUdsVixLQUFLLElBQUksT0FBTzNGLFFBQVAsRUFBaUIvQixRQUFqQixLQUE4QjtBQUMvRCxRQUFNO0FBQUU2YztBQUFGLE1BQWE3YyxRQUFRLEVBQTNCO0FBQ0EsUUFBTXdILFFBQVEsR0FBR3FWLE1BQU0sQ0FBQ3JDLE9BQVAsQ0FBZW5XLE1BQWYsR0FDYndZLE1BQU0sQ0FBQ3JDLE9BRE0sR0FFYixDQUFDLE1BQU1wUiw0Q0FBSyxDQUFDL0IsR0FBTixDQUFXLEdBQUUyUyxPQUFRLFlBQVdTLGlGQUFvQixDQUFDL1MsS0FBRCxDQUFRLEVBQTVELENBQVAsRUFBdUU1RixJQUYzRTtBQUlBQyxVQUFRLENBQUM7QUFBRTRJLFFBQUksRUFBRXVQLEtBQUssQ0FBQ3FDLFdBQWQ7QUFBMkI3QixXQUFPLEVBQUVsVDtBQUFwQyxHQUFELENBQVI7QUFDRCxDQVBNO0FBU0EsTUFBTXNWLFNBQVMsR0FBR2pWLEVBQUUsSUFBSSxNQUFNOUYsUUFBTixJQUFrQjtBQUMvQyxRQUFNeUYsUUFBUSxHQUFHLE1BQU00Qiw0Q0FBSyxDQUFDL0IsR0FBTixDQUFXLEdBQUUyUyxPQUFRLFlBQVduUyxFQUFHLEVBQW5DLENBQXZCO0FBQ0E5RixVQUFRLENBQUM7QUFBRTRJLFFBQUksRUFBRXVQLEtBQUssQ0FBQ3NDLFVBQWQ7QUFBMEI5QixXQUFPLEVBQUVsVCxRQUFRLENBQUMxRjtBQUE1QyxHQUFELENBQVI7QUFDRCxDQUhNO0FBS0EsTUFBTWliLFlBQVksR0FBR2piLElBQUksSUFBSSxNQUFNQyxRQUFOLElBQWtCO0FBQ3BELFFBQU15RixRQUFRLEdBQUcsTUFBTTRCLDRDQUFLLENBQUN0QixJQUFOLENBQVksR0FBRWtTLE9BQVEsVUFBdEIsRUFBaUNsWSxJQUFqQyxDQUF2QjtBQUNBQyxVQUFRLENBQUM7QUFBRTRJLFFBQUksRUFBRXVQLEtBQUssQ0FBQ3VDLFdBQWQ7QUFBMkIvQixXQUFPLEVBQUVsVCxRQUFRLENBQUMxRjtBQUE3QyxHQUFELENBQVI7QUFDRCxDQUhNO0FBS0EsTUFBTWtiLFlBQVksR0FBR2xiLElBQUksSUFBSSxNQUFNQyxRQUFOLElBQWtCO0FBQ3BELFFBQU15RixRQUFRLEdBQUcsTUFBTTRCLDRDQUFLLENBQUNiLEtBQU4sQ0FBYSxHQUFFeVIsT0FBUSxZQUFXbFksSUFBSSxDQUFDK0YsRUFBRyxFQUExQyxFQUE2Qy9GLElBQTdDLENBQXZCO0FBQ0FDLFVBQVEsQ0FBQztBQUFFNEksUUFBSSxFQUFFdVAsS0FBSyxDQUFDd0MsWUFBZDtBQUE0QmhDLFdBQU8sRUFBRWxULFFBQVEsQ0FBQzFGO0FBQTlDLEdBQUQsQ0FBUjtBQUNELENBSE07QUFLQSxNQUFNbWIsWUFBWSxHQUFHcFYsRUFBRSxJQUFJLE1BQU05RixRQUFOLElBQWtCO0FBQ2xELFFBQU15RixRQUFRLEdBQUcsTUFBTTRCLDRDQUFLLENBQUNHLE1BQU4sQ0FBYyxHQUFFeVEsT0FBUSxZQUFXblMsRUFBRyxFQUF0QyxDQUF2QjtBQUNBOUYsVUFBUSxDQUFDO0FBQUU0SSxRQUFJLEVBQUV1UCxLQUFLLENBQUN5QyxhQUFkO0FBQTZCakMsV0FBTyxFQUFFbFQsUUFBUSxDQUFDMUY7QUFBL0MsR0FBRCxDQUFSO0FBQ0QsQ0FITSxDOzs7Ozs7Ozs7Ozs7QUNwQ1A7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0EsTUFBTTtBQUFFa1k7QUFBRixJQUFjQyxrR0FBcEI7QUFFTyxNQUFNQyxLQUFLLEdBQUc7QUFDbkJnRCxZQUFVLEVBQUUsWUFETztBQUVuQkMsV0FBUyxFQUFFLFdBRlE7QUFHbkJDLFVBQVEsRUFBRSxVQUhTO0FBSW5CQyxtQkFBaUIsRUFBQyxtQkFKQztBQUtuQkMsdUJBQXFCLEVBQUUsdUJBTEo7QUFNbkJDLFlBQVUsRUFBRSxZQU5PO0FBT25CQyxpQkFBZSxFQUFFLGlCQVBFO0FBUW5CQyxPQUFLLEVBQUUsT0FSWTtBQVNuQkMsUUFBTSxFQUFFLFFBVFc7QUFVbkJDLFVBQVEsRUFBRSxVQVZTO0FBV25CQyxJQUFFLEVBQUUsSUFYZTtBQVluQkMsa0JBQWdCLEVBQUUsa0JBWkM7QUFhbkJDLFdBQVMsRUFBRSxXQWJRO0FBY25CQyxpQkFBZSxFQUFFLGlCQWRFO0FBZW5CQyxpQkFBZSxFQUFFLGlCQWZFO0FBZ0JuQkMsbUJBQWlCLEVBQUUsbUJBaEJBO0FBaUJuQkMsb0JBQWtCLEVBQUUsb0JBakJEO0FBa0JuQkMscUJBQW1CLEVBQUUscUJBbEJGO0FBbUJuQkMscUJBQW1CLEVBQUUscUJBbkJGO0FBb0JuQkMsZUFBYSxFQUFFO0FBcEJJLENBQWQ7QUF1QkEsTUFBTUMsY0FBYyxHQUFHQyxJQUFJLElBQUksT0FBT3hjLFFBQVAsRUFBaUIvQixRQUFqQixLQUE4QjtBQUNsRSxRQUFNO0FBQUVKO0FBQUYsTUFBV0ksUUFBUSxFQUF6QjtBQUNBLFFBQU13SCxRQUFRLEdBQUc1SCxJQUFJLENBQUM0ZSxNQUFMLEdBQ2I1ZSxJQUFJLENBQUM0ZSxNQURRLEdBRWIsQ0FBQyxNQUFNcFYsNENBQUssQ0FBQy9CLEdBQU4sQ0FBVyxHQUFFMlMsT0FBUSxpQkFBZ0J1RSxJQUFLLEVBQTFDLEVBQTZDO0FBQUVFLGdCQUFZLEVBQUU7QUFBaEIsR0FBN0MsQ0FBUCxFQUFxRjNjLElBRnpGO0FBSUFDLFVBQVEsQ0FBQztBQUFFNEksUUFBSSxFQUFFdVAsS0FBSyxDQUFDZ0QsVUFBZDtBQUEwQnhDLFdBQU8sRUFBRWxUO0FBQW5DLEdBQUQsQ0FBUjtBQUNELENBUE07QUFTQSxNQUFNa1gsUUFBUSxHQUFHaFgsS0FBSyxJQUFJLE9BQU8zRixRQUFQLEVBQWlCL0IsUUFBakIsS0FBOEI7QUFDN0QsUUFBTTtBQUFFSjtBQUFGLE1BQVdJLFFBQVEsRUFBekI7QUFDQSxRQUFNd0gsUUFBUSxHQUFHNUgsSUFBSSxDQUFDNGEsT0FBTCxDQUFhblcsTUFBYixHQUNiekUsSUFBSSxDQUFDNGEsT0FEUSxHQUViLENBQUMsTUFBTXBSLDRDQUFLLENBQUMvQixHQUFOLENBQVcsR0FBRTJTLE9BQVEsVUFBU1MsaUZBQW9CLENBQUMvUyxLQUFELENBQVEsRUFBMUQsQ0FBUCxFQUFxRTVGLElBRnpFO0FBSUFDLFVBQVEsQ0FBQztBQUFFNEksUUFBSSxFQUFFdVAsS0FBSyxDQUFDaUQsU0FBZDtBQUF5QnpDLFdBQU8sRUFBRWxUO0FBQWxDLEdBQUQsQ0FBUjtBQUNELENBUE07QUFTQSxNQUFNMkMsT0FBTyxHQUFHdEMsRUFBRSxJQUFJLE1BQU05RixRQUFOLElBQWtCO0FBQzdDLFFBQU15RixRQUFRLEdBQUcsTUFBTTRCLDRDQUFLLENBQUMvQixHQUFOLENBQVcsR0FBRTJTLE9BQVEsVUFBU25TLEVBQUcsRUFBakMsQ0FBdkI7QUFDQTlGLFVBQVEsQ0FBQztBQUFFNEksUUFBSSxFQUFFdVAsS0FBSyxDQUFDa0QsUUFBZDtBQUF3QjFDLFdBQU8sRUFBRWxULFFBQVEsQ0FBQzFGO0FBQTFDLEdBQUQsQ0FBUjtBQUNELENBSE07QUFLQSxNQUFNNmMsZUFBZSxHQUFFelUsTUFBTSxJQUFJLE1BQU1uSSxRQUFOLElBQWtCO0FBQ3hELFFBQU15RixRQUFRLEdBQUcsQ0FBQyxNQUFNNEIsNENBQUssQ0FBQy9CLEdBQU4sQ0FBVyxHQUFFMlMsT0FBUSxVQUFTOVAsTUFBTyxPQUFyQyxDQUFQLEVBQXFEcEksSUFBdEU7QUFDQUMsVUFBUSxDQUFDO0FBQUU0SSxRQUFJLEVBQUV1UCxLQUFLLENBQUNtRCxpQkFBZDtBQUFpQzNDLFdBQU8sRUFBRWxUO0FBQTFDLEdBQUQsQ0FBUjtBQUNELENBSE07QUFLQSxNQUFNb1gsbUJBQW1CLEdBQUcsQ0FBQy9XLEVBQUQsRUFBS0gsS0FBTCxLQUFlLE9BQU8zRixRQUFQLEVBQWlCL0IsUUFBakIsS0FBOEI7QUFDOUUsUUFBTTtBQUFFSjtBQUFGLE1BQVdJLFFBQVEsRUFBekI7QUFDQSxRQUFNd0gsUUFBUSxHQUFHNUgsSUFBSSxDQUFDaWYsZ0JBQUwsQ0FBc0J4YSxNQUF0QixHQUErQixDQUEvQixHQUNiekUsSUFBSSxDQUFDaWYsZ0JBRFEsR0FFYixDQUFDLE1BQU16Viw0Q0FBSyxDQUFDL0IsR0FBTixDQUFXLEdBQUUyUyxPQUFRLFVBQVNuUyxFQUFHLGFBQVk0UyxpRkFBb0IsQ0FBQy9TLEtBQUQsQ0FBUSxFQUF6RSxDQUFQLEVBQW9GNUYsSUFGeEY7QUFJQUMsVUFBUSxDQUFDO0FBQUU0SSxRQUFJLEVBQUV1UCxLQUFLLENBQUNvRCxxQkFBZDtBQUFxQzVDLFdBQU8sRUFBRWxUO0FBQTlDLEdBQUQsQ0FBUjtBQUNELENBUE07QUFTQSxNQUFNdUQsVUFBVSxHQUFHakosSUFBSSxJQUFJLE1BQU1DLFFBQU4sSUFBa0I7QUFDbEQsUUFBTXlGLFFBQVEsR0FBRyxNQUFNNEIsNENBQUssQ0FBQ2IsS0FBTixDQUFhLEdBQUV5UixPQUFRLFVBQVNsWSxJQUFJLENBQUMrRixFQUFHLEVBQXhDLEVBQTJDL0YsSUFBM0MsQ0FBdkI7QUFDQUMsVUFBUSxDQUFDO0FBQUU0SSxRQUFJLEVBQUV1UCxLQUFLLENBQUNxRCxVQUFkO0FBQTBCN0MsV0FBTyxFQUFFbFQsUUFBUSxDQUFDMUY7QUFBNUMsR0FBRCxDQUFSO0FBQ0QsQ0FITTtBQUtBLE1BQU1nZCxjQUFjLEdBQUdoZCxJQUFJLElBQUksTUFBTUMsUUFBTixJQUFrQjtBQUN0RCxRQUFNeUYsUUFBUSxHQUFHLE1BQU00Qiw0Q0FBSyxDQUFDdEIsSUFBTixDQUFZLEdBQUVrUyxPQUFRLHdCQUF0QixFQUErQ2xZLElBQS9DLENBQXZCO0FBQ0FDLFVBQVEsQ0FBQztBQUFFNEksUUFBSSxFQUFFdVAsS0FBSyxDQUFDc0QsZUFBZDtBQUErQjlDLFdBQU8sRUFBRWxULFFBQVEsQ0FBQzFGLElBQVQsQ0FBY2lkO0FBQXRELEdBQUQsQ0FBUjtBQUNELENBSE07QUFLQSxNQUFNckosS0FBSyxHQUFHNVQsSUFBSSxJQUFJLE1BQU1DLFFBQU4sSUFBa0I7QUFDN0MsUUFBTXlGLFFBQVEsR0FBRyxNQUFNNEIsNENBQUssQ0FBQ3RCLElBQU4sQ0FBWSxHQUFFa1MsT0FBUSxjQUF0QixFQUFxQ2xZLElBQXJDLENBQXZCO0FBQ0FDLFVBQVEsQ0FBQztBQUFFNEksUUFBSSxFQUFFdVAsS0FBSyxDQUFDdUQsS0FBZDtBQUFxQi9DLFdBQU8sRUFBRWxULFFBQVEsQ0FBQzFGO0FBQXZDLEdBQUQsQ0FBUjtBQUNELENBSE07QUFLQSxNQUFNMkssTUFBTSxHQUFHLE1BQU0sTUFBTTFLLFFBQU4sSUFBa0I7QUFDNUNBLFVBQVEsQ0FBQztBQUFFNEksUUFBSSxFQUFFdVAsS0FBSyxDQUFDd0QsTUFBZDtBQUFzQmhELFdBQU8sRUFBRTtBQUEvQixHQUFELENBQVI7QUFDRCxDQUZNO0FBSUEsTUFBTXBELFFBQVEsR0FBR3hWLElBQUksSUFBSSxNQUFNQyxRQUFOLElBQWtCO0FBQ2hELFFBQU15RixRQUFRLEdBQUcsTUFBTTRCLDRDQUFLLENBQUN0QixJQUFOLENBQVksR0FBRWtTLE9BQVEsaUJBQXRCLEVBQXdDbFksSUFBeEMsQ0FBdkI7QUFDQUMsVUFBUSxDQUFDO0FBQUU0SSxRQUFJLEVBQUV1UCxLQUFLLENBQUN5RCxRQUFkO0FBQXdCakQsV0FBTyxFQUFFbFQsUUFBUSxDQUFDMUY7QUFBMUMsR0FBRCxDQUFSO0FBQ0QsQ0FITTtBQUtBLE1BQU04VCxFQUFFLEdBQUcsTUFBTSxNQUFNN1QsUUFBTixJQUFrQjtBQUN4QyxNQUFJO0FBQ0YsUUFBSXlGLFFBQVEsR0FBRyxNQUFNNEIsNENBQUssQ0FBQy9CLEdBQU4sQ0FBVyxHQUFFMlMsT0FBUSxXQUFyQixDQUFyQjtBQUNBalksWUFBUSxDQUFDO0FBQUU0SSxVQUFJLEVBQUV1UCxLQUFLLENBQUMwRCxFQUFkO0FBQWtCbEQsYUFBTyxFQUFFbFQsUUFBUSxDQUFDMUY7QUFBcEMsS0FBRCxDQUFSO0FBQ0QsR0FIRCxDQUdFLE9BQU9WLENBQVAsRUFBVTtBQUNWVyxZQUFRLENBQUM7QUFBRTRJLFVBQUksRUFBRXVQLEtBQUssQ0FBQzBELEVBQWQ7QUFBa0JsRCxhQUFPLEVBQUU7QUFBM0IsS0FBRCxDQUFSO0FBQ0Q7QUFDRixDQVBNO0FBU0EsTUFBTTdELGVBQWUsR0FBRy9VLElBQUksSUFBSSxNQUFNQyxRQUFOLElBQWtCO0FBQ3ZELFFBQU15RixRQUFRLEdBQUcsTUFBTTRCLDRDQUFLLENBQUN0QixJQUFOLENBQVksR0FBRWtTLE9BQVEseUJBQXRCLEVBQWdEbFksSUFBaEQsQ0FBdkI7QUFDQUMsVUFBUSxDQUFDO0FBQUU0SSxRQUFJLEVBQUV1UCxLQUFLLENBQUMyRCxnQkFBZDtBQUFnQ25ELFdBQU8sRUFBRWxULFFBQVEsQ0FBQzFGO0FBQWxELEdBQUQsQ0FBUjtBQUNELENBSE07QUFLQSxNQUFNNlQsUUFBUSxHQUFHalMsS0FBSyxJQUFJLE1BQU0zQixRQUFOLElBQWtCO0FBQ2pEK0osY0FBWSxDQUFDcEksS0FBYixHQUFxQkEsS0FBckI7QUFDQTNCLFVBQVEsQ0FBQztBQUFFNEksUUFBSSxFQUFFdVAsS0FBSyxDQUFDNEQsU0FBZDtBQUF5QnBELFdBQU8sRUFBRWhYO0FBQWxDLEdBQUQsQ0FBUjtBQUNELENBSE07QUFLQSxNQUFNc2IsYUFBYSxHQUFHblgsRUFBRSxJQUFJLE1BQU05RixRQUFOLElBQWtCO0FBQ25ELFFBQU15RixRQUFRLEdBQUcsTUFBTTRCLDRDQUFLLENBQUMvQixHQUFOLENBQVcsR0FBRTJTLE9BQVEsVUFBU25TLEVBQUcsU0FBakMsQ0FBdkI7QUFDQTlGLFVBQVEsQ0FBQztBQUFFNEksUUFBSSxFQUFFdVAsS0FBSyxDQUFDNkQsZUFBZDtBQUErQnJELFdBQU8sRUFBRWxULFFBQVEsQ0FBQzFGO0FBQWpELEdBQUQsQ0FBUjtBQUNELENBSE07QUFLQSxNQUFNbWQsZUFBZSxHQUFHbmQsSUFBSSxJQUFJLE1BQU1DLFFBQU4sSUFBa0I7QUFDdkQsUUFBTXlGLFFBQVEsR0FBRyxNQUFNNEIsNENBQUssQ0FBQ3RCLElBQU4sQ0FBWSxHQUFFa1MsT0FBUSxVQUFTbFksSUFBSSxDQUFDK0YsRUFBRyxTQUF2QyxFQUFpRC9GLElBQWpELENBQXZCO0FBQ0FDLFVBQVEsQ0FBQztBQUFFNEksUUFBSSxFQUFFdVAsS0FBSyxDQUFDOEQsZUFBZDtBQUErQnRELFdBQU8sRUFBRWxULFFBQVEsQ0FBQzFGO0FBQWpELEdBQUQsQ0FBUjtBQUNELENBSE07QUFLQSxNQUFNb2QsZUFBZSxHQUFHcGQsSUFBSSxJQUFJLE1BQU1DLFFBQU4sSUFBa0I7QUFDdkQsUUFBTXlGLFFBQVEsR0FBRyxNQUFNNEIsNENBQUssQ0FBQ2IsS0FBTixDQUFhLEdBQUV5UixPQUFRLFVBQVNsWSxJQUFJLENBQUMrRixFQUFHLFNBQXhDLEVBQWtEL0YsSUFBbEQsQ0FBdkI7QUFDQUMsVUFBUSxDQUFDO0FBQUU0SSxRQUFJLEVBQUV1UCxLQUFLLENBQUMrRCxpQkFBZDtBQUFpQ3ZELFdBQU8sRUFBRWxULFFBQVEsQ0FBQzFGO0FBQW5ELEdBQUQsQ0FBUjtBQUNELENBSE07QUFLQSxNQUFNcWQsZ0JBQWdCLEdBQUcsQ0FBQ0MsWUFBRCxFQUFlQyxVQUFmLEtBQThCLE1BQU10ZCxRQUFOLElBQWtCO0FBQzlFLFFBQU15RixRQUFRLEdBQUcsTUFBTTRCLDRDQUFLLENBQUN0QixJQUFOLENBQVksR0FBRWtTLE9BQVEsVUFBU29GLFlBQWEscUJBQTVDLEVBQWtFQyxVQUFsRSxDQUF2QjtBQUNBdGQsVUFBUSxDQUFDO0FBQUU0SSxRQUFJLEVBQUV1UCxLQUFLLENBQUNnRSxrQkFBZDtBQUFrQ3hELFdBQU8sRUFBRWxULFFBQVEsQ0FBQzFGO0FBQXBELEdBQUQsQ0FBUjtBQUNELENBSE07QUFLQSxNQUFNd2Qsa0JBQWtCLEdBQUd4ZCxJQUFJLElBQUksTUFBTUMsUUFBTixJQUFrQjtBQUMxRCxNQUFJeUYsUUFBUSxHQUFHLE1BQU00Qiw0Q0FBSyxDQUFDdEIsSUFBTixDQUFZLEdBQUVrUyxPQUFRLDRCQUF0QixFQUFtRGxZLElBQW5ELENBQXJCO0FBQ0FDLFVBQVEsQ0FBQztBQUFFNEksUUFBSSxFQUFFdVAsS0FBSyxDQUFDaUUsbUJBQWQ7QUFBbUN6RCxXQUFPLEVBQUVsVCxRQUFRLENBQUMxRjtBQUFyRCxHQUFELENBQVI7QUFDRCxDQUhNO0FBS0EsTUFBTXlkLGtCQUFrQixHQUFHLENBQUNILFlBQUQsRUFBZXRkLElBQWYsS0FBd0IsTUFBTUMsUUFBTixJQUFrQjtBQUMxRSxRQUFNeUYsUUFBUSxHQUFHLE1BQU00Qiw0Q0FBSyxDQUFDdEIsSUFBTixDQUFZLEdBQUVrUyxPQUFRLFVBQVNvRixZQUFhLHNCQUE1QyxFQUFrRXRkLElBQWxFLENBQXZCO0FBQ0FDLFVBQVEsQ0FBQztBQUFFNEksUUFBSSxFQUFFdVAsS0FBSyxDQUFDZ0Usa0JBQWQ7QUFBa0N4RCxXQUFPLEVBQUVsVCxRQUFRLENBQUMxRjtBQUFwRCxHQUFELENBQVI7QUFDRCxDQUhNO0FBS0EsTUFBTTBkLFlBQVksR0FBRzFkLElBQUksSUFBSSxNQUFNQyxRQUFOLElBQWtCO0FBQ3BELE1BQUl5RixRQUFRLEdBQUcsTUFBTTRCLDRDQUFLLENBQUN0QixJQUFOLENBQVksR0FBRWtTLE9BQVEsc0JBQXRCLEVBQTZDbFksSUFBN0MsQ0FBckI7QUFDQUMsVUFBUSxDQUFDO0FBQUU0SSxRQUFJLEVBQUV1UCxLQUFLLENBQUNtRSxhQUFkO0FBQTZCM0QsV0FBTyxFQUFFbFQsUUFBUSxDQUFDMUY7QUFBL0MsR0FBRCxDQUFSO0FBQ0QsQ0FITSxDOzs7Ozs7Ozs7Ozs7QUN6SVA7QUFBQTtBQUFBO0FBQUE7O0FBRUEsTUFBTTJkLE9BQU8sR0FBRzFmLEtBQUssSUFBSTBELElBQUksSUFBSWljLE1BQU0sSUFBSTtBQUN6QyxRQUFNO0FBQUU1ZixVQUFGO0FBQVVGO0FBQVYsTUFBbUJHLEtBQUssQ0FBQ0MsUUFBTixFQUF6QjtBQUNBLFFBQU07QUFBRUk7QUFBRixNQUFrQk4sTUFBeEI7QUFDQSxTQUFPc0osNENBQUssQ0FBQ3VXLFFBQU4sQ0FBZWhjLE9BQWYsQ0FBdUJpYyxNQUF2QixDQUE4QnZjLGFBQXJDOztBQUVBLE1BQUl6RCxJQUFJLENBQUM4RCxLQUFULEVBQWdCO0FBQ2QwRixnREFBSyxDQUFDdVcsUUFBTixDQUFlaGMsT0FBZixDQUF1QmljLE1BQXZCLENBQThCLGVBQTlCLElBQWtELFVBQVNoZ0IsSUFBSSxDQUFDOEQsS0FBTSxFQUF0RTtBQUNEOztBQUVELE1BQUl0RCxXQUFXLENBQUNxTCxRQUFoQixFQUEwQjtBQUN4QnJDLGdEQUFLLENBQUN1VyxRQUFOLENBQWVoYyxPQUFmLENBQXVCaWMsTUFBdkIsQ0FBOEIsaUJBQTlCLElBQW1EeGYsV0FBVyxDQUFDcUwsUUFBL0Q7QUFDRDs7QUFFRCxNQUFJckwsV0FBVyxDQUFDQyxRQUFoQixFQUEwQjtBQUN4QitJLGdEQUFLLENBQUN1VyxRQUFOLENBQWVoYyxPQUFmLENBQXVCaWMsTUFBdkIsQ0FBOEIsaUJBQTlCLElBQW1EeGYsV0FBVyxDQUFDQyxRQUEvRDtBQUNEOztBQUVELFNBQU9vRCxJQUFJLENBQUNpYyxNQUFELENBQVg7QUFDRCxDQWxCRDs7QUFvQmVELHNFQUFmLEU7Ozs7Ozs7Ozs7OztBQ3RCQTtBQUFBO0FBQUE7QUFBQTtBQUVlSSxpSEFBZixFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNGQTtBQUVBLE1BQU0vRyxZQUFZLEdBQUc7QUFDbkIwQixTQUFPLEVBQUUsRUFEVTtBQUVuQjNPLE1BQUksRUFBRTtBQUZhLENBQXJCO0FBS2UsU0FBU2lVLE9BQVQsQ0FBaUJqTixLQUFLLEdBQUdpRyxZQUF6QixFQUF1QzRHLE1BQXZDLEVBQStDO0FBQzVELFVBQVFBLE1BQU0sQ0FBQy9VLElBQWY7QUFDRTtBQUNFLGFBQU9rSSxLQUFQOztBQUVGLFNBQUtxSCw4REFBSyxDQUFDQyxZQUFYO0FBQ0UsK0JBQ0t0SCxLQURMO0FBRUUySCxlQUFPLEVBQUVrRixNQUFNLENBQUNoRjtBQUZsQjs7QUFLRixTQUFLUiw4REFBSyxDQUFDRSxXQUFYO0FBQ0UsK0JBQ0t2SCxLQURMO0FBRUVoSCxZQUFJLEVBQUU2VCxNQUFNLENBQUNoRjtBQUZmOztBQUtGLFNBQUtSLDhEQUFLLENBQUNHLFlBQVg7QUFDRSwrQkFDS3hILEtBREw7QUFFRTJILGVBQU8sRUFBRTNILEtBQUssQ0FBQzJILE9BQU4sQ0FBY3JVLE1BQWQsQ0FBcUJ1WixNQUFNLENBQUNoRixPQUE1QixDQUZYO0FBR0U3TyxZQUFJLEVBQUU2VCxNQUFNLENBQUNoRjtBQUhmOztBQU1GLFNBQUtSLDhEQUFLLENBQUNJLGFBQVg7QUFDRSwrQkFDS3pILEtBREw7QUFFRTJILGVBQU8sRUFBRTNILEtBQUssQ0FBQzJILE9BQU4sQ0FBYy9KLEdBQWQsQ0FBa0JsTSxJQUFJLElBQUtBLElBQUksQ0FBQ3NELEVBQUwsS0FBWTZYLE1BQU0sQ0FBQ2hGLE9BQVAsQ0FBZTdTLEVBQTNCLEdBQWdDNlgsTUFBTSxDQUFDaEYsT0FBdkMsR0FBaURuVyxJQUE1RSxDQUZYO0FBR0VzSCxZQUFJLEVBQUU2VCxNQUFNLENBQUNoRjtBQUhmO0FBeEJKO0FBOEJELEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3RDRDs7QUFFQSxNQUFNcUYsZUFBZSxHQUFHbE4sS0FBSyxJQUFJO0FBQy9CLFNBQU9BLEtBQUssQ0FBQ3RTLFFBQU4sQ0FBZUMsT0FBZixDQUF1QnFTLEtBQUssQ0FBQ3pTLFdBQU4sQ0FBa0JDLFFBQXpDLEtBQXNELEVBQTdEO0FBQ0QsQ0FGRDs7QUFJQSxNQUFNd0MsYUFBYSxHQUFHLGtGQUF0QjtBQUVBLE1BQU1pVyxZQUFZLEdBQUc7QUFDbkJaLGFBQVcsRUFBRSxLQURNO0FBRW5COEgsb0JBQWtCLEVBQUUsRUFGRDtBQUduQjdZLGNBQVksRUFBRSxDQUFDLElBQUQsRUFBTyxJQUFQLENBSEs7QUFJbkI0QixhQUFXLEVBQUUsSUFKTTtBQUtuQnNGLFNBQU8sRUFBRyxHQUFFeEwsYUFBYyxvRUFMUDtBQU1uQnlMLFNBQU8sRUFBRSxrQkFOVTtBQU9uQjJSLFFBQU0sRUFBRSxtQkFQVztBQVFuQnJFLFVBQVEsRUFBRTtBQUNSTSxnQkFBWSxFQUFFLHNCQUROO0FBRVJMLGtCQUFjLEVBQUUsc0JBRlI7QUFHUkcsV0FBTyxFQUFFLHNCQUhEO0FBSVJJLGVBQVcsRUFBRSxzQkFKTDtBQUtSRSxhQUFTLEVBQUU7QUFMSCxHQVJTO0FBZW5CL2IsVUFBUSxFQUFFO0FBQ1IyZixnQkFBWSxFQUFFLEVBRE47QUFFUkMsYUFBUyxFQUFFLEVBRkg7QUFHUkMsY0FBVSxFQUFFLEVBSEo7QUFJUkMsdUJBQW1CLEVBQUUsRUFKYjtBQUtSelAsYUFBUyxFQUFFLEVBTEg7QUFNUnBRLFdBQU8sRUFBRTtBQU5ELEdBZlM7QUF1Qm5COGYsUUFBTSxFQUFFO0FBQ05DLFdBQU8sRUFBRSxrQkFESDtBQUVOQyxXQUFPLEVBQUU7QUFGSCxHQXZCVztBQTJCbkJySixPQUFLLEVBQUU7QUFDTEMsUUFBSSxFQUFFLHNCQUREO0FBRUxxSixZQUFRLEVBQUUsc0JBRkw7QUFHTEMsV0FBTyxFQUFFO0FBSEosR0EzQlk7QUFnQ25CdGdCLGFBQVcsRUFBRTtBQUNYcUwsWUFBUSxFQUFFLEtBREM7QUFFWFMsY0FBVSxFQUFFLFlBRkQ7QUFHWDdMLFlBQVEsRUFBRTtBQUhDLEdBaENNO0FBcUNuQk0sY0FBWSxFQUFFO0FBckNLLENBQXJCO0FBd0NlLFNBQVNtZixPQUFULENBQWlCak4sS0FBSyxHQUFHaUcsWUFBekIsRUFBdUM0RyxNQUF2QyxFQUErQztBQUM1RCxVQUFRQSxNQUFNLENBQUMvVSxJQUFmO0FBQ0U7QUFDRSxhQUFPa0ksS0FBUDs7QUFFRixTQUFLcUgsNkRBQUssQ0FBQ1ksd0JBQVg7QUFDRSwrQkFDS2pJLEtBREw7QUFFRWhILFlBQUksRUFBRTZULE1BQU0sQ0FBQ2hGO0FBRmY7O0FBS0YsU0FBS1IsNkRBQUssQ0FBQ2EsaUJBQVg7QUFBOEI7QUFDNUIsY0FBTTRGLFFBQVEsR0FBR3hjLE1BQU0sQ0FBQ3NFLE1BQVAsQ0FBY29LLEtBQWQsRUFBcUI2TSxNQUFNLENBQUNoRixPQUE1QixDQUFqQjtBQUNBLGlDQUNLaUcsUUFETDtBQUVFaGdCLHNCQUFZLEVBQUVvZixlQUFlLENBQUNZLFFBQUQ7QUFGL0I7QUFJRDs7QUFFRCxTQUFLekcsNkRBQUssQ0FBQ2Msd0JBQVg7QUFDRSwrQkFDS25JLEtBREw7QUFFRXRTLGdCQUFRLG9CQUNIc1MsS0FBSyxDQUFDdFMsUUFESDtBQUVOOGYsNkJBQW1CLEVBQUVYLE1BQU0sQ0FBQ2hGO0FBRnRCO0FBRlY7O0FBUUYsU0FBS1IsNkRBQUssQ0FBQ2UsZUFBWDtBQUNFLCtCQUNLcEksS0FETDtBQUVFelMsbUJBQVcsRUFBRXNmLE1BQU0sQ0FBQ2hGLE9BRnRCO0FBR0UvWixvQkFBWSxFQUFFb2YsZUFBZSxDQUFDbE4sS0FBRDtBQUgvQjtBQTVCSjtBQW1DRCxDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNwRkQ7QUFFQSxNQUFNaUcsWUFBWSxHQUFHO0FBQ25CZ0QsZ0JBQWMsRUFBRSxFQURHO0FBRW5CRSxTQUFPLEVBQUUsRUFGVTtBQUduQkUsY0FBWSxFQUFFLEVBSEs7QUFJbkJFLGFBQVcsRUFBRSxFQUpNO0FBS25CRSxXQUFTLEVBQUU7QUFMUSxDQUFyQjtBQVFlLFNBQVN3RCxPQUFULENBQWlCak4sS0FBSyxHQUFHaUcsWUFBekIsRUFBdUM0RyxNQUF2QyxFQUErQztBQUM1RCxVQUFRQSxNQUFNLENBQUMvVSxJQUFmO0FBQ0U7QUFDRSxhQUFPa0ksS0FBUDs7QUFFRixTQUFLcUgsMkRBQUssQ0FBQ2lCLG1CQUFYO0FBQ0UsK0JBQ0t0SSxLQURMO0FBRUVpSixzQkFBYyxFQUFFNEQsTUFBTSxDQUFDaEY7QUFGekI7O0FBS0YsU0FBS1IsMkRBQUssQ0FBQ2tCLFlBQVg7QUFDRSwrQkFDS3ZJLEtBREw7QUFFRW1KLGVBQU8sRUFBRTBELE1BQU0sQ0FBQ2hGO0FBRmxCOztBQUtGLFNBQUtSLDJEQUFLLENBQUNvQixpQkFBWDtBQUNFLCtCQUNLekksS0FETDtBQUVFcUosb0JBQVksRUFBRXdELE1BQU0sQ0FBQ2hGO0FBRnZCOztBQUtGLFNBQUtSLDJEQUFLLENBQUNtQixnQkFBWDtBQUNFLCtCQUNLeEksS0FETDtBQUVFdUosbUJBQVcsRUFBRXNELE1BQU0sQ0FBQ2hGO0FBRnRCOztBQUtGLFNBQUtSLDJEQUFLLENBQUNxQixjQUFYO0FBQ0UsK0JBQ0sxSSxLQURMO0FBRUV5SixpQkFBUyxFQUFFb0QsTUFBTSxDQUFDaEY7QUFGcEI7QUE3Qko7QUFrQ0QsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDN0NEO0FBRUEsTUFBTTVCLFlBQVksR0FBRztBQUNuQjBCLFNBQU8sRUFBRSxFQURVO0FBRW5CM08sTUFBSSxFQUFFO0FBRmEsQ0FBckI7QUFLZSxTQUFTaVUsT0FBVCxDQUFpQmpOLEtBQUssR0FBR2lHLFlBQXpCLEVBQXVDNEcsTUFBdkMsRUFBK0M7QUFDNUQsVUFBUUEsTUFBTSxDQUFDL1UsSUFBZjtBQUNFO0FBQ0UsYUFBT2tJLEtBQVA7O0FBRUYsU0FBS3FILDZEQUFLLENBQUNxQyxXQUFYO0FBQ0UsK0JBQ0sxSixLQURMO0FBRUUySCxlQUFPLEVBQUVrRixNQUFNLENBQUNoRjtBQUZsQjs7QUFLRixTQUFLUiw2REFBSyxDQUFDc0MsVUFBWDtBQUNFLCtCQUNLM0osS0FETDtBQUVFaEgsWUFBSSxFQUFFNlQsTUFBTSxDQUFDaEY7QUFGZjs7QUFLRixTQUFLUiw2REFBSyxDQUFDdUMsV0FBWDtBQUNFLCtCQUNLNUosS0FETDtBQUVFMkgsZUFBTyxFQUFFM0gsS0FBSyxDQUFDMkgsT0FBTixDQUFjclUsTUFBZCxDQUFxQnVaLE1BQU0sQ0FBQ2hGLE9BQTVCLENBRlg7QUFHRTdPLFlBQUksRUFBRTZULE1BQU0sQ0FBQ2hGO0FBSGY7O0FBTUYsU0FBS1IsNkRBQUssQ0FBQ3dDLFlBQVg7QUFDRSwrQkFDSzdKLEtBREw7QUFFRTJILGVBQU8sRUFBRTNILEtBQUssQ0FBQzJILE9BQU4sQ0FBYy9KLEdBQWQsQ0FBa0JsTSxJQUFJLElBQUtBLElBQUksQ0FBQ3NELEVBQUwsS0FBWTZYLE1BQU0sQ0FBQ2hGLE9BQVAsQ0FBZTdTLEVBQTNCLEdBQWdDNlgsTUFBTSxDQUFDaEYsT0FBdkMsR0FBaURuVyxJQUE1RSxDQUZYO0FBR0VzSCxZQUFJLEVBQUU2VCxNQUFNLENBQUNoRjtBQUhmOztBQU1GLFNBQUtSLDZEQUFLLENBQUN5QyxhQUFYO0FBQ0UsK0JBQ0s5SixLQURMO0FBRUUySCxlQUFPLEVBQUUzSCxLQUFLLENBQUMySCxPQUFOLENBQWNsVyxNQUFkLENBQXFCQyxJQUFJLElBQUlBLElBQUksQ0FBQ3NELEVBQUwsS0FBWTZYLE1BQU0sQ0FBQ2hGLE9BQVAsQ0FBZTdTLEVBQXhEO0FBRlg7QUEvQko7QUFxQ0QsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDN0NEO0FBRUEsTUFBTWlSLFlBQVksR0FBRztBQUNuQjlVLE1BQUksRUFBRSxJQURhO0FBRW5CaUwsWUFBVSxFQUFFLEtBRk87QUFHbkJ1UCxRQUFNLEVBQUUsSUFIVztBQUluQnRhLFFBQU0sRUFBRSxFQUpXO0FBS25CMGMsY0FBWSxFQUFFLElBTEs7QUFNbkIvQixrQkFBZ0IsRUFBRSxFQU5DO0FBT25CckUsU0FBTyxFQUFFLEVBUFU7QUFRbkIzTyxNQUFJLEVBQUUsSUFSYTtBQVNuQm5JLE9BQUssRUFBRTtBQVRZLENBQXJCO0FBWWUsU0FBU29jLE9BQVQsQ0FBaUJqTixLQUFLLEdBQUdpRyxZQUF6QixFQUF1QzRHLE1BQXZDLEVBQStDO0FBQzVELFVBQVFBLE1BQU0sQ0FBQy9VLElBQWY7QUFDRTtBQUNFLGFBQU9rSSxLQUFQOztBQUVGLFNBQUtxSCwyREFBSyxDQUFDZ0QsVUFBWDtBQUNFLCtCQUNLckssS0FETDtBQUVFMkwsY0FBTSxFQUFFa0IsTUFBTSxDQUFDaEY7QUFGakI7O0FBS0YsU0FBS1IsMkRBQUssQ0FBQ2lELFNBQVg7QUFDRSwrQkFDS3RLLEtBREw7QUFFRTJILGVBQU8sRUFBRWtGLE1BQU0sQ0FBQ2hGO0FBRmxCOztBQUtGLFNBQUtSLDJEQUFLLENBQUNrRCxRQUFYO0FBQ0UsK0JBQ0t2SyxLQURMO0FBRUVoSCxZQUFJLEVBQUU2VCxNQUFNLENBQUNoRjtBQUZmOztBQUtGLFNBQUtSLDJEQUFLLENBQUNtRCxpQkFBWDtBQUNFLCtCQUNLeEssS0FETDtBQUVFK04sb0JBQVksRUFBRWxCLE1BQU0sQ0FBQ2hGO0FBRnZCOztBQUtGLFNBQUtSLDJEQUFLLENBQUNvRCxxQkFBWDtBQUNFLCtCQUNLekssS0FETDtBQUVFZ00sd0JBQWdCLEVBQUVhLE1BQU0sQ0FBQ2hGO0FBRjNCOztBQUtGLFNBQUtSLDJEQUFLLENBQUNxRCxVQUFYO0FBQ0UsK0JBQ0sxSyxLQURMO0FBRUUySCxlQUFPLEVBQUUzSCxLQUFLLENBQUMySCxPQUFOLENBQWMvSixHQUFkLENBQWtCbE0sSUFBSSxJQUFLQSxJQUFJLENBQUNzRCxFQUFMLEtBQVk2WCxNQUFNLENBQUNoRixPQUFQLENBQWU3UyxFQUEzQixHQUFnQzZYLE1BQU0sQ0FBQ2hGLE9BQXZDLEdBQWlEblcsSUFBNUUsQ0FGWDtBQUdFc0gsWUFBSSxFQUFFNlQsTUFBTSxDQUFDaEY7QUFIZjs7QUFNRixTQUFLUiwyREFBSyxDQUFDc0QsZUFBWDtBQUNFLCtCQUNLM0ssS0FETDtBQUVFaEgsWUFBSSxFQUFFNlQsTUFBTSxDQUFDaEY7QUFGZjs7QUFLRixTQUFLUiwyREFBSyxDQUFDdUQsS0FBWDtBQUNFLCtCQUNLNUssS0FETDtBQUVFaEgsWUFBSSxFQUFFNlQsTUFBTSxDQUFDaEYsT0FBUCxDQUFlcUUsUUFGdkI7QUFHRXJiLGFBQUssRUFBRWdjLE1BQU0sQ0FBQ2hGLE9BQVAsQ0FBZWhYO0FBSHhCOztBQU1GLFNBQUt3VywyREFBSyxDQUFDd0QsTUFBWDtBQUNFLCtCQUNLN0ssS0FETDtBQUVFN08sWUFBSSxFQUFFLElBRlI7QUFHRU4sYUFBSyxFQUFFO0FBSFQ7O0FBTUYsU0FBS3dXLDJEQUFLLENBQUN5RCxRQUFYO0FBQ0UsK0JBQ0s5SyxLQURMO0FBRUVoSCxZQUFJLEVBQUU2VCxNQUFNLENBQUNoRixPQUFQLENBQWVxRSxRQUZ2QjtBQUdFcmIsYUFBSyxFQUFFZ2MsTUFBTSxDQUFDaEYsT0FBUCxDQUFlaFg7QUFIeEI7O0FBTUYsU0FBS3dXLDJEQUFLLENBQUMwRCxFQUFYO0FBQ0UsK0JBQ0svSyxLQURMO0FBRUU3TyxZQUFJLEVBQUUwYixNQUFNLENBQUNoRixPQUZmO0FBR0V6TCxrQkFBVSxFQUFFO0FBSGQ7O0FBTUYsU0FBS2lMLDJEQUFLLENBQUMyRCxnQkFBWDtBQUNFLCtCQUNLaEwsS0FETDtBQUVFaEgsWUFBSSxFQUFFNlQsTUFBTSxDQUFDaEY7QUFGZjs7QUFLRixTQUFLUiwyREFBSyxDQUFDNEQsU0FBWDtBQUNFLCtCQUNLakwsS0FETDtBQUVFblAsYUFBSyxFQUFFZ2MsTUFBTSxDQUFDaEY7QUFGaEI7O0FBS0YsU0FBS1IsMkRBQUssQ0FBQzZELGVBQVg7QUFDRSwrQkFDS2xMLEtBREw7QUFFRTNPLGNBQU0sRUFBRXdiLE1BQU0sQ0FBQ2hGO0FBRmpCOztBQUtGLFNBQUtSLDJEQUFLLENBQUM4RCxlQUFYO0FBQ0UsK0JBQ0tuTCxLQURMO0FBRUUzTyxjQUFNLEVBQUUyTyxLQUFLLENBQUMzTyxNQUFOLENBQWFpQyxNQUFiLENBQW9CdVosTUFBTSxDQUFDaEYsT0FBM0IsQ0FGVjtBQUdFN08sWUFBSSxFQUFFNlQsTUFBTSxDQUFDaEY7QUFIZjs7QUFNRixTQUFLUiwyREFBSyxDQUFDK0QsaUJBQVg7QUFDRSwrQkFDS3BMLEtBREw7QUFFRTNPLGNBQU0sRUFBRTJPLEtBQUssQ0FBQzNPLE1BQU4sQ0FBYUksTUFBYixDQUFvQkMsSUFBSSxJQUFJQSxJQUFJLEtBQUttYixNQUFNLENBQUNoRixPQUE1QyxDQUZWO0FBR0U3TyxZQUFJLEVBQUU2VCxNQUFNLENBQUNoRjtBQUhmOztBQU1GLFNBQUtSLDJEQUFLLENBQUNnRSxrQkFBWDtBQUNFLCtCQUNLckwsS0FETDtBQUVFaEgsWUFBSSxFQUFFNlQsTUFBTSxDQUFDaEY7QUFGZjs7QUFLRixTQUFLUiwyREFBSyxDQUFDaUUsbUJBQVg7QUFDRSwrQkFDS3RMLEtBREw7QUFFRW5QLGFBQUssRUFBRWdjLE1BQU0sQ0FBQ2hGLE9BQVAsQ0FBZWhYLEtBRnhCO0FBR0VtSSxZQUFJLEVBQUU2VCxNQUFNLENBQUNoRixPQUFQLENBQWV6WjtBQUh2Qjs7QUFNRixTQUFLaVosMkRBQUssQ0FBQ21FLGFBQVg7QUFDRSwrQkFDS3hMLEtBREw7QUFFRTtBQUNBaEgsWUFBSSxFQUFFNlQsTUFBTSxDQUFDaEYsT0FBUCxDQUFlelo7QUFIdkI7QUF6SEo7QUErSEQsQzs7Ozs7Ozs7Ozs7O0FDOUlEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtDQUNBOztBQUNBO0FBQ0E7Q0FFQTs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTZYLFlBQVksR0FBRyxFQUFuQjs7QUFFQSxJQUFJLE9BQU9sTSxNQUFQLEtBQWtCLFdBQXRCLEVBQW1DO0FBQ2pDa00sY0FBWSxHQUFHbE0sTUFBTSxDQUFDa00sWUFBUCxJQUF1QkEsWUFBdEM7QUFDRDs7QUFFRCxNQUFNK0gsUUFBUSxHQUFHQyw2REFBZSxDQUFDO0FBQy9CekwsU0FBTyxFQUFFMEwsaUVBRHNCO0FBRS9CamhCLFFBQU0sRUFBRWtoQixnRUFGdUI7QUFHL0J2RixNQUFJLEVBQUV3Riw4REFIeUI7QUFJL0JwRSxRQUFNLEVBQUVxRSxnRUFKdUI7QUFLL0J0aEIsTUFBSSxFQUFFdWhCLDhEQUFXQTtBQUxjLENBQUQsQ0FBaEM7QUFRQSxNQUFNQyxVQUFVLEdBQUdDLG9HQUFtQixDQUFDQyw2REFBZSxDQUFDQyx1RUFBRCxFQUFvQjFCLHFFQUFwQixDQUFoQixDQUF0QztBQUNBLE1BQU05ZixLQUFLLEdBQUd5aEIseURBQVcsQ0FBQ1gsUUFBRCxFQUFXL0gsWUFBWCxFQUF5QnNJLFVBQXpCLENBQXpCO0FBRWVyaEIsb0VBQWYsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM1Qk8sTUFBTTRILGFBQWEsR0FBRyxPQUFPTCxFQUFQLEVBQVdlLFVBQVgsRUFBdUJvWixVQUF2QixLQUFzQztBQUNqRSxNQUFJamEsUUFBUSxHQUFHLElBQWY7O0FBRUEsTUFBSSxDQUFDaWEsVUFBTCxFQUFpQjtBQUNmLFdBQU8sSUFBUDtBQUNEOztBQUVEamEsVUFBUSxHQUFHRixFQUFFLENBQUNlLFVBQUgsQ0FBY0EsVUFBZCxDQUFYO0FBQ0FiLFVBQVEsR0FBRyxNQUFNQSxRQUFRLENBQUNtQixHQUFULENBQWE4WSxVQUFiLEVBQXlCcGEsR0FBekIsRUFBakI7QUFDQUcsVUFBUSxHQUFHQSxRQUFRLENBQUMxRixJQUFULEVBQVg7O0FBRUEsTUFBSTBGLFFBQUosRUFBYztBQUNaQSxZQUFRLHFCQUFRQSxRQUFSO0FBQWtCSyxRQUFFLEVBQUU0WjtBQUF0QixNQUFSO0FBQ0Q7O0FBRUQsU0FBT2phLFFBQVA7QUFDRCxDQWhCTTtBQWtCQSxNQUFNQyxVQUFVLEdBQUcsT0FBT0gsRUFBUCxFQUFXZSxVQUFYLEVBQXVCWCxLQUF2QixLQUFpQztBQUN6RCxRQUFNZ2EsV0FBVyxHQUFHQyxnQkFBZ0IsQ0FBQ2phLEtBQUQsQ0FBcEM7QUFDQSxNQUFJa2EsT0FBTyxHQUFHdGEsRUFBRSxDQUFDZSxVQUFILENBQWNBLFVBQWQsQ0FBZDtBQUNBLE1BQUliLFFBQVEsR0FBRyxFQUFmOztBQUVBLE1BQUlrYSxXQUFXLENBQUMvRixLQUFaLElBQXFCK0YsV0FBVyxDQUFDL0YsS0FBWixDQUFrQnRYLE1BQTNDLEVBQW1EO0FBQ2pEcWQsZUFBVyxDQUFDL0YsS0FBWixDQUFrQmtHLE9BQWxCLENBQTBCdGQsSUFBSSxJQUFLcWQsT0FBTyxHQUFHQSxPQUFPLENBQUNqRyxLQUFSLENBQWNwWCxJQUFJLENBQUMsQ0FBRCxDQUFsQixFQUF1QkEsSUFBSSxDQUFDLENBQUQsQ0FBM0IsRUFBZ0NBLElBQUksQ0FBQyxDQUFELENBQXBDLENBQTdDO0FBQ0Q7O0FBQ0QsTUFBSW1kLFdBQVcsQ0FBQ3ZQLElBQVosSUFBb0J1UCxXQUFXLENBQUN2UCxJQUFaLENBQWlCOU4sTUFBekMsRUFBaUQ7QUFDL0NxZCxlQUFXLENBQUN2UCxJQUFaLENBQWlCMFAsT0FBakIsQ0FBeUJ0ZCxJQUFJLElBQUtxZCxPQUFPLEdBQUdBLE9BQU8sQ0FBQ0UsT0FBUixDQUFnQnZkLElBQUksQ0FBQyxDQUFELENBQXBCLEVBQXlCQSxJQUFJLENBQUMsQ0FBRCxDQUFKLElBQVcsS0FBcEMsQ0FBNUM7QUFDRDs7QUFDRCxNQUFJbWQsV0FBVyxDQUFDSyxLQUFoQixFQUF1QjtBQUNyQkgsV0FBTyxHQUFHQSxPQUFPLENBQUNHLEtBQVIsQ0FBY0wsV0FBVyxDQUFDSyxLQUExQixDQUFWO0FBQ0Q7O0FBRUR2YSxVQUFRLEdBQUcsTUFBTW9hLE9BQU8sQ0FBQ3ZhLEdBQVIsRUFBakI7QUFDQUcsVUFBUSxHQUFHQSxRQUFRLENBQUN3YSxJQUFULENBQWN2UixHQUFkLENBQWtCOUgsR0FBRyxzQkFBVUEsR0FBRyxDQUFDN0csSUFBSixFQUFWO0FBQXNCK0YsTUFBRSxFQUFFYyxHQUFHLENBQUNkO0FBQTlCLElBQXJCLENBQVg7O0FBRUEsTUFBSTZaLFdBQVcsQ0FBQ08sTUFBaEIsRUFBd0I7QUFDdEJ6YSxZQUFRLEdBQUdBLFFBQVEsQ0FBQ2lKLEdBQVQsQ0FBYWxNLElBQUksSUFBSTtBQUM5QixhQUFPSixNQUFNLENBQUNDLElBQVAsQ0FBWUcsSUFBWixFQUFrQm9GLE1BQWxCLENBQXlCLENBQUNDLEtBQUQsRUFBUW5KLEdBQVIsS0FBZ0I7QUFDOUMsZUFBT2loQixXQUFXLENBQUNPLE1BQVosQ0FBbUJ6ZCxRQUFuQixDQUE0Qi9ELEdBQTVCLElBQW1DMEQsTUFBTSxDQUFDc0UsTUFBUCxDQUFjbUIsS0FBZCxFQUFxQjtBQUFFLFdBQUNuSixHQUFELEdBQU84RCxJQUFJLENBQUM5RCxHQUFEO0FBQWIsU0FBckIsQ0FBbkMsR0FBZ0ZtSixLQUF2RjtBQUNELE9BRk0sRUFFSixFQUZJLENBQVA7QUFHRCxLQUpVLENBQVg7QUFLRDs7QUFFRCxTQUFPcEMsUUFBUDtBQUNELENBM0JNO0FBNkJBLE1BQU1XLGVBQWUsR0FBRyxPQUFPYixFQUFQLEVBQVdlLFVBQVgsRUFBdUI2WixHQUF2QixLQUErQjtBQUM1RCxNQUFJMWEsUUFBUSxHQUFHLEVBQWY7QUFDQSxNQUFJMmEsa0JBQWtCLEdBQUdELEdBQUcsQ0FBQzVkLE1BQUosQ0FBV3VELEVBQUUsSUFBSUEsRUFBakIsRUFBcUI0SSxHQUFyQixDQUF5QjVJLEVBQUUsSUFBSVAsRUFBRSxDQUFDZSxVQUFILENBQWNBLFVBQWQsRUFBMEJNLEdBQTFCLENBQThCZCxFQUE5QixFQUFrQ1IsR0FBbEMsRUFBL0IsQ0FBekI7QUFFQUcsVUFBUSxHQUFHLE1BQU00YSxPQUFPLENBQUNDLEdBQVIsQ0FBWUYsa0JBQVosQ0FBakI7QUFDQTNhLFVBQVEsR0FBR0EsUUFBUSxDQUFDbEQsTUFBVCxDQUFnQnFFLEdBQUcsSUFBSSxDQUFDQSxHQUFHLENBQUMyWixPQUE1QixFQUFxQzdSLEdBQXJDLENBQXlDOUgsR0FBRyxzQkFBVUEsR0FBRyxDQUFDN0csSUFBSixFQUFWO0FBQXNCK0YsTUFBRSxFQUFFYyxHQUFHLENBQUNkO0FBQTlCLElBQTVDLENBQVg7QUFFQSxTQUFPTCxRQUFQO0FBQ0QsQ0FSTTtBQVVBLE1BQU1tYSxnQkFBZ0IsR0FBRzdmLElBQUksSUFBSTtBQUN0QyxNQUFJMEYsUUFBUSxHQUFHLEVBQWY7QUFDQSxNQUFJK2EsY0FBYyxHQUFHLEVBQXJCOztBQUVBLE9BQUssTUFBTTloQixHQUFYLElBQWtCcUIsSUFBbEIsRUFBd0I7QUFDdEJ5Z0Isa0JBQWMsR0FBR3pnQixJQUFJLENBQUNyQixHQUFELENBQXJCOztBQUVBLFFBQUlBLEdBQUcsS0FBSyxPQUFaLEVBQXFCO0FBQ25CK0csY0FBUSxDQUFDdWEsS0FBVCxHQUFpQlEsY0FBakI7QUFDRDs7QUFDRCxRQUFJOWhCLEdBQUcsS0FBSyxRQUFaLEVBQXNCO0FBQ3BCK0csY0FBUSxDQUFDeWEsTUFBVCxHQUFrQk0sY0FBbEI7QUFDRDs7QUFDRCxRQUFJOWhCLEdBQUcsS0FBSyxNQUFaLEVBQW9CO0FBQ2xCK0csY0FBUSxDQUFDMkssSUFBVCxHQUFnQixFQUFoQjs7QUFFQSxXQUFLLE1BQU0xUixHQUFYLElBQWtCOGhCLGNBQWxCLEVBQWtDO0FBQ2hDL2EsZ0JBQVEsQ0FBQzJLLElBQVQsQ0FBY1YsSUFBZCxDQUFtQixDQUFDaFIsR0FBRCxFQUFNOGhCLGNBQWMsQ0FBQzloQixHQUFELENBQXBCLENBQW5CO0FBQ0Q7QUFDRjs7QUFDRCxRQUFJQSxHQUFHLEtBQUssT0FBWixFQUFxQjtBQUNuQitHLGNBQVEsQ0FBQ21VLEtBQVQsR0FBaUIsRUFBakI7O0FBRUEsV0FBSyxNQUFNbGIsR0FBWCxJQUFrQjhoQixjQUFsQixFQUFrQztBQUNoQyxhQUFLLE1BQU1DLFFBQVgsSUFBdUJELGNBQWMsQ0FBQzloQixHQUFELENBQXJDLEVBQTRDO0FBQzFDLGNBQUkraEIsUUFBUSxLQUFLLElBQWpCLEVBQXVCO0FBQ3JCaGIsb0JBQVEsQ0FBQ21VLEtBQVQsQ0FBZWxLLElBQWYsQ0FBb0IsQ0FBQ2hSLEdBQUQsRUFBTSxnQkFBTixFQUF3QjhoQixjQUFjLENBQUM5aEIsR0FBRCxDQUFkLENBQW9CK2hCLFFBQXBCLENBQXhCLENBQXBCO0FBQ0QsV0FGRCxNQUVPLElBQUlBLFFBQVEsS0FBSyxNQUFqQixFQUF5QjtBQUM5QmhiLG9CQUFRLENBQUNtVSxLQUFULENBQWVsSyxJQUFmLENBQW9CLENBQUNoUixHQUFELEVBQU0sSUFBTixFQUFZOGhCLGNBQWMsQ0FBQzloQixHQUFELENBQWQsQ0FBb0IraEIsUUFBcEIsQ0FBWixDQUFwQjtBQUNELFdBRk0sTUFFQSxJQUFJQSxRQUFRLEtBQUssT0FBakIsRUFBMEI7QUFDL0Isa0JBQU1DLEtBQUssR0FBR0YsY0FBYyxDQUFDOWhCLEdBQUQsQ0FBZCxDQUFvQitoQixRQUFwQixDQUFkO0FBQ0FoYixvQkFBUSxDQUFDbVUsS0FBVCxDQUFlbEssSUFBZixDQUFvQixDQUFDaFIsR0FBRCxFQUFNLElBQU4sRUFBWWdpQixLQUFLLENBQUNDLEtBQWxCLENBQXBCO0FBQ0FsYixvQkFBUSxDQUFDbVUsS0FBVCxDQUFlbEssSUFBZixDQUFvQixDQUFDaFIsR0FBRCxFQUFNLElBQU4sRUFBWWdpQixLQUFLLENBQUNsYyxHQUFsQixDQUFwQjtBQUNELFdBSk0sTUFJQTtBQUNMaUIsb0JBQVEsQ0FBQ21VLEtBQVQsQ0FBZWxLLElBQWYsQ0FBb0IsQ0FBQ2hSLEdBQUQsRUFBTStoQixRQUFOLEVBQWdCRCxjQUFjLENBQUM5aEIsR0FBRCxDQUFkLENBQW9CK2hCLFFBQXBCLENBQWhCLENBQXBCO0FBQ0Q7QUFDRjtBQUNGO0FBQ0Y7QUFDRjs7QUFFRCxTQUFPaGIsUUFBUDtBQUNELENBMUNNLEM7Ozs7Ozs7Ozs7OztBQ3pEUDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUVBLE1BQU1tYixLQUFLLEdBQUc7QUFDWkMsV0FBUyxFQUFFLGlCQURDO0FBRVpDLHNCQUFvQixFQUFFO0FBRlYsQ0FBZDtBQUtPLE1BQU1DLFVBQVUsR0FBRyxDQUFDQyxRQUFELEVBQVc5WCxNQUFYLEtBQXNCO0FBQzlDLFFBQU1VLElBQUksR0FBRyxJQUFJM0QsSUFBSixDQUFTK2EsUUFBVCxDQUFiO0FBQ0EsUUFBTUMsR0FBRyxHQUFHclgsSUFBSSxDQUFDc1gsT0FBTCxFQUFaO0FBQ0EsUUFBTUMsS0FBSyxHQUFHdlgsSUFBSSxDQUFDd1gsUUFBTCxLQUFrQixDQUFoQztBQUNBLFFBQU01RSxJQUFJLEdBQUc1UyxJQUFJLENBQUN5WCxXQUFMLEVBQWI7QUFDQSxRQUFNNWIsUUFBUSxHQUFHeUQsTUFBTSxDQUNwQmdOLFdBRGMsR0FFZC9CLE9BRmMsQ0FFTixJQUFJbU4sTUFBSixDQUFXLElBQVgsRUFBaUIsR0FBakIsQ0FGTSxFQUVpQkwsR0FBRyxDQUFDdEosUUFBSixHQUFlNEosUUFBZixDQUF3QixDQUF4QixFQUEyQixHQUEzQixDQUZqQixFQUdkcE4sT0FIYyxDQUdOLElBQUltTixNQUFKLENBQVcsSUFBWCxFQUFpQixHQUFqQixDQUhNLEVBR2lCSCxLQUFLLENBQUN4SixRQUFOLEdBQWlCNEosUUFBakIsQ0FBMEIsQ0FBMUIsRUFBNkIsR0FBN0IsQ0FIakIsRUFJZHBOLE9BSmMsQ0FJTixJQUFJbU4sTUFBSixDQUFXLE1BQVgsRUFBbUIsR0FBbkIsQ0FKTSxFQUltQjlFLElBSm5CLENBQWpCO0FBTUEsU0FBTy9XLFFBQVA7QUFDRCxDQVpNO0FBY0EsTUFBTStiLGFBQWEsR0FBRyxDQUFDM1MsU0FBRCxFQUFZcFEsT0FBWixLQUF3QjtBQUNuRCxNQUFJZ0gsUUFBUSxHQUFHLEVBQWY7QUFFQUEsVUFBUSxHQUFHb0osU0FBUyxDQUFDakgsTUFBVixDQUFpQixDQUFDQyxLQUFELEVBQVE0WixJQUFSLEtBQWlCO0FBQzNDaGpCLFdBQU8sQ0FBQ3FoQixPQUFSLENBQWdCaEYsTUFBTSxJQUFJO0FBQ3hCalQsV0FBSyxDQUFDNFosSUFBRCxDQUFMLEdBQWM1WixLQUFLLENBQUM0WixJQUFELENBQUwsSUFBZSxFQUE3QjtBQUNBNVosV0FBSyxDQUFDNFosSUFBRCxDQUFMLENBQVkzRyxNQUFNLENBQUM0RyxJQUFuQixJQUEyQjVHLE1BQU0sQ0FBQ2xNLEtBQVAsQ0FBYTZTLElBQWIsQ0FBM0I7QUFDRCxLQUhEO0FBS0EsV0FBTzVaLEtBQVA7QUFDRCxHQVBVLEVBT1IsRUFQUSxDQUFYO0FBU0EsU0FBT3BDLFFBQVA7QUFDRCxDQWJNO0FBZUEsTUFBTWtjLFdBQVcsR0FBRyxDQUFDL1MsS0FBRCxFQUFRZ1QsU0FBUyxHQUFHLEdBQXBCLEtBQTRCO0FBQ3JELFFBQU1DLFlBQVksR0FBR2pULEtBQUssR0FBRyxDQUFSLEdBQVksR0FBWixHQUFrQixFQUF2QztBQUNBLE1BQUlrVCxDQUFDLEdBQUdDLElBQUksQ0FBQ0MsS0FBTCxDQUFXcFQsS0FBWCxFQUFrQitJLFFBQWxCLEVBQVI7QUFDQSxNQUFJc0ssQ0FBQyxHQUFHSCxDQUFDLENBQUN4ZixNQUFGLEdBQVcsQ0FBWCxHQUFld2YsQ0FBQyxDQUFDeGYsTUFBRixHQUFXLENBQTFCLEdBQThCLENBQXRDO0FBRUEsU0FDRXVmLFlBQVksSUFDWEksQ0FBQyxHQUFHSCxDQUFDLENBQUNJLE1BQUYsQ0FBUyxDQUFULEVBQVlELENBQVosSUFBaUJMLFNBQXBCLEdBQWdDLEVBRHRCLENBQVosR0FFQUUsQ0FBQyxDQUFDSSxNQUFGLENBQVNELENBQVQsRUFBWTlOLE9BQVosQ0FBb0IsZ0JBQXBCLEVBQXNDLE9BQU95TixTQUE3QyxDQUhGO0FBS0QsQ0FWTTtBQVlBLE1BQU14YSxXQUFXLEdBQUcsT0FBT3JKLE1BQVAsRUFBZXNKLEtBQWYsRUFBc0JDLEtBQXRCLEVBQTZCL0IsRUFBN0IsRUFBaUM0YyxPQUFqQyxLQUE2QztBQUN0RSxNQUFJMWMsUUFBUSxHQUFHNkIsS0FBSyxDQUFDaEMsR0FBTixDQUFVc2IsS0FBSyxDQUFDQyxTQUFoQixDQUFmOztBQUVBLE1BQUksQ0FBQ3BiLFFBQUwsRUFBZTtBQUNiLFFBQUkyYyxVQUFVLEdBQUcsTUFBTS9hLEtBQUssQ0FBQy9CLEdBQU4sQ0FDcEIsR0FBRTZjLE9BQU8sQ0FBQ3RMLEdBQUksaUZBRE0sQ0FBdkI7QUFHQXVMLGNBQVUsR0FBR0EsVUFBVSxDQUFDcmlCLElBQXhCO0FBQ0EwRixZQUFRLEdBQUcsRUFBWCxDQUxhLENBT2I7O0FBQ0FBLFlBQVEsQ0FBQzBZLFlBQVQsR0FBd0JpRSxVQUFVLENBQy9CeGEsTUFEcUIsQ0FDZCxDQUFDQyxLQUFELEVBQVFyRixJQUFSLEtBQWlCO0FBQ3ZCcUYsV0FBSyxHQUFHQSxLQUFLLENBQUN6RCxNQUFOLENBQWE1QixJQUFJLENBQUMyYixZQUFMLENBQ2xCNWIsTUFEa0IsQ0FDWDhmLFdBQVcsSUFBSUEsV0FESixFQUVsQjNULEdBRmtCLENBRWQyVCxXQUFXLEtBQUs7QUFBRTFULGFBQUssRUFBRyxJQUFHMFQsV0FBWSxFQUF6QjtBQUE0QnpULGFBQUssRUFBRyxJQUFHeVQsV0FBWTtBQUFuRCxPQUFMLENBRkcsQ0FBYixDQUFSO0FBS0EsYUFBT3hhLEtBQVA7QUFDRCxLQVJxQixFQVFuQixFQVJtQixFQVNyQnRGLE1BVHFCLENBU2QsQ0FBQ0MsSUFBRCxFQUFPZ08sS0FBUCxFQUFjOFIsS0FBZCxLQUF3QkEsS0FBSyxDQUFDQyxTQUFOLENBQWdCdFMsR0FBRyxJQUFJQSxHQUFHLENBQUNyQixLQUFKLEtBQWNwTSxJQUFJLENBQUNvTSxLQUExQyxNQUFxRDRCLEtBVC9ELEVBVXJCSixJQVZxQixDQVVoQixDQUFDQyxDQUFELEVBQUlDLENBQUosS0FBVUQsQ0FBQyxDQUFDMUIsS0FBRixDQUFRNlQsYUFBUixDQUFzQmxTLENBQUMsQ0FBQzNCLEtBQXhCLENBVk0sQ0FBeEIsQ0FSYSxDQW9CYjs7QUFDQWxKLFlBQVEsQ0FBQzJZLFNBQVQsR0FBcUJnRSxVQUFVLENBQzVCeGEsTUFEa0IsQ0FDWCxDQUFDQyxLQUFELEVBQVFyRixJQUFSLEtBQWlCO0FBQ3ZCcUYsV0FBSyxHQUFHQSxLQUFLLENBQUN6RCxNQUFOLENBQWE7QUFBRXVLLGFBQUssRUFBRW5NLElBQUksQ0FBQ2tmLElBQWQ7QUFBb0I5UyxhQUFLLEVBQUVwTSxJQUFJLENBQUNpZ0IsVUFBTCxDQUFnQnZNLFdBQWhCO0FBQTNCLE9BQWIsQ0FBUjtBQUVBLGFBQU9yTyxLQUFQO0FBQ0QsS0FMa0IsRUFLaEIsRUFMZ0IsRUFNbEJ1SSxJQU5rQixDQU1iLENBQUNDLENBQUQsRUFBSUMsQ0FBSixLQUFVRCxDQUFDLENBQUMxQixLQUFGLENBQVE2VCxhQUFSLENBQXNCbFMsQ0FBQyxDQUFDM0IsS0FBeEIsQ0FORyxDQUFyQixDQXJCYSxDQTZCYjs7QUFDQWxKLFlBQVEsQ0FBQzRZLFVBQVQsR0FBc0IrRCxVQUFVLENBQzdCeGEsTUFEbUIsQ0FDWixDQUFDQyxLQUFELEVBQVFyRixJQUFSLEtBQWlCO0FBQ3ZCcUYsV0FBSyxHQUFHQSxLQUFLLENBQUN6RCxNQUFOLENBQWE1QixJQUFJLENBQUM2YixVQUFMLENBQ2xCOWIsTUFEa0IsQ0FDWG1nQixZQUFZLElBQUlBLFlBQVksQ0FBQ0MsSUFBYixJQUFxQkQsWUFBWSxDQUFDQyxJQUFiLEtBQXNCLFFBRGhELEVBRWxCalUsR0FGa0IsQ0FFZGdVLFlBQVksS0FBSztBQUFFL1QsYUFBSyxFQUFFK1QsWUFBWSxDQUFDQyxJQUFiLENBQWtCQyxXQUFsQixFQUFUO0FBQTBDaFUsYUFBSyxFQUFFOFQsWUFBWSxDQUFDQyxJQUFiLENBQWtCek0sV0FBbEI7QUFBakQsT0FBTCxDQUZFLENBQWIsQ0FBUjtBQUtBLGFBQU9yTyxLQUFQO0FBQ0QsS0FSbUIsRUFRakIsRUFSaUIsRUFTbkJ0RixNQVRtQixDQVNaLENBQUNDLElBQUQsRUFBT2dPLEtBQVAsRUFBYzhSLEtBQWQsS0FBd0JBLEtBQUssQ0FBQ0MsU0FBTixDQUFnQnRTLEdBQUcsSUFBSUEsR0FBRyxDQUFDckIsS0FBSixLQUFjcE0sSUFBSSxDQUFDb00sS0FBMUMsTUFBcUQ0QixLQVRqRSxFQVVuQkosSUFWbUIsQ0FVZCxDQUFDQyxDQUFELEVBQUlDLENBQUosS0FBVUQsQ0FBQyxDQUFDMUIsS0FBRixDQUFRNlQsYUFBUixDQUFzQmxTLENBQUMsQ0FBQzNCLEtBQXhCLENBVkksQ0FBdEIsQ0E5QmEsQ0EwQ2I7O0FBQ0FsSixZQUFRLENBQUNvSixTQUFULEdBQXFCdVQsVUFBVSxDQUM1QnhhLE1BRGtCLENBQ1gsQ0FBQ0MsS0FBRCxFQUFRckYsSUFBUixLQUFpQjtBQUN2QnFGLFdBQUssR0FBR0EsS0FBSyxDQUFDekQsTUFBTixDQUFhNUIsSUFBSSxDQUFDcU0sU0FBTCxDQUNsQnRNLE1BRGtCLENBQ1hzZ0IsWUFBWSxJQUFJQSxZQUFZLENBQUNDLFFBRGxCLEVBRWxCcFUsR0FGa0IsQ0FFZG1VLFlBQVksS0FBSztBQUFFbFUsYUFBSyxFQUFFa1UsWUFBWSxDQUFDbkIsSUFBdEI7QUFBNEI5UyxhQUFLLEVBQUVpVSxZQUFZLENBQUNDO0FBQWhELE9BQUwsQ0FGRSxDQUFiLENBQVI7QUFLQSxhQUFPamIsS0FBUDtBQUNELEtBUmtCLEVBUWhCLEVBUmdCLEVBU2xCdEYsTUFUa0IsQ0FTWCxDQUFDQyxJQUFELEVBQU9nTyxLQUFQLEVBQWM4UixLQUFkLEtBQXdCQSxLQUFLLENBQUNDLFNBQU4sQ0FBZ0J0UyxHQUFHLElBQUlBLEdBQUcsQ0FBQ3JCLEtBQUosS0FBY3BNLElBQUksQ0FBQ29NLEtBQTFDLE1BQXFENEIsS0FUbEUsRUFVbEJKLElBVmtCLENBVWIsQ0FBQ0MsQ0FBRCxFQUFJQyxDQUFKLEtBQVVELENBQUMsQ0FBQzFCLEtBQUYsQ0FBUTZULGFBQVIsQ0FBc0JsUyxDQUFDLENBQUMzQixLQUF4QixDQVZHLENBQXJCLENBM0NhLENBdURiOztBQUNBLFVBQU1sUSxPQUFPLEdBQUcsTUFBTWlILGtFQUFVLENBQUNILEVBQUQsRUFBSyxTQUFMLEVBQWdCLEVBQWhCLENBQWhDO0FBQ0EsVUFBTXdkLGdCQUFnQixHQUFHdkIsYUFBYSxDQUFDempCLE1BQU0sQ0FBQ3FILFlBQVIsRUFBc0IzRyxPQUF0QixDQUF0QztBQUNBZ0gsWUFBUSxDQUFDaEgsT0FBVCxHQUFtQnNrQixnQkFBbkI7QUFFQXpiLFNBQUssQ0FBQzBiLEdBQU4sQ0FBVXBDLEtBQUssQ0FBQ0MsU0FBaEIsRUFBMkJwYixRQUEzQjtBQUNEOztBQUVELFNBQU9BLFFBQVA7QUFDRCxDQW5FTTtBQXFFQSxNQUFNeEcsU0FBUyxHQUFHLENBQUNMLFlBQVksR0FBRyxFQUFoQixFQUFvQmtjLE1BQU0sR0FBRyxFQUE3QixFQUFpQ2pWLE1BQU0sR0FBRyxFQUExQyxLQUFpRDtBQUN4RSxRQUFNb2QsV0FBVyxHQUFHcmtCLFlBQVksQ0FBQ2tjLE1BQUQsQ0FBWixJQUF3QixFQUE1QztBQUNBLFFBQU1yVixRQUFRLEdBQUd5ZCxrRUFBYyxDQUFDRCxXQUFELEVBQWNwZCxNQUFkLENBQWQsSUFBdUNpVixNQUF4RDtBQUVBLFNBQU9yVixRQUFQO0FBQ0QsQ0FMTSxDOzs7Ozs7Ozs7Ozs7QUN0SFA7QUFBQTtBQUFBO0FBQUE7QUFFTyxNQUFNckcsZ0JBQWdCLEdBQUcsT0FBT3JCLE1BQVAsRUFBZW9sQixZQUFmLEVBQTZCdGtCLFNBQTdCLEtBQTJDO0FBQ3pFLE1BQUk7QUFDRixVQUFNO0FBQUVYLGNBQVEsRUFBRUM7QUFBWixRQUErQkosTUFBckM7QUFDQW9sQixnQkFBWSxDQUFDQyxTQUFiLENBQXVCamxCLGNBQWMsQ0FBQ08sR0FBdEM7QUFFQSxVQUFNeWtCLFlBQVksQ0FBQzdhLElBQWIsQ0FBa0I7QUFDdEIrYSxVQUFJLEVBQUUsa0JBRGdCO0FBRXRCdmtCLFFBQUUsRUFBRUQsU0FBUyxDQUFDQyxFQUZRO0FBR3RCd2tCLGdCQUFVLEVBQUVubEIsY0FBYyxDQUFDb2xCLGVBSEw7QUFJdEIsK0JBQXlCO0FBQ3ZCQyxjQUFNLEVBQUV2a0IsNkRBQVMsQ0FBQ2xCLE1BQU0sQ0FBQ2EsWUFBUixFQUFzQixRQUF0QixDQURNO0FBRXZCSSxlQUFPLEVBQUVILFNBQVMsQ0FBQ0csT0FGSTtBQUd2QkUsZUFBTyxFQUFFTCxTQUFTLENBQUNLO0FBSEk7QUFKSCxLQUFsQixDQUFOO0FBVUQsR0FkRCxDQWNFLE9BQU9LLEtBQVAsRUFBYztBQUNkRCxXQUFPLENBQUN3SSxJQUFSLENBQWEseUNBQWIsRUFBd0RqSixTQUF4RCxFQUFtRVUsS0FBbkU7QUFDRDtBQUNGLENBbEJNLEM7Ozs7Ozs7Ozs7OztBQ0ZQO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFPLE1BQU00RixlQUFlLEdBQUcsQ0FBQ0gsWUFBRCxFQUFlSSxZQUFmLEtBQWdDO0FBQzdELFNBQU8sQ0FDTDtBQUNFdUQsWUFBUSxFQUFFLFFBRFo7QUFFRUMsUUFBSSxFQUFFLFNBRlI7QUFHRUMsU0FBSyxFQUFFLENBQUM7QUFBRUMsY0FBUSxFQUFFO0FBQVosS0FBRDtBQUhULEdBREssRUFNTDtBQUNFSCxZQUFRLEVBQUUsV0FEWjtBQUVFQyxRQUFJLEVBQUUsUUFGUjtBQUdFQyxTQUFLLEVBQUUsQ0FBQztBQUFFQyxjQUFRLEVBQUU7QUFBWixLQUFEO0FBSFQsR0FOSyxFQVdMO0FBQ0VILFlBQVEsRUFBRSxNQURaO0FBRUVDLFFBQUksRUFBRSxRQUZSO0FBR0VDLFNBQUssRUFBRSxDQUFDO0FBQUVDLGNBQVEsRUFBRTtBQUFaLEtBQUQ7QUFIVCxHQVhLLEVBZ0JMO0FBQ0VILFlBQVEsRUFBRSxPQURaO0FBRUVDLFFBQUksRUFBRSxRQUZSO0FBR0VDLFNBQUssRUFBRSxDQUFDO0FBQUVDLGNBQVEsRUFBRTtBQUFaLEtBQUQ7QUFIVCxHQWhCSyxFQXFCTDtBQUNFSCxZQUFRLEVBQUUsUUFEWjtBQUVFQyxRQUFJLEVBQUUsUUFGUjtBQUdFQyxTQUFLLEVBQUUsQ0FBQztBQUFFNGEsaUJBQVcsRUFBRXplLFlBQVksQ0FBQ21CO0FBQTVCLEtBQUQ7QUFIVCxHQXJCSyxFQTBCTDtBQUNFd0MsWUFBUSxFQUFFLFdBRFo7QUFFRUMsUUFBSSxFQUFFLFFBRlI7QUFHRUMsU0FBSyxFQUFFLENBQUM7QUFBRUssWUFBTSxFQUFFO0FBQVYsS0FBRDtBQUhULEdBMUJLLEVBK0JMO0FBQ0VQLFlBQVEsRUFBRSxXQURaO0FBRUVDLFFBQUksRUFBRTtBQUZSLEdBL0JLLEVBbUNMO0FBQ0VELFlBQVEsRUFBRSxPQURaO0FBRUVDLFFBQUksRUFBRSxRQUZSO0FBR0VDLFNBQUssRUFBRSxDQUFDO0FBQUVDLGNBQVEsRUFBRTtBQUFaLEtBQUQsQ0FIVDtBQUlFNGEsZUFBVyxFQUFFdGUsWUFBWSxDQUFDc0osR0FBYixDQUFpQmhRLEdBQUcsSUFBSTtBQUNuQyxhQUFPO0FBQ0xpSyxnQkFBUSxFQUFFakssR0FETDtBQUVMa0ssWUFBSSxFQUFFO0FBRkQsT0FBUDtBQUlELEtBTFk7QUFKZixHQW5DSyxDQUFQO0FBK0NELENBaERNO0FBa0RBLE1BQU0xQixxQkFBcUIsR0FBRyxNQUFNO0FBQ3pDLFNBQU8sQ0FDTDtBQUNFeUIsWUFBUSxFQUFFLGFBRFo7QUFFRUMsUUFBSSxFQUFFLFNBRlI7QUFHRUMsU0FBSyxFQUFFLENBQUM7QUFBRUMsY0FBUSxFQUFFO0FBQVosS0FBRDtBQUhULEdBREssRUFNTDtBQUNFSCxZQUFRLEVBQUUsb0JBRFo7QUFFRUMsUUFBSSxFQUFFLFFBRlI7QUFHRUMsU0FBSyxFQUFFLENBQUM7QUFBRUMsY0FBUSxFQUFFO0FBQVosS0FBRDtBQUhULEdBTkssRUFXTDtBQUNFSCxZQUFRLEVBQUUsYUFEWjtBQUVFQyxRQUFJLEVBQUUsUUFGUjtBQUdFQyxTQUFLLEVBQUUsQ0FBQztBQUFFQyxjQUFRLEVBQUU7QUFBWixLQUFEO0FBSFQsR0FYSyxDQUFQO0FBaUJELENBbEJNO0FBb0JBLE1BQU12QixjQUFjLEdBQUluQyxZQUFELElBQWtCO0FBQzlDLFNBQU8sQ0FDTDtBQUNFdUQsWUFBUSxFQUFFLFFBRFo7QUFFRUMsUUFBSSxFQUFFLFNBRlI7QUFHRUMsU0FBSyxFQUFFLENBQUM7QUFBRUMsY0FBUSxFQUFFO0FBQVosS0FBRDtBQUhULEdBREssRUFNTDtBQUNFSCxZQUFRLEVBQUUsV0FEWjtBQUVFQyxRQUFJLEVBQUUsUUFGUjtBQUdFQyxTQUFLLEVBQUUsQ0FBQztBQUFFQyxjQUFRLEVBQUU7QUFBWixLQUFEO0FBSFQsR0FOSyxFQVdMO0FBQ0VILFlBQVEsRUFBRSxNQURaO0FBRUVDLFFBQUksRUFBRSxRQUZSO0FBR0VDLFNBQUssRUFBRSxDQUFDO0FBQUVDLGNBQVEsRUFBRTtBQUFaLEtBQUQ7QUFIVCxHQVhLLEVBZ0JMO0FBQ0VILFlBQVEsRUFBRSxXQURaO0FBRUVDLFFBQUksRUFBRTtBQUZSLEdBaEJLLEVBb0JMO0FBQ0VELFlBQVEsRUFBRSxPQURaO0FBRUVDLFFBQUksRUFBRSxRQUZSO0FBR0VDLFNBQUssRUFBRSxDQUFDO0FBQUVDLGNBQVEsRUFBRTtBQUFaLEtBQUQsQ0FIVDtBQUlFNGEsZUFBVyxFQUFFdGUsWUFBWSxDQUFDc0osR0FBYixDQUFpQmhRLEdBQUcsSUFBSTtBQUNuQyxhQUFPO0FBQ0xpSyxnQkFBUSxFQUFFakssR0FETDtBQUVMa0ssWUFBSSxFQUFFO0FBRkQsT0FBUDtBQUlELEtBTFk7QUFKZixHQXBCSyxDQUFQO0FBZ0NELENBakNNO0FBbUNBLE1BQU1aLFlBQVksR0FBRyxDQUFDUyxRQUFELEVBQVdoQixrQkFBWCxLQUFrQztBQUM1RCxTQUFPLENBQ0w7QUFDRWtCLFlBQVEsRUFBRSxRQURaO0FBRUVDLFFBQUksRUFBRSxTQUZSO0FBR0VDLFNBQUssRUFBRSxDQUFDO0FBQUVDLGNBQVEsRUFBRTtBQUFaLEtBQUQ7QUFIVCxHQURLLEVBTUw7QUFDRUgsWUFBUSxFQUFFLFdBRFo7QUFFRUMsUUFBSSxFQUFFLFFBRlI7QUFHRUMsU0FBSyxFQUFFLENBQUM7QUFBRUMsY0FBUSxFQUFFO0FBQVosS0FBRDtBQUhULEdBTkssRUFXTDtBQUNFSCxZQUFRLEVBQUUscUJBRFo7QUFFRUMsUUFBSSxFQUFFO0FBRlIsR0FYSyxFQWVMO0FBQ0VELFlBQVEsRUFBRSxhQURaO0FBRUVDLFFBQUksRUFBRTtBQUZSLEdBZkssRUFtQkw7QUFDRUQsWUFBUSxFQUFFLE9BRFo7QUFFRUMsUUFBSSxFQUFFLFFBRlI7QUFHRUMsU0FBSyxFQUFFLENBQUM7QUFBRUMsY0FBUSxFQUFFO0FBQVosS0FBRCxFQUFxQjtBQUFFSSxZQUFNLEVBQUU7QUFBVixLQUFyQjtBQUhULEdBbkJLLEVBd0JMO0FBQ0VQLFlBQVEsRUFBRSxVQURaO0FBRUVDLFFBQUksRUFBRSxRQUZSO0FBR0VDLFNBQUssRUFBRSxDQUFDO0FBQUVDLGNBQVEsRUFBRXJCO0FBQVosS0FBRCxFQUFtQztBQUFFc0IsZUFBUyxFQUFFO0FBQWIsS0FBbkM7QUFIVCxHQXhCSyxFQTZCTDtBQUNFSixZQUFRLEVBQUUsc0JBRFo7QUFFRUMsUUFBSSxFQUFFLFFBRlI7QUFHRUMsU0FBSyxFQUFFLENBQUM7QUFBRUMsY0FBUSxFQUFFckI7QUFBWixLQUFELEVBQW1DO0FBQUVrYyxhQUFPLEVBQUVsYixRQUFRLENBQUNRO0FBQXBCLEtBQW5DO0FBSFQsR0E3QkssRUFrQ0w7QUFDRU4sWUFBUSxFQUFFLGFBRFo7QUFFRUMsUUFBSSxFQUFFLFFBRlI7QUFHRThhLGVBQVcsRUFBRSxDQUNYO0FBQ0UvYSxjQUFRLEVBQUUsVUFEWjtBQUVFQyxVQUFJLEVBQUU7QUFGUixLQURXLEVBS1g7QUFDRUQsY0FBUSxFQUFFLFlBRFo7QUFFRUMsVUFBSSxFQUFFO0FBRlIsS0FMVyxFQVNYO0FBQ0VELGNBQVEsRUFBRSxVQURaO0FBRUVDLFVBQUksRUFBRTtBQUZSLEtBVFc7QUFIZixHQWxDSyxFQW9ETDtBQUNFRCxZQUFRLEVBQUUsV0FEWjtBQUVFQyxRQUFJLEVBQUU7QUFGUixHQXBESyxDQUFQO0FBeURELENBMURNO0FBNERBLE1BQU1sQixjQUFjLEdBQUcsTUFBTTtBQUNsQyxTQUFPLENBQ0w7QUFDRWlCLFlBQVEsRUFBRSxRQURaO0FBRUVDLFFBQUksRUFBRSxTQUZSO0FBR0VDLFNBQUssRUFBRSxDQUFDO0FBQUVDLGNBQVEsRUFBRTtBQUFaLEtBQUQsRUFBc0I7QUFBRThhLGFBQU8sRUFBRTtBQUFYLEtBQXRCO0FBSFQsR0FESyxFQU1MO0FBQ0VqYixZQUFRLEVBQUUsV0FEWjtBQUVFQyxRQUFJLEVBQUUsUUFGUjtBQUdFQyxTQUFLLEVBQUUsQ0FBQztBQUFFQyxjQUFRLEVBQUU7QUFBWixLQUFEO0FBSFQsR0FOSyxFQVdMO0FBQ0VILFlBQVEsRUFBRSxPQURaO0FBRUVDLFFBQUksRUFBRSxRQUZSO0FBR0VDLFNBQUssRUFBRSxDQUFDO0FBQUVDLGNBQVEsRUFBRTtBQUFaLEtBQUQ7QUFIVCxHQVhLLEVBZ0JMO0FBQ0VILFlBQVEsRUFBRSxXQURaO0FBRUVDLFFBQUksRUFBRSxRQUZSO0FBR0VDLFNBQUssRUFBRSxDQUFDO0FBQUVDLGNBQVEsRUFBRTtBQUFaLEtBQUQ7QUFIVCxHQWhCSyxFQXFCTDtBQUNFSCxZQUFRLEVBQUUsT0FEWjtBQUVFQyxRQUFJLEVBQUUsUUFGUjtBQUdFQyxTQUFLLEVBQUUsQ0FBQztBQUFFQyxjQUFRLEVBQUU7QUFBWixLQUFELEVBQXFCO0FBQUVJLFlBQU0sRUFBRTtBQUFWLEtBQXJCO0FBSFQsR0FyQkssRUEwQkw7QUFDRVAsWUFBUSxFQUFFLE9BRFo7QUFFRUMsUUFBSSxFQUFFLFFBRlI7QUFHRUMsU0FBSyxFQUFFLENBQUM7QUFBRUMsY0FBUSxFQUFFO0FBQVosS0FBRCxFQUFxQjtBQUFFQyxlQUFTLEVBQUU7QUFBYixLQUFyQjtBQUhULEdBMUJLLEVBK0JMO0FBQ0VKLFlBQVEsRUFBRSxZQURaO0FBRUVDLFFBQUksRUFBRSxRQUZSO0FBR0VDLFNBQUssRUFBRSxDQUFDO0FBQUVDLGNBQVEsRUFBRTtBQUFaLEtBQUQ7QUFIVCxHQS9CSyxFQW9DTDtBQUNFSCxZQUFRLEVBQUUsTUFEWjtBQUVFQyxRQUFJLEVBQUUsUUFGUjtBQUdFQyxTQUFLLEVBQUUsQ0FBQztBQUFFQyxjQUFRLEVBQUU7QUFBWixLQUFEO0FBSFQsR0FwQ0ssRUF5Q0w7QUFDRUgsWUFBUSxFQUFFLFNBRFo7QUFFRUMsUUFBSSxFQUFFO0FBRlIsR0F6Q0ssRUE2Q0w7QUFDRUQsWUFBUSxFQUFFLGFBRFo7QUFFRUMsUUFBSSxFQUFFLFFBRlI7QUFHRUMsU0FBSyxFQUFFLENBQUM7QUFBRUMsY0FBUSxFQUFFO0FBQVosS0FBRDtBQUhULEdBN0NLLEVBa0RMO0FBQ0VILFlBQVEsRUFBRSxnQkFEWjtBQUVFQyxRQUFJLEVBQUU7QUFGUixHQWxESyxDQUFQO0FBdURELENBeERNLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNyS0EsTUFBTWliLFdBQVcsR0FBRyxDQUFDOWpCLElBQUQsRUFBTzBELFFBQVAsS0FBb0I7QUFDN0MsTUFBSTRNLENBQUMsR0FBRzVRLFFBQVEsQ0FBQ3FrQixhQUFULENBQXVCLEdBQXZCLENBQVI7QUFFQXJrQixVQUFRLENBQUM4RCxJQUFULENBQWN3Z0IsV0FBZCxDQUEwQjFULENBQTFCO0FBQ0FBLEdBQUMsQ0FBQzRCLEtBQUYsR0FBVSxlQUFWO0FBQ0E1QixHQUFDLENBQUN0RixJQUFGLEdBQVNpWixHQUFHLENBQUNDLGVBQUosQ0FDUCxJQUFJQyxJQUFKLENBQVMsQ0FBQ25rQixJQUFELENBQVQsRUFBaUI7QUFDZjZJLFFBQUksRUFBRTtBQURTLEdBQWpCLENBRE8sQ0FBVDtBQUtBeUgsR0FBQyxDQUFDOFQsUUFBRixHQUFhMWdCLFFBQWI7QUFDQTRNLEdBQUMsQ0FBQytULEtBQUY7QUFDQS9ULEdBQUMsQ0FBQ2dVLE1BQUY7QUFDQXhaLFFBQU0sQ0FBQ21aLEdBQVAsQ0FBV00sZUFBWCxDQUEyQmpVLENBQUMsQ0FBQ3RGLElBQTdCO0FBQ0QsQ0FkTTtBQWdCQSxNQUFNMk4sb0JBQW9CLEdBQUcvUyxLQUFLLElBQUk7QUFDM0MsTUFBSTRlLFdBQVcsR0FBRyxFQUFsQjtBQUNBLE1BQUlDLFNBQVMsR0FBR3BpQixNQUFNLENBQUNDLElBQVAsQ0FBWXNELEtBQUssSUFBSSxFQUFyQixFQUF5QnBELE1BQXpCLENBQWdDN0QsR0FBRyxJQUFJaUgsS0FBSyxDQUFDakgsR0FBRCxDQUE1QyxDQUFoQjs7QUFFQSxPQUFLLE1BQU1BLEdBQVgsSUFBa0I4bEIsU0FBbEIsRUFBNkI7QUFDM0JELGVBQVcsR0FBR0EsV0FBVyxDQUFDbmdCLE1BQVosQ0FBb0IsR0FBRTFGLEdBQUksSUFBRytsQixJQUFJLENBQUNDLFNBQUwsQ0FBZS9lLEtBQUssQ0FBQ2pILEdBQUQsQ0FBcEIsQ0FBMkIsRUFBeEQsQ0FBZDtBQUNEOztBQUVELFNBQVEsSUFBRzZsQixXQUFXLENBQUN4Z0IsSUFBWixDQUFpQixHQUFqQixDQUFzQixFQUFqQztBQUNELENBVE07QUFXQSxNQUFNa1Esa0JBQWtCLEdBQUd0TyxLQUFLLElBQUk7QUFDekMsUUFBTWdmLFNBQVMsR0FBRyxJQUFJQyxlQUFKLENBQW9CamYsS0FBcEIsQ0FBbEI7QUFDQSxNQUFJa2YsVUFBVSxHQUFHLElBQWpCO0FBRUEsU0FBT0MsS0FBSyxDQUFDekIsSUFBTixDQUFXc0IsU0FBWCxFQUFzQi9jLE1BQXRCLENBQTZCLENBQUNDLEtBQUQsRUFBUXJGLElBQVIsS0FBaUI7QUFDbkQsUUFBSTtBQUNGcWlCLGdCQUFVLEdBQUdKLElBQUksQ0FBQ00sS0FBTCxDQUFXdmlCLElBQUksQ0FBQyxDQUFELENBQWYsQ0FBYjtBQUNELEtBRkQsQ0FFRSxPQUFPbkQsQ0FBUCxFQUFVO0FBQ1Z3bEIsZ0JBQVUsR0FBR3JpQixJQUFJLENBQUMsQ0FBRCxDQUFqQjtBQUNEOztBQUVEcUYsU0FBSyxxQkFBUUEsS0FBUjtBQUFlLE9BQUNyRixJQUFJLENBQUMsQ0FBRCxDQUFMLEdBQVdxaUI7QUFBMUIsTUFBTDtBQUNBLFdBQU9oZCxLQUFQO0FBQ0QsR0FUTSxFQVNKLEVBVEksQ0FBUDtBQVVELENBZE0sQzs7Ozs7Ozs7Ozs7O0FDM0JQO0FBQUE7QUFBQTtBQUFPLE1BQU1uRCxlQUFlLEdBQUduRixLQUFLLElBQUk7QUFDdEMsTUFBSWtHLFFBQVEsR0FBRztBQUNia2QsUUFBSSxFQUFFcGpCLEtBQUssQ0FBQ29qQixJQURDO0FBRWJ6akIsV0FBTyxFQUFFSyxLQUFLLENBQUNMLE9BRkY7QUFHYjhsQixTQUFLLEVBQUV6bEIsS0FBSyxDQUFDeWxCO0FBSEEsR0FBZjtBQU1BLFNBQU92ZixRQUFQO0FBQ0QsQ0FSTTtBQVVBLE1BQU1xTyxnQkFBZ0IsR0FBR3ZVLEtBQUssSUFBSTtBQUN2QyxNQUFJa0csUUFBUSxHQUFHO0FBQ2J2RyxXQUFPLEVBQUVLLEtBQUssQ0FBQ0w7QUFERixHQUFmO0FBSUF1RyxVQUFRLENBQUNKLE1BQVQsR0FBbUI5RixLQUFLLENBQUNrRyxRQUFOLElBQWtCbEcsS0FBSyxDQUFDa0csUUFBTixDQUFlMUYsSUFBakMsSUFBeUNSLEtBQUssQ0FBQ2tHLFFBQU4sQ0FBZTFGLElBQWYsQ0FBb0JzRixNQUE5RCxJQUF5RSxJQUEzRjtBQUNBSSxVQUFRLENBQUN2RyxPQUFULEdBQW9CSyxLQUFLLENBQUNrRyxRQUFOLElBQWtCbEcsS0FBSyxDQUFDa0csUUFBTixDQUFlMUYsSUFBakMsSUFBeUNSLEtBQUssQ0FBQ2tHLFFBQU4sQ0FBZTFGLElBQWYsQ0FBb0JiLE9BQTlELElBQTBFSyxLQUFLLENBQUNMLE9BQW5HO0FBRUEsU0FBT3VHLFFBQVA7QUFDRCxDQVRNLEM7Ozs7Ozs7Ozs7OztBQ1ZQO0FBQUE7QUFBQTtBQUFBO0FBQU8sTUFBTXlkLGNBQWMsR0FBRyxDQUFDK0IsUUFBRCxFQUFXbGxCLElBQVgsS0FBb0I7QUFDaEQsTUFBSTBGLFFBQVEsR0FBR3dmLFFBQWY7O0FBRUEsT0FBSyxNQUFNdm1CLEdBQVgsSUFBa0JxQixJQUFsQixFQUF3QjtBQUN0QjBGLFlBQVEsR0FBR0EsUUFBUSxDQUFDME8sT0FBVCxDQUFpQixJQUFJbU4sTUFBSixDQUFZLElBQUc1aUIsR0FBSSxHQUFuQixFQUF1QixHQUF2QixDQUFqQixFQUE4Q3FCLElBQUksQ0FBQ3JCLEdBQUQsQ0FBbEQsQ0FBWDtBQUNEOztBQUVELFNBQU8rRyxRQUFQO0FBQ0QsQ0FSTTtBQVVBLE1BQU15ZixJQUFJLEdBQUcsQ0FBQ3RXLEtBQUQsRUFBUTZSLFFBQVIsS0FBcUI7QUFDdkMsU0FBTyxDQUFDN1IsS0FBSyxJQUFJLEVBQVYsRUFBY3NILFdBQWQsR0FBNEJ6VCxRQUE1QixDQUFxQyxDQUFDZ2UsUUFBUSxJQUFJLEVBQWIsRUFBaUJ2SyxXQUFqQixFQUFyQyxDQUFQO0FBQ0QsQ0FGTTtBQUlBLE1BQU1pUCxLQUFLLEdBQUd2VyxLQUFLLElBQUk7QUFDNUIsTUFBSXdXLFVBQVUsR0FBR3hXLEtBQUssQ0FBQytJLFFBQU4sR0FBaUJ6QixXQUFqQixFQUFqQjtBQUVBa1AsWUFBVSxHQUFHQSxVQUFVLENBQUNqUixPQUFYLENBQW1CLFVBQW5CLEVBQStCLEVBQS9CLENBQWI7QUFDQWlSLFlBQVUsR0FBR0EsVUFBVSxDQUFDalIsT0FBWCxDQUFtQixLQUFuQixFQUEwQixHQUExQixDQUFiO0FBRUEsU0FBT2lSLFVBQVA7QUFDRCxDQVBNLEM7Ozs7Ozs7Ozs7OztBQ2RQO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtDQUVBOztBQUNBLE1BQU1DLGVBQWUsR0FBR3pXLEtBQUssSUFBSTtBQUMvQixNQUFJMlIsT0FBTyxHQUFHLEtBQWQ7O0FBRUEsVUFBUSxPQUFPM1IsS0FBZjtBQUNFLFNBQUssU0FBTDtBQUNFMlIsYUFBTyxHQUFHLENBQUMsQ0FBQyxJQUFELEVBQU8sS0FBUCxFQUFjOWQsUUFBZCxDQUF1Qm1NLEtBQXZCLENBQVg7QUFDQTs7QUFFRixTQUFLLFFBQUw7QUFDRTJSLGFBQU8sR0FBRyxDQUFDLElBQUQsRUFBTytFLFNBQVAsRUFBa0I3aUIsUUFBbEIsQ0FBMkJtTSxLQUEzQixDQUFWO0FBQ0E7O0FBRUYsU0FBSyxRQUFMO0FBQ0UsVUFBSUEsS0FBSyxLQUFLLElBQWQsRUFBb0I7QUFDbEIyUixlQUFPLEdBQUcsSUFBVjtBQUNELE9BRkQsTUFFTyxJQUFJdUUsS0FBSyxDQUFDUyxPQUFOLENBQWMzVyxLQUFkLENBQUosRUFBMEI7QUFDL0IyUixlQUFPLEdBQUczUixLQUFLLENBQUN0TSxNQUFOLEtBQWlCLENBQTNCO0FBQ0QsT0FGTSxNQUVBLElBQUksQ0FBQ3dpQixLQUFLLENBQUNTLE9BQU4sQ0FBYzNXLEtBQWQsQ0FBTCxFQUEyQjtBQUNoQzJSLGVBQU8sR0FBR25lLE1BQU0sQ0FBQ0MsSUFBUCxDQUFZdU0sS0FBWixFQUFtQnRNLE1BQW5CLEtBQThCLENBQXhDO0FBQ0Q7O0FBQ0Q7O0FBRUYsU0FBSyxRQUFMO0FBQ0VpZSxhQUFPLEdBQUczUixLQUFLLENBQUM0VyxJQUFOLEdBQWFsakIsTUFBYixLQUF3QixDQUFsQztBQUNBOztBQUVGO0FBQ0VpZSxhQUFPLEdBQUcsSUFBVjtBQUNBO0FBekJKOztBQTRCQSxTQUFPQSxPQUFQO0FBQ0QsQ0FoQ0Q7O0FBaUNBLE1BQU1rRixxQkFBcUIsR0FBRyxDQUFDMW5CLE1BQUQsRUFBUzJuQixZQUFULEtBQTBCO0FBQ3RELFFBQU07QUFBRUMsaUJBQUY7QUFBaUJDLGdCQUFqQjtBQUErQkM7QUFBL0IsTUFBaURILFlBQXZEO0FBQ0EsTUFBSW5tQixLQUFLLEdBQUcsRUFBWixDQUZzRCxDQUl0RDs7QUFDQSxNQUFJdW1CLGlCQUFpQixHQUFHaEIsS0FBSyxDQUFDUyxPQUFOLENBQWNNLGFBQWQsSUFDcEIsT0FEb0IsR0FFcEIsT0FBT0EsYUFGWDs7QUFJQSxNQUFJLENBQUNSLGVBQWUsQ0FBQ1EsYUFBRCxDQUFoQixJQUFtQ0MsaUJBQWlCLEtBQUtGLFlBQTdELEVBQTJFO0FBQ3pFcm1CLFNBQUssR0FBR04sNkRBQVMsQ0FBQ2xCLE1BQU0sQ0FBQ2EsWUFBUixFQUFzQixpQkFBdEIsRUFBeUM7QUFBRW1uQixjQUFRLEVBQUVIO0FBQVosS0FBekMsQ0FBakI7QUFDRCxHQVhxRCxDQWF0RDs7O0FBQ0EsUUFBTUksa0JBQWtCLEdBQUdMLGFBQWEsQ0FBQ2hpQixJQUFkLENBQ3pCc2lCLElBQUksSUFBSUEsSUFBSSxDQUFDdkUsSUFBTCxLQUFjLFVBQWQsSUFBNEJ1RSxJQUFJLENBQUNyWCxLQUFMLEtBQWUsSUFEMUIsQ0FBM0I7O0FBSUEsTUFBSXlXLGVBQWUsQ0FBQzlsQixLQUFELENBQWYsS0FBMkJ5bUIsa0JBQWtCLElBQUksQ0FBQ1gsZUFBZSxDQUFDUSxhQUFELENBQWpFLENBQUosRUFBdUY7QUFDckYsU0FBSyxNQUFNSSxJQUFYLElBQW1CTixhQUFuQixFQUFrQztBQUNoQyxVQUFJLENBQUNOLGVBQWUsQ0FBQzlsQixLQUFELENBQXBCLEVBQTZCO0FBQzNCO0FBQ0Q7O0FBRUQsY0FBUTBtQixJQUFJLENBQUN2RSxJQUFiO0FBQ0UsYUFBSyxVQUFMO0FBQ0UsY0FBSTJELGVBQWUsQ0FBQ1EsYUFBRCxDQUFuQixFQUFvQztBQUNsQ3RtQixpQkFBSyxHQUFHTiw2REFBUyxDQUFDbEIsTUFBTSxDQUFDYSxZQUFSLEVBQXNCLGlCQUF0QixDQUFqQjtBQUNEOztBQUNEOztBQUVGLGFBQUssYUFBTDtBQUNFLGNBQUksQ0FBQ3FuQixJQUFJLENBQUNyWCxLQUFMLENBQVdqTCxJQUFYLENBQWdCbkIsSUFBSSxJQUFJQSxJQUFJLENBQUNzRCxFQUFMLEtBQVkrZixhQUFwQyxDQUFMLEVBQXlEO0FBQ3ZEdG1CLGlCQUFLLEdBQUdOLDZEQUFTLENBQUNsQixNQUFNLENBQUNhLFlBQVIsRUFBc0IsbUJBQXRCLENBQWpCO0FBQ0Q7O0FBQ0Q7O0FBRUYsYUFBSyxZQUFMO0FBQ0UsY0FBSSxDQUFDcW5CLElBQUksQ0FBQ3JYLEtBQUwsQ0FBV25NLFFBQVgsQ0FBb0JvakIsYUFBYSxDQUFDM1AsV0FBZCxFQUFwQixDQUFMLEVBQXVEO0FBQ3JEM1csaUJBQUssR0FBR04sNkRBQVMsQ0FBQ2xCLE1BQU0sQ0FBQ2EsWUFBUixFQUFzQix1QkFBdEIsQ0FBakI7QUFDRDs7QUFDRDs7QUFFRixhQUFLLFNBQUw7QUFDRSxjQUFJcW5CLElBQUksQ0FBQ3JYLEtBQUwsS0FBZWlYLGFBQW5CLEVBQWtDO0FBQ2hDdG1CLGlCQUFLLEdBQUdOLDZEQUFTLENBQUNsQixNQUFNLENBQUNhLFlBQVIsRUFBc0IsVUFBdEIsQ0FBakI7QUFDRDs7QUFDRDs7QUFFRixhQUFLLFFBQUw7QUFDRSxjQUFJcW5CLElBQUksQ0FBQ3JYLEtBQUwsS0FBZSxPQUFmLElBQTBCLENBQUNzWCxPQUFPLENBQUNMLGFBQUQsQ0FBdEMsRUFBdUQ7QUFDckR0bUIsaUJBQUssR0FBR04sNkRBQVMsQ0FBQ2xCLE1BQU0sQ0FBQ2EsWUFBUixFQUFzQixpQkFBdEIsQ0FBakI7QUFDRDs7QUFDRCxjQUFJcW5CLElBQUksQ0FBQ3JYLEtBQUwsS0FBZSxLQUFmLElBQXdCLENBQUN1WCxLQUFLLENBQUNOLGFBQUQsQ0FBbEMsRUFBbUQ7QUFDakR0bUIsaUJBQUssR0FBR04sNkRBQVMsQ0FBQ2xCLE1BQU0sQ0FBQ2EsWUFBUixFQUFzQixpQkFBdEIsQ0FBakI7QUFDRDs7QUFDRDs7QUFFRixhQUFLLFNBQUw7QUFDRSxjQUFJLENBQUNxbkIsSUFBSSxDQUFDclgsS0FBTCxDQUFXbk0sUUFBWCxDQUFvQm9qQixhQUFwQixDQUFMLEVBQXlDO0FBQ3ZDdG1CLGlCQUFLLEdBQUdOLDZEQUFTLENBQUNsQixNQUFNLENBQUNhLFlBQVIsRUFBc0IsYUFBdEIsRUFBcUM7QUFBRXduQixvQkFBTSxFQUFFSCxJQUFJLENBQUNyWCxLQUFMLENBQVc3SyxJQUFYLENBQWdCLEdBQWhCO0FBQVYsYUFBckMsQ0FBakI7QUFDRDs7QUFDRDs7QUFFRixhQUFLLFdBQUw7QUFDRSxjQUFJOGhCLGFBQWEsQ0FBQ2xPLFFBQWQsR0FBeUJyVixNQUF6QixHQUFrQzJqQixJQUFJLENBQUNyWCxLQUEzQyxFQUFrRDtBQUNoRHJQLGlCQUFLLEdBQUdOLDZEQUFTLENBQUNsQixNQUFNLENBQUNhLFlBQVIsRUFBc0IsV0FBdEIsRUFBbUM7QUFBRXluQix1QkFBUyxFQUFFSixJQUFJLENBQUNyWDtBQUFsQixhQUFuQyxDQUFqQjtBQUNEOztBQUNEOztBQUVGLGFBQUssV0FBTDtBQUNFLGNBQUlpWCxhQUFhLENBQUNsTyxRQUFkLEdBQXlCclYsTUFBekIsR0FBa0MyakIsSUFBSSxDQUFDclgsS0FBM0MsRUFBa0Q7QUFDaERyUCxpQkFBSyxHQUFHTiw2REFBUyxDQUFDbEIsTUFBTSxDQUFDYSxZQUFSLEVBQXNCLFdBQXRCLEVBQW1DO0FBQUVtSyx1QkFBUyxFQUFFa2QsSUFBSSxDQUFDclg7QUFBbEIsYUFBbkMsQ0FBakI7QUFDRDs7QUFDRDtBQWxESjtBQW9ERDtBQUNGOztBQUVELFNBQU9yUCxLQUFQO0FBQ0QsQ0FoRkQsQyxDQWtGQTs7O0FBQ08sTUFBTTJtQixPQUFPLEdBQUd0WCxLQUFLLElBQUk7QUFDOUIsUUFBTTBYLFdBQVcsR0FBRyxpREFBcEI7QUFFQSxTQUFPQSxXQUFXLENBQUNDLElBQVosQ0FBaUIzWCxLQUFqQixDQUFQO0FBQ0QsQ0FKTTtBQU1BLE1BQU00WCxNQUFNLEdBQUc1WCxLQUFLLElBQUk7QUFDN0IsTUFBSWdDLEdBQUcsR0FBR25SLFFBQVEsQ0FBQ3FrQixhQUFULENBQXVCLEtBQXZCLENBQVY7QUFDQWxULEtBQUcsQ0FBQzZWLFNBQUosR0FBZ0I3WCxLQUFoQjs7QUFFQSxPQUFLLElBQUk4WCxDQUFDLEdBQUc5VixHQUFHLENBQUMrVixVQUFaLEVBQXdCN0UsQ0FBQyxHQUFHNEUsQ0FBQyxDQUFDcGtCLE1BQW5DLEVBQTJDd2YsQ0FBQyxFQUE1QyxHQUFrRDtBQUNoRCxRQUFJNEUsQ0FBQyxDQUFDNUUsQ0FBRCxDQUFELENBQUs4RSxRQUFMLEtBQWtCLENBQXRCLEVBQXlCO0FBQ3ZCLGFBQU8sSUFBUDtBQUNEO0FBQ0Y7O0FBRUQsU0FBTyxLQUFQO0FBQ0QsQ0FYTTtBQWFBLE1BQU1ULEtBQUssR0FBR3ZYLEtBQUssSUFBSTtBQUM1QixRQUFNaVksT0FBTyxHQUFHLElBQUl2RixNQUFKLENBQ2Qsc0JBQXNCO0FBQ3RCLHFEQURBLEdBQ3NEO0FBQ3RELCtCQUZBLEdBRWdDO0FBQ2hDLG1DQUhBLEdBR29DO0FBQ3BDLGdDQUpBLEdBSWlDO0FBQy9CLHNCQU5ZLEVBT2QsR0FQYyxDQUFoQjtBQVVBLFNBQU91RixPQUFPLENBQUNOLElBQVIsQ0FBYTNYLEtBQWIsQ0FBUDtBQUNELENBWk07QUFjQSxNQUFNMUosYUFBYSxHQUFHLENBQUNuSCxNQUFELEVBQVNnSCxLQUFULEVBQWdCMmUsV0FBVyxHQUFHLEVBQTlCLEtBQXFDO0FBQ2hFLE1BQUlyZSxNQUFNLEdBQUcsRUFBYjtBQUNBLE1BQUl0RixJQUFJLEdBQUcsRUFBWDtBQUNBLE1BQUk0SSxRQUFRLEdBQUcsSUFBZjtBQUNBLE1BQUlpZCxZQUFZLEdBQUcsSUFBbkI7QUFDQSxNQUFJRCxhQUFhLEdBQUcsRUFBcEI7QUFDQSxNQUFJRSxhQUFhLEdBQUcsSUFBcEI7O0FBRUEsT0FBSyxJQUFJaUIsVUFBVCxJQUF1QnBELFdBQXZCLEVBQW9DO0FBQ2xDL2EsWUFBUSxHQUFHbWUsVUFBVSxDQUFDbmUsUUFBdEI7QUFDQWlkLGdCQUFZLEdBQUdrQixVQUFVLENBQUNsZSxJQUExQjtBQUNBK2MsaUJBQWEsR0FBRyxDQUFDbUIsVUFBVSxDQUFDamUsS0FBWCxJQUFvQixFQUFyQixFQUF5QmtlLE9BQXpCLENBQWlDZCxJQUFJLElBQ25EN2pCLE1BQU0sQ0FBQ0MsSUFBUCxDQUFZNGpCLElBQVosRUFBa0J2WCxHQUFsQixDQUFzQmhRLEdBQUcsS0FBSztBQUFFZ2pCLFVBQUksRUFBRWhqQixHQUFSO0FBQWFrUSxXQUFLLEVBQUVxWCxJQUFJLENBQUN2bkIsR0FBRDtBQUF4QixLQUFMLENBQXpCLENBRGMsQ0FBaEI7QUFHQW1uQixpQkFBYSxHQUFHOWdCLEtBQUssQ0FBQzRELFFBQUQsQ0FBckIsQ0FOa0MsQ0FRbEM7O0FBQ0EsUUFBSSxDQUFDMGMsZUFBZSxDQUFDUSxhQUFELENBQWhCLElBQW1DLE9BQU9BLGFBQVAsS0FBeUIsUUFBaEUsRUFBMEU7QUFDeEU5bEIsVUFBSSxDQUFDNEksUUFBRCxDQUFKLEdBQWlCa2QsYUFBakI7QUFDRCxLQVhpQyxDQWFsQzs7O0FBQ0EsUUFDRSxDQUFDUixlQUFlLENBQUNRLGFBQUQsQ0FBaEIsSUFDQWYsS0FBSyxDQUFDUyxPQUFOLENBQWNNLGFBQWQsQ0FEQSxJQUVBRCxZQUFZLEtBQUssT0FIbkIsRUFJRTtBQUNBLFVBQUkzZ0Isa0JBQWtCLEdBQUcsSUFBekI7QUFDQSxVQUFJK2hCLFNBQVMsR0FBRyxFQUFoQjs7QUFFQSxXQUFLLE1BQU14a0IsSUFBWCxJQUFtQnFqQixhQUFuQixFQUFrQztBQUNoQyxZQUFJLE9BQU9yakIsSUFBUCxLQUFnQixRQUFwQixFQUE4QjtBQUM1QixjQUFJc2tCLFVBQVUsQ0FBQ3BELFdBQWYsRUFBNEI7QUFDMUJzRCxxQkFBUyxHQUFHNWtCLE1BQU0sQ0FBQ0MsSUFBUCxDQUFZRyxJQUFaLEVBQWtCb0YsTUFBbEIsQ0FBeUIsQ0FBQ0MsS0FBRCxFQUFRbkosR0FBUixLQUFnQjtBQUNuRCxxQkFBTyxDQUFDb29CLFVBQVUsQ0FBQ3BELFdBQVgsSUFBMEIsRUFBM0IsRUFBK0JoVixHQUEvQixDQUFtQ2xNLElBQUksSUFBSUEsSUFBSSxDQUFDbUcsUUFBaEQsRUFBMERsRyxRQUExRCxDQUFtRS9ELEdBQW5FLElBQ0gwRCxNQUFNLENBQUNzRSxNQUFQLENBQWNtQixLQUFkLEVBQXFCO0FBQUUsaUJBQUNuSixHQUFELEdBQU84RCxJQUFJLENBQUM5RCxHQUFEO0FBQWIsZUFBckIsQ0FERyxHQUVIbUosS0FGSjtBQUdELGFBSlcsRUFJVCxFQUpTLENBQVo7QUFNQTVDLDhCQUFrQixHQUFHQyxhQUFhLENBQ2hDbkgsTUFEZ0MsRUFFaENpcEIsU0FGZ0MsRUFHaENGLFVBQVUsQ0FBQ3BELFdBSHFCLENBQWxDO0FBTUEzakIsZ0JBQUksQ0FBQzRJLFFBQUQsQ0FBSixHQUFpQixDQUFDNUksSUFBSSxDQUFDNEksUUFBRCxDQUFKLElBQWtCLEVBQW5CLEVBQXVCdkUsTUFBdkIsQ0FDZmEsa0JBQWtCLENBQUNsRixJQURKLENBQWpCOztBQUlBLGdCQUFJLENBQUNzbEIsZUFBZSxDQUFDcGdCLGtCQUFrQixDQUFDSSxNQUFwQixDQUFwQixFQUFpRDtBQUMvQ0Esb0JBQU0sQ0FBQ3NELFFBQUQsQ0FBTixHQUFtQixDQUFDdEQsTUFBTSxDQUFDc0QsUUFBRCxDQUFOLElBQW9CLEVBQXJCLEVBQXlCdkUsTUFBekIsQ0FDakJhLGtCQUFrQixDQUFDSSxNQURGLENBQW5CO0FBR0Q7QUFDRixXQXRCRCxNQXNCTztBQUNMdEYsZ0JBQUksQ0FBQzRJLFFBQUQsQ0FBSixHQUFpQixDQUFDNUksSUFBSSxDQUFDNEksUUFBRCxDQUFKLElBQWtCLEVBQW5CLEVBQXVCdkUsTUFBdkIsQ0FBOEI1QixJQUE5QixDQUFqQjtBQUNEO0FBQ0YsU0ExQkQsTUEwQk87QUFDTHpDLGNBQUksQ0FBQzRJLFFBQUQsQ0FBSixHQUFpQixDQUFDNUksSUFBSSxDQUFDNEksUUFBRCxDQUFKLElBQWtCLEVBQW5CLEVBQXVCdkUsTUFBdkIsQ0FBOEI1QixJQUE5QixDQUFqQjtBQUNEO0FBQ0Y7QUFDRixLQXJEaUMsQ0F1RGxDOzs7QUFDQSxRQUNFLENBQUM2aUIsZUFBZSxDQUFDUSxhQUFELENBQWhCLElBQ0F6akIsTUFBTSxDQUFDNmtCLFNBQVAsQ0FBaUJ0UCxRQUFqQixDQUEwQnVQLElBQTFCLENBQStCckIsYUFBL0IsTUFBa0QsaUJBRGxELElBRUFELFlBQVksS0FBSyxRQUhuQixFQUlFO0FBQ0EsVUFBSWtCLFVBQVUsQ0FBQ3BELFdBQWYsRUFBNEI7QUFDMUJtQyxxQkFBYSxHQUFHempCLE1BQU0sQ0FBQ0MsSUFBUCxDQUFZd2pCLGFBQVosRUFBMkJqZSxNQUEzQixDQUFrQyxDQUFDQyxLQUFELEVBQVFuSixHQUFSLEtBQWdCO0FBQ2hFLGlCQUFPLENBQUNvb0IsVUFBVSxDQUFDcEQsV0FBWCxJQUEwQixFQUEzQixFQUErQmhWLEdBQS9CLENBQW1DbE0sSUFBSSxJQUFJQSxJQUFJLENBQUNtRyxRQUFoRCxFQUEwRGxHLFFBQTFELENBQW1FL0QsR0FBbkUsSUFDSDBELE1BQU0sQ0FBQ3NFLE1BQVAsQ0FBY21CLEtBQWQsRUFBcUI7QUFBRSxhQUFDbkosR0FBRCxHQUFPbW5CLGFBQWEsQ0FBQ25uQixHQUFEO0FBQXRCLFdBQXJCLENBREcsR0FFSG1KLEtBRko7QUFHRCxTQUplLEVBSWIsRUFKYSxDQUFoQjtBQUtBLGNBQU01QyxrQkFBa0IsR0FBR0MsYUFBYSxDQUN0Q25ILE1BRHNDLEVBRXRDOG5CLGFBRnNDLEVBR3RDaUIsVUFBVSxDQUFDcEQsV0FIMkIsQ0FBeEM7QUFLQTNqQixZQUFJLENBQUM0SSxRQUFELENBQUosR0FBaUIxRCxrQkFBa0IsQ0FBQ2xGLElBQXBDOztBQUVBLFlBQUksQ0FBQ3NsQixlQUFlLENBQUNwZ0Isa0JBQWtCLENBQUNJLE1BQXBCLENBQXBCLEVBQWlEO0FBQy9DQSxnQkFBTSxDQUFDc0QsUUFBRCxDQUFOLEdBQW1CMUQsa0JBQWtCLENBQUNJLE1BQXRDO0FBQ0Q7QUFDRixPQWhCRCxNQWdCTztBQUNMdEYsWUFBSSxDQUFDNEksUUFBRCxDQUFKLEdBQWlCa2QsYUFBakI7QUFDRDtBQUNGLEtBaEZpQyxDQWtGbEM7OztBQUNBLFVBQU01Z0Isa0JBQWtCLEdBQUd3Z0IscUJBQXFCLENBQUMxbkIsTUFBRCxFQUFTO0FBQ3ZENG5CLG1CQUR1RDtBQUV2REMsa0JBRnVEO0FBR3ZEQztBQUh1RCxLQUFULENBQWhEOztBQU1BLFFBQUksQ0FBQ1IsZUFBZSxDQUFDcGdCLGtCQUFELENBQXBCLEVBQTBDO0FBQ3hDSSxZQUFNLENBQUNzRCxRQUFELENBQU4sR0FBbUIxRCxrQkFBbkI7QUFDRDtBQUNGOztBQUVELFNBQU87QUFBRUksVUFBRjtBQUFVdEY7QUFBVixHQUFQO0FBQ0QsQ0F2R00sQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDeEpQLDRDOzs7Ozs7Ozs7OztBQ0FBLDJDOzs7Ozs7Ozs7OztBQ0FBLGtDOzs7Ozs7Ozs7OztBQ0FBLG1DOzs7Ozs7Ozs7OztBQ0FBLGlDOzs7Ozs7Ozs7OztBQ0FBLG9DOzs7Ozs7Ozs7OztBQ0FBLDJDOzs7Ozs7Ozs7OztBQ0FBLCtDOzs7Ozs7Ozs7OztBQ0FBLHlDOzs7Ozs7Ozs7OztBQ0FBLDBDOzs7Ozs7Ozs7OztBQ0FBLDhDOzs7Ozs7Ozs7OztBQ0FBLCtDOzs7Ozs7Ozs7OztBQ0FBLCtCOzs7Ozs7Ozs7OztBQ0FBLHlDOzs7Ozs7Ozs7OztBQ0FBLCtCOzs7Ozs7Ozs7OztBQ0FBLGlDOzs7Ozs7Ozs7OztBQ0FBLGtDOzs7Ozs7Ozs7OztBQ0FBLDZDOzs7Ozs7Ozs7OztBQ0FBLHlDOzs7Ozs7Ozs7OztBQ0FBLHdDOzs7Ozs7Ozs7OztBQ0FBLDZDOzs7Ozs7Ozs7OztBQ0FBLHlDOzs7Ozs7Ozs7OztBQ0FBLGtDOzs7Ozs7Ozs7OztBQ0FBLHFFOzs7Ozs7Ozs7OztBQ0FBLHdDOzs7Ozs7Ozs7OztBQ0FBLGlEOzs7Ozs7Ozs7OztBQ0FBLDhDIiwiZmlsZSI6ImluZGV4LmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZ2V0dGVyIH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSBmdW5jdGlvbihleHBvcnRzKSB7XG4gXHRcdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuIFx0XHR9XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG4gXHR9O1xuXG4gXHQvLyBjcmVhdGUgYSBmYWtlIG5hbWVzcGFjZSBvYmplY3RcbiBcdC8vIG1vZGUgJiAxOiB2YWx1ZSBpcyBhIG1vZHVsZSBpZCwgcmVxdWlyZSBpdFxuIFx0Ly8gbW9kZSAmIDI6IG1lcmdlIGFsbCBwcm9wZXJ0aWVzIG9mIHZhbHVlIGludG8gdGhlIG5zXG4gXHQvLyBtb2RlICYgNDogcmV0dXJuIHZhbHVlIHdoZW4gYWxyZWFkeSBucyBvYmplY3RcbiBcdC8vIG1vZGUgJiA4fDE6IGJlaGF2ZSBsaWtlIHJlcXVpcmVcbiBcdF9fd2VicGFja19yZXF1aXJlX18udCA9IGZ1bmN0aW9uKHZhbHVlLCBtb2RlKSB7XG4gXHRcdGlmKG1vZGUgJiAxKSB2YWx1ZSA9IF9fd2VicGFja19yZXF1aXJlX18odmFsdWUpO1xuIFx0XHRpZihtb2RlICYgOCkgcmV0dXJuIHZhbHVlO1xuIFx0XHRpZigobW9kZSAmIDQpICYmIHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcgJiYgdmFsdWUgJiYgdmFsdWUuX19lc01vZHVsZSkgcmV0dXJuIHZhbHVlO1xuIFx0XHR2YXIgbnMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIobnMpO1xuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkobnMsICdkZWZhdWx0JywgeyBlbnVtZXJhYmxlOiB0cnVlLCB2YWx1ZTogdmFsdWUgfSk7XG4gXHRcdGlmKG1vZGUgJiAyICYmIHR5cGVvZiB2YWx1ZSAhPSAnc3RyaW5nJykgZm9yKHZhciBrZXkgaW4gdmFsdWUpIF9fd2VicGFja19yZXF1aXJlX18uZChucywga2V5LCBmdW5jdGlvbihrZXkpIHsgcmV0dXJuIHZhbHVlW2tleV07IH0uYmluZChudWxsLCBrZXkpKTtcbiBcdFx0cmV0dXJuIG5zO1xuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCIvXCI7XG5cblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSAwKTtcbiIsImltcG9ydCAqIGFzIGZ1bmN0aW9ucyBmcm9tICdmaXJlYmFzZS1mdW5jdGlvbnMnO1xyXG5pbXBvcnQgKiBhcyBzZW5kZ3JpZCBmcm9tICdAc2VuZGdyaWQvbWFpbCc7XHJcbmltcG9ydCBzdG9yZSBmcm9tICcuLi9zdG9yZS9zdG9yZSc7XHJcbmltcG9ydCB7IHNldENvbmZpZ3VyYXRpb24gfSBmcm9tICcuLi9zdG9yZS9hY3Rpb25zL2NvbmZpZy1hY3Rpb25zJztcclxuaW1wb3J0IHsgdHJhbnNsYXRlIH0gZnJvbSAnLi4vdXRpbHMvaW50bC11dGlscyc7XHJcbmltcG9ydCB7IHNlbmROb3RpZmljYXRpb24gfSBmcm9tICcuLi91dGlscy9tYWlsLXV0aWxzJztcclxuY29uc3QgeyBhcHAgfSA9IGZ1bmN0aW9ucy5jb25maWcoKTtcclxuXHJcbi8vIGV2ZW50c1xyXG5leHBvcnQgY29uc3Qgb25OZXdVc2VyID0gZnVuY3Rpb25zLmF1dGhcclxuICAudXNlcigpXHJcbiAgLm9uQ3JlYXRlKGFzeW5jIHVzZXIgPT4ge1xyXG4gICAgdHJ5IHtcclxuICAgICAgY29uc3QgeyBjb25maWcgfSA9IHN0b3JlLmdldFN0YXRlKCk7XHJcbiAgICAgIGNvbnN0IHsgc2VuZGdyaWQ6IHNlbmRncmlkQ29uZmlnIH0gPSBmdW5jdGlvbnMuY29uZmlnKCk7XHJcbiAgICAgIGNvbnN0IGN1cnJlbnRMYW5ndWFnZSA9ICh1c2VyLnByZWZlcmVuY2VzICYmIHVzZXIucHJlZmVyZW5jZXMubGFuZ3VhZ2UpIHx8ICdlbic7XHJcbiAgICAgIGNvbnN0IGN1cnJlbnRUcmFuc2xhdGlvbnMgPSBjb25maWcuaW50bERhdGEubG9jYWxlc1tjdXJyZW50TGFuZ3VhZ2VdO1xyXG4gICAgICBzZW5kZ3JpZC5zZXRBcGlLZXkoc2VuZGdyaWRDb25maWcua2V5KTtcclxuXHJcbiAgICAgIGNvbnN0IGNvbmZpZ0VtYWlsID0geyBcclxuICAgICAgICAuLi5mdW5jdGlvbnMuY29uZmlnKCksIFxyXG4gICAgICAgIHRyYW5zbGF0aW9uczogY3VycmVudFRyYW5zbGF0aW9uc1xyXG4gICAgICB9O1xyXG4gICAgICBjb25zdCBlbWFpbERhdGEgPSB7XHJcbiAgICAgICAgdG86IHVzZXIuZW1haWwsXHJcbiAgICAgICAgc3ViamVjdDogdHJhbnNsYXRlKGN1cnJlbnRUcmFuc2xhdGlvbnMsICdzYW1wbGVTdWJqZWN0JyksXHJcbiAgICAgICAgbWVzc2FnZTogdHJhbnNsYXRlKGN1cnJlbnRUcmFuc2xhdGlvbnMsICdzYW1wbGVNZXNzYWdlJywgeyB1c2VybmFtZTogdXNlci5lbWFpbCB9KVxyXG4gICAgICB9O1xyXG4gICAgICBhd2FpdCBzZW5kTm90aWZpY2F0aW9uKGNvbmZpZ0VtYWlsLCBzZW5kZ3JpZCwgZW1haWxEYXRhKTtcclxuICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgY29uc29sZS5lcnJvcignRXJyb3I6IHJ1bm5pbmcgZXZlbnQgb25OZXdVc2VyJywgZSk7XHJcbiAgICB9XHJcbiAgfSk7XHJcblxyXG4gIGV4cG9ydCBjb25zdCB1cGRhdGVDb25maWd1cmF0aW9uID0gZnVuY3Rpb25zLmZpcmVzdG9yZVxyXG4gICAgLmRvY3VtZW50KGBjb25maWd1cmF0aW9uLyR7YXBwLnByb2plY3RpZH1gKVxyXG4gICAgLm9uVXBkYXRlKGNoYW5nZSA9PiB7XHJcbiAgICAgIGNvbnN0IG5ld0NvbmZpZyA9IGNoYW5nZS5hZnRlci5kYXRhKCk7XHJcbiAgICAgIHN0b3JlLmRpc3BhdGNoKHNldENvbmZpZ3VyYXRpb24obmV3Q29uZmlnKSk7XHJcbiAgICB9KTtcclxuIiwiaW1wb3J0ICdmaXJlYmFzZS9hdXRoJztcclxuaW1wb3J0ICdmaXJlYmFzZS9kYXRhYmFzZSc7XHJcbmltcG9ydCAnZmlyZWJhc2UvZmlyZXN0b3JlJztcclxuaW1wb3J0ICogYXMgZnVuY3Rpb25zIGZyb20gJ2ZpcmViYXNlLWZ1bmN0aW9ucyc7XHJcbmltcG9ydCAqIGFzIGZpcmViYXNlQWRtaW5MaWIgZnJvbSAnZmlyZWJhc2UtYWRtaW4nO1xyXG5pbXBvcnQgZmlyZWJhc2VDbGllbnRMaWIgZnJvbSAnZmlyZWJhc2UvYXBwJztcclxuXHJcbmNvbnN0IHNlcnZpY2VBY2NvdW50ID0gcmVxdWlyZSgnLi4vLi4vc2VydmljZS1hY2NvdW50Lmpzb24nKTtcclxuY29uc3QgeyBhcHAgfSA9IGZ1bmN0aW9ucy5jb25maWcoKTtcclxuXHJcbmV4cG9ydCBjb25zdCBmaXJlYmFzZUNsaWVudCA9IGZpcmViYXNlQ2xpZW50TGliLmluaXRpYWxpemVBcHAoe1xyXG4gIGFwaUtleTogYXBwLmFwaWtleSxcclxuICBhdXRoRG9tYWluOiBhcHAuYXV0aGRvbWFpbixcclxuICBkYXRhYmFzZVVSTDogYXBwLmRhdGFiYXNldXJsLFxyXG4gIHByb2plY3RJZDogYXBwLnByb2plY3RpZCxcclxuICBzdG9yYWdlQnVja2V0OiBhcHAuc3RvcmFnZWJ1Y2tldCxcclxuICBtZXNzYWdpbmdTZW5kZXJJZDogYXBwLm1lc3NhZ2luZ3NlbmRlcmlkXHJcbn0pO1xyXG5cclxuZXhwb3J0IGNvbnN0IGZpcmViYXNlQWRtaW4gPSBmaXJlYmFzZUFkbWluTGliLmluaXRpYWxpemVBcHAoe1xyXG4gIGNyZWRlbnRpYWw6IGZpcmViYXNlQWRtaW5MaWIuY3JlZGVudGlhbC5jZXJ0KHNlcnZpY2VBY2NvdW50KSxcclxuICBkYXRhYmFzZVVSTDogYXBwLmRhdGFiYXNldXJsXHJcbn0pO1xyXG4iLCJpbXBvcnQgZnMgZnJvbSAnZnMnO1xyXG5pbXBvcnQgb3MgZnJvbSAnb3MnO1xyXG5pbXBvcnQgcGF0aCBmcm9tICdwYXRoJztcclxuaW1wb3J0IEJ1c2JveSBmcm9tICdidXNib3knO1xyXG5pbXBvcnQgeyB0cmFuc2xhdGUgfSBmcm9tICcuLi91dGlscy9pbnRsLXV0aWxzJztcclxuaW1wb3J0IHsgZ2V0QmFja2VuZEVycm9yIH0gZnJvbSAnLi4vdXRpbHMvcmVzcG9uc2UtdXRpbHMnO1xyXG5pbXBvcnQgeyBmaXJlYmFzZUFkbWluIH0gZnJvbSAnLi9maXJlYmFzZSc7XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gYXV0aG9yaXphdGlvbihyZXF1aXJlZENsYWltcykge1xyXG4gIHJldHVybiBhc3luYyAocmVxLCByZXMsIG5leHQpID0+IHtcclxuICAgIHRyeSB7XHJcbiAgICAgIGxldCB0b2tlbiA9IHJlcS5oZWFkZXJzWydhdXRob3JpemF0aW9uJ10gfHwgJyc7XHJcblxyXG4gICAgICAvLyBjaGVja2luZyBlbXB0eSB0b2tlblxyXG4gICAgICBpZiAoIXRva2VuIHx8ICF0b2tlbi5zdGFydHNXaXRoKCdCZWFyZXIgJykpIHtcclxuICAgICAgICByZXMuc3RhdHVzKDQwMykuanNvbih7IG1lc3NhZ2U6IHRyYW5zbGF0ZShyZXEudHJhbnNsYXRpb25zLCAnYXV0aE5vdExvZ2luJykgfSk7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgICB9XHJcblxyXG4gICAgICAvLyBjaGFja2luZyB1c2VyIGdldCBpbmZvIGZyb20gdG9rZW5cclxuICAgICAgdG9rZW4gPSB0b2tlbi5zcGxpdCgnQmVhcmVyICcpWzFdO1xyXG4gICAgICByZXEudXNlciA9IGF3YWl0IGZpcmViYXNlQWRtaW4uYXV0aCgpLnZlcmlmeUlkVG9rZW4odG9rZW4pO1xyXG4gICAgICByZXEudXNlci5jbGFpbXMgPSBPYmplY3Qua2V5cyhyZXEudXNlcik7XHJcblxyXG4gICAgICAvLyBjaGVja2luZyBjbGFpbXNcclxuICAgICAgaWYgKFxyXG4gICAgICAgIHJlcXVpcmVkQ2xhaW1zLmxlbmd0aCA+IDAgJiZcclxuICAgICAgICAhcmVxLnVzZXIuY2xhaW1zLmZpbHRlcihpdGVtID0+IHJlcXVpcmVkQ2xhaW1zLmluY2x1ZGVzKGl0ZW0pKS5sZW5ndGhcclxuICAgICAgKSB7XHJcbiAgICAgICAgcmVzLnN0YXR1cyg0MDMpLmpzb24oeyBtZXNzYWdlOiB0cmFuc2xhdGUocmVxLnRyYW5zbGF0aW9ucywgJ2F1dGhOb3RQcml2aWxpZ2VzJykgfSk7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBuZXh0KCk7XHJcbiAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgIHJlcy5zdGF0dXMoNDAzKS5qc29uKHsgbWVzc2FnZTogdHJhbnNsYXRlKHJlcS50cmFuc2xhdGlvbnMsICdhdXRoTm90TG9naW4nKSB9KTtcclxuICAgIH1cclxuICB9O1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gZmlsZXMob3B0aW9ucykge1xyXG4gIHJldHVybiBhc3luYyAocmVxLCByZXMsIG5leHQpID0+IHtcclxuICAgIHRyeSB7XHJcbiAgICAgIGNvbnN0IGNvbnRlbnRUeXBlID0gcmVxLmhlYWRlcnNbJ2NvbnRlbnQtdHlwZSddO1xyXG5cclxuICAgICAgaWYgKCFjb250ZW50VHlwZSB8fCBjb250ZW50VHlwZS5pbmRleE9mKCdhcHBsaWNhdGlvbi9qc29uJykgPiAtMSkge1xyXG4gICAgICAgIHJldHVybiBuZXh0KCk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGNvbnN0IHsgbWF4RmlsZXMsIG1heEZpbGVTaXplIH0gPSBvcHRpb25zO1xyXG4gICAgICBjb25zdCBidXNib3kgPSBuZXcgQnVzYm95KHtcclxuICAgICAgICBoZWFkZXJzOiByZXEuaGVhZGVycyxcclxuICAgICAgICBsaW1pdHM6IHtcclxuICAgICAgICAgIGZpbGVzOiBtYXhGaWxlcyB8fCA1LFxyXG4gICAgICAgICAgZmlsZVNpemU6IChtYXhGaWxlU2l6ZSB8fCA1KSAqIDEwMjQgKiAxMDI0XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuXHJcbiAgICAgIGJ1c2JveS5vbignZmllbGQnLCAoZmllbGRuYW1lLCB2YWwpID0+IChyZXEuYm9keVtmaWVsZG5hbWVdID0gdmFsKSk7XHJcbiAgICAgIGJ1c2JveS5vbignZmlsZScsIChmaWVsZG5hbWUsIGZpbGUsIGZpbGVuYW1lKSA9PiB7XHJcbiAgICAgICAgY29uc3QgeyBhbGxvd2VkRXh0ZW50aW9ucyB9ID0gb3B0aW9ucztcclxuXHJcbiAgICAgICAgaWYgKCFhbGxvd2VkRXh0ZW50aW9ucy5maW5kKGV4dCA9PiBmaWxlbmFtZS5lbmRzV2l0aCgnLicgKyBleHQpKSkge1xyXG4gICAgICAgICAgcmVzLnN0YXR1cyg0MDApLmpzb24oe1xyXG4gICAgICAgICAgICBtZXNzYWdlOiB0cmFuc2xhdGUocmVxLnRyYW5zbGF0aW9ucywgJ3VwbG9hZEVycm9yRXh0ZW50aW9ucycsIHtcclxuICAgICAgICAgICAgICBleHRlbnRpb25zOiBhbGxvd2VkRXh0ZW50aW9ucy5qb2luKCcsICcpXHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGZpbGUub24oJ2xpbWl0JywgKCkgPT4ge1xyXG4gICAgICAgICAgcmVzLnN0YXR1cyg0MDApLmpzb24oe1xyXG4gICAgICAgICAgICBtZXNzYWdlOiB0cmFuc2xhdGUocmVxLnRyYW5zbGF0aW9ucywgJ3VwbG9hZEVycm9yU2l6ZScsIHsgbWF4RmlsZXMsIG1heEZpbGVTaXplIH0pXHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgbGV0IHRtcEZpbGVQYXRoID0gcGF0aC5qb2luKG9zLnRtcGRpcigpLCBmaWxlbmFtZSk7XHJcbiAgICAgICAgcmVxLmJvZHlbZmllbGRuYW1lXSA9IChyZXEuYm9keVtmaWVsZG5hbWVdIHx8IFtdKS5jb25jYXQodG1wRmlsZVBhdGgpO1xyXG4gICAgICAgIGZpbGUucGlwZShmcy5jcmVhdGVXcml0ZVN0cmVhbSh0bXBGaWxlUGF0aCkpO1xyXG4gICAgICB9KTtcclxuICAgICAgYnVzYm95Lm9uKCdmaW5pc2gnLCAoKSA9PiBuZXh0KCkpO1xyXG4gICAgICBidXNib3kuZW5kKHJlcS5yYXdCb2R5KTtcclxuICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgY29uc3QgZXJyb3IgPSBnZXRCYWNrZW5kRXJyb3IoZSk7XHJcbiAgICAgIHJlcy5zdGF0dXMoNTAwKS5qc29uKGVycm9yKTtcclxuICAgIH1cclxuICB9O1xyXG59XHJcbiIsImltcG9ydCBleHByZXNzIGZyb20gJ2V4cHJlc3MnO1xyXG5pbXBvcnQgeyBmaXJlYmFzZUFkbWluIH0gZnJvbSAnLi4vZmlyZWJhc2UnO1xyXG5pbXBvcnQgeyBhdXRob3JpemF0aW9uIH0gZnJvbSAnLi4vbWlkZGxld2FyZXMnO1xyXG5pbXBvcnQgeyBnZXREYkRvY3VtZW50LCBnZXREYlF1ZXJ5LCBnZXREYlF1ZXJ5QnlJZHMgfSBmcm9tICcuLi8uLi91dGlscy9kYXRhYmFzZS11dGlscyc7XHJcbmltcG9ydCB7IGdldENhdGFsb2dNb2RlbCB9IGZyb20gJy4uLy4uL3V0aWxzL21vZGVsLXV0aWxzJztcclxuaW1wb3J0IHsgZ2V0QmFja2VuZEVycm9yIH0gZnJvbSAnLi4vLi4vdXRpbHMvcmVzcG9uc2UtdXRpbHMnO1xyXG5pbXBvcnQgeyB2YWxpZGF0ZU1vZGVsIH0gZnJvbSAnLi4vLi4vdXRpbHMvdmFsaWRhdGlvbi11dGlscyc7XHJcblxyXG4vLyBoZWxwZXJzXHJcbmNvbnN0IHJvdXRlciA9IGV4cHJlc3MuUm91dGVyKCk7XHJcbmNvbnN0IGdldE1vZGVsVmFsaWRhdGlvbiA9IGFzeW5jIChyZXEsIG1vZGVsLCBhc3NvY2lhdGlvbnMpID0+IHsgIFxyXG4gIC8vIHZhbGlkYXRpbmcgc3RydWN0dXJlXHJcbiAgY29uc3QgdmFsaWRhdGlvblJlc3BvbnNlID0gdmFsaWRhdGVNb2RlbChyZXEsIG1vZGVsLCBnZXRDYXRhbG9nTW9kZWwoYXNzb2NpYXRpb25zLCByZXEuY29uZmlnLmFwcExhbmd1YWdlcykpO1xyXG4gIGNvbnN0IGVycm9ycyA9IHZhbGlkYXRpb25SZXNwb25zZS5lcnJvcnM7XHJcbiAgbGV0IGRhdGEgPSB2YWxpZGF0aW9uUmVzcG9uc2UuZGF0YTtcclxuXHJcbiAgcmV0dXJuIHsgZXJyb3JzLCBkYXRhIH07XHJcbn07XHJcblxyXG4vLyByb3V0ZXNcclxucm91dGVyLmdldCgnLycsIGFzeW5jIChyZXEsIHJlcykgPT4ge1xyXG4gIHRyeSB7XHJcbiAgICBjb25zdCBkYiA9IGZpcmViYXNlQWRtaW4uZmlyZXN0b3JlKCk7XHJcbiAgICBjb25zdCByZXNwb25zZSA9IGF3YWl0IGdldERiUXVlcnkoZGIsICdjYXRhbG9ncycsIHJlcS5xdWVyeSk7XHJcblxyXG4gICAgcmVzLmpzb24ocmVzcG9uc2UpOyBcclxuICB9IGNhdGNoIChlKSB7XHJcbiAgICBjb25zdCBlcnJvciA9IGdldEJhY2tlbmRFcnJvcihlKTtcclxuICAgIHJlcy5zdGF0dXMoNTAwKS5qc29uKGVycm9yKTtcclxuICB9XHJcbn0pO1xyXG5cclxucm91dGVyLmdldCgnLzppZCcsIGFzeW5jIChyZXEsIHJlcykgPT4ge1xyXG4gIHRyeSB7XHJcbiAgICBjb25zdCBkYiA9IGZpcmViYXNlQWRtaW4uZmlyZXN0b3JlKCk7XHJcbiAgICBjb25zdCByZXNwb25zZSA9IGF3YWl0IGdldERiRG9jdW1lbnQoZGIsICdjYXRhbG9ncycsIHJlcS5wYXJhbXMuaWQpO1xyXG5cclxuICAgIHJlcy5qc29uKHJlc3BvbnNlKTtcclxuICB9IGNhdGNoIChlKSB7XHJcbiAgICBjb25zdCBlcnJvciA9IGdldEJhY2tlbmRFcnJvcihlKTtcclxuICAgIHJlcy5zdGF0dXMoNTAwKS5qc29uKGVycm9yKTtcclxuICB9XHJcbn0pO1xyXG5cclxucm91dGVyLnBvc3QoJy8nLCBhdXRob3JpemF0aW9uKFsnaXNBZG1pbiddKSwgYXN5bmMgKHJlcSwgcmVzKSA9PiB7XHJcbiAgdHJ5IHtcclxuICAgIGNvbnN0IG1vZGVsID0geyAuLi5yZXEuYm9keSwgY3JlYXRlZEF0OiBEYXRlLm5vdygpIH07XHJcbiAgICBjb25zdCBkYiA9IGZpcmViYXNlQWRtaW4uZmlyZXN0b3JlKCk7XHJcbiAgICBjb25zdCBwYXJlbnRzID0gYXdhaXQgZ2V0RGJRdWVyeUJ5SWRzKGRiLCAnY2F0YWxvZ3MnLCBbbW9kZWwucGFyZW50XSk7XHJcbiAgICBjb25zdCB7IGVycm9ycywgZGF0YSB9ID0gYXdhaXQgZ2V0TW9kZWxWYWxpZGF0aW9uKHJlcSwgbW9kZWwsIHsgcGFyZW50cyB9KTtcclxuXHJcbiAgICBpZiAoT2JqZWN0LmtleXMoZXJyb3JzKS5sZW5ndGggPiAwKSB7XHJcbiAgICAgIHJldHVybiByZXMuc3RhdHVzKDQwMCkuanNvbih7IGVycm9ycyB9KTtcclxuICAgIH1cclxuXHJcbiAgICBsZXQgcmVzcG9uc2UgPSBhd2FpdCBkYi5jb2xsZWN0aW9uKCdjYXRhbG9ncycpLmFkZChkYXRhKTtcclxuXHJcbiAgICByZXNwb25zZSA9IHsgLi4uZGF0YSwgaWQ6IHJlc3BvbnNlLmlkIH07XHJcbiAgICByZXMuanNvbihyZXNwb25zZSk7XHJcbiAgfSBjYXRjaCAoZSkge1xyXG4gICAgY29uc3QgZXJyb3IgPSBnZXRCYWNrZW5kRXJyb3IoZSk7XHJcbiAgICByZXMuc3RhdHVzKDUwMCkuanNvbihlcnJvcik7XHJcbiAgfVxyXG59KTtcclxuXHJcbnJvdXRlci5wYXRjaCgnLzppZCcsIGF1dGhvcml6YXRpb24oWydpc0FkbWluJ10pLCBhc3luYyAocmVxLCByZXMpID0+IHtcclxuICB0cnkge1xyXG4gICAgY29uc3QgZGIgPSBmaXJlYmFzZUFkbWluLmZpcmVzdG9yZSgpO1xyXG4gICAgY29uc3QgZGJEYXRhID0gYXdhaXQgZ2V0RGJEb2N1bWVudChkYiwgJ2NhdGFsb2dzJywgcmVxLnBhcmFtcy5pZCk7XHJcbiAgICBjb25zdCBtb2RlbCA9IE9iamVjdC5hc3NpZ24oZGJEYXRhLCB7IC4uLnJlcS5ib2R5LCB1cGRhdGVkQXQ6IERhdGUubm93KCkgfSk7XHJcbiAgICBjb25zdCBwYXJlbnRzID0gYXdhaXQgZ2V0RGJRdWVyeUJ5SWRzKGRiLCAnY2F0YWxvZ3MnLCBbbW9kZWwucGFyZW50XSk7XHJcbiAgICBjb25zdCB7IGVycm9ycywgZGF0YSB9ID0gYXdhaXQgZ2V0TW9kZWxWYWxpZGF0aW9uKHJlcSwgbW9kZWwsIHsgcGFyZW50cyB9KTtcclxuXHJcbiAgICBpZiAoT2JqZWN0LmtleXMoZXJyb3JzKS5sZW5ndGggPiAwKSB7XHJcbiAgICAgIHJldHVybiByZXMuc3RhdHVzKDQwMCkuanNvbih7IGVycm9ycyB9KTtcclxuICAgIH1cclxuXHJcbiAgICBhd2FpdCBkYlxyXG4gICAgICAuY29sbGVjdGlvbignY2F0YWxvZ3MnKVxyXG4gICAgICAuZG9jKHJlcS5wYXJhbXMuaWQpXHJcbiAgICAgIC51cGRhdGUoZGF0YSk7XHJcblxyXG4gICAgY29uc3QgcmVzcG9uc2UgPSB7IC4uLmRhdGEsIGlkOiByZXEucGFyYW1zLmlkIH07XHJcbiAgICByZXMuanNvbihyZXNwb25zZSk7XHJcbiAgfSBjYXRjaCAoZSkge1xyXG4gICAgY29uc3QgZXJyb3IgPSBnZXRCYWNrZW5kRXJyb3IoZSk7XHJcbiAgICByZXMuc3RhdHVzKDUwMCkuanNvbihlcnJvcik7XHJcbiAgfVxyXG59KTtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IHJvdXRlcjtcclxuIiwiaW1wb3J0ICogYXMgZnVuY3Rpb25zIGZyb20gJ2ZpcmViYXNlLWZ1bmN0aW9ucyc7XHJcbmltcG9ydCBheGlvcyBmcm9tICdheGlvcyc7XHJcbmltcG9ydCBleHByZXNzIGZyb20gJ2V4cHJlc3MnO1xyXG5pbXBvcnQgY2FjaGUgZnJvbSAnbWVtb3J5LWNhY2hlJztcclxuaW1wb3J0IHsgYXV0aG9yaXphdGlvbiB9IGZyb20gJy4uL21pZGRsZXdhcmVzJztcclxuaW1wb3J0IHsgZmlyZWJhc2VBZG1pbiB9IGZyb20gJy4uL2ZpcmViYXNlJztcclxuaW1wb3J0IHsgZ2V0RGJRdWVyeSwgZ2V0RGJEb2N1bWVudCB9IGZyb20gJy4uLy4uL3V0aWxzL2RhdGFiYXNlLXV0aWxzJztcclxuaW1wb3J0IHsgZ2V0SW50bERhdGEgfSBmcm9tICcuLi8uLi91dGlscy9pbnRsLXV0aWxzJztcclxuaW1wb3J0IHsgZ2V0Q29uZmlndXJhdGlvbk1vZGVsIH0gZnJvbSAnLi4vLi4vdXRpbHMvbW9kZWwtdXRpbHMnO1xyXG5pbXBvcnQgeyBnZXRCYWNrZW5kRXJyb3IgfSBmcm9tICcuLi8uLi91dGlscy9yZXNwb25zZS11dGlscyc7XHJcbmltcG9ydCB7IHZhbGlkYXRlTW9kZWwgfSBmcm9tICcuLi8uLi91dGlscy92YWxpZGF0aW9uLXV0aWxzJztcclxuXHJcbmNvbnN0IHJvdXRlciA9IGV4cHJlc3MuUm91dGVyKCk7XHJcblxyXG4vLyBzZXJ2aWNlc1xyXG5yb3V0ZXIucG9zdCgnL3N5bmMnLCBhdXRob3JpemF0aW9uKFsnaXNBZG1pbiddKSwgYXN5bmMgKHJlcSwgcmVzKSA9PiB7XHJcbiAgdHJ5IHtcclxuICAgIGNvbnN0IHsgYXBwLCBpbnRsLCByYXRlIH0gPSBmdW5jdGlvbnMuY29uZmlnKCk7XHJcbiAgICBjb25zdCBkYiA9IGZpcmViYXNlQWRtaW4uZmlyZXN0b3JlKCk7XHJcbiAgICBjb25zdCBkYkRhdGEgPSBhd2FpdCBnZXREYkRvY3VtZW50KGRiLCAnY29uZmlndXJhdGlvbicsIGFwcC5wcm9qZWN0aWQpO1xyXG4gICAgY29uc3QgbW9kZWwgPSBPYmplY3QuYXNzaWduKGRiRGF0YSwgeyAuLi5yZXEuYm9keSwgYXBwTGFzdFN5bmM6IERhdGUubm93KCkgfSk7XHJcbiAgICBjb25zdCB7IGVycm9ycywgZGF0YTogY29uZmlndXJhdGlvbkRhdGEgfSA9IHZhbGlkYXRlTW9kZWwocmVxLCBtb2RlbCwgZ2V0Q29uZmlndXJhdGlvbk1vZGVsKCkpO1xyXG5cclxuICAgIGlmIChPYmplY3Qua2V5cyhlcnJvcnMpLmxlbmd0aCA+IDApIHtcclxuICAgICAgcmVzLnN0YXR1cyg0MDApLmpzb24oeyBlcnJvcnMgfSk7XHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuXHJcbiAgICBhd2FpdCBkYlxyXG4gICAgICAuY29sbGVjdGlvbignY29uZmlndXJhdGlvbicpXHJcbiAgICAgIC5kb2MoYXBwLnByb2plY3RpZClcclxuICAgICAgLnNldChjb25maWd1cmF0aW9uRGF0YSk7XHJcblxyXG4gICAgY29uc3QgaW50bERhdGEgPSBhd2FpdCBnZXRJbnRsRGF0YShyZXEsIGF4aW9zLCBjYWNoZSwgZGIsIGludGwsIHJhdGUpO1xyXG4gICAgY29uc3QgcmVzcG9uc2UgPSB7IC4uLmNvbmZpZ3VyYXRpb25EYXRhLCBpbnRsRGF0YSB9O1xyXG4gICAgXHJcbiAgICByZXMuanNvbihyZXNwb25zZSk7XHJcbiAgfSBjYXRjaCAoZSkge1xyXG4gICAgY29uc3QgZXJyb3IgPSBnZXRCYWNrZW5kRXJyb3IoZSk7XHJcbiAgICByZXMuc3RhdHVzKDUwMCkuanNvbihlcnJvcik7XHJcbiAgfVxyXG59KTtcclxuXHJcbnJvdXRlci5nZXQoJy90ZXN0JywgYXN5bmMgKHJlcSwgcmVzKSA9PiB7XHJcbiAgdHJ5IHtcclxuICAgIGNvbnN0IGRiID0gZmlyZWJhc2VBZG1pbi5maXJlc3RvcmUoKTtcclxuICAgIGNvbnN0IHJlc3BvbnNlID0gYXdhaXQgZ2V0RGJRdWVyeShkYiwgJ2xvY2FsZXMnLCB7fSk7XHJcblxyXG4gICAgcmVzLmpzb24ocmVzcG9uc2UpO1xyXG4gIH0gY2F0Y2ggKGUpIHtcclxuICAgIGNvbnN0IGVycm9yID0gZ2V0QmFja2VuZEVycm9yKGUpO1xyXG4gICAgcmVzLnN0YXR1cyg1MDApLmpzb24oZXJyb3IpO1xyXG4gIH1cclxufSk7XHJcblxyXG5leHBvcnQgZGVmYXVsdCByb3V0ZXI7XHJcbiIsImltcG9ydCBleHByZXNzIGZyb20gJ2V4cHJlc3MnO1xyXG5pbXBvcnQgeyBmaXJlYmFzZUFkbWluIH0gZnJvbSAnLi4vZmlyZWJhc2UnO1xyXG5pbXBvcnQgeyBnZXREYkRvY3VtZW50LCBnZXREYlF1ZXJ5IH0gZnJvbSAnLi4vLi4vdXRpbHMvZGF0YWJhc2UtdXRpbHMnO1xyXG5pbXBvcnQgeyBnZXRMb2NhbGVNb2RlbCB9IGZyb20gJy4uLy4uL3V0aWxzL21vZGVsLXV0aWxzJztcclxuaW1wb3J0IHsgZ2V0QmFja2VuZEVycm9yIH0gZnJvbSAnLi4vLi4vdXRpbHMvcmVzcG9uc2UtdXRpbHMnO1xyXG5pbXBvcnQgeyB2YWxpZGF0ZU1vZGVsIH0gZnJvbSAnLi4vLi4vdXRpbHMvdmFsaWRhdGlvbi11dGlscyc7XHJcbmltcG9ydCB7IGF1dGhvcml6YXRpb24gfSBmcm9tICcuLi9taWRkbGV3YXJlcyc7XHJcblxyXG4vLyBoZWxwZXJzXHJcbmNvbnN0IHJvdXRlciA9IGV4cHJlc3MuUm91dGVyKCk7XHJcbmNvbnN0IGdldE1vZGVsVmFsaWRhdGlvbiA9IGFzeW5jIChyZXEsIG1vZGVsKSA9PiB7XHJcbiAgLy8gdmFsaWRhdGluZyBzdHJ1Y3R1cmVcclxuICBjb25zdCB2YWxpZGF0aW9uUmVzcG9uc2UgPSB2YWxpZGF0ZU1vZGVsKHJlcSwgbW9kZWwsIGdldExvY2FsZU1vZGVsKHJlcS5jb25maWcuYXBwTGFuZ3VhZ2VzKSk7XHJcbiAgY29uc3QgZXJyb3JzID0gdmFsaWRhdGlvblJlc3BvbnNlLmVycm9ycztcclxuICBsZXQgZGF0YSA9IHZhbGlkYXRpb25SZXNwb25zZS5kYXRhO1xyXG5cclxuICByZXR1cm4geyBlcnJvcnMsIGRhdGEgfTtcclxufTtcclxuXHJcbi8vIHJvdXRlc1xyXG5yb3V0ZXIuZ2V0KCcvJywgYXN5bmMgKHJlcSwgcmVzKSA9PiB7XHJcbiAgdHJ5IHtcclxuICAgIGNvbnN0IGRiID0gZmlyZWJhc2VBZG1pbi5maXJlc3RvcmUoKTtcclxuICAgIGNvbnN0IHJlc3BvbnNlID0gYXdhaXQgZ2V0RGJRdWVyeShkYiwgJ2xvY2FsZXMnLCByZXEucXVlcnkpO1xyXG4gICAgXHJcbiAgICByZXMuanNvbihyZXNwb25zZSk7XHJcbiAgfSBjYXRjaCAoZSkge1xyXG4gICAgY29uc3QgZXJyb3IgPSBnZXRCYWNrZW5kRXJyb3IoZSk7XHJcbiAgICByZXMuc3RhdHVzKDUwMCkuanNvbihlcnJvcik7XHJcbiAgfVxyXG59KTtcclxuXHJcbnJvdXRlci5nZXQoJy86aWQnLCBhc3luYyAocmVxLCByZXMpID0+IHtcclxuICB0cnkge1xyXG4gICAgY29uc3QgZGIgPSBmaXJlYmFzZUFkbWluLmZpcmVzdG9yZSgpO1xyXG4gICAgY29uc3QgcmVzcG9uc2UgPSBhd2FpdCBnZXREYkRvY3VtZW50KGRiLCAnbG9jYWxlcycsIHJlcS5wYXJhbXMuaWQpO1xyXG4gICAgcmVzLmpzb24ocmVzcG9uc2UpO1xyXG4gIH0gY2F0Y2ggKGUpIHtcclxuICAgIGNvbnN0IGVycm9yID0gZ2V0QmFja2VuZEVycm9yKGUpO1xyXG4gICAgcmVzLnN0YXR1cyg1MDApLmpzb24oZXJyb3IpO1xyXG4gIH1cclxufSk7XHJcblxyXG5yb3V0ZXIucG9zdCgnLycsIGF1dGhvcml6YXRpb24oWydpc0FkbWluJ10pLCBhc3luYyAocmVxLCByZXMpID0+IHtcclxuICB0cnkge1xyXG4gICAgY29uc3QgbW9kZWwgPSB7IC4uLnJlcS5ib2R5LCBjcmVhdGVkQXQ6IERhdGUubm93KCkgfTtcclxuICAgIGNvbnN0IGRiID0gZmlyZWJhc2VBZG1pbi5maXJlc3RvcmUoKTtcclxuICAgIGNvbnN0IHsgZXJyb3JzLCBkYXRhIH0gPSBhd2FpdCBnZXRNb2RlbFZhbGlkYXRpb24ocmVxLCBtb2RlbCk7XHJcblxyXG4gICAgaWYgKE9iamVjdC5rZXlzKGVycm9ycykubGVuZ3RoID4gMCkge1xyXG4gICAgICByZXR1cm4gcmVzLnN0YXR1cyg0MDApLmpzb24oeyBlcnJvcnMgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgbGV0IHJlc3BvbnNlID0gYXdhaXQgZGIuY29sbGVjdGlvbignbG9jYWxlcycpLmFkZChkYXRhKTtcclxuXHJcbiAgICByZXNwb25zZSA9IHsgLi4uZGF0YSwgaWQ6IHJlc3BvbnNlLmlkIH07XHJcbiAgICByZXMuanNvbihyZXNwb25zZSk7XHJcbiAgfSBjYXRjaCAoZSkge1xyXG4gICAgY29uc3QgZXJyb3IgPSBnZXRCYWNrZW5kRXJyb3IoZSk7XHJcbiAgICByZXMuc3RhdHVzKDUwMCkuanNvbihlcnJvcik7XHJcbiAgfVxyXG59KTtcclxuXHJcbnJvdXRlci5wYXRjaCgnLzppZCcsIGF1dGhvcml6YXRpb24oWydpc0FkbWluJ10pLCBhc3luYyAocmVxLCByZXMpID0+IHtcclxuICB0cnkge1xyXG4gICAgY29uc3QgZGIgPSBmaXJlYmFzZUFkbWluLmZpcmVzdG9yZSgpO1xyXG4gICAgY29uc3QgZGJEYXRhID0gYXdhaXQgZ2V0RGJEb2N1bWVudChkYiwgJ2xvY2FsZXMnLCByZXEucGFyYW1zLmlkKTtcclxuICAgIGNvbnN0IG1vZGVsID0gT2JqZWN0LmFzc2lnbihkYkRhdGEsIHsgLi4ucmVxLmJvZHksIHVwZGF0ZWRBdDogRGF0ZS5ub3coKSB9KTtcclxuICAgIGNvbnN0IHsgZXJyb3JzLCBkYXRhIH0gPSBhd2FpdCBnZXRNb2RlbFZhbGlkYXRpb24ocmVxLCBtb2RlbCk7XHJcblxyXG4gICAgaWYgKE9iamVjdC5rZXlzKGVycm9ycykubGVuZ3RoID4gMCkge1xyXG4gICAgICByZXR1cm4gcmVzLnN0YXR1cyg0MDApLmpzb24oeyBlcnJvcnMgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgYXdhaXQgZGJcclxuICAgICAgLmNvbGxlY3Rpb24oJ2xvY2FsZXMnKVxyXG4gICAgICAuZG9jKHJlcS5wYXJhbXMuaWQpXHJcbiAgICAgIC51cGRhdGUoZGF0YSk7XHJcblxyXG4gICAgY29uc3QgcmVzcG9uc2UgPSB7IC4uLmRhdGEsIGlkOiByZXEucGFyYW1zLmlkIH07XHJcbiAgICByZXMuanNvbihyZXNwb25zZSk7XHJcbiAgfSBjYXRjaCAoZSkge1xyXG4gICAgY29uc3QgZXJyb3IgPSBnZXRCYWNrZW5kRXJyb3IoZSk7XHJcbiAgICByZXMuc3RhdHVzKDUwMCkuanNvbihlcnJvcik7XHJcbiAgfVxyXG59KTtcclxuXHJcbnJvdXRlci5kZWxldGUoJy86aWQnLCBhdXRob3JpemF0aW9uKFsnaXNBZG1pbiddKSwgYXN5bmMgKHJlcSwgcmVzKSA9PiB7XHJcbiAgdHJ5IHtcclxuICAgIGNvbnN0IHsgaWQgfSA9IHJlcS5wYXJhbXM7XHJcbiAgICBjb25zdCBkYiA9IGZpcmViYXNlQWRtaW4uZmlyZXN0b3JlKCk7XHJcbiAgICBjb25zdCByZXNwb25zZSA9IGF3YWl0IGdldERiRG9jdW1lbnQoZGIsICdsb2NhbGVzJywgcmVxLnBhcmFtcy5pZCk7XHJcblxyXG4gICAgYXdhaXQgZGJcclxuICAgICAgLmNvbGxlY3Rpb24oJ2xvY2FsZXMnKVxyXG4gICAgICAuZG9jKGlkKVxyXG4gICAgICAuZGVsZXRlKCk7XHJcblxyXG4gICAgcmVzLmpzb24ocmVzcG9uc2UpO1xyXG4gIH0gY2F0Y2ggKGUpIHtcclxuICAgIGNvbnN0IGVycm9yID0gZ2V0QmFja2VuZEVycm9yKGUpO1xyXG4gICAgcmVzLnN0YXR1cyg1MDApLmpzb24oZXJyb3IpO1xyXG4gIH1cclxufSk7XHJcblxyXG5leHBvcnQgZGVmYXVsdCByb3V0ZXI7XHJcbiIsImltcG9ydCBleHByZXNzIGZyb20gJ2V4cHJlc3MnO1xyXG5pbXBvcnQgeyBmaXJlYmFzZUFkbWluIH0gZnJvbSAnLi4vZmlyZWJhc2UnO1xyXG4vLyBpbXBvcnQgeyBnZXREYlF1ZXJ5LCBnZXREYkRvY3VtZW50IH0gZnJvbSAnLi4vLi4vdXRpbHMvZGF0YWJhc2UtdXRpbHMnO1xyXG4vLyBpbXBvcnQgeyB0cmFuc2xhdGUgfSBmcm9tICcuLi8uLi91dGlscy9pbnRsLXV0aWxzJztcclxuaW1wb3J0IHsgZ2V0UmVwb3J0TW9kZWwgfSBmcm9tICcuLi8uLi91dGlscy9tb2RlbC11dGlscyc7XHJcbmltcG9ydCB7IHZhbGlkYXRlTW9kZWwgfSBmcm9tICcuLi8uLi91dGlscy92YWxpZGF0aW9uLXV0aWxzJztcclxuaW1wb3J0IHsgZ2V0QmFja2VuZEVycm9yIH0gZnJvbSAnLi4vLi4vdXRpbHMvcmVzcG9uc2UtdXRpbHMnO1xyXG4vLyBpbXBvcnQgeyBhdXRob3JpemF0aW9uIH0gZnJvbSAnLi4vbWlkZGxld2FyZXMnO1xyXG5cclxuLy8gaGVscGVyc1xyXG5jb25zdCByb3V0ZXIgPSBleHByZXNzLlJvdXRlcigpO1xyXG5jb25zdCBnZXRNb2RlbFZhbGlkYXRpb24gPSBhc3luYyAocmVxLCBtb2RlbCkgPT4ge1xyXG4gIC8vIHZhbGlkYXRpbmcgc3RydWN0dXJlXHJcbiAgY29uc3QgcGFzc3dvcmRJc1JlcXVpcmVkID0gbW9kZWwuaWQgPyBmYWxzZSA6IHRydWU7XHJcbiAgY29uc3QgdmFsaWRhdGlvblJlc3BvbnNlID0gdmFsaWRhdGVNb2RlbChyZXEsIG1vZGVsLCBnZXRSZXBvcnRNb2RlbChtb2RlbCwgcGFzc3dvcmRJc1JlcXVpcmVkKSk7XHJcbiAgY29uc3QgZXJyb3JzID0gdmFsaWRhdGlvblJlc3BvbnNlLmVycm9ycztcclxuICBsZXQgZGF0YSA9IHZhbGlkYXRpb25SZXNwb25zZS5kYXRhO1xyXG5cclxuICByZXR1cm4geyBlcnJvcnMsIGRhdGEgfTtcclxufTtcclxuXHJcbi8vIHNlcnZpY2VzXHJcbnJvdXRlci5wb3N0KCcvcmVwb3J0JywgYXN5bmMgKHJlcSwgcmVzKSA9PiB7XHJcbiAgdHJ5IHtcclxuICAgIGNvbnN0IGRiID0gZmlyZWJhc2VBZG1pbi5maXJlc3RvcmUoKTtcclxuICAgIGNvbnN0IHJlcG9ydERhdGEgPSBPYmplY3Qua2V5cyhyZXEuYm9keSkucmVkdWNlKChhY2N1bSwga2V5KSA9PiB7XHJcbiAgICAgICAgcmV0dXJuICFbJ2lkJ10uaW5jbHVkZXMoa2V5KSBcclxuICAgICAgICAgID8gT2JqZWN0LmFzc2lnbihhY2N1bSwgeyBba2V5XTogcmVxLmJvZHlba2V5XSB9KSBcclxuICAgICAgICAgIDogYWNjdW07XHJcbiAgICB9LCB7fSk7XHJcbiAgICBjb25zdCBtb2RlbCA9IHsgLi4ucmVwb3J0RGF0YSwgY3JlYXRlZEF0OiBEYXRlLm5vdygpIH07XHJcbiAgICBjb25zdCB7IGVycm9ycywgZGF0YSB9ID0gYXdhaXQgZ2V0TW9kZWxWYWxpZGF0aW9uKHJlcSwgbW9kZWwpO1xyXG4gICAgaWYgKE9iamVjdC5rZXlzKGVycm9ycykubGVuZ3RoID4gMCkge1xyXG4gICAgICByZXR1cm4gcmVzLnN0YXR1cyg0MDApLmpzb24oeyBlcnJvcnMgfSk7XHJcbiAgICB9XHJcbiAgICBjb25zb2xlLndhcm4oJ2RhdGEgPT4nLGRhdGEpO1xyXG4gICAgYXdhaXQgZGIuY29sbGVjdGlvbigncmVwb3J0cycpLmRvYygpLnNldChyZXBvcnREYXRhKTtcclxuICAgIHJlcy5qc29uKHtjcmVhdGVkOiB0cnVlfSk7XHJcbiAgfSBjYXRjaCAoZSkge1xyXG4gICAgY29uc3QgZXJyb3IgPSBnZXRCYWNrZW5kRXJyb3IoZSk7XHJcbiAgICByZXMuc3RhdHVzKDUwMCkuanNvbihlcnJvcik7XHJcbiAgfVxyXG59KTtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IHJvdXRlcjtcclxuIiwiaW1wb3J0IGV4cHJlc3MgZnJvbSAnZXhwcmVzcyc7XHJcbmltcG9ydCB7IGZpcmViYXNlQWRtaW4sIGZpcmViYXNlQ2xpZW50IH0gZnJvbSAnLi4vZmlyZWJhc2UnO1xyXG5pbXBvcnQgeyBnZXREYlF1ZXJ5LCBnZXREYkRvY3VtZW50IH0gZnJvbSAnLi4vLi4vdXRpbHMvZGF0YWJhc2UtdXRpbHMnO1xyXG5pbXBvcnQgeyB0cmFuc2xhdGUgfSBmcm9tICcuLi8uLi91dGlscy9pbnRsLXV0aWxzJztcclxuaW1wb3J0IHsgZ2V0VXNlck1vZGVsIH0gZnJvbSAnLi4vLi4vdXRpbHMvbW9kZWwtdXRpbHMnO1xyXG5pbXBvcnQgeyB2YWxpZGF0ZU1vZGVsIH0gZnJvbSAnLi4vLi4vdXRpbHMvdmFsaWRhdGlvbi11dGlscyc7XHJcbmltcG9ydCB7IGdldEJhY2tlbmRFcnJvciB9IGZyb20gJy4uLy4uL3V0aWxzL3Jlc3BvbnNlLXV0aWxzJztcclxuaW1wb3J0IHsgYXV0aG9yaXphdGlvbiB9IGZyb20gJy4uL21pZGRsZXdhcmVzJztcclxuXHJcbi8vIGhlbHBlcnNcclxuY29uc3Qgcm91dGVyID0gZXhwcmVzcy5Sb3V0ZXIoKTtcclxuY29uc3QgZ2V0TW9kZWxWYWxpZGF0aW9uID0gYXN5bmMgKHJlcSwgbW9kZWwpID0+IHtcclxuICAvLyB2YWxpZGF0aW5nIHN0cnVjdHVyZVxyXG4gIGNvbnN0IHBhc3N3b3JkSXNSZXF1aXJlZCA9IG1vZGVsLmlkID8gZmFsc2UgOiB0cnVlO1xyXG4gIGNvbnN0IHZhbGlkYXRpb25SZXNwb25zZSA9IHZhbGlkYXRlTW9kZWwocmVxLCBtb2RlbCwgZ2V0VXNlck1vZGVsKG1vZGVsLCBwYXNzd29yZElzUmVxdWlyZWQpKTtcclxuICBjb25zdCBlcnJvcnMgPSB2YWxpZGF0aW9uUmVzcG9uc2UuZXJyb3JzO1xyXG4gIGxldCBkYXRhID0gdmFsaWRhdGlvblJlc3BvbnNlLmRhdGE7XHJcblxyXG4gIHJldHVybiB7IGVycm9ycywgZGF0YSB9O1xyXG59O1xyXG5cclxuLy8gc2VydmljZXNcclxucm91dGVyLmdldCgnLycsIGF1dGhvcml6YXRpb24oWydpc0FkbWluJ10pLCBhc3luYyAocmVxLCByZXMpID0+IHtcclxuICB0cnkge1xyXG4gICAgY29uc3QgZGIgPSBmaXJlYmFzZUFkbWluLmZpcmVzdG9yZSgpO1xyXG4gICAgY29uc3QgcmVzcG9uc2UgPSBhd2FpdCBnZXREYlF1ZXJ5KGRiLCAndXNlcnMnLCByZXEucXVlcnkpO1xyXG5cclxuICAgIHJlcy5qc29uKHJlc3BvbnNlKTtcclxuICB9IGNhdGNoIChlKSB7XHJcbiAgICBjb25zdCBlcnJvciA9IGdldEJhY2tlbmRFcnJvcihlKTtcclxuICAgIHJlcy5zdGF0dXMoNTAwKS5qc29uKGVycm9yKTtcclxuICB9XHJcbn0pO1xyXG5cclxucm91dGVyLmdldCgnL21lJywgYXV0aG9yaXphdGlvbihbJ2lzUmVnaXN0ZXJlZCddKSwgYXN5bmMgKHJlcSwgcmVzKSA9PiB7XHJcbiAgdHJ5IHtcclxuICAgIGxldCB1c2VyID0gcmVxLnVzZXI7XHJcblxyXG4gICAgaWYgKHVzZXIpIHtcclxuICAgICAgY29uc3QgZGIgPSBmaXJlYmFzZUFkbWluLmZpcmVzdG9yZSgpO1xyXG4gICAgICBjb25zdCBwcm9maWxlID0gYXdhaXQgZ2V0RGJEb2N1bWVudChkYiwgJ3VzZXJzJywgdXNlci51aWQpO1xyXG4gICAgICB1c2VyID0gT2JqZWN0LmFzc2lnbih1c2VyLCB7IHByb2ZpbGUgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVzLmpzb24odXNlcik7XHJcbiAgfSBjYXRjaCAoZSkge1xyXG4gICAgY29uc3QgZXJyb3IgPSBnZXRCYWNrZW5kRXJyb3IoZSk7XHJcbiAgICByZXMuc3RhdHVzKDUwMCkuanNvbihlcnJvcik7XHJcbiAgfVxyXG59KTtcclxuXHJcbnJvdXRlci5nZXQoJy86aWQnLCBhdXRob3JpemF0aW9uKFsnaXNSZWdpc3RlcmVkJ10pLCBhc3luYyAocmVxLCByZXMpID0+IHtcclxuICB0cnkge1xyXG4gICAgY29uc3QgZGIgPSBmaXJlYmFzZUFkbWluLmZpcmVzdG9yZSgpO1xyXG4gICAgY29uc3QgcmVzcG9uc2UgPSBhd2FpdCBnZXREYkRvY3VtZW50KGRiLCAndXNlcnMnLCByZXEucGFyYW1zLmlkKTtcclxuXHJcbiAgICByZXMuanNvbihyZXNwb25zZSk7XHJcbiAgfSBjYXRjaCAoZSkge1xyXG4gICAgY29uc3QgZXJyb3IgPSBnZXRCYWNrZW5kRXJyb3IoZSk7XHJcbiAgICByZXMuc3RhdHVzKDUwMCkuanNvbihlcnJvcik7XHJcbiAgfVxyXG59KTtcclxuXHJcbnJvdXRlci5nZXQoJy86aWQvY2xhaW1zJywgYXV0aG9yaXphdGlvbihbJ2lzQWRtaW4nXSksIGFzeW5jIChyZXEsIHJlcykgPT4ge1xyXG4gIHRyeSB7XHJcbiAgICBjb25zdCB7IGlkOiB1c2VySWQgfSA9IHJlcS5wYXJhbXM7XHJcbiAgICBjb25zdCB1c2VyID0gYXdhaXQgZmlyZWJhc2VBZG1pbi5hdXRoKCkuZ2V0VXNlcih1c2VySWQpO1xyXG4gICAgbGV0IHJlc3BvbnNlID0gW107XHJcblxyXG4gICAgaWYgKHVzZXIuY3VzdG9tQ2xhaW1zKSB7XHJcbiAgICAgIHJlc3BvbnNlID0gT2JqZWN0LmtleXModXNlci5jdXN0b21DbGFpbXMpO1xyXG4gICAgfVxyXG5cclxuICAgIHJlcy5zZW5kKHJlc3BvbnNlKTtcclxuICB9IGNhdGNoIChlKSB7XHJcbiAgICBjb25zdCBlcnJvciA9IGdldEJhY2tlbmRFcnJvcihlKTtcclxuICAgIHJlcy5zdGF0dXMoNTAwKS5qc29uKGVycm9yKTtcclxuICB9XHJcbn0pO1xyXG5cclxucm91dGVyLnBvc3QoJy86aWQvY2xhaW1zJywgYXV0aG9yaXphdGlvbihbJ2lzQWRtaW4nXSksIGFzeW5jIChyZXEsIHJlcykgPT4ge1xyXG4gIHRyeSB7XHJcbiAgICBjb25zdCB7IGlkOiB1c2VySWQgfSA9IHJlcS5wYXJhbXM7XHJcbiAgICBjb25zdCB7IGNsYWltIH0gPSByZXEuYm9keTtcclxuXHJcbiAgICBjb25zdCB1c2VyID0gYXdhaXQgZmlyZWJhc2VBZG1pbi5hdXRoKCkuZ2V0VXNlcih1c2VySWQpO1xyXG4gICAgY29uc3QgY2xhaW1zID0geyAuLi51c2VyLmN1c3RvbUNsYWltcywgW2NsYWltXTogdHJ1ZSB9O1xyXG4gICAgYXdhaXQgZmlyZWJhc2VBZG1pbi5hdXRoKCkuc2V0Q3VzdG9tVXNlckNsYWltcyh1c2VySWQsIGNsYWltcyk7XHJcblxyXG4gICAgcmVzLmpzb24oY2xhaW0pO1xyXG4gIH0gY2F0Y2ggKGUpIHtcclxuICAgIGNvbnN0IGVycm9yID0gZ2V0QmFja2VuZEVycm9yKGUpO1xyXG4gICAgcmVzLnN0YXR1cyg1MDApLmpzb24oZXJyb3IpO1xyXG4gIH1cclxufSk7XHJcblxyXG5yb3V0ZXIucGF0Y2goJy86aWQvY2xhaW1zJywgYXV0aG9yaXphdGlvbihbJ2lzQWRtaW4nXSksIGFzeW5jIChyZXEsIHJlcykgPT4ge1xyXG4gIHRyeSB7XHJcbiAgICBjb25zdCB7IGlkOiB1c2VySWQgfSA9IHJlcS5wYXJhbXM7XHJcbiAgICBjb25zdCB7IGNsYWltIH0gPSByZXEuYm9keTtcclxuXHJcbiAgICBjb25zdCB1c2VyID0gYXdhaXQgZmlyZWJhc2VBZG1pbi5hdXRoKCkuZ2V0VXNlcih1c2VySWQpO1xyXG4gICAgZGVsZXRlIHVzZXIuY3VzdG9tQ2xhaW1zW2NsYWltXTtcclxuICAgIGF3YWl0IGZpcmViYXNlQWRtaW4uYXV0aCgpLnNldEN1c3RvbVVzZXJDbGFpbXModXNlcklkLCB1c2VyLmN1c3RvbUNsYWltcyk7XHJcblxyXG4gICAgcmVzLmpzb24oY2xhaW0pO1xyXG4gIH0gY2F0Y2ggKGUpIHtcclxuICAgIGNvbnN0IGVycm9yID0gZ2V0QmFja2VuZEVycm9yKGUpO1xyXG4gICAgcmVzLnN0YXR1cyg1MDApLmpzb24oZXJyb3IpO1xyXG4gIH1cclxufSk7XHJcblxyXG5yb3V0ZXIucGF0Y2goJy86aWQnLCBhdXRob3JpemF0aW9uKFsnaXNSZWdpc3RlcmVkJ10pLCBhc3luYyAocmVxLCByZXMpID0+IHtcclxuICB0cnkge1xyXG4gICAgY29uc3QgZGIgPSBmaXJlYmFzZUFkbWluLmZpcmVzdG9yZSgpO1xyXG4gICAgY29uc3QgZGJEYXRhID0gYXdhaXQgZ2V0RGJEb2N1bWVudChkYiwgJ3VzZXJzJywgcmVxLnBhcmFtcy5pZCk7XHJcbiAgICBjb25zdCB1c2VyRGF0YSA9IE9iamVjdC5rZXlzKHJlcS5ib2R5KS5yZWR1Y2UoKGFjY3VtLCBrZXkpID0+IHtcclxuICAgICAgcmV0dXJuICFbJ2VtYWlsJ10uaW5jbHVkZXMoa2V5KSBcclxuICAgICAgICA/IE9iamVjdC5hc3NpZ24oYWNjdW0sIHsgW2tleV06IHJlcS5ib2R5W2tleV0gfSkgXHJcbiAgICAgICAgOiBhY2N1bTtcclxuICAgIH0sIHt9KTtcclxuICAgIGNvbnN0IG1vZGVsID0gT2JqZWN0LmFzc2lnbihkYkRhdGEsIHsgLi4udXNlckRhdGEsIHVwZGF0ZWRBdDogRGF0ZS5ub3coKSB9KTtcclxuICAgIGNvbnN0IHsgZXJyb3JzLCBkYXRhIH0gPSBhd2FpdCBnZXRNb2RlbFZhbGlkYXRpb24ocmVxLCBtb2RlbCk7XHJcblxyXG4gICAgaWYgKE9iamVjdC5rZXlzKGVycm9ycykubGVuZ3RoID4gMCkge1xyXG4gICAgICByZXMuc3RhdHVzKDQwMCkuanNvbih7IGVycm9ycyB9KTtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICBjb25zdCBkYXRhVG9TYXZlID0gT2JqZWN0LmtleXMoZGF0YSkucmVkdWNlKChhY2N1bSwga2V5KSA9PiB7XHJcbiAgICAgIHJldHVybiAhWydwYXNzd29yZCcsICdwYXNzd29yZENvbmZpcm1hdGlvbiddLmluY2x1ZGVzKGtleSkgXHJcbiAgICAgICAgPyBPYmplY3QuYXNzaWduKGFjY3VtLCB7IFtrZXldOiBkYXRhW2tleV0gfSkgXHJcbiAgICAgICAgOiBhY2N1bTtcclxuICAgIH0sIHt9KTtcclxuICAgIGF3YWl0IGRiXHJcbiAgICAgIC5jb2xsZWN0aW9uKCd1c2VycycpXHJcbiAgICAgIC5kb2MocmVxLnBhcmFtcy5pZClcclxuICAgICAgLnVwZGF0ZShkYXRhVG9TYXZlKTtcclxuXHJcbiAgICBjb25zdCByZXNwb25zZSA9IHsgLi4uZGF0YSwgaWQ6IHJlcS5wYXJhbXMuaWQgfTtcclxuICAgIHJlcy5qc29uKHJlc3BvbnNlKTtcclxuICB9IGNhdGNoIChlKSB7XHJcbiAgICBjb25zdCBlcnJvciA9IGdldEJhY2tlbmRFcnJvcihlKTtcclxuICAgIHJlcy5zdGF0dXMoNTAwKS5qc29uKGVycm9yKTtcclxuICB9XHJcbn0pO1xyXG5cclxucm91dGVyLnBvc3QoJy9jaGFuZ2UtcGFzc3dvcmQnLCBhdXRob3JpemF0aW9uKFsnaXNSZWdpc3RlcmVkJ10pLCBhc3luYyAocmVxLCByZXMpID0+IHtcclxuICB0cnkge1xyXG4gICAgY29uc3QgeyBib2R5IH0gPSByZXE7XHJcbiAgICBjb25zdCB7IGRhdGEsIGVycm9ycyB9ID0gdmFsaWRhdGVNb2RlbChyZXEsIGJvZHksIFtcclxuICAgICAge1xyXG4gICAgICAgIHByb3BlcnR5OiAncGFzc3dvcmQnLFxyXG4gICAgICAgIHR5cGU6ICdzdHJpbmcnLFxyXG4gICAgICAgIHJ1bGVzOiBbeyByZXF1aXJlZDogdHJ1ZSB9LCB7IG1pbkxlbmd0aDogNiB9XVxyXG4gICAgICB9LFxyXG4gICAgICB7XHJcbiAgICAgICAgcHJvcGVydHk6ICdwYXNzd29yZENvbmZpcm1hdGlvbicsXHJcbiAgICAgICAgdHlwZTogJ3N0cmluZycsXHJcbiAgICAgICAgcnVsZXM6IFt7IHJlcXVpcmVkOiB0cnVlIH0sIHsgbWluTGVuZ3RoOiA2IH1dXHJcbiAgICAgIH1cclxuICAgIF0pO1xyXG5cclxuICAgIGlmIChPYmplY3Qua2V5cyhlcnJvcnMpLmxlbmd0aCA+IDApIHtcclxuICAgICAgcmVzLnN0YXR1cyg0MDApLmpzb24oeyBlcnJvcnMgfSk7XHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuXHJcbiAgICBhd2FpdCBmaXJlYmFzZUFkbWluLmF1dGgoKS51cGRhdGVVc2VyKHJlcS51c2VyLnVpZCwge1xyXG4gICAgICBwYXNzd29yZDogZGF0YS5wYXNzd29yZFxyXG4gICAgfSk7XHJcbiAgICByZXMuanNvbih7IG1lc3NhZ2U6IHRyYW5zbGF0ZShyZXEudHJhbnNsYXRpb25zLCAnc3VjZXNzZnVsT3BlcmF0aW9uJykgfSk7XHJcbiAgfSBjYXRjaCAoZSkge1xyXG4gICAgY29uc3QgZXJyb3IgPSBnZXRCYWNrZW5kRXJyb3IoZSk7XHJcbiAgICByZXMuc3RhdHVzKDUwMCkuanNvbihlcnJvcik7XHJcbiAgfVxyXG59KTtcclxuXHJcbnJvdXRlci5wb3N0KCcvcmVjb3Zlci1wYXNzd29yZCcsIGFzeW5jIChyZXEsIHJlcykgPT4ge1xyXG4gIHRyeSB7XHJcbiAgICBjb25zdCB7IGJvZHkgfSA9IHJlcTtcclxuICAgIGNvbnN0IHsgZGF0YSwgZXJyb3JzIH0gPSB2YWxpZGF0ZU1vZGVsKHJlcSwgYm9keSwgW1xyXG4gICAgICB7XHJcbiAgICAgICAgcHJvcGVydHk6ICdlbWFpbCcsXHJcbiAgICAgICAgdHlwZTogJ3N0cmluZycsXHJcbiAgICAgICAgcnVsZXM6IFt7IHJlcXVpcmVkOiB0cnVlIH0sIHsgZm9ybWF0OiAnZW1haWwnIH1dXHJcbiAgICAgIH1cclxuICAgIF0pO1xyXG5cclxuICAgIGlmIChPYmplY3Qua2V5cyhlcnJvcnMpLmxlbmd0aCA+IDApIHtcclxuICAgICAgcmVzLnN0YXR1cyg0MDApLmpzb24oeyBlcnJvcnMgfSk7XHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuXHJcbiAgICBhd2FpdCBmaXJlYmFzZUNsaWVudC5hdXRoKCkuc2VuZFBhc3N3b3JkUmVzZXRFbWFpbChkYXRhLmVtYWlsKTtcclxuICAgIHJlcy5qc29uKHsgbWVzc2FnZTogdHJhbnNsYXRlKHJlcS50cmFuc2xhdGlvbnMsICdyZWNvdmVyUGFzc3dvcmRTdWNjZXNzJykgfSk7XHJcbiAgfSBjYXRjaCAoZSkge1xyXG4gICAgY29uc3QgZXJyb3IgPSBnZXRCYWNrZW5kRXJyb3IoZSk7XHJcbiAgICByZXMuc3RhdHVzKDUwMCkuanNvbihlcnJvcik7XHJcbiAgfVxyXG59KTtcclxuXHJcbnJvdXRlci5wb3N0KCcvbG9naW4nLCBhc3luYyAocmVxLCByZXMpID0+IHtcclxuICB0cnkge1xyXG4gICAgY29uc3QgbW9kZWwgPSByZXEuYm9keTtcclxuICAgIGNvbnN0IHsgZXJyb3JzLCBkYXRhIH0gPSB2YWxpZGF0ZU1vZGVsKHJlcSwgbW9kZWwsIFtcclxuICAgICAge1xyXG4gICAgICAgIHByb3BlcnR5OiAnZW1haWwnLFxyXG4gICAgICAgIHR5cGU6ICdzdHJpbmcnLFxyXG4gICAgICAgIHJ1bGVzOiBbeyByZXF1aXJlZDogdHJ1ZSB9LCB7IGZvcm1hdDogJ2VtYWlsJyB9XVxyXG4gICAgICB9LFxyXG4gICAgICB7XHJcbiAgICAgICAgcHJvcGVydHk6ICdwYXNzd29yZCcsXHJcbiAgICAgICAgdHlwZTogJ3N0cmluZycsXHJcbiAgICAgICAgcnVsZXM6IFt7IHJlcXVpcmVkOiB0cnVlIH1dXHJcbiAgICAgIH1cclxuICAgIF0pO1xyXG5cclxuICAgIGlmIChPYmplY3Qua2V5cyhlcnJvcnMpLmxlbmd0aCA+IDApIHtcclxuICAgICAgcmV0dXJuIHJlcy5zdGF0dXMoNDAwKS5qc29uKHsgZXJyb3JzIH0pO1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICBjb25zdCByZXNwb25zZSA9IGF3YWl0IGZpcmViYXNlQ2xpZW50XHJcbiAgICAgIC5hdXRoKClcclxuICAgICAgLnNpZ25JbldpdGhFbWFpbEFuZFBhc3N3b3JkKGRhdGEuZW1haWwsIGRhdGEucGFzc3dvcmQpO1xyXG4gICAgY29uc3QgdG9rZW4gPSBhd2FpdCByZXNwb25zZS51c2VyLmdldElkVG9rZW4oKTtcclxuXHJcbiAgICByZXMuanNvbih7IHRva2VuIH0pO1xyXG4gIH0gY2F0Y2ggKGUpIHtcclxuICAgIGNvbnN0IGVycm9yID0gZ2V0QmFja2VuZEVycm9yKGUpO1xyXG4gICAgcmVzLnN0YXR1cyg1MDApLmpzb24oZXJyb3IpO1xyXG4gIH1cclxufSk7XHJcblxyXG5yb3V0ZXIucG9zdCgnL3JlZ2lzdGVyJywgYXN5bmMgKHJlcSwgcmVzKSA9PiB7XHJcbiAgdHJ5IHtcclxuICAgIGNvbnN0IGRiID0gZmlyZWJhc2VBZG1pbi5maXJlc3RvcmUoKTtcclxuICAgIGNvbnN0IHVzZXJEYXRhID0gT2JqZWN0LmtleXMocmVxLmJvZHkpLnJlZHVjZSgoYWNjdW0sIGtleSkgPT4ge1xyXG4gICAgICByZXR1cm4gIVsnaWQnXS5pbmNsdWRlcyhrZXkpIFxyXG4gICAgICAgID8gT2JqZWN0LmFzc2lnbihhY2N1bSwgeyBba2V5XTogcmVxLmJvZHlba2V5XSB9KSBcclxuICAgICAgICA6IGFjY3VtO1xyXG4gICAgfSwge30pO1xyXG4gICAgY29uc3QgbW9kZWwgPSB7IC4uLnVzZXJEYXRhLCBjcmVhdGVkQXQ6IERhdGUubm93KCkgfTtcclxuICAgIGNvbnN0IHsgZXJyb3JzLCBkYXRhIH0gPSBhd2FpdCBnZXRNb2RlbFZhbGlkYXRpb24ocmVxLCBtb2RlbCk7XHJcblxyXG4gICAgaWYgKE9iamVjdC5rZXlzKGVycm9ycykubGVuZ3RoID4gMCkge1xyXG4gICAgICByZXR1cm4gcmVzLnN0YXR1cyg0MDApLmpzb24oeyBlcnJvcnMgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gcmVnaXN0ZXJpZyB1c2VyXHJcbiAgICBjb25zdCB1c2VyUmVnaXN0ZXJlZCA9IGF3YWl0IGZpcmViYXNlQ2xpZW50XHJcbiAgICAgIC5hdXRoKClcclxuICAgICAgLmNyZWF0ZVVzZXJXaXRoRW1haWxBbmRQYXNzd29yZChkYXRhLmVtYWlsLCBkYXRhLnBhc3N3b3JkKTtcclxuICAgIGNvbnN0IHsgdWlkIH0gPSB1c2VyUmVnaXN0ZXJlZC51c2VyO1xyXG5cclxuICAgIGNvbnN0IGRhdGFUb1NhdmUgPSBPYmplY3Qua2V5cyhkYXRhKS5yZWR1Y2UoKGFjY3VtLCBrZXkpID0+IHtcclxuICAgICAgcmV0dXJuICFbJ3Bhc3N3b3JkJywgJ3Bhc3N3b3JkQ29uZmlybWF0aW9uJ10uaW5jbHVkZXMoa2V5KSBcclxuICAgICAgICA/IE9iamVjdC5hc3NpZ24oYWNjdW0sIHsgW2tleV06IGRhdGFba2V5XSB9KSBcclxuICAgICAgICA6IGFjY3VtO1xyXG4gICAgfSwge30pO1xyXG4gICAgYXdhaXQgZGJcclxuICAgICAgLmNvbGxlY3Rpb24oJ3VzZXJzJylcclxuICAgICAgLmRvYyh1aWQpXHJcbiAgICAgIC5zZXQoZGF0YVRvU2F2ZSk7XHJcbiAgICBhd2FpdCBmaXJlYmFzZUFkbWluLmF1dGgoKS5zZXRDdXN0b21Vc2VyQ2xhaW1zKHVpZCwgeyBpc1JlZ2lzdGVyZWQ6IHRydWUgfSk7XHJcblxyXG4gICAgLy8gbG9naW5nIHVzZXIgdG8gcmVmcmVzaCBjdXN0b20gY2xhaW1zXHJcbiAgICBjb25zdCByZXNwb25zZSA9IGF3YWl0IGZpcmViYXNlQ2xpZW50XHJcbiAgICAgIC5hdXRoKClcclxuICAgICAgLnNpZ25JbldpdGhFbWFpbEFuZFBhc3N3b3JkKGRhdGEuZW1haWwsIGRhdGEucGFzc3dvcmQpO1xyXG4gICAgY29uc3QgdG9rZW4gPSBhd2FpdCByZXNwb25zZS51c2VyLmdldElkVG9rZW4oKTtcclxuXHJcbiAgICByZXMuanNvbih7IHRva2VuIH0pO1xyXG4gIH0gY2F0Y2ggKGUpIHtcclxuICAgIGNvbnN0IGVycm9yID0gZ2V0QmFja2VuZEVycm9yKGUpO1xyXG4gICAgcmVzLnN0YXR1cyg1MDApLmpzb24oZXJyb3IpO1xyXG4gIH1cclxufSk7XHJcblxyXG5leHBvcnQgZGVmYXVsdCByb3V0ZXI7XHJcbiIsImltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCBzdG9yZSBmcm9tICcuL3N0b3JlL3N0b3JlJztcclxuaW1wb3J0IHsgZ2V0Q3VycmVuY3lDb252ZXJzaW9ucywgc2V0Q3VycmVuY3lDb252ZXJzaW9ucywgc2V0UHJlZmVyZW5jZXMgfSBmcm9tICcuL3N0b3JlL2FjdGlvbnMvY29uZmlnLWFjdGlvbnMnO1xyXG5pbXBvcnQgeyB1cGRhdGVVc2VyLCBsb2dvdXQgYXMgbG9nb3V0QWN0aW9uIH0gZnJvbSAnLi9zdG9yZS9hY3Rpb25zL3VzZXItYWN0aW9ucyc7XHJcblxyXG5jb25zdCBjaGFuZ2VDdXJyZW5jeSA9IGFzeW5jIGN1cnJlbmN5ID0+IHtcclxuICB0cnkge1xyXG4gICAgY29uc3QgeyB1c2VyIH0gPSBzdG9yZS5nZXRTdGF0ZSgpO1xyXG4gICAgY29uc3QgeyBhdXRoIH0gPSB1c2VyO1xyXG5cclxuICAgIGF3YWl0IHN0b3JlLmRpc3BhdGNoKGdldEN1cnJlbmN5Q29udmVyc2lvbnMoeyBjdXJyZW5jeSwgZGF0ZTogJ2xhdGVzdCcgfSkpO1xyXG4gICAgc3RvcmUuZGlzcGF0Y2goc2V0Q3VycmVuY3lDb252ZXJzaW9ucyhzdG9yZS5nZXRTdGF0ZSgpLmNvbmZpZy50ZW1wKSk7XHJcbiAgICBsb2NhbFN0b3JhZ2UuY3VycmVuY3kgPSBjdXJyZW5jeTtcclxuICAgIHN0b3JlLmRpc3BhdGNoKHNldFByZWZlcmVuY2VzKHsgY3VycmVuY3k6IGN1cnJlbmN5IH0pKTtcclxuXHJcbiAgICBpZiAoYXV0aCkge1xyXG4gICAgICBjb25zdCBuZXdQcmVmZXJlbmNlcyA9IHsgLi4uYXV0aC5wcm9maWxlLnByZWZlcmVuY2VzLCBjdXJyZW5jeSB9O1xyXG4gICAgICBzdG9yZS5kaXNwYXRjaCh1cGRhdGVVc2VyKHsgaWQ6IGF1dGgudWlkLCBwcmVmZXJlbmNlczogbmV3UHJlZmVyZW5jZXMgfSkpO1xyXG4gICAgfVxyXG4gIH0gY2F0Y2ggKGUpIHtcclxuICAgIGNvbnNvbGUuZXJyb3IoZSk7XHJcbiAgfVxyXG59O1xyXG5cclxuY29uc3QgY2hhbmdlRGF0ZUZvcm1hdCA9IGFzeW5jIGRhdGVGb3JtYXQgPT4ge1xyXG4gIHRyeSB7XHJcbiAgICBjb25zdCB7IHVzZXIgfSA9IHN0b3JlLmdldFN0YXRlKCk7XHJcbiAgICBjb25zdCB7IGF1dGggfSA9IHVzZXI7XHJcblxyXG4gICAgbG9jYWxTdG9yYWdlLmRhdGVGb3JtYXQgPSBkYXRlRm9ybWF0O1xyXG4gICAgc3RvcmUuZGlzcGF0Y2goc2V0UHJlZmVyZW5jZXMoeyBkYXRlRm9ybWF0IH0pKTtcclxuXHJcbiAgICBpZiAoYXV0aCkge1xyXG4gICAgICBjb25zdCBuZXdQcmVmZXJlbmNlcyA9IHsgLi4uYXV0aC5wcm9maWxlLnByZWZlcmVuY2VzLCBkYXRlRm9ybWF0IH07XHJcbiAgICAgIHN0b3JlLmRpc3BhdGNoKHVwZGF0ZVVzZXIoeyBpZDogYXV0aC51aWQsIHByZWZlcmVuY2VzOiBuZXdQcmVmZXJlbmNlcyB9KSk7XHJcbiAgICB9XHJcbiAgfSBjYXRjaCAoZSkge1xyXG4gICAgY29uc29sZS5lcnJvcihlKTtcclxuICB9XHJcbn07XHJcblxyXG5jb25zdCBjaGFuZ2VMYW5ndWFnZSA9IGFzeW5jIGxhbmd1YWdlID0+IHtcclxuICB0cnkge1xyXG4gICAgY29uc3QgeyB1c2VyIH0gPSBzdG9yZS5nZXRTdGF0ZSgpO1xyXG4gICAgY29uc3QgeyBhdXRoIH0gPSB1c2VyO1xyXG4gICAgbG9jYWxTdG9yYWdlLmxhbmd1YWdlID0gbGFuZ3VhZ2U7XHJcbiAgICBzdG9yZS5kaXNwYXRjaChzZXRQcmVmZXJlbmNlcyh7IGxhbmd1YWdlOiBsYW5ndWFnZSB9KSk7XHJcblxyXG4gICAgaWYgKGF1dGgpIHtcclxuICAgICAgY29uc3QgbmV3UHJlZmVyZW5jZXMgPSB7IC4uLmF1dGgucHJvZmlsZS5wcmVmZXJlbmNlcywgbGFuZ3VhZ2UgfTtcclxuICAgICAgc3RvcmUuZGlzcGF0Y2godXBkYXRlVXNlcih7IGlkOiBhdXRoLnVpZCwgcHJlZmVyZW5jZXM6IG5ld1ByZWZlcmVuY2VzIH0pKTtcclxuICAgIH1cclxuICB9IGNhdGNoIChlKSB7XHJcbiAgICBjb25zb2xlLmVycm9yKGUpO1xyXG4gIH1cclxufTtcclxuXHJcbmNvbnN0IGxvYWRQcmVmZXJlbmNlcyA9ICgpID0+IHtcclxuICBjb25zdCB7IGNvbmZpZywgdXNlciB9ID0gc3RvcmUuZ2V0U3RhdGUoKTtcclxuICBjb25zdCBkZWZhdWx0UHJlZmVyZW5jZXMgPSBjb25maWcucHJlZmVyZW5jZXM7XHJcbiAgY29uc3QgdXNlclByZWZlcmVuY2VzID0gKHVzZXIuYXV0aCAmJiB1c2VyLmF1dGgucHJvZmlsZSAmJiB1c2VyLmF1dGgucHJvZmlsZS5wcmVmZXJlbmNlcyk7XHJcbiAgY29uc3QgbG9jYWxQcmVmZXJlbmNlcyA9IHtcclxuICAgIGN1cnJlbmN5OiBsb2NhbFN0b3JhZ2UuY3VycmVuY3kgfHwgZGVmYXVsdFByZWZlcmVuY2VzLmN1cnJlbmN5LFxyXG4gICAgZGF0ZUZvcm1hdDogbG9jYWxTdG9yYWdlLmRhdGVGb3JtYXQgfHwgZGVmYXVsdFByZWZlcmVuY2VzLmRhdGVGb3JtYXQsXHJcbiAgICBsYW5ndWFnZTogbG9jYWxTdG9yYWdlLmxhbmd1YWdlIHx8IGRlZmF1bHRQcmVmZXJlbmNlcy5sYW5ndWFnZVxyXG4gIH07XHJcbiAgY29uc3QgcHJlZmVyZW5jZXMgPSBPYmplY3QuYXNzaWduKFxyXG4gICAgZGVmYXVsdFByZWZlcmVuY2VzLCBcclxuICAgIGxvY2FsUHJlZmVyZW5jZXMsIFxyXG4gICAgdXNlclByZWZlcmVuY2VzXHJcbiAgKTtcclxuICBjb25zdCB7IGN1cnJlbmN5IH0gPSBwcmVmZXJlbmNlczsgICBcclxuXHJcbiAgc3RvcmUuZGlzcGF0Y2goZ2V0Q3VycmVuY3lDb252ZXJzaW9ucyh7IGN1cnJlbmN5LCBkYXRlOiAnbGF0ZXN0JyB9KSlcclxuICAgIC50aGVuKCgpID0+IHN0b3JlLmRpc3BhdGNoKHNldEN1cnJlbmN5Q29udmVyc2lvbnMoc3RvcmUuZ2V0U3RhdGUoKS5jb25maWcudGVtcCkpKTtcclxuICAgIFxyXG4gIHN0b3JlLmRpc3BhdGNoKHNldFByZWZlcmVuY2VzKHByZWZlcmVuY2VzKSk7XHJcbn07XHJcblxyXG5jb25zdCBsb2dvdXQgPSBhc3luYyAoKSA9PiB7XHJcbiAgdHJ5IHtcclxuICAgIGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKCd0b2tlbicpO1xyXG4gICAgYXdhaXQgc3RvcmUuZGlzcGF0Y2gobG9nb3V0QWN0aW9uKCkpO1xyXG4gICAgd2luZG93LmxvY2F0aW9uLmhyZWYgPSBgJHt3aW5kb3cubG9jYXRpb24ub3JpZ2lufS9sb2dpbmA7XHJcbiAgfSBjYXRjaCAoZSkge1xyXG4gICAgY29uc29sZS5lcnJvcihlKTtcclxuICB9XHJcbn07XHJcblxyXG5leHBvcnQgZGVmYXVsdCBSZWFjdC5jcmVhdGVDb250ZXh0KHtcclxuICBjaGFuZ2VDdXJyZW5jeSxcclxuICBjaGFuZ2VEYXRlRm9ybWF0LFxyXG4gIGNoYW5nZUxhbmd1YWdlLFxyXG4gIGxvYWRQcmVmZXJlbmNlcyxcclxuICBsb2dvdXRcclxufSk7IiwiaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIENvbGxhcHNpYmxlIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcclxuXHJcbiAgY29tcG9uZW50RGlkVXBkYXRlID0gcHJldlByb3BzID0+IHtcclxuICAgIGlmIChwcmV2UHJvcHMuaXNPcGVuICE9PSB0aGlzLnByb3BzLmlzT3Blbikge1xyXG4gICAgICB0aGlzLmNvbGxhcHNpYmxlLmNsYXNzTGlzdC50b2dnbGUoJ2NvbGxhcHNpbmcnKTtcclxuXHJcbiAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xyXG4gICAgICAgIHRoaXMuY29sbGFwc2libGUuY2xhc3NMaXN0LnRvZ2dsZSgnY29sbGFwc2luZycpO1xyXG4gICAgICAgIFxyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLmlzT3Blbikge1xyXG4gICAgICAgICAgdGhpcy5wcm9wcy5vbkVudGVyaW5nKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9LCAwKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHJlbmRlciAoKSB7XHJcbiAgICBjb25zdCB7IGlzT3BlbiwgY2xhc3NOYW1lLCBjaGlsZHJlbiB9ID0gdGhpcy5wcm9wcztcclxuXHJcbiAgICByZXR1cm4gKFxyXG4gICAgICA8ZGl2IFxyXG4gICAgICAgIHJlZj17ZWxlbSA9PiB0aGlzLmNvbGxhcHNpYmxlID0gZWxlbX1cclxuICAgICAgICBhcmlhLWV4cGFuZGVkPXtpc09wZW59XHJcbiAgICAgICAgY2xhc3NOYW1lPXtgJHtjbGFzc05hbWV9IGNvbGxhcHNlICR7aXNPcGVuICYmICdzaG93J31gfT5cclxuICAgICAgICB7Y2hpbGRyZW59XHJcbiAgICAgIDwvZGl2PlxyXG4gICAgKTtcclxuICB9XHJcbn0iLCJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgeyBMaW5rIH0gZnJvbSAncmVhY3Qtcm91dGVyLWRvbSc7XHJcbmltcG9ydCB7IHRyYW5zbGF0ZSB9IGZyb20gJy4uLy4uL3V0aWxzL2ludGwtdXRpbHMnO1xyXG5pbXBvcnQgc3R5bGVkIGZyb20gJ3N0eWxlZC1jb21wb25lbnRzJztcclxuXHJcbmNvbnN0IEZvb3RlcldyYXBwZXIgPSBzdHlsZWQuZm9vdGVyYFxyXG4gICYsICoge1xyXG4gICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICB9XHJcblxyXG4gIC5jb250YWluZXIge1xyXG4gICAgLnNsb2dhbiBzcGFuIHtcclxuICAgICAgZm9udC1zaXplOiAxLjI1cmVtO1xyXG4gICAgfVxyXG5cclxuICAgIC53cmFwcGVyLWJ1dHRvbnMge1xyXG4gICAgICBidXR0b24ge1xyXG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgIG1hcmdpbjogMCBhdXRvO1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbmA7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBGb290ZXIocHJvcHMpIHtcclxuICBjb25zdCB7IGNvbmZpZyB9ID0gcHJvcHM7XHJcblxyXG4gIHJldHVybiAoXHJcbiAgICA8Rm9vdGVyV3JhcHBlciBjbGFzc05hbWU9XCJtdC00IG1iLTQgcHQtNCBib3JkZXItdG9wXCI+XHJcbiAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29udGFpbmVyIHRleHQtYm9keVwiPlxyXG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicm93XCI+XHJcbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC1tZC03IHJvd1wiPlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC0xMiBzbG9nYW5cIj5cclxuICAgICAgICAgICAgICA8aW1nIHNyYz17Y29uZmlnLmFwcExvZ299IGFsdD17Y29uZmlnLmFwcE5hbWV9IHdpZHRoPXszNX0gLz57JyAnfVxyXG4gICAgICAgICAgICAgIDxzcGFuPntjb25maWcuYXBwTmFtZX08L3NwYW4+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC0xMlwiPlxyXG4gICAgICAgICAgICAgIDxwPnt0cmFuc2xhdGUoY29uZmlnLnRyYW5zbGF0aW9ucywgJ2hvbWVEZXNjcmlwdGlvbicpfTwvcD5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sLTEyXCI+XHJcbiAgICAgICAgICAgICAgPHVsIGNsYXNzTmFtZT1cImxpc3QtaW5saW5lXCI+XHJcbiAgICAgICAgICAgICAgICA8bGkgY2xhc3NOYW1lPVwibGlzdC1pbmxpbmUtaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICA8YVxyXG4gICAgICAgICAgICAgICAgICAgIGhyZWY9XCJodHRwczovL3d3dy5pbnN0YWdyYW0uY29tL2xpZGVyZXNzb2NpYWxlMy9cIlxyXG4gICAgICAgICAgICAgICAgICAgIHRpdGxlPVwiSW5zdGFncmFtXCJcclxuICAgICAgICAgICAgICAgICAgICB0YXJnZXQ9XCJfYmxhbmtcIlxyXG4gICAgICAgICAgICAgICAgICAgIHJlbD1cIm5vb3BlbmVyIG5vcmVmZXJyZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICA8aSBjbGFzc05hbWU9XCJmYWIgZmEtaW5zdGFncmFtIGZhLTJ4IHRleHQtZGFya1wiIC8+XHJcbiAgICAgICAgICAgICAgICAgIDwvYT5cclxuICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICA8bGkgY2xhc3NOYW1lPVwibGlzdC1pbmxpbmUtaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICA8YVxyXG4gICAgICAgICAgICAgICAgICAgIGhyZWY9XCJodHRwczovL3d3dy5mYWNlYm9vay5jb20vTGlkZXJlcy1Tb2NpYWxlcy0xMDY5MDI0MTc2ODAyNDAvXCJcclxuICAgICAgICAgICAgICAgICAgICB0aXRsZT1cIkZhY2Vib29rXCJcclxuICAgICAgICAgICAgICAgICAgICB0YXJnZXQ9XCJfYmxhbmtcIlxyXG4gICAgICAgICAgICAgICAgICAgIHJlbD1cIm5vb3BlbmVyIG5vcmVmZXJyZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICA8aSBjbGFzc05hbWU9XCJmYWIgZmEtZmFjZWJvb2sgZmEtMnggdGV4dC1kYXJrXCIgLz5cclxuICAgICAgICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgIDxsaSBjbGFzc05hbWU9XCJsaXN0LWlubGluZS1pdGVtXCI+XHJcbiAgICAgICAgICAgICAgICAgIDxhXHJcbiAgICAgICAgICAgICAgICAgICAgaHJlZj1cImh0dHBzOi8vdHdpdHRlci5jb20vTGlkZXJlc1NvY2lhbGUzXCJcclxuICAgICAgICAgICAgICAgICAgICB0aXRsZT1cIlR3aXR0ZXJcIlxyXG4gICAgICAgICAgICAgICAgICAgIHRhcmdldD1cIl9ibGFua1wiXHJcbiAgICAgICAgICAgICAgICAgICAgcmVsPVwibm9vcGVuZXIgbm9yZWZlcnJlclwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzTmFtZT1cImZhYiBmYS10d2l0dGVyIGZhLTJ4IHRleHQtZGFya1wiIC8+XHJcbiAgICAgICAgICAgICAgICAgIDwvYT5cclxuICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICA8bGkgY2xhc3NOYW1lPVwibGlzdC1pbmxpbmUtaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICA8YVxyXG4gICAgICAgICAgICAgICAgICAgIGhyZWY9XCJtYWlsdG86bGlkZXJlc3NvY2lhbGVzY29sb21iaWFAZ21haWwuY29tXCJcclxuICAgICAgICAgICAgICAgICAgICB0aXRsZT1cIkdtYWlsXCJcclxuICAgICAgICAgICAgICAgICAgICB0YXJnZXQ9XCJfYmxhbmtcIlxyXG4gICAgICAgICAgICAgICAgICAgIHJlbD1cIm5vb3BlbmVyIG5vcmVmZXJyZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICA8aSBjbGFzc05hbWU9XCJmYXMgZmEtZW52ZWxvcGUgZmEtMnggdGV4dC1kYXJrXCIvPlxyXG4gICAgICAgICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgIDwvdWw+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC1tZC01XCI+XHJcbiAgICAgICAgICAgIDx1bCBjbGFzc05hbWU9XCJsaXN0LWdyb3VwIGxpc3QtZ3JvdXAtZmx1c2hcIj5cclxuICAgICAgICAgICAgICA8bGkgY2xhc3NOYW1lPVwibGlzdC1ncm91cC1pdGVtXCI+XHJcbiAgICAgICAgICAgICAgICA8TGluayB0bz1cIi9jb250YWN0XCI+e3RyYW5zbGF0ZShjb25maWcudHJhbnNsYXRpb25zLCAnbWFpbENvbnRhY3RTdWJqZWN0Jyl9PC9MaW5rPlxyXG4gICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgPGxpIGNsYXNzTmFtZT1cImxpc3QtZ3JvdXAtaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgPExpbmsgdG89XCIvYXBwL2Rhc2hib2FyZFwiPnt0cmFuc2xhdGUoY29uZmlnLnRyYW5zbGF0aW9ucywgJ2NvbXBsYWludCcpfTwvTGluaz5cclxuICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICA8L3VsPlxyXG4gICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgIDwvZGl2PlxyXG4gICAgPC9Gb290ZXJXcmFwcGVyPlxyXG4gICk7XHJcbn1cclxuIiwiaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IHN0eWxlZCBmcm9tICdzdHlsZWQtY29tcG9uZW50cyc7XHJcbmltcG9ydCBOYXZiYXIgZnJvbSAnLi9uYXZiYXInO1xyXG5pbXBvcnQgTmF2aWdhdGlvbkJhciBmcm9tICcuL25hdmlnYXRpb24tYmFyJztcclxuXHJcbmNvbnN0IEhlYWRlcldyYXBwZXIgPSBzdHlsZWQuaGVhZGVyYFxyXG4gICYsICoge1xyXG4gICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICB9XHJcbmA7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBIZWFkZXIocHJvcHMpIHtcclxuICBjb25zdCB7IGNvbmZpZywgdXNlciwgdGl0bGUsIGRlc2NyaXB0aW9uLCBoaXN0b3J5LCBzaG93SGVhZGluZywgYnRuTGVmdCwgYnRuUmlnaHQgfSA9IHByb3BzO1xyXG5cclxuICByZXR1cm4gKFxyXG4gICAgPEhlYWRlcldyYXBwZXI+XHJcbiAgICAgIDxOYXZiYXJcclxuICAgICAgICBjb25maWc9e2NvbmZpZ31cclxuICAgICAgICB1c2VyPXt1c2VyfVxyXG4gICAgICAgIGhpc3Rvcnk9e2hpc3Rvcnl9PlxyXG4gICAgICA8L05hdmJhcj5cclxuICAgICAge1xyXG4gICAgICAgIHNob3dIZWFkaW5nICYmXHJcbiAgICAgICAgPE5hdmlnYXRpb25CYXJcclxuICAgICAgICAgIGNvbmZpZz17Y29uZmlnfVxyXG4gICAgICAgICAgdGl0bGU9e3RpdGxlfVxyXG4gICAgICAgICAgZGVzY3JpcHRpb249e2Rlc2NyaXB0aW9ufVxyXG4gICAgICAgICAgYnRuTGVmdD17YnRuTGVmdH1cclxuICAgICAgICAgIGJ0blJpZ2h0PXtidG5SaWdodH0+XHJcbiAgICAgICAgPC9OYXZpZ2F0aW9uQmFyPlxyXG4gICAgICB9XHJcbiAgICA8L0hlYWRlcldyYXBwZXI+XHJcbiAgKTtcclxufVxyXG4iLCJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgeyB0cmFuc2xhdGUgfSBmcm9tICcuLi8uLi91dGlscy9pbnRsLXV0aWxzJztcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIE1lbnVNYWluKHByb3BzKSB7XHJcbiAgY29uc3QgeyBhdXRoLCBhdXRoTG9hZGVkLCBjb25maWcsIGN1cnJlbnRQYXRoLCBvbkNoYW5nZVBhdGggfSA9IHByb3BzO1xyXG5cclxuICByZXR1cm4gKFxyXG4gICAgPHVsIGNsYXNzTmFtZT1cIm1sLWF1dG8gbmF2YmFyLW5hdlwiPlxyXG4gICAgICB7LyogQVBQICovfVxyXG4gICAgICB7YXV0aExvYWRlZCAmJiBhdXRoICYmIGF1dGguY2xhaW1zLmZpbmQoaXRlbSA9PiBpdGVtID09PSAnaXNSZWdpc3RlcmVkJykgJiYgKFxyXG4gICAgICAgIDxSZWFjdC5GcmFnbWVudD5cclxuICAgICAgICAgIDxhXHJcbiAgICAgICAgICAgIHJvbGU9XCJtZW51aXRlbVwiXHJcbiAgICAgICAgICAgIGNsYXNzTmFtZT17YG5hdi1saW5rICR7Y3VycmVudFBhdGggPT09ICcvYXBwL2Rhc2hib2FyZCcgJiYgJ2FjdGl2ZSd9YH1cclxuICAgICAgICAgICAgaHJlZj1cIi9hcHAvZGFzaGJvYXJkXCJcclxuICAgICAgICAgICAgdGl0bGU9e3RyYW5zbGF0ZShjb25maWcudHJhbnNsYXRpb25zLCAnY29tcGxhaW50Jyl9XHJcbiAgICAgICAgICAgIG9uQ2xpY2s9e2UgPT4ge1xyXG4gICAgICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICAgICAgICBvbkNoYW5nZVBhdGgoZS5jdXJyZW50VGFyZ2V0LnBhdGhuYW1lKTtcclxuICAgICAgICAgICAgfX0+XHJcbiAgICAgICAgICAgIDxpIGNsYXNzTmFtZT1cImZhcyBmYS1hc3Npc3RpdmUtbGlzdGVuaW5nLXN5c3RlbXMgbXItMVwiPjwvaT5cclxuICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwiZC1tZC1ub25lIGQtbGctaW5saW5lLWJsb2NrXCI+XHJcbiAgICAgICAgICAgICAge3RyYW5zbGF0ZShjb25maWcudHJhbnNsYXRpb25zLCAnY29tcGxhaW50Jyl9XHJcbiAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgIDwvYT5cclxuICAgICAgICA8L1JlYWN0LkZyYWdtZW50PlxyXG4gICAgICApfVxyXG4gICAgICB7LyogSE9NRSAqL31cclxuICAgICAgeyFhdXRoICYmIChcclxuICAgICAgICA8UmVhY3QuRnJhZ21lbnQ+XHJcbiAgICAgICAgICA8YVxyXG4gICAgICAgICAgICByb2xlPVwibWVudWl0ZW1cIlxyXG4gICAgICAgICAgICBjbGFzc05hbWU9e2BuYXYtbGluayAke2N1cnJlbnRQYXRoID09PSAnLycgJiYgJ2FjdGl2ZSd9YH1cclxuICAgICAgICAgICAgaHJlZj1cIi9cIlxyXG4gICAgICAgICAgICBvbkNsaWNrPXtlID0+IHtcclxuICAgICAgICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgICAgICAgb25DaGFuZ2VQYXRoKGUuY3VycmVudFRhcmdldC5wYXRobmFtZSk7XHJcbiAgICAgICAgICAgIH19PlxyXG4gICAgICAgICAgICB7dHJhbnNsYXRlKGNvbmZpZy50cmFuc2xhdGlvbnMsICdob21lJyl9XHJcbiAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICA8YVxyXG4gICAgICAgICAgICByb2xlPVwibWVudWl0ZW1cIlxyXG4gICAgICAgICAgICBjbGFzc05hbWU9e2BuYXYtbGluayBidG4gYnRuLWluZm8gdGV4dC13aGl0ZSBtbC1tZC0yIG1iLTIgbWItbWQtMCAke2N1cnJlbnRQYXRoID09PSAnL2xvZ2luJyAmJiAnYWN0aXZlJ31gfVxyXG4gICAgICAgICAgICBocmVmPVwiL2xvZ2luXCJcclxuICAgICAgICAgICAgb25DbGljaz17ZSA9PiB7XHJcbiAgICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgICAgICAgIG9uQ2hhbmdlUGF0aChlLmN1cnJlbnRUYXJnZXQucGF0aG5hbWUpO1xyXG4gICAgICAgICAgICB9fT5cclxuICAgICAgICAgICAge3RyYW5zbGF0ZShjb25maWcudHJhbnNsYXRpb25zLCAnbG9naW4nKX1cclxuICAgICAgICAgIDwvYT5cclxuICAgICAgICAgIDxhXHJcbiAgICAgICAgICAgIHJvbGU9XCJtZW51aXRlbVwiXHJcbiAgICAgICAgICAgIGNsYXNzTmFtZT17YG5hdi1saW5rIGJ0biBidG4tcHJpbWFyeSB0ZXh0LXdoaXRlIG1sLW1kLTIgJHtjdXJyZW50UGF0aCA9PT0gJy9yZWdpc3RlcicgJiYgJ2FjdGl2ZSd9YH1cclxuICAgICAgICAgICAgaHJlZj1cIi9yZWdpc3RlclwiXHJcbiAgICAgICAgICAgIG9uQ2xpY2s9e2UgPT4ge1xyXG4gICAgICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICAgICAgICBvbkNoYW5nZVBhdGgoZS5jdXJyZW50VGFyZ2V0LnBhdGhuYW1lKTtcclxuICAgICAgICAgICAgfX0+XHJcbiAgICAgICAgICAgIHt0cmFuc2xhdGUoY29uZmlnLnRyYW5zbGF0aW9ucywgJ3JlZ2lzdGVyJyl9XHJcbiAgICAgICAgICA8L2E+XHJcbiAgICAgICAgPC9SZWFjdC5GcmFnbWVudD5cclxuICAgICAgKX1cclxuICAgIDwvdWw+XHJcbiAgKTtcclxufSIsImltcG9ydCBSZWFjdCwgeyB1c2VTdGF0ZSB9IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IFNlbGVjdCBmcm9tICdyZWFjdC1zZWxlY3QnO1xyXG5pbXBvcnQgUG9wb3ZlciBmcm9tICcuLi9fY29tcG9uZW50cy9wb3BvdmVyJztcclxuaW1wb3J0IHsgdHJhbnNsYXRlIH0gZnJvbSAnLi4vLi4vdXRpbHMvaW50bC11dGlscyc7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBNZW51UG9wb3ZlcnMocHJvcHMpIHtcclxuICBjb25zdCBbZWxlbVBvcG92ZXJBZG1pbiwgc2V0RWxlbVBvcG92ZXJBZG1pbl0gPSB1c2VTdGF0ZShudWxsKTtcclxuICBjb25zdCBbZWxlbVBvcG92ZXJQcmVmZXJlbmNlcywgc2V0RWxlbVBvcG92ZXJQcmVmZXJlbmNlc10gPSB1c2VTdGF0ZShudWxsKTtcclxuICBjb25zdCBbZWxlbVBvcG92ZXJTZXNzaW9uLCBzZXRFbGVtUG9wb3ZlclNlc3Npb25dID0gdXNlU3RhdGUobnVsbCk7XHJcbiAgY29uc3QgW3Nob3dQb3BvdmVyQWRtaW4sIHNldFBvcG92ZXJBZG1pbl0gPSB1c2VTdGF0ZShmYWxzZSk7XHJcbiAgY29uc3QgW3Nob3dQb3BvdmVyUHJlZmVyZW5jZXMsIHNldFBvcG92ZXJQcmVmZXJlbmNlc10gPSB1c2VTdGF0ZShmYWxzZSk7XHJcbiAgY29uc3QgW3Nob3dQb3BvdmVyU2Vzc2lvbiwgc2V0UG9wb3ZlclNlc3Npb25dID0gdXNlU3RhdGUoZmFsc2UpO1xyXG5cclxuICBjb25zdCB7IFxyXG4gICAgYXV0aCwgYXV0aExvYWRlZCwgY29uZmlnLCBcclxuICAgIG9uQ2hhbmdlUGF0aCwgb25DaGFuZ2VEYXRlRm9ybWF0LCBvbkNoYW5nZUxhbmd1YWdlLCBvbkxvZ291dCBcclxuICB9ID0gcHJvcHM7XHJcbiAgLy8gY29uc3QgeyBcclxuICAvLyAgIGF1dGgsIGF1dGhMb2FkZWQsIGNvbmZpZywgXHJcbiAgLy8gICBvbkNoYW5nZVBhdGgsIG9uQ2hhbmdlQ3VycmVuY3ksIG9uQ2hhbmdlRGF0ZUZvcm1hdCwgb25DaGFuZ2VMYW5ndWFnZSwgb25Mb2dvdXQgXHJcbiAgLy8gfSA9IHByb3BzO1xyXG4gIC8vIGNvbnN0IGN1cnJlbmNpZXMgPSBjb25maWcuaW50bERhdGEuY3VycmVuY2llcyB8fCBbXTsgXHJcbiAgY29uc3QgZGF0ZUZvcm1hdHMgPSBbJ0REL01NL1lZWVknLCAnTU0vREQvWVlZWScsICdZWVlZL01NL0REJ10ubWFwKGl0ZW0gPT4gKHsgbGFiZWw6IGl0ZW0sIHZhbHVlOiBpdGVtIH0pKTtcclxuICBjb25zdCBsYW5ndWFnZXMgPSBjb25maWcuaW50bERhdGEubGFuZ3VhZ2VzIHx8IFtdO1xyXG5cclxuICBjb25zdCBjbGlja0FkbWluTWVudSA9IHBhdGggPT4ge1xyXG4gICAgc2V0UG9wb3ZlckFkbWluKGZhbHNlKTtcclxuICAgIG9uQ2hhbmdlUGF0aChwYXRoKTtcclxuICB9O1xyXG5cclxuICBjb25zdCBjbGlja1Nlc3Npb25NZW51ID0gcGF0aCA9PiB7XHJcbiAgICBzZXRQb3BvdmVyU2Vzc2lvbihmYWxzZSk7XHJcbiAgICBvbkNoYW5nZVBhdGgocGF0aCk7XHJcbiAgfTtcclxuXHJcbiAgcmV0dXJuIChcclxuICAgIDxSZWFjdC5GcmFnbWVudD5cclxuICAgICAgey8qIFBPUE9WRVIgUFJFRkVSRU5DRVMgKi99XHJcbiAgICAgICAgPGJ1dHRvblxyXG4gICAgICAgIHJlZj17ZWxlbSA9PiBzZXRFbGVtUG9wb3ZlclByZWZlcmVuY2VzKGVsZW0pfVxyXG4gICAgICAgIGNsYXNzTmFtZT1cImJ0biBidG4tbGlnaHQgbWwtMVwiXHJcbiAgICAgICAgdGl0bGU9e3RyYW5zbGF0ZShjb25maWcudHJhbnNsYXRpb25zLCAncHJlZmVyZW5jZXMnKX1cclxuICAgICAgICBvbkNsaWNrPXsoKSA9PiBzZXRQb3BvdmVyUHJlZmVyZW5jZXMoIXNob3dQb3BvdmVyUHJlZmVyZW5jZXMpfT5cclxuICAgICAgICA8aSBjbGFzc05hbWU9XCJmYXMgZmEtY29nXCIgLz5cclxuICAgICAgPC9idXR0b24+XHJcbiAgICAgIDxQb3BvdmVyXHJcbiAgICAgICAgaXNPcGVuPXtzaG93UG9wb3ZlclByZWZlcmVuY2VzfVxyXG4gICAgICAgIHRhcmdldD17ZWxlbVBvcG92ZXJQcmVmZXJlbmNlc31cclxuICAgICAgICBzdHlsZT17eyB3aWR0aDogMjAwIH19XHJcbiAgICAgICAgb25Ub2dnbGU9eygpID0+IHNldFBvcG92ZXJQcmVmZXJlbmNlcyghc2hvd1BvcG92ZXJQcmVmZXJlbmNlcyl9PlxyXG4gICAgICAgIDxoMyBjbGFzc05hbWU9XCJwb3BvdmVyLWhlYWRlclwiPnt0cmFuc2xhdGUoY29uZmlnLnRyYW5zbGF0aW9ucywgJ3ByZWZlcmVuY2VzJyl9PC9oMz5cclxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInBvcG92ZXItYm9keVwiPlxyXG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJmb3JtLWdyb3VwXCI+XHJcbiAgICAgICAgICAgIDxsYWJlbD57dHJhbnNsYXRlKGNvbmZpZy50cmFuc2xhdGlvbnMsICdkYXRlRm9ybWF0Jyl9PC9sYWJlbD5cclxuICAgICAgICAgICAgPFNlbGVjdFxyXG4gICAgICAgICAgICAgIHBsYWNlaG9sZGVyPXt0cmFuc2xhdGUoY29uZmlnLnRyYW5zbGF0aW9ucywgJ3NlbGVjdCcpfVxyXG4gICAgICAgICAgICAgIG9wdGlvbnM9e2RhdGVGb3JtYXRzfVxyXG4gICAgICAgICAgICAgIHZhbHVlPXtkYXRlRm9ybWF0cy5maW5kKGl0ZW0gPT4gaXRlbS52YWx1ZSA9PT0gY29uZmlnLnByZWZlcmVuY2VzLmRhdGVGb3JtYXQpfVxyXG4gICAgICAgICAgICAgIG9uQ2hhbmdlPXtvcHRpb24gPT4gb25DaGFuZ2VEYXRlRm9ybWF0KG9wdGlvbi52YWx1ZSl9IC8+XHJcbiAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZm9ybS1ncm91cFwiPlxyXG4gICAgICAgICAgICA8bGFiZWw+e3RyYW5zbGF0ZShjb25maWcudHJhbnNsYXRpb25zLCAnbGFuZ3VhZ2UnKX08L2xhYmVsPlxyXG4gICAgICAgICAgICA8U2VsZWN0XHJcbiAgICAgICAgICAgICAgcGxhY2Vob2xkZXI9e3RyYW5zbGF0ZShjb25maWcudHJhbnNsYXRpb25zLCAnc2VsZWN0Jyl9XHJcbiAgICAgICAgICAgICAgb3B0aW9ucz17bGFuZ3VhZ2VzLmZpbHRlcihpdGVtID0+IGNvbmZpZy5hcHBMYW5ndWFnZXMuaW5jbHVkZXMoaXRlbS52YWx1ZSkpfVxyXG4gICAgICAgICAgICAgIHZhbHVlPXtsYW5ndWFnZXMuZmluZChpdGVtID0+IGl0ZW0udmFsdWUgPT09IGNvbmZpZy5wcmVmZXJlbmNlcy5sYW5ndWFnZSl9XHJcbiAgICAgICAgICAgICAgb25DaGFuZ2U9e29wdGlvbiA9PiBvbkNoYW5nZUxhbmd1YWdlKG9wdGlvbi52YWx1ZSl9IC8+XHJcbiAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgIHsvKiA8ZGl2IGNsYXNzTmFtZT1cImZvcm0tZ3JvdXBcIj5cclxuICAgICAgICAgICAgPGxhYmVsPnt0cmFuc2xhdGUoY29uZmlnLnRyYW5zbGF0aW9ucywgJ2N1cnJlbmN5Jyl9PC9sYWJlbD5cclxuICAgICAgICAgICAgPFNlbGVjdFxyXG4gICAgICAgICAgICAgIHBsYWNlaG9sZGVyPXt0cmFuc2xhdGUoY29uZmlnLnRyYW5zbGF0aW9ucywgJ3NlbGVjdCcpfVxyXG4gICAgICAgICAgICAgIG9wdGlvbnM9e2N1cnJlbmNpZXN9XHJcbiAgICAgICAgICAgICAgdmFsdWU9e2N1cnJlbmNpZXMuZmluZChpdGVtID0+IGl0ZW0udmFsdWUgPT09IGNvbmZpZy5wcmVmZXJlbmNlcy5jdXJyZW5jeSl9XHJcbiAgICAgICAgICAgICAgb25DaGFuZ2U9e29wdGlvbiA9PiBvbkNoYW5nZUN1cnJlbmN5KG9wdGlvbi52YWx1ZSl9IC8+XHJcbiAgICAgICAgICA8L2Rpdj4gKi99XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgIDwvUG9wb3Zlcj5cclxuICAgICAgey8qIFBPUE9WRVIgQURNSU4gKi99XHJcbiAgICAgIHtcclxuICAgICAgICBhdXRoTG9hZGVkICYmIGF1dGggJiYgYXV0aC5jbGFpbXMuZmluZChpdGVtID0+IGl0ZW0gPT09ICdpc0FkbWluJykgJiZcclxuICAgICAgICA8UmVhY3QuRnJhZ21lbnQ+XHJcbiAgICAgICAgICA8YnV0dG9uXHJcbiAgICAgICAgICAgIHJlZj17ZWxlbSA9PiBzZXRFbGVtUG9wb3ZlckFkbWluKGVsZW0pfVxyXG4gICAgICAgICAgICBjbGFzc05hbWU9XCJidG4gYnRuLWxpZ2h0IG1sLTFcIlxyXG4gICAgICAgICAgICB0aXRsZT17dHJhbnNsYXRlKGNvbmZpZy50cmFuc2xhdGlvbnMsICdhZG1pbicpfVxyXG4gICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiBzZXRQb3BvdmVyQWRtaW4oIXNob3dQb3BvdmVyQWRtaW4pfT5cclxuICAgICAgICAgICAgPGkgY2xhc3NOYW1lPVwiZmFzIGZhLWxvY2tcIiAvPlxyXG4gICAgICAgICAgPC9idXR0b24+XHJcbiAgICAgICAgICA8UG9wb3ZlclxyXG4gICAgICAgICAgICBpc09wZW49e3Nob3dQb3BvdmVyQWRtaW59XHJcbiAgICAgICAgICAgIHRhcmdldD17ZWxlbVBvcG92ZXJBZG1pbn1cclxuICAgICAgICAgICAgc3R5bGU9e3sgd2lkdGg6IDIwMCB9fVxyXG4gICAgICAgICAgICBvblRvZ2dsZT17KCkgPT4gc2V0UG9wb3ZlckFkbWluKCFzaG93UG9wb3ZlckFkbWluKX0+XHJcbiAgICAgICAgICAgIDxoMyBjbGFzc05hbWU9XCJwb3BvdmVyLWhlYWRlclwiPnt0cmFuc2xhdGUoY29uZmlnLnRyYW5zbGF0aW9ucywgJ2FkbWluJyl9PC9oMz5cclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJwb3BvdmVyLWJvZHlcIj5cclxuICAgICAgICAgICAgICA8dWwgY2xhc3NOYW1lPVwibGlzdC1ncm91cCBsaXN0LWdyb3VwLWZsdXNoXCI+XHJcbiAgICAgICAgICAgICAgICA8bGkgY2xhc3NOYW1lPVwibGlzdC1ncm91cC1pdGVtXCI+XHJcbiAgICAgICAgICAgICAgICAgIDxidXR0b25cclxuICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJidG4gcC0wXCJcclxuICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiBjbGlja0FkbWluTWVudSgnL2FkbWluL2NhdGFsb2dzJyl9PlxyXG4gICAgICAgICAgICAgICAgICAgIHt0cmFuc2xhdGUoY29uZmlnLnRyYW5zbGF0aW9ucywgJ2NhdGFsb2dzJyl9XHJcbiAgICAgICAgICAgICAgICAgIDwvYnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgIDxsaSBjbGFzc05hbWU9XCJsaXN0LWdyb3VwLWl0ZW1cIj5cclxuICAgICAgICAgICAgICAgICAgPGJ1dHRvblxyXG4gICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cImJ0biBwLTBcIlxyXG4gICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IGNsaWNrQWRtaW5NZW51KCcvYWRtaW4vY29uZmlndXJhdGlvbicpfT5cclxuICAgICAgICAgICAgICAgICAgICB7dHJhbnNsYXRlKGNvbmZpZy50cmFuc2xhdGlvbnMsICdjb25maWd1cmF0aW9uJyl9XHJcbiAgICAgICAgICAgICAgICAgIDwvYnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgIDxsaSBjbGFzc05hbWU9XCJsaXN0LWdyb3VwLWl0ZW1cIj5cclxuICAgICAgICAgICAgICAgICAgPGJ1dHRvblxyXG4gICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cImJ0biBwLTBcIlxyXG4gICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IGNsaWNrQWRtaW5NZW51KCcvYWRtaW4vbG9jYWxlcycpfT5cclxuICAgICAgICAgICAgICAgICAgICB7dHJhbnNsYXRlKGNvbmZpZy50cmFuc2xhdGlvbnMsICdsb2NhbGVzJyl9XHJcbiAgICAgICAgICAgICAgICAgIDwvYnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgIDxsaSBjbGFzc05hbWU9XCJsaXN0LWdyb3VwLWl0ZW1cIj5cclxuICAgICAgICAgICAgICAgICAgPGJ1dHRvblxyXG4gICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cImJ0biBwLTBcIlxyXG4gICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IGNsaWNrQWRtaW5NZW51KCcvYWRtaW4vdXNlcnMnKX0+XHJcbiAgICAgICAgICAgICAgICAgICAge3RyYW5zbGF0ZShjb25maWcudHJhbnNsYXRpb25zLCAndXNlcnMnKX1cclxuICAgICAgICAgICAgICAgICAgPC9idXR0b24+XHJcbiAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgIDwvdWw+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgPC9Qb3BvdmVyPlxyXG4gICAgICAgIDwvUmVhY3QuRnJhZ21lbnQ+XHJcbiAgICAgIH1cclxuICAgICAgey8qIFBPUE9WRVIgU0VTU0lPTiAqL31cclxuICAgICAge1xyXG4gICAgICAgIGF1dGggJiZcclxuICAgICAgICA8UmVhY3QuRnJhZ21lbnQ+XHJcbiAgICAgICAgICA8YnV0dG9uXHJcbiAgICAgICAgICAgIHJlZj17ZWxlbSA9PiBzZXRFbGVtUG9wb3ZlclNlc3Npb24oZWxlbSl9XHJcbiAgICAgICAgICAgIGNsYXNzTmFtZT1cImJ0biBidG4tbGlnaHQgbWwtMVwiXHJcbiAgICAgICAgICAgIHRpdGxlPXt0cmFuc2xhdGUoY29uZmlnLnRyYW5zbGF0aW9ucywgJ3lvdXJBY2NvdW50Jyl9XHJcbiAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHNldFBvcG92ZXJTZXNzaW9uKCFzaG93UG9wb3ZlclNlc3Npb24pfT5cclxuICAgICAgICAgICAgPGkgY2xhc3NOYW1lPVwiZmFzIGZhLXVzZXJcIiAvPlxyXG4gICAgICAgICAgPC9idXR0b24+XHJcbiAgICAgICAgICA8UG9wb3ZlclxyXG4gICAgICAgICAgICBpc09wZW49e3Nob3dQb3BvdmVyU2Vzc2lvbn1cclxuICAgICAgICAgICAgdGFyZ2V0PXtlbGVtUG9wb3ZlclNlc3Npb259XHJcbiAgICAgICAgICAgIHN0eWxlPXt7IHdpZHRoOiAyMDAgfX1cclxuICAgICAgICAgICAgb25Ub2dnbGU9eygpID0+IHNldFBvcG92ZXJTZXNzaW9uKCFzaG93UG9wb3ZlclNlc3Npb24pfT5cclxuICAgICAgICAgICAgPGgzIGNsYXNzTmFtZT1cInBvcG92ZXItaGVhZGVyXCI+e3RyYW5zbGF0ZShjb25maWcudHJhbnNsYXRpb25zLCAneW91ckFjY291bnQnKX08L2gzPlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInBvcG92ZXItYm9keVwiPlxyXG4gICAgICAgICAgICAgIDx1bCBjbGFzc05hbWU9XCJsaXN0LWdyb3VwIGxpc3QtZ3JvdXAtZmx1c2hcIj5cclxuICAgICAgICAgICAgICAgIDxsaSBjbGFzc05hbWU9XCJsaXN0LWdyb3VwLWl0ZW1cIj5cclxuICAgICAgICAgICAgICAgICAgPGJ1dHRvblxyXG4gICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cImJ0biBwLTBcIlxyXG4gICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IGNsaWNrU2Vzc2lvbk1lbnUoJy9hcHAvdXNlci9wcm9maWxlJyl9PlxyXG4gICAgICAgICAgICAgICAgICAgIHt0cmFuc2xhdGUoY29uZmlnLnRyYW5zbGF0aW9ucywgJ3Byb2ZpbGUnKX1cclxuICAgICAgICAgICAgICAgICAgPC9idXR0b24+XHJcbiAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgPGxpIGNsYXNzTmFtZT1cImxpc3QtZ3JvdXAtaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICA8YnV0dG9uXHJcbiAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiYnRuIHAtMFwiXHJcbiAgICAgICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgc2V0UG9wb3ZlclNlc3Npb24oZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgb25Mb2dvdXQoKTtcclxuICAgICAgICAgICAgICAgICAgICB9fT5cclxuICAgICAgICAgICAgICAgICAgICB7dHJhbnNsYXRlKGNvbmZpZy50cmFuc2xhdGlvbnMsICdsb2dvdXQnKX1cclxuICAgICAgICAgICAgICAgICAgPC9idXR0b24+XHJcbiAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgIDwvdWw+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgPC9Qb3BvdmVyPlxyXG4gICAgICAgIDwvUmVhY3QuRnJhZ21lbnQ+XHJcbiAgICAgIH1cclxuICAgIDwvUmVhY3QuRnJhZ21lbnQ+XHJcbiAgKTtcclxufSIsImltcG9ydCBSZWFjdCwgeyB1c2VDb250ZXh0LCB1c2VTdGF0ZSB9IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IHN0eWxlZCBmcm9tICdzdHlsZWQtY29tcG9uZW50cyc7XHJcbmltcG9ydCBDb250ZXh0IGZyb20gJy4uLy4uL2NvbnRleHQnO1xyXG5pbXBvcnQgQ29sbGFwc2libGUgZnJvbSAnLi9jb2xsYXBzaWJsZSc7XHJcbmltcG9ydCBNZW51TWFpbiBmcm9tICcuL21lbnUtbWFpbic7XHJcbmltcG9ydCBNZW51UG9wb3ZlcnMgZnJvbSAnLi9tZW51LXBvcG92ZXJzJztcclxuXHJcbmNvbnN0IE5hdmJhcldyYXBwZXIgPSBzdHlsZWQubmF2YFxyXG4gICYsICoge1xyXG4gICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICB9XHJcblxyXG4gIC5uYXZiYXItY29sbGFwc2Uge1xyXG4gICAgb3JkZXI6IDA7XHJcbiAgfVxyXG4gIC5uYXZiYXItb3B0aW9ucyB7XHJcbiAgICBvcmRlcjogMTtcclxuICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xyXG5cclxuICAgIC5idG4ge1xyXG4gICAgICBjb2xvcjogIzY2NjtcclxuICAgIH1cclxuICB9ICAgIFxyXG4gIC5uYXZiYXItdG9nZ2xlciB7XHJcbiAgICBvcmRlcjogMjtcclxuICB9XHJcblxyXG4gIEBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDc2OHB4KSB7XHJcbiAgICAubmF2YmFyLm5hdmJhci1saWdodCB7XHJcbiAgICAgIHotaW5kZXg6IDk7XHJcbiAgICAgIC5uYXZiYXItdG9nZ2xlciB7XHJcbiAgICAgICAgY29sb3I6ICM2NjY7XHJcbiAgICAgICAgYm9yZGVyOiBub25lO1xyXG4gICAgICAgIHBhZGRpbmc6IGluaGVyaXQ7XHJcbiAgICAgICAgZm9udC1zaXplOiBpbmhlcml0O1xyXG4gICAgICAgIGxpbmUtaGVpZ2h0OiBub3JtYWw7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIC5uYXZiYXItb3B0aW9ucyB7XHJcbiAgICAgIG9yZGVyOiAwO1xyXG4gICAgfVxyXG4gICAgLm5hdmJhci10b2dnbGVyIHtcclxuICAgICAgb3JkZXI6IDE7XHJcbiAgICB9XHJcbiAgICAubmF2YmFyLWNvbGxhcHNlIHtcclxuICAgICAgb3JkZXI6IDI7XHJcbiAgICAgIHVsID4gYTpub3QoLmJ0biksIFxyXG4gICAgICB1bCA+IGxpIHtcclxuICAgICAgICBib3JkZXItdG9wOiAxcHggc29saWQgI0VFRTtcclxuICAgICAgICBwYWRkaW5nLWxlZnQ6IDE2cHg7XHJcbiAgICAgICAgcGFkZGluZy1yaWdodDogMTZweDtcclxuXHJcbiAgICAgICAgJi5hY3RpdmUsXHJcbiAgICAgICAgJjpob3ZlciB7XHJcbiAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDE2MSwgMTk0LCAyNTAsIDAuMTYpO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuYDtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIE5hdmJhcihwcm9wcykge1xyXG4gIGNvbnN0IFtzaG93TWVudSwgc2V0TWVudV0gPSB1c2VTdGF0ZShmYWxzZSk7XHJcbiAgY29uc3QgY29udGV4dCA9IHVzZUNvbnRleHQoQ29udGV4dCk7XHJcbiAgY29uc3QgeyBjb25maWcsIHVzZXIsIGhpc3RvcnkgfSA9IHByb3BzO1xyXG4gIGNvbnN0IHsgYXV0aCwgYXV0aExvYWRlZCB9ID0gdXNlcjtcclxuICBjb25zdCBjdXJyZW50UGF0aCA9IGhpc3RvcnkubG9jYXRpb24ucGF0aG5hbWU7XHJcblxyXG4gIHJldHVybiAoXHJcbiAgICA8TmF2YmFyV3JhcHBlciBjbGFzc05hbWU9XCJib3JkZXItYm90dG9tIG5hdmJhciBuYXZiYXItZXhwYW5kLW1kIG5hdmJhci1saWdodCBiZy1saWdodFwiPlxyXG4gICAgICA8YnV0dG9uIGNsYXNzTmFtZT1cImJ0biBuYXZiYXItYnJhbmRcIiBvbkNsaWNrPXsoKSA9PiBoaXN0b3J5LnB1c2goJy8nKX0+XHJcbiAgICAgICAgPGltZyBzcmM9e2NvbmZpZy5hcHBMb2dvfSBhbHQ9e2NvbmZpZy5hcHBOYW1lfSB3aWR0aD17MzV9IC8+eycgJ31cclxuICAgICAgICA8c3Bhbj57Y29uZmlnLmFwcE5hbWV9PC9zcGFuPlxyXG4gICAgICA8L2J1dHRvbj5cclxuICAgICAgPENvbGxhcHNpYmxlIGNsYXNzTmFtZT1cIm5hdmJhci1jb2xsYXBzZVwiIGlzT3Blbj17c2hvd01lbnV9PlxyXG4gICAgICAgIDxNZW51TWFpblxyXG4gICAgICAgICAgYXV0aD17YXV0aH1cclxuICAgICAgICAgIGF1dGhMb2FkZWQ9e2F1dGhMb2FkZWR9XHJcbiAgICAgICAgICBjb25maWc9e2NvbmZpZ31cclxuICAgICAgICAgIGN1cnJlbnRQYXRoPXtjdXJyZW50UGF0aH1cclxuICAgICAgICAgIG9uQ2hhbmdlUGF0aD17cGF0aCA9PiB7XHJcbiAgICAgICAgICAgIHNldE1lbnUoZmFsc2UpO1xyXG4gICAgICAgICAgICBoaXN0b3J5LnB1c2gocGF0aCk7XHJcbiAgICAgICAgICB9fT5cclxuICAgICAgICA8L01lbnVNYWluPlxyXG4gICAgICA8L0NvbGxhcHNpYmxlPlxyXG4gICAgICA8ZGl2IGNsYXNzTmFtZT1cIm5hdmJhci1vcHRpb25zXCI+XHJcbiAgICAgICAgPE1lbnVQb3BvdmVyc1xyXG4gICAgICAgICAgYXV0aD17YXV0aH1cclxuICAgICAgICAgIGF1dGhMb2FkZWQ9e2F1dGhMb2FkZWR9XHJcbiAgICAgICAgICBjb25maWc9e2NvbmZpZ31cclxuICAgICAgICAgIG9uQ2hhbmdlUGF0aD17cGF0aCA9PiBoaXN0b3J5LnB1c2gocGF0aCl9XHJcbiAgICAgICAgICBvbkNoYW5nZUN1cnJlbmN5PXtjb250ZXh0LmNoYW5nZUN1cnJlbmN5fVxyXG4gICAgICAgICAgb25DaGFuZ2VEYXRlRm9ybWF0PXtjb250ZXh0LmNoYW5nZURhdGVGb3JtYXR9XHJcbiAgICAgICAgICBvbkNoYW5nZUxhbmd1YWdlPXtjb250ZXh0LmNoYW5nZUxhbmd1YWdlfVxyXG4gICAgICAgICAgb25Mb2dvdXQ9e2NvbnRleHQubG9nb3V0fT5cclxuICAgICAgICA8L01lbnVQb3BvdmVycz5cclxuICAgICAgPC9kaXY+XHJcbiAgICAgIDxidXR0b24gXHJcbiAgICAgICAgYXJpYS1sYWJlbD1cIlRvZ2dsZSBuYXZpZ2F0aW9uXCJcclxuICAgICAgICBjbGFzc05hbWU9XCJuYXZiYXItdG9nZ2xlciBib3JkZXItMCBidG4gYnRuLWxpZ2h0IG1sLTFcIiBcclxuICAgICAgICBvbkNsaWNrPXsoKSA9PiBzZXRNZW51KCFzaG93TWVudSl9PlxyXG4gICAgICAgIHshc2hvd01lbnUgJiYgPGkgY2xhc3NOYW1lPVwiZmFzIGZhLWJhcnNcIiAvPn1cclxuICAgICAgICB7c2hvd01lbnUgJiYgPGkgY2xhc3NOYW1lPVwiZmFzIGZhLXRpbWVzXCIgLz59XHJcbiAgICAgIDwvYnV0dG9uPlxyXG4gICAgPC9OYXZiYXJXcmFwcGVyPlxyXG4gICk7XHJcbn0iLCJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgeyBIZWxtZXQgfSBmcm9tICdyZWFjdC1oZWxtZXQnO1xyXG5pbXBvcnQgc3R5bGVkIGZyb20gJ3N0eWxlZC1jb21wb25lbnRzJztcclxuXHJcbmNvbnN0IE5hdmlnYXRpb25CYXJXcmFwcGVyID0gc3R5bGVkLnNlY3Rpb25gXHJcbiAgJiwgKiB7XHJcbiAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG4gIH1cclxuXHJcbiAgZGlzcGxheTogZmxleDtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG5cclxuICAubGVmdC1ib3gge1xyXG4gICAgZmxleC1ncm93OiAwO1xyXG4gICAgdGV4dC1hbGlnbjogbGVmdDtcclxuICB9XHJcblxyXG4gIC5jZW50ZXItYm94IHtcclxuICAgIGZsZXgtZ3JvdzogMTtcclxuICAgIG1heC13aWR0aDogMTAwJTtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuXHJcbiAgICBoMSB7XHJcbiAgICAgIGZvbnQtc2l6ZTogMnJlbTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC5yaWdodC1ib3gge1xyXG4gICAgZmxleC1ncm93OiAwO1xyXG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbiAgfVxyXG5cclxuICBAbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3NjhweCkge1xyXG4gICAgLmNlbnRlci1ib3ggaDEge1xyXG4gICAgICBmb250LXNpemU6IDEuNXJlbTtcclxuICAgIH1cclxuICB9XHJcbmA7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBOYXZpZ2F0aW9uQmFyKHByb3BzKSB7XHJcbiAgY29uc3QgeyBjb25maWcsIHRpdGxlLCBkZXNjcmlwdGlvbiwgYnRuTGVmdCwgYnRuUmlnaHQgfSA9IHByb3BzO1xyXG4gIGNvbnN0IGtleXdvcmRzID0gKHByb3BzLmtleXdvcmRzIHx8IFtdKS5jb25jYXQoJ3NhbXBsZScpO1xyXG5cclxuICByZXR1cm4gKFxyXG4gICAgPFJlYWN0LkZyYWdtZW50PlxyXG4gICAgICA8SGVsbWV0PlxyXG4gICAgICAgIDxodG1sIGxhbmc9e2NvbmZpZy5wcmVmZXJlbmNlcy5sYW5ndWFnZX0gLz5cclxuICAgICAgICA8dGl0bGU+e3Byb3BzLnRpdGxlfTwvdGl0bGU+XHJcbiAgICAgICAgey8qIEdPT0dMRSAqL31cclxuICAgICAgICA8bWV0YSBjaGFyc2V0PVwidXRmLThcIiAvPlxyXG4gICAgICAgIDxtZXRhIG5hbWU9XCJkZXNjcmlwdGlvblwiIGNvbnRlbnQ9e3Byb3BzLmRlc2NyaXB0aW9ufSAvPlxyXG4gICAgICAgIDxtZXRhIG5hbWU9XCJrZXl3b3Jkc1wiIGNvbnRlbnQ9e2tleXdvcmRzLmpvaW4oJywgJyl9IC8+XHJcbiAgICAgICAgPG1ldGEgbmFtZT1cInJvYm90c1wiIGNvbnRlbnQ9XCJpbmRleCwgZm9sbG93XCIgLz5cclxuICAgICAgICB7LyogRkFDRUJPT0sgKi99XHJcbiAgICAgICAgPG1ldGEgcHJvcGVydHk9XCJvZzp0aXRsZVwiIGNvbnRlbnQ9e3Byb3BzLnRpdGxlfSAvPlxyXG4gICAgICAgIDxtZXRhIHByb3BlcnR5PVwib2c6dHlwZVwiIGNvbnRlbnQ9XCJidXNpbmVzcy5idXNpbmVzc1wiIC8+XHJcbiAgICAgICAgPG1ldGEgcHJvcGVydHk9XCJvZzpzaXRlX25hbWVcIiBjb250ZW50PXtjb25maWcuYXBwTmFtZX0gLz5cclxuICAgICAgICA8bWV0YSBwcm9wZXJ0eT1cIm9nOmRlc2NyaXB0aW9uXCIgY29udGVudD17cHJvcHMudGl0bGV9IC8+XHJcbiAgICAgICAgey8qIFRXSVRURVIgICovfVxyXG4gICAgICAgIDxtZXRhIG5hbWU9XCJ0d2l0dGVyOmNhcmRcIiBjb250ZW50PXtwcm9wcy5kZXNjcmlwdGlvbn0gLz5cclxuICAgICAgICA8bWV0YSBuYW1lPVwidHdpdHRlcjpjcmVhdG9yXCIgY29udGVudD1cIkBqaG9uZnJlZHlub3ZhXCIgLz5cclxuICAgICAgICA8bWV0YSBuYW1lPVwidHdpdHRlcjpzaXRlXCIgY29udGVudD1cIkBqaG9uZnJlZHlub3ZhXCIgLz5cclxuICAgICAgPC9IZWxtZXQ+XHJcbiAgICAgIDxOYXZpZ2F0aW9uQmFyV3JhcHBlciBjbGFzc05hbWU9XCJjb250YWluZXItZmx1aWQgbWItNCBib3JkZXItYm90dG9tXCI+XHJcbiAgICAgICAge2J0bkxlZnQgJiYgPGRpdiBjbGFzc05hbWU9XCJsZWZ0LWJveFwiPntidG5MZWZ0fTwvZGl2Pn1cclxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNlbnRlci1ib3hcIiByb2xlPVwiaGVhZGluZ1wiPlxyXG4gICAgICAgICAgPGgxIGNsYXNzTmFtZT1cIm10LTJcIj57dGl0bGV9PC9oMT5cclxuICAgICAgICAgIDxwIGNsYXNzTmFtZT1cIm10LTJcIj57ZGVzY3JpcHRpb259PC9wPlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICAgIHtidG5SaWdodCAmJiA8ZGl2IGNsYXNzTmFtZT1cInJpZ2h0LWJveFwiPntidG5SaWdodH08L2Rpdj59ICAgICAgXHJcbiAgICAgIDwvTmF2aWdhdGlvbkJhcldyYXBwZXI+XHJcbiAgICA8L1JlYWN0LkZyYWdtZW50PlxyXG4gICk7XHJcbn0iLCJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG4vLyBpbXBvcnQgc3R5bGVkIGZyb20gJ3N0eWxlZC1jb21wb25lbnRzJztcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIE5ld3MocHJvcHMpIHtcclxuICAgIGNvbnN0IHsgbmV3cyB9ID0gcHJvcHM7XHJcbiAgICBuZXdzLm1hcCgob2JqKSA9PiB7IFxyXG4gICAgICAgIG9iai5kYXRlRm9yU29ydGVkID0gbmV3IERhdGUob2JqLnB1YmxpY2F0aW9uRGF0ZSk7XHJcbiAgICB9KTtcclxuICAgIG5ld3Muc29ydCgoYSwgYikgPT4gYi5kYXRlRm9yU29ydGVkIC0gYS5kYXRlRm9yU29ydGVkKTtcclxuICAgIHJldHVybiAoXHJcbiAgICAgICAgPFJlYWN0LkZyYWdtZW50PlxyXG4gICAgICAgIHtuZXdzWzBdICYmIG5ld3MubWFwKChpdGVtTmV3LCBpbmRleCkgPT4gKFxyXG4gICAgICAgIDxkaXYga2V5PXtpbmRleH0gY2xhc3NOYW1lPVwiY2FyZFwiPlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNhcmQtYm9keVwiPlxyXG4gICAgICAgICAgICAgICAgPGg1IGNsYXNzTmFtZT1cImNhcmQtdGl0bGVcIj57aXRlbU5ldy50aXRsZX08L2g1PlxyXG4gICAgICAgICAgICAgICAgPHAgY2xhc3NOYW1lPVwiY2FyZC10ZXh0XCI+e2l0ZW1OZXcuZGVzY3JpcHRpb259LjwvcD5cclxuICAgICAgICAgICAgICAgIDxhIGhyZWY9e2l0ZW1OZXcubGlua1RvUGFnZX0gY2xhc3NOYW1lPVwiYnRuIGJ0bi1wcmltYXJ5XCI+Q29ub2NlciBNw6FzPC9hPlxyXG4gICAgICAgICAgICAgICAgPHAgY2xhc3NOYW1lPVwiY2FyZC10ZXh0XCI+PHNtYWxsIGNsYXNzTmFtZT1cInRleHQtbXV0ZWRcIj57aXRlbU5ldy5wdWJsaWNhdGlvbkRhdGV9PC9zbWFsbD48L3A+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8aW1nIHNyYz17aXRlbU5ldy5pbWFnZX0gY2xhc3NOYW1lPVwiY2FyZC1pbWctYm90dG9tXCIgYWx0PXtpdGVtTmV3LnRpdGxlfS8+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgXHJcbiAgICAgICAgKSl9XHJcblxyXG4gICAgICAgIDwvUmVhY3QuRnJhZ21lbnQ+XHJcbiAgICApOyAgXHJcbn1cclxuIiwiaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IHN0eWxlZCBmcm9tICdzdHlsZWQtY29tcG9uZW50cyc7XHJcblxyXG5jb25zdCBQb3BvdmVyV3JhcHBlciA9IHN0eWxlZC5kaXZgXHJcbiAgJiwgKiB7XHJcbiAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG4gIH1cclxuXHJcbiAgZGlzcGxheTogbm9uZTtcclxuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICB0b3A6IDBweDtcclxuICBsZWZ0OiAwcHg7XHJcbiAgd2lkdGg6IDMwMHB4O1xyXG4gIHotaW5kZXg6IDEwMDA7XHJcblxyXG4gICYuc2hvdyB7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICB9XHJcbmA7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBQb3BvdmVyIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcclxuXHJcbiAgc3RhdGUgPSB7XHJcbiAgICBzY3JlZW5XaWR0aDogMFxyXG4gIH1cclxuXHJcbiAgY29tcG9uZW50RGlkTW91bnQgPSAoKSA9PiB7XHJcbiAgICBkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIHRoaXMub25Eb2N1bWVudENsaWNrKTtcclxuICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdyZXNpemUnLCB0aGlzLm9uUmVzaXplV2luZG93KTtcclxuICAgIHdpbmRvdy5kaXNwYXRjaEV2ZW50KG5ldyBDdXN0b21FdmVudCgncmVzaXplJykpO1xyXG4gIH07XHJcblxyXG4gIGNvbXBvbmVudFdpbGxVbk1vdW50ID0gKCkgPT4ge1xyXG4gICAgZG9jdW1lbnQucmVtb3ZlRXZlbnRMaXN0ZW5lcignY2xpY2snLCB0aGlzLm9uRG9jdW1lbnRDbGljayk7XHJcbiAgICB3aW5kb3cucmVtb3ZlRXZlbnRMaXN0ZW5lcigncmVzaXplJywgdGhpcy5vblJlc2l6ZVdpbmRvdyk7XHJcbiAgfVxyXG5cclxuICBvbkRvY3VtZW50Q2xpY2sgPSBldmVudCA9PiB7XHJcbiAgICBjb25zdCB7IGlzT3BlbiwgdGFyZ2V0IH0gPSB0aGlzLnByb3BzO1xyXG5cclxuICAgIGlmIChpc09wZW4gJiYgIXRhcmdldC5jb250YWlucyhldmVudC50YXJnZXQpICYmICF0aGlzLnBvcG92ZXIuY29udGFpbnMoZXZlbnQudGFyZ2V0KSkge1xyXG4gICAgICB0aGlzLnByb3BzLm9uVG9nZ2xlKCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBvblJlc2l6ZVdpbmRvdyA9ICgpID0+IHtcclxuICAgIHRoaXMuc2V0U3RhdGUoeyBzY3JlZW5XaWR0aDogd2luZG93LmlubmVyV2lkdGggfSk7XHJcbiAgfVxyXG5cclxuICBnZXRQb3BvdmVyUG9zaXRpb24gPSB0YXJnZXRFbGVtID0+IHtcclxuICAgIGNvbnN0IHsgc3R5bGUgfSA9IHRoaXMucHJvcHM7XHJcbiAgICBjb25zdCBwb3BvdmVyV2lkdGggPSAoc3R5bGUgJiYgc3R5bGUud2lkdGgpIHx8IDMwMDtcclxuICAgIGNvbnN0IHRhcmdldFBvc2l0aW9uID0gdGFyZ2V0RWxlbSA/IHRhcmdldEVsZW0uZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkgOiBudWxsO1xyXG4gICAgY29uc3QgdGFyZ2V0V2lkdGggPSB0YXJnZXRFbGVtID8gdGFyZ2V0RWxlbS5jbGllbnRXaWR0aCA6IDA7XHJcbiAgICBjb25zdCB0YXJnZXRIZWlnaHQgPSB0YXJnZXRFbGVtID8gdGFyZ2V0RWxlbS5jbGllbnRIZWlnaHQgOiAwO1xyXG4gICAgY29uc3QgdGFyZ2V0UG9zaXRpb25YID0gdGFyZ2V0UG9zaXRpb24gPyB0YXJnZXRQb3NpdGlvbi54IDogMDtcclxuICAgIGNvbnN0IHRhcmdldFBvc2l0aW9uWSA9IHRhcmdldFBvc2l0aW9uID8gdGFyZ2V0UG9zaXRpb24ueSA6IDA7XHJcbiAgICBjb25zdCBzY3JlZW5XaWR0aCA9ICh0aGlzLnN0YXRlLnNjcmVlbldpZHRoIC0gMTApOyAgXHJcblxyXG4gICAgbGV0IHBvcG92ZXJQb3NpdGlvblggPSB0YXJnZXRQb3NpdGlvblggKyAodGFyZ2V0V2lkdGggLyAyKSAtIChwb3BvdmVyV2lkdGggLyAyKTtcclxuICAgIGxldCBwb3BvdmVyUG9zaXRpb25ZID0gdGFyZ2V0UG9zaXRpb25ZICsgKHRhcmdldEhlaWdodCArIDEwKTtcclxuXHJcbiAgICByZXR1cm4ge1xyXG4gICAgICB4OiAocG9wb3ZlclBvc2l0aW9uWCArIHBvcG92ZXJXaWR0aCkgPiBzY3JlZW5XaWR0aCA/IChzY3JlZW5XaWR0aCAtIHBvcG92ZXJXaWR0aCAtIDUpIDogcG9wb3ZlclBvc2l0aW9uWCxcclxuICAgICAgeTogcG9wb3ZlclBvc2l0aW9uWVxyXG4gICAgfTtcclxuICB9XHJcblxyXG4gIHJlbmRlcigpIHtcclxuICAgIGNvbnN0IHsgaXNPcGVuLCBjbGFzc05hbWUsIGNoaWxkcmVuLCB0YXJnZXQsIHN0eWxlIH0gPSB0aGlzLnByb3BzO1xyXG4gICAgY29uc3QgdGFyZ2V0UG9zaXRpb24gPSB0aGlzLmdldFBvcG92ZXJQb3NpdGlvbih0YXJnZXQpO1xyXG5cclxuICAgIHJldHVybiAoXHJcbiAgICAgIDxQb3BvdmVyV3JhcHBlclxyXG4gICAgICAgIHJlZj17ZWxlbSA9PiB0aGlzLnBvcG92ZXIgPSBlbGVtfVxyXG4gICAgICAgIGFyaWEtZXhwYW5kZWQ9e2lzT3Blbn0gXHJcbiAgICAgICAgY2xhc3NOYW1lPXtgJHtjbGFzc05hbWV9ICR7aXNPcGVuICYmICdzaG93J31gfVxyXG4gICAgICAgIHN0eWxlPXt7IC4uLnN0eWxlLCB0cmFuc2Zvcm06IGB0cmFuc2xhdGUzZCgke3RhcmdldFBvc2l0aW9uLnh9cHgsICR7dGFyZ2V0UG9zaXRpb24ueX1weCwgMHB4KWAgfX0+XHJcbiAgICAgICAge2NoaWxkcmVufVxyXG4gICAgICA8L1BvcG92ZXJXcmFwcGVyPlxyXG4gICAgKTtcclxuICB9XHJcbn0iLCJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgeyBjb25uZWN0IH0gZnJvbSAncmVhY3QtcmVkdXgnO1xyXG5pbXBvcnQgc3R5bGVkIGZyb20gJ3N0eWxlZC1jb21wb25lbnRzJztcclxuaW1wb3J0IEhlYWRlciBmcm9tICcuL19jb21wb25lbnRzL2hlYWRlcic7XHJcbmltcG9ydCBOZXdzIGZyb20gJy4vX2NvbXBvbmVudHMvbmV3cyc7XHJcbmltcG9ydCBGb290ZXIgZnJvbSAnLi9fY29tcG9uZW50cy9mb290ZXInO1xyXG5pbXBvcnQgeyB0cmFuc2xhdGUgfSBmcm9tICcuLi91dGlscy9pbnRsLXV0aWxzJztcclxuY29uc3QgSG9tZVdyYXBwZXIgPSBzdHlsZWQubWFpbmBcclxuICAmLCAqIHtcclxuICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgfVxyXG5gO1xyXG5cclxuaW1wb3J0IGRhdGEgZnJvbSAnLi9hbnkuanNvbic7XHJcbmNsYXNzIEhvbWUgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xyXG4gIHN0YXRlID0ge1xyXG4gICAgZXJyb3I6IG51bGwsXHJcbiAgICBpc0xvYWRpbmc6IGZhbHNlLFxyXG4gICAgbmV3czogW11cclxuICB9XHJcbiAgYXN5bmMgY29tcG9uZW50RGlkTW91bnQoKSB7XHJcbiAgICBhd2FpdCB0aGlzLmZldGNoTmV3cygpO1xyXG4gIH1cclxuXHJcbiAgZmV0Y2hOZXdzID0gYXN5bmMgKCkgPT4ge1xyXG4gICAgdGhpcy5zZXRTdGF0ZSh7aXNMb2FkaW5nOiB0cnVlLCBlcnJvcjogbnVsbH0pO1xyXG4gICAgLy8gdHJ5IHtcclxuICAgIC8vICAgY29uc3QgcmVzcG9uc2UgPSBhd2FpdCBmZXRjaCgnaHR0cHM6Ly9ldmlkZW50LWZhY3Rvci0yNTk0MTEuYXBwc3BvdC5jb20vYXBpL3NjcmFwaW5nL2FuYWx5emVXZWJzJyk7XHJcbiAgICAvLyAgIGNvbnN0IGRhdGEgPSBhd2FpdCByZXNwb25zZS5qc29uKCk7XHJcbiAgICAvLyAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgLy8gICAgIGlzTG9hZGluZzogZmFsc2UsXHJcbiAgICAvLyAgICAgbmV3czogZGF0YVxyXG4gICAgLy8gICB9KTtcclxuICAgIC8vIH0gY2F0Y2ggKGVycm9yKSB7XHJcbiAgICAvLyAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgLy8gICAgIGlzTG9hZGluZzogZmFsc2UsXHJcbiAgICAvLyAgICAgZXJyb3I6IGVycm9yXHJcbiAgICAvLyAgIH0pO1xyXG4gICAgLy8gfVxyXG4gICAgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgIGlzTG9hZGluZzogZmFsc2UsXHJcbiAgICAgIG5ld3M6IGRhdGEubmV3c1xyXG4gICAgfSk7XHJcbiAgfTtcclxuXHJcbiAgcmVuZGVyKCkge1xyXG4gICAgY29uc3QgeyBjb25maWcsIHVzZXIgfSA9IHRoaXMucHJvcHM7XHJcbiAgICByZXR1cm4gKFxyXG4gICAgICA8UmVhY3QuRnJhZ21lbnQ+XHJcbiAgICAgICAgPEhlYWRlclxyXG4gICAgICAgICAgY29uZmlnPXtjb25maWd9XHJcbiAgICAgICAgICB1c2VyPXt1c2VyfVxyXG4gICAgICAgICAgdGl0bGU9e3RyYW5zbGF0ZShjb25maWcudHJhbnNsYXRpb25zLCAnaG9tZVRpdGxlJyl9XHJcbiAgICAgICAgICBkZXNjcmlwdGlvbj17dHJhbnNsYXRlKGNvbmZpZy50cmFuc2xhdGlvbnMsICdob21lRGVzY3JpcHRpb24nKX1cclxuICAgICAgICAgIGhpc3Rvcnk9e3RoaXMucHJvcHMuaGlzdG9yeX1cclxuICAgICAgICAgIHNob3dIZWFkaW5nPXt0cnVlfSAvPlxyXG4gICAgICAgIDxIb21lV3JhcHBlciBjbGFzc05hbWU9XCJjb250YWluZXItZmx1aWRcIj5cclxuICAgICAgICAgIHt0aGlzLnN0YXRlLmlzTG9hZGluZyAmJiAoXHJcbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInRleHQtY2VudGVyXCI+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwic3Bpbm5lci1ib3JkZXIgdGV4dC1pbmZvXCIgcm9sZT1cInN0YXR1c1wiPlxyXG4gICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cInNyLW9ubHlcIj5Mb2FkaW5nLi4uPC9zcGFuPlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgKX1cclxuICAgICAgICAgIDxOZXdzIG5ld3M9e3RoaXMuc3RhdGUubmV3c30vPlxyXG4gICAgICAgICAge3RoaXMuc3RhdGUuZXJyb3IgJiYgKFxyXG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJ0ZXh0LWNlbnRlclwiPlxyXG4gICAgICAgICAgICA8cCBjbGFzc05hbWU9XCJ0ZXh0LWRhbmdlclwiPk5vdGljaWFzIE5vIERpc3BvbmlibGVzPC9wPlxyXG4gICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICApfVxyXG4gICAgICAgIDwvSG9tZVdyYXBwZXI+XHJcbiAgICAgICAgPEZvb3RlciBcclxuICAgICAgICAgIGNvbmZpZz17Y29uZmlnfVxyXG4gICAgICAgICAgcm91dGVySGlzdG9yeT17dGhpcy5wcm9wcy5oaXN0b3J5fSAvPlxyXG4gICAgICA8L1JlYWN0LkZyYWdtZW50PlxyXG4gICAgKTtcclxuICB9XHJcbn1cclxuY29uc3QgbWFwU3RhdGVUb1Byb3BzID0gc3RhdGUgPT4gKHtcclxuICBjb25maWc6IHN0YXRlLmNvbmZpZyxcclxuICBjYXRhbG9nOiBzdGF0ZS5jYXRhbG9nLFxyXG4gIHBsYW46IHN0YXRlLnBsYW4sXHJcbiAgdXNlcjogc3RhdGUudXNlclxyXG59KTtcclxuZXhwb3J0IGRlZmF1bHQgY29ubmVjdChtYXBTdGF0ZVRvUHJvcHMpKEhvbWUpOyIsImltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCB7IGNvbm5lY3QgfSBmcm9tICdyZWFjdC1yZWR1eCc7XHJcbmltcG9ydCB7IExpbmsgfSBmcm9tICdyZWFjdC1yb3V0ZXItZG9tJztcclxuaW1wb3J0IHN0eWxlZCBmcm9tICdzdHlsZWQtY29tcG9uZW50cyc7XHJcbmltcG9ydCBIZWFkZXIgZnJvbSAnLi9fY29tcG9uZW50cy9oZWFkZXInO1xyXG5pbXBvcnQgRm9vdGVyIGZyb20gJy4vX2NvbXBvbmVudHMvZm9vdGVyJztcclxuaW1wb3J0IHN0b3JlIGZyb20gJy4uL3N0b3JlL3N0b3JlJztcclxuaW1wb3J0IHsgbG9naW4sIG1lLCBzZXRUb2tlbiB9IGZyb20gJy4uL3N0b3JlL2FjdGlvbnMvdXNlci1hY3Rpb25zJztcclxuaW1wb3J0IHsgdHJhbnNsYXRlIH0gZnJvbSAnLi4vdXRpbHMvaW50bC11dGlscyc7XHJcbmltcG9ydCB7IGdldFF1ZXJ5UGFyYW1zSnNvbiB9IGZyb20gJy4uL3V0aWxzL3JlcXVlc3QtdXRpbHMnO1xyXG5pbXBvcnQgeyBnZXRGcm9udGVuZEVycm9yIH0gZnJvbSAnLi4vdXRpbHMvcmVzcG9uc2UtdXRpbHMnO1xyXG5cclxuY29uc3QgTG9naW5XcmFwcGVyID0gc3R5bGVkLm1haW5gXHJcbiAgJiwgKiB7XHJcbiAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG4gIH1cclxuYDtcclxuXHJcbmNsYXNzIExvZ2luIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcclxuICBzdGF0ZSA9IHtcclxuICAgIGlzTG9hZGluZzogZmFsc2UsXHJcbiAgICBlcnJvcnM6IHt9LFxyXG4gICAgbW9kZWw6IHtcclxuICAgICAgZW1haWw6ICcnLFxyXG4gICAgICBwYXNzd29yZDogJydcclxuICAgIH1cclxuICB9O1xyXG5cclxuICBjb21wb25lbnREaWRVcGRhdGUgPSBhc3luYyAoKSA9PiB7XHJcbiAgICBpZiAodGhpcy5wcm9wcy5hdXRoKSB7XHJcbiAgICAgIHRoaXMucHJvcHMuaGlzdG9yeS5wdXNoKCcvYXBwL2Rhc2hib2FyZCcpO1xyXG4gICAgfVxyXG4gIH07XHJcblxyXG4gIGxvZ2luID0gYXN5bmMgZSA9PiB7XHJcbiAgICB0cnkge1xyXG4gICAgICBpZiAoZSkge1xyXG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgdGhpcy5zZXRTdGF0ZSh7IGlzTG9hZGluZzogdHJ1ZSwgZXJyb3JzOiB7fSB9KTtcclxuICAgICAgYXdhaXQgc3RvcmUuZGlzcGF0Y2gobG9naW4odGhpcy5zdGF0ZS5tb2RlbCkpO1xyXG4gICAgICBhd2FpdCBzdG9yZS5kaXNwYXRjaChzZXRUb2tlbih0aGlzLnByb3BzLnVzZXIudG9rZW4pKTtcclxuICAgICAgYXdhaXQgc3RvcmUuZGlzcGF0Y2gobWUoKSk7XHJcbiAgICAgIHRoaXMuc2V0U3RhdGUoeyBpc0xvYWRpbmc6IGZhbHNlIH0pO1xyXG4gICAgfSBjYXRjaCAoZSkge1xyXG4gICAgICBjb25zdCB7IGVycm9ycywgbWVzc2FnZSB9ID0gZ2V0RnJvbnRlbmRFcnJvcihlKTtcclxuICAgICAgdGhpcy5zZXRTdGF0ZSh7IGlzTG9hZGluZzogZmFsc2UsIGVycm9yczogey4uLmVycm9ycywgZ2VuZXJhbDogbWVzc2FnZSB9IH0pO1xyXG4gICAgfVxyXG4gIH07XHJcblxyXG4gIHJlbmRlcigpIHtcclxuICAgIGNvbnN0IHsgaXNMb2FkaW5nIH0gPSB0aGlzLnN0YXRlO1xyXG4gICAgY29uc3QgeyBjb25maWcsIHVzZXIgfSA9IHRoaXMucHJvcHM7XHJcbiAgICBjb25zdCBxdWVyeVBhcmFtcyA9IGdldFF1ZXJ5UGFyYW1zSnNvbih0aGlzLnByb3BzLmxvY2F0aW9uLnNlYXJjaCk7XHJcblxyXG4gICAgcmV0dXJuIChcclxuICAgICAgPFJlYWN0LkZyYWdtZW50PlxyXG4gICAgICAgIHtcclxuICAgICAgICAgIHF1ZXJ5UGFyYW1zLm1lc3NhZ2UgJiZcclxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiYWxlcnQgYWxlcnQtaW5mbyBtLTBcIj5cclxuICAgICAgICAgICAge3F1ZXJ5UGFyYW1zLm1lc3NhZ2V9XHJcbiAgICAgICAgICAgIDxidXR0b24gY2xhc3NOYW1lPVwiY2xvc2VcIiBhcmlhLWxhYmVsPVwiQ2xvc2VcIiBvbkNsaWNrPXsoKSA9PiB0aGlzLnByb3BzLmhpc3RvcnkucHVzaCh0aGlzLnByb3BzLmxvY2F0aW9uLnBhdGhuYW1lKX0+XHJcbiAgICAgICAgICAgICAgPHNwYW4gYXJpYS1oaWRkZW49XCJ0cnVlXCI+JnRpbWVzOzwvc3Bhbj5cclxuICAgICAgICAgICAgPC9idXR0b24+XHJcbiAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICB9XHJcbiAgICAgICAgPEhlYWRlclxyXG4gICAgICAgICAgY29uZmlnPXtjb25maWd9XHJcbiAgICAgICAgICB1c2VyPXt1c2VyfVxyXG4gICAgICAgICAgdGl0bGU9e3RyYW5zbGF0ZShjb25maWcudHJhbnNsYXRpb25zLCAnbG9naW5UaXRsZScpfVxyXG4gICAgICAgICAgZGVzY3JpcHRpb249e3RyYW5zbGF0ZShjb25maWcudHJhbnNsYXRpb25zLCAnbG9naW5EZXNjcmlwdGlvbicpfVxyXG4gICAgICAgICAgaGlzdG9yeT17dGhpcy5wcm9wcy5oaXN0b3J5fVxyXG4gICAgICAgICAgc2hvd0hlYWRpbmc9e3RydWV9IC8+XHJcbiAgICAgICAgPExvZ2luV3JhcHBlciBjbGFzc05hbWU9XCJjb250YWluZXJcIj5cclxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicm93XCI+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sLW1kLTEyXCI+XHJcbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJhbGVydCBhbGVydC1pbmZvXCIgcm9sZT1cImFsZXJ0XCI+XHJcbiAgICAgICAgICAgICAgICA8cCBjbGFzc05hbWU9XCJtYi0wXCI+XHJcbiAgICAgICAgICAgICAgICAgIDxMaW5rIHRvPVwiL3JlZ2lzdGVyXCI+e3RyYW5zbGF0ZShjb25maWcudHJhbnNsYXRpb25zLCAnaGF2ZU5vdEFjY291bnQnKX08L0xpbms+XHJcbiAgICAgICAgICAgICAgICA8L3A+XHJcbiAgICAgICAgICAgICAgICA8cCBjbGFzc05hbWU9XCJtYi0wXCI+XHJcbiAgICAgICAgICAgICAgICAgIDxMaW5rIHRvPVwiL3JlY292ZXItcGFzc3dvcmRcIj57dHJhbnNsYXRlKGNvbmZpZy50cmFuc2xhdGlvbnMsICdmb3Jnb3RQYXNzd29yZCcpfTwvTGluaz5cclxuICAgICAgICAgICAgICAgIDwvcD5cclxuICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImFsZXJ0IGFsZXJ0LXdhcm5pbmdcIiByb2xlPVwiYWxlcnRcIj5cclxuICAgICAgICAgICAgICAgIHt0cmFuc2xhdGUoY29uZmlnLnRyYW5zbGF0aW9ucywgJ3JlcXVpcmVkRmllbGRzJyl9XHJcbiAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zdGF0ZS5lcnJvcnMuZ2VuZXJhbCAmJlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJhbGVydCBhbGVydC1kYW5nZXJcIiByb2xlPVwiYWxlcnRcIj5cclxuICAgICAgICAgICAgICAgICAge3RoaXMuc3RhdGUuZXJyb3JzLmdlbmVyYWx9XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgPGZvcm0gb25TdWJtaXQ9e3RoaXMubG9naW59PlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJmb3JtLWdyb3VwXCI+XHJcbiAgICAgICAgICAgICAgICAgIDxsYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICB7dHJhbnNsYXRlKGNvbmZpZy50cmFuc2xhdGlvbnMsICdlbWFpbCcpfSA8c3Bhbj4qPC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICA8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICA8aW5wdXRcclxuICAgICAgICAgICAgICAgICAgICB0eXBlPVwidGV4dFwiXHJcbiAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiZm9ybS1jb250cm9sXCJcclxuICAgICAgICAgICAgICAgICAgICB2YWx1ZT17dGhpcy5zdGF0ZS5tb2RlbC5lbWFpbH1cclxuICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17ZSA9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IG1vZGVsOiBPYmplY3QuYXNzaWduKHRoaXMuc3RhdGUubW9kZWwsIHsgZW1haWw6IChlLnRhcmdldC52YWx1ZSB8fCAnJylcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAucmVwbGFjZSgvXFxzL2csICcnKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KSBcclxuICAgICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAgICAgfSAvPlxyXG4gICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJ0ZXh0LWRhbmdlclwiPnt0aGlzLnN0YXRlLmVycm9ycy5lbWFpbH08L3NwYW4+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZm9ybS1ncm91cFwiPlxyXG4gICAgICAgICAgICAgICAgICA8bGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAge3RyYW5zbGF0ZShjb25maWcudHJhbnNsYXRpb25zLCAncGFzc3dvcmQnKX0gPHNwYW4+Kjwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgPGlucHV0XHJcbiAgICAgICAgICAgICAgICAgICAgdHlwZT1cInBhc3N3b3JkXCJcclxuICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJmb3JtLWNvbnRyb2xcIlxyXG4gICAgICAgICAgICAgICAgICAgIHZhbHVlPXt0aGlzLnN0YXRlLm1vZGVsLnBhc3N3b3JkfVxyXG4gICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXtlID0+IHRoaXMuc2V0U3RhdGUoeyBtb2RlbDogT2JqZWN0LmFzc2lnbih0aGlzLnN0YXRlLm1vZGVsLCB7IHBhc3N3b3JkOiBlLnRhcmdldC52YWx1ZSB9KSB9KX0gLz5cclxuICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwidGV4dC1kYW5nZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICB7dGhpcy5zdGF0ZS5lcnJvcnMucGFzc3dvcmR9XHJcbiAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJmb3JtLWdyb3VwIHRleHQtcmlnaHRcIj5cclxuICAgICAgICAgICAgICAgICAgPGJ1dHRvblxyXG4gICAgICAgICAgICAgICAgICAgIHR5cGU9XCJzdWJtaXRcIlxyXG4gICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cImJ0biBidG4tcHJpbWFyeVwiXHJcbiAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ9e2lzTG9hZGluZ30+XHJcbiAgICAgICAgICAgICAgICAgICAge3RyYW5zbGF0ZShjb25maWcudHJhbnNsYXRpb25zLCAnbG9naW4nKX1cclxuICAgICAgICAgICAgICAgICAgPC9idXR0b24+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICA8L2Zvcm0+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9Mb2dpbldyYXBwZXI+XHJcbiAgICAgICAgPEZvb3RlciBcclxuICAgICAgICAgIGNvbmZpZz17Y29uZmlnfVxyXG4gICAgICAgICAgcm91dGVySGlzdG9yeT17dGhpcy5wcm9wcy5oaXN0b3J5fSAvPlxyXG4gICAgICA8L1JlYWN0LkZyYWdtZW50PlxyXG4gICAgKTtcclxuICB9XHJcbn1cclxuXHJcbmNvbnN0IG1hcFN0YXRlVG9Qcm9wcyA9IHN0YXRlID0+ICh7XHJcbiAgYXV0aDogc3RhdGUudXNlci5hdXRoLFxyXG4gIGNvbmZpZzogc3RhdGUuY29uZmlnLFxyXG4gIHVzZXI6IHN0YXRlLnVzZXJcclxufSk7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjb25uZWN0KG1hcFN0YXRlVG9Qcm9wcykoTG9naW4pO1xyXG4iLCJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgeyB3aXRoUm91dGVyIH0gZnJvbSAncmVhY3Qtcm91dGVyLWRvbSc7XHJcbmltcG9ydCBDb250ZXh0IGZyb20gJy4uL2NvbnRleHQnO1xyXG5pbXBvcnQgeyBtZSwgc2V0VG9rZW4gfSBmcm9tICcuLi9zdG9yZS9hY3Rpb25zL3VzZXItYWN0aW9ucyc7XHJcbmltcG9ydCBzdG9yZSBmcm9tICcuLi9zdG9yZS9zdG9yZSc7XHJcblxyXG5jbGFzcyBNYWluIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcclxuICBzdGF0aWMgY29udGV4dFR5cGUgPSBDb250ZXh0O1xyXG5cclxuICBjb21wb25lbnREaWRNb3VudCA9ICgpID0+IHtcclxuICAgIHN0b3JlLmRpc3BhdGNoKHNldFRva2VuKGxvY2FsU3RvcmFnZS50b2tlbikpO1xyXG4gICAgc3RvcmUuZGlzcGF0Y2gobWUoKSlcclxuICAgICAgLmZpbmFsbHkoKCkgPT4gdGhpcy5jb250ZXh0LmxvYWRQcmVmZXJlbmNlcygpKTsgXHJcbiAgfSBcclxuXHJcbiAgcmVuZGVyKCkge1xyXG4gICAgcmV0dXJuIChcclxuICAgICAgPFJlYWN0LkZyYWdtZW50PlxyXG4gICAgICAgIHt0aGlzLnByb3BzLmNoaWxkcmVufVxyXG4gICAgICA8L1JlYWN0LkZyYWdtZW50PlxyXG4gICAgKTtcclxuICB9XHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IHdpdGhSb3V0ZXIoTWFpbik7XHJcbiIsImltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCB7IGNvbm5lY3QgfSBmcm9tICdyZWFjdC1yZWR1eCc7XHJcbmltcG9ydCB7IExpbmsgfSBmcm9tICdyZWFjdC1yb3V0ZXItZG9tJztcclxuaW1wb3J0IHN0eWxlZCBmcm9tICdzdHlsZWQtY29tcG9uZW50cyc7XHJcbmltcG9ydCBIZWFkZXIgZnJvbSAnLi9fY29tcG9uZW50cy9oZWFkZXInO1xyXG5pbXBvcnQgRm9vdGVyIGZyb20gJy4vX2NvbXBvbmVudHMvZm9vdGVyJztcclxuaW1wb3J0IHsgdHJhbnNsYXRlIH0gZnJvbSAnLi4vdXRpbHMvaW50bC11dGlscyc7XHJcblxyXG5jb25zdCBOb3RGb3VuZFdyYXBwZXIgPSBzdHlsZWQubWFpbmBcclxuICAmLCAqIHtcclxuICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgfVxyXG5gO1xyXG5jbGFzcyBOb3RGb3VuZCBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XHJcbiAgcmVuZGVyKCkge1xyXG4gICAgY29uc3QgeyBjb25maWcsIHVzZXIgfSA9IHRoaXMucHJvcHM7XHJcblxyXG4gICAgcmV0dXJuIChcclxuICAgICAgPFJlYWN0LkZyYWdtZW50PlxyXG4gICAgICAgIDxIZWFkZXJcclxuICAgICAgICAgIGNvbmZpZz17Y29uZmlnfVxyXG4gICAgICAgICAgdXNlcj17dXNlcn1cclxuICAgICAgICAgIHRpdGxlPXt0cmFuc2xhdGUoY29uZmlnLnRyYW5zbGF0aW9ucywgJ25vdEZvdW5kVGl0bGUnKX1cclxuICAgICAgICAgIGRlc2NyaXB0aW9uPXt0cmFuc2xhdGUoY29uZmlnLnRyYW5zbGF0aW9ucywgJ25vdEZvdW5kRGVzY3JpcHRpb24nKX1cclxuICAgICAgICAgIGhpc3Rvcnk9e3RoaXMucHJvcHMuaGlzdG9yeX1cclxuICAgICAgICAgIHNob3dIZWFkaW5nPXt0cnVlfSAvPlxyXG4gICAgICAgIDxOb3RGb3VuZFdyYXBwZXIgY2xhc3NOYW1lPVwiY29udGFpbmVyIHRleHQtY2VudGVyXCI+XHJcbiAgICAgICAgICA8aDE+PGkgY2xhc3NOYW1lPVwiZmFzIGZhLXVubGluayBmYS0yeFwiIC8+PC9oMT5cclxuICAgICAgICAgIDxwPnt0cmFuc2xhdGUoY29uZmlnLnRyYW5zbGF0aW9ucywgJ25vdEZvdW5kRGVzY3JpcHRpb24nKX08L3A+XHJcbiAgICAgICAgICA8cD48TGluayB0bz1cIi9cIj57dHJhbnNsYXRlKGNvbmZpZy50cmFuc2xhdGlvbnMsICdnb0hvbWVQYWdlJyl9PC9MaW5rPjwvcD5cclxuICAgICAgICA8L05vdEZvdW5kV3JhcHBlcj5cclxuICAgICAgICA8Rm9vdGVyIFxyXG4gICAgICAgICAgY29uZmlnPXtjb25maWd9XHJcbiAgICAgICAgICByb3V0ZXJIaXN0b3J5PXt0aGlzLnByb3BzLmhpc3Rvcnl9IC8+XHJcbiAgICAgIDwvUmVhY3QuRnJhZ21lbnQ+XHJcbiAgICApO1xyXG4gIH1cclxufVxyXG5cclxuY29uc3QgbWFwU3RhdGVUb1Byb3BzID0gc3RhdGUgPT4gKHtcclxuICBjb25maWc6IHN0YXRlLmNvbmZpZyxcclxuICB1c2VyOiBzdGF0ZS51c2VyXHJcbn0pO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY29ubmVjdChtYXBTdGF0ZVRvUHJvcHMpKE5vdEZvdW5kKTtcclxuIiwiaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IHsgY29ubmVjdCB9IGZyb20gJ3JlYWN0LXJlZHV4JztcclxuaW1wb3J0IHN0eWxlZCBmcm9tICdzdHlsZWQtY29tcG9uZW50cyc7XHJcbmltcG9ydCB7IHRyYW5zbGF0ZSB9IGZyb20gJy4uL3V0aWxzL2ludGwtdXRpbHMnO1xyXG5pbXBvcnQgeyBnZXRGcm9udGVuZEVycm9yIH0gZnJvbSAnLi4vdXRpbHMvcmVzcG9uc2UtdXRpbHMnO1xyXG5pbXBvcnQgeyByZWNvdmVyUGFzc3dvcmQgfSBmcm9tICcuLi9zdG9yZS9hY3Rpb25zL3VzZXItYWN0aW9ucyc7XHJcbmltcG9ydCBIZWFkZXIgZnJvbSAnLi9fY29tcG9uZW50cy9oZWFkZXInO1xyXG5pbXBvcnQgRm9vdGVyIGZyb20gJy4vX2NvbXBvbmVudHMvZm9vdGVyJztcclxuaW1wb3J0IHN0b3JlIGZyb20gJy4uL3N0b3JlL3N0b3JlJztcclxuXHJcbmNvbnN0IFJlY292ZXJQYXNzd29yZFdyYXBwZXIgPSBzdHlsZWQubWFpbmBcclxuICAmLCAqIHtcclxuICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgfVxyXG5gO1xyXG5cclxuY2xhc3MgUmVjb3ZlclBhc3N3b3JkIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcclxuICBzdGF0ZSA9IHtcclxuICAgIGVycm9yczoge30sXHJcbiAgICBtb2RlbDoge1xyXG4gICAgICBlbWFpbDogJydcclxuICAgIH1cclxuICB9O1xyXG5cclxuICBmb3Jnb3RQYXNzd29yZCA9IGFzeW5jIGUgPT4ge1xyXG4gICAgdHJ5IHtcclxuICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cclxuICAgICAgdGhpcy5zZXRTdGF0ZSh7IGlzTG9hZGluZzogdHJ1ZSwgZXJyb3JzOiB7fSB9KTtcclxuICAgICAgYXdhaXQgdGhpcy5zZXRTdGF0ZSh7XHJcbiAgICAgICAgbW9kZWw6IHtcclxuICAgICAgICAgIC4uLnRoaXMuc3RhdGUubW9kZWwsXHJcbiAgICAgICAgICB0b2tlbjogdGhpcy5wcm9wcy5tYXRjaC5wYXJhbXMudG9rZW5cclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgICBhd2FpdCBzdG9yZS5kaXNwYXRjaChyZWNvdmVyUGFzc3dvcmQodGhpcy5zdGF0ZS5tb2RlbCkpO1xyXG4gICAgICBjb25zdCBzdWNjZXNzTWVzc2FnZSA9IHRoaXMucHJvcHMudXNlci50ZW1wLm1lc3NhZ2U7XHJcbiAgICAgIHRoaXMuc2V0U3RhdGUoeyBpc0xvYWRpbmc6IGZhbHNlIH0pO1xyXG4gICAgICB0aGlzLnByb3BzLmhpc3RvcnkucHVzaChgL2xvZ2luP21lc3NhZ2U9JHtzdWNjZXNzTWVzc2FnZX1gKTtcclxuICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgY29uc3QgeyBlcnJvcnMsIG1lc3NhZ2UgfSA9IGdldEZyb250ZW5kRXJyb3IoZSk7XHJcbiAgICAgIHRoaXMuc2V0U3RhdGUoeyBpc0xvYWRpbmc6IGZhbHNlLCBlcnJvcnM6IHsgLi4uZXJyb3JzLCBnZW5lcmFsOiBtZXNzYWdlIH0gfSk7XHJcbiAgICB9XHJcbiAgfTtcclxuXHJcbiAgcmVuZGVyKCkge1xyXG4gICAgY29uc3QgeyBpc0xvYWRpbmcgfSA9IHRoaXMuc3RhdGU7XHJcbiAgICBjb25zdCB7IGNvbmZpZywgdXNlciB9ID0gdGhpcy5wcm9wcztcclxuXHJcbiAgICByZXR1cm4gKFxyXG4gICAgICA8UmVhY3QuRnJhZ21lbnQ+XHJcbiAgICAgICAgPEhlYWRlclxyXG4gICAgICAgICAgY29uZmlnPXtjb25maWd9XHJcbiAgICAgICAgICB1c2VyPXt1c2VyfVxyXG4gICAgICAgICAgdGl0bGU9e3RyYW5zbGF0ZShjb25maWcudHJhbnNsYXRpb25zLCAncmVjb3ZlclBhc3N3b3JkVGl0bGUnKX1cclxuICAgICAgICAgIGRlc2NyaXB0aW9uPXt0cmFuc2xhdGUoY29uZmlnLnRyYW5zbGF0aW9ucywgJ3JlY292ZXJQYXNzd29yZERlc2NyaXB0aW9uJyl9XHJcbiAgICAgICAgICBoaXN0b3J5PXt0aGlzLnByb3BzLmhpc3Rvcnl9XHJcbiAgICAgICAgICBzaG93SGVhZGluZz17dHJ1ZX0gLz5cclxuICAgICAgICA8UmVjb3ZlclBhc3N3b3JkV3JhcHBlciBjbGFzc05hbWU9XCJjb250YWluZXJcIj5cclxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicm93XCI+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sLW1kLTEyXCI+XHJcbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJhbGVydCBhbGVydC13YXJuaW5nXCIgcm9sZT1cImFsZXJ0XCI+XHJcbiAgICAgICAgICAgICAgICB7dHJhbnNsYXRlKGNvbmZpZy50cmFuc2xhdGlvbnMsICdyZXF1aXJlZEZpZWxkcycpfVxyXG4gICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuc3RhdGUuZXJyb3JzLmdlbmVyYWwgJiZcclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiYWxlcnQgYWxlcnQtZGFuZ2VyXCIgcm9sZT1cImFsZXJ0XCI+XHJcbiAgICAgICAgICAgICAgICAgIHt0aGlzLnN0YXRlLmVycm9ycy5nZW5lcmFsfVxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIDxmb3JtIG9uU3VibWl0PXt0aGlzLmZvcmdvdFBhc3N3b3JkfT5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZm9ybS1ncm91cFwiPlxyXG4gICAgICAgICAgICAgICAgICA8bGFiZWw+e3RyYW5zbGF0ZShjb25maWcudHJhbnNsYXRpb25zLCAnZW1haWwnKX0gKjwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgIDxpbnB1dFxyXG4gICAgICAgICAgICAgICAgICAgIHR5cGU9XCJ0ZXh0XCJcclxuICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJmb3JtLWNvbnRyb2xcIlxyXG4gICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXtlID0+IFxyXG4gICAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IG1vZGVsOiBPYmplY3QuYXNzaWduKHRoaXMuc3RhdGUubW9kZWwsIHsgZW1haWw6IGUudGFyZ2V0LnZhbHVlIH0pIH0pXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJ0ZXh0LWRhbmdlclwiPnt0aGlzLnN0YXRlLmVycm9ycy5lbWFpbH08L3NwYW4+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZm9ybS1ncm91cCB0ZXh0LXJpZ2h0XCI+XHJcbiAgICAgICAgICAgICAgICAgIDxidXR0b24gdHlwZT1cInN1Ym1pdFwiIGNsYXNzTmFtZT1cImJ0biBidG4tcHJpbWFyeVwiIGRpc2FibGVkPXtpc0xvYWRpbmd9PlxyXG4gICAgICAgICAgICAgICAgICAgIHt0cmFuc2xhdGUoY29uZmlnLnRyYW5zbGF0aW9ucywgJ3JlY292ZXJQYXNzd29yZCcpfVxyXG4gICAgICAgICAgICAgICAgICA8L2J1dHRvbj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgIDwvZm9ybT5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8L1JlY292ZXJQYXNzd29yZFdyYXBwZXI+XHJcbiAgICAgICAgPEZvb3RlciBcclxuICAgICAgICAgIGNvbmZpZz17Y29uZmlnfVxyXG4gICAgICAgICAgcm91dGVySGlzdG9yeT17dGhpcy5wcm9wcy5oaXN0b3J5fSAvPlxyXG4gICAgICA8L1JlYWN0LkZyYWdtZW50PlxyXG4gICAgKTtcclxuICB9XHJcbn1cclxuXHJcbmNvbnN0IG1hcFN0YXRlVG9Qcm9wcyA9IHN0YXRlID0+ICh7XHJcbiAgY29uZmlnOiBzdGF0ZS5jb25maWcsXHJcbiAgdXNlcjogc3RhdGUudXNlclxyXG59KTtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNvbm5lY3QobWFwU3RhdGVUb1Byb3BzKShSZWNvdmVyUGFzc3dvcmQpO1xyXG4iLCJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgeyBjb25uZWN0IH0gZnJvbSAncmVhY3QtcmVkdXgnO1xyXG5pbXBvcnQgeyBMaW5rIH0gZnJvbSAncmVhY3Qtcm91dGVyLWRvbSc7XHJcbmltcG9ydCBzdHlsZWQgZnJvbSAnc3R5bGVkLWNvbXBvbmVudHMnO1xyXG5pbXBvcnQgeyB0cmFuc2xhdGUgfSBmcm9tICcuLi91dGlscy9pbnRsLXV0aWxzJztcclxuaW1wb3J0IHsgZ2V0RnJvbnRlbmRFcnJvciB9IGZyb20gJy4uL3V0aWxzL3Jlc3BvbnNlLXV0aWxzJztcclxuaW1wb3J0IEhlYWRlciBmcm9tICcuL19jb21wb25lbnRzL2hlYWRlcic7XHJcbmltcG9ydCBGb290ZXIgZnJvbSAnLi9fY29tcG9uZW50cy9mb290ZXInO1xyXG5pbXBvcnQgc3RvcmUgZnJvbSAnLi4vc3RvcmUvc3RvcmUnO1xyXG5pbXBvcnQgeyByZWdpc3Rlciwgc2V0VG9rZW4sIG1lIH0gZnJvbSAnLi4vc3RvcmUvYWN0aW9ucy91c2VyLWFjdGlvbnMnO1xyXG5cclxuY29uc3QgUmVnaXN0ZXJXcmFwcGVyID0gc3R5bGVkLm1haW5gXHJcbiAgJiwgKiB7XHJcbiAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG4gIH1cclxuYDtcclxuXHJcbmNsYXNzIFJlZ2lzdGVyIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcclxuICBzdGF0ZSA9IHtcclxuICAgIGlzTG9hZGluZzogZmFsc2UsXHJcbiAgICBlcnJvcnM6IHt9LFxyXG4gICAgbW9kZWw6IHtcclxuICAgICAgYWN0aXZlOiB0cnVlLFxyXG4gICAgICBlbWFpbDogJycsXHJcbiAgICAgIHBhc3N3b3JkOiAnJyxcclxuICAgICAgcGFzc3dvcmRDb25maXJtYXRpb246ICcnLFxyXG4gICAgICBwbGFuOiB0aGlzLnByb3BzLmNvbmZpZy5wbGFucy5mcmVlXHJcbiAgICB9XHJcbiAgfTtcclxuXHJcbiAgY29tcG9uZW50RGlkTW91bnQgPSAoKSA9PiB7XHJcbiAgICBpZiAodGhpcy5wcm9wcy51c2VyLmF1dGgpIHtcclxuICAgICAgdGhpcy5wcm9wcy5oaXN0b3J5LnB1c2goJy9hcHAvZGFzaGJvYXJkJyk7XHJcbiAgICB9XHJcbiAgfTtcclxuXHJcbiAgcmVnaXN0ZXJXaXRoRW1haWwgPSBhc3luYyBlID0+IHtcclxuICAgIHRyeSB7XHJcbiAgICAgIGlmIChlKSB7XHJcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICB9XHJcblxyXG4gICAgICB0aGlzLnNldFN0YXRlKHsgaXNMb2FkaW5nOiB0cnVlLCBlcnJvcnM6IHt9IH0pO1xyXG4gICAgICBhd2FpdCBzdG9yZS5kaXNwYXRjaChyZWdpc3Rlcih0aGlzLnN0YXRlLm1vZGVsKSk7XHJcbiAgICAgIGF3YWl0IHN0b3JlLmRpc3BhdGNoKHNldFRva2VuKHRoaXMucHJvcHMudXNlci50b2tlbikpO1xyXG4gICAgICBhd2FpdCBzdG9yZS5kaXNwYXRjaChtZSgpKTtcclxuICAgICAgdGhpcy5zZXRTdGF0ZSh7IGlzTG9hZGluZzogZmFsc2UgfSk7XHJcbiAgICAgIHRoaXMucHJvcHMuaGlzdG9yeS5wdXNoKCcvYXBwL2Rhc2hib2FyZCcpO1xyXG4gICAgfSBjYXRjaCAoZSkge1xyXG4gICAgICBjb25zdCB7IGVycm9ycywgbWVzc2FnZSB9ID0gZ2V0RnJvbnRlbmRFcnJvcihlKTtcclxuICAgICAgdGhpcy5zZXRTdGF0ZSh7IGlzTG9hZGluZzogZmFsc2UsIGVycm9yczogey4uLmVycm9ycywgZ2VuZXJhbDogbWVzc2FnZSB9IH0pO1xyXG4gICAgfVxyXG4gIH07XHJcblxyXG4gIHJlbmRlcigpIHtcclxuICAgIGNvbnN0IHsgaXNMb2FkaW5nIH0gPSB0aGlzLnN0YXRlO1xyXG4gICAgY29uc3QgeyBjb25maWcsIHVzZXIgfSA9IHRoaXMucHJvcHM7XHJcblxyXG4gICAgcmV0dXJuIChcclxuICAgICAgPFJlYWN0LkZyYWdtZW50PlxyXG4gICAgICAgIDxIZWFkZXJcclxuICAgICAgICAgIGNvbmZpZz17Y29uZmlnfVxyXG4gICAgICAgICAgdXNlcj17dXNlcn1cclxuICAgICAgICAgIHRpdGxlPXt0cmFuc2xhdGUoY29uZmlnLnRyYW5zbGF0aW9ucywgJ3JlZ2lzdGVyVGl0bGUnKX1cclxuICAgICAgICAgIGRlc2NyaXB0aW9uPXt0cmFuc2xhdGUoY29uZmlnLnRyYW5zbGF0aW9ucywgJ3JlZ2lzdGVyRGVzY3JpcHRpb24nKX1cclxuICAgICAgICAgIGhpc3Rvcnk9e3RoaXMucHJvcHMuaGlzdG9yeX1cclxuICAgICAgICAgIHNob3dIZWFkaW5nPXt0cnVlfSAvPlxyXG4gICAgICAgIDxSZWdpc3RlcldyYXBwZXIgY2xhc3NOYW1lPVwiY29udGFpbmVyXCI+XHJcbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInJvd1wiPlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbC1tZC0xMlwiPlxyXG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiYWxlcnQgYWxlcnQtaW5mb1wiPlxyXG4gICAgICAgICAgICAgICAge3RyYW5zbGF0ZShjb25maWcudHJhbnNsYXRpb25zLCAndXNlckFscmVhZHlIYXNBY2NvdW50Jyl9eycgJ31cclxuICAgICAgICAgICAgICAgIDxMaW5rIHRvPVwiL2xvZ2luXCI+e3RyYW5zbGF0ZShjb25maWcudHJhbnNsYXRpb25zLCAnaGVyZScpfTwvTGluaz5cclxuICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImFsZXJ0IGFsZXJ0LXdhcm5pbmdcIiByb2xlPVwiYWxlcnRcIj5cclxuICAgICAgICAgICAgICAgIHt0cmFuc2xhdGUoY29uZmlnLnRyYW5zbGF0aW9ucywgJ3JlcXVpcmVkRmllbGRzJyl9XHJcbiAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zdGF0ZS5lcnJvcnMuZ2VuZXJhbCAmJlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJhbGVydCBhbGVydC1kYW5nZXJcIiByb2xlPVwiYWxlcnRcIj5cclxuICAgICAgICAgICAgICAgICAge3RoaXMuc3RhdGUuZXJyb3JzLmdlbmVyYWx9XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgPGZvcm0gb25TdWJtaXQ9e3RoaXMucmVnaXN0ZXJXaXRoRW1haWx9PlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJmb3JtLWdyb3VwXCI+XHJcbiAgICAgICAgICAgICAgICAgIDxsYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICB7dHJhbnNsYXRlKGNvbmZpZy50cmFuc2xhdGlvbnMsICdlbWFpbCcpfSA8c3Bhbj4qPC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICA8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICA8aW5wdXRcclxuICAgICAgICAgICAgICAgICAgICB0eXBlPVwidGV4dFwiXHJcbiAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiZm9ybS1jb250cm9sXCJcclxuICAgICAgICAgICAgICAgICAgICB2YWx1ZT17dGhpcy5zdGF0ZS5tb2RlbC5lbWFpbH1cclxuICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17ZSA9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IG1vZGVsOiBPYmplY3QuYXNzaWduKHRoaXMuc3RhdGUubW9kZWwsIHsgZW1haWw6IChlLnRhcmdldC52YWx1ZSB8fCAnJylcclxuICAgICAgICAgICAgICAgICAgICAgICAgLnJlcGxhY2UoL1xccy9nLCAnJykgXHJcbiAgICAgICAgICAgICAgICAgICAgICB9KSBcclxuICAgICAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwidGV4dC1kYW5nZXJcIj57dGhpcy5zdGF0ZS5lcnJvcnMuZW1haWx9PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImZvcm0tZ3JvdXBcIj5cclxuICAgICAgICAgICAgICAgICAgPGxhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgIHt0cmFuc2xhdGUoY29uZmlnLnRyYW5zbGF0aW9ucywgJ3Bhc3N3b3JkJyl9IDxzcGFuPio8L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgIDwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgIDxpbnB1dFxyXG4gICAgICAgICAgICAgICAgICAgIHR5cGU9XCJwYXNzd29yZFwiXHJcbiAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiZm9ybS1jb250cm9sXCJcclxuICAgICAgICAgICAgICAgICAgICB2YWx1ZT17dGhpcy5zdGF0ZS5tb2RlbC5wYXNzd29yZH1cclxuICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17ZSA9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IG1vZGVsOiBPYmplY3QuYXNzaWduKHRoaXMuc3RhdGUubW9kZWwsIHsgcGFzc3dvcmQ6IGUudGFyZ2V0LnZhbHVlIH0pIH0pXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJ0ZXh0LWRhbmdlclwiPlxyXG4gICAgICAgICAgICAgICAgICAgIHt0aGlzLnN0YXRlLmVycm9ycy5wYXNzd29yZH1cclxuICAgICAgICAgICAgICAgICAgPC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImZvcm0tZ3JvdXBcIj5cclxuICAgICAgICAgICAgICAgICAgPGxhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgIHt0cmFuc2xhdGUoY29uZmlnLnRyYW5zbGF0aW9ucywgJ3Bhc3N3b3JkQ29uZmlybScpfSA8c3Bhbj4qPC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICA8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICA8aW5wdXRcclxuICAgICAgICAgICAgICAgICAgICB0eXBlPVwicGFzc3dvcmRcIlxyXG4gICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cImZvcm0tY29udHJvbFwiXHJcbiAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9e2UgPT5cclxuICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoeyBtb2RlbDogT2JqZWN0LmFzc2lnbih0aGlzLnN0YXRlLm1vZGVsLCB7IHBhc3N3b3JkQ29uZmlybWF0aW9uOiBlLnRhcmdldC52YWx1ZSB9KSB9KVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwidGV4dC1kYW5nZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICB7dGhpcy5zdGF0ZS5lcnJvcnMucGFzc3dvcmRDb25maXJtYXRpb259XHJcbiAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJmb3JtLWdyb3VwIHRleHQtcmlnaHRcIj5cclxuICAgICAgICAgICAgICAgICAgPGJ1dHRvbiB0eXBlPVwic3VibWl0XCIgZGlzYWJsZWQ9e2lzTG9hZGluZ30gY2xhc3NOYW1lPVwiYnRuIGJ0bi1wcmltYXJ5XCI+XHJcbiAgICAgICAgICAgICAgICAgICAge3RyYW5zbGF0ZShjb25maWcudHJhbnNsYXRpb25zLCAncmVnaXN0ZXInKX1cclxuICAgICAgICAgICAgICAgICAgPC9idXR0b24+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICA8L2Zvcm0+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9SZWdpc3RlcldyYXBwZXI+XHJcbiAgICAgICAgPEZvb3RlciBcclxuICAgICAgICAgIGNvbmZpZz17Y29uZmlnfVxyXG4gICAgICAgICAgcm91dGVySGlzdG9yeT17dGhpcy5wcm9wcy5oaXN0b3J5fSAvPlxyXG4gICAgICA8L1JlYWN0LkZyYWdtZW50PlxyXG4gICAgKTtcclxuICB9XHJcbn1cclxuXHJcbmNvbnN0IG1hcFN0YXRlVG9Qcm9wcyA9IHN0YXRlID0+ICh7XHJcbiAgY29uZmlnOiBzdGF0ZS5jb25maWcsXHJcbiAgdXNlcjogc3RhdGUudXNlclxyXG59KTtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNvbm5lY3QobWFwU3RhdGVUb1Byb3BzKShSZWdpc3Rlcik7XHJcbiIsImltcG9ydCBIb21lIGZyb20gJy4vcGFnZXMvaG9tZSc7XHJcbmltcG9ydCBMb2dpbiBmcm9tICcuL3BhZ2VzL2xvZ2luJztcclxuaW1wb3J0IFJlY292ZXJQYXNzd29yZCBmcm9tICcuL3BhZ2VzL3JlY292ZXItcGFzc3dvcmQnO1xyXG5pbXBvcnQgUmVnaXN0ZXIgZnJvbSAnLi9wYWdlcy9yZWdpc3Rlcic7XHJcbmltcG9ydCBOb3RGb3VuZCBmcm9tICcuL3BhZ2VzL25vdC1mb3VuZCc7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBbXHJcbiAge1xyXG4gICAgZXhhY3Q6IHRydWUsXHJcbiAgICBwYXRoOiAnLycsXHJcbiAgICBjb21wb25lbnQ6IEhvbWVcclxuICB9LFxyXG4gIHtcclxuICAgIHBhdGg6ICcvbG9naW4nLFxyXG4gICAgY29tcG9uZW50OiBMb2dpblxyXG4gIH0sXHJcbiAge1xyXG4gICAgcGF0aDogJy9sb2dpbi86cHJvdmlkZXI/Lzp0b2tlbicsXHJcbiAgICBjb21wb25lbnQ6IExvZ2luXHJcbiAgfSxcclxuICB7XHJcbiAgICBwYXRoOiAnL3JlZ2lzdGVyJyxcclxuICAgIGNvbXBvbmVudDogUmVnaXN0ZXJcclxuICB9LFxyXG4gIHtcclxuICAgIHBhdGg6ICcvcmVjb3Zlci1wYXNzd29yZCcsXHJcbiAgICBjb21wb25lbnQ6IFJlY292ZXJQYXNzd29yZFxyXG4gIH0sXHJcbiAge1xyXG4gICAgY29tcG9uZW50OiBOb3RGb3VuZFxyXG4gIH1cclxuXTtcclxuIiwiLy8gbGlicmFyaWVzXHJcbmltcG9ydCAqIGFzIGZ1bmN0aW9ucyBmcm9tICdmaXJlYmFzZS1mdW5jdGlvbnMnO1xyXG5pbXBvcnQgZXhwcmVzcyBmcm9tICdleHByZXNzJztcclxuaW1wb3J0IGZzIGZyb20gJ2ZzJztcclxuaW1wb3J0IHsgSGVsbWV0IH0gZnJvbSAncmVhY3QtaGVsbWV0JztcclxuaW1wb3J0IHNlcmlhbGl6ZSBmcm9tICdzZXJpYWxpemUtamF2YXNjcmlwdCc7XHJcbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCB7IFByb3ZpZGVyIH0gZnJvbSAncmVhY3QtcmVkdXgnO1xyXG5pbXBvcnQgeyBSb3V0ZSwgU3dpdGNoLCBTdGF0aWNSb3V0ZXIsIG1hdGNoUGF0aCB9IGZyb20gJ3JlYWN0LXJvdXRlci1kb20nO1xyXG5pbXBvcnQgeyByZW5kZXJUb1N0cmluZyB9IGZyb20gJ3JlYWN0LWRvbS9zZXJ2ZXInO1xyXG5pbXBvcnQgeyBTZXJ2ZXJTdHlsZVNoZWV0LCBTdHlsZVNoZWV0TWFuYWdlciB9IGZyb20gJ3N0eWxlZC1jb21wb25lbnRzJztcclxuLy8gdXRpbGl0aWVzXHJcbmltcG9ydCBjb25maWd1cmF0aW9uIGZyb20gJy4vc3RhdGljL2NvbmZpZ3VyYXRpb24uanNvbic7XHJcbmltcG9ydCBNYWluIGZyb20gJy4vcGFnZXMvbWFpbic7XHJcbmltcG9ydCBzdG9yZSBmcm9tICcuL3N0b3JlL3N0b3JlJztcclxuaW1wb3J0IHsgc2V0Q29uZmlndXJhdGlvbiwgc2V0UHJlZmVyZW5jZXMgfSBmcm9tICcuL3N0b3JlL2FjdGlvbnMvY29uZmlnLWFjdGlvbnMnO1xyXG5pbXBvcnQgeyBnZXRRdWVyeVBhcmFtc0pzb24gfSBmcm9tICcuL3V0aWxzL3JlcXVlc3QtdXRpbHMnO1xyXG5pbXBvcnQgeyBnZXRCYWNrZW5kRXJyb3IgfSBmcm9tICcuL3V0aWxzL3Jlc3BvbnNlLXV0aWxzJztcclxuaW1wb3J0IHJvdXRlc1NTUiBmcm9tICcuL3JvdXRlcy1zc3InO1xyXG4vLyByb3V0ZXNcclxuaW1wb3J0IHJvdXRlc0NhdGFsb2cgZnJvbSAnLi9hcGlzL3JvdXRlcy9jYXRhbG9nJztcclxuaW1wb3J0IHJvdXRlc0NvbmZpZyBmcm9tICcuL2FwaXMvcm91dGVzL2NvbmZpZyc7XHJcbmltcG9ydCByb3V0ZXNMb2NhbGUgZnJvbSAnLi9hcGlzL3JvdXRlcy9sb2NhbGUnO1xyXG5pbXBvcnQgcm91dGVzVXNlciBmcm9tICcuL2FwaXMvcm91dGVzL3VzZXInO1xyXG5pbXBvcnQgcm91dGVzU29jaWFsTGVhZGVyIGZyb20gJy4vYXBpcy9yb3V0ZXMvc29jaWFsTGVhZGVyJztcclxuXHJcbi8vIGhlbHBlcnNcclxuc3RvcmUuZGlzcGF0Y2goc2V0Q29uZmlndXJhdGlvbihjb25maWd1cmF0aW9uKSk7XHJcbmNvbnN0IGluZGV4SHRtbCA9IGZzLnJlYWRGaWxlU3luYygnLi9zcmMvaW5kZXguaHRtbCcsICd1dGY4Jyk7IFxyXG5jb25zdCBtYWludGVuYW5jZUh0bWwgPSBmcy5yZWFkRmlsZVN5bmMoJy4vc3JjL3N0YXRpYy9tYWludGVuYW5jZS5odG1sJywgJ3V0ZjgnKTsgXHJcbmNvbnN0IGFwcFNlcnZlciA9IGV4cHJlc3MoKTtcclxuY29uc3QgY29ycyA9IHJlcXVpcmUoJ2NvcnMnKSh7IG9yaWdpbjogdHJ1ZSB9KTtcclxuXHJcbi8vIHNlcnZlciBjb25maWd1cmF0aW9uXHJcbmFwcFNlcnZlci51c2UoY29ycyk7XHJcbmFwcFNlcnZlci51c2UoKHJlcSwgcmVzLCBuZXh0KSA9PiB7XHJcbiAgdHJ5IHtcclxuICAgIGNvbnN0IGFjY2VwdExhbmd1YWdlID0gKHJlcS5oZWFkZXJzWydhY2NlcHQtbGFuZ3VhZ2UnXSB8fCAnJykudG9Mb3dlckNhc2UoKTtcclxuXHJcbiAgICByZXEuY29uZmlnID0gc3RvcmUuZ2V0U3RhdGUoKS5jb25maWc7XHJcbiAgICByZXEuY3VycmVuY3kgPSByZXEuaGVhZGVyc1snYWNjZXB0LWN1cnJlbmN5J10gfHwgJ3VzZCc7XHJcbiAgICByZXEuZGF0ZUZvcm1hdCA9IHJlcS5oZWFkZXJzWydhY2NlcHQtZGF0ZS1mb3JtYXQnXSB8fCAnZGQvbW0veXl5eSc7XHJcbiAgICByZXEubGFuZ3VhZ2UgPSByZXEuY29uZmlnLmFwcExhbmd1YWdlcy5pbmNsdWRlcyhhY2NlcHRMYW5ndWFnZSkgPyBhY2NlcHRMYW5ndWFnZSA6ICdlbic7XHJcbiAgICByZXEudHJhbnNsYXRpb25zID0gKHJlcS5jb25maWcuaW50bERhdGEgJiYgcmVxLmNvbmZpZy5pbnRsRGF0YS5sb2NhbGVzW3JlcS5sYW5ndWFnZV0pIHx8IHt9O1xyXG4gICAgcmVxLnF1ZXJ5ID0gZ2V0UXVlcnlQYXJhbXNKc29uKHJlcS5xdWVyeSk7XHJcblxyXG4gICAgaWYgKHJlcS5jb25maWcuYXBwRGlzYWJsZWQpIHtcclxuICAgICAgcmVzLnNlbmQobWFpbnRlbmFuY2VIdG1sKTtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG5cclxuICAgIG5leHQoKTtcclxuICB9IGNhdGNoIChlKSB7XHJcbiAgICBjb25zdCBlcnJvciA9IGdldEJhY2tlbmRFcnJvcihlKTtcclxuICAgIHJlcy5zdGF0dXMoNTAwKS5qc29uKGVycm9yKTtcclxuICB9XHJcbn0pO1xyXG5cclxuLy8gc2VydmVyIHJvdXRlc1xyXG5hcHBTZXJ2ZXIudXNlKCcvYXBpL2NhdGFsb2dzJywgcm91dGVzQ2F0YWxvZyk7XHJcbmFwcFNlcnZlci51c2UoJy9hcGkvY29uZmlnJywgcm91dGVzQ29uZmlnKTtcclxuYXBwU2VydmVyLnVzZSgnL2FwaS9sb2NhbGVzJywgcm91dGVzTG9jYWxlKTtcclxuYXBwU2VydmVyLnVzZSgnL2FwaS91c2VycycsIHJvdXRlc1VzZXIpO1xyXG5hcHBTZXJ2ZXIudXNlKCcvYXBpL3NvY2lhbExlYWRlcicsIHJvdXRlc1NvY2lhbExlYWRlcik7XHJcbmFwcFNlcnZlci5nZXQoJy8qJywgYXN5bmMgKHJlcSwgcmVzKSA9PiB7XHJcbiAgdHJ5IHtcclxuICAgIGNvbnN0IGN1cnJlbnRSb3V0ZSA9IHJvdXRlc1NTUi5maW5kKHJvdXRlID0+IG1hdGNoUGF0aChyZXEudXJsLCByb3V0ZSkpO1xyXG4gICAgXHJcbiAgICBhd2FpdCBzdG9yZS5kaXNwYXRjaChzZXRQcmVmZXJlbmNlcyh7XHJcbiAgICAgIGN1cnJlbmN5OiByZXEuY3VycmVuY3ksXHJcbiAgICAgIGRhdGVGb3JtYXQ6IHJlcS5kYXRlRm9ybWF0LFxyXG4gICAgICBsYW5ndWFnZTogcmVxLmxhbmd1YWdlXHJcbiAgICB9KSk7XHJcblxyXG4gICAgaWYgKGN1cnJlbnRSb3V0ZSAmJiBjdXJyZW50Um91dGUuY29tcG9uZW50LmdldEluaXRpYWxTdGF0ZSkge1xyXG4gICAgICBhd2FpdCBjdXJyZW50Um91dGUuY29tcG9uZW50LmdldEluaXRpYWxTdGF0ZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0IGluaXRpYWxTdGF0ZSA9IHN0b3JlLmdldFN0YXRlKCk7XHJcbiAgICBjb25zdCBzdHlsZXMgPSBuZXcgU2VydmVyU3R5bGVTaGVldCgpO1xyXG4gICAgY29uc3QgcmVhY3REb20gPSByZW5kZXJUb1N0cmluZyhcclxuICAgICAgPFByb3ZpZGVyIHN0b3JlPXtzdG9yZX0+XHJcbiAgICAgICAgPFN0YXRpY1JvdXRlciBsb2NhdGlvbj17cmVxLnVybH0+XHJcbiAgICAgICAgICA8U3R5bGVTaGVldE1hbmFnZXIgc2hlZXQ9e3N0eWxlcy5pbnN0YW5jZX0+XHJcbiAgICAgICAgICAgIDxNYWluPlxyXG4gICAgICAgICAgICAgIDxTd2l0Y2g+XHJcbiAgICAgICAgICAgICAgICB7cm91dGVzU1NSLm1hcCgocm91dGUsIGluZGV4KSA9PiA8Um91dGUga2V5PXtpbmRleH0gey4uLnJvdXRlfSAvPil9XHJcbiAgICAgICAgICAgICAgPC9Td2l0Y2g+XHJcbiAgICAgICAgICAgIDwvTWFpbj5cclxuICAgICAgICAgIDwvU3R5bGVTaGVldE1hbmFnZXI+XHJcbiAgICAgICAgPC9TdGF0aWNSb3V0ZXI+XHJcbiAgICAgIDwvUHJvdmlkZXI+XHJcbiAgICApO1xyXG5cclxuICAgIGNvbnN0IGhlbG1ldCA9IEhlbG1ldC5yZW5kZXJTdGF0aWMoKTtcclxuICAgIGNvbnN0IHN0eWxlVGFncyA9IHN0eWxlcy5nZXRTdHlsZVRhZ3MoKTtcclxuICAgIGxldCByZXNwb25zZSA9IGluZGV4SHRtbDtcclxuICAgIFxyXG4gICAgcmVzcG9uc2UgPSByZXNwb25zZS5yZXBsYWNlKC97aHRtbEF0dHJpYnV0ZXN9L2csIGhlbG1ldC5odG1sQXR0cmlidXRlcy50b1N0cmluZygpKTtcclxuICAgIHJlc3BvbnNlID0gcmVzcG9uc2UucmVwbGFjZSgve2JvZHlBdHRyaWJ1dGVzfS9nLCBoZWxtZXQuYm9keUF0dHJpYnV0ZXMudG9TdHJpbmcoKSk7XHJcbiAgICByZXNwb25zZSA9IHJlc3BvbnNlLnJlcGxhY2UoLzwhLS10aXRsZS0tPi9nLCBoZWxtZXQudGl0bGUudG9TdHJpbmcoKSk7XHJcbiAgICByZXNwb25zZSA9IHJlc3BvbnNlLnJlcGxhY2UoLzwhLS1tZXRhVGFncy0tPi9nLCBoZWxtZXQubWV0YS50b1N0cmluZygpKTtcclxuICAgIHJlc3BvbnNlID0gcmVzcG9uc2UucmVwbGFjZSgvPCEtLWxpbmtzLS0+L2csIGhlbG1ldC5saW5rLnRvU3RyaW5nKCkpO1xyXG4gICAgcmVzcG9uc2UgPSByZXNwb25zZS5yZXBsYWNlKC88IS0tc3R5bGVzLS0+L2csIHN0eWxlVGFncyk7XHJcbiAgICByZXNwb25zZSA9IHJlc3BvbnNlLnJlcGxhY2UoLzwhLS1jb250ZW50LS0+L2csIHJlYWN0RG9tKTtcclxuICAgIHJlc3BvbnNlID0gcmVzcG9uc2UucmVwbGFjZShcclxuICAgICAgLzwhLS1zY3JpcHRzLS0+L2csXHJcbiAgICAgIGA8c2NyaXB0PndpbmRvdy5pbml0aWFsU3RhdGU9JHtzZXJpYWxpemUoaW5pdGlhbFN0YXRlKX08L3NjcmlwdD5gXHJcbiAgICApO1xyXG4gICAgXHJcbiAgICByZXMuc2VuZChyZXNwb25zZSk7XHJcbiAgfSBjYXRjaCAoZSkge1xyXG4gICAgY29uc3QgZXJyb3IgPSBnZXRCYWNrZW5kRXJyb3IoZSk7XHJcbiAgICByZXMuc3RhdHVzKDUwMCkuanNvbihlcnJvcik7XHJcbiAgfVxyXG59KTtcclxuXHJcbi8vIGV4cG9ydGluZyBzZXJ2aWNlc1xyXG4vLyBleHBvcnQgKiBmcm9tICcuL2FwaXMvY3Jvbm9zJztcclxuZXhwb3J0ICogZnJvbSAnLi9hcGlzL2V2ZW50cyc7XHJcbmV4cG9ydCBjb25zdCBhcHAgPSBmdW5jdGlvbnMuaHR0cHMub25SZXF1ZXN0KGFwcFNlcnZlcik7XHJcbiIsImltcG9ydCBheGlvcyBmcm9tICdheGlvcyc7XHJcbmltcG9ydCB7IGdldFF1ZXJ5UGFyYW1zU3RyaW5nIH0gZnJvbSAnLi4vLi4vdXRpbHMvcmVxdWVzdC11dGlscyc7XHJcbmNvbnN0IHsgQVBJX1VSTCB9ID0gcHJvY2Vzcy5lbnY7XHJcblxyXG5leHBvcnQgY29uc3QgVFlQRVMgPSB7XHJcbiAgR0VUX0NBVEFMT0dTOiAnR0VUX0NBVEFMT0dTJyxcclxuICBHRVRfQ0FUQUxPRzogJ0dFVF9DQVRBTE9HJyxcclxuICBQT1NUX0NBVEFMT0c6ICdQT1NUX0NBVEFMT0cnLFxyXG4gIFBBVENIX0NBVEFMT0c6ICdQQVRDSF9DQVRBTE9HJ1xyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IGdldENhdGFsb2dzID0gcXVlcnkgPT4gYXN5bmMgKGRpc3BhdGNoLCBnZXRTdGF0ZSkgPT4ge1xyXG4gIGNvbnN0IHsgY2F0YWxvZyB9ID0gZ2V0U3RhdGUoKTtcclxuICBjb25zdCByZXNwb25zZSA9IGNhdGFsb2cucmVjb3Jkcy5sZW5ndGggXHJcbiAgICA/IGNhdGFsb2cucmVjb3Jkc1xyXG4gICAgOiAoYXdhaXQgYXhpb3MuZ2V0KGAke0FQSV9VUkx9L2NhdGFsb2dzLyR7Z2V0UXVlcnlQYXJhbXNTdHJpbmcocXVlcnkpfWApKS5kYXRhO1xyXG5cclxuICBkaXNwYXRjaCh7IHR5cGU6IFRZUEVTLkdFVF9DQVRBTE9HUywgcGF5bG9hZDogcmVzcG9uc2UgfSk7XHJcbn07XHJcblxyXG5leHBvcnQgY29uc3QgZ2V0Q2F0YWxvZyA9IGlkID0+IGFzeW5jIGRpc3BhdGNoID0+IHtcclxuICBjb25zdCByZXNwb25zZSA9IGF3YWl0IGF4aW9zLmdldChgJHtBUElfVVJMfS9jYXRhbG9ncy8ke2lkfWApO1xyXG4gIGRpc3BhdGNoKHsgdHlwZTogVFlQRVMuR0VUX0NBVEFMT0csIHBheWxvYWQ6IHJlc3BvbnNlLmRhdGEgfSk7XHJcbn07XHJcblxyXG5leHBvcnQgY29uc3QgY3JlYXRlQ2F0YWxvZyA9IGRhdGEgPT4gYXN5bmMgZGlzcGF0Y2ggPT4ge1xyXG4gIGNvbnN0IHJlc3BvbnNlID0gYXdhaXQgYXhpb3MucG9zdChgJHtBUElfVVJMfS9jYXRhbG9nc2AsIGRhdGEpO1xyXG4gIGRpc3BhdGNoKHsgdHlwZTogVFlQRVMuUE9TVF9DQVRBTE9HLCBwYXlsb2FkOiByZXNwb25zZS5kYXRhIH0pO1xyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IHVwZGF0ZUNhdGFsb2cgPSBkYXRhID0+IGFzeW5jIGRpc3BhdGNoID0+IHtcclxuICBjb25zdCByZXNwb25zZSA9IGF3YWl0IGF4aW9zLnBhdGNoKGAke0FQSV9VUkx9L2NhdGFsb2dzLyR7ZGF0YS5pZH1gLCBkYXRhKTtcclxuICBkaXNwYXRjaCh7IHR5cGU6IFRZUEVTLlBBVENIX0NBVEFMT0csIHBheWxvYWQ6IHJlc3BvbnNlLmRhdGEgfSk7XHJcbn07IiwiaW1wb3J0IGF4aW9zIGZyb20gJ2F4aW9zJztcclxuY29uc3QgeyBBUElfVVJMIH0gPSBwcm9jZXNzLmVudjtcclxuXHJcbmV4cG9ydCBjb25zdCBUWVBFUyA9IHtcclxuICBHRVRfQ1VSUkVOQ1lfQ09OVkVSU0lPTlM6ICdHRVRfQ1VSUkVOQ1lfQ09OVkVSU0lPTlMnLFxyXG4gIFNFVF9DT05GSUdVUkFUSU9OOiAnU0VUX0NPTkZJR1VSQVRJT04nLFxyXG4gIFNFVF9DVVJSRU5DWV9DT05WRVJTSU9OUzogJ1NFVF9DVVJSRU5DWV9DT05WRVJTSU9OUycsXHJcbiAgU0VUX1BSRUZFUkVOQ0VTOiAnU0VUX1BSRUZFUkVOQ0VTJ1xyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IGdldEN1cnJlbmN5Q29udmVyc2lvbnMgPSBkYXRhID0+IGFzeW5jIGRpc3BhdGNoID0+IHtcclxuICBjb25zdCB7IGN1cnJlbmN5LCBkYXRlIH0gPSBkYXRhO1xyXG4gIGNvbnN0IHJlc3BvbnNlID0gYXdhaXQgYXhpb3MuZ2V0KGAke0FQSV9VUkx9L2NvbmZpZy9jdXJyZW5jeS1jb252ZXJzaW9ucy8ke2N1cnJlbmN5fS8ke2RhdGV9YCk7XHJcbiAgZGlzcGF0Y2goeyB0eXBlOiBUWVBFUy5HRVRfQ1VSUkVOQ1lfQ09OVkVSU0lPTlMsIHBheWxvYWQ6IHJlc3BvbnNlLmRhdGEgfSk7XHJcbn07XHJcblxyXG5leHBvcnQgY29uc3Qgc2V0Q29uZmlndXJhdGlvbiA9IGRhdGEgPT4gYXN5bmMgZGlzcGF0Y2ggPT4ge1xyXG4gIGRpc3BhdGNoKHsgdHlwZTogVFlQRVMuU0VUX0NPTkZJR1VSQVRJT04sIHBheWxvYWQ6IGRhdGEgfSk7XHJcbn07XHJcblxyXG5leHBvcnQgY29uc3Qgc2V0Q3VycmVuY3lDb252ZXJzaW9ucyA9IGRhdGEgPT4gYXN5bmMgZGlzcGF0Y2ggPT4ge1xyXG4gIGRpc3BhdGNoKHsgdHlwZTogVFlQRVMuU0VUX0NVUlJFTkNZX0NPTlZFUlNJT05TLCBwYXlsb2FkOiBkYXRhIH0pO1xyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IHNldFByZWZlcmVuY2VzID0gZGF0YSA9PiBhc3luYyAoZGlzcGF0Y2gsIGdldFN0YXRlKSA9PiB7XHJcbiAgY29uc3QgcmVzcG9uc2UgPSBPYmplY3QuYXNzaWduKGdldFN0YXRlKCkuY29uZmlnLnByZWZlcmVuY2VzLCBkYXRhKTtcclxuICBkaXNwYXRjaCh7IHR5cGU6IFRZUEVTLlNFVF9QUkVGRVJFTkNFUywgcGF5bG9hZDogcmVzcG9uc2UgfSk7XHJcbn07XHJcblxyXG5leHBvcnQgY29uc3Qgc3luY0NvbmZpZ3VyYXRpb24gPSBkYXRhID0+IGFzeW5jIGRpc3BhdGNoID0+IHtcclxuICBjb25zdCByZXNwb25zZSA9IGF3YWl0IGF4aW9zLnBvc3QoYCR7QVBJX1VSTH0vY29uZmlnL3N5bmNgLCBkYXRhKTtcclxuICBkaXNwYXRjaCh7IHR5cGU6IFRZUEVTLlNFVF9DT05GSUdVUkFUSU9OLCBwYXlsb2FkOiByZXNwb25zZS5kYXRhIH0pO1xyXG59OyIsImltcG9ydCBheGlvcyBmcm9tICdheGlvcyc7XHJcbmltcG9ydCB7IGdldFF1ZXJ5UGFyYW1zU3RyaW5nIH0gZnJvbSAnLi4vLi4vdXRpbHMvcmVxdWVzdC11dGlscyc7XHJcbmNvbnN0IHsgQVBJX1VSTCB9ID0gcHJvY2Vzcy5lbnY7XHJcblxyXG5leHBvcnQgY29uc3QgVFlQRVMgPSB7XHJcbiAgR0VUX0RFVkVMT1BFUlNfSU5GTzogJ0dFVF9ERVZFTE9QRVJTX0lORk8nLFxyXG4gIEdFVF9GQVFfSU5GTzogJ0dFVF9GQVFfSU5GTycsXHJcbiAgR0VUX1BSSVZBQ1lfSU5GTzogJ0dFVF9QUklWQUNZX0lORk8nLFxyXG4gIEdFVF9QTEFOX0ZFQVRVUkVTOiAnR0VUX1BMQU5fRkVBVFVSRVMnLFxyXG4gIEdFVF9URVJNU19JTkZPOiAnR0VUX1RFUk1TX0lORk8nXHJcbn07XHJcblxyXG5leHBvcnQgY29uc3QgZ2V0RGV2ZWxvcGVyc0luZm8gPSAoKSA9PiBhc3luYyAoZGlzcGF0Y2gsIGdldFN0YXRlKSA9PiB7XHJcbiAgY29uc3QgeyBjb25maWcsIGhvbWUgfSA9IGdldFN0YXRlKCk7XHJcbiAgY29uc3QgcXVlcnkgPSB7IFxyXG4gICAgc29ydDogeyBvcmRlcjogJ2FzYycgfSxcclxuICAgIHdoZXJlOiB7IHBhcmVudDogeyAnPT0nOiBjb25maWcuY2F0YWxvZ3MuZGV2ZWxvcGVyc0RvY3MgfSB9XHJcbiAgfTtcclxuICBjb25zdCByZXNwb25zZSA9IGhvbWUuZGV2ZWxvcGVyc0luZm8ubGVuZ3RoXHJcbiAgICA/IGhvbWUuZGV2ZWxvcGVyc0luZm9cclxuICAgIDogKGF3YWl0IGF4aW9zLmdldChgJHtBUElfVVJMfS9jYXRhbG9ncy8ke2dldFF1ZXJ5UGFyYW1zU3RyaW5nKHF1ZXJ5KX1gKSkuZGF0YTtcclxuXHJcbiAgZGlzcGF0Y2goeyB0eXBlOiBUWVBFUy5HRVRfREVWRUxPUEVSU19JTkZPLCBwYXlsb2FkOiByZXNwb25zZSB9KTtcclxufTtcclxuXHJcbmV4cG9ydCBjb25zdCBnZXRGYXFJbmZvID0gKCkgPT4gYXN5bmMgKGRpc3BhdGNoLCBnZXRTdGF0ZSkgPT4ge1xyXG4gIGNvbnN0IHsgY29uZmlnLCBob21lIH0gPSBnZXRTdGF0ZSgpO1xyXG4gIGNvbnN0IHJlc3BvbnNlID0gT2JqZWN0LmtleXMoaG9tZS5mYXFJbmZvKS5sZW5ndGhcclxuICAgID8gaG9tZS5mYXFJbmZvXHJcbiAgICA6IChhd2FpdCBheGlvcy5nZXQoYCR7QVBJX1VSTH0vY2F0YWxvZ3MvJHtjb25maWcuY2F0YWxvZ3MuZmFxSW5mb31gKSkuZGF0YTtcclxuXHJcbiAgZGlzcGF0Y2goeyB0eXBlOiBUWVBFUy5HRVRfRkFRX0lORk8sIHBheWxvYWQ6IHJlc3BvbnNlIH0pO1xyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IGdldFBsYW5GZWF0dXJlcyA9ICgpID0+IGFzeW5jIChkaXNwYXRjaCwgZ2V0U3RhdGUpID0+IHtcclxuICBjb25zdCB7IGNvbmZpZywgaG9tZSB9ID0gZ2V0U3RhdGUoKTtcclxuICBjb25zdCBxdWVyeSA9IHsgXHJcbiAgICBzb3J0OiB7IG9yZGVyOiAnYXNjJyB9LFxyXG4gICAgd2hlcmU6IHsgcGFyZW50OiB7ICc9PSc6IGNvbmZpZy5jYXRhbG9ncy5wbGFuRmVhdHVyZXMgfSB9XHJcbiAgfTtcclxuICBjb25zdCByZXNwb25zZSA9IGhvbWUucGxhbkZlYXR1cmVzLmxlbmd0aFxyXG4gICAgPyBob21lLnBsYW5GZWF0dXJlc1xyXG4gICAgOiAoYXdhaXQgYXhpb3MuZ2V0KGAke0FQSV9VUkx9L2NhdGFsb2dzLyR7Z2V0UXVlcnlQYXJhbXNTdHJpbmcocXVlcnkpfWApKS5kYXRhO1xyXG5cclxuICBkaXNwYXRjaCh7IHR5cGU6IFRZUEVTLkdFVF9QTEFOX0ZFQVRVUkVTLCBwYXlsb2FkOiByZXNwb25zZSB9KTtcclxufTtcclxuXHJcbmV4cG9ydCBjb25zdCBnZXRQcml2YWN5SW5mbyA9ICgpID0+IGFzeW5jIChkaXNwYXRjaCwgZ2V0U3RhdGUpID0+IHtcclxuICBjb25zdCB7IGNvbmZpZywgaG9tZSB9ID0gZ2V0U3RhdGUoKTtcclxuICBjb25zdCByZXNwb25zZSA9IE9iamVjdC5rZXlzKGhvbWUucHJpdmFjeUluZm8pLmxlbmd0aFxyXG4gICAgPyBob21lLnByaXZhY3lJbmZvXHJcbiAgICA6IChhd2FpdCBheGlvcy5nZXQoYCR7QVBJX1VSTH0vY2F0YWxvZ3MvJHtjb25maWcuY2F0YWxvZ3MucHJpdmFjeUluZm99YCkpLmRhdGE7XHJcblxyXG4gIGRpc3BhdGNoKHsgdHlwZTogVFlQRVMuR0VUX1BSSVZBQ1lfSU5GTywgcGF5bG9hZDogcmVzcG9uc2UgfSk7XHJcbn07XHJcblxyXG5leHBvcnQgY29uc3QgZ2V0VGVybXNJbmZvID0gKCkgPT4gYXN5bmMgKGRpc3BhdGNoLCBnZXRTdGF0ZSkgPT4ge1xyXG4gIGNvbnN0IHsgY29uZmlnLCBob21lIH0gPSBnZXRTdGF0ZSgpO1xyXG4gIGNvbnN0IHJlc3BvbnNlID0gT2JqZWN0LmtleXMoaG9tZS50ZXJtc0luZm8pLmxlbmd0aFxyXG4gICAgPyBob21lLnRlcm1zSW5mb1xyXG4gICAgOiAoYXdhaXQgYXhpb3MuZ2V0KGAke0FQSV9VUkx9L2NhdGFsb2dzLyR7Y29uZmlnLmNhdGFsb2dzLnRlcm1zSW5mb31gKSkuZGF0YTtcclxuXHJcbiAgZGlzcGF0Y2goeyB0eXBlOiBUWVBFUy5HRVRfVEVSTVNfSU5GTywgcGF5bG9hZDogcmVzcG9uc2UgfSk7XHJcbn07XHJcbiIsImltcG9ydCBheGlvcyBmcm9tICdheGlvcyc7XHJcbmltcG9ydCB7IGdldFF1ZXJ5UGFyYW1zU3RyaW5nIH0gZnJvbSAnLi4vLi4vdXRpbHMvcmVxdWVzdC11dGlscyc7XHJcbmNvbnN0IHsgQVBJX1VSTCB9ID0gcHJvY2Vzcy5lbnY7XHJcblxyXG5leHBvcnQgY29uc3QgVFlQRVMgPSB7XHJcbiAgR0VUX0xPQ0FMRVM6ICdHRVRfTE9DQUxFUycsXHJcbiAgR0VUX0xPQ0FMRTogJ0dFVF9MT0NBTEUnLFxyXG4gIFBPU1RfTE9DQUxFOiAnUE9TVF9MT0NBTEUnLFxyXG4gIFBBVENIX0xPQ0FMRTogJ1BBVENIX0xPQ0FMRScsXHJcbiAgREVMRVRFX0xPQ0FMRTogJ0RFTEVURV9MT0NBTEUnXHJcbn07XHJcblxyXG5leHBvcnQgY29uc3QgZ2V0TG9jYWxlcyA9IHF1ZXJ5ID0+IGFzeW5jIChkaXNwYXRjaCwgZ2V0U3RhdGUpID0+IHtcclxuICBjb25zdCB7IGxvY2FsZSB9ID0gZ2V0U3RhdGUoKTtcclxuICBjb25zdCByZXNwb25zZSA9IGxvY2FsZS5yZWNvcmRzLmxlbmd0aCBcclxuICAgID8gbG9jYWxlLnJlY29yZHNcclxuICAgIDogKGF3YWl0IGF4aW9zLmdldChgJHtBUElfVVJMfS9sb2NhbGVzLyR7Z2V0UXVlcnlQYXJhbXNTdHJpbmcocXVlcnkpfWApKS5kYXRhO1xyXG5cclxuICBkaXNwYXRjaCh7IHR5cGU6IFRZUEVTLkdFVF9MT0NBTEVTLCBwYXlsb2FkOiByZXNwb25zZSB9KTtcclxufTtcclxuXHJcbmV4cG9ydCBjb25zdCBnZXRMb2NhbGUgPSBpZCA9PiBhc3luYyBkaXNwYXRjaCA9PiB7XHJcbiAgY29uc3QgcmVzcG9uc2UgPSBhd2FpdCBheGlvcy5nZXQoYCR7QVBJX1VSTH0vbG9jYWxlcy8ke2lkfWApO1xyXG4gIGRpc3BhdGNoKHsgdHlwZTogVFlQRVMuR0VUX0xPQ0FMRSwgcGF5bG9hZDogcmVzcG9uc2UuZGF0YSB9KTtcclxufTtcclxuXHJcbmV4cG9ydCBjb25zdCBjcmVhdGVMb2NhbGUgPSBkYXRhID0+IGFzeW5jIGRpc3BhdGNoID0+IHtcclxuICBjb25zdCByZXNwb25zZSA9IGF3YWl0IGF4aW9zLnBvc3QoYCR7QVBJX1VSTH0vbG9jYWxlc2AsIGRhdGEpO1xyXG4gIGRpc3BhdGNoKHsgdHlwZTogVFlQRVMuUE9TVF9MT0NBTEUsIHBheWxvYWQ6IHJlc3BvbnNlLmRhdGEgfSk7XHJcbn07XHJcblxyXG5leHBvcnQgY29uc3QgdXBkYXRlTG9jYWxlID0gZGF0YSA9PiBhc3luYyBkaXNwYXRjaCA9PiB7XHJcbiAgY29uc3QgcmVzcG9uc2UgPSBhd2FpdCBheGlvcy5wYXRjaChgJHtBUElfVVJMfS9sb2NhbGVzLyR7ZGF0YS5pZH1gLCBkYXRhKTtcclxuICBkaXNwYXRjaCh7IHR5cGU6IFRZUEVTLlBBVENIX0xPQ0FMRSwgcGF5bG9hZDogcmVzcG9uc2UuZGF0YSB9KTtcclxufTtcclxuXHJcbmV4cG9ydCBjb25zdCBkZWxldGVMb2NhbGUgPSBpZCA9PiBhc3luYyBkaXNwYXRjaCA9PiB7XHJcbiAgY29uc3QgcmVzcG9uc2UgPSBhd2FpdCBheGlvcy5kZWxldGUoYCR7QVBJX1VSTH0vbG9jYWxlcy8ke2lkfWApO1xyXG4gIGRpc3BhdGNoKHsgdHlwZTogVFlQRVMuREVMRVRFX0xPQ0FMRSwgcGF5bG9hZDogcmVzcG9uc2UuZGF0YSB9KTtcclxufTtcclxuIiwiaW1wb3J0IGF4aW9zIGZyb20gJ2F4aW9zJztcclxuaW1wb3J0IHsgZ2V0UXVlcnlQYXJhbXNTdHJpbmcgfSBmcm9tICcuLi8uLi91dGlscy9yZXF1ZXN0LXV0aWxzJztcclxuY29uc3QgeyBBUElfVVJMIH0gPSBwcm9jZXNzLmVudjtcclxuXHJcbmV4cG9ydCBjb25zdCBUWVBFUyA9IHtcclxuICBHRVRfQkFDS1VQOiAnR0VUX0JBQ0tVUCcsXHJcbiAgR0VUX1VTRVJTOiAnR0VUX1VTRVJTJyxcclxuICBHRVRfVVNFUjogJ0dFVF9VU0VSJyxcclxuICBHRVRfQ1VTVE9NRVJfSU5GTzonR0VUX0NVU1RPTUVSX0lORk8nLFxyXG4gIEdFVF9DVVNUT01FUl9SRUNFSVBUUzogJ0dFVF9DVVNUT01FUl9SRUNFSVBUUycsXHJcbiAgUEFUQ0hfVVNFUjogJ1BBVENIX1VTRVInLFxyXG4gIENIQU5HRV9QQVNTV09SRDogJ0NIQU5HRV9QQVNTV09SRCcsXHJcbiAgTE9HSU46ICdMT0dJTicsXHJcbiAgTE9HT1VUOiAnTE9HT1VUJyxcclxuICBSRUdJU1RFUjogJ1JFR0lTVEVSJyxcclxuICBNRTogJ01FJyxcclxuICBSRUNPVkVSX1BBU1NXT1JEOiAnUkVDT1ZFUl9QQVNTV09SRCcsXHJcbiAgU0VUX1RPS0VOOiAnU0VUX1RPS0VOJyxcclxuICBHRVRfVVNFUl9DTEFJTVM6ICdHRVRfVVNFUl9DTEFJTVMnLFxyXG4gIFBPU1RfVVNFUl9DTEFJTTogJ1BPU1RfVVNFUl9DTEFJTScsXHJcbiAgREVMRVRFX1VTRVJfQ0xBSU06ICdERUxFVEVfVVNFUl9DTEFJTScsXHJcbiAgVVBEQVRFX0NSRURJVF9DQVJEOiAnVVBEQVRFX0NSRURJVF9DQVJEJyxcclxuICBDUkVBVEVfU1VCU0NSSVBUSU9OOiAnQ1JFQVRFX1NVQlNDUklQVElPTicsXHJcbiAgVVBEQVRFX1NVQlNDUklQVElPTjogJ1VQREFURV9TVUJTQ1JJUFRJT04nLFxyXG4gIENSRUFURV9SRVBPUlQ6ICdDUkVBVEVfUkVQT1JUJ1xyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IGdldEJhY2t1cEV4Y2VsID0geWVhciA9PiBhc3luYyAoZGlzcGF0Y2gsIGdldFN0YXRlKSA9PiB7XHJcbiAgY29uc3QgeyB1c2VyIH0gPSBnZXRTdGF0ZSgpO1xyXG4gIGNvbnN0IHJlc3BvbnNlID0gdXNlci5iYWNrdXBcclxuICAgID8gdXNlci5iYWNrdXBcclxuICAgIDogKGF3YWl0IGF4aW9zLmdldChgJHtBUElfVVJMfS91c2Vycy9iYWNrdXAvJHt5ZWFyfWAsIHsgcmVzcG9uc2VUeXBlOiAnYXJyYXlidWZmZXInfSkpLmRhdGE7XHJcbiAgXHJcbiAgZGlzcGF0Y2goeyB0eXBlOiBUWVBFUy5HRVRfQkFDS1VQLCBwYXlsb2FkOiByZXNwb25zZSB9KTtcclxufTtcclxuXHJcbmV4cG9ydCBjb25zdCBnZXRVc2VycyA9IHF1ZXJ5ID0+IGFzeW5jIChkaXNwYXRjaCwgZ2V0U3RhdGUpID0+IHtcclxuICBjb25zdCB7IHVzZXIgfSA9IGdldFN0YXRlKCk7XHJcbiAgY29uc3QgcmVzcG9uc2UgPSB1c2VyLnJlY29yZHMubGVuZ3RoIFxyXG4gICAgPyB1c2VyLnJlY29yZHNcclxuICAgIDogKGF3YWl0IGF4aW9zLmdldChgJHtBUElfVVJMfS91c2Vycy8ke2dldFF1ZXJ5UGFyYW1zU3RyaW5nKHF1ZXJ5KX1gKSkuZGF0YTtcclxuXHJcbiAgZGlzcGF0Y2goeyB0eXBlOiBUWVBFUy5HRVRfVVNFUlMsIHBheWxvYWQ6IHJlc3BvbnNlIH0pO1xyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IGdldFVzZXIgPSBpZCA9PiBhc3luYyBkaXNwYXRjaCA9PiB7XHJcbiAgY29uc3QgcmVzcG9uc2UgPSBhd2FpdCBheGlvcy5nZXQoYCR7QVBJX1VSTH0vdXNlcnMvJHtpZH1gKTtcclxuICBkaXNwYXRjaCh7IHR5cGU6IFRZUEVTLkdFVF9VU0VSLCBwYXlsb2FkOiByZXNwb25zZS5kYXRhIH0pO1xyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IGdldEN1c3RvbWVySW5mbz0gdXNlcklkID0+IGFzeW5jIGRpc3BhdGNoID0+IHtcclxuICBjb25zdCByZXNwb25zZSA9IChhd2FpdCBheGlvcy5nZXQoYCR7QVBJX1VSTH0vdXNlcnMvJHt1c2VySWR9L2luZm9gKSkuZGF0YTtcclxuICBkaXNwYXRjaCh7IHR5cGU6IFRZUEVTLkdFVF9DVVNUT01FUl9JTkZPLCBwYXlsb2FkOiByZXNwb25zZSB9KTtcclxufTtcclxuXHJcbmV4cG9ydCBjb25zdCBnZXRDdXN0b21lclJlY2VpcHRzID0gKGlkLCBxdWVyeSkgPT4gYXN5bmMgKGRpc3BhdGNoLCBnZXRTdGF0ZSkgPT4ge1xyXG4gIGNvbnN0IHsgdXNlciB9ID0gZ2V0U3RhdGUoKTtcclxuICBjb25zdCByZXNwb25zZSA9IHVzZXIuY3VzdG9tZXJSZWNlaXB0cy5sZW5ndGggPiAwXHJcbiAgICA/IHVzZXIuY3VzdG9tZXJSZWNlaXB0c1xyXG4gICAgOiAoYXdhaXQgYXhpb3MuZ2V0KGAke0FQSV9VUkx9L3VzZXJzLyR7aWR9L3JlY2VpcHRzLyR7Z2V0UXVlcnlQYXJhbXNTdHJpbmcocXVlcnkpfWApKS5kYXRhO1xyXG5cclxuICBkaXNwYXRjaCh7IHR5cGU6IFRZUEVTLkdFVF9DVVNUT01FUl9SRUNFSVBUUywgcGF5bG9hZDogcmVzcG9uc2UgfSk7XHJcbn07XHJcblxyXG5leHBvcnQgY29uc3QgdXBkYXRlVXNlciA9IGRhdGEgPT4gYXN5bmMgZGlzcGF0Y2ggPT4ge1xyXG4gIGNvbnN0IHJlc3BvbnNlID0gYXdhaXQgYXhpb3MucGF0Y2goYCR7QVBJX1VSTH0vdXNlcnMvJHtkYXRhLmlkfWAsIGRhdGEpO1xyXG4gIGRpc3BhdGNoKHsgdHlwZTogVFlQRVMuUEFUQ0hfVVNFUiwgcGF5bG9hZDogcmVzcG9uc2UuZGF0YSB9KTtcclxufTtcclxuXHJcbmV4cG9ydCBjb25zdCBjaGFuZ2VQYXNzd29yZCA9IGRhdGEgPT4gYXN5bmMgZGlzcGF0Y2ggPT4ge1xyXG4gIGNvbnN0IHJlc3BvbnNlID0gYXdhaXQgYXhpb3MucG9zdChgJHtBUElfVVJMfS91c2Vycy9jaGFuZ2UtcGFzc3dvcmRgLCBkYXRhKTtcclxuICBkaXNwYXRjaCh7IHR5cGU6IFRZUEVTLkNIQU5HRV9QQVNTV09SRCwgcGF5bG9hZDogcmVzcG9uc2UuZGF0YS5tZXNzc2FnZSB9KTtcclxufTtcclxuXHJcbmV4cG9ydCBjb25zdCBsb2dpbiA9IGRhdGEgPT4gYXN5bmMgZGlzcGF0Y2ggPT4ge1xyXG4gIGNvbnN0IHJlc3BvbnNlID0gYXdhaXQgYXhpb3MucG9zdChgJHtBUElfVVJMfS91c2Vycy9sb2dpbmAsIGRhdGEpO1xyXG4gIGRpc3BhdGNoKHsgdHlwZTogVFlQRVMuTE9HSU4sIHBheWxvYWQ6IHJlc3BvbnNlLmRhdGEgfSk7XHJcbn07XHJcblxyXG5leHBvcnQgY29uc3QgbG9nb3V0ID0gKCkgPT4gYXN5bmMgZGlzcGF0Y2ggPT4ge1xyXG4gIGRpc3BhdGNoKHsgdHlwZTogVFlQRVMuTE9HT1VULCBwYXlsb2FkOiBudWxsIH0pO1xyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IHJlZ2lzdGVyID0gZGF0YSA9PiBhc3luYyBkaXNwYXRjaCA9PiB7XHJcbiAgY29uc3QgcmVzcG9uc2UgPSBhd2FpdCBheGlvcy5wb3N0KGAke0FQSV9VUkx9L3VzZXJzL3JlZ2lzdGVyYCwgZGF0YSk7XHJcbiAgZGlzcGF0Y2goeyB0eXBlOiBUWVBFUy5SRUdJU1RFUiwgcGF5bG9hZDogcmVzcG9uc2UuZGF0YSB9KTtcclxufTtcclxuXHJcbmV4cG9ydCBjb25zdCBtZSA9ICgpID0+IGFzeW5jIGRpc3BhdGNoID0+IHtcclxuICB0cnkge1xyXG4gICAgbGV0IHJlc3BvbnNlID0gYXdhaXQgYXhpb3MuZ2V0KGAke0FQSV9VUkx9L3VzZXJzL21lYCk7XHJcbiAgICBkaXNwYXRjaCh7IHR5cGU6IFRZUEVTLk1FLCBwYXlsb2FkOiByZXNwb25zZS5kYXRhIH0pO1xyXG4gIH0gY2F0Y2ggKGUpIHtcclxuICAgIGRpc3BhdGNoKHsgdHlwZTogVFlQRVMuTUUsIHBheWxvYWQ6IG51bGwgfSk7XHJcbiAgfVxyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IHJlY292ZXJQYXNzd29yZCA9IGRhdGEgPT4gYXN5bmMgZGlzcGF0Y2ggPT4ge1xyXG4gIGNvbnN0IHJlc3BvbnNlID0gYXdhaXQgYXhpb3MucG9zdChgJHtBUElfVVJMfS91c2Vycy9yZWNvdmVyLXBhc3N3b3JkYCwgZGF0YSk7XHJcbiAgZGlzcGF0Y2goeyB0eXBlOiBUWVBFUy5SRUNPVkVSX1BBU1NXT1JELCBwYXlsb2FkOiByZXNwb25zZS5kYXRhIH0pO1xyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IHNldFRva2VuID0gdG9rZW4gPT4gYXN5bmMgZGlzcGF0Y2ggPT4ge1xyXG4gIGxvY2FsU3RvcmFnZS50b2tlbiA9IHRva2VuO1xyXG4gIGRpc3BhdGNoKHsgdHlwZTogVFlQRVMuU0VUX1RPS0VOLCBwYXlsb2FkOiB0b2tlbiB9KTtcclxufTtcclxuXHJcbmV4cG9ydCBjb25zdCBnZXRVc2VyQ2xhaW1zID0gaWQgPT4gYXN5bmMgZGlzcGF0Y2ggPT4ge1xyXG4gIGNvbnN0IHJlc3BvbnNlID0gYXdhaXQgYXhpb3MuZ2V0KGAke0FQSV9VUkx9L3VzZXJzLyR7aWR9L2NsYWltc2ApO1xyXG4gIGRpc3BhdGNoKHsgdHlwZTogVFlQRVMuR0VUX1VTRVJfQ0xBSU1TLCBwYXlsb2FkOiByZXNwb25zZS5kYXRhIH0pO1xyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IGNyZWF0ZVVzZXJDbGFpbSA9IGRhdGEgPT4gYXN5bmMgZGlzcGF0Y2ggPT4ge1xyXG4gIGNvbnN0IHJlc3BvbnNlID0gYXdhaXQgYXhpb3MucG9zdChgJHtBUElfVVJMfS91c2Vycy8ke2RhdGEuaWR9L2NsYWltc2AsIGRhdGEpO1xyXG4gIGRpc3BhdGNoKHsgdHlwZTogVFlQRVMuUE9TVF9VU0VSX0NMQUlNLCBwYXlsb2FkOiByZXNwb25zZS5kYXRhIH0pO1xyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IGRlbGV0ZVVzZXJDbGFpbSA9IGRhdGEgPT4gYXN5bmMgZGlzcGF0Y2ggPT4ge1xyXG4gIGNvbnN0IHJlc3BvbnNlID0gYXdhaXQgYXhpb3MucGF0Y2goYCR7QVBJX1VSTH0vdXNlcnMvJHtkYXRhLmlkfS9jbGFpbXNgLCBkYXRhKTtcclxuICBkaXNwYXRjaCh7IHR5cGU6IFRZUEVTLkRFTEVURV9VU0VSX0NMQUlNLCBwYXlsb2FkOiByZXNwb25zZS5kYXRhIH0pO1xyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IHVwZGF0ZUNyZWRpdENhcmQgPSAoY3VzdG9tZXJDb2RlLCBjcmVkaXRDYXJkKSA9PiBhc3luYyBkaXNwYXRjaCA9PiB7XHJcbiAgY29uc3QgcmVzcG9uc2UgPSBhd2FpdCBheGlvcy5wb3N0KGAke0FQSV9VUkx9L3VzZXJzLyR7Y3VzdG9tZXJDb2RlfS91cGRhdGUtY3JlZGl0LWNhcmRgLCBjcmVkaXRDYXJkKTtcclxuICBkaXNwYXRjaCh7IHR5cGU6IFRZUEVTLlVQREFURV9DUkVESVRfQ0FSRCwgcGF5bG9hZDogcmVzcG9uc2UuZGF0YSB9KTtcclxufTtcclxuXHJcbmV4cG9ydCBjb25zdCBjcmVhdGVTdWJzY3JpcHRpb24gPSBkYXRhID0+IGFzeW5jIGRpc3BhdGNoID0+IHtcclxuICBsZXQgcmVzcG9uc2UgPSBhd2FpdCBheGlvcy5wb3N0KGAke0FQSV9VUkx9L3VzZXJzL2NyZWF0ZS1zdWJzY3JpcHRpb25gLCBkYXRhKTtcclxuICBkaXNwYXRjaCh7IHR5cGU6IFRZUEVTLkNSRUFURV9TVUJTQ1JJUFRJT04sIHBheWxvYWQ6IHJlc3BvbnNlLmRhdGEgfSk7XHJcbn07XHJcblxyXG5leHBvcnQgY29uc3QgdXBkYXRlU3Vic2NyaXB0aW9uID0gKGN1c3RvbWVyQ29kZSwgZGF0YSkgPT4gYXN5bmMgZGlzcGF0Y2ggPT4ge1xyXG4gIGNvbnN0IHJlc3BvbnNlID0gYXdhaXQgYXhpb3MucG9zdChgJHtBUElfVVJMfS91c2Vycy8ke2N1c3RvbWVyQ29kZX0vdXBkYXRlLXN1YnNjcmlwdGlvbmAsZGF0YSk7XHJcbiAgZGlzcGF0Y2goeyB0eXBlOiBUWVBFUy5VUERBVEVfQ1JFRElUX0NBUkQsIHBheWxvYWQ6IHJlc3BvbnNlLmRhdGEgfSk7XHJcbn07XHJcblxyXG5leHBvcnQgY29uc3QgY3JlYXRlUmVwb3J0ID0gZGF0YSA9PiBhc3luYyBkaXNwYXRjaCA9PiB7XHJcbiAgbGV0IHJlc3BvbnNlID0gYXdhaXQgYXhpb3MucG9zdChgJHtBUElfVVJMfS9zb2NpYWxMZWFkZXIvcmVwb3J0YCwgZGF0YSk7XHJcbiAgZGlzcGF0Y2goeyB0eXBlOiBUWVBFUy5DUkVBVEVfUkVQT1JULCBwYXlsb2FkOiByZXNwb25zZS5kYXRhIH0pO1xyXG59OyIsImltcG9ydCBheGlvcyBmcm9tICdheGlvcyc7XHJcblxyXG5jb25zdCByZXF1ZXN0ID0gc3RvcmUgPT4gbmV4dCA9PiBhY3Rpb24gPT4ge1xyXG4gIGNvbnN0IHsgY29uZmlnLCB1c2VyIH0gPSBzdG9yZS5nZXRTdGF0ZSgpO1xyXG4gIGNvbnN0IHsgcHJlZmVyZW5jZXMgfSA9IGNvbmZpZztcclxuICBkZWxldGUgYXhpb3MuZGVmYXVsdHMuaGVhZGVycy5jb21tb24uYXV0aG9yaXphdGlvbjtcclxuXHJcbiAgaWYgKHVzZXIudG9rZW4pIHtcclxuICAgIGF4aW9zLmRlZmF1bHRzLmhlYWRlcnMuY29tbW9uWydhdXRob3JpemF0aW9uJ10gPSBgQmVhcmVyICR7dXNlci50b2tlbn1gO1xyXG4gIH1cclxuXHJcbiAgaWYgKHByZWZlcmVuY2VzLmN1cnJlbmN5KSB7XHJcbiAgICBheGlvcy5kZWZhdWx0cy5oZWFkZXJzLmNvbW1vblsnYWNjZXB0LWN1cnJlbmN5J10gPSBwcmVmZXJlbmNlcy5jdXJyZW5jeTtcclxuICB9XHJcblxyXG4gIGlmIChwcmVmZXJlbmNlcy5sYW5ndWFnZSkge1xyXG4gICAgYXhpb3MuZGVmYXVsdHMuaGVhZGVycy5jb21tb25bJ2FjY2VwdC1sYW5ndWFnZSddID0gcHJlZmVyZW5jZXMubGFuZ3VhZ2U7XHJcbiAgfVxyXG5cclxuICByZXR1cm4gbmV4dChhY3Rpb24pO1xyXG59O1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgcmVxdWVzdDtcclxuIiwiaW1wb3J0IHRodW5rTWlkZGxld2FyZSBmcm9tICdyZWR1eC10aHVuayc7XHJcblxyXG5leHBvcnQgZGVmYXVsdCB0aHVua01pZGRsZXdhcmU7IFxyXG4iLCJpbXBvcnQgeyBUWVBFUyB9IGZyb20gJy4uL2FjdGlvbnMvY2F0YWxvZy1hY3Rpb25zJztcclxuXHJcbmNvbnN0IGluaXRpYWxTdGF0ZSA9IHtcclxuICByZWNvcmRzOiBbXSxcclxuICB0ZW1wOiBudWxsXHJcbn07XHJcblxyXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiByZWR1Y2VyKHN0YXRlID0gaW5pdGlhbFN0YXRlLCBhY3Rpb24pIHtcclxuICBzd2l0Y2ggKGFjdGlvbi50eXBlKSB7XHJcbiAgICBkZWZhdWx0OlxyXG4gICAgICByZXR1cm4gc3RhdGU7XHJcblxyXG4gICAgY2FzZSBUWVBFUy5HRVRfQ0FUQUxPR1M6XHJcbiAgICAgIHJldHVybiB7XHJcbiAgICAgICAgLi4uc3RhdGUsXHJcbiAgICAgICAgcmVjb3JkczogYWN0aW9uLnBheWxvYWRcclxuICAgICAgfTtcclxuXHJcbiAgICBjYXNlIFRZUEVTLkdFVF9DQVRBTE9HOlxyXG4gICAgICByZXR1cm4ge1xyXG4gICAgICAgIC4uLnN0YXRlLFxyXG4gICAgICAgIHRlbXA6IGFjdGlvbi5wYXlsb2FkXHJcbiAgICAgIH07XHJcblxyXG4gICAgY2FzZSBUWVBFUy5QT1NUX0NBVEFMT0c6XHJcbiAgICAgIHJldHVybiB7XHJcbiAgICAgICAgLi4uc3RhdGUsXHJcbiAgICAgICAgcmVjb3Jkczogc3RhdGUucmVjb3Jkcy5jb25jYXQoYWN0aW9uLnBheWxvYWQpLFxyXG4gICAgICAgIHRlbXA6IGFjdGlvbi5wYXlsb2FkXHJcbiAgICAgIH07XHJcblxyXG4gICAgY2FzZSBUWVBFUy5QQVRDSF9DQVRBTE9HOlxyXG4gICAgICByZXR1cm4ge1xyXG4gICAgICAgIC4uLnN0YXRlLFxyXG4gICAgICAgIHJlY29yZHM6IHN0YXRlLnJlY29yZHMubWFwKGl0ZW0gPT4gKGl0ZW0uaWQgPT09IGFjdGlvbi5wYXlsb2FkLmlkID8gYWN0aW9uLnBheWxvYWQgOiBpdGVtKSksXHJcbiAgICAgICAgdGVtcDogYWN0aW9uLnBheWxvYWRcclxuICAgICAgfTtcclxuICB9XHJcbn0iLCJpbXBvcnQgeyBUWVBFUyB9IGZyb20gJy4uL2FjdGlvbnMvY29uZmlnLWFjdGlvbnMnO1xyXG5cclxuY29uc3QgZ2V0VHJhbnNsYXRpb25zID0gc3RhdGUgPT4ge1xyXG4gIHJldHVybiBzdGF0ZS5pbnRsRGF0YS5sb2NhbGVzW3N0YXRlLnByZWZlcmVuY2VzLmxhbmd1YWdlXSB8fCB7fTtcclxufTtcclxuXHJcbmNvbnN0IHN0b3JhZ2VCdWNrZXQgPSAnaHR0cHM6Ly9maXJlYmFzZXN0b3JhZ2UuZ29vZ2xlYXBpcy5jb20vdjAvYi9saWRlcmVzLXNvY2lhbGVzLWJkMTUyLmFwcHNwb3QuY29tL28nO1xyXG5cclxuY29uc3QgaW5pdGlhbFN0YXRlID0ge1xyXG4gIGFwcERpc2FibGVkOiBmYWxzZSxcclxuICBhcHBEaXNhYmxlZE1lc3NhZ2U6ICcnLFxyXG4gIGFwcExhbmd1YWdlczogWydlbicsICdlcyddLFxyXG4gIGFwcExhc3RTeW5jOiBudWxsLFxyXG4gIGFwcExvZ286IGAke3N0b3JhZ2VCdWNrZXR9L3BhbG9taXRhLnBuZz9hbHQ9bWVkaWEmdG9rZW49MjFhZDYxZTgtOGU2OS00ZGM3LTkzMDMtYjkyYjMyYTNhOGQ0YCxcclxuICBhcHBOYW1lOiAnTGlkZXJlcyBTb2NpYWxlcycsXHJcbiAgYXBwVXJsOiAnaHR0cHM6Ly9yZWFjdC5jb20nLFxyXG4gIGNhdGFsb2dzOiB7XHJcbiAgICBwbGFuRmVhdHVyZXM6ICdPbGZtMldqclc0MERaTUFGZExyWicsXHJcbiAgICBkZXZlbG9wZXJzRG9jczogJ3Z6VmEyOVdUcUwxY2Zod1dibzZDJyxcclxuICAgIGZhcUluZm86ICdOYXE5R1FCWFlIRlk3eFhmSDA2bCcsXHJcbiAgICBwcml2YWN5SW5mbzogJ1I1VW9sYTZrSVY5OXRkNTdtSWdGJyxcclxuICAgIHRlcm1zSW5mbzogJ1puMEU2enMyQjlUbnk3eTRUMUJPJ1xyXG4gIH0sXHJcbiAgaW50bERhdGE6IHtcclxuICAgIGNhbGxpbmdDb2RlczogW10sXHJcbiAgICBjb3VudHJpZXM6IFtdLFxyXG4gICAgY3VycmVuY2llczogW10sXHJcbiAgICBjdXJyZW5jeUNvbnZlcnNpb25zOiB7fSxcclxuICAgIGxhbmd1YWdlczogW10sXHJcbiAgICBsb2NhbGVzOiB7fVxyXG4gIH0sXHJcbiAgZW1haWxzOiB7XHJcbiAgICBub3JlcGx5OiAnbm9yZXBseUBtYWlsLmNvbScsXHJcbiAgICBzdXBwb3J0OiAnc2FtcGxlQG1haWwuY29tJ1xyXG4gIH0sXHJcbiAgcGxhbnM6IHtcclxuICAgIGZyZWU6ICd0Q2ZyV2hGU1k4YVB2NGJpeDZwVicsXHJcbiAgICBzdGFuZGFyZDogJ25aSkYyUnlyTjlHWHNpV0Q3aU9iJyxcclxuICAgIHByZW1pdW06ICd2S1RISmhGcVhHcDRLa0JhQlBXRydcclxuICB9LFxyXG4gIHByZWZlcmVuY2VzOiB7XHJcbiAgICBjdXJyZW5jeTogJ3VzZCcsXHJcbiAgICBkYXRlRm9ybWF0OiAnREQvTU0vWVlZWScsXHJcbiAgICBsYW5ndWFnZTogJ2VuJ1xyXG4gIH0sXHJcbiAgdHJhbnNsYXRpb25zOiB7fVxyXG59O1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gcmVkdWNlcihzdGF0ZSA9IGluaXRpYWxTdGF0ZSwgYWN0aW9uKSB7XHJcbiAgc3dpdGNoIChhY3Rpb24udHlwZSkge1xyXG4gICAgZGVmYXVsdDpcclxuICAgICAgcmV0dXJuIHN0YXRlO1xyXG5cclxuICAgIGNhc2UgVFlQRVMuR0VUX0NVUlJFTkNZX0NPTlZFUlNJT05TOlxyXG4gICAgICByZXR1cm4ge1xyXG4gICAgICAgIC4uLnN0YXRlLFxyXG4gICAgICAgIHRlbXA6IGFjdGlvbi5wYXlsb2FkXHJcbiAgICAgIH07XHJcblxyXG4gICAgY2FzZSBUWVBFUy5TRVRfQ09ORklHVVJBVElPTjoge1xyXG4gICAgICBjb25zdCBuZXdTdGF0ZSA9IE9iamVjdC5hc3NpZ24oc3RhdGUsIGFjdGlvbi5wYXlsb2FkKTtcclxuICAgICAgcmV0dXJuIHtcclxuICAgICAgICAuLi5uZXdTdGF0ZSxcclxuICAgICAgICB0cmFuc2xhdGlvbnM6IGdldFRyYW5zbGF0aW9ucyhuZXdTdGF0ZSlcclxuICAgICAgfTtcclxuICAgIH1cclxuXHJcbiAgICBjYXNlIFRZUEVTLlNFVF9DVVJSRU5DWV9DT05WRVJTSU9OUzpcclxuICAgICAgcmV0dXJuIHtcclxuICAgICAgICAuLi5zdGF0ZSxcclxuICAgICAgICBpbnRsRGF0YToge1xyXG4gICAgICAgICAgLi4uc3RhdGUuaW50bERhdGEsXHJcbiAgICAgICAgICBjdXJyZW5jeUNvbnZlcnNpb25zOiBhY3Rpb24ucGF5bG9hZFxyXG4gICAgICAgIH1cclxuICAgICAgfTtcclxuXHJcbiAgICBjYXNlIFRZUEVTLlNFVF9QUkVGRVJFTkNFUzpcclxuICAgICAgcmV0dXJuIHtcclxuICAgICAgICAuLi5zdGF0ZSxcclxuICAgICAgICBwcmVmZXJlbmNlczogYWN0aW9uLnBheWxvYWQsXHJcbiAgICAgICAgdHJhbnNsYXRpb25zOiBnZXRUcmFuc2xhdGlvbnMoc3RhdGUpXHJcbiAgICAgIH07XHJcblxyXG4gIH1cclxufVxyXG4iLCJpbXBvcnQgeyBUWVBFUyB9IGZyb20gJy4uL2FjdGlvbnMvaG9tZS1hY3Rpb25zJztcclxuIFxyXG5jb25zdCBpbml0aWFsU3RhdGUgPSB7XHJcbiAgZGV2ZWxvcGVyc0luZm86IFtdLFxyXG4gIGZhcUluZm86IHt9LFxyXG4gIHBsYW5GZWF0dXJlczogW10sXHJcbiAgcHJpdmFjeUluZm86IHt9LFxyXG4gIHRlcm1zSW5mbzoge31cclxufTtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIHJlZHVjZXIoc3RhdGUgPSBpbml0aWFsU3RhdGUsIGFjdGlvbikge1xyXG4gIHN3aXRjaCAoYWN0aW9uLnR5cGUpIHtcclxuICAgIGRlZmF1bHQ6XHJcbiAgICAgIHJldHVybiBzdGF0ZTtcclxuXHJcbiAgICBjYXNlIFRZUEVTLkdFVF9ERVZFTE9QRVJTX0lORk86XHJcbiAgICAgIHJldHVybiB7XHJcbiAgICAgICAgLi4uc3RhdGUsXHJcbiAgICAgICAgZGV2ZWxvcGVyc0luZm86IGFjdGlvbi5wYXlsb2FkXHJcbiAgICAgIH07XHJcblxyXG4gICAgY2FzZSBUWVBFUy5HRVRfRkFRX0lORk86XHJcbiAgICAgIHJldHVybiB7XHJcbiAgICAgICAgLi4uc3RhdGUsXHJcbiAgICAgICAgZmFxSW5mbzogYWN0aW9uLnBheWxvYWRcclxuICAgICAgfTtcclxuXHJcbiAgICBjYXNlIFRZUEVTLkdFVF9QTEFOX0ZFQVRVUkVTOlxyXG4gICAgICByZXR1cm4ge1xyXG4gICAgICAgIC4uLnN0YXRlLFxyXG4gICAgICAgIHBsYW5GZWF0dXJlczogYWN0aW9uLnBheWxvYWRcclxuICAgICAgfTtcclxuXHJcbiAgICBjYXNlIFRZUEVTLkdFVF9QUklWQUNZX0lORk86XHJcbiAgICAgIHJldHVybiB7XHJcbiAgICAgICAgLi4uc3RhdGUsXHJcbiAgICAgICAgcHJpdmFjeUluZm86IGFjdGlvbi5wYXlsb2FkXHJcbiAgICAgIH07XHJcblxyXG4gICAgY2FzZSBUWVBFUy5HRVRfVEVSTVNfSU5GTzpcclxuICAgICAgcmV0dXJuIHtcclxuICAgICAgICAuLi5zdGF0ZSxcclxuICAgICAgICB0ZXJtc0luZm86IGFjdGlvbi5wYXlsb2FkXHJcbiAgICAgIH07XHJcbiAgfVxyXG59XHJcbiIsImltcG9ydCB7IFRZUEVTIH0gZnJvbSAnLi4vYWN0aW9ucy9sb2NhbGUtYWN0aW9ucyc7IFxyXG5cclxuY29uc3QgaW5pdGlhbFN0YXRlID0ge1xyXG4gIHJlY29yZHM6IFtdLFxyXG4gIHRlbXA6IG51bGxcclxufTtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIHJlZHVjZXIoc3RhdGUgPSBpbml0aWFsU3RhdGUsIGFjdGlvbikge1xyXG4gIHN3aXRjaCAoYWN0aW9uLnR5cGUpIHtcclxuICAgIGRlZmF1bHQ6XHJcbiAgICAgIHJldHVybiBzdGF0ZTtcclxuXHJcbiAgICBjYXNlIFRZUEVTLkdFVF9MT0NBTEVTOlxyXG4gICAgICByZXR1cm4ge1xyXG4gICAgICAgIC4uLnN0YXRlLFxyXG4gICAgICAgIHJlY29yZHM6IGFjdGlvbi5wYXlsb2FkXHJcbiAgICAgIH07XHJcblxyXG4gICAgY2FzZSBUWVBFUy5HRVRfTE9DQUxFOlxyXG4gICAgICByZXR1cm4ge1xyXG4gICAgICAgIC4uLnN0YXRlLFxyXG4gICAgICAgIHRlbXA6IGFjdGlvbi5wYXlsb2FkXHJcbiAgICAgIH07XHJcblxyXG4gICAgY2FzZSBUWVBFUy5QT1NUX0xPQ0FMRTpcclxuICAgICAgcmV0dXJuIHtcclxuICAgICAgICAuLi5zdGF0ZSxcclxuICAgICAgICByZWNvcmRzOiBzdGF0ZS5yZWNvcmRzLmNvbmNhdChhY3Rpb24ucGF5bG9hZCksXHJcbiAgICAgICAgdGVtcDogYWN0aW9uLnBheWxvYWRcclxuICAgICAgfTtcclxuXHJcbiAgICBjYXNlIFRZUEVTLlBBVENIX0xPQ0FMRTpcclxuICAgICAgcmV0dXJuIHtcclxuICAgICAgICAuLi5zdGF0ZSxcclxuICAgICAgICByZWNvcmRzOiBzdGF0ZS5yZWNvcmRzLm1hcChpdGVtID0+IChpdGVtLmlkID09PSBhY3Rpb24ucGF5bG9hZC5pZCA/IGFjdGlvbi5wYXlsb2FkIDogaXRlbSkpLFxyXG4gICAgICAgIHRlbXA6IGFjdGlvbi5wYXlsb2FkXHJcbiAgICAgIH07XHJcblxyXG4gICAgY2FzZSBUWVBFUy5ERUxFVEVfTE9DQUxFOlxyXG4gICAgICByZXR1cm4ge1xyXG4gICAgICAgIC4uLnN0YXRlLFxyXG4gICAgICAgIHJlY29yZHM6IHN0YXRlLnJlY29yZHMuZmlsdGVyKGl0ZW0gPT4gaXRlbS5pZCAhPT0gYWN0aW9uLnBheWxvYWQuaWQpXHJcbiAgICAgIH07XHJcbiAgICAgIFxyXG4gIH1cclxufVxyXG4iLCJpbXBvcnQgeyBUWVBFUyB9IGZyb20gJy4uL2FjdGlvbnMvdXNlci1hY3Rpb25zJztcclxuXHJcbmNvbnN0IGluaXRpYWxTdGF0ZSA9IHtcclxuICBhdXRoOiBudWxsLFxyXG4gIGF1dGhMb2FkZWQ6IGZhbHNlLFxyXG4gIGJhY2t1cDogbnVsbCxcclxuICBjbGFpbXM6IFtdLFxyXG4gIGN1c3RvbWVySW5mbzogbnVsbCxcclxuICBjdXN0b21lclJlY2VpcHRzOiBbXSxcclxuICByZWNvcmRzOiBbXSxcclxuICB0ZW1wOiBudWxsLFxyXG4gIHRva2VuOiBudWxsXHJcbn07XHJcblxyXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiByZWR1Y2VyKHN0YXRlID0gaW5pdGlhbFN0YXRlLCBhY3Rpb24pIHtcclxuICBzd2l0Y2ggKGFjdGlvbi50eXBlKSB7XHJcbiAgICBkZWZhdWx0OlxyXG4gICAgICByZXR1cm4gc3RhdGU7XHJcblxyXG4gICAgY2FzZSBUWVBFUy5HRVRfQkFDS1VQOlxyXG4gICAgICByZXR1cm4ge1xyXG4gICAgICAgIC4uLnN0YXRlLFxyXG4gICAgICAgIGJhY2t1cDogYWN0aW9uLnBheWxvYWRcclxuICAgICAgfTtcclxuXHJcbiAgICBjYXNlIFRZUEVTLkdFVF9VU0VSUzpcclxuICAgICAgcmV0dXJuIHtcclxuICAgICAgICAuLi5zdGF0ZSxcclxuICAgICAgICByZWNvcmRzOiBhY3Rpb24ucGF5bG9hZFxyXG4gICAgICB9O1xyXG5cclxuICAgIGNhc2UgVFlQRVMuR0VUX1VTRVI6XHJcbiAgICAgIHJldHVybiB7XHJcbiAgICAgICAgLi4uc3RhdGUsXHJcbiAgICAgICAgdGVtcDogYWN0aW9uLnBheWxvYWRcclxuICAgICAgfTtcclxuICAgIFxyXG4gICAgY2FzZSBUWVBFUy5HRVRfQ1VTVE9NRVJfSU5GTzpcclxuICAgICAgcmV0dXJuIHtcclxuICAgICAgICAuLi5zdGF0ZSxcclxuICAgICAgICBjdXN0b21lckluZm86IGFjdGlvbi5wYXlsb2FkXHJcbiAgICAgIH07XHJcblxyXG4gICAgY2FzZSBUWVBFUy5HRVRfQ1VTVE9NRVJfUkVDRUlQVFM6XHJcbiAgICAgIHJldHVybiB7XHJcbiAgICAgICAgLi4uc3RhdGUsXHJcbiAgICAgICAgY3VzdG9tZXJSZWNlaXB0czogYWN0aW9uLnBheWxvYWRcclxuICAgICAgfTtcclxuXHJcbiAgICBjYXNlIFRZUEVTLlBBVENIX1VTRVI6XHJcbiAgICAgIHJldHVybiB7XHJcbiAgICAgICAgLi4uc3RhdGUsXHJcbiAgICAgICAgcmVjb3Jkczogc3RhdGUucmVjb3Jkcy5tYXAoaXRlbSA9PiAoaXRlbS5pZCA9PT0gYWN0aW9uLnBheWxvYWQuaWQgPyBhY3Rpb24ucGF5bG9hZCA6IGl0ZW0pKSxcclxuICAgICAgICB0ZW1wOiBhY3Rpb24ucGF5bG9hZFxyXG4gICAgICB9O1xyXG5cclxuICAgIGNhc2UgVFlQRVMuQ0hBTkdFX1BBU1NXT1JEOlxyXG4gICAgICByZXR1cm4ge1xyXG4gICAgICAgIC4uLnN0YXRlLFxyXG4gICAgICAgIHRlbXA6IGFjdGlvbi5wYXlsb2FkXHJcbiAgICAgIH07XHJcblxyXG4gICAgY2FzZSBUWVBFUy5MT0dJTjpcclxuICAgICAgcmV0dXJuIHtcclxuICAgICAgICAuLi5zdGF0ZSxcclxuICAgICAgICB0ZW1wOiBhY3Rpb24ucGF5bG9hZC5tZXNzc2FnZSxcclxuICAgICAgICB0b2tlbjogYWN0aW9uLnBheWxvYWQudG9rZW5cclxuICAgICAgfTtcclxuXHJcbiAgICBjYXNlIFRZUEVTLkxPR09VVDpcclxuICAgICAgcmV0dXJuIHtcclxuICAgICAgICAuLi5zdGF0ZSxcclxuICAgICAgICBhdXRoOiBudWxsLFxyXG4gICAgICAgIHRva2VuOiBudWxsXHJcbiAgICAgIH07XHJcblxyXG4gICAgY2FzZSBUWVBFUy5SRUdJU1RFUjpcclxuICAgICAgcmV0dXJuIHtcclxuICAgICAgICAuLi5zdGF0ZSxcclxuICAgICAgICB0ZW1wOiBhY3Rpb24ucGF5bG9hZC5tZXNzc2FnZSxcclxuICAgICAgICB0b2tlbjogYWN0aW9uLnBheWxvYWQudG9rZW5cclxuICAgICAgfTtcclxuXHJcbiAgICBjYXNlIFRZUEVTLk1FOlxyXG4gICAgICByZXR1cm4ge1xyXG4gICAgICAgIC4uLnN0YXRlLFxyXG4gICAgICAgIGF1dGg6IGFjdGlvbi5wYXlsb2FkLFxyXG4gICAgICAgIGF1dGhMb2FkZWQ6IHRydWVcclxuICAgICAgfTtcclxuXHJcbiAgICBjYXNlIFRZUEVTLlJFQ09WRVJfUEFTU1dPUkQ6XHJcbiAgICAgIHJldHVybiB7XHJcbiAgICAgICAgLi4uc3RhdGUsXHJcbiAgICAgICAgdGVtcDogYWN0aW9uLnBheWxvYWRcclxuICAgICAgfTtcclxuXHJcbiAgICBjYXNlIFRZUEVTLlNFVF9UT0tFTjpcclxuICAgICAgcmV0dXJuIHtcclxuICAgICAgICAuLi5zdGF0ZSxcclxuICAgICAgICB0b2tlbjogYWN0aW9uLnBheWxvYWRcclxuICAgICAgfTtcclxuXHJcbiAgICBjYXNlIFRZUEVTLkdFVF9VU0VSX0NMQUlNUzpcclxuICAgICAgcmV0dXJuIHtcclxuICAgICAgICAuLi5zdGF0ZSxcclxuICAgICAgICBjbGFpbXM6IGFjdGlvbi5wYXlsb2FkXHJcbiAgICAgIH07XHJcblxyXG4gICAgY2FzZSBUWVBFUy5QT1NUX1VTRVJfQ0xBSU06XHJcbiAgICAgIHJldHVybiB7XHJcbiAgICAgICAgLi4uc3RhdGUsXHJcbiAgICAgICAgY2xhaW1zOiBzdGF0ZS5jbGFpbXMuY29uY2F0KGFjdGlvbi5wYXlsb2FkKSxcclxuICAgICAgICB0ZW1wOiBhY3Rpb24ucGF5bG9hZFxyXG4gICAgICB9O1xyXG5cclxuICAgIGNhc2UgVFlQRVMuREVMRVRFX1VTRVJfQ0xBSU06XHJcbiAgICAgIHJldHVybiB7XHJcbiAgICAgICAgLi4uc3RhdGUsXHJcbiAgICAgICAgY2xhaW1zOiBzdGF0ZS5jbGFpbXMuZmlsdGVyKGl0ZW0gPT4gaXRlbSAhPT0gYWN0aW9uLnBheWxvYWQpLFxyXG4gICAgICAgIHRlbXA6IGFjdGlvbi5wYXlsb2FkXHJcbiAgICAgIH07XHJcblxyXG4gICAgY2FzZSBUWVBFUy5VUERBVEVfQ1JFRElUX0NBUkQ6XHJcbiAgICAgIHJldHVybiB7XHJcbiAgICAgICAgLi4uc3RhdGUsXHJcbiAgICAgICAgdGVtcDogYWN0aW9uLnBheWxvYWRcclxuICAgICAgfTtcclxuXHJcbiAgICBjYXNlIFRZUEVTLkNSRUFURV9TVUJTQ1JJUFRJT046XHJcbiAgICAgIHJldHVybiB7XHJcbiAgICAgICAgLi4uc3RhdGUsXHJcbiAgICAgICAgdG9rZW46IGFjdGlvbi5wYXlsb2FkLnRva2VuLFxyXG4gICAgICAgIHRlbXA6IGFjdGlvbi5wYXlsb2FkLm1lc3NhZ2VcclxuICAgICAgfTtcclxuICAgIFxyXG4gICAgY2FzZSBUWVBFUy5DUkVBVEVfUkVQT1JUOlxyXG4gICAgICByZXR1cm4ge1xyXG4gICAgICAgIC4uLnN0YXRlLFxyXG4gICAgICAgIC8vIHRva2VuOiBhY3Rpb24ucGF5bG9hZC50b2tlbixcclxuICAgICAgICB0ZW1wOiBhY3Rpb24ucGF5bG9hZC5tZXNzYWdlXHJcbiAgICAgIH07XHJcbiAgfVxyXG59XHJcbiIsImltcG9ydCB7IGFwcGx5TWlkZGxld2FyZSwgY3JlYXRlU3RvcmUsIGNvbWJpbmVSZWR1Y2VycyB9IGZyb20gJ3JlZHV4JztcclxuLy8gbWlkZGxld2FyZXNcclxuaW1wb3J0IHsgY29tcG9zZVdpdGhEZXZUb29scyB9IGZyb20gJ3JlZHV4LWRldnRvb2xzLWV4dGVuc2lvbi9kZXZlbG9wbWVudE9ubHknO1xyXG5pbXBvcnQgcmVxdWVzdE1pZGRsZXdhcmUgZnJvbSAnLi9taWRkbGV3YXJlcy9yZXF1ZXN0LW1pZGRsZXdhcmUnO1xyXG5pbXBvcnQgdGh1bmtNaWRkbGV3YXJlIGZyb20gJy4vbWlkZGxld2FyZXMvdGh1bmstbWlkZGxld2FyZSc7XHJcbi8vIHJlZHVjZXJzXHJcbmltcG9ydCBjYXRhbG9nUmVkdWNlciBmcm9tICcuL3JlZHVjZXJzL2NhdGFsb2ctcmVkdWNlcic7XHJcbmltcG9ydCBjb25maWdSZWR1Y2VyIGZyb20gJy4vcmVkdWNlcnMvY29uZmlnLXJlZHVjZXInO1xyXG5pbXBvcnQgaG9tZVJlZHVjZXIgZnJvbSAnLi9yZWR1Y2Vycy9ob21lLXJlZHVjZXInO1xyXG5pbXBvcnQgbG9jYWxlUmVkdWNlciBmcm9tICcuL3JlZHVjZXJzL2xvY2FsZS1yZWR1Y2VyJztcclxuaW1wb3J0IHVzZXJSZWR1Y2VyIGZyb20gJy4vcmVkdWNlcnMvdXNlci1yZWR1Y2VyJztcclxubGV0IGluaXRpYWxTdGF0ZSA9IHt9O1xyXG5cclxuaWYgKHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnKSB7XHJcbiAgaW5pdGlhbFN0YXRlID0gd2luZG93LmluaXRpYWxTdGF0ZSB8fCBpbml0aWFsU3RhdGU7XHJcbn1cclxuXHJcbmNvbnN0IHJlZHVjZXJzID0gY29tYmluZVJlZHVjZXJzKHtcclxuICBjYXRhbG9nOiBjYXRhbG9nUmVkdWNlcixcclxuICBjb25maWc6IGNvbmZpZ1JlZHVjZXIsXHJcbiAgaG9tZTogaG9tZVJlZHVjZXIsXHJcbiAgbG9jYWxlOiBsb2NhbGVSZWR1Y2VyLFxyXG4gIHVzZXI6IHVzZXJSZWR1Y2VyXHJcbn0pO1xyXG5cclxuY29uc3QgbWlkZGxld2FyZSA9IGNvbXBvc2VXaXRoRGV2VG9vbHMoYXBwbHlNaWRkbGV3YXJlKHJlcXVlc3RNaWRkbGV3YXJlLCB0aHVua01pZGRsZXdhcmUpKTtcclxuY29uc3Qgc3RvcmUgPSBjcmVhdGVTdG9yZShyZWR1Y2VycywgaW5pdGlhbFN0YXRlLCBtaWRkbGV3YXJlKTtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IHN0b3JlO1xyXG4iLCJleHBvcnQgY29uc3QgZ2V0RGJEb2N1bWVudCA9IGFzeW5jIChkYiwgY29sbGVjdGlvbiwgZG9jdW1lbnRJZCkgPT4ge1xyXG4gIGxldCByZXNwb25zZSA9IG51bGw7XHJcblxyXG4gIGlmICghZG9jdW1lbnRJZCkge1xyXG4gICAgcmV0dXJuIG51bGw7XHJcbiAgfVxyXG5cclxuICByZXNwb25zZSA9IGRiLmNvbGxlY3Rpb24oY29sbGVjdGlvbik7XHJcbiAgcmVzcG9uc2UgPSBhd2FpdCByZXNwb25zZS5kb2MoZG9jdW1lbnRJZCkuZ2V0KCk7XHJcbiAgcmVzcG9uc2UgPSByZXNwb25zZS5kYXRhKCk7XHJcblxyXG4gIGlmIChyZXNwb25zZSkge1xyXG4gICAgcmVzcG9uc2UgPSB7IC4uLnJlc3BvbnNlLCBpZDogZG9jdW1lbnRJZCB9O1xyXG4gIH1cclxuXHJcbiAgcmV0dXJuIHJlc3BvbnNlO1xyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IGdldERiUXVlcnkgPSBhc3luYyAoZGIsIGNvbGxlY3Rpb24sIHF1ZXJ5KSA9PiB7XHJcbiAgY29uc3QgcGFyc2VkUXVlcnkgPSBnZXREYlBhcnNlZFF1ZXJ5KHF1ZXJ5KTtcclxuICBsZXQgZGJRdWVyeSA9IGRiLmNvbGxlY3Rpb24oY29sbGVjdGlvbik7XHJcbiAgbGV0IHJlc3BvbnNlID0gW107XHJcblxyXG4gIGlmIChwYXJzZWRRdWVyeS53aGVyZSAmJiBwYXJzZWRRdWVyeS53aGVyZS5sZW5ndGgpIHtcclxuICAgIHBhcnNlZFF1ZXJ5LndoZXJlLmZvckVhY2goaXRlbSA9PiAoZGJRdWVyeSA9IGRiUXVlcnkud2hlcmUoaXRlbVswXSwgaXRlbVsxXSwgaXRlbVsyXSkpKTtcclxuICB9XHJcbiAgaWYgKHBhcnNlZFF1ZXJ5LnNvcnQgJiYgcGFyc2VkUXVlcnkuc29ydC5sZW5ndGgpIHtcclxuICAgIHBhcnNlZFF1ZXJ5LnNvcnQuZm9yRWFjaChpdGVtID0+IChkYlF1ZXJ5ID0gZGJRdWVyeS5vcmRlckJ5KGl0ZW1bMF0sIGl0ZW1bMV0gfHwgJ2FzYycpKSk7XHJcbiAgfVxyXG4gIGlmIChwYXJzZWRRdWVyeS5saW1pdCkge1xyXG4gICAgZGJRdWVyeSA9IGRiUXVlcnkubGltaXQocGFyc2VkUXVlcnkubGltaXQpO1xyXG4gIH1cclxuXHJcbiAgcmVzcG9uc2UgPSBhd2FpdCBkYlF1ZXJ5LmdldCgpO1xyXG4gIHJlc3BvbnNlID0gcmVzcG9uc2UuZG9jcy5tYXAoZG9jID0+ICh7IC4uLmRvYy5kYXRhKCksIGlkOiBkb2MuaWQgfSkpO1xyXG5cclxuICBpZiAocGFyc2VkUXVlcnkuc2VsZWN0KSB7XHJcbiAgICByZXNwb25zZSA9IHJlc3BvbnNlLm1hcChpdGVtID0+IHtcclxuICAgICAgcmV0dXJuIE9iamVjdC5rZXlzKGl0ZW0pLnJlZHVjZSgoYWNjdW0sIGtleSkgPT4ge1xyXG4gICAgICAgIHJldHVybiBwYXJzZWRRdWVyeS5zZWxlY3QuaW5jbHVkZXMoa2V5KSA/IE9iamVjdC5hc3NpZ24oYWNjdW0sIHsgW2tleV06IGl0ZW1ba2V5XSB9KSA6IGFjY3VtO1xyXG4gICAgICB9LCB7fSk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIHJldHVybiByZXNwb25zZTtcclxufTtcclxuXHJcbmV4cG9ydCBjb25zdCBnZXREYlF1ZXJ5QnlJZHMgPSBhc3luYyAoZGIsIGNvbGxlY3Rpb24sIGlkcykgPT4ge1xyXG4gIGxldCByZXNwb25zZSA9IFtdO1xyXG4gIGxldCBkb2N1bWVudFJlZmVyZW5jZXMgPSBpZHMuZmlsdGVyKGlkID0+IGlkKS5tYXAoaWQgPT4gZGIuY29sbGVjdGlvbihjb2xsZWN0aW9uKS5kb2MoaWQpLmdldCgpKTtcclxuXHJcbiAgcmVzcG9uc2UgPSBhd2FpdCBQcm9taXNlLmFsbChkb2N1bWVudFJlZmVyZW5jZXMpO1xyXG4gIHJlc3BvbnNlID0gcmVzcG9uc2UuZmlsdGVyKGRvYyA9PiAhZG9jLmlzRW1wdHkpLm1hcChkb2MgPT4gKHsgLi4uZG9jLmRhdGEoKSwgaWQ6IGRvYy5pZCB9KSk7XHJcbiAgXHJcbiAgcmV0dXJuIHJlc3BvbnNlO1xyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IGdldERiUGFyc2VkUXVlcnkgPSBkYXRhID0+IHtcclxuICBsZXQgcmVzcG9uc2UgPSB7fTtcclxuICBsZXQgc2VhcmNoQ3JpdGVyaWEgPSB7fTtcclxuXHJcbiAgZm9yIChjb25zdCBrZXkgaW4gZGF0YSkge1xyXG4gICAgc2VhcmNoQ3JpdGVyaWEgPSBkYXRhW2tleV07XHJcblxyXG4gICAgaWYgKGtleSA9PT0gJ2xpbWl0Jykge1xyXG4gICAgICByZXNwb25zZS5saW1pdCA9IHNlYXJjaENyaXRlcmlhO1xyXG4gICAgfVxyXG4gICAgaWYgKGtleSA9PT0gJ3NlbGVjdCcpIHtcclxuICAgICAgcmVzcG9uc2Uuc2VsZWN0ID0gc2VhcmNoQ3JpdGVyaWE7XHJcbiAgICB9XHJcbiAgICBpZiAoa2V5ID09PSAnc29ydCcpIHtcclxuICAgICAgcmVzcG9uc2Uuc29ydCA9IFtdO1xyXG5cclxuICAgICAgZm9yIChjb25zdCBrZXkgaW4gc2VhcmNoQ3JpdGVyaWEpIHtcclxuICAgICAgICByZXNwb25zZS5zb3J0LnB1c2goW2tleSwgc2VhcmNoQ3JpdGVyaWFba2V5XV0pO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICBpZiAoa2V5ID09PSAnd2hlcmUnKSB7XHJcbiAgICAgIHJlc3BvbnNlLndoZXJlID0gW107XHJcblxyXG4gICAgICBmb3IgKGNvbnN0IGtleSBpbiBzZWFyY2hDcml0ZXJpYSkge1xyXG4gICAgICAgIGZvciAoY29uc3QgY3JpdGVyaWEgaW4gc2VhcmNoQ3JpdGVyaWFba2V5XSkge1xyXG4gICAgICAgICAgaWYgKGNyaXRlcmlhID09PSAnaW4nKSB7XHJcbiAgICAgICAgICAgIHJlc3BvbnNlLndoZXJlLnB1c2goW2tleSwgJ2FycmF5LWNvbnRhaW5zJywgc2VhcmNoQ3JpdGVyaWFba2V5XVtjcml0ZXJpYV1dKTtcclxuICAgICAgICAgIH0gZWxzZSBpZiAoY3JpdGVyaWEgPT09ICdsaWtlJykge1xyXG4gICAgICAgICAgICByZXNwb25zZS53aGVyZS5wdXNoKFtrZXksICc+PScsIHNlYXJjaENyaXRlcmlhW2tleV1bY3JpdGVyaWFdXSk7XHJcbiAgICAgICAgICB9IGVsc2UgaWYgKGNyaXRlcmlhID09PSAncmFuZ2UnKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHJhbmdlID0gc2VhcmNoQ3JpdGVyaWFba2V5XVtjcml0ZXJpYV07XHJcbiAgICAgICAgICAgIHJlc3BvbnNlLndoZXJlLnB1c2goW2tleSwgJz49JywgcmFuZ2Uuc3RhcnRdKTtcclxuICAgICAgICAgICAgcmVzcG9uc2Uud2hlcmUucHVzaChba2V5LCAnPD0nLCByYW5nZS5lbmRdKTtcclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJlc3BvbnNlLndoZXJlLnB1c2goW2tleSwgY3JpdGVyaWEsIHNlYXJjaENyaXRlcmlhW2tleV1bY3JpdGVyaWFdXSk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICByZXR1cm4gcmVzcG9uc2U7XHJcbn07XHJcbiIsImltcG9ydCB7IGdldERiUXVlcnkgfSBmcm9tICcuL2RhdGFiYXNlLXV0aWxzJztcclxuaW1wb3J0IHsgZm9ybWF0VGVtcGxhdGUgfSBmcm9tICcuL3RleHQtdXRpbHMnO1xyXG5cclxuY29uc3QgQ0FDSEUgPSB7XHJcbiAgSU5UTF9EQVRBOiAnQ0FDSEVfSU5UTF9EQVRBJyxcclxuICBDVVJSRU5DWV9DT05WRVJTSU9OUzogJ0NBQ0hFX0NVUlJFTkNZX0NPTlZFUlNJT05TJ1xyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IGZvcm1hdERhdGUgPSAoZGF0ZXRpbWUsIGZvcm1hdCkgPT4ge1xyXG4gIGNvbnN0IGRhdGUgPSBuZXcgRGF0ZShkYXRldGltZSk7XHJcbiAgY29uc3QgZGF5ID0gZGF0ZS5nZXREYXRlKCk7XHJcbiAgY29uc3QgbW9udGggPSBkYXRlLmdldE1vbnRoKCkgKyAxO1xyXG4gIGNvbnN0IHllYXIgPSBkYXRlLmdldEZ1bGxZZWFyKCk7XHJcbiAgY29uc3QgcmVzcG9uc2UgPSBmb3JtYXRcclxuICAgIC50b0xvd2VyQ2FzZSgpXHJcbiAgICAucmVwbGFjZShuZXcgUmVnRXhwKCdkZCcsICdnJyksIGRheS50b1N0cmluZygpLnBhZFN0YXJ0KDIsICcwJykpXHJcbiAgICAucmVwbGFjZShuZXcgUmVnRXhwKCdtbScsICdnJyksIG1vbnRoLnRvU3RyaW5nKCkucGFkU3RhcnQoMiwgJzAnKSlcclxuICAgIC5yZXBsYWNlKG5ldyBSZWdFeHAoJ3l5eXknLCAnZycpLCB5ZWFyKTtcclxuXHJcbiAgcmV0dXJuIHJlc3BvbnNlO1xyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IGZvcm1hdExvY2FsZXMgPSAobGFuZ3VhZ2VzLCBsb2NhbGVzKSA9PiB7XHJcbiAgbGV0IHJlc3BvbnNlID0ge307XHJcblxyXG4gIHJlc3BvbnNlID0gbGFuZ3VhZ2VzLnJlZHVjZSgoYWNjdW0sIGxhbmcpID0+IHtcclxuICAgIGxvY2FsZXMuZm9yRWFjaChsb2NhbGUgPT4ge1xyXG4gICAgICBhY2N1bVtsYW5nXSA9IGFjY3VtW2xhbmddIHx8IHt9O1xyXG4gICAgICBhY2N1bVtsYW5nXVtsb2NhbGUubmFtZV0gPSBsb2NhbGUudmFsdWVbbGFuZ107XHJcbiAgICB9KTtcclxuXHJcbiAgICByZXR1cm4gYWNjdW07XHJcbiAgfSwge30pO1xyXG5cclxuICByZXR1cm4gcmVzcG9uc2U7XHJcbn07XHJcblxyXG5leHBvcnQgY29uc3QgZm9ybWF0TW9uZXkgPSAodmFsdWUsIHRob3VzYW5kcyA9ICcsJykgPT4ge1xyXG4gIGNvbnN0IG5lZ2F0aXZlU2lnbiA9IHZhbHVlIDwgMCA/ICctJyA6ICcnO1xyXG4gIGxldCBpID0gTWF0aC5yb3VuZCh2YWx1ZSkudG9TdHJpbmcoKTtcclxuICBsZXQgaiA9IGkubGVuZ3RoID4gMyA/IGkubGVuZ3RoICUgMyA6IDA7XHJcblxyXG4gIHJldHVybiAoXHJcbiAgICBuZWdhdGl2ZVNpZ24gK1xyXG4gICAgKGogPyBpLnN1YnN0cigwLCBqKSArIHRob3VzYW5kcyA6ICcnKSArXHJcbiAgICBpLnN1YnN0cihqKS5yZXBsYWNlKC8oXFxkezN9KSg/PVxcZCkvZywgJyQxJyArIHRob3VzYW5kcylcclxuICApO1xyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IGdldEludGxEYXRhID0gYXN5bmMgKGNvbmZpZywgYXhpb3MsIGNhY2hlLCBkYiwgaW50bEFQSSkgPT4ge1xyXG4gIGxldCByZXNwb25zZSA9IGNhY2hlLmdldChDQUNIRS5JTlRMX0RBVEEpO1xyXG5cclxuICBpZiAoIXJlc3BvbnNlKSB7XHJcbiAgICBsZXQgc291cmNlSW50bCA9IGF3YWl0IGF4aW9zLmdldChcclxuICAgICAgYCR7aW50bEFQSS51cmx9L2FsbD9maWVsZHM9YWxwaGEzQ29kZTtjYWxsaW5nQ29kZXM7Y3VycmVuY2llcztmbGFnO2xhbmd1YWdlcztuYW1lO3RyYW5zbGF0aW9uc2BcclxuICAgICk7XHJcbiAgICBzb3VyY2VJbnRsID0gc291cmNlSW50bC5kYXRhO1xyXG4gICAgcmVzcG9uc2UgPSB7fTtcclxuICAgIFxyXG4gICAgLy8gY2FsbGluZyBjb2Rlc1xyXG4gICAgcmVzcG9uc2UuY2FsbGluZ0NvZGVzID0gc291cmNlSW50bFxyXG4gICAgICAucmVkdWNlKChhY2N1bSwgaXRlbSkgPT4ge1xyXG4gICAgICAgIGFjY3VtID0gYWNjdW0uY29uY2F0KGl0ZW0uY2FsbGluZ0NvZGVzXHJcbiAgICAgICAgICAuZmlsdGVyKGNhbGxpbmdDb2RlID0+IGNhbGxpbmdDb2RlKVxyXG4gICAgICAgICAgLm1hcChjYWxsaW5nQ29kZSA9PiAoeyBsYWJlbDogYCske2NhbGxpbmdDb2RlfWAsIHZhbHVlOiBgKyR7Y2FsbGluZ0NvZGV9YCB9KSlcclxuICAgICAgICApO1xyXG5cclxuICAgICAgICByZXR1cm4gYWNjdW07XHJcbiAgICAgIH0sIFtdKVxyXG4gICAgICAuZmlsdGVyKChpdGVtLCBpbmRleCwgYXJyYXkpID0+IGFycmF5LmZpbmRJbmRleChvYmogPT4gb2JqLnZhbHVlID09PSBpdGVtLnZhbHVlKSA9PT0gaW5kZXgpXHJcbiAgICAgIC5zb3J0KChhLCBiKSA9PiBhLmxhYmVsLmxvY2FsZUNvbXBhcmUoYi5sYWJlbCkpO1xyXG4gICAgXHJcbiAgICAvLyBjb3VudHJpZXMgXHJcbiAgICByZXNwb25zZS5jb3VudHJpZXMgPSBzb3VyY2VJbnRsXHJcbiAgICAgIC5yZWR1Y2UoKGFjY3VtLCBpdGVtKSA9PiB7XHJcbiAgICAgICAgYWNjdW0gPSBhY2N1bS5jb25jYXQoeyBsYWJlbDogaXRlbS5uYW1lLCB2YWx1ZTogaXRlbS5hbHBoYTNDb2RlLnRvTG93ZXJDYXNlKCkgfSk7XHJcblxyXG4gICAgICAgIHJldHVybiBhY2N1bTtcclxuICAgICAgfSwgW10pXHJcbiAgICAgIC5zb3J0KChhLCBiKSA9PiBhLmxhYmVsLmxvY2FsZUNvbXBhcmUoYi5sYWJlbCkpO1xyXG5cclxuICAgIC8vIGN1cnJlbmNpZXNcclxuICAgIHJlc3BvbnNlLmN1cnJlbmNpZXMgPSBzb3VyY2VJbnRsXHJcbiAgICAgIC5yZWR1Y2UoKGFjY3VtLCBpdGVtKSA9PiB7XHJcbiAgICAgICAgYWNjdW0gPSBhY2N1bS5jb25jYXQoaXRlbS5jdXJyZW5jaWVzXHJcbiAgICAgICAgICAuZmlsdGVyKGN1cnJlbmN5SW5mbyA9PiBjdXJyZW5jeUluZm8uY29kZSAmJiBjdXJyZW5jeUluZm8uY29kZSAhPT0gJyhub25lKScpXHJcbiAgICAgICAgICAubWFwKGN1cnJlbmN5SW5mbyA9PiAoeyBsYWJlbDogY3VycmVuY3lJbmZvLmNvZGUudG9VcHBlckNhc2UoKSwgdmFsdWU6IGN1cnJlbmN5SW5mby5jb2RlLnRvTG93ZXJDYXNlKCkgfSkpXHJcbiAgICAgICAgKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGFjY3VtO1xyXG4gICAgICB9LCBbXSlcclxuICAgICAgLmZpbHRlcigoaXRlbSwgaW5kZXgsIGFycmF5KSA9PiBhcnJheS5maW5kSW5kZXgob2JqID0+IG9iai52YWx1ZSA9PT0gaXRlbS52YWx1ZSkgPT09IGluZGV4KVxyXG4gICAgICAuc29ydCgoYSwgYikgPT4gYS5sYWJlbC5sb2NhbGVDb21wYXJlKGIubGFiZWwpKTtcclxuICAgIFxyXG4gICAgLy8gbGFuZ3VhZ2VzXHJcbiAgICByZXNwb25zZS5sYW5ndWFnZXMgPSBzb3VyY2VJbnRsXHJcbiAgICAgIC5yZWR1Y2UoKGFjY3VtLCBpdGVtKSA9PiB7XHJcbiAgICAgICAgYWNjdW0gPSBhY2N1bS5jb25jYXQoaXRlbS5sYW5ndWFnZXNcclxuICAgICAgICAgIC5maWx0ZXIobGFuZ3VhZ2VJbmZvID0+IGxhbmd1YWdlSW5mby5pc282MzlfMSlcclxuICAgICAgICAgIC5tYXAobGFuZ3VhZ2VJbmZvID0+ICh7IGxhYmVsOiBsYW5ndWFnZUluZm8ubmFtZSwgdmFsdWU6IGxhbmd1YWdlSW5mby5pc282MzlfMSB9KSlcclxuICAgICAgICApO1xyXG5cclxuICAgICAgICByZXR1cm4gYWNjdW07XHJcbiAgICAgIH0sIFtdKVxyXG4gICAgICAuZmlsdGVyKChpdGVtLCBpbmRleCwgYXJyYXkpID0+IGFycmF5LmZpbmRJbmRleChvYmogPT4gb2JqLnZhbHVlID09PSBpdGVtLnZhbHVlKSA9PT0gaW5kZXgpXHJcbiAgICAgIC5zb3J0KChhLCBiKSA9PiBhLmxhYmVsLmxvY2FsZUNvbXBhcmUoYi5sYWJlbCkpO1xyXG5cclxuICAgIC8vIGxvY2FsZXNcclxuICAgIGNvbnN0IGxvY2FsZXMgPSBhd2FpdCBnZXREYlF1ZXJ5KGRiLCAnbG9jYWxlcycsIHt9KTtcclxuICAgIGNvbnN0IGZvcm1hdHRlZExvY2FsZXMgPSBmb3JtYXRMb2NhbGVzKGNvbmZpZy5hcHBMYW5ndWFnZXMsIGxvY2FsZXMpO1xyXG4gICAgcmVzcG9uc2UubG9jYWxlcyA9IGZvcm1hdHRlZExvY2FsZXM7XHJcblxyXG4gICAgY2FjaGUucHV0KENBQ0hFLklOVExfREFUQSwgcmVzcG9uc2UpO1xyXG4gIH1cclxuXHJcbiAgcmV0dXJuIHJlc3BvbnNlO1xyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IHRyYW5zbGF0ZSA9ICh0cmFuc2xhdGlvbnMgPSB7fSwgbG9jYWxlID0gJycsIHBhcmFtcyA9IHt9KSA9PiB7XHJcbiAgY29uc3QgbG9jYWxlVmFsdWUgPSB0cmFuc2xhdGlvbnNbbG9jYWxlXSB8fCAnJztcclxuICBjb25zdCByZXNwb25zZSA9IGZvcm1hdFRlbXBsYXRlKGxvY2FsZVZhbHVlLCBwYXJhbXMpIHx8IGxvY2FsZTtcclxuXHJcbiAgcmV0dXJuIHJlc3BvbnNlO1xyXG59O1xyXG4iLCJpbXBvcnQgeyB0cmFuc2xhdGUgfSBmcm9tICcuL2ludGwtdXRpbHMnO1xyXG5cclxuZXhwb3J0IGNvbnN0IHNlbmROb3RpZmljYXRpb24gPSBhc3luYyAoY29uZmlnLCBlbWFpbExpYnJhcnksIGVtYWlsRGF0YSkgPT4ge1xyXG4gIHRyeSB7XHJcbiAgICBjb25zdCB7IHNlbmRncmlkOiBzZW5kZ3JpZENvbmZpZyB9ID0gY29uZmlnO1xyXG4gICAgZW1haWxMaWJyYXJ5LnNldEFwaUtleShzZW5kZ3JpZENvbmZpZy5rZXkpO1xyXG5cclxuICAgIGF3YWl0IGVtYWlsTGlicmFyeS5zZW5kKHtcclxuICAgICAgZnJvbTogJ25vcmVwbHlAbWFpbC5jb20nLFxyXG4gICAgICB0bzogZW1haWxEYXRhLnRvLFxyXG4gICAgICB0ZW1wbGF0ZUlkOiBzZW5kZ3JpZENvbmZpZy50ZW1wbGF0ZWdlbmVyYWwsXHJcbiAgICAgICdkeW5hbWljX3RlbXBsYXRlX2RhdGEnOiB7XHJcbiAgICAgICAgc2xvZ2FuOiB0cmFuc2xhdGUoY29uZmlnLnRyYW5zbGF0aW9ucywgJ3Nsb2dhbicpLFxyXG4gICAgICAgIHN1YmplY3Q6IGVtYWlsRGF0YS5zdWJqZWN0LFxyXG4gICAgICAgIG1lc3NhZ2U6IGVtYWlsRGF0YS5tZXNzYWdlXHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH0gY2F0Y2ggKGVycm9yKSB7XHJcbiAgICBjb25zb2xlLndhcm4oJ0Vycm9yIHNlbmRpbmcgc3Vic2NyaXB0aW9uIG5vdGlmaWNhdGlvbicsIGVtYWlsRGF0YSwgZXJyb3IpO1xyXG4gIH1cclxufTsiLCJleHBvcnQgY29uc3QgZ2V0Q2F0YWxvZ01vZGVsID0gKGFzc29jaWF0aW9ucywgYXBwTGFuZ3VhZ2VzKSA9PiB7XHJcbiAgcmV0dXJuIFtcclxuICAgIHtcclxuICAgICAgcHJvcGVydHk6ICdhY3RpdmUnLFxyXG4gICAgICB0eXBlOiAnYm9vbGVhbicsXHJcbiAgICAgIHJ1bGVzOiBbeyByZXF1aXJlZDogdHJ1ZSB9XVxyXG4gICAgfSxcclxuICAgIHtcclxuICAgICAgcHJvcGVydHk6ICdjcmVhdGVkQXQnLFxyXG4gICAgICB0eXBlOiAnbnVtYmVyJyxcclxuICAgICAgcnVsZXM6IFt7IHJlcXVpcmVkOiB0cnVlIH1dXHJcbiAgICB9LFxyXG4gICAge1xyXG4gICAgICBwcm9wZXJ0eTogJ25hbWUnLFxyXG4gICAgICB0eXBlOiAnc3RyaW5nJyxcclxuICAgICAgcnVsZXM6IFt7IHJlcXVpcmVkOiB0cnVlIH1dXHJcbiAgICB9LFxyXG4gICAge1xyXG4gICAgICBwcm9wZXJ0eTogJ29yZGVyJyxcclxuICAgICAgdHlwZTogJ251bWJlcicsXHJcbiAgICAgIHJ1bGVzOiBbeyByZXF1aXJlZDogdHJ1ZSB9XVxyXG4gICAgfSxcclxuICAgIHtcclxuICAgICAgcHJvcGVydHk6ICdwYXJlbnQnLFxyXG4gICAgICB0eXBlOiAnc3RyaW5nJyxcclxuICAgICAgcnVsZXM6IFt7IGFzc29jaWF0aW9uOiBhc3NvY2lhdGlvbnMucGFyZW50cyB9XVxyXG4gICAgfSxcclxuICAgIHtcclxuICAgICAgcHJvcGVydHk6ICd0aHVtYm5haWwnLFxyXG4gICAgICB0eXBlOiAnc3RyaW5nJyxcclxuICAgICAgcnVsZXM6IFt7IGZvcm1hdDogJ3VybCcgfV1cclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgIHByb3BlcnR5OiAndXBkYXRlZEF0JyxcclxuICAgICAgdHlwZTogJ251bWJlcidcclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgIHByb3BlcnR5OiAndmFsdWUnLFxyXG4gICAgICB0eXBlOiAnb2JqZWN0JyxcclxuICAgICAgcnVsZXM6IFt7IHJlcXVpcmVkOiB0cnVlIH1dLFxyXG4gICAgICB2YWxpZGF0aW9uczogYXBwTGFuZ3VhZ2VzLm1hcChrZXkgPT4ge1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICBwcm9wZXJ0eToga2V5LFxyXG4gICAgICAgICAgdHlwZTogJ3N0cmluZydcclxuICAgICAgICB9O1xyXG4gICAgICB9KVxyXG4gICAgfVxyXG4gIF07XHJcbn07XHJcblxyXG5leHBvcnQgY29uc3QgZ2V0Q29uZmlndXJhdGlvbk1vZGVsID0gKCkgPT4ge1xyXG4gIHJldHVybiBbXHJcbiAgICB7XHJcbiAgICAgIHByb3BlcnR5OiAnYXBwRGlzYWJsZWQnLFxyXG4gICAgICB0eXBlOiAnYm9vbGVhbicsXHJcbiAgICAgIHJ1bGVzOiBbeyByZXF1aXJlZDogdHJ1ZSB9XVxyXG4gICAgfSxcclxuICAgIHtcclxuICAgICAgcHJvcGVydHk6ICdhcHBEaXNhYmxlZE1lc3NhZ2UnLFxyXG4gICAgICB0eXBlOiAnc3RyaW5nJyxcclxuICAgICAgcnVsZXM6IFt7IHJlcXVpcmVkOiB0cnVlIH1dXHJcbiAgICB9LFxyXG4gICAge1xyXG4gICAgICBwcm9wZXJ0eTogJ2FwcExhc3RTeW5jJyxcclxuICAgICAgdHlwZTogJ251bWJlcicsXHJcbiAgICAgIHJ1bGVzOiBbeyByZXF1aXJlZDogdHJ1ZSB9XVxyXG4gICAgfVxyXG4gIF07XHJcbn07XHJcblxyXG5leHBvcnQgY29uc3QgZ2V0TG9jYWxlTW9kZWwgPSAoYXBwTGFuZ3VhZ2VzKSA9PiB7XHJcbiAgcmV0dXJuIFtcclxuICAgIHtcclxuICAgICAgcHJvcGVydHk6ICdhY3RpdmUnLFxyXG4gICAgICB0eXBlOiAnYm9vbGVhbicsXHJcbiAgICAgIHJ1bGVzOiBbeyByZXF1aXJlZDogdHJ1ZSB9XVxyXG4gICAgfSxcclxuICAgIHtcclxuICAgICAgcHJvcGVydHk6ICdjcmVhdGVkQXQnLFxyXG4gICAgICB0eXBlOiAnbnVtYmVyJyxcclxuICAgICAgcnVsZXM6IFt7IHJlcXVpcmVkOiB0cnVlIH1dXHJcbiAgICB9LFxyXG4gICAge1xyXG4gICAgICBwcm9wZXJ0eTogJ25hbWUnLFxyXG4gICAgICB0eXBlOiAnc3RyaW5nJyxcclxuICAgICAgcnVsZXM6IFt7IHJlcXVpcmVkOiB0cnVlIH1dXHJcbiAgICB9LFxyXG4gICAge1xyXG4gICAgICBwcm9wZXJ0eTogJ3VwZGF0ZWRBdCcsXHJcbiAgICAgIHR5cGU6ICdudW1iZXInXHJcbiAgICB9LFxyXG4gICAge1xyXG4gICAgICBwcm9wZXJ0eTogJ3ZhbHVlJyxcclxuICAgICAgdHlwZTogJ29iamVjdCcsXHJcbiAgICAgIHJ1bGVzOiBbeyByZXF1aXJlZDogdHJ1ZSB9XSxcclxuICAgICAgdmFsaWRhdGlvbnM6IGFwcExhbmd1YWdlcy5tYXAoa2V5ID0+IHtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgcHJvcGVydHk6IGtleSxcclxuICAgICAgICAgIHR5cGU6ICdzdHJpbmcnXHJcbiAgICAgICAgfTtcclxuICAgICAgfSlcclxuICAgIH1cclxuICBdO1xyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IGdldFVzZXJNb2RlbCA9ICh1c2VyRGF0YSwgcGFzc3dvcmRJc1JlcXVpcmVkKSA9PiB7XHJcbiAgcmV0dXJuIFtcclxuICAgIHtcclxuICAgICAgcHJvcGVydHk6ICdhY3RpdmUnLFxyXG4gICAgICB0eXBlOiAnYm9vbGVhbicsXHJcbiAgICAgIHJ1bGVzOiBbeyByZXF1aXJlZDogdHJ1ZSB9XVxyXG4gICAgfSxcclxuICAgIHtcclxuICAgICAgcHJvcGVydHk6ICdjcmVhdGVkQXQnLFxyXG4gICAgICB0eXBlOiAnbnVtYmVyJyxcclxuICAgICAgcnVsZXM6IFt7IHJlcXVpcmVkOiB0cnVlIH1dXHJcbiAgICB9LFxyXG4gICAge1xyXG4gICAgICBwcm9wZXJ0eTogJ2N1c3RvbWVyQ29kZVBheW1lbnQnLFxyXG4gICAgICB0eXBlOiAnc3RyaW5nJ1xyXG4gICAgfSxcclxuICAgIHtcclxuICAgICAgcHJvcGVydHk6ICdkaXNwbGF5TmFtZScsXHJcbiAgICAgIHR5cGU6ICdzdHJpbmcnXHJcbiAgICB9LFxyXG4gICAge1xyXG4gICAgICBwcm9wZXJ0eTogJ2VtYWlsJyxcclxuICAgICAgdHlwZTogJ3N0cmluZycsXHJcbiAgICAgIHJ1bGVzOiBbeyByZXF1aXJlZDogdHJ1ZSB9LCB7IGZvcm1hdDogJ2VtYWlsJyB9XVxyXG4gICAgfSxcclxuICAgIHtcclxuICAgICAgcHJvcGVydHk6ICdwYXNzd29yZCcsXHJcbiAgICAgIHR5cGU6ICdzdHJpbmcnLFxyXG4gICAgICBydWxlczogW3sgcmVxdWlyZWQ6IHBhc3N3b3JkSXNSZXF1aXJlZCB9LCB7IG1pbkxlbmd0aDogNiB9XVxyXG4gICAgfSxcclxuICAgIHtcclxuICAgICAgcHJvcGVydHk6ICdwYXNzd29yZENvbmZpcm1hdGlvbicsXHJcbiAgICAgIHR5cGU6ICdzdHJpbmcnLFxyXG4gICAgICBydWxlczogW3sgcmVxdWlyZWQ6IHBhc3N3b3JkSXNSZXF1aXJlZCB9LCB7IGVxdWFsVG86IHVzZXJEYXRhLnBhc3N3b3JkIH1dXHJcbiAgICB9LFxyXG4gICAge1xyXG4gICAgICBwcm9wZXJ0eTogJ3ByZWZlcmVuY2VzJyxcclxuICAgICAgdHlwZTogJ29iamVjdCcsXHJcbiAgICAgIHZhbGlkYXRpb25zOiBbXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgcHJvcGVydHk6ICdjdXJyZW5jeScsXHJcbiAgICAgICAgICB0eXBlOiAnc3RyaW5nJ1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgcHJvcGVydHk6ICdkYXRlRm9ybWF0JyxcclxuICAgICAgICAgIHR5cGU6ICdzdHJpbmcnXHJcbiAgICAgICAgfSxcclxuICAgICAgICB7XHJcbiAgICAgICAgICBwcm9wZXJ0eTogJ2xhbmd1YWdlJyxcclxuICAgICAgICAgIHR5cGU6ICdzdHJpbmcnXHJcbiAgICAgICAgfVxyXG4gICAgICBdXHJcbiAgICB9LFxyXG4gICAge1xyXG4gICAgICBwcm9wZXJ0eTogJ3VwZGF0ZWRBdCcsXHJcbiAgICAgIHR5cGU6ICdudW1iZXInXHJcbiAgICB9XHJcbiAgXTtcclxufTtcclxuXHJcbmV4cG9ydCBjb25zdCBnZXRSZXBvcnRNb2RlbCA9ICgpID0+IHtcclxuICByZXR1cm4gW1xyXG4gICAge1xyXG4gICAgICBwcm9wZXJ0eTogJ2FjdGl2ZScsXHJcbiAgICAgIHR5cGU6ICdib29sZWFuJyxcclxuICAgICAgcnVsZXM6IFt7IHJlcXVpcmVkOiBmYWxzZSB9LCB7IGRlZmF1bHQ6IGZhbHNlIH1dXHJcbiAgICB9LFxyXG4gICAge1xyXG4gICAgICBwcm9wZXJ0eTogJ2NyZWF0ZWRBdCcsXHJcbiAgICAgIHR5cGU6ICdudW1iZXInLFxyXG4gICAgICBydWxlczogW3sgcmVxdWlyZWQ6IHRydWUgfV1cclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgIHByb3BlcnR5OiAnbmFtZXMnLFxyXG4gICAgICB0eXBlOiAnc3RyaW5nJyxcclxuICAgICAgcnVsZXM6IFt7IHJlcXVpcmVkOiB0cnVlIH1dXHJcbiAgICB9LFxyXG4gICAge1xyXG4gICAgICBwcm9wZXJ0eTogJ2xhc3ROYW1lcycsXHJcbiAgICAgIHR5cGU6ICdzdHJpbmcnLFxyXG4gICAgICBydWxlczogW3sgcmVxdWlyZWQ6IHRydWUgfV1cclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgIHByb3BlcnR5OiAnZW1haWwnLFxyXG4gICAgICB0eXBlOiAnc3RyaW5nJyxcclxuICAgICAgcnVsZXM6IFt7IHJlcXVpcmVkOiB0cnVlIH0sIHsgZm9ybWF0OiAnZW1haWwnIH1dXHJcbiAgICB9LFxyXG4gICAge1xyXG4gICAgICBwcm9wZXJ0eTogJ3Bob25lJyxcclxuICAgICAgdHlwZTogJ3N0cmluZycsXHJcbiAgICAgIHJ1bGVzOiBbeyByZXF1aXJlZDogdHJ1ZSB9LCB7IG1pbkxlbmd0aDogMTAgfV1cclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgIHByb3BlcnR5OiAnZGVwYXJ0bWVudCcsXHJcbiAgICAgIHR5cGU6ICdzdHJpbmcnLFxyXG4gICAgICBydWxlczogW3sgcmVxdWlyZWQ6IHRydWUgfV1cclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgIHByb3BlcnR5OiAnY2l0eScsXHJcbiAgICAgIHR5cGU6ICdzdHJpbmcnLFxyXG4gICAgICBydWxlczogW3sgcmVxdWlyZWQ6IHRydWUgfV1cclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgIHByb3BlcnR5OiAnYWRkcmVzcycsXHJcbiAgICAgIHR5cGU6ICdzdHJpbmcnLFxyXG4gICAgfSxcclxuICAgIHtcclxuICAgICAgcHJvcGVydHk6ICdkZXNjcmlwdGlvbicsXHJcbiAgICAgIHR5cGU6ICdzdHJpbmcnLFxyXG4gICAgICBydWxlczogW3sgcmVxdWlyZWQ6IHRydWUgfV1cclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgIHByb3BlcnR5OiAnY2hlY2tTZW5kRW1haWwnLFxyXG4gICAgICB0eXBlOiAnc3RyaW5nJ1xyXG4gICAgfVxyXG4gIF07XHJcbn07IiwiZXhwb3J0IGNvbnN0IGdldERvd25sb2FkID0gKGRhdGEsIGZpbGVuYW1lKSA9PiB7XHJcbiAgbGV0IGEgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdhJyk7XHJcblxyXG4gIGRvY3VtZW50LmJvZHkuYXBwZW5kQ2hpbGQoYSk7XHJcbiAgYS5zdHlsZSA9ICdkaXNwbGF5OiBub25lJztcclxuICBhLmhyZWYgPSBVUkwuY3JlYXRlT2JqZWN0VVJMKFxyXG4gICAgbmV3IEJsb2IoW2RhdGFdLCB7XHJcbiAgICAgIHR5cGU6ICdvY3RldC9zdHJlYW0nXHJcbiAgICB9KVxyXG4gICk7XHJcbiAgYS5kb3dubG9hZCA9IGZpbGVuYW1lO1xyXG4gIGEuY2xpY2soKTtcclxuICBhLnJlbW92ZSgpO1xyXG4gIHdpbmRvdy5VUkwucmV2b2tlT2JqZWN0VVJMKGEuaHJlZik7XHJcbn07XHJcblxyXG5leHBvcnQgY29uc3QgZ2V0UXVlcnlQYXJhbXNTdHJpbmcgPSBxdWVyeSA9PiB7XHJcbiAgbGV0IHF1ZXJ5U3RyaW5nID0gW107XHJcbiAgbGV0IHF1ZXJ5S2V5cyA9IE9iamVjdC5rZXlzKHF1ZXJ5IHx8IHt9KS5maWx0ZXIoa2V5ID0+IHF1ZXJ5W2tleV0pO1xyXG5cclxuICBmb3IgKGNvbnN0IGtleSBvZiBxdWVyeUtleXMpIHtcclxuICAgIHF1ZXJ5U3RyaW5nID0gcXVlcnlTdHJpbmcuY29uY2F0KGAke2tleX09JHtKU09OLnN0cmluZ2lmeShxdWVyeVtrZXldKX1gKTtcclxuICB9XHJcblxyXG4gIHJldHVybiBgPyR7cXVlcnlTdHJpbmcuam9pbignJicpfWA7XHJcbn07XHJcblxyXG5leHBvcnQgY29uc3QgZ2V0UXVlcnlQYXJhbXNKc29uID0gcXVlcnkgPT4ge1xyXG4gIGNvbnN0IHVybFBhcmFtcyA9IG5ldyBVUkxTZWFyY2hQYXJhbXMocXVlcnkpO1xyXG4gIGxldCBwYXJhbVZhbHVlID0gbnVsbDsgXHJcblxyXG4gIHJldHVybiBBcnJheS5mcm9tKHVybFBhcmFtcykucmVkdWNlKChhY2N1bSwgaXRlbSkgPT4ge1xyXG4gICAgdHJ5IHtcclxuICAgICAgcGFyYW1WYWx1ZSA9IEpTT04ucGFyc2UoaXRlbVsxXSk7XHJcbiAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgIHBhcmFtVmFsdWUgPSBpdGVtWzFdO1xyXG4gICAgfVxyXG5cclxuICAgIGFjY3VtID0geyAuLi5hY2N1bSwgW2l0ZW1bMF1dOiBwYXJhbVZhbHVlIH07XHJcbiAgICByZXR1cm4gYWNjdW07XHJcbiAgfSwge30pO1xyXG59O1xyXG4iLCJleHBvcnQgY29uc3QgZ2V0QmFja2VuZEVycm9yID0gZXJyb3IgPT4ge1xyXG4gIGxldCByZXNwb25zZSA9IHtcclxuICAgIGNvZGU6IGVycm9yLmNvZGUsXHJcbiAgICBtZXNzYWdlOiBlcnJvci5tZXNzYWdlLFxyXG4gICAgc3RhY2s6IGVycm9yLnN0YWNrXHJcbiAgfTtcclxuXHJcbiAgcmV0dXJuIHJlc3BvbnNlO1xyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IGdldEZyb250ZW5kRXJyb3IgPSBlcnJvciA9PiB7XHJcbiAgbGV0IHJlc3BvbnNlID0ge1xyXG4gICAgbWVzc2FnZTogZXJyb3IubWVzc2FnZVxyXG4gIH07XHJcblxyXG4gIHJlc3BvbnNlLmVycm9ycyA9IChlcnJvci5yZXNwb25zZSAmJiBlcnJvci5yZXNwb25zZS5kYXRhICYmIGVycm9yLnJlc3BvbnNlLmRhdGEuZXJyb3JzKSB8fCBudWxsO1xyXG4gIHJlc3BvbnNlLm1lc3NhZ2UgPSAoZXJyb3IucmVzcG9uc2UgJiYgZXJyb3IucmVzcG9uc2UuZGF0YSAmJiBlcnJvci5yZXNwb25zZS5kYXRhLm1lc3NhZ2UpIHx8IGVycm9yLm1lc3NhZ2U7XHJcblxyXG4gIHJldHVybiByZXNwb25zZTtcclxufTtcclxuIiwiZXhwb3J0IGNvbnN0IGZvcm1hdFRlbXBsYXRlID0gKHRlbXBsYXRlLCBkYXRhKSA9PiB7XHJcbiAgbGV0IHJlc3BvbnNlID0gdGVtcGxhdGU7XHJcblxyXG4gIGZvciAoY29uc3Qga2V5IGluIGRhdGEpIHtcclxuICAgIHJlc3BvbnNlID0gcmVzcG9uc2UucmVwbGFjZShuZXcgUmVnRXhwKGB7JHtrZXl9fWAsICdnJyksIGRhdGFba2V5XSk7XHJcbiAgfVxyXG5cclxuICByZXR1cm4gcmVzcG9uc2U7XHJcbn07XHJcblxyXG5leHBvcnQgY29uc3QgbGlrZSA9ICh2YWx1ZSwgY3JpdGVyaWEpID0+IHtcclxuICByZXR1cm4gKHZhbHVlIHx8ICcnKS50b0xvd2VyQ2FzZSgpLmluY2x1ZGVzKChjcml0ZXJpYSB8fCAnJykudG9Mb3dlckNhc2UoKSk7XHJcbn07XHJcblxyXG5leHBvcnQgY29uc3QgdG9VcmwgPSB2YWx1ZSA9PiB7XHJcbiAgbGV0IGVuY29kZWRVcmwgPSB2YWx1ZS50b1N0cmluZygpLnRvTG93ZXJDYXNlKCk7XHJcblxyXG4gIGVuY29kZWRVcmwgPSBlbmNvZGVkVXJsLnJlcGxhY2UoL1teXFx3IF0rL2csICcnKTtcclxuICBlbmNvZGVkVXJsID0gZW5jb2RlZFVybC5yZXBsYWNlKC8gKy9nLCAnLScpO1xyXG5cclxuICByZXR1cm4gZW5jb2RlZFVybDtcclxufTtcclxuIiwiaW1wb3J0IHsgdHJhbnNsYXRlIH0gZnJvbSAnLi9pbnRsLXV0aWxzJztcclxuXHJcbi8vIGhlbHBlcnNcclxuY29uc3QgaXNQcm9wZXJ0eUVtcHR5ID0gdmFsdWUgPT4ge1xyXG4gIGxldCBpc0VtcHR5ID0gZmFsc2U7XHJcblxyXG4gIHN3aXRjaCAodHlwZW9mIHZhbHVlKSB7XHJcbiAgICBjYXNlICdib29sZWFuJzpcclxuICAgICAgaXNFbXB0eSA9ICFbdHJ1ZSwgZmFsc2VdLmluY2x1ZGVzKHZhbHVlKTtcclxuICAgICAgYnJlYWs7XHJcblxyXG4gICAgY2FzZSAnbnVtYmVyJzpcclxuICAgICAgaXNFbXB0eSA9IFtudWxsLCB1bmRlZmluZWRdLmluY2x1ZGVzKHZhbHVlKTtcclxuICAgICAgYnJlYWs7XHJcblxyXG4gICAgY2FzZSAnb2JqZWN0JzpcclxuICAgICAgaWYgKHZhbHVlID09PSBudWxsKSB7XHJcbiAgICAgICAgaXNFbXB0eSA9IHRydWU7XHJcbiAgICAgIH0gZWxzZSBpZiAoQXJyYXkuaXNBcnJheSh2YWx1ZSkpIHtcclxuICAgICAgICBpc0VtcHR5ID0gdmFsdWUubGVuZ3RoID09PSAwO1xyXG4gICAgICB9IGVsc2UgaWYgKCFBcnJheS5pc0FycmF5KHZhbHVlKSkge1xyXG4gICAgICAgIGlzRW1wdHkgPSBPYmplY3Qua2V5cyh2YWx1ZSkubGVuZ3RoID09PSAwO1xyXG4gICAgICB9XHJcbiAgICAgIGJyZWFrO1xyXG5cclxuICAgIGNhc2UgJ3N0cmluZyc6XHJcbiAgICAgIGlzRW1wdHkgPSB2YWx1ZS50cmltKCkubGVuZ3RoID09PSAwO1xyXG4gICAgICBicmVhaztcclxuXHJcbiAgICBkZWZhdWx0OlxyXG4gICAgICBpc0VtcHR5ID0gdHJ1ZTtcclxuICAgICAgYnJlYWs7XHJcbiAgfVxyXG5cclxuICByZXR1cm4gaXNFbXB0eTtcclxufTtcclxuY29uc3QgZ2V0UHJvcGVydHlWYWxpZGF0aW9uID0gKGNvbmZpZywgcHJvcGVydHlEYXRhKSA9PiB7XHJcbiAgY29uc3QgeyBwcm9wZXJ0eVJ1bGVzLCBwcm9wZXJ0eVR5cGUsIHByb3BlcnR5VmFsdWUgfSA9IHByb3BlcnR5RGF0YTtcclxuICBsZXQgZXJyb3IgPSAnJztcclxuXHJcbiAgLy8gdmFsaWRhdGluZyBwcm9wZXJ0eSB0eXBlXHJcbiAgbGV0IHByb3BlcnR5VmFsdWVUeXBlID0gQXJyYXkuaXNBcnJheShwcm9wZXJ0eVZhbHVlKVxyXG4gICAgPyAnYXJyYXknXHJcbiAgICA6IHR5cGVvZiBwcm9wZXJ0eVZhbHVlO1xyXG5cclxuICBpZiAoIWlzUHJvcGVydHlFbXB0eShwcm9wZXJ0eVZhbHVlKSAmJiBwcm9wZXJ0eVZhbHVlVHlwZSAhPT0gcHJvcGVydHlUeXBlKSB7XHJcbiAgICBlcnJvciA9IHRyYW5zbGF0ZShjb25maWcudHJhbnNsYXRpb25zLCAnaW52YWxpZERhdGFUeXBlJywgeyBkYXRhVHlwZTogcHJvcGVydHlUeXBlIH0pO1xyXG4gIH1cclxuXHJcbiAgLy8gdmFsaWRhdGluZyBwcm9wZXJ0eSBydWxlc1xyXG4gIGNvbnN0IGlzUHJvcGVydHlSZXF1aXJlZCA9IHByb3BlcnR5UnVsZXMuZmluZChcclxuICAgIHJ1bGUgPT4gcnVsZS5uYW1lID09PSAncmVxdWlyZWQnICYmIHJ1bGUudmFsdWUgPT09IHRydWVcclxuICApO1xyXG5cclxuICBpZiAoaXNQcm9wZXJ0eUVtcHR5KGVycm9yKSAmJiAoaXNQcm9wZXJ0eVJlcXVpcmVkIHx8ICFpc1Byb3BlcnR5RW1wdHkocHJvcGVydHlWYWx1ZSkpKSB7XHJcbiAgICBmb3IgKGNvbnN0IHJ1bGUgb2YgcHJvcGVydHlSdWxlcykge1xyXG4gICAgICBpZiAoIWlzUHJvcGVydHlFbXB0eShlcnJvcikpIHtcclxuICAgICAgICBicmVhaztcclxuICAgICAgfVxyXG5cclxuICAgICAgc3dpdGNoIChydWxlLm5hbWUpIHtcclxuICAgICAgICBjYXNlICdyZXF1aXJlZCc6XHJcbiAgICAgICAgICBpZiAoaXNQcm9wZXJ0eUVtcHR5KHByb3BlcnR5VmFsdWUpKSB7XHJcbiAgICAgICAgICAgIGVycm9yID0gdHJhbnNsYXRlKGNvbmZpZy50cmFuc2xhdGlvbnMsICdmaWVsZElzUmVxdWlyZWQnKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIGJyZWFrO1xyXG5cclxuICAgICAgICBjYXNlICdhc3NvY2lhdGlvbic6XHJcbiAgICAgICAgICBpZiAoIXJ1bGUudmFsdWUuZmluZChpdGVtID0+IGl0ZW0uaWQgPT09IHByb3BlcnR5VmFsdWUpKSB7XHJcbiAgICAgICAgICAgIGVycm9yID0gdHJhbnNsYXRlKGNvbmZpZy50cmFuc2xhdGlvbnMsICd3cm9uZ0Fzc29jaWF0ZWRJRCcpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgYnJlYWs7XHJcblxyXG4gICAgICAgIGNhc2UgJ2N1cnJlbmNpZXMnOlxyXG4gICAgICAgICAgaWYgKCFydWxlLnZhbHVlLmluY2x1ZGVzKHByb3BlcnR5VmFsdWUudG9Mb3dlckNhc2UoKSkpIHtcclxuICAgICAgICAgICAgZXJyb3IgPSB0cmFuc2xhdGUoY29uZmlnLnRyYW5zbGF0aW9ucywgJ2N1cnJlbmN5Q29kZUluY29ycmVjdCcpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgYnJlYWs7XHJcblxyXG4gICAgICAgIGNhc2UgJ2VxdWFsVG8nOlxyXG4gICAgICAgICAgaWYgKHJ1bGUudmFsdWUgIT09IHByb3BlcnR5VmFsdWUpIHtcclxuICAgICAgICAgICAgZXJyb3IgPSB0cmFuc2xhdGUoY29uZmlnLnRyYW5zbGF0aW9ucywgJ25vdE1hdGNoJyk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICBicmVhaztcclxuXHJcbiAgICAgICAgY2FzZSAnZm9ybWF0JzpcclxuICAgICAgICAgIGlmIChydWxlLnZhbHVlID09PSAnZW1haWwnICYmICFpc0VtYWlsKHByb3BlcnR5VmFsdWUpKSB7XHJcbiAgICAgICAgICAgIGVycm9yID0gdHJhbnNsYXRlKGNvbmZpZy50cmFuc2xhdGlvbnMsICdpbmNvcnJlY3RGb3JtYXQnKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIGlmIChydWxlLnZhbHVlID09PSAndXJsJyAmJiAhaXNVcmwocHJvcGVydHlWYWx1ZSkpIHtcclxuICAgICAgICAgICAgZXJyb3IgPSB0cmFuc2xhdGUoY29uZmlnLnRyYW5zbGF0aW9ucywgJ2luY29ycmVjdEZvcm1hdCcpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgYnJlYWs7XHJcblxyXG4gICAgICAgIGNhc2UgJ2luY2x1ZGUnOlxyXG4gICAgICAgICAgaWYgKCFydWxlLnZhbHVlLmluY2x1ZGVzKHByb3BlcnR5VmFsdWUpKSB7XHJcbiAgICAgICAgICAgIGVycm9yID0gdHJhbnNsYXRlKGNvbmZpZy50cmFuc2xhdGlvbnMsICdtdXN0SW5jbHVkZScsIHsgdmFsdWVzOiBydWxlLnZhbHVlLmpvaW4oJywnKSB9KTtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIGJyZWFrO1xyXG5cclxuICAgICAgICBjYXNlICdtYXhMZW5ndGgnOlxyXG4gICAgICAgICAgaWYgKHByb3BlcnR5VmFsdWUudG9TdHJpbmcoKS5sZW5ndGggPiBydWxlLnZhbHVlKSB7XHJcbiAgICAgICAgICAgIGVycm9yID0gdHJhbnNsYXRlKGNvbmZpZy50cmFuc2xhdGlvbnMsICdtYXhMZW5ndGgnLCB7IG1heExlbmd0aDogcnVsZS52YWx1ZSB9KTtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIGJyZWFrO1xyXG5cclxuICAgICAgICBjYXNlICdtaW5MZW5ndGgnOlxyXG4gICAgICAgICAgaWYgKHByb3BlcnR5VmFsdWUudG9TdHJpbmcoKS5sZW5ndGggPCBydWxlLnZhbHVlKSB7XHJcbiAgICAgICAgICAgIGVycm9yID0gdHJhbnNsYXRlKGNvbmZpZy50cmFuc2xhdGlvbnMsICdtaW5MZW5ndGgnLCB7IG1pbkxlbmd0aDogcnVsZS52YWx1ZSB9KTtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIGJyZWFrO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICByZXR1cm4gZXJyb3I7XHJcbn07XHJcblxyXG4vLyBtZXRob2RzXHJcbmV4cG9ydCBjb25zdCBpc0VtYWlsID0gdmFsdWUgPT4ge1xyXG4gIGNvbnN0IHJlZ0V4cEVtYWlsID0gL15bYS16QS1aMC05Ll8tXStAW2EtekEtWjAtOS4tXStcXC5bYS16QS1aXXsyLDZ9JC87XHJcblxyXG4gIHJldHVybiByZWdFeHBFbWFpbC50ZXN0KHZhbHVlKTtcclxufTtcclxuXHJcbmV4cG9ydCBjb25zdCBpc0h0bWwgPSB2YWx1ZSA9PiB7XHJcbiAgbGV0IGRpdiA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xyXG4gIGRpdi5pbm5lckhUTUwgPSB2YWx1ZTtcclxuXHJcbiAgZm9yIChsZXQgYyA9IGRpdi5jaGlsZE5vZGVzLCBpID0gYy5sZW5ndGg7IGktLTsgKSB7XHJcbiAgICBpZiAoY1tpXS5ub2RlVHlwZSA9PT0gMSkge1xyXG4gICAgICByZXR1cm4gdHJ1ZTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHJldHVybiBmYWxzZTtcclxufTtcclxuXHJcbmV4cG9ydCBjb25zdCBpc1VybCA9IHZhbHVlID0+IHtcclxuICBjb25zdCBwYXR0ZXJuID0gbmV3IFJlZ0V4cChcclxuICAgICdeKGh0dHBzPzpcXFxcL1xcXFwvKT8nICsgLy8gcHJvdG9jb2xcclxuICAgICcoKChbYS16XFxcXGRdKFthLXpcXFxcZC1dKlthLXpcXFxcZF0pKilcXFxcLj8pK1thLXpdezIsfXwnICsgLy8gZG9tYWluIG5hbWVcclxuICAgICcoKFxcXFxkezEsM31cXFxcLil7M31cXFxcZHsxLDN9KSknICsgLy8gaXAgKHY0KSBhZGRyZXNzXHJcbiAgICAnKFxcXFw6XFxcXGQrKT8oXFxcXC9bLWEtelxcXFxkJV8ufitdKikqJyArIC8vIHBvcnRcclxuICAgICcoXFxcXD9bOyZhbXA7YS16XFxcXGQlXy5+Kz0tXSopPycgKyAvLyBxdWVyeSBzdHJpbmdcclxuICAgICAgJyhcXFxcI1stYS16XFxcXGRfXSopPyQnLFxyXG4gICAgJ2knXHJcbiAgKTtcclxuXHJcbiAgcmV0dXJuIHBhdHRlcm4udGVzdCh2YWx1ZSk7XHJcbn07XHJcblxyXG5leHBvcnQgY29uc3QgdmFsaWRhdGVNb2RlbCA9IChjb25maWcsIG1vZGVsLCB2YWxpZGF0aW9ucyA9IFtdKSA9PiB7XHJcbiAgbGV0IGVycm9ycyA9IHt9O1xyXG4gIGxldCBkYXRhID0ge307XHJcbiAgbGV0IHByb3BlcnR5ID0gbnVsbDtcclxuICBsZXQgcHJvcGVydHlUeXBlID0gbnVsbDtcclxuICBsZXQgcHJvcGVydHlSdWxlcyA9IFtdO1xyXG4gIGxldCBwcm9wZXJ0eVZhbHVlID0gbnVsbDtcclxuXHJcbiAgZm9yIChsZXQgdmFsaWRhdGlvbiBvZiB2YWxpZGF0aW9ucykge1xyXG4gICAgcHJvcGVydHkgPSB2YWxpZGF0aW9uLnByb3BlcnR5O1xyXG4gICAgcHJvcGVydHlUeXBlID0gdmFsaWRhdGlvbi50eXBlO1xyXG4gICAgcHJvcGVydHlSdWxlcyA9ICh2YWxpZGF0aW9uLnJ1bGVzIHx8IFtdKS5mbGF0TWFwKHJ1bGUgPT5cclxuICAgICAgT2JqZWN0LmtleXMocnVsZSkubWFwKGtleSA9PiAoeyBuYW1lOiBrZXksIHZhbHVlOiBydWxlW2tleV0gfSkpXHJcbiAgICApO1xyXG4gICAgcHJvcGVydHlWYWx1ZSA9IG1vZGVsW3Byb3BlcnR5XTtcclxuXHJcbiAgICAvLyBhc3NpZ25pbmcgb25seSBwcmltaXRpdmUgZGF0YSAoYm9vbGVhbiwgbnVtYmVycywgc3RyaW5ncylcclxuICAgIGlmICghaXNQcm9wZXJ0eUVtcHR5KHByb3BlcnR5VmFsdWUpICYmIHR5cGVvZiBwcm9wZXJ0eVZhbHVlICE9PSAnb2JqZWN0Jykge1xyXG4gICAgICBkYXRhW3Byb3BlcnR5XSA9IHByb3BlcnR5VmFsdWU7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gdmFsaWRhdGluZyBpbm5lciBwcm9wZXJ0aWVzIGZvciBhcnJheXNcclxuICAgIGlmIChcclxuICAgICAgIWlzUHJvcGVydHlFbXB0eShwcm9wZXJ0eVZhbHVlKSAmJlxyXG4gICAgICBBcnJheS5pc0FycmF5KHByb3BlcnR5VmFsdWUpICYmXHJcbiAgICAgIHByb3BlcnR5VHlwZSA9PT0gJ2FycmF5J1xyXG4gICAgKSB7XHJcbiAgICAgIGxldCB2YWxpZGF0aW9uUmVzcG9uc2UgPSBudWxsO1xyXG4gICAgICBsZXQgaXRlbVZhbHVlID0ge307XHJcblxyXG4gICAgICBmb3IgKGNvbnN0IGl0ZW0gb2YgcHJvcGVydHlWYWx1ZSkge1xyXG4gICAgICAgIGlmICh0eXBlb2YgaXRlbSA9PT0gJ29iamVjdCcpIHtcclxuICAgICAgICAgIGlmICh2YWxpZGF0aW9uLnZhbGlkYXRpb25zKSB7XHJcbiAgICAgICAgICAgIGl0ZW1WYWx1ZSA9IE9iamVjdC5rZXlzKGl0ZW0pLnJlZHVjZSgoYWNjdW0sIGtleSkgPT4ge1xyXG4gICAgICAgICAgICAgIHJldHVybiAodmFsaWRhdGlvbi52YWxpZGF0aW9ucyB8fCBbXSkubWFwKGl0ZW0gPT4gaXRlbS5wcm9wZXJ0eSkuaW5jbHVkZXMoa2V5KSBcclxuICAgICAgICAgICAgICAgID8gT2JqZWN0LmFzc2lnbihhY2N1bSwgeyBba2V5XTogaXRlbVtrZXldIH0pIFxyXG4gICAgICAgICAgICAgICAgOiBhY2N1bTtcclxuICAgICAgICAgICAgfSwge30pO1xyXG5cclxuICAgICAgICAgICAgdmFsaWRhdGlvblJlc3BvbnNlID0gdmFsaWRhdGVNb2RlbChcclxuICAgICAgICAgICAgICBjb25maWcsIFxyXG4gICAgICAgICAgICAgIGl0ZW1WYWx1ZSxcclxuICAgICAgICAgICAgICB2YWxpZGF0aW9uLnZhbGlkYXRpb25zXHJcbiAgICAgICAgICAgICk7XHJcblxyXG4gICAgICAgICAgICBkYXRhW3Byb3BlcnR5XSA9IChkYXRhW3Byb3BlcnR5XSB8fCBbXSkuY29uY2F0KFxyXG4gICAgICAgICAgICAgIHZhbGlkYXRpb25SZXNwb25zZS5kYXRhXHJcbiAgICAgICAgICAgICk7XHJcblxyXG4gICAgICAgICAgICBpZiAoIWlzUHJvcGVydHlFbXB0eSh2YWxpZGF0aW9uUmVzcG9uc2UuZXJyb3JzKSkge1xyXG4gICAgICAgICAgICAgIGVycm9yc1twcm9wZXJ0eV0gPSAoZXJyb3JzW3Byb3BlcnR5XSB8fCBbXSkuY29uY2F0KFxyXG4gICAgICAgICAgICAgICAgdmFsaWRhdGlvblJlc3BvbnNlLmVycm9yc1xyXG4gICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGRhdGFbcHJvcGVydHldID0gKGRhdGFbcHJvcGVydHldIHx8IFtdKS5jb25jYXQoaXRlbSk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIGRhdGFbcHJvcGVydHldID0gKGRhdGFbcHJvcGVydHldIHx8IFtdKS5jb25jYXQoaXRlbSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLy8gdmFsaWRhdGluZyBpbm5lciBwcm9wZXJ0aWVzIGZvciBvYmplY3RzXHJcbiAgICBpZiAoXHJcbiAgICAgICFpc1Byb3BlcnR5RW1wdHkocHJvcGVydHlWYWx1ZSkgJiZcclxuICAgICAgT2JqZWN0LnByb3RvdHlwZS50b1N0cmluZy5jYWxsKHByb3BlcnR5VmFsdWUpID09PSAnW29iamVjdCBPYmplY3RdJyAmJlxyXG4gICAgICBwcm9wZXJ0eVR5cGUgPT09ICdvYmplY3QnXHJcbiAgICApIHtcclxuICAgICAgaWYgKHZhbGlkYXRpb24udmFsaWRhdGlvbnMpIHtcclxuICAgICAgICBwcm9wZXJ0eVZhbHVlID0gT2JqZWN0LmtleXMocHJvcGVydHlWYWx1ZSkucmVkdWNlKChhY2N1bSwga2V5KSA9PiB7XHJcbiAgICAgICAgICByZXR1cm4gKHZhbGlkYXRpb24udmFsaWRhdGlvbnMgfHwgW10pLm1hcChpdGVtID0+IGl0ZW0ucHJvcGVydHkpLmluY2x1ZGVzKGtleSkgXHJcbiAgICAgICAgICAgID8gT2JqZWN0LmFzc2lnbihhY2N1bSwgeyBba2V5XTogcHJvcGVydHlWYWx1ZVtrZXldIH0pIFxyXG4gICAgICAgICAgICA6IGFjY3VtO1xyXG4gICAgICAgIH0sIHt9KTtcclxuICAgICAgICBjb25zdCB2YWxpZGF0aW9uUmVzcG9uc2UgPSB2YWxpZGF0ZU1vZGVsKFxyXG4gICAgICAgICAgY29uZmlnLFxyXG4gICAgICAgICAgcHJvcGVydHlWYWx1ZSxcclxuICAgICAgICAgIHZhbGlkYXRpb24udmFsaWRhdGlvbnNcclxuICAgICAgICApO1xyXG4gICAgICAgIGRhdGFbcHJvcGVydHldID0gdmFsaWRhdGlvblJlc3BvbnNlLmRhdGE7XHJcblxyXG4gICAgICAgIGlmICghaXNQcm9wZXJ0eUVtcHR5KHZhbGlkYXRpb25SZXNwb25zZS5lcnJvcnMpKSB7XHJcbiAgICAgICAgICBlcnJvcnNbcHJvcGVydHldID0gdmFsaWRhdGlvblJlc3BvbnNlLmVycm9ycztcclxuICAgICAgICB9XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgZGF0YVtwcm9wZXJ0eV0gPSBwcm9wZXJ0eVZhbHVlO1xyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLy8gdmFsaWRhdGluZyBwcm9wZXJ0eVxyXG4gICAgY29uc3QgdmFsaWRhdGlvblJlc3BvbnNlID0gZ2V0UHJvcGVydHlWYWxpZGF0aW9uKGNvbmZpZywge1xyXG4gICAgICBwcm9wZXJ0eVJ1bGVzLFxyXG4gICAgICBwcm9wZXJ0eVR5cGUsXHJcbiAgICAgIHByb3BlcnR5VmFsdWVcclxuICAgIH0pO1xyXG5cclxuICAgIGlmICghaXNQcm9wZXJ0eUVtcHR5KHZhbGlkYXRpb25SZXNwb25zZSkpIHtcclxuICAgICAgZXJyb3JzW3Byb3BlcnR5XSA9IHZhbGlkYXRpb25SZXNwb25zZTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHJldHVybiB7IGVycm9ycywgZGF0YSB9O1xyXG59O1xyXG4iLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJAYmFiZWwvcG9seWZpbGxcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiQHNlbmRncmlkL21haWxcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiYXhpb3NcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiYnVzYm95XCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcImNvcnNcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiZXhwcmVzc1wiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJmaXJlYmFzZS1hZG1pblwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJmaXJlYmFzZS1mdW5jdGlvbnNcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiZmlyZWJhc2UvYXBwXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcImZpcmViYXNlL2F1dGhcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiZmlyZWJhc2UvZGF0YWJhc2VcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiZmlyZWJhc2UvZmlyZXN0b3JlXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcImZzXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIm1lbW9yeS1jYWNoZVwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJvc1wiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJwYXRoXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlYWN0XCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlYWN0LWRvbS9zZXJ2ZXJcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwicmVhY3QtaGVsbWV0XCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlYWN0LXJlZHV4XCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlYWN0LXJvdXRlci1kb21cIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwicmVhY3Qtc2VsZWN0XCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlZHV4XCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlZHV4LWRldnRvb2xzLWV4dGVuc2lvbi9kZXZlbG9wbWVudE9ubHlcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwicmVkdXgtdGh1bmtcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwic2VyaWFsaXplLWphdmFzY3JpcHRcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwic3R5bGVkLWNvbXBvbmVudHNcIik7Il0sInNvdXJjZVJvb3QiOiIifQ==