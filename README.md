# About

Archive and manage your income and expenses invoices, get real time reports of your personal or business finances.

## Available Scripts

In the project directory, you can run:

### `npm install`

Runs this command to install node dependencies of the project.

### `npm analyze`

Open a new tab in the browser with details of each library inside the bundle.

### `npm run build:dev`

Generate a bundle with the development version

### `npm run build:prod`

Generate a bundle with the production version

### `npm start`

Runs the app in development mode. Open [http://localhost:5000](http://localhost:5000)<br>
Runs the api in development mode. Open [http://localhost:5001](http://localhost:5001)

### `npm run deploy`

Will publish hosting and cloud functions to google hosting.
