import Home from './pages/home';
import Login from './pages/login';
import RecoverPassword from './pages/recover-password';
import Register from './pages/register';
import NotFound from './pages/not-found';

export default [
  {
    exact: true,
    path: '/',
    component: Home
  },
  {
    path: '/login',
    component: Login
  },
  {
    path: '/login/:provider?/:token',
    component: Login
  },
  {
    path: '/register',
    component: Register
  },
  {
    path: '/recover-password',
    component: RecoverPassword
  },
  {
    component: NotFound
  }
];
