import React from 'react';
import Authorization from './pages/_components/authorization';
import NotFound from './pages/not-found';
// Admin components
const CatalogList = React.lazy(() => import('./pages/admin/catalogs/catalog-list'));
const CatalogEdition = React.lazy(() => import('./pages/admin/catalogs/catalog-edition'));
const ConfigurationEdition = React.lazy(() => import('./pages/admin/configuration/configurationEdition'));
const LocaleList = React.lazy(() => import('./pages/admin/locales/locale-list'));
const LocaleEdition = React.lazy(() => import('./pages/admin/locales/locale-edition'));
const UserList = React.lazy(() => import('./pages/admin/users/user-list'));
const UserEdition = React.lazy(() => import('./pages/admin/users/user-edition'));
// App components
const Dashboard = React.lazy(() => import('./pages/app/users/dashboard'));
const Profile = React.lazy(() => import('./pages/app/users/profile'));
// Home components
const Home = React.lazy(() => import('./pages/home'));
const Contact = React.lazy(() => import('./pages/contact'));
const Login = React.lazy(() => import('./pages/login'));
const RecoverPassword = React.lazy(() => import('./pages/recover-password'));
const Register = React.lazy(() => import('./pages/register'));

export default [
  // ADMIN
  {
    exact: true,
    path: '/admin/catalogs',
    component: Authorization(CatalogList, ['isAdmin'])
  },
  {
    path: '/admin/catalogs/new',
    component: Authorization(CatalogEdition, ['isAdmin'])
  },
  {
    path: '/admin/catalogs/:id/children',
    component: Authorization(CatalogList, ['isAdmin'])
  },
  {
    path: '/admin/catalogs/:id',
    component: Authorization(CatalogEdition, ['isAdmin'])
  },
  {
    path: '/admin/configuration',
    component: Authorization(ConfigurationEdition, ['isAdmin'])
  },
  {
    exact: true,
    path: '/admin/locales',
    component: Authorization(LocaleList, ['isAdmin'])
  },
  {
    path: '/admin/locales/new',
    component: Authorization(LocaleEdition, ['isAdmin'])
  },
  {
    path: '/admin/locales/:id',
    component: Authorization(LocaleEdition, ['isAdmin'])
  },
  {
    exact: true,
    path: '/admin/users',
    component: Authorization(UserList, ['isAdmin'])
  },
  {
    path: '/admin/users/new',
    component: Authorization(UserEdition, ['isAdmin'])
  },
  {
    path: '/admin/users/:id',
    component: Authorization(UserEdition, ['isAdmin'])
  },
  // APP
  {
    path: '/app/dashboard',
    component: Authorization(Dashboard, ['isRegistered'])
  },
  {
    path: '/app/user/profile',
    component: Authorization(Profile, ['isRegistered'])
  },
  // HOME
  {
    exact: true,
    path: '/',
    component: Home
  },
  {
    exact: true,
    path: '/contact',
    component: Contact
  },
  {
    path: '/login',
    component: Login
  },
  {
    path: '/login/:provider?/:token',
    component: Login
  },
  {
    path: '/register',
    component: Register
  },
  {
    path: '/recover-password',
    component: RecoverPassword
  },
  {
    component: NotFound
  }
];
