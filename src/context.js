import React from 'react';
import store from './store/store';
import { getCurrencyConversions, setCurrencyConversions, setPreferences } from './store/actions/config-actions';
import { updateUser, logout as logoutAction } from './store/actions/user-actions';

const changeCurrency = async currency => {
  try {
    const { user } = store.getState();
    const { auth } = user;

    await store.dispatch(getCurrencyConversions({ currency, date: 'latest' }));
    store.dispatch(setCurrencyConversions(store.getState().config.temp));
    localStorage.currency = currency;
    store.dispatch(setPreferences({ currency: currency }));

    if (auth) {
      const newPreferences = { ...auth.profile.preferences, currency };
      store.dispatch(updateUser({ id: auth.uid, preferences: newPreferences }));
    }
  } catch (e) {
    console.error(e);
  }
};

const changeDateFormat = async dateFormat => {
  try {
    const { user } = store.getState();
    const { auth } = user;

    localStorage.dateFormat = dateFormat;
    store.dispatch(setPreferences({ dateFormat }));

    if (auth) {
      const newPreferences = { ...auth.profile.preferences, dateFormat };
      store.dispatch(updateUser({ id: auth.uid, preferences: newPreferences }));
    }
  } catch (e) {
    console.error(e);
  }
};

const changeLanguage = async language => {
  try {
    const { user } = store.getState();
    const { auth } = user;
    localStorage.language = language;
    store.dispatch(setPreferences({ language: language }));

    if (auth) {
      const newPreferences = { ...auth.profile.preferences, language };
      store.dispatch(updateUser({ id: auth.uid, preferences: newPreferences }));
    }
  } catch (e) {
    console.error(e);
  }
};

const loadPreferences = () => {
  const { config, user } = store.getState();
  const defaultPreferences = config.preferences;
  const userPreferences = (user.auth && user.auth.profile && user.auth.profile.preferences);
  const localPreferences = {
    currency: localStorage.currency || defaultPreferences.currency,
    dateFormat: localStorage.dateFormat || defaultPreferences.dateFormat,
    language: localStorage.language || defaultPreferences.language
  };
  const preferences = Object.assign(
    defaultPreferences, 
    localPreferences, 
    userPreferences
  );
  const { currency } = preferences;   

  store.dispatch(getCurrencyConversions({ currency, date: 'latest' }))
    .then(() => store.dispatch(setCurrencyConversions(store.getState().config.temp)));
    
  store.dispatch(setPreferences(preferences));
};

const logout = async () => {
  try {
    localStorage.removeItem('token');
    await store.dispatch(logoutAction());
    window.location.href = `${window.location.origin}/login`;
  } catch (e) {
    console.error(e);
  }
};

export default React.createContext({
  changeCurrency,
  changeDateFormat,
  changeLanguage,
  loadPreferences,
  logout
});