export const getBackendError = error => {
  let response = {
    code: error.code,
    message: error.message,
    stack: error.stack
  };

  return response;
};

export const getFrontendError = error => {
  let response = {
    message: error.message
  };

  response.errors = (error.response && error.response.data && error.response.data.errors) || null;
  response.message = (error.response && error.response.data && error.response.data.message) || error.message;

  return response;
};
