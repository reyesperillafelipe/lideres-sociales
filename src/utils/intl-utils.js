import { getDbQuery } from './database-utils';
import { formatTemplate } from './text-utils';

const CACHE = {
  INTL_DATA: 'CACHE_INTL_DATA',
  CURRENCY_CONVERSIONS: 'CACHE_CURRENCY_CONVERSIONS'
};

export const formatDate = (datetime, format) => {
  const date = new Date(datetime);
  const day = date.getDate();
  const month = date.getMonth() + 1;
  const year = date.getFullYear();
  const response = format
    .toLowerCase()
    .replace(new RegExp('dd', 'g'), day.toString().padStart(2, '0'))
    .replace(new RegExp('mm', 'g'), month.toString().padStart(2, '0'))
    .replace(new RegExp('yyyy', 'g'), year);

  return response;
};

export const formatLocales = (languages, locales) => {
  let response = {};

  response = languages.reduce((accum, lang) => {
    locales.forEach(locale => {
      accum[lang] = accum[lang] || {};
      accum[lang][locale.name] = locale.value[lang];
    });

    return accum;
  }, {});

  return response;
};

export const formatMoney = (value, thousands = ',') => {
  const negativeSign = value < 0 ? '-' : '';
  let i = Math.round(value).toString();
  let j = i.length > 3 ? i.length % 3 : 0;

  return (
    negativeSign +
    (j ? i.substr(0, j) + thousands : '') +
    i.substr(j).replace(/(\d{3})(?=\d)/g, '$1' + thousands)
  );
};

export const getIntlData = async (config, axios, cache, db, intlAPI) => {
  let response = cache.get(CACHE.INTL_DATA);

  if (!response) {
    let sourceIntl = await axios.get(
      `${intlAPI.url}/all?fields=alpha3Code;callingCodes;currencies;flag;languages;name;translations`
    );
    sourceIntl = sourceIntl.data;
    response = {};
    
    // calling codes
    response.callingCodes = sourceIntl
      .reduce((accum, item) => {
        accum = accum.concat(item.callingCodes
          .filter(callingCode => callingCode)
          .map(callingCode => ({ label: `+${callingCode}`, value: `+${callingCode}` }))
        );

        return accum;
      }, [])
      .filter((item, index, array) => array.findIndex(obj => obj.value === item.value) === index)
      .sort((a, b) => a.label.localeCompare(b.label));
    
    // countries 
    response.countries = sourceIntl
      .reduce((accum, item) => {
        accum = accum.concat({ label: item.name, value: item.alpha3Code.toLowerCase() });

        return accum;
      }, [])
      .sort((a, b) => a.label.localeCompare(b.label));

    // currencies
    response.currencies = sourceIntl
      .reduce((accum, item) => {
        accum = accum.concat(item.currencies
          .filter(currencyInfo => currencyInfo.code && currencyInfo.code !== '(none)')
          .map(currencyInfo => ({ label: currencyInfo.code.toUpperCase(), value: currencyInfo.code.toLowerCase() }))
        );

        return accum;
      }, [])
      .filter((item, index, array) => array.findIndex(obj => obj.value === item.value) === index)
      .sort((a, b) => a.label.localeCompare(b.label));
    
    // languages
    response.languages = sourceIntl
      .reduce((accum, item) => {
        accum = accum.concat(item.languages
          .filter(languageInfo => languageInfo.iso639_1)
          .map(languageInfo => ({ label: languageInfo.name, value: languageInfo.iso639_1 }))
        );

        return accum;
      }, [])
      .filter((item, index, array) => array.findIndex(obj => obj.value === item.value) === index)
      .sort((a, b) => a.label.localeCompare(b.label));

    // locales
    const locales = await getDbQuery(db, 'locales', {});
    const formattedLocales = formatLocales(config.appLanguages, locales);
    response.locales = formattedLocales;

    cache.put(CACHE.INTL_DATA, response);
  }

  return response;
};

export const translate = (translations = {}, locale = '', params = {}) => {
  const localeValue = translations[locale] || '';
  const response = formatTemplate(localeValue, params) || locale;

  return response;
};
