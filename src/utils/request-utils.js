export const getDownload = (data, filename) => {
  let a = document.createElement('a');

  document.body.appendChild(a);
  a.style = 'display: none';
  a.href = URL.createObjectURL(
    new Blob([data], {
      type: 'octet/stream'
    })
  );
  a.download = filename;
  a.click();
  a.remove();
  window.URL.revokeObjectURL(a.href);
};

export const getQueryParamsString = query => {
  let queryString = [];
  let queryKeys = Object.keys(query || {}).filter(key => query[key]);

  for (const key of queryKeys) {
    queryString = queryString.concat(`${key}=${JSON.stringify(query[key])}`);
  }

  return `?${queryString.join('&')}`;
};

export const getQueryParamsJson = query => {
  const urlParams = new URLSearchParams(query);
  let paramValue = null; 

  return Array.from(urlParams).reduce((accum, item) => {
    try {
      paramValue = JSON.parse(item[1]);
    } catch (e) {
      paramValue = item[1];
    }

    accum = { ...accum, [item[0]]: paramValue };
    return accum;
  }, {});
};
