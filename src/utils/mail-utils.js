import { translate } from './intl-utils';

export const sendNotification = async (config, emailLibrary, emailData) => {
  try {
    const { sendgrid: sendgridConfig } = config;
    emailLibrary.setApiKey(sendgridConfig.key);

    await emailLibrary.send({
      from: 'noreply@mail.com',
      to: emailData.to,
      templateId: sendgridConfig.templategeneral,
      'dynamic_template_data': {
        slogan: translate(config.translations, 'slogan'),
        subject: emailData.subject,
        message: emailData.message
      }
    });
  } catch (error) {
    console.warn('Error sending subscription notification', emailData, error);
  }
};