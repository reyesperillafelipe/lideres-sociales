import { translate } from './intl-utils';

// helpers
const isPropertyEmpty = value => {
  let isEmpty = false;

  switch (typeof value) {
    case 'boolean':
      isEmpty = ![true, false].includes(value);
      break;

    case 'number':
      isEmpty = [null, undefined].includes(value);
      break;

    case 'object':
      if (value === null) {
        isEmpty = true;
      } else if (Array.isArray(value)) {
        isEmpty = value.length === 0;
      } else if (!Array.isArray(value)) {
        isEmpty = Object.keys(value).length === 0;
      }
      break;

    case 'string':
      isEmpty = value.trim().length === 0;
      break;

    default:
      isEmpty = true;
      break;
  }

  return isEmpty;
};
const getPropertyValidation = (config, propertyData) => {
  const { propertyRules, propertyType, propertyValue } = propertyData;
  let error = '';

  // validating property type
  let propertyValueType = Array.isArray(propertyValue)
    ? 'array'
    : typeof propertyValue;

  if (!isPropertyEmpty(propertyValue) && propertyValueType !== propertyType) {
    error = translate(config.translations, 'invalidDataType', { dataType: propertyType });
  }

  // validating property rules
  const isPropertyRequired = propertyRules.find(
    rule => rule.name === 'required' && rule.value === true
  );

  if (isPropertyEmpty(error) && (isPropertyRequired || !isPropertyEmpty(propertyValue))) {
    for (const rule of propertyRules) {
      if (!isPropertyEmpty(error)) {
        break;
      }

      switch (rule.name) {
        case 'required':
          if (isPropertyEmpty(propertyValue)) {
            error = translate(config.translations, 'fieldIsRequired');
          }
          break;

        case 'association':
          if (!rule.value.find(item => item.id === propertyValue)) {
            error = translate(config.translations, 'wrongAssociatedID');
          }
          break;

        case 'currencies':
          if (!rule.value.includes(propertyValue.toLowerCase())) {
            error = translate(config.translations, 'currencyCodeIncorrect');
          }
          break;

        case 'equalTo':
          if (rule.value !== propertyValue) {
            error = translate(config.translations, 'notMatch');
          }
          break;

        case 'format':
          if (rule.value === 'email' && !isEmail(propertyValue)) {
            error = translate(config.translations, 'incorrectFormat');
          }
          if (rule.value === 'url' && !isUrl(propertyValue)) {
            error = translate(config.translations, 'incorrectFormat');
          }
          break;

        case 'include':
          if (!rule.value.includes(propertyValue)) {
            error = translate(config.translations, 'mustInclude', { values: rule.value.join(',') });
          }
          break;

        case 'maxLength':
          if (propertyValue.toString().length > rule.value) {
            error = translate(config.translations, 'maxLength', { maxLength: rule.value });
          }
          break;

        case 'minLength':
          if (propertyValue.toString().length < rule.value) {
            error = translate(config.translations, 'minLength', { minLength: rule.value });
          }
          break;
      }
    }
  }

  return error;
};

// methods
export const isEmail = value => {
  const regExpEmail = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/;

  return regExpEmail.test(value);
};

export const isHtml = value => {
  let div = document.createElement('div');
  div.innerHTML = value;

  for (let c = div.childNodes, i = c.length; i--; ) {
    if (c[i].nodeType === 1) {
      return true;
    }
  }

  return false;
};

export const isUrl = value => {
  const pattern = new RegExp(
    '^(https?:\\/\\/)?' + // protocol
    '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.?)+[a-z]{2,}|' + // domain name
    '((\\d{1,3}\\.){3}\\d{1,3}))' + // ip (v4) address
    '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port
    '(\\?[;&amp;a-z\\d%_.~+=-]*)?' + // query string
      '(\\#[-a-z\\d_]*)?$',
    'i'
  );

  return pattern.test(value);
};

export const validateModel = (config, model, validations = []) => {
  let errors = {};
  let data = {};
  let property = null;
  let propertyType = null;
  let propertyRules = [];
  let propertyValue = null;

  for (let validation of validations) {
    property = validation.property;
    propertyType = validation.type;
    propertyRules = (validation.rules || []).flatMap(rule =>
      Object.keys(rule).map(key => ({ name: key, value: rule[key] }))
    );
    propertyValue = model[property];

    // assigning only primitive data (boolean, numbers, strings)
    if (!isPropertyEmpty(propertyValue) && typeof propertyValue !== 'object') {
      data[property] = propertyValue;
    }

    // validating inner properties for arrays
    if (
      !isPropertyEmpty(propertyValue) &&
      Array.isArray(propertyValue) &&
      propertyType === 'array'
    ) {
      let validationResponse = null;
      let itemValue = {};

      for (const item of propertyValue) {
        if (typeof item === 'object') {
          if (validation.validations) {
            itemValue = Object.keys(item).reduce((accum, key) => {
              return (validation.validations || []).map(item => item.property).includes(key) 
                ? Object.assign(accum, { [key]: item[key] }) 
                : accum;
            }, {});

            validationResponse = validateModel(
              config, 
              itemValue,
              validation.validations
            );

            data[property] = (data[property] || []).concat(
              validationResponse.data
            );

            if (!isPropertyEmpty(validationResponse.errors)) {
              errors[property] = (errors[property] || []).concat(
                validationResponse.errors
              );
            }
          } else {
            data[property] = (data[property] || []).concat(item);
          }
        } else {
          data[property] = (data[property] || []).concat(item);
        }
      }
    }

    // validating inner properties for objects
    if (
      !isPropertyEmpty(propertyValue) &&
      Object.prototype.toString.call(propertyValue) === '[object Object]' &&
      propertyType === 'object'
    ) {
      if (validation.validations) {
        propertyValue = Object.keys(propertyValue).reduce((accum, key) => {
          return (validation.validations || []).map(item => item.property).includes(key) 
            ? Object.assign(accum, { [key]: propertyValue[key] }) 
            : accum;
        }, {});
        const validationResponse = validateModel(
          config,
          propertyValue,
          validation.validations
        );
        data[property] = validationResponse.data;

        if (!isPropertyEmpty(validationResponse.errors)) {
          errors[property] = validationResponse.errors;
        }
      } else {
        data[property] = propertyValue;
      }
    }

    // validating property
    const validationResponse = getPropertyValidation(config, {
      propertyRules,
      propertyType,
      propertyValue
    });

    if (!isPropertyEmpty(validationResponse)) {
      errors[property] = validationResponse;
    }
  }

  return { errors, data };
};
