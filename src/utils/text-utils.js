export const formatTemplate = (template, data) => {
  let response = template;

  for (const key in data) {
    response = response.replace(new RegExp(`{${key}}`, 'g'), data[key]);
  }

  return response;
};

export const like = (value, criteria) => {
  return (value || '').toLowerCase().includes((criteria || '').toLowerCase());
};

export const toUrl = value => {
  let encodedUrl = value.toString().toLowerCase();

  encodedUrl = encodedUrl.replace(/[^\w ]+/g, '');
  encodedUrl = encodedUrl.replace(/ +/g, '-');

  return encodedUrl;
};
