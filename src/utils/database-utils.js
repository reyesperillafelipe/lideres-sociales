export const getDbDocument = async (db, collection, documentId) => {
  let response = null;

  if (!documentId) {
    return null;
  }

  response = db.collection(collection);
  response = await response.doc(documentId).get();
  response = response.data();

  if (response) {
    response = { ...response, id: documentId };
  }

  return response;
};

export const getDbQuery = async (db, collection, query) => {
  const parsedQuery = getDbParsedQuery(query);
  let dbQuery = db.collection(collection);
  let response = [];

  if (parsedQuery.where && parsedQuery.where.length) {
    parsedQuery.where.forEach(item => (dbQuery = dbQuery.where(item[0], item[1], item[2])));
  }
  if (parsedQuery.sort && parsedQuery.sort.length) {
    parsedQuery.sort.forEach(item => (dbQuery = dbQuery.orderBy(item[0], item[1] || 'asc')));
  }
  if (parsedQuery.limit) {
    dbQuery = dbQuery.limit(parsedQuery.limit);
  }

  response = await dbQuery.get();
  response = response.docs.map(doc => ({ ...doc.data(), id: doc.id }));

  if (parsedQuery.select) {
    response = response.map(item => {
      return Object.keys(item).reduce((accum, key) => {
        return parsedQuery.select.includes(key) ? Object.assign(accum, { [key]: item[key] }) : accum;
      }, {});
    });
  }

  return response;
};

export const getDbQueryByIds = async (db, collection, ids) => {
  let response = [];
  let documentReferences = ids.filter(id => id).map(id => db.collection(collection).doc(id).get());

  response = await Promise.all(documentReferences);
  response = response.filter(doc => !doc.isEmpty).map(doc => ({ ...doc.data(), id: doc.id }));
  
  return response;
};

export const getDbParsedQuery = data => {
  let response = {};
  let searchCriteria = {};

  for (const key in data) {
    searchCriteria = data[key];

    if (key === 'limit') {
      response.limit = searchCriteria;
    }
    if (key === 'select') {
      response.select = searchCriteria;
    }
    if (key === 'sort') {
      response.sort = [];

      for (const key in searchCriteria) {
        response.sort.push([key, searchCriteria[key]]);
      }
    }
    if (key === 'where') {
      response.where = [];

      for (const key in searchCriteria) {
        for (const criteria in searchCriteria[key]) {
          if (criteria === 'in') {
            response.where.push([key, 'array-contains', searchCriteria[key][criteria]]);
          } else if (criteria === 'like') {
            response.where.push([key, '>=', searchCriteria[key][criteria]]);
          } else if (criteria === 'range') {
            const range = searchCriteria[key][criteria];
            response.where.push([key, '>=', range.start]);
            response.where.push([key, '<=', range.end]);
          } else {
            response.where.push([key, criteria, searchCriteria[key][criteria]]);
          }
        }
      }
    }
  }

  return response;
};
