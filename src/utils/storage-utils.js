export const renameFile = (fs, path, filePath, fileName) => {
  const fileDir = path.dirname(filePath);
  const newFilePath = path.join(fileDir, fileName);

  fs.renameSync(filePath, newFilePath);

  return newFilePath;
};

export const uploadToStorage = async (path, storage, data) => {
  const gcs = new storage({
    projectId: 'sample-188918',
    keyFilename: 'service-account.json'
  });
  const bucket = gcs.bucket('sample-188918.appspot.com');
  let uploadedFiles = [];
  let uploadResponse = null;
  let uploadSignerUrl = null;

  for (const file of data.files) {
    uploadResponse = await bucket.upload(file, {
      destination: `${data.bucketPath}/${path.basename(file)}`
    });

    uploadSignerUrl = await uploadResponse[0].getSignedUrl({
      action: 'read',
      expires: '01-01-3000'
    });

    uploadedFiles.push(uploadSignerUrl[0]);
  }

  return uploadedFiles;
};
