export const getCatalogModel = (associations, appLanguages) => {
  return [
    {
      property: 'active',
      type: 'boolean',
      rules: [{ required: true }]
    },
    {
      property: 'createdAt',
      type: 'number',
      rules: [{ required: true }]
    },
    {
      property: 'name',
      type: 'string',
      rules: [{ required: true }]
    },
    {
      property: 'order',
      type: 'number',
      rules: [{ required: true }]
    },
    {
      property: 'parent',
      type: 'string',
      rules: [{ association: associations.parents }]
    },
    {
      property: 'thumbnail',
      type: 'string',
      rules: [{ format: 'url' }]
    },
    {
      property: 'updatedAt',
      type: 'number'
    },
    {
      property: 'value',
      type: 'object',
      rules: [{ required: true }],
      validations: appLanguages.map(key => {
        return {
          property: key,
          type: 'string'
        };
      })
    }
  ];
};

export const getConfigurationModel = () => {
  return [
    {
      property: 'appDisabled',
      type: 'boolean',
      rules: [{ required: true }]
    },
    {
      property: 'appDisabledMessage',
      type: 'string',
      rules: [{ required: true }]
    },
    {
      property: 'appLastSync',
      type: 'number',
      rules: [{ required: true }]
    }
  ];
};

export const getLocaleModel = (appLanguages) => {
  return [
    {
      property: 'active',
      type: 'boolean',
      rules: [{ required: true }]
    },
    {
      property: 'createdAt',
      type: 'number',
      rules: [{ required: true }]
    },
    {
      property: 'name',
      type: 'string',
      rules: [{ required: true }]
    },
    {
      property: 'updatedAt',
      type: 'number'
    },
    {
      property: 'value',
      type: 'object',
      rules: [{ required: true }],
      validations: appLanguages.map(key => {
        return {
          property: key,
          type: 'string'
        };
      })
    }
  ];
};

export const getUserModel = (userData, passwordIsRequired) => {
  return [
    {
      property: 'active',
      type: 'boolean',
      rules: [{ required: true }]
    },
    {
      property: 'createdAt',
      type: 'number',
      rules: [{ required: true }]
    },
    {
      property: 'customerCodePayment',
      type: 'string'
    },
    {
      property: 'displayName',
      type: 'string'
    },
    {
      property: 'email',
      type: 'string',
      rules: [{ required: true }, { format: 'email' }]
    },
    {
      property: 'password',
      type: 'string',
      rules: [{ required: passwordIsRequired }, { minLength: 6 }]
    },
    {
      property: 'passwordConfirmation',
      type: 'string',
      rules: [{ required: passwordIsRequired }, { equalTo: userData.password }]
    },
    {
      property: 'preferences',
      type: 'object',
      validations: [
        {
          property: 'currency',
          type: 'string'
        },
        {
          property: 'dateFormat',
          type: 'string'
        },
        {
          property: 'language',
          type: 'string'
        }
      ]
    },
    {
      property: 'updatedAt',
      type: 'number'
    }
  ];
};

export const getReportModel = () => {
  return [
    {
      property: 'active',
      type: 'boolean',
      rules: [{ required: false }, { default: false }]
    },
    {
      property: 'createdAt',
      type: 'number',
      rules: [{ required: true }]
    },
    {
      property: 'names',
      type: 'string',
      rules: [{ required: true }]
    },
    {
      property: 'lastNames',
      type: 'string',
      rules: [{ required: true }]
    },
    {
      property: 'email',
      type: 'string',
      rules: [{ required: true }, { format: 'email' }]
    },
    {
      property: 'phone',
      type: 'string',
      rules: [{ required: true }, { minLength: 10 }]
    },
    {
      property: 'department',
      type: 'string',
      rules: [{ required: true }]
    },
    {
      property: 'city',
      type: 'string',
      rules: [{ required: true }]
    },
    {
      property: 'address',
      type: 'string',
    },
    {
      property: 'description',
      type: 'string',
      rules: [{ required: true }]
    },
    {
      property: 'checkSendEmail',
      type: 'string'
    }
  ];
};