import { applyMiddleware, createStore, combineReducers } from 'redux';
// middlewares
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import requestMiddleware from './middlewares/request-middleware';
import thunkMiddleware from './middlewares/thunk-middleware';
// reducers
import catalogReducer from './reducers/catalog-reducer';
import configReducer from './reducers/config-reducer';
import homeReducer from './reducers/home-reducer';
import localeReducer from './reducers/locale-reducer';
import userReducer from './reducers/user-reducer';
let initialState = {};

if (typeof window !== 'undefined') {
  initialState = window.initialState || initialState;
}

const reducers = combineReducers({
  catalog: catalogReducer,
  config: configReducer,
  home: homeReducer,
  locale: localeReducer,
  user: userReducer
});

const middleware = composeWithDevTools(applyMiddleware(requestMiddleware, thunkMiddleware));
const store = createStore(reducers, initialState, middleware);

export default store;
