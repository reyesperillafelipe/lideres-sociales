import axios from 'axios';

const request = store => next => action => {
  const { config, user } = store.getState();
  const { preferences } = config;
  delete axios.defaults.headers.common.authorization;

  if (user.token) {
    axios.defaults.headers.common['authorization'] = `Bearer ${user.token}`;
  }

  if (preferences.currency) {
    axios.defaults.headers.common['accept-currency'] = preferences.currency;
  }

  if (preferences.language) {
    axios.defaults.headers.common['accept-language'] = preferences.language;
  }

  return next(action);
};

export default request;
