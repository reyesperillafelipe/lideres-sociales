import { TYPES } from '../actions/config-actions';

const getTranslations = state => {
  return state.intlData.locales[state.preferences.language] || {};
};

const storageBucket = 'https://firebasestorage.googleapis.com/v0/b/lideres-sociales-bd152.appspot.com/o';

const initialState = {
  appDisabled: false,
  appDisabledMessage: '',
  appLanguages: ['en', 'es'],
  appLastSync: null,
  appLogo: `${storageBucket}/palomita.png?alt=media&token=21ad61e8-8e69-4dc7-9303-b92b32a3a8d4`,
  appName: 'Lideres Sociales',
  appUrl: 'https://react.com',
  catalogs: {
    planFeatures: 'Olfm2WjrW40DZMAFdLrZ',
    developersDocs: 'vzVa29WTqL1cfhwWbo6C',
    faqInfo: 'Naq9GQBXYHFY7xXfH06l',
    privacyInfo: 'R5Uola6kIV99td57mIgF',
    termsInfo: 'Zn0E6zs2B9Tny7y4T1BO'
  },
  intlData: {
    callingCodes: [],
    countries: [],
    currencies: [],
    currencyConversions: {},
    languages: [],
    locales: {}
  },
  emails: {
    noreply: 'noreply@mail.com',
    support: 'sample@mail.com'
  },
  plans: {
    free: 'tCfrWhFSY8aPv4bix6pV',
    standard: 'nZJF2RyrN9GXsiWD7iOb',
    premium: 'vKTHJhFqXGp4KkBaBPWG'
  },
  preferences: {
    currency: 'usd',
    dateFormat: 'DD/MM/YYYY',
    language: 'en'
  },
  translations: {}
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    default:
      return state;

    case TYPES.GET_CURRENCY_CONVERSIONS:
      return {
        ...state,
        temp: action.payload
      };

    case TYPES.SET_CONFIGURATION: {
      const newState = Object.assign(state, action.payload);
      return {
        ...newState,
        translations: getTranslations(newState)
      };
    }

    case TYPES.SET_CURRENCY_CONVERSIONS:
      return {
        ...state,
        intlData: {
          ...state.intlData,
          currencyConversions: action.payload
        }
      };

    case TYPES.SET_PREFERENCES:
      return {
        ...state,
        preferences: action.payload,
        translations: getTranslations(state)
      };

  }
}
