import { TYPES } from '../actions/locale-actions'; 

const initialState = {
  records: [],
  temp: null
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    default:
      return state;

    case TYPES.GET_LOCALES:
      return {
        ...state,
        records: action.payload
      };

    case TYPES.GET_LOCALE:
      return {
        ...state,
        temp: action.payload
      };

    case TYPES.POST_LOCALE:
      return {
        ...state,
        records: state.records.concat(action.payload),
        temp: action.payload
      };

    case TYPES.PATCH_LOCALE:
      return {
        ...state,
        records: state.records.map(item => (item.id === action.payload.id ? action.payload : item)),
        temp: action.payload
      };

    case TYPES.DELETE_LOCALE:
      return {
        ...state,
        records: state.records.filter(item => item.id !== action.payload.id)
      };
      
  }
}
