import { TYPES } from '../actions/catalog-actions';

const initialState = {
  records: [],
  temp: null
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    default:
      return state;

    case TYPES.GET_CATALOGS:
      return {
        ...state,
        records: action.payload
      };

    case TYPES.GET_CATALOG:
      return {
        ...state,
        temp: action.payload
      };

    case TYPES.POST_CATALOG:
      return {
        ...state,
        records: state.records.concat(action.payload),
        temp: action.payload
      };

    case TYPES.PATCH_CATALOG:
      return {
        ...state,
        records: state.records.map(item => (item.id === action.payload.id ? action.payload : item)),
        temp: action.payload
      };
  }
}