import { TYPES } from '../actions/home-actions';
 
const initialState = {
  developersInfo: [],
  faqInfo: {},
  planFeatures: [],
  privacyInfo: {},
  termsInfo: {}
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    default:
      return state;

    case TYPES.GET_DEVELOPERS_INFO:
      return {
        ...state,
        developersInfo: action.payload
      };

    case TYPES.GET_FAQ_INFO:
      return {
        ...state,
        faqInfo: action.payload
      };

    case TYPES.GET_PLAN_FEATURES:
      return {
        ...state,
        planFeatures: action.payload
      };

    case TYPES.GET_PRIVACY_INFO:
      return {
        ...state,
        privacyInfo: action.payload
      };

    case TYPES.GET_TERMS_INFO:
      return {
        ...state,
        termsInfo: action.payload
      };
  }
}
