import { TYPES } from '../actions/user-actions';

const initialState = {
  auth: null,
  authLoaded: false,
  backup: null,
  claims: [],
  customerInfo: null,
  customerReceipts: [],
  records: [],
  temp: null,
  token: null
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    default:
      return state;

    case TYPES.GET_BACKUP:
      return {
        ...state,
        backup: action.payload
      };

    case TYPES.GET_USERS:
      return {
        ...state,
        records: action.payload
      };

    case TYPES.GET_USER:
      return {
        ...state,
        temp: action.payload
      };
    
    case TYPES.GET_CUSTOMER_INFO:
      return {
        ...state,
        customerInfo: action.payload
      };

    case TYPES.GET_CUSTOMER_RECEIPTS:
      return {
        ...state,
        customerReceipts: action.payload
      };

    case TYPES.PATCH_USER:
      return {
        ...state,
        records: state.records.map(item => (item.id === action.payload.id ? action.payload : item)),
        temp: action.payload
      };

    case TYPES.CHANGE_PASSWORD:
      return {
        ...state,
        temp: action.payload
      };

    case TYPES.LOGIN:
      return {
        ...state,
        temp: action.payload.messsage,
        token: action.payload.token
      };

    case TYPES.LOGOUT:
      return {
        ...state,
        auth: null,
        token: null
      };

    case TYPES.REGISTER:
      return {
        ...state,
        temp: action.payload.messsage,
        token: action.payload.token
      };

    case TYPES.ME:
      return {
        ...state,
        auth: action.payload,
        authLoaded: true
      };

    case TYPES.RECOVER_PASSWORD:
      return {
        ...state,
        temp: action.payload
      };

    case TYPES.SET_TOKEN:
      return {
        ...state,
        token: action.payload
      };

    case TYPES.GET_USER_CLAIMS:
      return {
        ...state,
        claims: action.payload
      };

    case TYPES.POST_USER_CLAIM:
      return {
        ...state,
        claims: state.claims.concat(action.payload),
        temp: action.payload
      };

    case TYPES.DELETE_USER_CLAIM:
      return {
        ...state,
        claims: state.claims.filter(item => item !== action.payload),
        temp: action.payload
      };

    case TYPES.UPDATE_CREDIT_CARD:
      return {
        ...state,
        temp: action.payload
      };

    case TYPES.CREATE_SUBSCRIPTION:
      return {
        ...state,
        token: action.payload.token,
        temp: action.payload.message
      };
    
    case TYPES.CREATE_REPORT:
      return {
        ...state,
        // token: action.payload.token,
        temp: action.payload.message
      };
  }
}
