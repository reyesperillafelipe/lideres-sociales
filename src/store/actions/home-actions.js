import axios from 'axios';
import { getQueryParamsString } from '../../utils/request-utils';
const { API_URL } = process.env;

export const TYPES = {
  GET_DEVELOPERS_INFO: 'GET_DEVELOPERS_INFO',
  GET_FAQ_INFO: 'GET_FAQ_INFO',
  GET_PRIVACY_INFO: 'GET_PRIVACY_INFO',
  GET_PLAN_FEATURES: 'GET_PLAN_FEATURES',
  GET_TERMS_INFO: 'GET_TERMS_INFO'
};

export const getDevelopersInfo = () => async (dispatch, getState) => {
  const { config, home } = getState();
  const query = { 
    sort: { order: 'asc' },
    where: { parent: { '==': config.catalogs.developersDocs } }
  };
  const response = home.developersInfo.length
    ? home.developersInfo
    : (await axios.get(`${API_URL}/catalogs/${getQueryParamsString(query)}`)).data;

  dispatch({ type: TYPES.GET_DEVELOPERS_INFO, payload: response });
};

export const getFaqInfo = () => async (dispatch, getState) => {
  const { config, home } = getState();
  const response = Object.keys(home.faqInfo).length
    ? home.faqInfo
    : (await axios.get(`${API_URL}/catalogs/${config.catalogs.faqInfo}`)).data;

  dispatch({ type: TYPES.GET_FAQ_INFO, payload: response });
};

export const getPlanFeatures = () => async (dispatch, getState) => {
  const { config, home } = getState();
  const query = { 
    sort: { order: 'asc' },
    where: { parent: { '==': config.catalogs.planFeatures } }
  };
  const response = home.planFeatures.length
    ? home.planFeatures
    : (await axios.get(`${API_URL}/catalogs/${getQueryParamsString(query)}`)).data;

  dispatch({ type: TYPES.GET_PLAN_FEATURES, payload: response });
};

export const getPrivacyInfo = () => async (dispatch, getState) => {
  const { config, home } = getState();
  const response = Object.keys(home.privacyInfo).length
    ? home.privacyInfo
    : (await axios.get(`${API_URL}/catalogs/${config.catalogs.privacyInfo}`)).data;

  dispatch({ type: TYPES.GET_PRIVACY_INFO, payload: response });
};

export const getTermsInfo = () => async (dispatch, getState) => {
  const { config, home } = getState();
  const response = Object.keys(home.termsInfo).length
    ? home.termsInfo
    : (await axios.get(`${API_URL}/catalogs/${config.catalogs.termsInfo}`)).data;

  dispatch({ type: TYPES.GET_TERMS_INFO, payload: response });
};
