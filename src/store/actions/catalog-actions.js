import axios from 'axios';
import { getQueryParamsString } from '../../utils/request-utils';
const { API_URL } = process.env;

export const TYPES = {
  GET_CATALOGS: 'GET_CATALOGS',
  GET_CATALOG: 'GET_CATALOG',
  POST_CATALOG: 'POST_CATALOG',
  PATCH_CATALOG: 'PATCH_CATALOG'
};

export const getCatalogs = query => async (dispatch, getState) => {
  const { catalog } = getState();
  const response = catalog.records.length 
    ? catalog.records
    : (await axios.get(`${API_URL}/catalogs/${getQueryParamsString(query)}`)).data;

  dispatch({ type: TYPES.GET_CATALOGS, payload: response });
};

export const getCatalog = id => async dispatch => {
  const response = await axios.get(`${API_URL}/catalogs/${id}`);
  dispatch({ type: TYPES.GET_CATALOG, payload: response.data });
};

export const createCatalog = data => async dispatch => {
  const response = await axios.post(`${API_URL}/catalogs`, data);
  dispatch({ type: TYPES.POST_CATALOG, payload: response.data });
};

export const updateCatalog = data => async dispatch => {
  const response = await axios.patch(`${API_URL}/catalogs/${data.id}`, data);
  dispatch({ type: TYPES.PATCH_CATALOG, payload: response.data });
};