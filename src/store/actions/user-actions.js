import axios from 'axios';
import { getQueryParamsString } from '../../utils/request-utils';
const { API_URL } = process.env;

export const TYPES = {
  GET_BACKUP: 'GET_BACKUP',
  GET_USERS: 'GET_USERS',
  GET_USER: 'GET_USER',
  GET_CUSTOMER_INFO:'GET_CUSTOMER_INFO',
  GET_CUSTOMER_RECEIPTS: 'GET_CUSTOMER_RECEIPTS',
  PATCH_USER: 'PATCH_USER',
  CHANGE_PASSWORD: 'CHANGE_PASSWORD',
  LOGIN: 'LOGIN',
  LOGOUT: 'LOGOUT',
  REGISTER: 'REGISTER',
  ME: 'ME',
  RECOVER_PASSWORD: 'RECOVER_PASSWORD',
  SET_TOKEN: 'SET_TOKEN',
  GET_USER_CLAIMS: 'GET_USER_CLAIMS',
  POST_USER_CLAIM: 'POST_USER_CLAIM',
  DELETE_USER_CLAIM: 'DELETE_USER_CLAIM',
  UPDATE_CREDIT_CARD: 'UPDATE_CREDIT_CARD',
  CREATE_SUBSCRIPTION: 'CREATE_SUBSCRIPTION',
  UPDATE_SUBSCRIPTION: 'UPDATE_SUBSCRIPTION',
  CREATE_REPORT: 'CREATE_REPORT'
};

export const getBackupExcel = year => async (dispatch, getState) => {
  const { user } = getState();
  const response = user.backup
    ? user.backup
    : (await axios.get(`${API_URL}/users/backup/${year}`, { responseType: 'arraybuffer'})).data;
  
  dispatch({ type: TYPES.GET_BACKUP, payload: response });
};

export const getUsers = query => async (dispatch, getState) => {
  const { user } = getState();
  const response = user.records.length 
    ? user.records
    : (await axios.get(`${API_URL}/users/${getQueryParamsString(query)}`)).data;

  dispatch({ type: TYPES.GET_USERS, payload: response });
};

export const getUser = id => async dispatch => {
  const response = await axios.get(`${API_URL}/users/${id}`);
  dispatch({ type: TYPES.GET_USER, payload: response.data });
};

export const getCustomerInfo= userId => async dispatch => {
  const response = (await axios.get(`${API_URL}/users/${userId}/info`)).data;
  dispatch({ type: TYPES.GET_CUSTOMER_INFO, payload: response });
};

export const getCustomerReceipts = (id, query) => async (dispatch, getState) => {
  const { user } = getState();
  const response = user.customerReceipts.length > 0
    ? user.customerReceipts
    : (await axios.get(`${API_URL}/users/${id}/receipts/${getQueryParamsString(query)}`)).data;

  dispatch({ type: TYPES.GET_CUSTOMER_RECEIPTS, payload: response });
};

export const updateUser = data => async dispatch => {
  const response = await axios.patch(`${API_URL}/users/${data.id}`, data);
  dispatch({ type: TYPES.PATCH_USER, payload: response.data });
};

export const changePassword = data => async dispatch => {
  const response = await axios.post(`${API_URL}/users/change-password`, data);
  dispatch({ type: TYPES.CHANGE_PASSWORD, payload: response.data.messsage });
};

export const login = data => async dispatch => {
  const response = await axios.post(`${API_URL}/users/login`, data);
  dispatch({ type: TYPES.LOGIN, payload: response.data });
};

export const logout = () => async dispatch => {
  dispatch({ type: TYPES.LOGOUT, payload: null });
};

export const register = data => async dispatch => {
  const response = await axios.post(`${API_URL}/users/register`, data);
  dispatch({ type: TYPES.REGISTER, payload: response.data });
};

export const me = () => async dispatch => {
  try {
    let response = await axios.get(`${API_URL}/users/me`);
    dispatch({ type: TYPES.ME, payload: response.data });
  } catch (e) {
    dispatch({ type: TYPES.ME, payload: null });
  }
};

export const recoverPassword = data => async dispatch => {
  const response = await axios.post(`${API_URL}/users/recover-password`, data);
  dispatch({ type: TYPES.RECOVER_PASSWORD, payload: response.data });
};

export const setToken = token => async dispatch => {
  localStorage.token = token;
  dispatch({ type: TYPES.SET_TOKEN, payload: token });
};

export const getUserClaims = id => async dispatch => {
  const response = await axios.get(`${API_URL}/users/${id}/claims`);
  dispatch({ type: TYPES.GET_USER_CLAIMS, payload: response.data });
};

export const createUserClaim = data => async dispatch => {
  const response = await axios.post(`${API_URL}/users/${data.id}/claims`, data);
  dispatch({ type: TYPES.POST_USER_CLAIM, payload: response.data });
};

export const deleteUserClaim = data => async dispatch => {
  const response = await axios.patch(`${API_URL}/users/${data.id}/claims`, data);
  dispatch({ type: TYPES.DELETE_USER_CLAIM, payload: response.data });
};

export const updateCreditCard = (customerCode, creditCard) => async dispatch => {
  const response = await axios.post(`${API_URL}/users/${customerCode}/update-credit-card`, creditCard);
  dispatch({ type: TYPES.UPDATE_CREDIT_CARD, payload: response.data });
};

export const createSubscription = data => async dispatch => {
  let response = await axios.post(`${API_URL}/users/create-subscription`, data);
  dispatch({ type: TYPES.CREATE_SUBSCRIPTION, payload: response.data });
};

export const updateSubscription = (customerCode, data) => async dispatch => {
  const response = await axios.post(`${API_URL}/users/${customerCode}/update-subscription`,data);
  dispatch({ type: TYPES.UPDATE_CREDIT_CARD, payload: response.data });
};

export const createReport = data => async dispatch => {
  let response = await axios.post(`${API_URL}/socialLeader/report`, data);
  dispatch({ type: TYPES.CREATE_REPORT, payload: response.data });
};