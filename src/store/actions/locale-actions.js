import axios from 'axios';
import { getQueryParamsString } from '../../utils/request-utils';
const { API_URL } = process.env;

export const TYPES = {
  GET_LOCALES: 'GET_LOCALES',
  GET_LOCALE: 'GET_LOCALE',
  POST_LOCALE: 'POST_LOCALE',
  PATCH_LOCALE: 'PATCH_LOCALE',
  DELETE_LOCALE: 'DELETE_LOCALE'
};

export const getLocales = query => async (dispatch, getState) => {
  const { locale } = getState();
  const response = locale.records.length 
    ? locale.records
    : (await axios.get(`${API_URL}/locales/${getQueryParamsString(query)}`)).data;

  dispatch({ type: TYPES.GET_LOCALES, payload: response });
};

export const getLocale = id => async dispatch => {
  const response = await axios.get(`${API_URL}/locales/${id}`);
  dispatch({ type: TYPES.GET_LOCALE, payload: response.data });
};

export const createLocale = data => async dispatch => {
  const response = await axios.post(`${API_URL}/locales`, data);
  dispatch({ type: TYPES.POST_LOCALE, payload: response.data });
};

export const updateLocale = data => async dispatch => {
  const response = await axios.patch(`${API_URL}/locales/${data.id}`, data);
  dispatch({ type: TYPES.PATCH_LOCALE, payload: response.data });
};

export const deleteLocale = id => async dispatch => {
  const response = await axios.delete(`${API_URL}/locales/${id}`);
  dispatch({ type: TYPES.DELETE_LOCALE, payload: response.data });
};
