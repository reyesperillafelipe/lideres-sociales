import axios from 'axios';
const { API_URL } = process.env;

export const TYPES = {
  GET_CURRENCY_CONVERSIONS: 'GET_CURRENCY_CONVERSIONS',
  SET_CONFIGURATION: 'SET_CONFIGURATION',
  SET_CURRENCY_CONVERSIONS: 'SET_CURRENCY_CONVERSIONS',
  SET_PREFERENCES: 'SET_PREFERENCES'
};

export const getCurrencyConversions = data => async dispatch => {
  const { currency, date } = data;
  const response = await axios.get(`${API_URL}/config/currency-conversions/${currency}/${date}`);
  dispatch({ type: TYPES.GET_CURRENCY_CONVERSIONS, payload: response.data });
};

export const setConfiguration = data => async dispatch => {
  dispatch({ type: TYPES.SET_CONFIGURATION, payload: data });
};

export const setCurrencyConversions = data => async dispatch => {
  dispatch({ type: TYPES.SET_CURRENCY_CONVERSIONS, payload: data });
};

export const setPreferences = data => async (dispatch, getState) => {
  const response = Object.assign(getState().config.preferences, data);
  dispatch({ type: TYPES.SET_PREFERENCES, payload: response });
};

export const syncConfiguration = data => async dispatch => {
  const response = await axios.post(`${API_URL}/config/sync`, data);
  dispatch({ type: TYPES.SET_CONFIGURATION, payload: response.data });
};