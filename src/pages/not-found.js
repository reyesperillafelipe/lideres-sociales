import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import Header from './_components/header';
import Footer from './_components/footer';
import { translate } from '../utils/intl-utils';

const NotFoundWrapper = styled.main`
  &, * {
    box-sizing: border-box;
  }
`;
class NotFound extends React.Component {
  render() {
    const { config, user } = this.props;

    return (
      <React.Fragment>
        <Header
          config={config}
          user={user}
          title={translate(config.translations, 'notFoundTitle')}
          description={translate(config.translations, 'notFoundDescription')}
          history={this.props.history}
          showHeading={true} />
        <NotFoundWrapper className="container text-center">
          <h1><i className="fas fa-unlink fa-2x" /></h1>
          <p>{translate(config.translations, 'notFoundDescription')}</p>
          <p><Link to="/">{translate(config.translations, 'goHomePage')}</Link></p>
        </NotFoundWrapper>
        <Footer 
          config={config}
          routerHistory={this.props.history} />
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  config: state.config,
  user: state.user
});

export default connect(mapStateToProps)(NotFound);
