import React from 'react';
import { withRouter } from 'react-router-dom';
import Context from '../context';
import { me, setToken } from '../store/actions/user-actions';
import store from '../store/store';

class Main extends React.Component {
  static contextType = Context;

  componentDidMount = () => {
    store.dispatch(setToken(localStorage.token));
    store.dispatch(me())
      .finally(() => this.context.loadPreferences()); 
  } 

  render() {
    return (
      <React.Fragment>
        {this.props.children}
      </React.Fragment>
    );
  }
}

export default withRouter(Main);
