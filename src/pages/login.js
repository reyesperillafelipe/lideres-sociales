import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import Header from './_components/header';
import Footer from './_components/footer';
import store from '../store/store';
import { login, me, setToken } from '../store/actions/user-actions';
import { translate } from '../utils/intl-utils';
import { getQueryParamsJson } from '../utils/request-utils';
import { getFrontendError } from '../utils/response-utils';

const LoginWrapper = styled.main`
  &, * {
    box-sizing: border-box;
  }
`;

class Login extends React.Component {
  state = {
    isLoading: false,
    errors: {},
    model: {
      email: '',
      password: ''
    }
  };

  componentDidUpdate = async () => {
    if (this.props.auth) {
      this.props.history.push('/app/dashboard');
    }
  };

  login = async e => {
    try {
      if (e) {
        e.preventDefault();
      }

      this.setState({ isLoading: true, errors: {} });
      await store.dispatch(login(this.state.model));
      await store.dispatch(setToken(this.props.user.token));
      await store.dispatch(me());
      this.setState({ isLoading: false });
    } catch (e) {
      const { errors, message } = getFrontendError(e);
      this.setState({ isLoading: false, errors: {...errors, general: message } });
    }
  };

  render() {
    const { isLoading } = this.state;
    const { config, user } = this.props;
    const queryParams = getQueryParamsJson(this.props.location.search);

    return (
      <React.Fragment>
        {
          queryParams.message &&
          <div className="alert alert-info m-0">
            {queryParams.message}
            <button className="close" aria-label="Close" onClick={() => this.props.history.push(this.props.location.pathname)}>
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        }
        <Header
          config={config}
          user={user}
          title={translate(config.translations, 'loginTitle')}
          description={translate(config.translations, 'loginDescription')}
          history={this.props.history}
          showHeading={true} />
        <LoginWrapper className="container">
          <div className="row">
            <div className="col-md-12">
              <div className="alert alert-info" role="alert">
                <p className="mb-0">
                  <Link to="/register">{translate(config.translations, 'haveNotAccount')}</Link>
                </p>
                <p className="mb-0">
                  <Link to="/recover-password">{translate(config.translations, 'forgotPassword')}</Link>
                </p>
              </div>
              <div className="alert alert-warning" role="alert">
                {translate(config.translations, 'requiredFields')}
              </div>
              {
                this.state.errors.general &&
                <div className="alert alert-danger" role="alert">
                  {this.state.errors.general}
                </div>
              }
              <form onSubmit={this.login}>
                <div className="form-group">
                  <label>
                    {translate(config.translations, 'email')} <span>*</span>
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    value={this.state.model.email}
                    onChange={e =>
                      this.setState({ model: Object.assign(this.state.model, { email: (e.target.value || '')
                          .replace(/\s/g, '')
                        }) 
                      })
                    } />
                  <span className="text-danger">{this.state.errors.email}</span>
                </div>
                <div className="form-group">
                  <label>
                    {translate(config.translations, 'password')} <span>*</span>
                  </label>
                  <input
                    type="password"
                    className="form-control"
                    value={this.state.model.password}
                    onChange={e => this.setState({ model: Object.assign(this.state.model, { password: e.target.value }) })} />
                  <span className="text-danger">
                    {this.state.errors.password}
                  </span>
                </div>
                <div className="form-group text-right">
                  <button
                    type="submit"
                    className="btn btn-primary"
                    disabled={isLoading}>
                    {translate(config.translations, 'login')}
                  </button>
                </div>
              </form>
            </div>
          </div>
        </LoginWrapper>
        <Footer 
          config={config}
          routerHistory={this.props.history} />
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.user.auth,
  config: state.config,
  user: state.user
});

export default connect(mapStateToProps)(Login);
