import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { formatDate } from '../../../utils/intl-utils';
import Header from '../../_components/header';
import Pager from '../../_components/pager';
import store from '../../../store/store';
import { getCatalog, getCatalogs, updateCatalog } from '../../../store/actions/catalog-actions';

const CatalogWrapper = styled.main`
  &, * {
    box-sizing: border-box;
  }

  .nav-tabs {
    .nav-link {
      cursor: pointer;
    }
  }
`;

class CatalogList extends React.Component {
  state = {
    isLoading: false,
    parentCatalog: null,
    query: {
      limit: 10,
      sort: { name: 'asc' },
      where: {
        active: { '==': true },
        name: { like: '' }
      }
    }
  };

  componentDidMount = () => {
    const parent = (this.props.match.params.id || null);
    this.serverSearch(this.state.query);

    store.dispatch(getCatalog(parent))
      .then(() => this.setState({ parentCatalog: this.props.catalog.temp }));
  };

  clientSearch = (query, catalogs) => {
    const active = query.where.active['=='];
    const name = query.where.name.like.toLowerCase();
    const parent = this.props.match.params.id;
    let response = catalogs.filter(item => item.active === active);

    if (name) {
      response = response.filter(item => item.name.toLowerCase().includes(name));
    }
    
    if (parent) {
      response = response.filter(item => item.parent === parent);
    } else {
      response = response.filter(item => !item.parent);
    }

    return response;
  }

  serverSearch = async query => {
    try {
      this.setState({ isLoading: true, query });
      const serverQuery = { sort: query.sort };
      await store.dispatch(getCatalogs(serverQuery));
      this.setState({ isLoading: false });
    } catch (e) {
      console.error(e);
      this.setState({ isLoading: false });
    }
  };

  delete = async item => {
    try {
      this.setState({ isLoading: true });
      await store.dispatch(updateCatalog({ id: item.id, active: false }));
      this.setState({ isLoading: false });
    } catch (e) {
      console.error(e);
      this.setState({ isLoading: false });
    }
  };

  restore = async item => {
    try {
      this.setState({ isLoading: true });
      await store.dispatch(updateCatalog({ id: item.id, active: true }));
      this.setState({ isLoading: false });
    } catch (e) {
      console.error(e);
      this.setState({ isLoading: false });
    }
  };

  render() {
    const redirectUrl = (this.state.parentCatalog && this.state.parentCatalog.parent)
      ? `/admin/catalogs/${this.state.parentCatalog.parent}/children`
      : '/admin/catalogs';
    const { isLoading, parentCatalog, query } = this.state;
    const { catalogs, config, user } = this.props;
    const filteredCatalogs = this.clientSearch(query, catalogs);
    const isActiveTab = (query.where.active['=='] === true);

    return (
      <React.Fragment>
        <Header
          config={config}
          user={user}
          title="Catalogs"
          description={(parentCatalog && parentCatalog.name) || 'All'}
          history={this.props.history}
          showHeading={true}
          btnLeft={
            parentCatalog && 
            <button
              className="btn"
              onClick={() => this.props.history.push(redirectUrl)}>
              <i className="fas fa-arrow-left" />
            </button>
          }
          btnRight={
            <button
              className="btn"
              onClick={() => this.props.history.push('/admin/catalogs/new')}>
              <i className="fas fa-plus" />
            </button>
          } />
        <CatalogWrapper className="container">
          <ul className="nav nav-tabs mb-4">
            <li className="nav-item">
              <a 
                className={`nav-link ${isActiveTab && 'active'}`}
                onClick={() => 
                  this.setState({ 
                    query: Object.assign(this.state.query, { 
                      where: Object.assign(this.state.query.where, { active: { '==': true } }) 
                    })
                  })
                }>
                Active
              </a>
            </li>
            <li className="nav-item">
              <a 
                className={`nav-link ${!isActiveTab && 'active'}`}
                onClick={() => 
                  this.setState({ 
                    query: Object.assign(this.state.query, { 
                      where: Object.assign(this.state.query.where, { active: { '==': false } }) 
                    })
                  })
                }>
                Inactive
              </a>
            </li>
          </ul>
          <Pager
            isLoading={isLoading}
            config={config}
            query={query}
            items={filteredCatalogs}
            onChange={query => this.serverSearch(query)}>
            <ul className="list-group list-group-flush">
              {filteredCatalogs
                .sort((a, b) => a.order - b.order)
                .map(item => (
                  <li
                    className="list-group-item list-group-item-action d-flex justify-content-between align-items-center"
                    key={item.id}>
                    <div>
                      <p className="mb-0">{item.name}</p>
                      <small>{formatDate(item.createdAt, config.preferences.dateFormat)}</small>
                    </div>
                    {isActiveTab && (
                      <div className="btn-group">
                        <Link to={`/admin/catalogs/${item.id}`} className="btn">
                          <i className="fas fa-pen" />
                        </Link>
                        {!parentCatalog && (
                          <Link
                            to={`/admin/catalogs/${item.id}/children`}
                            className="btn"
                          >
                            <i className="fas fa-list" />
                          </Link>
                        )}
                        <button className="btn" onClick={() => this.delete(item)}>
                          <i className="fas fa-trash" />
                        </button>
                      </div>
                    )}
                    {!isActiveTab && (
                      <div>
                        <button className="btn" onClick={() => this.restore(item)}>
                          <i className="fas fa-undo" />
                        </button>
                      </div>
                    )}
                  </li>
                ))}
            </ul>
          </Pager>
        </CatalogWrapper>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  catalog: state.catalog,
  catalogs: state.catalog.records,
  config: state.config,
  user: state.user
});

export default connect(mapStateToProps)(CatalogList);
