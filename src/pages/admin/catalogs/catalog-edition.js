import React from 'react';
import { connect } from 'react-redux';
import Select from 'react-select';
import styled from 'styled-components';
import Counter from '../../_components/counter';
import Header from '../../_components/header';
import Multilanguage from '../../_components/multilanguage';
import store from '../../../store/store';
import {
  getCatalog, getCatalogs,
  createCatalog, updateCatalog
} from '../../../store/actions/catalog-actions';
import { translate } from '../../../utils/intl-utils';
import { getFrontendError } from '../../../utils/response-utils';

const CatalogEditionWrapper = styled.main`
  &, * {
    box-sizing: border-box;
  }
`;
class CatalogEdition extends React.Component {
  state = {
    errors: {},
    model: {
      active: true,
      parent: null,
      name: '',
      value: {},
      thumbnail: '',
      order: 1
    }
  };

  componentDidMount = () => {
    store.dispatch(
      getCatalogs({
        limit: 500,
        sort: { name: 'asc' },
        where: {
          parent: { '>=': '' }
        }
      })
    );
    store.dispatch(getCatalog(this.props.match.params.id)).then(() => {
      this.setState({
        isLoading: false,
        model: Object.assign(this.state.model, this.props.catalog.temp)
      });
    });
  };

  submit = async e => {
    try {
      if (e) {
        e.preventDefault();
      }

      this.setState({ isLoading: true, errors: {} });

      if (!this.state.model.id) {
        await store.dispatch(createCatalog(this.state.model));
      } else {
        await store.dispatch(updateCatalog(this.state.model));
      }

      const redirectUrl = this.state.model.parent
        ? `/admin/catalogs/${this.state.model.parent}/children`
        : '/admin/catalogs';

      this.setState({ isLoading: false });
      this.props.history.push(redirectUrl);
    } catch (e) {
      const { errors, message } = getFrontendError(e);
      this.setState({ isLoading: false, errors: {...errors, general: message } });
    }
  };

  render() {
    const redirectUrl = this.state.model.parent
      ? `/admin/catalogs/${this.state.model.parent}/children`
      : '/admin/catalogs';
    const { config, catalogs, user } = this.props;

    return (
      <React.Fragment>
        <Header
          config={config}
          user={user}
          title={translate(config.translations, 'catalogEditionTitle')}
          description={translate(config.translations, 'catalogEditionDescription')}
          history={this.props.history}
          showHeading={true} 
          btnLeft={
            <button
              className="btn"
              onClick={() => this.props.history.push(redirectUrl)}>
              <i className="fas fa-arrow-left" />
            </button>
          }
          btnRight={
            <button className="btn" onClick={this.submit}>
              <i className="fas fa-save" />
            </button>
          } />
        <CatalogEditionWrapper className="container">            
          <div className="alert alert-warning" role="alert">
            {translate(config.translations, 'requiredFields')}
          </div>
          {
            this.state.errors.general &&
            <div className="alert alert-danger" role="alert">
              {this.state.errors.general}
            </div>
          }
          <form className="row" onSubmit={this.submit}>
            <div className="form-group col-md-6">
              <label>Parent Catalog</label>
              <Select
                isClearable={true}
                options={catalogs
                  .filter(item => !item.parent)
                  .sort((a, b) => a.name.localeCompare(b.name))
                }
                value={catalogs.find(item => item.id === (this.state.model.parent && this.state.model.parent.id))}
                getOptionLabel={option => option.name}
                getOptionValue={option => option.id}
                onChange={option =>
                  this.setState({ model: Object.assign(this.state.model, { parent: (option ? option.id : option) })})
                } />
              <span className="text-danger">{this.state.errors.parent}</span>
            </div>
            <div className="form-group col-md-6">
              <label>Name *</label>
              <div className="input-group">
                <div className="input-group-prepend">
                  <span className="input-group-text">
                    <i className="fas fa-question-circle" title={translate(config.translations, 'addLocaleCodeForMultilanguage')} />
                  </span>
                </div>
                <input
                  type="text"
                  className="form-control"
                  value={this.state.model.name}
                  onChange={e => this.setState({ model: Object.assign(this.state.model, { name: e.target.value })})} />
              </div>
              <span className="text-danger">{this.state.errors.name}</span>
            </div>
            <div className="form-group col-md-6">
              <label>Thumbnail</label>
              <input
                type="text"
                className="form-control"
                value={this.state.model.thumbnail}
                onChange={e => this.setState({ model: Object.assign(this.state.model, { thumbnail: e.target.value })})} />
              <span className="text-danger">{this.state.errors.thumbnail}</span>
            </div>
            <div className="form-group col-md-6">
              <label>Order *</label>
              <Counter
                value={this.state.model.order}
                min={1}
                max={100}
                onChange={value => this.setState({ model: Object.assign(this.state.model, { order: value })})} />
              <span className="text-danger">{this.state.errors.order}</span>
            </div>
            <div className="form-group col-md-12">
              <label>Value *</label>
              <Multilanguage
                languages={config.appLanguages}
                value={this.state.model.value}
                onChange={value => this.setState({ model: Object.assign(this.state.model, { value })})} />
              <span className="text-danger">{this.state.errors.value}</span>
            </div>
            <button type="submit" className="d-none" />
          </form>
        </CatalogEditionWrapper>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  catalog: state.catalog,
  catalogs: state.catalog.records,
  config: state.config,
  user: state.user
});

export default connect(mapStateToProps)(CatalogEdition);
