import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import Header from '../../_components/header';
import UserClaimEdition from './components/user-claim-edition';
import UserClaimList from './components/user-claim-list';
import Modal from '../../_components/modal';
import { translate } from '../../../utils/intl-utils';
import { getFrontendError } from '../../../utils/response-utils';
import store from '../../../store/store';
import { getUserClaims, createUserClaim, deleteUserClaim, getUser } from '../../../store/actions/user-actions';

const UserEditionWrapper = styled.main`
  &, * {
    box-sizing: border-box;
  }
`;

class UserEdition extends React.Component {
  state = {
    isLoading: false,
    errors: {},
    userClaimEdition: '',
    showModalClaimEdition: false,
    model: {
      id: '',
      email: ''
    }
  };

  componentDidMount = () => {
    const userId = this.props.match.params.id;
    store.dispatch(getUserClaims(userId));
    store.dispatch(getUser(userId))
      .then(() => {
        const { user } = this.props;

        this.setState({
          model: Object.assign(this.state.model, {
            id: (user.temp && user.temp.id),
            email: (user.temp && user.temp.email)
          })
        });
      });
  };

  openClaimEdition = claim => {
    this.setState({
      userClaimEdition: claim,
      showModalClaimEdition: true
    });
  }

  closeClaimEdition = () => {
    this.setState({
      userClaimEdition: '',
      showModalClaimEdition: false
    });
  };

  saveClaim = async claim => {
    try {
      this.setState({ isLoading: true, errors: {} });
      const data = {
        id: this.state.model.id,
        claim: claim
      };
      await store.dispatch(createUserClaim(data));
      this.closeClaimEdition();
      this.setState({ isLoading: false });
    } catch (e) {
      const { message, errors } = getFrontendError(e);
      this.setState({ isLoading: false, errors: {...errors, general: message } });
    }
  };

  deleteClaim = async claim => {
    try {
      this.setState({ isLoading: true });
      const data = {
        id: this.state.model.id,
        claim: claim
      };
      await store.dispatch(deleteUserClaim(data));
      this.setState({ isLoading: false });
    } catch (e) {
      console.error(e);
      this.setState({ isLoading: false });
    }
  };

  render() {
    const { model, showModalClaimEdition } = this.state;
    const { config, claims, user } = this.props;

    return (
      <React.Fragment>
        <Header
          config={config}
          user={user}
          title={translate(config.translations, 'userEditionTitle')}
          description={translate(config.translations, 'userEditionDescription')}
          history={this.props.history}
          showHeading={true} 
          btnLeft={
            <button
              className="btn"
              onClick={() => this.props.history.push('/admin/users')}>
              <i className="fas fa-arrow-left" />
            </button>
          }
          btnRight={
            <button className="btn" onClick={this.submit}>
              <i className="fas fa-save" />
            </button>
          } />
        <UserEditionWrapper className="container">
          <div className="alert alert-warning" role="alert">
            {translate(config.translations, 'requiredFields')}
          </div>
          {
            this.state.errors.general &&
            <div className="alert alert-danger" role="alert">
              {this.state.errors.general}
            </div>
          }
          <div className="row">
            <div className="form-group col-md-12">
              <label>{translate(config.translations, 'email')}</label>
              <input type="text" className="form-control" disabled={true} value={`${model.email}`} />
            </div>
          </div>
          <div className="card">
            <div className="card-header d-flex align-items-center">
              <i className="fas fa-lock mr-2" /> {translate(config.translations, 'userClaims')}
              <span className="ml-auto">
                <button className="btn" onClick={() => this.openClaimEdition('')}>
                  <i className="fas fa-plus"></i>
                </button>
              </span>
            </div>
            <div className="card-body">
              <UserClaimList 
                claims={claims}
                onDelete={this.deleteClaim}>
              </UserClaimList>
            </div>
          </div>
          <Modal 
            isOpen={showModalClaimEdition} 
            title={translate(config.translations, 'saveUserClaim')}
            onToggle={this.closeClaimEdition}>
            <UserClaimEdition 
              config={config}
              claim={this.state.userClaimEdition}
              claims={claims}
              onClose={this.closeClaimEdition}
              onSave={this.saveClaim}>
            </UserClaimEdition>
          </Modal>
        </UserEditionWrapper>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  config: state.config,
  claims: state.user.claims,
  user: state.user
});

export default connect(mapStateToProps)(UserEdition);
