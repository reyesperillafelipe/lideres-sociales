import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import Header from '../../_components/header';
import { Link } from 'react-router-dom';
import Pager from '../../_components/pager';
import store from '../../../store/store';
import { getUsers, updateUser } from '../../../store/actions/user-actions';
import { formatDate, translate } from '../../../utils/intl-utils';

const UserWrapper = styled.main`
  &, * {
    box-sizing: border-box;
  }

  .nav-tabs {
    .nav-link {
      cursor: pointer;
    }
  }
`;

class UserList extends React.Component {
  state = {
    isLoading: false,
    query: {
      limit: 10,
      sort: { createdAt: 'asc' },
      where: {
        active: { '==': true },
        email: { like: '' }
      }
    }
  };

  componentDidMount = () => {
    this.serverSearch(this.state.query);
  };

  clientSearch = (query, users) => {
    const active = query.where.active['=='];
    const email = query.where.email.like.toLowerCase();
    let response = users.filter(item => item.active === active);

    if (email) {
      response = response.filter(item => item.email.toLowerCase().includes(email));
    }

    return response;
  }

  serverSearch = async query => {
    try {
      this.setState({ isLoading: true, query });
      const serverQuery = { sort: query.sort };
      await store.dispatch(getUsers(serverQuery));
      this.setState({ isLoading: false });
    } catch (e) {
      console.error(e);
      this.setState({ isLoading: false });
    }
  };

  deleteData = async item => {
    try {
      this.setState({ isLoading: true });
      await store.dispatch(updateUser({ id: item.id, active: false }));
      this.setState({ isLoading: false });
    } catch (e) {
      console.error(e);
      this.setState({ isLoading: false });
    }
  };

  restoreData = async item => {
    try {
      this.setState({ isLoading: true });
      await store.dispatch(updateUser({ id: item.id, active: true }));
      this.setState({ isLoading: false });
    } catch (e) {
      console.error(e);
      this.setState({ isLoading: false });
    }
  };

  render() {
    const { isLoading, query } = this.state;
    const { config, users, user } = this.props;
    const filteredUsers = this.clientSearch(query, users);
    const isActiveTab = (query.where.active['=='] === true);

    return (
      <React.Fragment>
        <Header
          config={config}
          user={user}
          title={translate(config.translations, 'usersTitle')}
          description={translate(config.translations, 'usersDescription')}
          history={this.props.history}
          showHeading={true} />
        <UserWrapper className="container">
          <ul className="nav nav-tabs mb-4">
            <li className="nav-item">
              <a 
                className={`nav-link ${isActiveTab && 'active'}`}
                onClick={() => 
                  this.setState({ 
                    query: Object.assign(this.state.query, { 
                      where: Object.assign(this.state.query.where, { active: { '==': true } }) 
                    })
                  })
                }>
                Active
              </a>
            </li>
            <li className="nav-item">
              <a 
                className={`nav-link ${!isActiveTab && 'active'}`}
                onClick={() => 
                  this.setState({ 
                    query: Object.assign(this.state.query, { 
                      where: Object.assign(this.state.query.where, { active: { '==': false } }) 
                    })
                  })
                }>
                Inactive
              </a>
            </li>
          </ul>
          <Pager
            isLoading={isLoading}
            config={config}
            query={query}
            items={filteredUsers}
            onChange={query => this.serverSearch(query)}>
            <ul className="list-group list-group-flush">
              {filteredUsers
                .sort((a, b) => a.createdAt - b.createdAt)
                .reverse()
                .slice(0, query.limit)
                .map(item => (
                  <li 
                    className="list-group-item list-group-item-action d-flex justify-content-between align-items-center"
                    key={item.id}>
                    <div>
                      <p className="mb-0">{item.email}</p>
                      <small>{formatDate(item.createdAt, config.preferences.dateFormat)}</small>
                    </div>
                    {isActiveTab && (
                      <div className="btn-group">
                        <Link to={`/admin/users/${item.id}`} className="btn">
                          <i className="fas fa-pen" />
                        </Link>
                        <button className="btn" onClick={() => this.deleteData(item)}>
                          <i className="fas fa-trash" />
                        </button>
                      </div>
                    )}
                    {!isActiveTab && (
                      <div>
                        <button className="btn" onClick={() => this.restoreData(item)}>
                          <i className="fas fa-check" />
                        </button>
                      </div>
                    )}
                  </li>
                ))}
            </ul>
          </Pager>
        </UserWrapper>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  config: state.config,
  user: state.user,
  users: state.user.records
});

export default connect(mapStateToProps)(UserList);
