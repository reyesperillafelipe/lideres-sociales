import React from 'react';
import Select from 'react-select';
import { translate } from '../../../../utils/intl-utils';
import { getFrontendError } from '../../../../utils/response-utils';

export default class UserClaimEdition extends React.Component {
  state = {
    isLoading: false,
    errors: {},
    claim: ''
  };

  componentDidMount = () => {
    this.setState({ claim: this.props.claim });
  };

  validate = async () => {
    const { config } = this.props;
    const { claim } = this.state;
    let errors = {};

    if (!claim.trim()) {
      errors.claim = translate(config.translations, 'notEmpty');
    }

    await this.setState({ errors });
  };

  submit = async e => {
    try {
      if (e) {
        e.preventDefault();
      }

      await this.validate();

      if (Object.values(this.state.errors).filter(item => item).length > 0) {
        return;
      }

      this.props.onSave(this.state.claim);
    } catch (e) {
      const { message, errors } = getFrontendError(e);
      this.setState({ isLoading: false, errors: {...errors, general: message } });
    }
  };

  render() {
    const { isLoading, claim } = this.state;
    const { config, claims } = this.props;
    const allClaims = ['isAdmin', 'isRegistered'];
    const claimsAllowed = allClaims.filter(item => !claims.includes(item));

    return (
      <div className="user-claim-edition">
        <div className="alert alert-warning" role="alert">
          {translate(config.translations, 'requiredFields')}
        </div>
        {
          this.state.errors.general &&
          <div className="alert alert-danger" role="alert">
            {this.state.errors.general}
          </div>
        }
        <form className="row" onSubmit={this.submit}>
          <div className="form-group col-md-12">
            <label>
              {translate(config.translations, 'permission')} <span>*</span>
            </label>
            <Select
              options={claimsAllowed.sort()}
              value={claim}
              getOptionLabel={option => option}
              getOptionValue={option => option}
              onChange={claim => this.setState({ claim })} />
            <span className="text-danger">{this.state.errors.claim}</span>
          </div>
          <div className="form-group col-12 text-right pt-2 mb-0">
            <button
              type="button"
              disabled={isLoading}
              className="btn btn-light mr-2"
              onClick={this.props.onClose}>
              {translate(config.translations, 'cancel')}
            </button>
            <button
              type="submit"
              disabled={isLoading}
              className="btn btn-primary"
              onClick={this.submit}>
              {translate(config.translations, 'save')}
            </button>
          </div>
          <button type="submit" className="d-none" />
        </form>
      </div>
    );
  }
}
