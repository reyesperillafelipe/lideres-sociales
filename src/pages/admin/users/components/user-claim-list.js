import React from 'react';

export default function UserClaimList(props) {
  const { claims, onDelete } = props;

  return (
    <ul className="list-group list-group-flush">
      {claims.sort().map(item => (
        <li
          className="list-group-item list-group-item-action d-flex 
            justify-content-between align-items-center"
          key={item}
        >
          <div>{item}</div>
          <div className="btn-group">
            <button className="btn" onClick={() => onDelete(item)}>
              <i className="fas fa-trash" />
            </button>
          </div>
        </li>
      ))}
    </ul>
  );
}
