import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import Header from '../../_components/header';
import Multilanguage from '../../_components/multilanguage';
import store from '../../../store/store';
import { getLocale, createLocale, updateLocale } from '../../../store/actions/locale-actions';
import { translate } from '../../../utils/intl-utils';
import { getFrontendError } from '../../../utils/response-utils';

const LocaleEditionWrapper = styled.main`
  &, * {
    box-sizing: border-box;
  }
`;

class LocaleEdition extends React.Component {
  state = {
    isLoading: false,
    errors: {},
    model: {
      active: true,
      name: '',
      value: {}
    }
  };

  componentDidMount = () => {
    store.dispatch(getLocale(this.props.match.params.id))
      .then(() => {
        this.setState({ model: Object.assign(this.state.model, this.props.locale.temp) });
      });
  };

  submit = async e => {
    try {
      if (e) {
        e.preventDefault();
      }

      this.setState({ isLoading: true, errors: {} });

      if (!this.state.model.id) {
        await store.dispatch(createLocale(this.state.model));
      } else {
        await store.dispatch(updateLocale(this.state.model));
      }

      this.setState({ isLoading: false });
      this.props.history.push('/admin/locales');
    } catch (e) {
      const { errors, message } = getFrontendError(e);
      this.setState({ isLoading: false, errors: {...errors, general: message } });
    }
  };

  render() {
    const { isLoading } = this.state;
    const { config, user } = this.props;

    return (
      <React.Fragment>
        <Header
          config={config}
          user={user}
          title={translate(config.translations, 'localeEditionTitle')}
          description={translate(config.translations, 'localeEditionDescription')}
          history={this.props.history}
          showHeading={true} 
          btnLeft={
            <button className="btn" onClick={() => this.props.history.goBack()}>
              <i className="fas fa-arrow-left" />
            </button>
          }
          btnRight={
            <button className="btn" onClick={this.submit} disabled={isLoading}>
              <i className="fas fa-save" />
            </button>
          } />
        <LocaleEditionWrapper className="container">            
          <div className="alert alert-warning" role="alert">
            {translate(config.translations, 'requiredFields')}
          </div>
          {
            this.state.errors.general &&
            <div className="alert alert-danger" role="alert">
              {this.state.errors.general}
            </div>
          }
          <form className="row" onSubmit={this.submit}>
            <div className="form-group col-md-12">
              <label>Name *</label>
              <input
                type="text"
                className="form-control"
                value={this.state.model.name}
                onChange={e => this.setState({ model: Object.assign(this.state.model, { name: e.target.value }) })} />
              <span className="text-danger">{this.state.errors.name}</span>
            </div>
            <div className="form-group col-md-12">
              <label>Value *</label>
              <Multilanguage
                languages={config.appLanguages}
                value={this.state.model.value}
                onChange={value => this.setState({ model: Object.assign(this.state.model, { value }) })} />
              <span className="text-danger">{this.state.errors.value}</span>
            </div>
            <button type="submit" className="d-none" />
          </form>
        </LocaleEditionWrapper>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  config: state.config,
  locale: state.locale,
  user: state.user
});

export default connect(mapStateToProps)(LocaleEdition);
