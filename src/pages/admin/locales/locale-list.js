import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import Header from '../../_components/header';
import Pager from '../../_components/pager';
import Modal from '../../_components/modal';
import store from '../../../store/store';
import { getLocales, deleteLocale } from '../../../store/actions/locale-actions';
import { formatDate, translate } from '../../../utils/intl-utils';

const LocaleListWrapper = styled.main`
  &, * {
    box-sizing: border-box;
  }
`;

class LocaleList extends React.Component {
  state = {
    isLoading: false,
    showModalDeleteLocale: false,
    selectedItemToDelete: null,
    query: {
      limit: 10,
      sort: { name: 'asc' },
      where: {
        name: { like: '' }
      }
    }
  };

  componentDidMount = () => {
    this.serverSearch(this.state.query);
  };

  clientSearch = (query, locales) => {
    let response = locales
      .filter(item => item.name.toLowerCase().includes(query.where.name.like.toLowerCase()));

    return response;
  }

  serverSearch = async query => {
    try {
      this.setState({ isLoading: true, query });
      const serverSearch = { sort: query.sort };
      await store.dispatch(getLocales(serverSearch));
      this.setState({ isLoading: false });
    } catch (e) {
      console.error(e);
      this.setState({ isLoading: false });
    }
  };

  delete = async item => {
    try {
      this.setState({ isLoading: true });
      await store.dispatch(deleteLocale(item.id));
      this.setState({ isLoading: false });
    } catch (e) {
      console.error(e);
      this.setState({ isLoading: false });
    }
  };

  render() {
    const { isLoading, query } = this.state;
    const { config, locales, user } = this.props;
    const filteredLocales = this.clientSearch(query, locales);

    return (
      <React.Fragment>
        <Header
          config={config}
          user={user}
          title={translate(config.translations, 'localesTitle')}
          description={translate(config.translations, 'localesDescription')}
          history={this.props.history}
          showHeading={true}
          btnRight={
            <React.Fragment>
              <button
                className="btn"
                onClick={() => this.props.history.push('/admin/locales/new')}>
                <i className="fas fa-plus" />
              </button>
            </React.Fragment>
          } />
        <LocaleListWrapper className="container">
          <Pager
            isLoading={isLoading}
            config={config}
            query={query}
            items={filteredLocales}
            onChange={query => this.serverSearch(query)}>
            <ul className="list-group list-group-flush">
              {filteredLocales
                .sort((a, b) => a.name.localeCompare(b.name))
                .slice(0, query.limit)
                .map(item => (
                  <li
                    className="list-group-item list-group-item-action d-flex 
                    justify-content-between align-items-center"
                    key={item.id}>
                    <div>
                      <p className="mb-0">{item.name}</p>
                      <small>{formatDate(item.createdAt, config.preferences.dateFormat)}</small>
                    </div>
                    {
                      <div className="btn-group">
                        <Link to={`/admin/locales/${item.id}`} className="btn">
                          <i className="fas fa-pen" />
                        </Link>
                        <button className="btn" onClick={() => this.setState({ showModalDeleteLocale: true, selectedItemToDelete: item })}>
                          <i className="fas fa-trash" />
                        </button>
                      </div>
                    }
                  </li>
                ))
              }
            </ul>
          </Pager>
          <Modal 
            isOpen={this.state.showModalDeleteLocale} 
            title={translate(config.translations, 'confirmation')}
            onToggle={() => this.setState({ showModalDeleteLocale: !this.state.showModalDeleteLocale })}>
            <p>{translate(config.translations, 'deleteConfirmation')}</p>
            <div className="mt-2 text-right">
              <button 
                className="btn btn-light mr-2" 
                onClick={() => this.setState({ showModalDeleteLocale: false, selectedItemToDelete: null })}>
                {translate(config.translations, 'cancel')}
              </button>
              <button 
                className="btn btn-primary" 
                onClick={() => {
                  this.setState({ showModalDeleteLocale: false });
                  this.delete(this.state.selectedItemToDelete);
                }}>
                {translate(config.translations, 'ok')}
              </button>
            </div>
          </Modal>
        </LocaleListWrapper>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  config: state.config,
  locales: state.locale.records,
  user: state.user
});

export default connect(mapStateToProps)(LocaleList);
