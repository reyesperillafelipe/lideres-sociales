import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import Header from '../../_components/header';
import store from '../../../store/store';
import { syncConfiguration } from '../../../store/actions/config-actions';
import { formatDate, translate } from '../../../utils/intl-utils';

const ConfigurationEditionWrapper = styled.main`
  &, * {
    box-sizing: border-box;
  }
`;

class ConfigurationEdition extends React.Component {
  state = {
    isLoading: false,
    model: {
      appDisabled: false,
      appDisabledMessage: '',
      appLastSync: null
    }
  };

  componentDidMount = () => {
    const { config } = this.props;

    this.setState({
      model: Object.assign(this.state.model, {
        appDisabled: config.appDisabled,
        appDisabledMessage: config.appDisabledMessage,
        appLastSync: config.appLastSync
      })
    });
  }

  syncConfiguration = async () => {
    try {
      this.setState({ isLoading: true });
      await store.dispatch(syncConfiguration(this.state.model));
      this.setState({ isLoading: false });
    } catch (e) {
      console.error(e);
      this.setState({ isLoading: false });
    }
  }

  render() {
    const { isLoading, model } = this.state;
    const { config, user } = this.props;
    const { intlData } = config;

    return (
      <React.Fragment>
        <Header
          config={config}
          user={user}
          title={translate(config.translations, 'configurationEditionTitle')}
          description={translate(config.translations, 'configurationEditionDescription')}
          history={this.props.history}
          showHeading={true} />
        <ConfigurationEditionWrapper className="container">
          <div className="card mb-4">
            <div className="card-header d-flex align-items-center">
              <h5 className="mb-0">{translate(config.translations, 'configuration')}</h5>
              <div className="ml-auto">
                <label className="mr-2">{formatDate(model.appLastSync, 'dd/mm/yyyy')}</label>
                <button 
                  className="btn btn-primary ml-auto" 
                  disabled={isLoading}
                  onClick={this.syncConfiguration}>
                  <i className="fas fa-redo mr-1"></i> {translate(config.translations, 'sync')}
                </button>
              </div>
            </div>
            <div className="card-body">
              <div className="form-group form-check">
                <input 
                  type="checkbox" 
                  className="form-check-input"
                  onChange={event => this.setState({ model: Object.assign(model, { appDisabled: event.target.checked }) })}>
                </input>
                <label className="form-check-label">{translate(config.translations, 'disableApp')}</label>
              </div>
              <div className="form-group">
                <label>{translate(config.translations, 'customMessage')}</label>
                <textarea 
                  className="form-control" 
                  rows="3" 
                  value={model.appDisabledMessage}
                  onChange={event => 
                    this.setState({ model: Object.assign(model, { appDisabledMessage: event.target.value }) })
                  }>                  
                </textarea>
              </div>
              <ul className="list-group list-group-flush">
                <li className="list-group-item">CallingCodes: {intlData.callingCodes.length}</li>
                <li className="list-group-item">Countries: {intlData.countries.length}</li>
                <li className="list-group-item">Currencies: {intlData.currencies.length}</li>
                <li className="list-group-item">Languages: {intlData.languages.length}</li>
                <li className="list-group-item">
                  Locales: {' '}
                  ({
                    Object.keys(intlData.locales)
                      .map(lang => `${lang}: ${Object.keys(intlData.locales[lang]).length}`)
                      .join(', ')
                  })
                </li>
              </ul>
            </div>
          </div>
        </ConfigurationEditionWrapper>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  config: state.config
});

export default connect(mapStateToProps)(ConfigurationEdition);
