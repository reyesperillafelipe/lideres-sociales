import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import Header from './_components/header';
import News from './_components/news';
import Footer from './_components/footer';
import { translate } from '../utils/intl-utils';
const HomeWrapper = styled.main`
  &, * {
    box-sizing: border-box;
  }
`;

import data from './any.json';
class Home extends React.Component {
  state = {
    error: null,
    isLoading: false,
    news: []
  }
  async componentDidMount() {
    await this.fetchNews();
  }

  fetchNews = async () => {
    this.setState({isLoading: true, error: null});
    // try {
    //   const response = await fetch('https://evident-factor-259411.appspot.com/api/scraping/analyzeWebs');
    //   const data = await response.json();
    //   this.setState({
    //     isLoading: false,
    //     news: data
    //   });
    // } catch (error) {
    //   this.setState({
    //     isLoading: false,
    //     error: error
    //   });
    // }
    this.setState({
      isLoading: false,
      news: data.news
    });
  };

  render() {
    const { config, user } = this.props;
    return (
      <React.Fragment>
        <Header
          config={config}
          user={user}
          title={translate(config.translations, 'homeTitle')}
          description={translate(config.translations, 'homeDescription')}
          history={this.props.history}
          showHeading={true} />
        <HomeWrapper className="container-fluid">
          {this.state.isLoading && (
          <div className="text-center">
            <div className="spinner-border text-info" role="status">
              <span className="sr-only">Loading...</span>
            </div>
          </div>
          )}
          <News news={this.state.news}/>
          {this.state.error && (
          <div className="text-center">
            <p className="text-danger">Noticias No Disponibles</p>
          </div>
          )}
        </HomeWrapper>
        <Footer 
          config={config}
          routerHistory={this.props.history} />
      </React.Fragment>
    );
  }
}
const mapStateToProps = state => ({
  config: state.config,
  catalog: state.catalog,
  plan: state.plan,
  user: state.user
});
export default connect(mapStateToProps)(Home);