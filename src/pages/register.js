import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { translate } from '../utils/intl-utils';
import { getFrontendError } from '../utils/response-utils';
import Header from './_components/header';
import Footer from './_components/footer';
import store from '../store/store';
import { register, setToken, me } from '../store/actions/user-actions';

const RegisterWrapper = styled.main`
  &, * {
    box-sizing: border-box;
  }
`;

class Register extends React.Component {
  state = {
    isLoading: false,
    errors: {},
    model: {
      active: true,
      email: '',
      password: '',
      passwordConfirmation: '',
      plan: this.props.config.plans.free
    }
  };

  componentDidMount = () => {
    if (this.props.user.auth) {
      this.props.history.push('/app/dashboard');
    }
  };

  registerWithEmail = async e => {
    try {
      if (e) {
        e.preventDefault();
      }

      this.setState({ isLoading: true, errors: {} });
      await store.dispatch(register(this.state.model));
      await store.dispatch(setToken(this.props.user.token));
      await store.dispatch(me());
      this.setState({ isLoading: false });
      this.props.history.push('/app/dashboard');
    } catch (e) {
      const { errors, message } = getFrontendError(e);
      this.setState({ isLoading: false, errors: {...errors, general: message } });
    }
  };

  render() {
    const { isLoading } = this.state;
    const { config, user } = this.props;

    return (
      <React.Fragment>
        <Header
          config={config}
          user={user}
          title={translate(config.translations, 'registerTitle')}
          description={translate(config.translations, 'registerDescription')}
          history={this.props.history}
          showHeading={true} />
        <RegisterWrapper className="container">
          <div className="row">
            <div className="col-md-12">
              <div className="alert alert-info">
                {translate(config.translations, 'userAlreadyHasAccount')}{' '}
                <Link to="/login">{translate(config.translations, 'here')}</Link>
              </div>
              <div className="alert alert-warning" role="alert">
                {translate(config.translations, 'requiredFields')}
              </div>
              {
                this.state.errors.general &&
                <div className="alert alert-danger" role="alert">
                  {this.state.errors.general}
                </div>
              }
              <form onSubmit={this.registerWithEmail}>
                <div className="form-group">
                  <label>
                    {translate(config.translations, 'email')} <span>*</span>
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    value={this.state.model.email}
                    onChange={e =>
                      this.setState({ model: Object.assign(this.state.model, { email: (e.target.value || '')
                        .replace(/\s/g, '') 
                      }) 
                    })
                    }
                  />
                  <span className="text-danger">{this.state.errors.email}</span>
                </div>
                <div className="form-group">
                  <label>
                    {translate(config.translations, 'password')} <span>*</span>
                  </label>
                  <input
                    type="password"
                    className="form-control"
                    value={this.state.model.password}
                    onChange={e =>
                      this.setState({ model: Object.assign(this.state.model, { password: e.target.value }) })
                    }
                  />
                  <span className="text-danger">
                    {this.state.errors.password}
                  </span>
                </div>
                <div className="form-group">
                  <label>
                    {translate(config.translations, 'passwordConfirm')} <span>*</span>
                  </label>
                  <input
                    type="password"
                    className="form-control"
                    onChange={e =>
                      this.setState({ model: Object.assign(this.state.model, { passwordConfirmation: e.target.value }) })
                    }
                  />
                  <span className="text-danger">
                    {this.state.errors.passwordConfirmation}
                  </span>
                </div>
                <div className="form-group text-right">
                  <button type="submit" disabled={isLoading} className="btn btn-primary">
                    {translate(config.translations, 'register')}
                  </button>
                </div>
              </form>
            </div>
          </div>
        </RegisterWrapper>
        <Footer 
          config={config}
          routerHistory={this.props.history} />
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  config: state.config,
  user: state.user
});

export default connect(mapStateToProps)(Register);
