import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import Header from './_components/header';
import { translate } from '../utils/intl-utils';
const HomeWrapper = styled.main`
  &, * {
    box-sizing: border-box;
  }
`;

class Contact extends React.Component {
  state = {
    error: null,
    isLoading: false,
    news: []
  }
  async componentDidMount() {
    await this.fetchNews();
  }

  fetchNews = async () => {
    this.setState({isLoading: true, error: null});
    // try {
    //   const response = await fetch('https://evident-factor-259411.appspot.com/api/scraping/analyzeWebs');
    //   const data = await response.json();
    //   this.setState({
    //     isLoading: false,
    //     news: data
    //   });
    // } catch (error) {
    //   this.setState({
    //     isLoading: false,
    //     error: error
    //   });
    // }
    this.setState({
      isLoading: false,
    //   news: data.news
    });
  };

  render() {
    const { config, user } = this.props;
    return (
      <React.Fragment>
        <Header
          config={config}
          user={user}
          title={translate(config.translations, 'contact')}
          description={translate(config.translations, 'contactDescription')}
          history={this.props.history}
          showHeading={true} />
        <HomeWrapper className="container">
        <div className="jumbotron">
            <h1>Canales</h1>
            <div className="col-12 mt-4">
                <ul className="list-inline">
                <li className="list-inline-item">
                  <a
                    href="https://www.instagram.com/lideressociale3/"
                    title="Instagram"
                    target="_blank"
                    rel="noopener noreferrer">
                    <i className="fab fa-instagram fa-2x text-dark" />
                  </a>
                </li>
                <li className="list-inline-item">
                  <a
                    href="https://www.facebook.com/Lideres-Sociales-106902417680240/"
                    title="Facebook"
                    target="_blank"
                    rel="noopener noreferrer">
                    <i className="fab fa-facebook fa-2x text-dark" />
                  </a>
                </li>
                <li className="list-inline-item">
                  <a
                    href="https://twitter.com/LideresSociale3"
                    title="Twitter"
                    target="_blank"
                    rel="noopener noreferrer">
                    <i className="fab fa-twitter fa-2x text-dark" />
                  </a>
                </li>
                <li className="list-inline-item">
                  <a
                    href="mailto:lideressocialescolombia@gmail.com"
                    title="Gmail"
                    target="_blank"
                    rel="noopener noreferrer">
                    <i className="fas fa-envelope fa-2x text-dark"/>
                  </a>
                </li>
                </ul>
            </div>
        </div>
        </HomeWrapper>
      </React.Fragment>
    );
  }
}
const mapStateToProps = state => ({
  config: state.config,
  catalog: state.catalog,
  plan: state.plan,
  user: state.user
});
export default connect(mapStateToProps)(Contact);