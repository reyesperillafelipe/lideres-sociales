import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { translate } from '../utils/intl-utils';
import { getFrontendError } from '../utils/response-utils';
import { recoverPassword } from '../store/actions/user-actions';
import Header from './_components/header';
import Footer from './_components/footer';
import store from '../store/store';

const RecoverPasswordWrapper = styled.main`
  &, * {
    box-sizing: border-box;
  }
`;

class RecoverPassword extends React.Component {
  state = {
    errors: {},
    model: {
      email: ''
    }
  };

  forgotPassword = async e => {
    try {
      e.preventDefault();

      this.setState({ isLoading: true, errors: {} });
      await this.setState({
        model: {
          ...this.state.model,
          token: this.props.match.params.token
        }
      });
      await store.dispatch(recoverPassword(this.state.model));
      const successMessage = this.props.user.temp.message;
      this.setState({ isLoading: false });
      this.props.history.push(`/login?message=${successMessage}`);
    } catch (e) {
      const { errors, message } = getFrontendError(e);
      this.setState({ isLoading: false, errors: { ...errors, general: message } });
    }
  };

  render() {
    const { isLoading } = this.state;
    const { config, user } = this.props;

    return (
      <React.Fragment>
        <Header
          config={config}
          user={user}
          title={translate(config.translations, 'recoverPasswordTitle')}
          description={translate(config.translations, 'recoverPasswordDescription')}
          history={this.props.history}
          showHeading={true} />
        <RecoverPasswordWrapper className="container">
          <div className="row">
            <div className="col-md-12">
              <div className="alert alert-warning" role="alert">
                {translate(config.translations, 'requiredFields')}
              </div>
              {
                this.state.errors.general &&
                <div className="alert alert-danger" role="alert">
                  {this.state.errors.general}
                </div>
              }
              <form onSubmit={this.forgotPassword}>
                <div className="form-group">
                  <label>{translate(config.translations, 'email')} *</label>
                  <input
                    type="text"
                    className="form-control"
                    onChange={e => 
                      this.setState({ model: Object.assign(this.state.model, { email: e.target.value }) })
                    }
                  />
                  <span className="text-danger">{this.state.errors.email}</span>
                </div>
                <div className="form-group text-right">
                  <button type="submit" className="btn btn-primary" disabled={isLoading}>
                    {translate(config.translations, 'recoverPassword')}
                  </button>
                </div>
              </form>
            </div>
          </div>
        </RecoverPasswordWrapper>
        <Footer 
          config={config}
          routerHistory={this.props.history} />
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  config: state.config,
  user: state.user
});

export default connect(mapStateToProps)(RecoverPassword);
