import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import Header from '../../_components/header';
import Footer from '../../_components/footer';
import store from '../../../store/store';
import { createReport } from '../../../store/actions/user-actions';
import { translate } from '../../../utils/intl-utils';
import colombia from '../../colombia.json';

const DashboardWrapper = styled.main`
  &, * {
    box-sizing: border-box;
  }
`;

class Dashboard extends React.Component {
  state = {
    isLoading: false,
    reportCreated: false,
    colombia,
    citiesByDepartment: [],
    model: {
      names: '',
      lastNames: '',
      email: '',
      phone: '',
      department: '',
      city: '',
      address: '',
      description: '',
      checkSendEmail: 'off'
    }
  };

  validatePhone = e => {
    try {
      // e.preventDefault();
      this.setState({ isLoading: true, errors: {} });
      const phoneCharacters = document.getElementById('inputPhone').value.length;
      if(phoneCharacters !== 10) {
        document.getElementById('inputPhone').value = '';
      }
      this.setState({ model: Object.assign(this.state.model, { phone: e.target.value }) });
      this.setState({ isLoading: false });
    } catch (e) {
      this.setState({ isLoading: false, errors: e});
    }
  };

  loadCities = async e => {
    try {
      e.preventDefault();
      this.setState({ isLoading: true, errors: {} });
      const departmentSeleted = document.getElementById('inputDepartment').value;
      const citiesByDepartment = this.state.colombia.find(department => department.departamento === departmentSeleted);
      await this.setState({ citiesByDepartment: citiesByDepartment.ciudades });
      this.setState({ isLoading: false });
    } catch (e) {
      this.setState({ isLoading: false, errors: e});
    }
  };

  registerDenounce = async e => {
    try {
      if (e) {
        e.preventDefault();
      }
      this.setState({ isLoading: true, errors: {} });
      await store.dispatch(createReport(this.state.model));
       this.setState({ reportCreated: true });
       this.setState({ isLoading: false });
      // if(this.state.reportCreated) {

      // }
      // this.props.history.push('/');
    } catch (e) {
      this.setState({ isLoading: false, errors: e});
    }
  };

  render() {
    const { isLoading, reportCreated } = this.state;
    const { config, user } = this.props;

    return (
      <React.Fragment>
        <Header
          config={config}
          user={user}
          title={translate(config.translations, 'dashboardTitle')}
          description={translate(config.translations, 'dashboardDescription')}
          history={this.props.history}
          showHeading={true} />
        <DashboardWrapper className="container-fluid">
        {isLoading && (
          <div className="text-center">
            <div className="spinner-border text-info" role="status">
              <span className="sr-only">Loading...</span>
            </div>
          </div>
        )}
        {reportCreated && (
        <div className="jumbotron">
          <h1 className="display-4">Registro Exitoso!</h1>
          <p className="lead">Pronto empezaremos una gestión de tu caso.</p>
          <hr className="my-4"/>
          <a className="btn btn-primary btn-lg" href="/" role="button">Inicio</a>
        </div>
        )}
        <div className="container">
          <form className="my-4" onSubmit={this.registerDenounce}>
            <div className="form-row">
              <div className="form-group col-md-6">
                <input type="text" className="form-control" id="inputNombres"
                placeholder={translate(config.translations, 'firstname')} required
                onBlur={e =>
                  this.setState({ model: Object.assign(this.state.model, { names: e.target.value }) })
                }/>
              </div>
              <div className="form-group col-md-6">
                <input type="text" className="form-control" id="inputApellidos"
                placeholder={translate(config.translations, 'lastname')} required
                onBlur={e =>
                  this.setState({ model: Object.assign(this.state.model, { lastNames: e.target.value }) })
                }/>
              </div>
            </div>
            <div className="form-row mt-3">
              <div className="form-group col-md-6">
                <input type="email" className="form-control" id="inputEmail"
                placeholder={translate(config.translations, 'email')} required
                onBlur={e =>
                  this.setState({ model: Object.assign(this.state.model, { email: e.target.value }) })
                }/>
              </div>
              <div className="form-group col-md-6">
              <div className="input-group mb-2">
                <div className="input-group-prepend">
                  <div className="input-group-text">+57</div>
                </div>
                <input type="number" className="form-control" id="inputPhone" 
                placeholder={translate(config.translations, 'phone')} required 
                onBlur={this.validatePhone}/>
              </div>
              </div>
            </div>
            <div className="form-row mt-2">
              <div className="form-group col-md-4">
                <label htmlFor="inputDepartment">{translate(config.translations, 'department')}</label>
                <select id="inputDepartment" 
                required
                className="form-control"
                onChange={this.loadCities}
                onBlur={e =>
                  this.setState({ model: Object.assign(this.state.model, { department: e.target.value }) })
                }>
                  {this.state.colombia.map(department => (
                    <option key={department.id} value={department.departamento}>{department.departamento}</option>
                  ))}
                </select>
              </div>
              <div className="form-group col-md-5">
              <label htmlFor="inputCity">{translate(config.translations, 'city')}</label>
                <select id="inputCity"
                required
                className="form-control"
                onBlur={e =>
                  this.setState({ model: Object.assign(this.state.model, { city: e.target.value }) })
                }>                
                  {this.state.citiesByDepartment.map(city => (
                    <option key={city} value={city}>{city}</option>
                  ))}
                </select>
              </div>
              <div className="form-group col-md-3">
                <label htmlFor="inputAddress">{translate(config.translations, 'address')}</label>
                <input type="text" className="form-control" id="inputAddress"
                onBlur={e =>
                  this.setState({ model: Object.assign(this.state.model, { address: e.target.value }) })
                }/>
              </div>
            </div>
            <div className="form-row mt-2">
              <div className="col-md-10">
                <label htmlFor="description">{translate(config.translations, 'description')}</label>
                <textarea className="form-control" id="description" rows="3" 
                required
                onBlur={e =>
                  this.setState({ model: Object.assign(this.state.model, { description: e.target.value }) })
                }>
                </textarea>
              </div>
            </div>
            <div className="form-row mt-2">
              <div className="form-group">
                <div className="form-check">
                  <input className="form-check-input" type="checkbox" id="checkForSendEmails"
                  onMouseDown={e =>
                    this.setState({ model: Object.assign(this.state.model, { checkSendEmail: e.target.value }) })
                  }/>
                  <label className="form-check-label" htmlFor="checkForSendEmails">
                    {translate(config.translations, 'sendEmail')}
                  </label>
                </div>
              </div>
            </div>
            <button type="submit" className="btn btn-warning" disabled={isLoading}>{translate(config.translations, 'denounce')}</button>
          </form>
        </div>

        </DashboardWrapper>
        <Footer 
          config={config}
          routerHistory={this.props.history} />
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  config: state.config,
  contact: state.contact,
  expense: state.expense,
  invoice: state.invoice,
  report: state.report,
  user: state.user
});

export default connect(mapStateToProps)(Dashboard);
