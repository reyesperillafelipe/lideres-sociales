import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import Header from '../../_components/header';
import { changePassword, updateUser } from '../../../store/actions/user-actions';
import store from '../../../store/store';
import { translate } from '../../../utils/intl-utils';
import { getFrontendError } from '../../../utils/response-utils';

const ProfileWrapper = styled.main`
  &, * {
    box-sizing: border-box;
  }
`;

class Profile extends React.Component {
  state = {
    isLoading: false,
    changePassword: false,
    errors: {},
    model: {
      id: this.props.auth.uid,
      displayName: '',
      email: '',
      photoURL: null,
      password: '',
      passwordConfirmation: ''
    }
  };

  componentDidMount = () => {
    const profile = Object.keys(this.props.auth.profile).reduce((accum, key) => {
      return !['plan'].includes(key) 
        ? Object.assign(accum, { [key]: this.props.auth.profile[key] }) 
        : accum;
    }, {});

    this.setState({
      model: Object.assign(
        this.state.model,
        profile
      )
    });
  };

  submit = async e => {
    try {
      if (e) {
        e.preventDefault();
      }

      this.setState({ isLoading: true, errors: {} });

      if (this.state.changePassword) {
        const { model } = this.state;
        const data = {
          password: model.password,
          passwordConfirmation: model.passwordConfirmation
        };
        await store.dispatch(changePassword(data));
      }

      await store.dispatch(updateUser(this.state.model));
      this.setState({
        changePassword: false,
        model: Object.assign(this.state.model, {
          password: '',
          passwordConfirmation: ''
        })
      });

      this.setState({ isLoading: false });
    } catch (e) {
      const { message, errors } = getFrontendError(e);
      this.setState({ isLoading: false, errors: { ...errors, general: message } });
    }
  };

  render() {
    const { isLoading } = this.state;
    const { auth, config, user } = this.props;

    return (
      <React.Fragment>
        <Header
          config={config}
          user={user}
          title={translate(config.translations, 'profileTitle')}
          description={translate(config.translations, 'profileDescription')}
          history={this.props.history}
          showHeading={true} />
        <ProfileWrapper className="container">
          <div className="alert alert-warning" role="alert">
            {translate(config.translations, 'requiredFields')}
          </div>
          {
            this.state.errors.general &&
            <div className="alert alert-danger" role="alert">
              {this.state.errors.general}
            </div>
          }
          <form className="row" onSubmit={this.submit}>
            <div className="col-md-12">
              <div className="form-group">
                <label>{translate(config.translations, 'displayName')} *</label>
                <input
                  type="text"
                  className="form-control"
                  value={this.state.model.displayName}
                  onChange={e =>
                    this.setState({ model: Object.assign(this.state.model, { displayName: e.target.value }) })
                  }
                />
                <span className="text-danger">
                  {this.state.errors.displayName}
                </span>
              </div>
              <div className="form-group">
                <label>{translate(config.translations, 'email')} *</label>
                <div className="input-group">
                  <div 
                    title={auth.email_verified 
                      ? translate(config.translations, 'emailConfirmed') 
                      : translate(config.translations, 'emailUnconfirmed')} 
                    className="input-group-prepend">
                    <span className="input-group-text">
                      <i className={`${(auth && auth.email_verified) ? 'fas fa-check text-success' : 'fas fa-times text-danger'}`}/>
                    </span>
                  </div>
                  <label className="form-control">{this.state.model.email}</label>
                </div>
                <span className="text-danger">{this.state.errors.email}</span>
              </div>
              <div className="alert alert-info">
                <input
                  type="checkbox"
                  checked={this.state.changePassword}
                  onChange={() =>
                    this.setState({ model: Object.assign(this.state.model, { changePassword: !this.state.changePassword }) })
                  }
                />{' '}
                Do you want to change your password?
              </div>
              {
                this.state.changePassword &&
                <React.Fragment>
                  <div className="form-group">
                    <label>{translate(config.translations, 'password')} *</label>
                    <input
                      type="password"
                      className="form-control"
                      value={this.state.model.password}
                      onChange={e =>
                        this.setState({ model: Object.assign(this.state.model, { password: e.target.value }) })
                      }
                    />
                    <span className="text-danger">{this.state.errors.password}</span>
                  </div>
                  <div className="form-group">
                    <label>{translate(config.translations, 'passwordConfirm')} *</label>
                    <input
                      type="password"
                      className="form-control"
                      value={this.state.model.passwordConfirmation}
                      onChange={e =>
                        this.setState({ model: Object.assign(this.state.model, { passwordConfirmation: e.target.value }) })
                      }
                    />
                    <span className="text-danger">
                      {this.state.errors.passwordConfirmation}
                    </span>
                  </div>
                </React.Fragment>
              }
              <div className="form-group text-right">
                <button type="submit" className="btn btn-primary" disabled={isLoading}>
                  {translate(config.translations, 'save')}
                </button>
              </div>
            </div>
          </form>
        </ProfileWrapper>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.user.auth,
  config: state.config,
  user: state.user
});

export default connect(mapStateToProps)(Profile);
