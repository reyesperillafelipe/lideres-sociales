import React from 'react';

export default class Collapsible extends React.Component {

  componentDidUpdate = prevProps => {
    if (prevProps.isOpen !== this.props.isOpen) {
      this.collapsible.classList.toggle('collapsing');

      setTimeout(() => {
        this.collapsible.classList.toggle('collapsing');
        
        if (this.props.isOpen) {
          this.props.onEntering();
        }
      }, 0);
    }
  }

  render () {
    const { isOpen, className, children } = this.props;

    return (
      <div 
        ref={elem => this.collapsible = elem}
        aria-expanded={isOpen}
        className={`${className} collapse ${isOpen && 'show'}`}>
        {children}
      </div>
    );
  }
}