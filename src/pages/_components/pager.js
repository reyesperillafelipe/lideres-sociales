import React from 'react';
import { translate } from '../../utils/intl-utils';

export default class Pager extends React.Component {
  state = {
    search: ''
  };

  componentDidMount = () => {
    const { query } = this.props;
    const searchValue = this.getSearchValue(query);

    if (this.state.search !== searchValue) {
      this.setState({ search: searchValue });
    }
  };

  changeSearch = async newQuery => {
    this.props.onChange(newQuery);
  };

  cleanSearch = async () => {
    await this.setState({ search: '' });

    const { query } = this.props;
    const newQuery = {
      ...query,
      limit: 10,
      where: this.setSearchValue(query.where, '')
    };
    this.changeSearch(newQuery);
  };

  getSearchValue = filters => {
    filters = filters || {};
    let searchValue = '';

    for (let key in filters) {
      for (let operator in filters[key]) {
        if (operator === 'like') {
          searchValue = filters[key][operator];
          break;
        }
      }
    }

    return searchValue;
  };

  setSearchValue = (filters, searchValue) => {
    filters = filters || {};

    for (let key in filters) {
      for (let operator in filters[key]) {
        if (operator === 'like') {
          filters[key][operator] = searchValue;
        }
      }
    }

    return filters;
  };

  submitSearch = e => {
    if (e) {
      e.preventDefault();
    }

    const { query } = this.props;
    const newQuery = {
      ...query,
      where: this.setSearchValue(query.where, this.state.search)
    };
    
    this.changeSearch(newQuery);
  };

  render() {
    const { search } = this.state;
    const { isLoading, config, hideSearch, items, query } = this.props;
    const thereIsData = items.length > 0;

    return (
      <section className={this.props.className}>
        <div className="help-block" />
        {
          !hideSearch && 
          <form onSubmit={e => this.submitSearch(e)}>
            <div className="form-group input-group">
              <input
                type="text"
                className="form-control"
                placeholder="Search"
                value={search}
                onChange={e => this.setState({ search: e.target.value })} />
              <div className="input-group-append">
                <button
                  type="button"
                  className="btn btn-outline-primary"
                  onClick={this.cleanSearch}>
                  <i className="fas fa-times" />
                </button>
                <button type="submit" className="btn btn-outline-primary">
                  <i className="fas fa-search" />
                </button>
              </div>
            </div>
          </form>
        }
        <section>
          {thereIsData && this.props.children}
          {!isLoading && !thereIsData && (
            <div className="alert alert-warning">{translate(config.translations, 'thereIsNotData')}</div>
          )}
        </section>
        {
          !isLoading && thereIsData &&
          <section>
            <div className="text-center mt-3">
              {query.limit <= items.length && (
                <button
                  className="btn btn-primary"
                  onClick={() => this.changeSearch(Object.assign(query, { limit: (query.limit + 10) }))}
                >
                  <i className="fas fa-redo mr-1" /> {translate(config.translations, 'seeMore')}
                </button>
              )}
            </div>
          </section>
        }
      </section>
    );
  }
}
