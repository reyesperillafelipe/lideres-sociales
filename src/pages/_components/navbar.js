import React, { useContext, useState } from 'react';
import styled from 'styled-components';
import Context from '../../context';
import Collapsible from './collapsible';
import MenuMain from './menu-main';
import MenuPopovers from './menu-popovers';

const NavbarWrapper = styled.nav`
  &, * {
    box-sizing: border-box;
  }

  .navbar-collapse {
    order: 0;
  }
  .navbar-options {
    order: 1;
    margin-left: auto;

    .btn {
      color: #666;
    }
  }    
  .navbar-toggler {
    order: 2;
  }

  @media screen and (max-width: 768px) {
    .navbar.navbar-light {
      z-index: 9;
      .navbar-toggler {
        color: #666;
        border: none;
        padding: inherit;
        font-size: inherit;
        line-height: normal;
      }
    }
    .navbar-options {
      order: 0;
    }
    .navbar-toggler {
      order: 1;
    }
    .navbar-collapse {
      order: 2;
      ul > a:not(.btn), 
      ul > li {
        border-top: 1px solid #EEE;
        padding-left: 16px;
        padding-right: 16px;

        &.active,
        &:hover {
          background-color: rgba(161, 194, 250, 0.16);
        }
      }
    }
  }
`;

export default function Navbar(props) {
  const [showMenu, setMenu] = useState(false);
  const context = useContext(Context);
  const { config, user, history } = props;
  const { auth, authLoaded } = user;
  const currentPath = history.location.pathname;

  return (
    <NavbarWrapper className="border-bottom navbar navbar-expand-md navbar-light bg-light">
      <button className="btn navbar-brand" onClick={() => history.push('/')}>
        <img src={config.appLogo} alt={config.appName} width={35} />{' '}
        <span>{config.appName}</span>
      </button>
      <Collapsible className="navbar-collapse" isOpen={showMenu}>
        <MenuMain
          auth={auth}
          authLoaded={authLoaded}
          config={config}
          currentPath={currentPath}
          onChangePath={path => {
            setMenu(false);
            history.push(path);
          }}>
        </MenuMain>
      </Collapsible>
      <div className="navbar-options">
        <MenuPopovers
          auth={auth}
          authLoaded={authLoaded}
          config={config}
          onChangePath={path => history.push(path)}
          onChangeCurrency={context.changeCurrency}
          onChangeDateFormat={context.changeDateFormat}
          onChangeLanguage={context.changeLanguage}
          onLogout={context.logout}>
        </MenuPopovers>
      </div>
      <button 
        aria-label="Toggle navigation"
        className="navbar-toggler border-0 btn btn-light ml-1" 
        onClick={() => setMenu(!showMenu)}>
        {!showMenu && <i className="fas fa-bars" />}
        {showMenu && <i className="fas fa-times" />}
      </button>
    </NavbarWrapper>
  );
}