import React from 'react';
import NumberFormat from 'react-number-format';

export default class Numeric extends React.Component {
  state = {
    value: (this.props.value === null || this.props.value === undefined) ? '' : this.props.value
  };

  componentDidUpdate = prevProps => {
    if (prevProps.value !== this.props.value) {
      this.setState({ value: this.props.value });
    }
  };

  changeValue = async value => {
    await this.setState({ value });
    this.props.onChange(this.state.value);
  };

  render() {
    let { value } = this.state;
    if (this.props.currencyConversion && this.props.to) {
      value =
        this.state.value /
        this.props.currencyConversion[this.props.to.toUpperCase()];
      value = isNaN(value) ? this.state.value : value;
    }

    return (
      <NumberFormat
        className={this.props.className}
        decimalScale={this.props.decimalScale}
        format={this.props.format}
        placeholder={this.props.placeholder}
        allowEmptyFormatting={false}
        value={value}
        displayType={this.props.display}
        prefix={this.props.prefix}
        suffix={this.props.suffix}
        thousandSeparator={this.props.thousandSeparator}
        decimalSeparator={this.props.decimalSeparator}
        onValueChange={value => this.changeValue(isNaN(value.floatValue) ? '' : value.floatValue)}
      />
    );
  }
}
