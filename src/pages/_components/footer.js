import React from 'react';
import { Link } from 'react-router-dom';
import { translate } from '../../utils/intl-utils';
import styled from 'styled-components';

const FooterWrapper = styled.footer`
  &, * {
    box-sizing: border-box;
  }

  .container {
    .slogan span {
      font-size: 1.25rem;
    }

    .wrapper-buttons {
      button {
        display: block;
        margin: 0 auto;
        margin-bottom: 10px;
      }
    }
  }
`;

export default function Footer(props) {
  const { config } = props;

  return (
    <FooterWrapper className="mt-4 mb-4 pt-4 border-top">
      <div className="container text-body">
        <div className="row">
          <div className="col-md-7 row">
            <div className="col-12 slogan">
              <img src={config.appLogo} alt={config.appName} width={35} />{' '}
              <span>{config.appName}</span>
            </div>
            <div className="col-12">
              <p>{translate(config.translations, 'homeDescription')}</p>
            </div>
            <div className="col-12">
              <ul className="list-inline">
                <li className="list-inline-item">
                  <a
                    href="https://www.instagram.com/lideressociale3/"
                    title="Instagram"
                    target="_blank"
                    rel="noopener noreferrer">
                    <i className="fab fa-instagram fa-2x text-dark" />
                  </a>
                </li>
                <li className="list-inline-item">
                  <a
                    href="https://www.facebook.com/Lideres-Sociales-106902417680240/"
                    title="Facebook"
                    target="_blank"
                    rel="noopener noreferrer">
                    <i className="fab fa-facebook fa-2x text-dark" />
                  </a>
                </li>
                <li className="list-inline-item">
                  <a
                    href="https://twitter.com/LideresSociale3"
                    title="Twitter"
                    target="_blank"
                    rel="noopener noreferrer">
                    <i className="fab fa-twitter fa-2x text-dark" />
                  </a>
                </li>
                <li className="list-inline-item">
                  <a
                    href="mailto:lideressocialescolombia@gmail.com"
                    title="Gmail"
                    target="_blank"
                    rel="noopener noreferrer">
                    <i className="fas fa-envelope fa-2x text-dark"/>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div className="col-md-5">
            <ul className="list-group list-group-flush">
              <li className="list-group-item">
                <Link to="/contact">{translate(config.translations, 'mailContactSubject')}</Link>
              </li>
              <li className="list-group-item">
                <Link to="/app/dashboard">{translate(config.translations, 'complaint')}</Link>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </FooterWrapper>
  );
}
