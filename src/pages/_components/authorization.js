import React from 'react';
import { connect } from 'react-redux';
import { translate } from '../../utils/intl-utils';

export default (ComposedComponent, requiredClaims = []) => {
  class Authorization extends React.Component {
    state = {
      renderComponent: false
    };

    componentDidMount = () => {
      this.checkAuthentication(this.props);
    };

    componentDidUpdate = () => {
      this.checkAuthentication(this.props);
    };

    checkAuthentication = props => {
      const { config, user } = props;
      const { auth, authLoaded } = user;

      if (!authLoaded) {
        return;
      }

      if (!auth) {
        props.history.push(`/login?message=${translate(config.translations, 'authNotLogin')}`);
        return;
      } else if (requiredClaims.length > 0 && !auth.claims.filter(item => requiredClaims.includes(item)).length) {
        props.history.push(`/login?message=${translate(config.translations, 'authNotPriviliges')}`);
        return;
      }

      if (!this.state.renderComponent) {
        this.setState({ renderComponent: true });
      }
    };

    render() {
      return (
        <React.Fragment>
          {this.state.renderComponent ? (
            <ComposedComponent {...this.props} />
          ) : null}
        </React.Fragment>
      );
    }
  }

  return connect(state => {
    return {
      config: state.config,
      user: state.user
    };
  })(Authorization);
};
