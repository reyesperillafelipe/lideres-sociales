import React from 'react';
// import styled from 'styled-components';

export default function News(props) {
    const { news } = props;
    news.map((obj) => { 
        obj.dateForSorted = new Date(obj.publicationDate);
    });
    news.sort((a, b) => b.dateForSorted - a.dateForSorted);
    return (
        <React.Fragment>
        {news[0] && news.map((itemNew, index) => (
        <div key={index} className="card">
            <div className="card-body">
                <h5 className="card-title">{itemNew.title}</h5>
                <p className="card-text">{itemNew.description}.</p>
                <a href={itemNew.linkToPage} className="btn btn-primary">Conocer Más</a>
                <p className="card-text"><small className="text-muted">{itemNew.publicationDate}</small></p>
            </div>
            <img src={itemNew.image} className="card-img-bottom" alt={itemNew.title}/>
        </div>
        
        ))}

        </React.Fragment>
    );  
}
