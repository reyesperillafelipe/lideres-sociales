import React from 'react';
import styled from 'styled-components';

const ModalWrapper = styled.section`
  .modal {
    pointer-events: none;
  }
`;

export default class Modal extends React.Component {
  state = {
    modalId: `modal-${Date.now()}`
  }

  componentDidMount = () => {
    document.addEventListener('keyup', this.closeOnKeyup);
  }

  componentWillUnmount = () => {
    document.removeEventListener('keyup', this.closeOnKeyup);
  }

  closeOnKeyup = event => {
    const openedModals = Array.from(document.querySelectorAll('.modal.show'))
      .reduce((accum, modalEl) => accum.concat({ modalEl, order: parseInt(modalEl.getAttribute('order')) }), [])
      .sort((a, b) => a.order - b.order);
    const lastModal = openedModals[openedModals.length - 1].modalEl;

    if (this.modal === lastModal && event.keyCode === 27 && this.props.isOpen) {
      event.stopImmediatePropagation();
      this.props.onToggle();
    }
  }

  render() {
    const { modalId } = this.state;
    const { isOpen, children, title, onToggle } = this.props;
    const openedModals = document.querySelectorAll(`.modal.show:not(.${modalId}) > .modal-dialog`).length;
    const additionalZIndex = isOpen ? (openedModals * 10) : 0;

    return (
      <ModalWrapper>
        {
          isOpen && 
          <div 
            className="modal-backdrop fade show" 
            style={{ zIndex: (1040 + additionalZIndex) }} 
            onClick={onToggle}>
          </div>
        }
        {
          isOpen && 
          <div 
            ref={elem => this.modal = elem}
            role="dialog"
            title={title}
            order={openedModals}
            className={`${modalId} modal fade show`}
            style={{ display: 'block', zIndex: (1050 + additionalZIndex) }}>
            <div 
              className="modal-dialog modal-dialog-centered" 
              role="document">
              <div className="modal-content">
                <div className="modal-header">
                  <h5 className="modal-title">{title}</h5>
                  <button 
                    aria-label="Close"
                    type="button" 
                    className="close" 
                    data-dismiss="modal" 
                    onClick={onToggle}>
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div className="modal-body">
                  {children}
                </div>
              </div>
            </div>
          </div>
        }
      </ModalWrapper>
    );
  }

}