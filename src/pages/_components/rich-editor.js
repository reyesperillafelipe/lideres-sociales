import 'react-quill/dist/quill.snow.css'; 
import React from 'react';
import ReactQuill from 'react-quill';

export default class RichEditor extends React.Component {
  state = {
    value: this.props.value || ''
  };

  componentDidUpdate(prevProps) {
    if (prevProps.value !== this.props.value) {
      this.setState({
        value: this.props.value
      });
    }
  }

  changeValue = async value => {
    await this.setState({ value });
    this.props.onChange(this.state.value);
  };

  render() {
    return (
      <ReactQuill 
        value={this.state.value}
        onChange={value => this.changeValue(value)}
      />
    );
  }
}
