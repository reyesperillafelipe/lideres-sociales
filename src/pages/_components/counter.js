import React from 'react';
import NumberFormat from 'react-number-format';

export default class Counter extends React.Component {
  state = {
    max: this.props.max || 100,
    min: this.props.min || 0,
    value: (this.props.value === null || this.props.value === undefined) ? '' : this.props.value
  };

  componentDidUpdate(prevProps) {
    if (prevProps.value !== this.props.value) {
      this.setState({
        value: this.props.value
      });
    }
  }

  changeValue = async value => {
    const { min, max } = this.props;

    if (value < min) {
      value = '';
    }
    if (value > max) {
      value = max;
    }

    await this.setState({ value });
    this.props.onChange(this.state.value);
  };

  increaseValue = async () => {
    let value = (this.state.value === null || this.state.value === undefined)
      ? this.props.min
      : this.state.value + 1;
    if (value < this.props.max) {
      await this.setState({
        value: parseInt(value, 10)
      });
      this.props.onChange(this.state.value);
    }
  };

  decreaseValue = async () => {
    let value = (this.state.value === null || this.state.value === undefined)
      ? this.props.min
      : this.state.value - 1;
    if (value >= this.props.min) {
      await this.setState({
        value: parseInt(value, 10)
      });
      this.props.onChange(this.state.value);
    }
  };

  render() {
    return (
      <div>
        <div className="input-group">
          <NumberFormat
            className="form-control"
            isNumericString={true}
            allowEmptyFormatting={false}
            value={this.state.value}
            onValueChange={value => this.changeValue(isNaN(value.floatValue) ? '' : value.floatValue)
            }
          />
          <span className="input-group-append">
            <button
              className="btn btn-outline-primary rounded-0"
              type="button"
              onClick={this.decreaseValue}
            >
              <i className="fa fa-minus" aria-hidden="true" />
            </button>
            <button
              className="btn btn-outline-primary"
              type="button"
              onClick={this.increaseValue}
            >
              <i className="fa fa-plus" aria-hidden="true" />
            </button>
          </span>
        </div>
      </div>
    );
  }
}
