import React from 'react';
import { Helmet } from 'react-helmet';
import styled from 'styled-components';

const NavigationBarWrapper = styled.section`
  &, * {
    box-sizing: border-box;
  }

  display: flex;
  align-items: center;
  justify-content: center;

  .left-box {
    flex-grow: 0;
    text-align: left;
  }

  .center-box {
    flex-grow: 1;
    max-width: 100%;
    text-align: center;

    h1 {
      font-size: 2rem;
    }
  }

  .right-box {
    flex-grow: 0;
    text-align: right;
  }

  @media screen and (max-width: 768px) {
    .center-box h1 {
      font-size: 1.5rem;
    }
  }
`;

export default function NavigationBar(props) {
  const { config, title, description, btnLeft, btnRight } = props;
  const keywords = (props.keywords || []).concat('sample');

  return (
    <React.Fragment>
      <Helmet>
        <html lang={config.preferences.language} />
        <title>{props.title}</title>
        {/* GOOGLE */}
        <meta charset="utf-8" />
        <meta name="description" content={props.description} />
        <meta name="keywords" content={keywords.join(', ')} />
        <meta name="robots" content="index, follow" />
        {/* FACEBOOK */}
        <meta property="og:title" content={props.title} />
        <meta property="og:type" content="business.business" />
        <meta property="og:site_name" content={config.appName} />
        <meta property="og:description" content={props.title} />
        {/* TWITTER  */}
        <meta name="twitter:card" content={props.description} />
        <meta name="twitter:creator" content="@jhonfredynova" />
        <meta name="twitter:site" content="@jhonfredynova" />
      </Helmet>
      <NavigationBarWrapper className="container-fluid mb-4 border-bottom">
        {btnLeft && <div className="left-box">{btnLeft}</div>}
        <div className="center-box" role="heading">
          <h1 className="mt-2">{title}</h1>
          <p className="mt-2">{description}</p>
        </div>
        {btnRight && <div className="right-box">{btnRight}</div>}      
      </NavigationBarWrapper>
    </React.Fragment>
  );
}