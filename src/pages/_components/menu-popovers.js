import React, { useState } from 'react';
import Select from 'react-select';
import Popover from '../_components/popover';
import { translate } from '../../utils/intl-utils';

export default function MenuPopovers(props) {
  const [elemPopoverAdmin, setElemPopoverAdmin] = useState(null);
  const [elemPopoverPreferences, setElemPopoverPreferences] = useState(null);
  const [elemPopoverSession, setElemPopoverSession] = useState(null);
  const [showPopoverAdmin, setPopoverAdmin] = useState(false);
  const [showPopoverPreferences, setPopoverPreferences] = useState(false);
  const [showPopoverSession, setPopoverSession] = useState(false);

  const { 
    auth, authLoaded, config, 
    onChangePath, onChangeDateFormat, onChangeLanguage, onLogout 
  } = props;
  // const { 
  //   auth, authLoaded, config, 
  //   onChangePath, onChangeCurrency, onChangeDateFormat, onChangeLanguage, onLogout 
  // } = props;
  // const currencies = config.intlData.currencies || []; 
  const dateFormats = ['DD/MM/YYYY', 'MM/DD/YYYY', 'YYYY/MM/DD'].map(item => ({ label: item, value: item }));
  const languages = config.intlData.languages || [];

  const clickAdminMenu = path => {
    setPopoverAdmin(false);
    onChangePath(path);
  };

  const clickSessionMenu = path => {
    setPopoverSession(false);
    onChangePath(path);
  };

  return (
    <React.Fragment>
      {/* POPOVER PREFERENCES */}
        <button
        ref={elem => setElemPopoverPreferences(elem)}
        className="btn btn-light ml-1"
        title={translate(config.translations, 'preferences')}
        onClick={() => setPopoverPreferences(!showPopoverPreferences)}>
        <i className="fas fa-cog" />
      </button>
      <Popover
        isOpen={showPopoverPreferences}
        target={elemPopoverPreferences}
        style={{ width: 200 }}
        onToggle={() => setPopoverPreferences(!showPopoverPreferences)}>
        <h3 className="popover-header">{translate(config.translations, 'preferences')}</h3>
        <div className="popover-body">
          <div className="form-group">
            <label>{translate(config.translations, 'dateFormat')}</label>
            <Select
              placeholder={translate(config.translations, 'select')}
              options={dateFormats}
              value={dateFormats.find(item => item.value === config.preferences.dateFormat)}
              onChange={option => onChangeDateFormat(option.value)} />
          </div>
          <div className="form-group">
            <label>{translate(config.translations, 'language')}</label>
            <Select
              placeholder={translate(config.translations, 'select')}
              options={languages.filter(item => config.appLanguages.includes(item.value))}
              value={languages.find(item => item.value === config.preferences.language)}
              onChange={option => onChangeLanguage(option.value)} />
          </div>
          {/* <div className="form-group">
            <label>{translate(config.translations, 'currency')}</label>
            <Select
              placeholder={translate(config.translations, 'select')}
              options={currencies}
              value={currencies.find(item => item.value === config.preferences.currency)}
              onChange={option => onChangeCurrency(option.value)} />
          </div> */}
        </div>
      </Popover>
      {/* POPOVER ADMIN */}
      {
        authLoaded && auth && auth.claims.find(item => item === 'isAdmin') &&
        <React.Fragment>
          <button
            ref={elem => setElemPopoverAdmin(elem)}
            className="btn btn-light ml-1"
            title={translate(config.translations, 'admin')}
            onClick={() => setPopoverAdmin(!showPopoverAdmin)}>
            <i className="fas fa-lock" />
          </button>
          <Popover
            isOpen={showPopoverAdmin}
            target={elemPopoverAdmin}
            style={{ width: 200 }}
            onToggle={() => setPopoverAdmin(!showPopoverAdmin)}>
            <h3 className="popover-header">{translate(config.translations, 'admin')}</h3>
            <div className="popover-body">
              <ul className="list-group list-group-flush">
                <li className="list-group-item">
                  <button
                    className="btn p-0"
                    onClick={() => clickAdminMenu('/admin/catalogs')}>
                    {translate(config.translations, 'catalogs')}
                  </button>
                </li>
                <li className="list-group-item">
                  <button
                    className="btn p-0"
                    onClick={() => clickAdminMenu('/admin/configuration')}>
                    {translate(config.translations, 'configuration')}
                  </button>
                </li>
                <li className="list-group-item">
                  <button
                    className="btn p-0"
                    onClick={() => clickAdminMenu('/admin/locales')}>
                    {translate(config.translations, 'locales')}
                  </button>
                </li>
                <li className="list-group-item">
                  <button
                    className="btn p-0"
                    onClick={() => clickAdminMenu('/admin/users')}>
                    {translate(config.translations, 'users')}
                  </button>
                </li>
              </ul>
            </div>
          </Popover>
        </React.Fragment>
      }
      {/* POPOVER SESSION */}
      {
        auth &&
        <React.Fragment>
          <button
            ref={elem => setElemPopoverSession(elem)}
            className="btn btn-light ml-1"
            title={translate(config.translations, 'yourAccount')}
            onClick={() => setPopoverSession(!showPopoverSession)}>
            <i className="fas fa-user" />
          </button>
          <Popover
            isOpen={showPopoverSession}
            target={elemPopoverSession}
            style={{ width: 200 }}
            onToggle={() => setPopoverSession(!showPopoverSession)}>
            <h3 className="popover-header">{translate(config.translations, 'yourAccount')}</h3>
            <div className="popover-body">
              <ul className="list-group list-group-flush">
                <li className="list-group-item">
                  <button
                    className="btn p-0"
                    onClick={() => clickSessionMenu('/app/user/profile')}>
                    {translate(config.translations, 'profile')}
                  </button>
                </li>
                <li className="list-group-item">
                  <button
                    className="btn p-0"
                    onClick={() => {
                      setPopoverSession(false);
                      onLogout();
                    }}>
                    {translate(config.translations, 'logout')}
                  </button>
                </li>
              </ul>
            </div>
          </Popover>
        </React.Fragment>
      }
    </React.Fragment>
  );
}