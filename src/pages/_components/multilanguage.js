import React from 'react';
import RichEditor from './rich-editor';
import { isHtml } from '../../utils/validation-utils';

export default class Multilanguage extends React.Component {
  state = {
    activeTab: 'en',
    isHtml: null,
    languages: this.props.languages || [],
    value: this.props.value || {}
  };

  componentDidUpdate(prevProps) {
    if (prevProps.activeTab !== this.props.activeTab) {
      this.setState({ activeTab: this.props.activeTab });
    }

    if (prevProps.isHtml !== this.props.isHtml) {
      this.setState({ isHtml: this.props.isHtml });
    }

    if (prevProps.value !== this.props.value) {
      this.setState({ value: this.props.value });
    }
  }

  changeValue = async (lang, value) => {
    await this.setState({ value: Object.assign(this.state.value, { [lang]: value }) });
    this.props.onChange(this.state.value);
  };

  render() {
    const editorIsHtml = (this.state.isHtml === null)
      ? isHtml(this.state.value[this.state.activeTab])
      : this.state.isHtml;

    return (
      <div>
        <ul className="nav nav-tabs mb-4">
          {this.props.languages.map(lang => (
            <li key={lang} className="nav-item">
              <a 
                className={`nav-link ${this.state.activeTab === lang && 'active'}`}
                onClick={() => this.setState({ activeTab: lang })}>
                {lang}
              </a>
            </li>
          ))}
        </ul>
        <div className="tab-content">
          {this.props.languages.map(lang => (
            <div className={`tab-pane ${this.state.activeTab === lang && 'active show'}`} key={lang} title={lang}>
              {editorIsHtml 
                ? (<RichEditor
                    value={this.state.value[lang] || ''}
                    onChange={value => this.changeValue(lang, value)} />
                  ) 
                : (<textarea
                    value={this.state.value[lang] || ''}
                    className="form-control"
                    rows="4"
                    onChange={e => this.changeValue(lang, e.target.value) }/>
                  )
              }
            </div>
          ))}
        </div>
        <div className="btn-group">
          <button
            type="button"
            className={`btn btn-default ${!editorIsHtml && 'btn-primary'}`}
            onClick={() => this.setState({ isHtml: false })}>
            TEXT
          </button>
          <button
            type="button"
            className={`btn btn-default ${editorIsHtml && 'btn-primary'}`}
            onClick={() => this.setState({ isHtml: true })}>
            HTML
          </button>
        </div>
      </div>
    );
  }
}
