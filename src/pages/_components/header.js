import React from 'react';
import styled from 'styled-components';
import Navbar from './navbar';
import NavigationBar from './navigation-bar';

const HeaderWrapper = styled.header`
  &, * {
    box-sizing: border-box;
  }
`;

export default function Header(props) {
  const { config, user, title, description, history, showHeading, btnLeft, btnRight } = props;

  return (
    <HeaderWrapper>
      <Navbar
        config={config}
        user={user}
        history={history}>
      </Navbar>
      {
        showHeading &&
        <NavigationBar
          config={config}
          title={title}
          description={description}
          btnLeft={btnLeft}
          btnRight={btnRight}>
        </NavigationBar>
      }
    </HeaderWrapper>
  );
}
