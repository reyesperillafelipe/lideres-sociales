import React from 'react';
import styled from 'styled-components';

const PopoverWrapper = styled.div`
  &, * {
    box-sizing: border-box;
  }

  display: none;
  background: white;
  border: 1px solid #ccc;
  position: absolute;
  top: 0px;
  left: 0px;
  width: 300px;
  z-index: 1000;

  &.show {
    display: block;
  }
`;

export default class Popover extends React.Component {

  state = {
    screenWidth: 0
  }

  componentDidMount = () => {
    document.addEventListener('click', this.onDocumentClick);
    window.addEventListener('resize', this.onResizeWindow);
    window.dispatchEvent(new CustomEvent('resize'));
  };

  componentWillUnMount = () => {
    document.removeEventListener('click', this.onDocumentClick);
    window.removeEventListener('resize', this.onResizeWindow);
  }

  onDocumentClick = event => {
    const { isOpen, target } = this.props;

    if (isOpen && !target.contains(event.target) && !this.popover.contains(event.target)) {
      this.props.onToggle();
    }
  }

  onResizeWindow = () => {
    this.setState({ screenWidth: window.innerWidth });
  }

  getPopoverPosition = targetElem => {
    const { style } = this.props;
    const popoverWidth = (style && style.width) || 300;
    const targetPosition = targetElem ? targetElem.getBoundingClientRect() : null;
    const targetWidth = targetElem ? targetElem.clientWidth : 0;
    const targetHeight = targetElem ? targetElem.clientHeight : 0;
    const targetPositionX = targetPosition ? targetPosition.x : 0;
    const targetPositionY = targetPosition ? targetPosition.y : 0;
    const screenWidth = (this.state.screenWidth - 10);  

    let popoverPositionX = targetPositionX + (targetWidth / 2) - (popoverWidth / 2);
    let popoverPositionY = targetPositionY + (targetHeight + 10);

    return {
      x: (popoverPositionX + popoverWidth) > screenWidth ? (screenWidth - popoverWidth - 5) : popoverPositionX,
      y: popoverPositionY
    };
  }

  render() {
    const { isOpen, className, children, target, style } = this.props;
    const targetPosition = this.getPopoverPosition(target);

    return (
      <PopoverWrapper
        ref={elem => this.popover = elem}
        aria-expanded={isOpen} 
        className={`${className} ${isOpen && 'show'}`}
        style={{ ...style, transform: `translate3d(${targetPosition.x}px, ${targetPosition.y}px, 0px)` }}>
        {children}
      </PopoverWrapper>
    );
  }
}