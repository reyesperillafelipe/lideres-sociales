import React from 'react';
import { translate } from '../../utils/intl-utils';

export default function MenuMain(props) {
  const { auth, authLoaded, config, currentPath, onChangePath } = props;

  return (
    <ul className="ml-auto navbar-nav">
      {/* APP */}
      {authLoaded && auth && auth.claims.find(item => item === 'isRegistered') && (
        <React.Fragment>
          <a
            role="menuitem"
            className={`nav-link ${currentPath === '/app/dashboard' && 'active'}`}
            href="/app/dashboard"
            title={translate(config.translations, 'complaint')}
            onClick={e => {
              e.preventDefault();
              onChangePath(e.currentTarget.pathname);
            }}>
            <i className="fas fa-assistive-listening-systems mr-1"></i>
            <span className="d-md-none d-lg-inline-block">
              {translate(config.translations, 'complaint')}
            </span>
          </a>
        </React.Fragment>
      )}
      {/* HOME */}
      {!auth && (
        <React.Fragment>
          <a
            role="menuitem"
            className={`nav-link ${currentPath === '/' && 'active'}`}
            href="/"
            onClick={e => {
              e.preventDefault();
              onChangePath(e.currentTarget.pathname);
            }}>
            {translate(config.translations, 'home')}
          </a>
          <a
            role="menuitem"
            className={`nav-link btn btn-info text-white ml-md-2 mb-2 mb-md-0 ${currentPath === '/login' && 'active'}`}
            href="/login"
            onClick={e => {
              e.preventDefault();
              onChangePath(e.currentTarget.pathname);
            }}>
            {translate(config.translations, 'login')}
          </a>
          <a
            role="menuitem"
            className={`nav-link btn btn-primary text-white ml-md-2 ${currentPath === '/register' && 'active'}`}
            href="/register"
            onClick={e => {
              e.preventDefault();
              onChangePath(e.currentTarget.pathname);
            }}>
            {translate(config.translations, 'register')}
          </a>
        </React.Fragment>
      )}
    </ul>
  );
}