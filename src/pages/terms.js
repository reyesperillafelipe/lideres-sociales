import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import store from '../store/store';
import { translate } from '../utils/intl-utils';
import { getTermsInfo } from '../store/actions/home-actions';
import Header from './_components/header';
import Footer from './_components/footer';

const TermsWrapper = styled.main`
  &, * {
    box-sizing: border-box;
  }
`;

class Terms extends React.Component {
  static getInitialState = () => {
    return Promise.all([
      store.dispatch(getTermsInfo())
    ]);
  }

  state = {
    isLoading: false
  };

  componentDidMount = () => {
    Terms.getInitialState();
  }

  render() {
    const { config, home, user } = this.props;

    return (
      <React.Fragment>
        <Header
          config={config}
          user={user}
          title={translate(config.translations, 'termsTitle')}
          description={translate(config.translations, 'termsDescription')}
          history={this.props.history}
          showHeading={true} />
        <TermsWrapper className="container">
          <article
            dangerouslySetInnerHTML={{
              __html: (home.termsInfo && home.termsInfo.value && home.termsInfo.value[config.preferences.language])
            }} />
        </TermsWrapper>
        <Footer 
          config={config}
          routerHistory={this.props.history} />
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.user.auth,
  config: state.config,
  home: state.home,
  user: state.user
});

export default connect(mapStateToProps)(Terms);
