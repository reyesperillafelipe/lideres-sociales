import express from 'express';
import { firebaseAdmin } from '../firebase';
import { getDbDocument, getDbQuery } from '../../utils/database-utils';
import { getLocaleModel } from '../../utils/model-utils';
import { getBackendError } from '../../utils/response-utils';
import { validateModel } from '../../utils/validation-utils';
import { authorization } from '../middlewares';

// helpers
const router = express.Router();
const getModelValidation = async (req, model) => {
  // validating structure
  const validationResponse = validateModel(req, model, getLocaleModel(req.config.appLanguages));
  const errors = validationResponse.errors;
  let data = validationResponse.data;

  return { errors, data };
};

// routes
router.get('/', async (req, res) => {
  try {
    const db = firebaseAdmin.firestore();
    const response = await getDbQuery(db, 'locales', req.query);
    
    res.json(response);
  } catch (e) {
    const error = getBackendError(e);
    res.status(500).json(error);
  }
});

router.get('/:id', async (req, res) => {
  try {
    const db = firebaseAdmin.firestore();
    const response = await getDbDocument(db, 'locales', req.params.id);
    res.json(response);
  } catch (e) {
    const error = getBackendError(e);
    res.status(500).json(error);
  }
});

router.post('/', authorization(['isAdmin']), async (req, res) => {
  try {
    const model = { ...req.body, createdAt: Date.now() };
    const db = firebaseAdmin.firestore();
    const { errors, data } = await getModelValidation(req, model);

    if (Object.keys(errors).length > 0) {
      return res.status(400).json({ errors });
    }

    let response = await db.collection('locales').add(data);

    response = { ...data, id: response.id };
    res.json(response);
  } catch (e) {
    const error = getBackendError(e);
    res.status(500).json(error);
  }
});

router.patch('/:id', authorization(['isAdmin']), async (req, res) => {
  try {
    const db = firebaseAdmin.firestore();
    const dbData = await getDbDocument(db, 'locales', req.params.id);
    const model = Object.assign(dbData, { ...req.body, updatedAt: Date.now() });
    const { errors, data } = await getModelValidation(req, model);

    if (Object.keys(errors).length > 0) {
      return res.status(400).json({ errors });
    }

    await db
      .collection('locales')
      .doc(req.params.id)
      .update(data);

    const response = { ...data, id: req.params.id };
    res.json(response);
  } catch (e) {
    const error = getBackendError(e);
    res.status(500).json(error);
  }
});

router.delete('/:id', authorization(['isAdmin']), async (req, res) => {
  try {
    const { id } = req.params;
    const db = firebaseAdmin.firestore();
    const response = await getDbDocument(db, 'locales', req.params.id);

    await db
      .collection('locales')
      .doc(id)
      .delete();

    res.json(response);
  } catch (e) {
    const error = getBackendError(e);
    res.status(500).json(error);
  }
});

export default router;
