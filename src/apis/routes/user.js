import express from 'express';
import { firebaseAdmin, firebaseClient } from '../firebase';
import { getDbQuery, getDbDocument } from '../../utils/database-utils';
import { translate } from '../../utils/intl-utils';
import { getUserModel } from '../../utils/model-utils';
import { validateModel } from '../../utils/validation-utils';
import { getBackendError } from '../../utils/response-utils';
import { authorization } from '../middlewares';

// helpers
const router = express.Router();
const getModelValidation = async (req, model) => {
  // validating structure
  const passwordIsRequired = model.id ? false : true;
  const validationResponse = validateModel(req, model, getUserModel(model, passwordIsRequired));
  const errors = validationResponse.errors;
  let data = validationResponse.data;

  return { errors, data };
};

// services
router.get('/', authorization(['isAdmin']), async (req, res) => {
  try {
    const db = firebaseAdmin.firestore();
    const response = await getDbQuery(db, 'users', req.query);

    res.json(response);
  } catch (e) {
    const error = getBackendError(e);
    res.status(500).json(error);
  }
});

router.get('/me', authorization(['isRegistered']), async (req, res) => {
  try {
    let user = req.user;

    if (user) {
      const db = firebaseAdmin.firestore();
      const profile = await getDbDocument(db, 'users', user.uid);
      user = Object.assign(user, { profile });
    }

    res.json(user);
  } catch (e) {
    const error = getBackendError(e);
    res.status(500).json(error);
  }
});

router.get('/:id', authorization(['isRegistered']), async (req, res) => {
  try {
    const db = firebaseAdmin.firestore();
    const response = await getDbDocument(db, 'users', req.params.id);

    res.json(response);
  } catch (e) {
    const error = getBackendError(e);
    res.status(500).json(error);
  }
});

router.get('/:id/claims', authorization(['isAdmin']), async (req, res) => {
  try {
    const { id: userId } = req.params;
    const user = await firebaseAdmin.auth().getUser(userId);
    let response = [];

    if (user.customClaims) {
      response = Object.keys(user.customClaims);
    }

    res.send(response);
  } catch (e) {
    const error = getBackendError(e);
    res.status(500).json(error);
  }
});

router.post('/:id/claims', authorization(['isAdmin']), async (req, res) => {
  try {
    const { id: userId } = req.params;
    const { claim } = req.body;

    const user = await firebaseAdmin.auth().getUser(userId);
    const claims = { ...user.customClaims, [claim]: true };
    await firebaseAdmin.auth().setCustomUserClaims(userId, claims);

    res.json(claim);
  } catch (e) {
    const error = getBackendError(e);
    res.status(500).json(error);
  }
});

router.patch('/:id/claims', authorization(['isAdmin']), async (req, res) => {
  try {
    const { id: userId } = req.params;
    const { claim } = req.body;

    const user = await firebaseAdmin.auth().getUser(userId);
    delete user.customClaims[claim];
    await firebaseAdmin.auth().setCustomUserClaims(userId, user.customClaims);

    res.json(claim);
  } catch (e) {
    const error = getBackendError(e);
    res.status(500).json(error);
  }
});

router.patch('/:id', authorization(['isRegistered']), async (req, res) => {
  try {
    const db = firebaseAdmin.firestore();
    const dbData = await getDbDocument(db, 'users', req.params.id);
    const userData = Object.keys(req.body).reduce((accum, key) => {
      return !['email'].includes(key) 
        ? Object.assign(accum, { [key]: req.body[key] }) 
        : accum;
    }, {});
    const model = Object.assign(dbData, { ...userData, updatedAt: Date.now() });
    const { errors, data } = await getModelValidation(req, model);

    if (Object.keys(errors).length > 0) {
      res.status(400).json({ errors });
      return;
    }
    
    const dataToSave = Object.keys(data).reduce((accum, key) => {
      return !['password', 'passwordConfirmation'].includes(key) 
        ? Object.assign(accum, { [key]: data[key] }) 
        : accum;
    }, {});
    await db
      .collection('users')
      .doc(req.params.id)
      .update(dataToSave);

    const response = { ...data, id: req.params.id };
    res.json(response);
  } catch (e) {
    const error = getBackendError(e);
    res.status(500).json(error);
  }
});

router.post('/change-password', authorization(['isRegistered']), async (req, res) => {
  try {
    const { body } = req;
    const { data, errors } = validateModel(req, body, [
      {
        property: 'password',
        type: 'string',
        rules: [{ required: true }, { minLength: 6 }]
      },
      {
        property: 'passwordConfirmation',
        type: 'string',
        rules: [{ required: true }, { minLength: 6 }]
      }
    ]);

    if (Object.keys(errors).length > 0) {
      res.status(400).json({ errors });
      return;
    }

    await firebaseAdmin.auth().updateUser(req.user.uid, {
      password: data.password
    });
    res.json({ message: translate(req.translations, 'sucessfulOperation') });
  } catch (e) {
    const error = getBackendError(e);
    res.status(500).json(error);
  }
});

router.post('/recover-password', async (req, res) => {
  try {
    const { body } = req;
    const { data, errors } = validateModel(req, body, [
      {
        property: 'email',
        type: 'string',
        rules: [{ required: true }, { format: 'email' }]
      }
    ]);

    if (Object.keys(errors).length > 0) {
      res.status(400).json({ errors });
      return;
    }

    await firebaseClient.auth().sendPasswordResetEmail(data.email);
    res.json({ message: translate(req.translations, 'recoverPasswordSuccess') });
  } catch (e) {
    const error = getBackendError(e);
    res.status(500).json(error);
  }
});

router.post('/login', async (req, res) => {
  try {
    const model = req.body;
    const { errors, data } = validateModel(req, model, [
      {
        property: 'email',
        type: 'string',
        rules: [{ required: true }, { format: 'email' }]
      },
      {
        property: 'password',
        type: 'string',
        rules: [{ required: true }]
      }
    ]);

    if (Object.keys(errors).length > 0) {
      return res.status(400).json({ errors });
    }
    
    const response = await firebaseClient
      .auth()
      .signInWithEmailAndPassword(data.email, data.password);
    const token = await response.user.getIdToken();

    res.json({ token });
  } catch (e) {
    const error = getBackendError(e);
    res.status(500).json(error);
  }
});

router.post('/register', async (req, res) => {
  try {
    const db = firebaseAdmin.firestore();
    const userData = Object.keys(req.body).reduce((accum, key) => {
      return !['id'].includes(key) 
        ? Object.assign(accum, { [key]: req.body[key] }) 
        : accum;
    }, {});
    const model = { ...userData, createdAt: Date.now() };
    const { errors, data } = await getModelValidation(req, model);

    if (Object.keys(errors).length > 0) {
      return res.status(400).json({ errors });
    }

    // registerig user
    const userRegistered = await firebaseClient
      .auth()
      .createUserWithEmailAndPassword(data.email, data.password);
    const { uid } = userRegistered.user;

    const dataToSave = Object.keys(data).reduce((accum, key) => {
      return !['password', 'passwordConfirmation'].includes(key) 
        ? Object.assign(accum, { [key]: data[key] }) 
        : accum;
    }, {});
    await db
      .collection('users')
      .doc(uid)
      .set(dataToSave);
    await firebaseAdmin.auth().setCustomUserClaims(uid, { isRegistered: true });

    // loging user to refresh custom claims
    const response = await firebaseClient
      .auth()
      .signInWithEmailAndPassword(data.email, data.password);
    const token = await response.user.getIdToken();

    res.json({ token });
  } catch (e) {
    const error = getBackendError(e);
    res.status(500).json(error);
  }
});

export default router;
