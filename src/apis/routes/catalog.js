import express from 'express';
import { firebaseAdmin } from '../firebase';
import { authorization } from '../middlewares';
import { getDbDocument, getDbQuery, getDbQueryByIds } from '../../utils/database-utils';
import { getCatalogModel } from '../../utils/model-utils';
import { getBackendError } from '../../utils/response-utils';
import { validateModel } from '../../utils/validation-utils';

// helpers
const router = express.Router();
const getModelValidation = async (req, model, associations) => {  
  // validating structure
  const validationResponse = validateModel(req, model, getCatalogModel(associations, req.config.appLanguages));
  const errors = validationResponse.errors;
  let data = validationResponse.data;

  return { errors, data };
};

// routes
router.get('/', async (req, res) => {
  try {
    const db = firebaseAdmin.firestore();
    const response = await getDbQuery(db, 'catalogs', req.query);

    res.json(response); 
  } catch (e) {
    const error = getBackendError(e);
    res.status(500).json(error);
  }
});

router.get('/:id', async (req, res) => {
  try {
    const db = firebaseAdmin.firestore();
    const response = await getDbDocument(db, 'catalogs', req.params.id);

    res.json(response);
  } catch (e) {
    const error = getBackendError(e);
    res.status(500).json(error);
  }
});

router.post('/', authorization(['isAdmin']), async (req, res) => {
  try {
    const model = { ...req.body, createdAt: Date.now() };
    const db = firebaseAdmin.firestore();
    const parents = await getDbQueryByIds(db, 'catalogs', [model.parent]);
    const { errors, data } = await getModelValidation(req, model, { parents });

    if (Object.keys(errors).length > 0) {
      return res.status(400).json({ errors });
    }

    let response = await db.collection('catalogs').add(data);

    response = { ...data, id: response.id };
    res.json(response);
  } catch (e) {
    const error = getBackendError(e);
    res.status(500).json(error);
  }
});

router.patch('/:id', authorization(['isAdmin']), async (req, res) => {
  try {
    const db = firebaseAdmin.firestore();
    const dbData = await getDbDocument(db, 'catalogs', req.params.id);
    const model = Object.assign(dbData, { ...req.body, updatedAt: Date.now() });
    const parents = await getDbQueryByIds(db, 'catalogs', [model.parent]);
    const { errors, data } = await getModelValidation(req, model, { parents });

    if (Object.keys(errors).length > 0) {
      return res.status(400).json({ errors });
    }

    await db
      .collection('catalogs')
      .doc(req.params.id)
      .update(data);

    const response = { ...data, id: req.params.id };
    res.json(response);
  } catch (e) {
    const error = getBackendError(e);
    res.status(500).json(error);
  }
});

export default router;
