import express from 'express';
import { firebaseAdmin } from '../firebase';
// import { getDbQuery, getDbDocument } from '../../utils/database-utils';
// import { translate } from '../../utils/intl-utils';
import { getReportModel } from '../../utils/model-utils';
import { validateModel } from '../../utils/validation-utils';
import { getBackendError } from '../../utils/response-utils';
// import { authorization } from '../middlewares';

// helpers
const router = express.Router();
const getModelValidation = async (req, model) => {
  // validating structure
  const passwordIsRequired = model.id ? false : true;
  const validationResponse = validateModel(req, model, getReportModel(model, passwordIsRequired));
  const errors = validationResponse.errors;
  let data = validationResponse.data;

  return { errors, data };
};

// services
router.post('/report', async (req, res) => {
  try {
    const db = firebaseAdmin.firestore();
    const reportData = Object.keys(req.body).reduce((accum, key) => {
        return !['id'].includes(key) 
          ? Object.assign(accum, { [key]: req.body[key] }) 
          : accum;
    }, {});
    const model = { ...reportData, createdAt: Date.now() };
    const { errors, data } = await getModelValidation(req, model);
    if (Object.keys(errors).length > 0) {
      return res.status(400).json({ errors });
    }
    console.warn('data =>',data);
    await db.collection('reports').doc().set(reportData);
    res.json({created: true});
  } catch (e) {
    const error = getBackendError(e);
    res.status(500).json(error);
  }
});

export default router;
