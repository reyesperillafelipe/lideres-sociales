import * as functions from 'firebase-functions';
import axios from 'axios';
import express from 'express';
import cache from 'memory-cache';
import { authorization } from '../middlewares';
import { firebaseAdmin } from '../firebase';
import { getDbQuery, getDbDocument } from '../../utils/database-utils';
import { getIntlData } from '../../utils/intl-utils';
import { getConfigurationModel } from '../../utils/model-utils';
import { getBackendError } from '../../utils/response-utils';
import { validateModel } from '../../utils/validation-utils';

const router = express.Router();

// services
router.post('/sync', authorization(['isAdmin']), async (req, res) => {
  try {
    const { app, intl, rate } = functions.config();
    const db = firebaseAdmin.firestore();
    const dbData = await getDbDocument(db, 'configuration', app.projectid);
    const model = Object.assign(dbData, { ...req.body, appLastSync: Date.now() });
    const { errors, data: configurationData } = validateModel(req, model, getConfigurationModel());

    if (Object.keys(errors).length > 0) {
      res.status(400).json({ errors });
      return;
    }

    await db
      .collection('configuration')
      .doc(app.projectid)
      .set(configurationData);

    const intlData = await getIntlData(req, axios, cache, db, intl, rate);
    const response = { ...configurationData, intlData };
    
    res.json(response);
  } catch (e) {
    const error = getBackendError(e);
    res.status(500).json(error);
  }
});

router.get('/test', async (req, res) => {
  try {
    const db = firebaseAdmin.firestore();
    const response = await getDbQuery(db, 'locales', {});

    res.json(response);
  } catch (e) {
    const error = getBackendError(e);
    res.status(500).json(error);
  }
});

export default router;
