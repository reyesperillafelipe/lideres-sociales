import * as functions from 'firebase-functions';

export const subscriptionControl = functions.pubsub
  .schedule('0 6 * * *')
  .timeZone('America/Bogota')
  .onRun(async () => {
    try {
      console.warn('Crono running');
    } catch (error) {
      console.error('Error: running crono service', error);
    }
  });