import * as functions from 'firebase-functions';
import * as sendgrid from '@sendgrid/mail';
import store from '../store/store';
import { setConfiguration } from '../store/actions/config-actions';
import { translate } from '../utils/intl-utils';
import { sendNotification } from '../utils/mail-utils';
const { app } = functions.config();

// events
export const onNewUser = functions.auth
  .user()
  .onCreate(async user => {
    try {
      const { config } = store.getState();
      const { sendgrid: sendgridConfig } = functions.config();
      const currentLanguage = (user.preferences && user.preferences.language) || 'en';
      const currentTranslations = config.intlData.locales[currentLanguage];
      sendgrid.setApiKey(sendgridConfig.key);

      const configEmail = { 
        ...functions.config(), 
        translations: currentTranslations
      };
      const emailData = {
        to: user.email,
        subject: translate(currentTranslations, 'sampleSubject'),
        message: translate(currentTranslations, 'sampleMessage', { username: user.email })
      };
      await sendNotification(configEmail, sendgrid, emailData);
    } catch (e) {
      console.error('Error: running event onNewUser', e);
    }
  });

  export const updateConfiguration = functions.firestore
    .document(`configuration/${app.projectid}`)
    .onUpdate(change => {
      const newConfig = change.after.data();
      store.dispatch(setConfiguration(newConfig));
    });
