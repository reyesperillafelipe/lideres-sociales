import fs from 'fs';
import os from 'os';
import path from 'path';
import Busboy from 'busboy';
import { translate } from '../utils/intl-utils';
import { getBackendError } from '../utils/response-utils';
import { firebaseAdmin } from './firebase';

export function authorization(requiredClaims) {
  return async (req, res, next) => {
    try {
      let token = req.headers['authorization'] || '';

      // checking empty token
      if (!token || !token.startsWith('Bearer ')) {
        res.status(403).json({ message: translate(req.translations, 'authNotLogin') });
        return;
      }

      // chacking user get info from token
      token = token.split('Bearer ')[1];
      req.user = await firebaseAdmin.auth().verifyIdToken(token);
      req.user.claims = Object.keys(req.user);

      // checking claims
      if (
        requiredClaims.length > 0 &&
        !req.user.claims.filter(item => requiredClaims.includes(item)).length
      ) {
        res.status(403).json({ message: translate(req.translations, 'authNotPriviliges') });
        return;
      }

      next();
    } catch (e) {
      res.status(403).json({ message: translate(req.translations, 'authNotLogin') });
    }
  };
}

export function files(options) {
  return async (req, res, next) => {
    try {
      const contentType = req.headers['content-type'];

      if (!contentType || contentType.indexOf('application/json') > -1) {
        return next();
      }

      const { maxFiles, maxFileSize } = options;
      const busboy = new Busboy({
        headers: req.headers,
        limits: {
          files: maxFiles || 5,
          fileSize: (maxFileSize || 5) * 1024 * 1024
        }
      });

      busboy.on('field', (fieldname, val) => (req.body[fieldname] = val));
      busboy.on('file', (fieldname, file, filename) => {
        const { allowedExtentions } = options;

        if (!allowedExtentions.find(ext => filename.endsWith('.' + ext))) {
          res.status(400).json({
            message: translate(req.translations, 'uploadErrorExtentions', {
              extentions: allowedExtentions.join(', ')
            })
          });
          return;
        }

        file.on('limit', () => {
          res.status(400).json({
            message: translate(req.translations, 'uploadErrorSize', { maxFiles, maxFileSize })
          });
        });

        let tmpFilePath = path.join(os.tmpdir(), filename);
        req.body[fieldname] = (req.body[fieldname] || []).concat(tmpFilePath);
        file.pipe(fs.createWriteStream(tmpFilePath));
      });
      busboy.on('finish', () => next());
      busboy.end(req.rawBody);
    } catch (e) {
      const error = getBackendError(e);
      res.status(500).json(error);
    }
  };
}
