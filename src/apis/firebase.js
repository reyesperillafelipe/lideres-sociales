import 'firebase/auth';
import 'firebase/database';
import 'firebase/firestore';
import * as functions from 'firebase-functions';
import * as firebaseAdminLib from 'firebase-admin';
import firebaseClientLib from 'firebase/app';

const serviceAccount = require('../../service-account.json');
const { app } = functions.config();

export const firebaseClient = firebaseClientLib.initializeApp({
  apiKey: app.apikey,
  authDomain: app.authdomain,
  databaseURL: app.databaseurl,
  projectId: app.projectid,
  storageBucket: app.storagebucket,
  messagingSenderId: app.messagingsenderid
});

export const firebaseAdmin = firebaseAdminLib.initializeApp({
  credential: firebaseAdminLib.credential.cert(serviceAccount),
  databaseURL: app.databaseurl
});
