import './index.scss';
import React, { Suspense } from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Route, Switch, BrowserRouter } from 'react-router-dom';
import Main from './pages/main';
import routes from './routes';
import store from './store/store';

const renderMethod = window.initialState ? ReactDOM.hydrate : ReactDOM.render;

renderMethod(
  <Provider store={store}>
    <BrowserRouter>
      <Main>
        <Suspense fallback={<p>Loading</p>}>
          <Switch>
            {routes.map((route, index) => {
              return <Route key={index} {...route} />;
            })}
          </Switch>
        </Suspense>
      </Main>
    </BrowserRouter>
  </Provider>,
  document.querySelector('#app')
);

if ('serviceWorker' in navigator && new URL(window.location).hostname === 'sample.com') {
  navigator.serviceWorker.register('/service-worker.js');
}
