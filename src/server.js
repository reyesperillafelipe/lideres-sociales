// libraries
import * as functions from 'firebase-functions';
import express from 'express';
import fs from 'fs';
import { Helmet } from 'react-helmet';
import serialize from 'serialize-javascript';
import React from 'react';
import { Provider } from 'react-redux';
import { Route, Switch, StaticRouter, matchPath } from 'react-router-dom';
import { renderToString } from 'react-dom/server';
import { ServerStyleSheet, StyleSheetManager } from 'styled-components';
// utilities
import configuration from './static/configuration.json';
import Main from './pages/main';
import store from './store/store';
import { setConfiguration, setPreferences } from './store/actions/config-actions';
import { getQueryParamsJson } from './utils/request-utils';
import { getBackendError } from './utils/response-utils';
import routesSSR from './routes-ssr';
// routes
import routesCatalog from './apis/routes/catalog';
import routesConfig from './apis/routes/config';
import routesLocale from './apis/routes/locale';
import routesUser from './apis/routes/user';
import routesSocialLeader from './apis/routes/socialLeader';

// helpers
store.dispatch(setConfiguration(configuration));
const indexHtml = fs.readFileSync('./src/index.html', 'utf8'); 
const maintenanceHtml = fs.readFileSync('./src/static/maintenance.html', 'utf8'); 
const appServer = express();
const cors = require('cors')({ origin: true });

// server configuration
appServer.use(cors);
appServer.use((req, res, next) => {
  try {
    const acceptLanguage = (req.headers['accept-language'] || '').toLowerCase();

    req.config = store.getState().config;
    req.currency = req.headers['accept-currency'] || 'usd';
    req.dateFormat = req.headers['accept-date-format'] || 'dd/mm/yyyy';
    req.language = req.config.appLanguages.includes(acceptLanguage) ? acceptLanguage : 'en';
    req.translations = (req.config.intlData && req.config.intlData.locales[req.language]) || {};
    req.query = getQueryParamsJson(req.query);

    if (req.config.appDisabled) {
      res.send(maintenanceHtml);
      return;
    }

    next();
  } catch (e) {
    const error = getBackendError(e);
    res.status(500).json(error);
  }
});

// server routes
appServer.use('/api/catalogs', routesCatalog);
appServer.use('/api/config', routesConfig);
appServer.use('/api/locales', routesLocale);
appServer.use('/api/users', routesUser);
appServer.use('/api/socialLeader', routesSocialLeader);
appServer.get('/*', async (req, res) => {
  try {
    const currentRoute = routesSSR.find(route => matchPath(req.url, route));
    
    await store.dispatch(setPreferences({
      currency: req.currency,
      dateFormat: req.dateFormat,
      language: req.language
    }));

    if (currentRoute && currentRoute.component.getInitialState) {
      await currentRoute.component.getInitialState();
    }

    const initialState = store.getState();
    const styles = new ServerStyleSheet();
    const reactDom = renderToString(
      <Provider store={store}>
        <StaticRouter location={req.url}>
          <StyleSheetManager sheet={styles.instance}>
            <Main>
              <Switch>
                {routesSSR.map((route, index) => <Route key={index} {...route} />)}
              </Switch>
            </Main>
          </StyleSheetManager>
        </StaticRouter>
      </Provider>
    );

    const helmet = Helmet.renderStatic();
    const styleTags = styles.getStyleTags();
    let response = indexHtml;
    
    response = response.replace(/{htmlAttributes}/g, helmet.htmlAttributes.toString());
    response = response.replace(/{bodyAttributes}/g, helmet.bodyAttributes.toString());
    response = response.replace(/<!--title-->/g, helmet.title.toString());
    response = response.replace(/<!--metaTags-->/g, helmet.meta.toString());
    response = response.replace(/<!--links-->/g, helmet.link.toString());
    response = response.replace(/<!--styles-->/g, styleTags);
    response = response.replace(/<!--content-->/g, reactDom);
    response = response.replace(
      /<!--scripts-->/g,
      `<script>window.initialState=${serialize(initialState)}</script>`
    );
    
    res.send(response);
  } catch (e) {
    const error = getBackendError(e);
    res.status(500).json(error);
  }
});

// exporting services
// export * from './apis/cronos';
export * from './apis/events';
export const app = functions.https.onRequest(appServer);
