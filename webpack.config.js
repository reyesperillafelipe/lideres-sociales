const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const NodeExternals = require('webpack-node-externals');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const Webpack = require('webpack');

module.exports = (env, args) => {
  const distFolder = './dist';
  const filename = 'sample';
  const isProduction = args.mode === 'production';
  let environmentVars = {};

  if (args.mode === 'development') {
    environmentVars.APP_URL = 'http://localhost:8080';
    environmentVars.API_URL = 'http://localhost:8080/api';
    environmentVars.NODE_ENV = 'development';
  } else if (args.mode === 'production') {
    environmentVars.APP_URL = 'https://lideres-sociales-bd152.firebaseapp.com';
    environmentVars.API_URL = 'https://lideres-sociales-bd152.firebaseapp.com/api';
    environmentVars.NODE_ENV = 'production';
  }

  return [
    {
      name: 'browser', 
      devtool: isProduction ? false : 'source-map',
      optimization: {
        minimizer: [new TerserPlugin(), new OptimizeCSSAssetsPlugin()],
        splitChunks: {
          chunks: 'all',
          minChunks: 2
        }
      },
      plugins: [
        new CopyPlugin([{ from: './src/pages/_assets/', to: './' }]),
        new MiniCssExtractPlugin({ filename: `${filename}.css` }),
        new Webpack.DefinePlugin({ 'process.env': JSON.stringify(environmentVars) })
      ],
      target: 'web',
      entry: ['@babel/polyfill', './src/index.js'],
      output: {
        filename: `${filename}.js`,
        path: path.resolve(__dirname, distFolder),
        publicPath: '/'
      },
      devServer: {
        contentBase: [path.resolve(__dirname, distFolder)],
        compress: true,
        disableHostCheck: true,
        historyApiFallback: true,
        port: 9000,
        proxy: {
          '*': environmentVars.APP_URL
        }
      },
      module: {
        rules: [
          {
            test: /\.(css|scss)$/,
            use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
          },
          {
            test: /\.(html)$/,
            use: ['html-loader']
          },
          {
            enforce: 'pre',
            test: /\.js$/,
            exclude: /node_modules/,
            loader: 'eslint-loader',
            options: {
              failOnError: true,
              failOnWarning: true
            }
          },
          {
            test: /\.js$/,
            exclude: /node_modules/,
            loader: 'babel-loader'
          }
        ]
      }
    },
    {
      name: 'server',
      devtool: 'inline-source-map',
      externals: [NodeExternals()],
      plugins: [
        new Webpack.DefinePlugin({ 'process.env': JSON.stringify(environmentVars) })
      ],
      target: 'node',
      entry: ['@babel/polyfill', './src/server.js'],
      output: {
        filename: 'index.js',
        libraryTarget: 'commonjs2',
        path: path.resolve(__dirname, ''),
        publicPath: '/'
      },
      module: {
        rules: [
          {
            enforce: 'pre',
            test: /\.js$/,
            exclude: /node_modules/,
            loader: 'eslint-loader',
            options: {
              failOnError: true,
              failOnWarning: true
            }
          },
          {
            test: /\.js$/,
            loader: 'babel-loader',
            exclude: /node_modules/
          }
        ]
      }
    }
  ];
};
